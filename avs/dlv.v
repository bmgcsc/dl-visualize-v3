
APPS.DefaultApplication DLV<NEdisplayMode="maximized"> {
  CCP3.Core_Macros.UI.Window.app_window app_window<NEx=198.,NEy=110.> {
    preferences => <-.DLVcore.preferences;
    UIapp {
      title => "DL Visualize v3.3beta" +
	//#ifdef DLV_DL
	//#ifdef DLV_BGS
	//	" (experimental bgs)" +
	//#else
	//" (STFC/Materials Group internal)" +
	//#endif // DLV_BGS
	//#endif // DLV_DL
#ifdef DLV_DEMO
	" demo" +
#endif // DLV_DEMO
	", Project: " + <-.project_name;
    };
    active_menus => ((projectUI.dialog_visible == 0) &&
		     (ModelUI.dialog_visible == 0) &&
		     (GlyphsUI.dialog_visible == 0) &&
		     (<-.calculations.dialog_visible == 0) &&
		     (prefsUI.dialog_visible == 0));
    // Todo - temporary attach of viewer controls
    Menu_bar {
      File_menu {
	UImenuSeparator save_separator {
	  &color => <-.preferences.colour;
	  &fontAttributes => <-.preferences.fontAttributes;
	};
	UIcmdList {
	  cmdList => {
	    <-.<-.<-.projectUI.menu_file.dir_ops.create,
	    <-.<-.<-.ModelUI.menu_file.Create_Model.UIcmd,
	    <-.<-.<-.projectUI.menu_file.dir_ops.load,
	    <-.<-.<-.ModelUI.menu_file.Load_Model.load_menu,
	    <-.load_separator,
	    <-.<-.<-.projectUI.menu_file.sub_project.menu,
	    <-.<-.<-.projectUI.menu_file.Select.UIcmd,	    
	    <-.save_separator,
	    <-.<-.<-.projectUI.menu_file.dir_ops.save,
	    <-.<-.<-.projectUI.menu_file.dir_ops.save_as,
	    <-.<-.<-.ModelUI.menu_file.Save_Model.save_menu,
	    <-.<-.<-.<-.Output.Save_As,
	    <-.<-.<-.<-.Viewers.DLVscene_editor.Print_Editor.UI.option,
	    <-.exit_separator,
	    <-.<-.<-.exit_app.UIcmd
	  };
	};
      };
      Display_menu {
	UIcmdList {
	  cmdList => {
	    <-.<-.<-.JobUI.menu_display.UIoption,
	    <-.<-.<-.ModelUI.menu_display.k_space.UIcmd,
	    <-.<-.<-.ModelUI.menu_display.Structure,
	    <-.<-.<-.ModelUI.Loop.Animate,
	    <-.<-.<-.DataUI.menu_display.data,
	    <-.<-.<-.<-.Output.Animator.Keyframes,
	    <-.<-.<-.<-.Viewers.DLVscene_editor.Graph_Editor.UI.option
	  };
	};
      };
      View_menu {
	UIcmdList {
	  cmdList => {
	    <-.<-.<-.<-.Viewers.DLVscene_editor.View_Editor.UI.option,
	    <-.<-.<-.<-.Viewers.DLVscene_editor.Light_Editor.UI.option,
	    <-.<-.<-.<-.Viewers.DLVscene_editor.Camera_Editor.UI.option,
	    <-.<-.<-.<-.Viewers.DLVscene_editor.Track_Editor.UI.option,
	    <-.<-.<-.<-.Viewers.DLVscene_editor.Object_Editor.UI.option,
	    <-.<-.<-.<-.Viewers.DLVscene_editor.Datamap_Editor.UI.option
	  };
	};
      };
    };
    projectUI {
      menu_file {
	dir_ops {
	  atom_prefs => <-.<-.<-.<-.DLVcore.atom_prefs_file;
	  display_prefs => <-.<-.<-.<-.DLVcore.display_file;
	};
      };
    };
    DataUI {
      menu_edit {
	editUI {
	  edit_panels {
	    SliceUI {
	      planes => <-.<-.<-.<-.<-.<-.calculations.object_data.planes;
	    };
	    CutUI {
	      planes => <-.<-.<-.<-.<-.<-.calculations.object_data.planes;
	    };
	  };
	};
      };
      menu_display {
	displayUI {
	  data_panels {
	    nthreads => <-.<-.<-.<-.<-.DLVcore.nthreads;
	    StreamlineUI {
	      planes {
	       strings => <-.<-.<-.<-.<-.<-.<-.calculations.object_data.planes;
	      };
	    };
	  };
	};
      };
    };
    ModelUI {
      menu_file {
	bond_all => <-.<-.<-.Display_Objs.default_data.bond_all;
      };
    };
    JobUI {
      menu_prefs {
	local_instance {
	  Value => ((<-.panel_open == 1) &&
		    (<-.<-.<-.<-.calculations.programs.GROWL == 0));
	};
	remote_instance {
	  Value => ((<-.panel_open == 1) &&
		    (<-.<-.<-.<-.calculations.programs.GROWL == 1));
	};
      };
      menu_display {
	job_listUI {
	  list_of_jobs => <-.<-.<-.<-.project.job_list;
	  selection => <-.<-.<-.<-.project.selected_job;
	};
      };
    };
    GlyphsUI {
      edit_line {
	LineUI {
	  list {
	    strings => <-.<-.<-.<-.<-.calculations.object_data.lines;
	  };
	};
      };
      edit_plane {
	PlaneUI {
	  list {
	    strings => <-.<-.<-.<-.<-.calculations.object_data.planes;
	  };
	};
      };
      edit_box {
	BoxUI {
	  list {
	    strings => <-.<-.<-.<-.<-.calculations.object_data.volumes;
	  };
	};
      };
    };
    prefsUI {
      menu_prefs {
	config {
	  EXCURV {
	    set => <-.<-.<-.<-.<-.<-.DLV.calculations.programs.EXCURV;
	  };
	  GULP {
	    set => <-.<-.<-.<-.<-.<-.DLV.calculations.programs.GULP;
	  };
	  ROD {
	    set => <-.<-.<-.<-.<-.<-.DLV.calculations.programs.ROD;
	  };
	  CRY98 {
	    set => <-.<-.<-.<-.<-.<-.DLV.calculations.programs.CRYSTAL98;
	  };
	  CRY03 {
	    set => <-.<-.<-.<-.<-.<-.DLV.calculations.programs.CRYSTAL03;
	  };
	  CRY06 {
	    set => <-.<-.<-.<-.<-.<-.DLV.calculations.programs.CRYSTAL06;
	  };
	  CRY09 {
	    set => <-.<-.<-.<-.<-.<-.DLV.calculations.programs.CRYSTAL09;
	  };
	  CRY14 {
	    set => <-.<-.<-.<-.<-.<-.DLV.calculations.programs.CRYSTAL14;
	  };
	  CRY17 {
	    set => <-.<-.<-.<-.<-.<-.DLV.calculations.programs.CRYSTAL17;
	  };
	  /*
	  CDS {
	    set => <-.<-.<-.<-.<-.<-.DLV.calculations.programs.CDS;
	  };
	  */
	  ONETEP {
	    set => <-.<-.<-.<-.<-.<-.DLV.calculations.programs.ONETEP;
	  };
	};
	bonds {
	  bonds {
	    set => <-.<-.<-.<-.<-.<-.DLV.Display_Objs.default_data.bond_all;
	  };
	};
      };
    };
  };
  CCP3.Core_Macros.Core.DLVcore DLVcore<NEx=220.,NEy=231.> {
    initialise {
      default_project => <-.<-.app_window.project_name;
    };
  };
  CCP3.Core_Macros.Project.project project<NEx=473.,NEy=165.>;
  CCP3.Core_Macros.Calculations.config_calcs calculations<NEx=473.,NEy=231.> {
    file => <-.DLVcore.environment.DLVRC + "/calculations";
    preferences => <-.DLVcore.preferences;
    active_menus => <-.app_window.active_menus;
    UIparent => <-.app_window.UIapp;
    model_type => <-.app_window.model_type;
    job_data => <-.app_window.JobUI.menu_prefs.job_data;
    object_data {
      list_lines => <-.<-.app_window.GlyphsUI.edit_line.ok;
      list_planes => <-.<-.app_window.GlyphsUI.edit_plane.ok;
      list_volumes => <-.<-.app_window.GlyphsUI.edit_box.ok;
    };
    atom_groups => <-.app_window.ModelUI.menu_display.atoms.groups;
    create_objects {
      activate_lines {
	v_commands = "visible = 1;";
	relative => <-.<-.<-.app_window.GlyphsUI.edit_line;
      };
      activate_planes {
	v_commands = "visible = 1;";
	relative => <-.<-.<-.app_window.GlyphsUI.edit_plane;
      };
      activate_volumes {
	v_commands = "visible = 1;";
	relative => <-.<-.<-.app_window.GlyphsUI.edit_box;
      };
    };
    CDS {
      bond_all => <-.<-.Display_Objs.default_data.bond_all;
    };
  };
  macro Display_Objs<NEx=264.,NEy=297.> {
    CCP3.Renderers.Model.model_render_data default_data<NEx=352.,NEy=55.>;
    GMOD.load_v_script load_v_script<NEx=110.,NEy=55.> {
      filename => <-.<-.DLVcore.display_file;
      relative => <-.default_data;
    };
  };
  CCP3.Viewers.DLVviewers Viewers<NEx=264.,NEy=374.> {
    DLVscene_editor {
      preferences => <-.<-.DLVcore.preferences;
      View_Editor {
	InfoPanel {
	  // Attach object selector to main window
	  UI {
	    panel {
	      parent => <-.<-.<-.<-.<-.<-.app_window.UIapp;
	      height => <-.<-.<-.<-.<-.<-.app_window.Status_bar.AUstatus_bar.height;
	      y => <-.<-.<-.<-.<-.<-.app_window.Status_bar.AUstatus_bar.y;
	    };
	  };
	};
      };
    };
  };
  CCP3.Version2.Macros.OutputView.Output_Model Output<NEx=264.,NEy=484.> {
    prefs => <-.DLVcore.preferences;
    view => <-.Viewers.DLVview_selector.curr_view;
    active_menus => <-.app_window.active_menus;
    OutputImage {
      toggle {
	value => <-.<-.<-.Viewers.DLVscene_editor.View_Editor.HW_SW.set;
      };
      parse_v {
	v_commands = "run_on_do.trigger = 1;";
	relative => <-.<-.<-.Viewers.DLVscene_editor.View_Editor.HW_SW;
      };
    };
  };
#ifdef DLV_DEMO
  CCP3.Core_Macros.Demo.demo demo<NEx=737.,NEy=187.> {
    UIwarningDialog {
      parent => <-.<-.app_window.UIapp;
    };
    UIerrorDialog {
      parent => <-.<-.app_window.UIapp;
    };
  };
#endif // DLV_DEMO
};
