
#include "avs/src/core/jobs/sel_gen.hxx"
#include "avs/src/core/jobs/rcv_gen.hxx"
#include "avs/src/core/jobs/kill_gen.hxx"
#include "avs/src/core/jobs/del_gen.hxx"
#include "src/dlv/types.hxx"
#include "src/dlv/boost_lib.hxx"
#include "src/graphics/render_base.hxx"
#include "src/dlv/operation.hxx"
#include "avs/src/core/messages.hxx"
#include "avs/src/core/ui/ui.hxx"
#include "avs/src/core/render/model.hxx"
#include "avs/src/core/viewer/view.hxx"  

int CCP3_core_JobInfo_job_select::select(OMevent_mask event_mask, int seq_num)
{
  // selection (OMXint read notify)
  // info (OMXstr write)
  // file_list (OMXstr_array write)
  // active_list (OMXint write)
  // active_recover (OMXint write)
  // active_log (OMXint write)
  // is_running (OMXint write)
  bool ok = true;
  if (selection.valid_obj()) {
    CCP3::show_as_busy();
    int index = (int) selection;
    char status[128];
    bool use_list = false;
    bool recover = false;
    bool log = false;
    bool run = false;
    char message[256];
    ok = DLV::operation::select_current_job(index, status, /*files */
					    use_list, recover, log, run,
					    message, 256);
    CCP3::show_as_idle();
    if (ok) {
      info = status;
      active_list = use_list;
      active_recover = recover;
      active_log = log;
      is_running = run;
      return OM_STAT_SUCCESS;
    } else
      return CCP3::return_info(DLV_ERROR, "CCP3.Job.Select", message);
  } else {
    info.set_obj_val(OMnull_obj, 0);
    active_list = 0;
    active_recover = 0;
    active_log = 0;
    is_running = 0;
  }
  return OM_STAT_SUCCESS;
}

int CCP3_core_JobInfo_job_recover::recover(OMevent_mask event_mask,
					   int seq_num)
{
  // selection (OMXint read req)
  // file_list (OMXint_array read)
  // use_list (OMXint read req)
  // update (OMXint write)
  CCP3::show_as_busy();
  int index = (int) selection;
  DLVreturn_type ok = DLV_OK;
  char message[256];
  bool use_files = false;
  DLV::operation *op = 0;
  ok = DLV::operation::recover_current_job(index, use_files,
					   op, message, 256);
  update = 1; // Todo ?
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(DLV_ERROR, "CCP3.Job.Recover", message);
  else {
    if (op != 0){
      CCP3::select_3Dview(op);
      return CCP3::render_in_view(op, op->open_new_view());
    }
    else
      return OM_STAT_SUCCESS;
  }
}

int CCP3_core_JobInfo_job_kill::kill(OMevent_mask event_mask, int seq_num)
{
  // selection (OMXint read req)
  CCP3::show_as_busy();
  DLVreturn_type ok = DLV_OK;
  char message[256];
  ok = DLV::operation::kill_current_job((int) selection, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(DLV_ERROR, "CCP3.Job.Kill", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_JobInfo_job_delete::remove(OMevent_mask event_mask, int seq_num)
{
  // selection (OMXint read req)
  CCP3::show_as_busy();
  DLVreturn_type ok = DLV_OK;
  char message[256];
  ok = DLV::operation::delete_current_job((int) selection, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(DLV_ERROR, "CCP3.Job.Delete", message);
  else
    return OM_STAT_SUCCESS;
}
