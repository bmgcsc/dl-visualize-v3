
#include "avs/src/core/objects/cpnt_gen.hxx"
#include "avs/src/core/objects/upnt_gen.hxx"
#include "avs/src/core/objects/clne_gen.hxx"
#include "avs/src/core/objects/ulne_gen.hxx"
#include "avs/src/core/objects/cpln_gen.hxx"
#include "avs/src/core/objects/upln_gen.hxx"
#include "avs/src/core/objects/cbox_gen.hxx"
#include "avs/src/core/objects/ubox_gen.hxx"
#include "avs/src/core/objects/llne_gen.hxx"
#include "avs/src/core/objects/lpln_gen.hxx"
#include "avs/src/core/objects/lbox_gen.hxx"
#include "avs/src/core/objects/lwlf_gen.hxx"
#include "avs/src/core/objects/wlff_gen.hxx"
#include "avs/src/core/objects/leed_gen.hxx"
#include "avs/src/core/objects/edit_gen.hxx"
#include "avs/src/core/objects/csph_gen.hxx"
#include "avs/src/core/objects/usph_gen.hxx"
#include "src/dlv/types.hxx"
#include "src/dlv/boost_lib.hxx"
#include "src/graphics/render_base.hxx"
#include "src/dlv/operation.hxx"
#include "avs/src/core/ui/ui.hxx"
#include "avs/src/core/messages.hxx"
#include "avs/src/core/viewer/view.hxx"

int CCP3_core_Objects_create_point::create(OMevent_mask event_mask,
					   int seq_num)
{
  // name (OMXstr read req)
  // space (OMXint read req)
  // trigger (OMXint read req notify)
  int ok = OM_STAT_SUCCESS;
  if ((int)trigger == 1) {
    CCP3::show_as_busy();
    char message[256];
    DLV::operation *op = 0;
    bool kspace = (bool) (space == 1);
    op = DLV::operation::create_point((char *)name, kspace, message, 256);
    if (op == 0)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.Object.Point.create", message);
    else
      ok = CCP3::update_op_in_3Dview(op, kspace);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Objects_create_point::cancel(OMevent_mask event_mask,
					   int seq_num)
{
  // trigger (OMXint read req)
  int ok = OM_STAT_SUCCESS;
  if ((int)trigger == 1) {
    CCP3::show_as_busy();
    if (DLV::operation::cancel_pending() != DLV_OK)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Point.cancel",
			     "Problem canceling point");
    else
      ok = CCP3::attach_op_to_current_3Dview(DLV::operation::get_current(),
					     false, false);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Objects_create_point::accept(OMevent_mask event_mask,
					   int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  if (DLV::operation::accept_pending() != DLV_OK)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Point.accept",
			   "Problem accepting point");
  CCP3::show_as_idle();
  return ok;
}

int CCP3_core_Objects_update_point::set_fractional(OMevent_mask event_mask,
						   int seq_num)
{
  // x (OMXfloat read req)
  // y (OMXfloat read req)
  // z (OMXfloat read req)
  // conventional (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 0;
  bool conv = ((int)conventional) == 1;
  ok = DLV::operation::update_point(flag, (float)x, (float)y, (float)z,
				    conv, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Point.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_point::set_cartesian(OMevent_mask event_mask,
						  int seq_num)
{
  // x (OMXfloat read req)
  // y (OMXfloat read req)
  // z (OMXfloat read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 1;
  bool conv = ((int)conventional) == 1;
  ok = DLV::operation::update_point(flag, (float)x, (float)y, (float)z,
				    conv, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Point.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_create_line::create(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req)
  // space (OMXint read req)
  // trigger (OMXint read req notify)
  int ok = OM_STAT_SUCCESS;
  if ((int)trigger == 1) {
    CCP3::show_as_busy();
    char message[256];
    DLV::operation *op = 0;
    bool kspace = (bool) (space == 1);
    op = DLV::operation::create_line((char *)name, kspace, message, 256);
    if (op == 0)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.Object.Line.create", message);
    else
      ok = CCP3::update_op_in_3Dview(op, kspace);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Objects_create_line::cancel(OMevent_mask event_mask, int seq_num)
{
  // trigger (OMXint read req)
  int ok = OM_STAT_SUCCESS;
  if ((int)trigger == 1) {
    CCP3::show_as_busy();
    if (DLV::operation::cancel_pending() != DLV_OK)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Line.cancel",
			     "Problem canceling line");
    else
      ok = CCP3::attach_op_to_current_3Dview(DLV::operation::get_current(),
					     false, false);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Objects_create_line::accept(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  if (DLV::operation::accept_pending() != DLV_OK)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Line.accept",
			   "Problem accepting line");
  CCP3::show_as_idle();
  return ok;
}

int CCP3_core_Objects_update_line::update_atom_oa(OMevent_mask event_mask,
						  int seq_num)
{
  // oa (OMXint read req notify)
  // include_o (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 0;
  ok = DLV::operation::update_line(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0,
				   false, false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Line.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_line::update_atom_o(OMevent_mask event_mask,
						 int seq_num)
{
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 2;
  ok = DLV::operation::update_line(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0,
				   false, false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Line.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_line::update_atom_a(OMevent_mask event_mask,
						 int seq_num)
{
  // atom_a (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 1;
  ok = DLV::operation::update_line(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0,
				   false, false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Line.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_line::set_miller(OMevent_mask event_mask,
					      int seq_num)
{
  // miller (OMXint read req notify)
  // h (OMXint read req)
  // k (OMXint read req)
  // l (OMXint read req)
  // conventional (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 3;
  bool conv = ((int)conventional) == 1;
  bool p = ((int)parent_lattice) == 1;
  ok = DLV::operation::update_line(flag, (int)h, (int)k, (int)l, 0.0, 0.0, 0.0,
				   0, conv, false, p, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Line.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_line::inherit(OMevent_mask event_mask, int seq_num)
{
  // object (OMXint read req)
  // copy_from (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 4;
  ok = DLV::operation::update_line(flag, 0, 0, 0, 0.0, 0.0, 0.0, (int)object,
				   false, false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Line.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_line::update_coord_o(OMevent_mask event_mask,
						  int seq_num)
{
  // coord_o (OMXint read req notify)
  // x (OMXfloat read req)
  // y (OMXfloat read req)
  // z (OMXfloat read req)
  // fractional (OMXint read req)
  // conventional (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 6;
  bool frac = ((int)fractional) == 1;
  bool conv = ((int)conventional) == 1;
  bool p = ((int)parent_lattice) == 1;
  ok = DLV::operation::update_line(flag, 0, 0, 0, (float)x, (float)y, (float)z,
				   0, conv, frac, p, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Line.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_line::update_coord_a(OMevent_mask event_mask,
						  int seq_num)
{
  // coord_a (OMXint read req notify)
  // x (OMXfloat read req)
  // y (OMXfloat read req)
  // z (OMXfloat read req)
  // fractional (OMXint read req)
  // conventional (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 5;
  bool frac = ((int)fractional) == 1;
  bool conv = ((int)conventional) == 1;
  bool p = ((int)parent_lattice) == 1;
  ok = DLV::operation::update_line(flag, 0, 0, 0, (float)x, (float)y, (float)z,
				   0, conv, frac, p, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Line.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_create_plane::create(OMevent_mask event_mask,
					   int seq_num)
{
  // name (OMXstr read req)
  // space (OMXint read req)
  // trigger (OMXint read req notify)
  int ok = OM_STAT_SUCCESS;
  if ((int)trigger == 1) {
    CCP3::show_as_busy();
    char message[256];
    DLV::operation *op = 0;
    bool kspace = (bool) (space == 1);
    op = DLV::operation::create_plane((char *)name, kspace, message, 256);
    if (op == 0)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.Object.Plane.create", message);
    else
      ok = CCP3::update_op_in_3Dview(op, kspace);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Objects_create_plane::cancel(OMevent_mask event_mask,
					   int seq_num)
{
  // trigger (OMXint read req)
  int ok = OM_STAT_SUCCESS;
  if ((int)trigger == 1) {
    CCP3::show_as_busy();
    char message[256];
    (void) DLV::operation::set_data_transform(false, message, 256);
    if (DLV::operation::cancel_pending() != DLV_OK)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Plane.cancel",
			     "Problem canceling plane");
    else
      ok = CCP3::attach_op_to_current_3Dview(DLV::operation::get_current(),
					     false, false);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Objects_create_plane::accept(OMevent_mask event_mask,
					   int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  char message[256];
  (void) DLV::operation::set_data_transform(false, message, 256);
  if (DLV::operation::accept_pending() != DLV_OK)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Plane.accept",
			   "Problem accepting plane");
  CCP3::show_as_idle();
  return ok;
}

int CCP3_core_Objects_update_plane::update_oa(OMevent_mask event_mask,
					      int seq_num)
{
  // oa (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 0;
  ok = DLV::operation::update_plane(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0, false,
				    false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Plane.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_plane::update_ob(OMevent_mask event_mask,
					      int seq_num)
{
  // ob (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 2;
  ok = DLV::operation::update_plane(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0, false,
				    false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Plane.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_plane::set_origin(OMevent_mask event_mask,
					       int seq_num)
{
  // origin (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 4;
  ok = DLV::operation::update_plane(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0, false,
				    false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Plane.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_plane::set_centre(OMevent_mask event_mask,
					       int seq_num)
{
  // centre (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 5;
  ok = DLV::operation::update_plane(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0, false,
				    false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Plane.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_plane::set_miller(OMevent_mask event_mask,
					       int seq_num)
{
  // miller (OMXint read req notify)
  // h (OMXint read req)
  // k (OMXint read req)
  // l (OMXint read req)
  bool conv = ((int)conventional) == 1;
  bool p = ((int)parent_obj) == 1;
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 6;
  ok = DLV::operation::update_plane(flag, (int)h, (int)k, (int)l, 0.0, 0.0, 0.0,
				    0, conv, false, p, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Plane.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_plane::align_l(OMevent_mask event_mask,
					    int seq_num)
{
  // align_lattice (OMXint read req notify)
  // conventional (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 7;
  bool conv = ((int)conventional) == 1;
  bool p = ((int)parent_obj) == 1;
  ok = DLV::operation::update_plane(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0, conv,
				    false, p, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Plane.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_plane::align_c(OMevent_mask event_mask,
					    int seq_num)
{
  // align_xyz (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 8;
  bool conv = ((int)conventional) == 1;
  ok = DLV::operation::update_plane(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0, conv,
				    false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Plane.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_plane::set_l(OMevent_mask event_mask, int seq_num)
{
  // lattice (OMXint read req notify)
  // conventional (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 9;
  bool conv = ((int)conventional) == 1;
  bool p = ((int)parent_obj) == 1;
  ok = DLV::operation::update_plane(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0, conv,
				    false, p, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Plane.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_plane::set_c(OMevent_mask event_mask, int seq_num)
{
  // cartesian (OMXint read req notify)
  // conventional (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 10;
  bool conv = ((int)conventional) == 1;
  ok = DLV::operation::update_plane(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0, conv,
				    false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Plane.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_plane::update_boa(OMevent_mask event_mask,
					       int seq_num)
{
  // boa (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 11;
  ok = DLV::operation::update_plane(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0, false,
				    false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Plane.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_plane::update_a(OMevent_mask event_mask,
					     int seq_num)
{
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 1;
  ok = DLV::operation::update_plane(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0, false,
				    false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Plane.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_plane::update_b(OMevent_mask event_mask,
					     int seq_num)
{
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 3;
  ok = DLV::operation::update_plane(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0, false,
				    false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Plane.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_plane::coord_o(OMevent_mask event_mask,
					    int seq_num)
{
  // conventional (OMXint read req)
  // x (OMXfloat read req)
  // y (OMXfloat read req)
  // z (OMXfloat read req)
  // frac (OMXint read req)
  // parent_obj (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 12;
  bool f = ((int)frac) == 1;
  bool conv = ((int)conventional) == 1;
  bool p = ((int)parent_obj) == 1;
  ok = DLV::operation::update_plane(flag, 0, 0, 0, (float)x, (float)y, (float)z,
				    0, conv, f, p, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Plane.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_plane::coord_a(OMevent_mask event_mask,
					    int seq_num)
{
  // conventional (OMXint read req)
  // x (OMXfloat read req)
  // y (OMXfloat read req)
  // z (OMXfloat read req)
  // frac (OMXint read req)
  // parent_obj (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 13;
  bool f = ((int)frac) == 1;
  bool conv = ((int)conventional) == 1;
  bool p = ((int)parent_obj) == 1;
  ok = DLV::operation::update_plane(flag, 0, 0, 0, (float)x, (float)y, (float)z,
				    0, conv, f, p, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Plane.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_plane::coord_b(OMevent_mask event_mask,
					    int seq_num)
{
  // conventional (OMXint read req)
  // x (OMXfloat read req)
  // y (OMXfloat read req)
  // z (OMXfloat read req)
  // frac (OMXint read req)
  // parent_obj (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 14;
  bool f = ((int)frac) == 1;
  bool conv = ((int)conventional) == 1;
  bool p = ((int)parent_obj) == 1;
  ok = DLV::operation::update_plane(flag, 0, 0, 0, (float)x, (float)y, (float)z,
				    0, conv, f, p, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Plane.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_plane::coord_centre(OMevent_mask event_mask,
						 int seq_num)
{
  // conventional (OMXint read req)
  // x (OMXfloat read req)
  // y (OMXfloat read req)
  // z (OMXfloat read req)
  // frac (OMXint read req)
  // parent_obj (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 15;
  bool f = ((int)frac) == 1;
  bool conv = ((int)conventional) == 1;
  bool p = ((int)parent_obj) == 1;
  ok = DLV::operation::update_plane(flag, 0, 0, 0, (float)x, (float)y, (float)z,
				    0, conv, f, p, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Plane.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_plane::inherit(OMevent_mask event_mask,
					    int seq_num)
{
  // object (OMXint read req)
  // copy_from (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 16;
  ok = DLV::operation::update_plane(flag, 0, 0, 0, 0.0, 0.0, 0.0, (int)object,
				    false, false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Plane.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_create_box::create(OMevent_mask event_mask, int seq_num)
{
  // name (OMXstr read req)
  // space (OMXint read req)
  // trigger (OMXint read req notify)
  int ok = OM_STAT_SUCCESS;
  if ((int)trigger == 1) {
    CCP3::show_as_busy();
    char message[256];
    DLV::operation *op = 0;
    bool kspace = (bool) (space == 1);
    op = DLV::operation::create_volume((char *)name, kspace, message, 256);
    if (op == 0)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.Object.Volume.create", message);
    else
      ok = CCP3::update_op_in_3Dview(op, kspace);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Objects_create_box::cancel(OMevent_mask event_mask, int seq_num)
{
  // trigger (OMXint read req)
  int ok = OM_STAT_SUCCESS;
  if ((int)trigger == 1) {
    CCP3::show_as_busy();
    char message[256];
    (void) DLV::operation::set_data_transform(false, message, 256);
    if (DLV::operation::cancel_pending() != DLV_OK)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Volume.cancel",
			     "Problem canceling 3D region");
    else
      ok = CCP3::attach_op_to_current_3Dview(DLV::operation::get_current(),
					     false, false);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Objects_create_box::accept(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  char message[256];
  (void) DLV::operation::set_data_transform(false, message, 256);
  if (DLV::operation::accept_pending() != DLV_OK)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Volume.accept",
			   "Problem accepting 3D region");
  CCP3::show_as_idle();
  return ok;
}

int CCP3_core_Objects_update_box::update_oa(OMevent_mask event_mask,
					    int seq_num)
{
  // oa (OMXint read req notify)
  // include_o (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 0;
  ok = DLV::operation::update_3D_region(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0,
					false, false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_region", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_box::update_ob(OMevent_mask event_mask,
					    int seq_num)
{
  // ob (OMXint read req notify)
  // include_o (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 2;
  ok = DLV::operation::update_3D_region(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0,
					false, false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_region", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_box::update_oc(OMevent_mask event_mask,
					    int seq_num)
{
  // oc (OMXint read req notify)
  // include_o (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 4;
  ok = DLV::operation::update_3D_region(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0,
					false, false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_region", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_box::set_origin(OMevent_mask event_mask,
					     int seq_num)
{
  // origin (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 6;
  ok = DLV::operation::update_3D_region(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0,
					false, false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_region", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_box::set_centre(OMevent_mask event_mask,
					     int seq_num)
{
  // centre (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 7;
  ok = DLV::operation::update_3D_region(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0,
					false, false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_region", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_box::set_miller(OMevent_mask event_mask,
					     int seq_num)
{
  // miller (OMXint read req notify)
  // h (OMXint read req)
  // k (OMXint read req)
  // l (OMXint read req)
  bool f = ((int)frac) == 1;
  bool conv = ((int)conventional) == 1;
  bool p = ((int)parent_obj) == 1;
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 8;
  ok = DLV::operation::update_3D_region(flag, (int)h, (int)k, (int)l,
					0.0, 0.0, 0.0, 0, conv, f, p,
					message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_region", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_box::align_l(OMevent_mask event_mask, int seq_num)
{
  // align_lattice (OMXint read req notify)
  // conventional (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 9;
  bool f = ((int)frac) == 1;
  bool conv = ((int)conventional) == 1;
  bool p = ((int)parent_obj) == 1;
  ok = DLV::operation::update_3D_region(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0,
					conv, f, p, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_region", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_box::align_c(OMevent_mask event_mask, int seq_num)
{
  // align_xyz (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 10;
  ok = DLV::operation::update_3D_region(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0,
					false, false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_region", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_box::set_l(OMevent_mask event_mask, int seq_num)
{
  // lattice (OMXint read req notify)
  // conventional (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 11;
  bool f = ((int)frac) == 1;
  bool conv = ((int)conventional) == 1;
  bool p = ((int)parent_obj) == 1;
  ok = DLV::operation::update_3D_region(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0,
					conv, f, p, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_region", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_box::set_c(OMevent_mask event_mask, int seq_num)
{
  // cartesian (OMXint read req notify)
  // conventional (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 12;
  ok = DLV::operation::update_3D_region(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0,
					false, false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_region", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_box::inherit(OMevent_mask event_mask, int seq_num)
{
  // object (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 18;
  ok = DLV::operation::update_3D_region(flag, 0, 0, 0, 0.0, 0.0, 0.0,
					(int)object, false, false, false,
					message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_region", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_box::update_a(OMevent_mask event_mask, int seq_num)
{
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 1;
  ok = DLV::operation::update_3D_region(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0,
					false, false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_region", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_box::update_b(OMevent_mask event_mask, int seq_num)
{
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 3;
  ok = DLV::operation::update_3D_region(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0,
					false, false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_region", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_box::update_c(OMevent_mask event_mask, int seq_num)
{
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 5;
  ok = DLV::operation::update_3D_region(flag, 0, 0, 0, 0.0, 0.0, 0.0, 0,
					false, false, false, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_region", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_box::coord_o(OMevent_mask event_mask, int seq_num)
{
  // conventional (OMXint read req)
  // x (OMXfloat read req)
  // y (OMXfloat read req)
  // z (OMXfloat read req)
  // frac (OMXint read req)
  // parent_obj (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 16;
  bool f = ((int)frac) == 1;
  bool conv = ((int)conventional) == 1;
  bool p = ((int)parent_obj) == 1;
  ok = DLV::operation::update_3D_region(flag, 0, 0, 0, (float)x, (float)y,
					(float)z, 0, conv, f, p, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_region", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_box::coord_a(OMevent_mask event_mask, int seq_num)
{
  // conventional (OMXint read req)
  // x (OMXfloat read req)
  // y (OMXfloat read req)
  // z (OMXfloat read req)
  // frac (OMXint read req)
  // parent_obj (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 13;
  bool f = ((int)frac) == 1;
  bool conv = ((int)conventional) == 1;
  bool p = ((int)parent_obj) == 1;
  ok = DLV::operation::update_3D_region(flag, 0, 0, 0, (float)x, (float)y,
					(float)z, 0, conv, f, p, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_region", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_box::coord_b(OMevent_mask event_mask, int seq_num)
{
  // conventional (OMXint read req)
  // x (OMXfloat read req)
  // y (OMXfloat read req)
  // z (OMXfloat read req)
  // frac (OMXint read req)
  // parent_obj (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 14;
  bool f = ((int)frac) == 1;
  bool conv = ((int)conventional) == 1;
  bool p = ((int)parent_obj) == 1;
  ok = DLV::operation::update_3D_region(flag, 0, 0, 0, (float)x, (float)y,
					(float)z, 0, conv, f, p, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_region", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_box::coord_c(OMevent_mask event_mask, int seq_num)
{
  // conventional (OMXint read req)
  // x (OMXfloat read req)
  // y (OMXfloat read req)
  // z (OMXfloat read req)
  // frac (OMXint read req)
  // parent_obj (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 15;
  bool f = ((int)frac) == 1;
  bool conv = ((int)conventional) == 1;
  bool p = ((int)parent_obj) == 1;
  ok = DLV::operation::update_3D_region(flag, 0, 0, 0, (float)x, (float)y,
					(float)z, 0, conv, f, p, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_region", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_box::coord_centre(OMevent_mask event_mask,
					       int seq_num)
{
  // conventional (OMXint read req)
  // x (OMXfloat read req)
  // y (OMXfloat read req)
  // z (OMXfloat read req)
  // frac (OMXint read req)
  // parent_obj (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 17;
  bool f = ((int)frac) == 1;
  bool conv = ((int)conventional) == 1;
  bool p = ((int)parent_obj) == 1;
  ok = DLV::operation::update_3D_region(flag, 0, 0, 0, (float)x, (float)y,
					(float)z, 0, conv, f, p, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_region", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_list_lines::generate(OMevent_mask event_mask,
					   int seq_num)
{
  // trigger (OMXint read)
  CCP3::show_as_busy();
  char message[256];
  DLV::string *names;
  int n = DLV::operation::list_lines(names, message, 256);
  OMset_array_size(labels.obj_id(), n);
  if (n > 0) {
    for (int i = 0; i < n; i++)
      OMset_str_array_val(labels.obj_id(), i, names[i].c_str());
    delete [] names;
  }
  CCP3::show_as_idle();
  return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_list_planes::generate(OMevent_mask event_mask,
					    int seq_num)
{
  // trigger (OMXint read)
  CCP3::show_as_busy();
  char message[256];
  DLV::string *names;
  int n = DLV::operation::list_planes(names, message, 256);
  OMset_array_size(labels.obj_id(), n);
  if (n > 0) {
    for (int i = 0; i < n; i++)
      OMset_str_array_val(labels.obj_id(), i, names[i].c_str());
    delete [] names;
  }
  CCP3::show_as_idle();
  return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_list_volumes::generate(OMevent_mask event_mask,
					     int seq_num)
{
  // trigger (OMXint read)
  CCP3::show_as_busy();
  char message[256];
  DLV::string *names;
  int n = DLV::operation::list_3D_regions(names, message, 256);
  OMset_array_size(labels.obj_id(), n);
  if (n > 0) {
    for (int i = 0; i < n; i++)
      OMset_str_array_val(labels.obj_id(), i, names[i].c_str());
    delete [] names;
  }
  CCP3::show_as_idle();
  return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_create_wulff::create(OMevent_mask event_mask,
					   int seq_num)
{
  // name (OMXstr read req)
  // trigger (OMXint read req notify)
  int ok = OM_STAT_SUCCESS;
  if ((int)trigger == 1) {
    CCP3::show_as_busy();
    char message[256];
    DLV::operation *op = 0;
    op = DLV::operation::create_wulff((char *)name, message, 256);
    if (op == 0)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.Object.Wulff.create", message);
    else
      ok = CCP3::update_op_in_3Dview(op);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Objects_create_wulff::add(OMevent_mask event_mask, int seq_num)
{
  // data (wulff_data read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  bool conv = ((int)data.conventional) == 1;
  ok = DLV::operation::update_wulff((int)data.h, (int)data.k, (int)data.l,
				    (float)data.energy, (float)data.r,
				    (float)data.g, (float)data.b, conv,
				    message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Wulff.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_create_wulff::load(OMevent_mask event_mask, int seq_num)
{
  // data (wulff_data read req)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  ok = DLV::operation::update_wulff((char *)data.filename, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Wulff.load", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_create_wulff::cancel(OMevent_mask event_mask,
					   int seq_num)
{
  // trigger (OMXint read req)
  int ok = OM_STAT_SUCCESS;
  if ((int)trigger == 1) {
    CCP3::show_as_busy();
    if (DLV::operation::cancel_pending() != DLV_OK)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Wulff.cancel",
			     "Problem canceling Wulff Plot");
    else
      ok = CCP3::attach_op_to_current_3Dview(DLV::operation::get_current(),
					     false, false);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Objects_create_wulff::accept(OMevent_mask event_mask,
					   int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  if (DLV::operation::accept_pending() != DLV_OK)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Wulff.accept",
			   "Problem accepting Wulff Plot");
  CCP3::show_as_idle();
  return ok;
}

int CCP3_core_Objects_create_leed::create(OMevent_mask event_mask, int seq_num)
{
  // trigger (OMXint read req notify)
  int ok = OM_STAT_SUCCESS;
  if ((int)trigger == 1) {
    CCP3::show_as_busy();
    char message[256];
    DLV::operation *op = 0;
    op = DLV::operation::create_leed(message, 256);
    if (op == 0)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.Object.LEED.create", message);
    else
      ok = CCP3::update_op_in_3Dview(op);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Objects_create_leed::update(OMevent_mask event_mask, int seq_num)
{
  // domains (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  bool value = ((int)domains) == 1;
  ok = DLV::operation::update_leed(value, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.LEED.update", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_create_leed::cancel(OMevent_mask event_mask, int seq_num)
{
  // trigger (OMXint read req)
  int ok = OM_STAT_SUCCESS;
  if ((int)trigger == 1) {
    CCP3::show_as_busy();
    if (DLV::operation::cancel_pending() != DLV_OK)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.core.LEED.cancel",
			     "Problem canceling LEED pattern");
    else
      ok = CCP3::attach_op_to_current_3Dview(DLV::operation::get_current(),
					     false, false);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Objects_create_leed::accept(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  if (DLV::operation::accept_pending() != DLV_OK)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.core.LEED.accept",
			   "Problem accepting LEED pattern");
  CCP3::show_as_idle();
  return ok;
}

int CCP3_core_Objects_edit_object::attach(OMevent_mask event_mask, int seq_num)
{
  // trigger (OMXint read req notify)
  int ok = OM_STAT_SUCCESS;
  if ((int)active == 1) {
    CCP3::show_as_busy();
    bool value = ((bool)((int)trigger == 1));
    char message[256];
    if (DLV::operation::set_data_transform(value, message, 256) != DLV_OK)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.core.DataEditor", message);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Objects_create_sphere::create(OMevent_mask event_mask,
					    int seq_num)
{
  // name (OMXstr read req)
  // space (OMXint read req)
  // trigger (OMXint read req notify)
  int ok = OM_STAT_SUCCESS;
  if ((int)trigger == 1) {
    CCP3::show_as_busy();
    char message[256];
    DLV::operation *op = 0;
    //bool kspace = (bool) (space == 1);
    bool kspace = false;
    op = DLV::operation::create_sphere((char *)name, kspace, message, 256);
    if (op == 0)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.Object.Sphere.create", message);
    else
      ok = CCP3::update_op_in_3Dview(op, kspace);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Objects_create_sphere::cancel(OMevent_mask event_mask,
					    int seq_num)
{
  // trigger (OMXint read req)
  int ok = OM_STAT_SUCCESS;
  if ((int)trigger == 1) {
    CCP3::show_as_busy();
    //char message[256];
    //(void) DLV::operation::set_data_transform(false, message, 256);
    if (DLV::operation::cancel_pending() != DLV_OK)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Sphere.cancel",
			     "Problem canceling sphere");
    else
      ok = CCP3::attach_op_to_current_3Dview(DLV::operation::get_current(),
					     false, false);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_core_Objects_create_sphere::accept(OMevent_mask event_mask,
					    int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  //char message[256];
  //(void) DLV::operation::set_data_transform(false, message, 256);
  if (DLV::operation::accept_pending() != DLV_OK)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Sphere.accept",
			   "Problem accepting sphere");
  CCP3::show_as_idle();
  return ok;
}

int CCP3_core_Objects_update_sphere::set_origin(OMevent_mask event_mask,
						int seq_num)
{
  // origin (OMXint read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 0;
  ok = DLV::operation::update_sphere(flag, 0.0, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_sphere", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_update_sphere::set_radius(OMevent_mask event_mask,
						int seq_num)
{
  // radius (OMXfloat read req notify)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int flag = 1;
  ok = DLV::operation::update_sphere(flag, (float)radius, message, 256);
  CCP3::show_as_idle();
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Volume.update_sphere", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Objects_list_wulff::generate(OMevent_mask event_mask, int seq_num)
{
  CCP3::show_as_busy();
  char message[256];
  DLV::string *names;
  int n = DLV::operation::list_wulff(names, message, 256);
  OMset_array_size(labels.obj_id(), n);
  if (n > 0) {
    for (int i = 0; i < n; i++)
      OMset_str_array_val(labels.obj_id(), i, names[i].c_str());
    delete [] names;
  }
  CCP3::show_as_idle();
  return OM_STAT_SUCCESS;
}
