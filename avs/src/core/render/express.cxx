
#include <cmath>
#include "avs/src/express/scenes.hxx"
#include "src/dlv/types.hxx"
#include "src/dlv/boost_lib.hxx"
#include "src/dlv/utils.hxx"
#include "src/graphics/render_base.hxx"
#include "src/dlv/operation.hxx"
#include "src/graphics/toolkit_obj.hxx"
#include "avs/src/core/render/model.hxx"
#include "avs/src/core/viewer/view.hxx"
#include "avs/src/core/ui/ui.hxx"
#include "avs/src/core/messages.hxx"
#include "avs/src/core/render/cell_gen.hxx"
#include "avs/src/core/render/latt_gen.hxx"
#include "avs/src/core/render/atom_gen.hxx"
#include "avs/src/core/render/bond_gen.hxx"
#include "avs/src/core/render/sel_gen.hxx"
#include "avs/src/core/render/outl_gen.hxx"
#include "avs/src/core/render/ksp_gen.hxx"
#include "avs/src/core/render/conn_gen.hxx"
#include "avs/src/core/render/st2D_gen.hxx"
#include "avs/src/core/render/stag_gen.hxx"
#include "avs/src/core/render/rtag_gen.hxx"
#include "avs/src/core/render/c2D_gen.hxx"
#include "avs/src/core/render/sl_gen.hxx"
#include "avs/src/core/render/file_gen.hxx"
#include "avs/src/core/render/krep_gen.hxx"

void CCP3::set_model_type(const int m)
{
  static OMobj_id model_type = OMnull_obj;

  if (OMis_null_obj(model_type))
    model_type = OMfind_str_subobj(OMinst_obj, "DLV.app_window.model_type",
				   OM_OBJ_RW);
  OMset_int_val(model_type, m);
}

void CCP3::set_model_name(CCP3_Viewers_Scenes_DLV3Dscene *view,
			  const char name[])
{
  view->Model_Name.set_str_val(name);
}

int CCP3::render_in_view(DLV::operation *op, const bool new_view,
			 const bool kspace)
{
  CCP3_Viewers_Scenes_DLV3Dscene *view = 0;
  bool is_new = false;

  show_as_busy();
  if (!new_view)
    view = CCP3::get_current_3Dview(true, kspace);
  if (view == 0) {
    is_new = true;
    if (CCP3::create_3Dview(view, kspace) != OM_STAT_SUCCESS) {
      ERRverror("CCP3.core.Model.render", ERR_ERROR,
		"Failed to create new 3D viewer");
      return OM_STAT_ERROR;
    }
  }
  CCP3::set_model_type(op->get_model_type());
  char model_name[256];
  op->get_model_name(model_name, 256);
  CCP3::set_model_name(view, model_name);
  char message[256];
  DLVreturn_type ok = DLV_OK;
  if (kspace)
    ok = op->render_k(message, 256);
  else
    ok = op->render_r(message, 256);
  show_as_idle();
  if (ok == DLV_OK) {
    int status = CCP3::attach_op_to_current_3Dview(op);
    if (status == OM_STAT_SUCCESS and !is_new)
      status = CCP3::attach_op_to_UI(op, kspace);
    return status;
  } else
    return CCP3::return_info(ok, "CCP3::render_model", message);
}

int CCP3_core_Render_update_cell::update(OMevent_mask event_mask, int seq_num)
{
  // data (common_options notify)
  CCP3::show_as_busy();
  char message[256];
  // The object contains the data, so we don't need to pass it.
  DLVreturn_type ok = DLV::operation::update_current_cell(message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Render.Cell", message);
  else
    return OM_STAT_SUCCESS;  
}

int CCP3_core_Render_connector::connect(OMevent_mask event_mask, int seq_num)
{
  // trigger (OMXint read req notify)
  // from_obj (OMXstr read)
  // to_obj (OMXstr read)
  // ref (OMXint read)
  // connected (OMXint read write)
  int ok = OM_STAT_SUCCESS;
  OMobj_id from_id = OMfind_str_subobj(obj_id(), (char *)from_obj, OM_OBJ_RW);
  if (!OMis_null_obj(from_id)) {
    OMobj_id to_id = OMfind_str_subobj(obj_id(), (char *)to_obj, OM_OBJ_RW);
    if (!OMis_null_obj(from_id)) {
      int val = (int)trigger;
      int is_ref = (int) ref;
      int conn = (int)connected;
      if (val == 0 or (event_mask & OM_EVENT_DEINST)) {
	if (conn == 1) {
	  if (is_ref == 1)
	    ok = OMdel_obj_ref(to_id, from_id, 0);
	  else
	    ok = OMset_obj_val(to_id, OMnull_obj, 0);
	  connected = 0;
	}
      } else {
	if (conn == 0) {
	  if (is_ref == 1)
	    ok = OMadd_obj_ref(to_id, from_id, 0);
	  else
	    ok = OMset_obj_val(to_id, from_id, 0);
	  connected = 1;
	}
      }
    }
  }
  if (ok != OM_STAT_SUCCESS)
    return CCP3::return_info(DLV_ERROR, "CCP3.core.Render.connector",
			     "Failed to make connection");
  else
    return OM_STAT_SUCCESS;  
}

int CCP3_core_Render_plane_to_2Dstruct::reduce_dim(OMevent_mask event_mask,
						   int seq_num)
{
  // in (Mesh_Struct read req notify)
  CCP3::show_as_busy();
  if ((int)in.ndim == 0)
    return OM_STAT_UNDEF;
  xp_long *dims = (xp_long *)in.dims.ret_array_ptr(OM_GET_ARRAY_RD);
  int nx = dims[0];
  int ny = dims[1];
  ARRfree(dims);
  out.ndim = 2;
  out.nspace = 2;
  dims = (xp_long *)out.dims.ret_array_ptr(OM_GET_ARRAY_WR);
  if (dims == 0)
    return CCP3::return_info(DLV_ERROR, "Mapping to 2D",
			     "Unable to set dimensions");
  dims[0] = nx;
  dims[1] = ny;
  ARRfree(dims);
  float (*coords)[3] =
    (float (*)[3])in.coordinates.values.ret_array_ptr(OM_GET_ARRAY_RD);
  if (coords == 0)
    return CCP3::return_info(DLV_ERROR, "Mapping to 2D",
			     "Unable to read coordinates");
  // Now sort out the new coordinates.
  float xstep[3];
  float ystep[3];
  for (int i = 0; i < 3; i++) {
    xstep[i] = coords[1][i] - coords[0][i];
    ystep[i] = coords[nx][i] - coords[0][i];
  }
  float xlen = std::sqrt(xstep[0] * xstep[0]
			 + xstep[1] * xstep[1] + xstep[2] * xstep[2]);
  float ylen = std::sqrt(ystep[0] * ystep[0]
			 + ystep[1] * ystep[1] + ystep[2] * ystep[2]);
  float cangle = ((xstep[0] * ystep[0] + xstep[1] * ystep[1]
		   + xstep[2] * ystep[2]) / (xlen * ylen));
  ARRfree(coords);
  xstep[0] = xlen;
  xstep[1] = 0.0;
  float sangle = std::sqrt(1.0 - cangle * cangle);
  ystep[0] = cangle * ylen;
  ystep[1] = sangle * ylen;
  float (*pos)[2] =
    (float (*)[2]) out.coordinates.values.ret_array_ptr(OM_GET_ARRAY_WR);
  if (pos == 0)
    return CCP3::return_info(DLV_ERROR, "Mapping to 2D",
			     "Unable to allocate coordinates");
  int k = 0;
  for (int j = 0; j < ny; j++) {
    float shift[2];
    shift[0] = ((float) j) * ystep[0];
    shift[1] = ((float) j) * ystep[1];
    for (int i = 0; i < nx; i++) {
      pos[k][0] = shift[0] + ((float) i) * xstep[0];
      pos[k][1] = shift[1] + ((float) i) * xstep[1];
      k++;
    }
  }
  ARRfree(pos);
  CCP3::show_as_idle();
  return OM_STAT_SUCCESS;
}

int CCP3_core_Render_copy_slice::agcopy(OMevent_mask event_mask, int seq_num)
{
  // in (Mesh_Struct+Node_Data read req notify)
  CCP3::show_as_busy();
  int undef = 0;
  if (!in.nnodes.valid_obj())
    undef = 1;
  else if ((int) in.nnodes == 0)
    undef = 1;
  if (undef)
    return OM_STAT_SUCCESS;
  // Todo - is the order of these correct in all cases?
  /*
  int na;
  int nb;
  if ((int) axis == 0) {
    na = (int) context.nb;
    nb = (int) context.nc;
  } else if ((int) axis == 1) {
    na = (int) context.na;
    nb = (int) context.nc;
  } else {
    na = (int) context.na;
    nb = (int) context.nb;
  }
  */
  int na = 1;
  int nb = 1;
  xp_long *dims = (xp_long *)in.dims.ret_array_ptr(OM_GET_ARRAY_RD);
  int nx = dims[0];
  int ny = dims[1];
  ARRfree(dims);
  out.ndim = 2;
  out.nspace = 2;
  dims = (xp_long *)out.dims.ret_array_ptr(OM_GET_ARRAY_WR);
  if (dims == 0)
    return CCP3::return_info(DLV_ERROR, "Slice to 2D",
			     "Unable to set dimensions");
  dims[0] = nx * na;
  dims[1] = ny * nb;
  ARRfree(dims);
  float (*coords)[3] =
    (float (*)[3])in.coordinates.values.ret_array_ptr(OM_GET_ARRAY_RD);
  if (coords == 0)
    return CCP3::return_info(DLV_ERROR, "Slice to 2D",
			     "Unable to read coordinates");
  // Now sort out the new coordinates - we are making some assumptions.
  float xstep[3];
  float ystep[3];
  for (int i = 0; i < 3; i++) {
    xstep[i] = coords[1][i] - coords[0][i];
    ystep[i] = coords[nx][i] - coords[0][i];
  }
  float xlen = std::sqrt(xstep[0] * xstep[0]
			 + xstep[1] * xstep[1] + xstep[2] * xstep[2]);
  float ylen = std::sqrt(ystep[0] * ystep[0]
			 + ystep[1] * ystep[1] + ystep[2] * ystep[2]);
  float cangle = ((xstep[0] * ystep[0] + xstep[1] * ystep[1]
		   + xstep[2] * ystep[2]) / (xlen * ylen));
  ARRfree(coords);
  xstep[0] = xlen;
  xstep[1] = 0.0;
  float sangle = std::sqrt(1.0 - cangle * cangle);
  ystep[0] = cangle * ylen;
  ystep[1] = sangle * ylen;
  float (*pos)[2] =
    (float (*)[2])out.coordinates.values.ret_array_ptr(OM_GET_ARRAY_WR);
 if (pos == 0)
    return CCP3::return_info(DLV_ERROR, "Slice to 2D",
			     "Unable to allocate new coordinates");
  int k = 0;
  for (int j = 0; j < ny * nb; j++) {
    float shift[2];
    shift[0] = ((float) j) * ystep[0];
    shift[1] = ((float) j) * ystep[1];
    for (int i = 0; i < nx * na; i++) {
      pos[k][0] = shift[0] + ((float) i) * xstep[0];
      pos[k][1] = shift[1] + ((float) i) * xstep[1];
      k++;
    }
  }
  ARRfree(pos);
  // Now replicate the grid.
  float *iptr =
    (float *) in.node_data[(int)comp].values.ret_array_ptr(OM_GET_ARRAY_RD);
  if (iptr == 0)
    return CCP3::return_info(DLV_ERROR, "Slice to 2D",
			     "Unable to get data");
  out.nnode_data = 1;
  out.node_data[0].veclen = 1;
  float *optr =
    (float *) out.node_data[0].values.ret_typed_array_ptr(OM_GET_ARRAY_WR,
							  OM_TYPE_FLOAT);
  if (optr == 0) {
    ARRfree(iptr);
    return CCP3::return_info(DLV_ERROR, "Slice to 2D",
			     "Unable to allocate new data");
  }
  k = 0;
  for (int j = 0; j < ny; j++) {
    for (int i = 0; i < nx; i++) {
      for (int jb = 0; jb < nb; jb++) {
        for (int ib = 0; ib < na; ib++) {
          optr[(na * nx) * (ny * jb + j) + ib * nx + i] = iptr[k];
        }
      }
      k++; // nx * j + i
    }
  }
  CCP3::show_as_idle();
  return OM_STAT_SUCCESS;
}

int CCP3_core_Render_copy_rect::agcopy(OMevent_mask event_mask, int seq_num)
{
  // in (Mesh_Rect+Node_Data read req notify)
  CCP3::show_as_busy();
  int undef = 0;
  if (!in.nnodes.valid_obj())
    undef = 1;
  else if ((int) in.nnodes == 0)
    undef = 1;
  if (undef)
    return OM_STAT_SUCCESS;
  // Todo - is the order of these correct in all cases?
  /*
  int na;
  int nb;
  if ((int) axis == 0) {
    na = (int) context.nb;
    nb = (int) context.nc;
  } else if ((int) axis == 1) {
    na = (int) context.na;
    nb = (int) context.nc;
  } else {
    na = (int) context.na;
    nb = (int) context.nb;
  }
  */
  int na = 1;
  int nb = 1;
  xp_long *dims = (xp_long *)in.dims.ret_array_ptr(OM_GET_ARRAY_RD);
  int nx = dims[0];
  int ny = dims[1];
  ARRfree(dims);
  out.ndim = 2;
  out.nspace = 2;
  dims = (xp_long *)out.dims.ret_array_ptr(OM_GET_ARRAY_WR);
  if (dims == 0)
    return CCP3::return_info(DLV_ERROR, "SliceR to 2D",
			     "Unable to set dimensions");
  dims[0] = nx * na;
  dims[1] = ny * nb;
  ARRfree(dims);
  float (*coords)[2] = (float (*)[2])in.points.ret_array_ptr(OM_GET_ARRAY_RD);
  if (coords == 0)
    return CCP3::return_info(DLV_ERROR, "SliceR to 2D",
			     "Unable to read coordinates");
  // Rect grid has orthogonal axes.
  float xstep[2];
  float ystep[2];
  for (int i = 0; i < 2; i++) {
    xstep[i] = coords[1][i] - coords[0][i];
    ystep[i] = coords[nx + 1][i] - coords[nx][i];
  }
  ARRfree(coords);
  float (*pos)[2] = (float (*)[2])out.points.ret_array_ptr(OM_GET_ARRAY_WR);
  if (pos == 0)
    return CCP3::return_info(DLV_ERROR, "SliceR to 2D",
			     "Unable to allocate new coordinates");
  // Extend list of points.
  int k = 0;
  for (int i = 0; i < nx * na; i++) {
    pos[k][0] = ((float) i) * xstep[0];
    pos[k][1] = ((float) i) * xstep[1];
    k++;
  }
  for (int i = 0; i < ny * nb; i++) {
    pos[k][0] = ((float) i) * ystep[0];
    pos[k][1] = ((float) i) * ystep[1];
    k++;
  }
  ARRfree(pos);
  // Now replicate the grid.
  float *iptr =
    (float *) in.node_data[(int)comp].values.ret_array_ptr(OM_GET_ARRAY_RD);
  if (iptr == 0)
    return CCP3::return_info(DLV_ERROR, "SliceR to 2D",
			     "Unable to get data");
  out.nnode_data = 1;
  out.node_data[0].veclen = 1;
  float *optr =
    (float *) out.node_data[0].values.ret_typed_array_ptr(OM_GET_ARRAY_WR,
							  OM_TYPE_FLOAT);
  if (optr == 0) {
    ARRfree(iptr);
    return CCP3::return_info(DLV_ERROR, "SliceR to 2D",
			     "Unable to allocate new data");
  }
  k = 0;
  for (int j = 0; j < ny; j++) {
    for (int i = 0; i < nx; i++) {
      for (int jb = 0; jb < nb; jb++) {
        for (int ib = 0; ib < na; ib++) {
          optr[(na * nx) * (ny * jb + j) + ib * nx + i] = iptr[k];
        }
      }
      k++; // nx * j + i
    }
  }
  CCP3::show_as_idle();
  return OM_STAT_SUCCESS;
}

int CCP3_core_Render_attach_plane::create(OMevent_mask event_mask, int seq_num)
{
  OMobj_id id;
  OMget_obj_parent(obj_id(), &id);
  OMobj_id viewid = OMfind_str_subobj(id, "DLV2Dscene", OM_OBJ_RW);
  if ((int)visible == 1)
    (void) CCP3::add_2Dview(viewid, true);
  else
    (void) CCP3::add_2Dview(viewid, false);
  return OM_STAT_SUCCESS;
}

int CCP3_core_Render_attach_plane::close(OMevent_mask event_mask, int seq_num)
{
  // visible (OMXint write)
  visible = 0;
  return OM_STAT_SUCCESS;
}

int CCP3_core_Render_update_lattice::update(OMevent_mask event_mask,
					    int seq_num)
{
  // space (OMXint read req)
  CCP3::show_as_busy();
  char message[256];
  // The object contains the data, so we don't need to pass it.
  DLVreturn_type ok = DLV::operation::update_current_lattice((bool)space,
							     message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Render.Lattice", message);
  else
    return OM_STAT_SUCCESS;  
  return OM_STAT_SUCCESS;
}

int CCP3_core_Render_update_atoms::update(OMevent_mask event_mask, int seq_num)
{
  CCP3::show_as_busy();
  char message[256];
  // The object contains the data, so we don't need to pass it.
  DLVreturn_type ok = DLV::operation::update_current_atoms(message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Render.Atoms", message);
  else
    return OM_STAT_SUCCESS;  
  return OM_STAT_SUCCESS;
}

int CCP3_core_Render_update_bonds::update(OMevent_mask event_mask, int seq_num)
{
  CCP3::show_as_busy();
  char message[256];
  // The object contains the data, so we don't need to pass it.
  DLVreturn_type ok = DLV::operation::update_current_bonds(message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Render.Bonds", message);
  else
    return OM_STAT_SUCCESS;  
}

int CCP3_core_Render_update_bonds::add_bond(OMevent_mask event_mask,
					    int seq_num)
{
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV::operation::add_current_bond(message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Render.Bond.Add", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Render_update_bonds::del_bond(OMevent_mask event_mask,
					    int seq_num)
{
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV::operation::del_current_bond(message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Render.Bond.Del", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Render_update_bonds::add_type(OMevent_mask event_mask,
					    int seq_num)
{
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV::operation::add_current_bond_type(message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Render.Bond.Add", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Render_update_bonds::del_type(OMevent_mask event_mask,
					    int seq_num)
{
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV::operation::del_current_bond_type(message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Render.Bond.Del", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Render_update_bonds::add_all(OMevent_mask event_mask,
					   int seq_num)
{
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV::operation::add_current_bond_all(message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Render.Bond.Add", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Render_update_bonds::del_all(OMevent_mask event_mask,
					   int seq_num)
{
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV::operation::del_current_bond_all(message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Render.Bond.Del", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Render_select_atom::select(OMevent_mask event_mask, int seq_num)
{
  // coords (OMXfloat_array read req notify)
  // event (OMXint read req)
  // npicked (OMXint read req notify)
  // id (OMXint read req)
  // nselects (OMXint write)
  CCP3::show_as_busy();
  char message[256];
  DLVreturn_type ok = DLV_OK;
  int nevent = (int)event;
  if (event == 0) {
    // New model connected to the uer interface - no actual event
    return OM_STAT_SUCCESS;
  }
  if ((int)npicked == 0) {
    if ((int)id == 1) {
      if (nevent == 3) {
	ok = DLV::operation::deselect_all_atoms(message, 256);
	nselects = 0;
      }
    }
  } else {
    float position[3];
    float *p = (float *) coords.ret_array_ptr(OM_GET_ARRAY_RD);
    position[0] = p[0];
    position[1] = p[1];
    position[2] = p[2];
    coords.free_array(p);
    int n = 0;
    ok = DLV::operation::select_current_atom(position, nevent, n,
					     message, 256);
    if (nevent == 3)
      nselects = n;
  }
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Render.Select_Atom", message);
  else
    return OM_STAT_SUCCESS;
}

int CCP3_core_Render_update_outline::update(OMevent_mask event_mask,
					    int seq_num)
{
  CCP3::show_as_busy();
  char message[256];
  // The object contains the data, so we don't need to pass it.
  DLVreturn_type ok = DLV::operation::update_current_outline(message, 256);
  CCP3::show_as_idle();  
  if (ok != DLV_OK)
    return CCP3::return_info(ok, "CCP3.core.Render.Outline_obj", message);
  else
    return OM_STAT_SUCCESS;  
}

int CCP3_core_Render_reciprocal_space::draw(OMevent_mask event_mask,
					    int seq_num)
{
  CCP3::show_as_busy();
  DLV::operation *op = DLV::operation::get_current();
  int ok = CCP3::render_in_view(op, true, true);
  CCP3::show_as_idle();  
  return ok;
}

int CCP3_core_Render_slice_to_2D::reduce_dim(OMevent_mask event_mask,
					     int seq_num)
{
  // in (Mesh_Struct read req notify)
  // axis (OMXint read req notify)
  // out (Mesh_Struct write)
  // maxx (OMXfloat_array write)
  // maxy (OMXfloat_array write)
  if ((int)in.ndim == 0)
    return OM_STAT_UNDEF;
  xp_long *dims = (xp_long *)in.dims.ret_array_ptr(OM_GET_ARRAY_RD);
  /*if ((int) axis == 0) {
    x = 1;
    y = 2;
  } else if ((int) axis == 1) {
    x = 0;
    y = 2;
  } else {
    x = 0;
    y = 1;
    }*/
  int nx = dims[0];
  int ny = dims[1];
  ARRfree(dims);
  out.ndim = 2;
  out.nspace = 2;
  dims = (xp_long *)out.dims.ret_array_ptr(OM_GET_ARRAY_WR);
  if (dims == 0)
    return CCP3::return_info(DLV_ERROR, "SliceS to 2D",
			     "Unable to set dimensions");
  dims[0] = nx;
  dims[1] = ny;
  ARRfree(dims);
  if ((int)in.ndim == 3)
    (void) CCP3::return_info(DLV_WARNING, "SliceS to 2D",
			     "Bad coordinate dimensions");
  float (*coords)[2] =
    (float (*)[2])in.coordinates.values.ret_array_ptr(OM_GET_ARRAY_RD);
  if (coords == 0)
    return CCP3::return_info(DLV_ERROR, "SliceS to 2D",
			     "Unable to read coordinates");
  // Now sort out the new coordinates.
  float xstep[3];
  xstep[2] = 0.0;
  float ystep[3];
  ystep[2] = 0.0;
  for (int i = 0; i < 2; i++) {
    xstep[i] = coords[1][i] - coords[0][i];
    ystep[i] = coords[nx][i] - coords[0][i];
  }
  float xlen = std::sqrt(xstep[0] * xstep[0]
			 + xstep[1] * xstep[1] + xstep[2] * xstep[2]);
  float ylen = std::sqrt(ystep[0] * ystep[0]
			 + ystep[1] * ystep[1] + ystep[2] * ystep[2]);
  float cangle = ((xstep[0] * ystep[0] + xstep[1] * ystep[1]
		   + xstep[2] * ystep[2]) / (xlen * ylen));
  ARRfree(coords);
  xstep[0] = xlen;
  xstep[1] = 0.0;
  float sangle = std::sqrt(1.0 - cangle * cangle);
  ystep[0] = cangle * ylen;
  ystep[1] = sangle * ylen;
  float *extent = (float *) maxx.ret_array_ptr(OM_GET_ARRAY_WR);
  extent[0] = ((float) (nx - 1)) * xstep[0];
  extent[1] = ((float) (nx - 1)) * xstep[1];
  ARRfree(extent);
  extent = (float *) maxy.ret_array_ptr(OM_GET_ARRAY_WR);
  extent[0] = ((float) (ny - 1)) * ystep[0];
  extent[1] = ((float) (ny - 1)) * ystep[1];
  ARRfree(extent);
  return OM_STAT_SUCCESS;
}

int CCP3_core_Render_read_file::read(OMevent_mask event_mask, int seq_num)
{
  // filename (OMXstr read)
  // text (OMXstr_array write)
  char *name = (char *)filename;
  char message[256];
  message[0] = '\0';
  if (DLV::check_filename(name, message, 256)) {
    std::ifstream input;
    if (DLV::open_file_read(input, name, message, 256)) {
      char line[512];
      int nlines = 0;
      while (!input.eof()) {
	input.getline(line, 512);
	int i = strlen(line);
	if (line[i - 1] == '\n')
	  i--;
	while (line[i - 1] == ' ' and i > 0)
	  i--;
	line[i] = '\0';
	OMset_array_size(text.obj_id(), nlines + 1);
	// OM_ARRAY_APPEND seems to fail
	OMset_str_array_val(text.obj_id(), nlines, line);
	nlines++;
      }
    }
  }
  if (strlen(message) > 0) 
    return CCP3::return_info(DLV_ERROR, "CCP3.core.Render.logfile", message);
  else
    return OM_STAT_SUCCESS;  
}

int CCP3_core_Render_k_replicate::transform(OMevent_mask event_mask,
					    int seq_num)
{
  // na (OMXint read notify)
  // nb (OMXint read notify)
  // nc (OMXint read notify)
  // sa (OMXint read)
  // sb (OMXint read)
  // sc (OMXint read)
  // n (OMXint write)
  // Xform (Xform_array write)
  int xa = (int)na;
  int xb = (int)nb;
  int xc = (int)nc;
  int ya = (int)sa;
  int yb = (int)sb;
  int yc = (int)sc;
  int a = (xa - 1) / ya + 1;
  int b = (xb - 1) / yb + 1;
  int c = (xc - 1) / yc + 1;
  int ntransforms;
  float (*transforms)[3];
  DLV::operation::generate_transforms(a, b, c, ya, yb, yc,
				      ntransforms, transforms);
  n = ntransforms;
  Xform.set_array_size(ntransforms);
  for (int i = 0; i < ntransforms; i++) {
    float *ptr = (float *) Xform[i].xform.xlate.ret_array_ptr(OM_GET_ARRAY_WR);
    if (ptr != 0) {
      ptr[0] = transforms[i][0];
      ptr[1] = transforms[i][1];
      ptr[2] = transforms[i][2];
      Xform[i].xform.xlate.free_array(ptr);
    }
    // else HELP!
  }
  delete [] transforms;
  return OM_STAT_SUCCESS;
}
