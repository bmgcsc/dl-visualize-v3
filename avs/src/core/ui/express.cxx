
#include "avs/omx.hxx"
#include "src/dlv/types.hxx"
#include "src/dlv/boost_lib.hxx"
#include "src/graphics/render_base.hxx"
#include "src/dlv/operation.hxx"
#include "src/graphics/toolkit_obj.hxx"
#include "avs/src/core/messages.hxx"
#include "avs/src/core/ui/ui.hxx"

void CCP3::show_as_busy()
{
  (void) OMstatus_check(1, NULL, NULL);
}

void CCP3::show_as_idle()
{
  (void) OMstatus_check(100, NULL, NULL);
}

int CCP3::attach_op_to_UI(DLV::operation *op, const bool kspace)
{
  DLVreturn_type ok = DLV_OK;
  if (kspace)
    ok = op->attach_kui();
  else
    ok = op->attach_rui();
  if (ok == DLV_OK)
    return OM_STAT_SUCCESS;
  else
    return return_info(ok, "CCP3::ui_attach", "UI attach failed");
}

int CCP3::attach_2D_UI()
{
  // Todo
  return OM_STAT_SUCCESS;
}

void CCP3::set_ui_view_type(const bool kspace)
{
  static OMobj_id ui = OMnull_obj;
  if (OMis_null_obj(ui)) {
    ui = OMfind_str_subobj(OMinst_obj,
	       "DLV.app_window.ModelUI.menu_display.atoms.renderUI.k_space",
			   OM_OBJ_RW);
  }
  if (kspace)
    OMset_int_val(ui, 1);
  else
    OMset_int_val(ui, 0);
}
