
#ifndef CCP3_VIEWER_CREATE
#define CCP3_VIEWER_CREATE

// Todo - not ideal placing these here, issue calling init_view otherwise
class CCP3_Viewers_Scenes_DLV2Dscene;
class CCP3_Viewers_Scenes_DLV3Dscene;

namespace CCP3 {

  extern void init_view();
  extern int add_2Dview(OMobj_id view, const bool set_current);
  //extern int create_2Dview(CCP3_Viewers_Scenes_DLV2Dscene * &view,
  //		   const bool set_current = true);
  extern int create_3Dview(CCP3_Viewers_Scenes_DLV3Dscene * &view,
			   const bool kspace, const bool set_current = true);
  extern int set_current_2Dview(const int index);
  extern int set_current_3Dview(const int index);
  extern bool select_3Dview(const DLV::operation *op);
  extern void freeze_other_3Dviews(const DLV::operation *op);
  extern void activate_3Dviews();
  //extern CCP3_Viewers_Scenes_DLV2Dscene *get_current_2Dview();
  extern CCP3_Viewers_Scenes_DLV3Dscene *
  get_current_3Dview(const bool reset, const bool kspace);
  extern int attach_op_to_current_3Dview(DLV::operation *op,
					 const bool detach = true,
					 const bool reattach = true);
  extern int update_op_in_3Dview(DLV::operation *op,
				 const bool kspace = false);
  extern int check_3Dview(DLV::operation *op, const bool kspace);
  extern int close_viewers();
  extern int reset_viewers();
  extern int delete_3Dview(const int index);
  extern int delete_3Dview(const DLV::operation *op, const bool kspace);

}

#endif // CCP3_VIEWER_CREATE
