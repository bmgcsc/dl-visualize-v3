
#include "avs/src/crystal/base_gen.hxx"
#include "avs/src/express/crystal_base.hxx"
#include "avs/src/express/crystal_scf.hxx"
#include "avs/src/express/crystal_props.hxx"
#include "avs/src/express/crystal.hxx"

int CCP3_Calcs_CRYSTAL_create_ui::create(OMevent_mask event_mask,
					 int seq_num)
{
#ifdef DLV_RELEASE
  // Load the object definitions from binary V
  OMobj_id templates = OMfind_str_subobj(OMroot_obj,
					 "Templates.CCP3.CRYSTAL", OM_OBJ_RW);
  if (OMis_null_obj(templates)) {
    ERRverror("CrystalUI", ERR_ERROR, "Failed to find template");
    return OM_STAT_ERROR;
  } else {
    if (OMopen_file(templates, "crystal_mods.vo", 0) != OM_STAT_SUCCESS) {
      ERRverror("CrystalUI", ERR_ERROR, "Failed to load modules");
      return OM_STAT_ERROR;
    } else {
      if (OMopen_file(templates, "crystal_base.vo", 0) != OM_STAT_SUCCESS) {
	ERRverror("CrystalUI", ERR_ERROR, "Failed to load base macros");
	return OM_STAT_ERROR;
      } else {
	if (OMopen_file(templates, "crystal_scf.vo", 0) != OM_STAT_SUCCESS) {
	  ERRverror("CrystalUI", ERR_ERROR, "Failed to load scf macros");
	  return OM_STAT_ERROR;
	} else {
	  if (OMopen_file(templates, "crystal_props.vo", 0) !=
	      OM_STAT_SUCCESS) {
	    ERRverror("CrystalUI", ERR_ERROR, "Failed to load properties");
	    return OM_STAT_ERROR;
	  } else {
	    if (OMopen_file(templates, "crystal_macs.vo", 0) !=
		OM_STAT_SUCCESS) {
	      ERRverror("CrystalUI", ERR_ERROR, "Failed to load macros");
	      return OM_STAT_ERROR;
	    }
	  }
	}
      }
    }
  }
#endif // DLV_RELEASE
  // Create the interface's network and attach to parent objects
  OMobj_id id = OMfind_str_subobj(OMinst_obj,
				  "DLV.calculations", OM_OBJ_RW);
  CCP3_CRYSTAL_Macros_CRYSTAL *ptr = new CCP3_CRYSTAL_Macros_CRYSTAL(id);
  if (ptr == 0) {
    ERRverror("CrystalUI", ERR_ERROR, "Failed to create interface");
    return OM_STAT_ERROR;
  } else {
    // Base.fontinfo => <-.fontAttributes
    OMobj_id obj = OMfind_str_subobj(id, "preferences", OM_OBJ_RW);
    OMset_obj_ref(ptr->preferences.obj_id(), obj, 0); 
    obj = OMfind_str_subobj(id, "active_menus", OM_OBJ_RW);
    OMset_obj_ref(ptr->active_menus.obj_id(), obj, 0); 
    obj = OMfind_str_subobj(id, "UIparent", OM_OBJ_RW);
    OMset_obj_ref(ptr->UIparent.obj_id(), obj, 0); 
    // Base.prefs.prefsUI.bool_versions => <-.<-.<-.versions
    obj = OMfind_str_subobj(id, "versions", OM_OBJ_RW);
    OMset_obj_ref(ptr->Prefs.bool_versions.obj_id(), obj, 0); 
    // This could be a problem with multiple UIs
    // Currently its kind of the wrong way round - necessarily.
    obj = OMfind_str_subobj(id, "crystal_dialog", OM_OBJ_RW);
    OMset_obj_ref(obj, ptr->dialog_visible.obj_id(), 0); 
    obj = OMfind_str_subobj(id, "model_type", OM_OBJ_RW);
    OMset_obj_ref(ptr->model_type.obj_id(), obj, 0); 
    obj = OMfind_str_subobj(id, "job_data", OM_OBJ_RW);
    OMset_obj_ref(ptr->job_data.obj_id(), obj, 0); 
    obj = OMfind_str_subobj(id, "object_data", OM_OBJ_RW);
    OMset_obj_ref(ptr->objects.obj_id(), obj, 0); 
    /* get menu colours right
    obj = OMfind_str_subobj(OMinst_obj,
	    "DLV.app_window.Menu_bar.GUIs_menu.UIcmdList.cmdList", OM_OBJ_RW);
    int xx = OMadd_obj_ref(obj, ptr->UIcmdList.obj_id(), 0); 
    */
    return OM_STAT_SUCCESS;
  }
}
