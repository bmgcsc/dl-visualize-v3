
#include "avs/src/excurv/end_gen.hxx"
#include "avs/src/excurv/expt_gen.hxx"
#include "avs/src/excurv/pot_gen.hxx"
#include "avs/src/excurv/save_gen.hxx"
#include "avs/src/excurv/plot_gen.hxx"
#include "avs/src/excurv/ref_gen.hxx"
#include "avs/src/excurv/list_gen.hxx"
#include "avs/src/excurv/kw_gen.hxx"
#include "avs/src/excurv/ek_gen.hxx"
#include "avs/src/excurv/ms_gen.hxx"
#include "avs/src/excurv/err_gen.hxx"
#include "avs/src/excurv/info_gen.hxx"
#include "avs/src/excurv/str_gen.hxx"
#include "avs/src/excurv/make_gen.hxx"
#include "avs/src/excurv/scf_gen.hxx"
#include "src/dlv/types.hxx"
#include "src/dlv/boost_lib.hxx"
#include "src/graphics/render_base.hxx"
#include "src/dlv/utils.hxx"
#include "src/dlv/constants.hxx"
#include "src/dlv/atom_prefs.hxx"
#include "src/dlv/operation.hxx"
#include "src/dlv/calculation.hxx"
#include "src/dlv/op_model.hxx"
#include "src/dlv/atom_model.hxx"
#include "src/dlv/model.hxx"
#include "src/dlv/job_setup.hxx"
#include "avs/src/core/render/model.hxx"
#include "avs/src/core/viewer/view.hxx"
#include "avs/src/core/ui/ui.hxx"
#include "avs/src/core/messages.hxx"
#include "src/excurv/calcs.hxx"

int CCP3_EXCURV_Modules_close_excurv::close(OMevent_mask event_mask,
					    int seq_num)
{
  // trigger (OMXint read notify)
  CCP3::show_as_busy();
  DLVreturn_type ok = EXCURV::calculation::close();
  CCP3::show_as_idle();
  return CCP3::return_info(ok, "CCP3.EXCURV.Close", "Problem closing EXCURV");
}

int CCP3_EXCURV_Modules_read_expt::read(OMevent_mask event_mask, int seq_num)
{
  // trigger (OMXint read req notify)
  // kweight (OMXint read req)
  // atom_type (OMXint read req)
  // edge (OMXstr read)
  // data (expt_data_type read req)
  // prog_ok (OMXint write)
  // job_data (read req)
  CCP3::show_as_busy();
  int ok  = OM_STAT_SUCCESS;
  if ((int)trigger == 0)
    ok = OM_STAT_SUCCESS;
  else {
    int xcol;
    int ycol;
    bool started;
    switch ((int)data.selected_button) {
    case 0:
      // ecabs/exbrook
      xcol = 1;
      ycol = 2;
      break;
    case 1:
      // exback/exspline
      xcol = 3;
      ycol = 2;
      break;
    default:
      xcol = (int) data.x_column;
      ycol = (int) data.y_column;
      break;
    }
    DLV::job_setup_data job((char *) job_data.hostname,
			    (char *) job_data.work_dir,
			    (char *) job_data.scratch_dir,
			    (int)job_data.nprocs, (int)job_data.nnodes,
			    (int)job_data.hours, (int)job_data.memory_gb,
			    (char *)job_data.account_id,
			    (char *) job_data.queue,
			    (bool) job_data.is_parallel);
    char message[256];
    DLV::operation *op =
      EXCURV::read_experimental_data::create((char *) data.file_name,
					     (int) atom_type, (char *) edge,
					     (int)data.sampling_frequency,
					     xcol, ycol, (int)kweight,
					     started, job, message, 256);
    if (op == 0) {
      ok = CCP3::return_info(DLV_ERROR, "CCP3.EXCURV.Read_Expt_Data", message);
      prog_ok = 0;
    } else {
      ok = CCP3::update_op_in_3Dview(op);
      if (started)
	prog_ok = 1;
      else
	prog_ok = 0;
    }
  }
  CCP3::show_as_idle();
  return ok;
}

int CCP3_EXCURV_Modules_pot_phase::calc(OMevent_mask event_mask, int seq_num)
{
  // atom_type (OMXint read req)
  // data (pot_data_type read req)
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  // Probable cancel of exit EXCURV.
  if ((int)trigger == 0)
    ok = DLV_OK;
  else {
    bool V0 = ((bool)(data.constant_V0 == 1));
    int calc_method;
    switch ((int)data.method) {
    case 0:
      calc_method = 0;
      break;
    case 1:
      calc_method = 1;
      break;
    case 2:
      calc_method = -1;
      break;
    }
    int ntypes = (int) data.no_atom_types;
    int *atoms = new int[ntypes];
    int *neighb = new int[ntypes];
    for (int i = 0; i < ntypes; i++) {
      atoms[i] = (int)data.atom_pairs[i].atom_type;
      neighb[i] = (int)data.atom_pairs[i].neighbour;
    }
    char message[256];
    DLV::operation *op = 
      EXCURV::potential_and_phase::create((int)atom_type,
					  (int)data.central_neighb,
					  (int)data.no_atom_types,
					  atoms, neighb, V0,
					  calc_method, message, 256);
    if (op == 0)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.EXCURV.Potential_and_Phase",
			     message);
    else
      ok = CCP3::update_op_in_3Dview(op);
    delete [] neighb;
    delete [] atoms;
  }
  CCP3::show_as_idle();
  return ok;
}

int CCP3_EXCURV_Modules_plot::plot(OMevent_mask event_mask, int seq_num)
{
  // x (OMXint read req)
  // y (OMXint_array read req)
  xp_long size = 0;
  y.get_array_size(&size);
  if (size == 0)
    return CCP3::return_info(DLV_WARNING, "CCP3.EXCURV.Plot",
			     "No values selected");
  int *y_vals = (int *)y.ret_array_ptr(OM_GET_ARRAY_RD);
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  char message[256];
  DLV::operation *op = EXCURV::plot::create((int) x, y_vals, size,
					    message, 256);
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.EXCURV.Plot", message);
  else
    ok = CCP3::update_op_in_3Dview(op);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_EXCURV_Modules_plot::print(OMevent_mask event_mask, int seq_num)
{
  // filename (OMXstr read req)
  // x (OMXint read req)
  // y (OMXint_array read req)
  xp_long size = 0;
  y.get_array_size(&size);
  if (size == 0)
    return CCP3::return_info(DLV_WARNING, "CCP3.EXCURV.Plot",
			     "No values selected");
  int *y_vals = (int *)y.ret_array_ptr(OM_GET_ARRAY_RD);
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  char message[256];
  DLV::operation *op = EXCURV::print::create((int) x, y_vals, size,
					     (char *)filename, message, 256);
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.EXCURV.Print", message);
  else
    ok = CCP3::update_op_in_3Dview(op);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_EXCURV_Modules_save_refine::save(OMevent_mask event_mask, int seq_num)
{
  // filename (OMXstr read req)
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  char message[256];
  DLV::operation *op = EXCURV::save_data::create((char *)filename,
						 message, 256);
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.EXCURV.Save", message);
  else
    ok = CCP3::update_op_in_3Dview(op);
  CCP3::show_as_idle();
  return OM_STAT_SUCCESS;
}

int CCP3_EXCURV_Modules_refine::update(OMevent_mask event_mask, int seq_num)
{
  // data (refinement_data read req)
  CCP3::show_as_busy();
  EXCURV::refine_data refine_info(data.number_of_shells, data.step_size,
				  data.number_of_iterations, data.central_dw,
				  data.fermi_energy,
				  (bool)(data.refine_cdw == 1),
				  (bool)(data.user_defined_ref == 1),
				  (bool)(data.refine_all_atom_num == 1),
				  (bool)(data.refine_all_radii == 1),
				  (bool)(data.refine_all_dw == 1),
				  (bool)(data.refine_efermi == 1),
				  (bool)(data.correlations == 1),
				  (bool)(data.multiple_scattering == 1),
				  (int)data.max_atoms_path,
				  (float)data.max_path_length,
				  (float) data.float_plmin,
				  (float) data.float_minang,
				  (float) data.float_minmag,
				  (int) data.int_dlmax,
				  (int) data.int_tlmax,
				  (int) data.int_numax,
				  (int) data.int_omin,
				  (int) data.int_omax,
				  (float) data.float_output,
				  (int) data.theory,
				  (char *)data.symmetry);
  int ns = (int)data.number_of_shells;
  for (int i = 0; i < ns; i++)
    refine_info.add_shell(data.shells[i].atom_type,
			  data.shells[i].number_of_atoms,
			  (bool)(data.shells[i].refine_number == 1),
			  data.shells[i].radius,
			  (bool)(data.shells[i].refine_radius == 1),
			  data.shells[i].debye_waller,
			  (bool)(data.shells[i].refine_dw == 1),
			  data.shells[i].theta, data.shells[i].phi);
  int ok = OM_STAT_SUCCESS;
  char message[256];
  DLV::operation *op = EXCURV::update_refine_params::create(refine_info, false,
							    message, 256);
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR,
			   "CCP3.EXCURV.Update_Refine_Params", message);
  else
    ok = CCP3::update_op_in_3Dview(op);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_EXCURV_Modules_refine::resume(OMevent_mask event_mask, int seq_num)
{
  // data (refinement_data read req)
  // complete (OMXint write)
  CCP3::show_as_busy();
  EXCURV::refine_data refine_info(data.number_of_shells, data.step_size,
				  data.number_of_iterations, data.central_dw,
				  data.fermi_energy,
				  (bool)(data.refine_cdw == 1),
				  (bool)(data.user_defined_ref == 1),
				  (bool)(data.refine_all_atom_num == 1),
				  (bool)(data.refine_all_radii == 1),
				  (bool)(data.refine_all_dw == 1),
				  (bool)(data.refine_efermi == 1),
				  (bool)(data.correlations == 1),
				  (bool)(data.multiple_scattering == 1),
				  (int)data.max_atoms_path,
				  (float)data.max_path_length,
				  (float) data.float_plmin,
				  (float) data.float_minang,
				  (float) data.float_minmag,
				  (int) data.int_dlmax,
				  (int) data.int_tlmax,
				  (int) data.int_numax,
				  (int) data.int_omin,
				  (int) data.int_omax,
				  (float) data.float_output,
				  (int) data.theory,
				  (char *)data.symmetry);
  char message[256];
  int ok = OM_STAT_SUCCESS;
  DLV::operation *op = EXCURV::resume_refinement::create(refine_info,
							 message, 256);
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR,
			   "CCP3.EXCURV.Resume_Refinement", message);
  else
    ok = CCP3::update_op_in_3Dview(op);
  complete = 1;
  CCP3::show_as_idle();
  return ok;
}

int CCP3_EXCURV_Modules_refine::refine(OMevent_mask event_mask, int seq_num)
{
  // data (refinement_data read req)
  // complete (OMXint write)
  CCP3::show_as_busy();
  EXCURV::refine_data refine_info(data.number_of_shells, data.step_size,
				  data.number_of_iterations, data.central_dw,
				  data.fermi_energy,
				  (bool)(data.refine_cdw == 1),
				  (bool)(data.user_defined_ref == 1),
				  (bool)(data.refine_all_atom_num == 1),
				  (bool)(data.refine_all_radii == 1),
				  (bool)(data.refine_all_dw == 1),
				  (bool)(data.refine_efermi == 1),
				  (bool)(data.correlations == 1),
				  (bool)(data.multiple_scattering == 1),
				  (int)data.max_atoms_path,
				  (float)data.max_path_length,
				  (float) data.float_plmin,
				  (float) data.float_minang,
				  (float) data.float_minmag,
				  (int) data.int_dlmax,
				  (int) data.int_tlmax,
				  (int) data.int_numax,
				  (int) data.int_omin,
				  (int) data.int_omax,
				  (float) data.float_output,
				  (int) data.theory,
				  (char *)data.symmetry);
  int ns = (int)data.number_of_shells;
  for (int i = 0; i < ns; i++)
    refine_info.add_shell(data.shells[i].atom_type,
			  data.shells[i].number_of_atoms,
			  (bool)(data.shells[i].refine_number == 1),
			  data.shells[i].radius,
			  (bool)(data.shells[i].refine_radius == 1),
			  data.shells[i].debye_waller,
			  (bool)(data.shells[i].refine_dw == 1),
			  data.shells[i].theta, data.shells[i].phi);
  int ok = OM_STAT_SUCCESS;
  char message[256];
  DLV::operation *op = EXCURV::refine_structure::create(refine_info,
							message, 256);
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.EXCURV.Refine_Structure", message);
  else
    ok = CCP3::update_op_in_3Dview(op);
  complete = 1;
  CCP3::show_as_idle();
  return ok;
}

int CCP3_EXCURV_Modules_refine::list(OMevent_mask event_mask, int seq_num)
{
  // do_list (OMXint read req notify)
  // nshells (OMXint read req notify)
  // visible (OMXint read req)
  CCP3::show_as_busy();
  static int ns = 1;
  int ok = OM_STAT_SUCCESS;
  bool done = false;
  char message[256];
  if (nshells.changed(seq_num)) {
    if (ns != (int)nshells) {
      ns = (int) nshells;
      // Panel isn't up so it was changed due to a structure import.
      if ((int)visible == 1) {
	DLV::operation *op = EXCURV::list_parameters::create(ns, message, 256);
	if (op == 0)
	  ok = CCP3::return_info(DLV_ERROR,
				 "CCP3.EXCURV.List_Parameters", message);
	done = true;
      } else
	done = true;
    }
  }
  if (!done) {
    if (do_list.changed(seq_num) && ((int) do_list > 0)) {
      done = true;
      DLV::operation *op = EXCURV::list_parameters::create(0, message, 256);
      if (op == 0)
	ok = CCP3::return_info(DLV_ERROR,
			       "CCP3.EXCURV.List_Parameters", message);
      else
	ok = CCP3::update_op_in_3Dview(op);
    } else
      done = true;
  }
  CCP3::show_as_idle();
  return ok;
}

int CCP3_EXCURV_Modules_refine::import(OMevent_mask event_mask, int seq_num)
{
  // visible (OMXint read req)
  // cluster (OMXint read req)
  // data (refinement_data read req)
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  char message[256];
  if (((int)cluster == 1) && ((int)visible == 0)) {
    // Copy of update_refine
    EXCURV::refine_data refine_info(data.number_of_shells, data.step_size,
				    data.number_of_iterations, data.central_dw,
				    data.fermi_energy,
				    (bool)(data.refine_cdw == 1),
				    (bool)(data.user_defined_ref == 1),
				    (bool)(data.refine_all_atom_num == 1),
				    (bool)(data.refine_all_radii == 1),
				    (bool)(data.refine_all_dw == 1),
				    (bool)(data.refine_efermi == 1),
				    (bool)(data.correlations == 1),
				    false, 1, 1.0, 1.0, 1.0, 1.0, 1, 1, 1,
				    1, 1, 1.0, 1, "");
    int ns = (int)data.number_of_shells;
    for (int i = 0; i < ns; i++)
      refine_info.add_shell(data.shells[i].atom_type,
			    data.shells[i].number_of_atoms,
			    (bool)(data.shells[i].refine_number == 1),
			    data.shells[i].radius,
			    (bool)(data.shells[i].refine_radius == 1),
			    data.shells[i].debye_waller,
			    (bool)(data.shells[i].refine_dw == 1),
			    data.shells[i].theta, data.shells[i].phi);
    DLV::operation *op = EXCURV::update_refine_params::create(refine_info,
							      true, message,
							      256);
    if (op == 0)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.EXCURV.Import", message);
    else
      ok = CCP3::update_op_in_3Dview(op);
  }
  CCP3::show_as_idle();
  return ok;
}

int CCP3_EXCURV_Modules_set_params::update(OMevent_mask event_mask,
					   int seq_num)
{
  // params (OMXstr read req notify)
  // data (refinement_data write)
  CCP3::show_as_busy();
  char buff[32768], text[128];
  int bufptr = 0;
  int nshells = 0;
  // Todo - improve buff[bufptr] == '\0' handling.
  strcpy(buff, (char *)params);
  // Fermi E or ERROR flag
  sscanf(&buff[bufptr], "%s", text);
  if (strcmp(text, "ERROR") == 0) {
    // We shouldn't really be here.
    // Go to next line
    while (buff[bufptr] != '\n' && buff[bufptr] != '\0')
      bufptr++;
    if (buff[bufptr] == '\n')
      bufptr++;
    // Next line is error message
    int i = 0;
    while (buff[bufptr] != '\n' && buff[bufptr] != '\0') {
      text[i] = buff[bufptr];
      i++;
      bufptr++;
    }
    text[i] = '\0';
    CCP3::show_as_idle();
    return CCP3::return_info(DLV_ERROR, "CCP3.EXCURV.Update_Params",
			     "Error reading data");
    //return OM_STAT_SUCCESS;
  } // else
  float efermi;
  sscanf(text, "%f", &efermi);
  data.fermi_energy = efermi;
  while (buff[bufptr] != '\n' && buff[bufptr] != '\0')
    bufptr++;
  if (buff[bufptr] == '\n')
    bufptr++;
  // No of shells - don't set it. Todo problem if reading previous calc?
  sscanf(&buff[bufptr], "%d", &nshells);
  //data.number_of_shells = nshells;
  while (buff[bufptr] != '\n' && buff[bufptr] != '\0')
    bufptr++;
  if (buff[bufptr] == '\n')
    bufptr++;
  // Central atom data, only want A (Debye-Waller)
  while (buff[bufptr] != '\n' && buff[bufptr] != '\0')
    bufptr++;
  if (buff[bufptr] == '\n')
    bufptr++;
  while (buff[bufptr] != '\n' && buff[bufptr] != '\0')
    bufptr++;
  if (buff[bufptr] == '\n')
    bufptr++;
  float value;
  sscanf(&buff[bufptr], "%f", &value);
  data.central_dw = value;
  while (buff[bufptr] != '\n' && buff[bufptr] != '\0')
    bufptr++;
  if (buff[bufptr] == '\n')
    bufptr++;
  while (buff[bufptr] != '\n' && buff[bufptr] != '\0')
    bufptr++;
  if (buff[bufptr] == '\n')
    bufptr++;
  // Loops for N, R, A, B(skip) of each shell.
  float natoms;
  for (int i = 0; i < nshells; i++) {
    sscanf(&buff[bufptr], "%f", &natoms);
    data.shells[i].number_of_atoms = natoms;
    while (buff[bufptr] != '\n' && buff[bufptr] != '\0')
      bufptr++;
    if (buff[bufptr] == '\n')
      bufptr++;
    sscanf(&buff[bufptr], "%f", &value);
    data.shells[i].radius = value;
    while (buff[bufptr] != '\n' && buff[bufptr] != '\0')
      bufptr++;
    if (buff[bufptr] == '\n')
      bufptr++;
    sscanf(&buff[bufptr], "%f", &value);
    data.shells[i].debye_waller = value;
    while (buff[bufptr] != '\n' && buff[bufptr] != '\0')
      bufptr++;
    if (buff[bufptr] == '\n')
      bufptr++;
    while (buff[bufptr] != '\n' && buff[bufptr] != '\0')
      bufptr++;
    if (buff[bufptr] == '\n')
      bufptr++;
  }
  sscanf(&buff[bufptr], "%s", text);
  data.fit_factor = text;
  while (buff[bufptr] != '\n' && buff[bufptr] != '\0')
    bufptr++;
  if (buff[bufptr] == '\n')
    bufptr++;
  sscanf(&buff[bufptr], "%s", text);
  data.R_factor = text;
  CCP3::show_as_idle();
  return OM_STAT_SUCCESS;
}

int CCP3_EXCURV_Modules_set_kweight::update(OMevent_mask event_mask,
					    int seq_num)
{
  // kweight (OMXint read req notify)
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  char message[256];
  DLV::operation *op = EXCURV::set_kweight::create((int)kweight, message, 256);
  if (op == 0) {
    if (strlen(message) > 0)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.EXCURV.K_Weight", message);
  } else
    ok = CCP3::update_op_in_3Dview(op);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_EXCURV_Modules_set_minmax::update(OMevent_mask event_mask,
					   int seq_num)
{
  // emin (OMXfloat read req notify)
  // emax (OMXfloat read req notify)
  // kmin (OMXfloat read req notify)
  // kmax (OMXfloat read req notify)
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  DLV::operation *op = 0;
  char message[256];
  if (emin.changed(seq_num))
    op = EXCURV::set_emin::create((float) emin, message, 256);
  else if (emax.changed(seq_num))
    op = EXCURV::set_emax::create((float) emax, message, 256);
  else if (kmin.changed(seq_num))
    op = EXCURV::set_kmin::create((float) kmin, message, 256);
  else
    op = EXCURV::set_kmax::create((float) kmax, message, 256);
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.EXCURV.MinMax", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_EXCURV_Modules_set_multiple_scat::set(OMevent_mask event_mask,
					       int seq_num)
{
  // ms (OMXint read req notify)
  // symmetry (OMXstr read req)
  // model_type (OMXint read req)
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  char message[256];
  (void) EXCURV::multiple_scattering::create((int) ms, message, 256);
  if (strlen(message) > 0)
    ok = CCP3::return_info(DLV_ERROR,
			   "CCP3.EXCURV.Multiple_Scattering", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_EXCURV_Modules_excurv_errors::update(OMevent_mask event_mask,
					      int seq_num)
{
  // params (OMXstr read req notify)
  // do_potential (OMXint read req)
  // do_expt (OMXint read req)
  // potential_ok (OMXint write)
  // expt_ok (OMXint write)
  CCP3::show_as_busy();
  char buff[32768], text[128];
  int bufptr = 0;
  // Todo - improve buff[bufptr] == '\0' handling.
  strcpy(buff, (char *)params);
  if (strlen(buff) == 0) {
    //fprintf(stderr, "Clear\n");
    if (do_potential.changed(seq_num)) {
      //fprintf(stderr, "Set pot ok\n");
      potential_ok = 1;
    }
    if (do_expt.changed(seq_num)) {
      //fprintf(stderr, "Set exp ok\n");
      expt_ok = 1;
    }
  } else {
    // ERROR flag
    sscanf(&buff[bufptr], "%s", text);
    if (strcmp(text, "ERROR") == 0) {
      //fprintf(stderr, "ERROR\n");
      // Go to next line
      while (buff[bufptr] != '\n' && buff[bufptr] != '\0')
	bufptr++;
      if (buff[bufptr] == '\n')
	bufptr++;
      // Next line is error message
      int i = 0;
      while (buff[bufptr] != '\n' && buff[bufptr] != '\0') {
	text[i] = buff[bufptr];
	i++;
	bufptr++;
      }
      text[i] = '\0';
      (void) CCP3::return_info(DLV_ERROR, "CCP3.EXCURV.Error_Handler", text);
      //fprintf(stderr, "Got error - %s\n", text);
      // Async comms may mean do_pot changes on a cleared error.
      if (do_potential.changed(seq_num)) {
	//fprintf(stderr, "pot error\n");
	potential_ok = 0;
      }
      if (do_expt.changed(seq_num)) {
	//fprintf(stderr, "exp error\n");
	expt_ok = 0;
      }
    } else
      (void) CCP3::return_info(DLV_ERROR, "CCP3.EXCURV.Error_Handling",
			       "BUG - no error occurred");
  }
  CCP3::show_as_idle();
  return OM_STAT_SUCCESS;
}

int CCP3_EXCURV_Modules_create_model::create(OMevent_mask event_mask,
					     int seq_num)
{
  // atom (OMXint read req notify)
  // model_build (OMXint read req)
  int ok = OM_STAT_SUCCESS;
  if ((int)model_build == 0) {
    CCP3::show_as_busy();
    char message[256];
    DLV::operation *op = EXCURV::model::create((int) atom, message, 256);
    if (op == 0)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.EXCURV.Create_Model", message);
    else
      ok = CCP3::render_in_view(op, false);
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_EXCURV_Modules_excurv_cluster::gen_info(OMevent_mask event_mask,
						 int seq_num)
{
  // trigger (OMXint read req notify)
  // visible (OMXint read req)
  // atom_type (OMXint write)
  // pot_data (pot_data_type write)
  // ref_data (refinement_data write)
  if ((int)visible == 1) {
    if ((int)trigger == 0) {
      //ref_data.max_shells = 10;
      ref_data.number_of_shells = 1;
    } else {
      CCP3::show_as_busy();
      DLV::model *m = DLV::operation::get_current_model();
      int natoms = m->get_number_of_primitive_atoms();
      int *atom_types = new int[natoms];
      m->get_primitive_atom_types(atom_types, natoms);
      DLV::coord_type (*coords)[3] = new DLV::coord_type[natoms][3];
      m->get_primitive_atom_cart_coords(coords, natoms);
      // Todo - make it atom nearest centre of cluster?
      // Find atom closest to origin.
      double *d = new double[natoms];
      double min_d = 1e10;
      int centre = 0;
      for (int i = 0; i < natoms; i++) {
        d[i] = (coords[i][0] * coords[i][0] + coords[i][1] * coords[i][1]
                + coords[i][2] * coords[i][2]);
	if (d[i] < min_d) {
	  min_d = d[i];
	  centre = i;
	}
      }
      atom_type = atom_types[centre];
      // Shift coords.
      for (int i = 0; i < natoms; i++) {
        coords[i][0] -= coords[centre][0];
        coords[i][1] -= coords[centre][1];
        coords[i][2] -= coords[centre][2];
      }
      // Set atom types for potential
      int n = m->get_number_of_asym_atoms();
      int *asym_types = new int[n];
      m->get_asym_atom_types(asym_types, n);
      pot_data.no_atom_types = n;
      for (int i = 0; i < n; i++) {
        pot_data.atom_pairs[i].atom_type = asym_types[i];
        pot_data.atom_pairs[i].atom_symbol =
	  DLV::atomic_data->get_symbol(asym_types[i]).c_str();
      }
      // Get and set neighbours.
      //DLV::coord_type (*asym)[3] = new DLV::coord_type[n][3];
      //m->get_asym_atom_cart_coords(asym, n);
      int *indices = new int[natoms];
      m->get_primitive_asym_indices(indices, natoms);
      double diff[3], dist;
      int k = 0;
      for (int i = 0; i < n; i++) {
        min_d = 1e10;
        // find atom of this index
        for (int j = 0; j < natoms; j++) {
	  if (indices[j] == i) {
	    k = j;
	    break;
	  }
        }
        // Find distances to other atoms
        int keep = 0;
        for (int j = 0; j < natoms; j++) {
          if (j != k) {
            diff[0] = (coords[j][0] - coords[k][0]);
            diff[1] = (coords[j][1] - coords[k][1]);
            diff[2] = (coords[j][2] - coords[k][2]);
            dist = (diff[0] * diff[0] + diff[1] * diff[1] + diff[2] * diff[2]);
            if (dist < min_d) {
              min_d = dist;
              keep = j;
            }
          }
        }
        pot_data.atom_pairs[i].neighbour = indices[keep] + 1;
        if (indices[centre] == i)
          pot_data.central_neighb = indices[i] + 1;
      }
      // Get radii and types.
      // Find number of shells
      const double tol = 0.01;
      double *dlist = new double[natoms];
      int *dtype = new int[natoms];
      int *dcount = new int[natoms];
      int nshells = 0;
      for (int i = 0; i < natoms; i++) {
        if (d[i] > tol) {
	  int j;
          for (j = 0; j < nshells; j++) {
            if (dtype[j] == indices[i]) {
              if ((d[i] < dlist[j] + tol) and (d[i] > dlist[j] - tol))
                break;
            }
          }
          if (j == nshells) {
            dlist[nshells] = d[i];
            dtype[nshells] = indices[i];
            dcount[nshells] = 1;
            nshells++;
          } else
            dcount[j]++;
        }
      }
      ref_data.max_shells = nshells;
      ref_data.number_of_shells = nshells;
      bool *done = new bool[nshells];
      for (int i = 0; i < nshells; i++)
        done[i] = false;
      double theta, phi;
      for (int i = 0; i < nshells; i++) {
        k = 0;
        min_d = 1e10;
        for (int j = 0; j < nshells; j++) {
          if (!done[j]) {
            if (dlist[j] < min_d) {
              min_d = dlist[j];
              k = j;
            }
          }
        }
        done[k] = true;
        ref_data.shells[i].atom_type = dtype[k];
        ref_data.shells[i].number_of_atoms = (float)dcount[k];
        ref_data.shells[i].radius = std::sqrt(dlist[k]);
        // Find theta/phi for atom type k
	int j;
        for (j = 0; j < natoms; j++) {
          if (indices[j] == dtype[k])
            break;
        }
        if (j == natoms) {
          ERRverror("EXCURV cluster", ERR_WARNING,
                    "Couldn't find theta/phi for atom");
          theta = 90.0;
          phi = 0.0;
        } else {
          double ang_tol = 0.0001;
          double xyl = (coords[j][0] * coords[j][0]
			+ coords[j][1] * coords[j][1]);
          double len = xyl + coords[j][2] * coords[j][2];
          xyl = sqrt(xyl);
          if (xyl < ang_tol) {
            phi = 0.0;
            if (coords[k][2] > ang_tol)
              theta = 0.0;
            else
              theta = 180.0;
          } else {
            if (coords[k][1] < -ang_tol)
              phi = 360.0 - std::acos(coords[j][0] / xyl) * 180.0 / DLV::pi;
            else
              phi = std::acos(coords[j][0] / xyl) * 180.0 / DLV::pi;
            theta = std::acos(coords[j][2] / sqrt(len)) * 180.0 / DLV::pi;
          }
        }
        ref_data.shells[i].theta = theta;
        ref_data.shells[i].phi = phi;
      }
      delete [] done;
      DLV::string base;
      int size;
      DLV::string suffix;
      m->get_point_group(base, size, suffix);
      char ptgp[64];
      if (size > 1 or (size == 1 and base == "C"))
	snprintf(ptgp, 64, "%s%1d", base.c_str(), size);
      else
	strcpy(ptgp, base.c_str());
      if (suffix.length() > 0)
	strcat(ptgp, suffix.c_str());
      ref_data.symmetry = ptgp;
      delete [] dcount;
      delete [] dtype;
      delete [] dlist;
      delete [] indices;
      delete [] asym_types;
      delete [] d;
      delete [] coords;
      delete [] atom_types;
      CCP3::show_as_idle();
    }
  }
  return OM_STAT_SUCCESS;
}

int CCP3_EXCURV_Modules_excurv_model::update(OMevent_mask event_mask,
					     int seq_num)
{
  // convert (OMXint read req notify)
  // visible (OMXint read req)
  // potentials (OMXint read req)
  // atom (OMXint read req)
  // atom_types (OMXint_array read)
  // data (refinement_data read req notify)
  int ok = OM_STAT_SUCCESS;
  if ((int) potentials == 1 and (int)visible == 1) {
    CCP3::show_as_busy();
    char message[256];
    int nshells = (int)data.number_of_shells;
    int *atn = new int[nshells];
    float *natoms = new float[nshells];
    float *radii = new float[nshells];
    int *shell_atn = (int *) atom_types.ret_array_ptr(OM_GET_ARRAY_RD);
    for (int i = 0; i < nshells; i++) {
      atn[i] = shell_atn[(int)data.shells[i].atom_type];
      natoms[i] = (float)data.shells[i].number_of_atoms;
      radii[i] = (float)data.shells[i].radius;
    }
    atom_types.free_array(shell_atn);
    if (convert.changed(seq_num)) {
      DLV::operation *op = EXCURV::convert_model::create((int) atom,
							 atn, natoms,
							 radii, nshells,
							 message, 256);
      if (op == 0)
	ok = CCP3::return_info(DLV_ERROR, "CCP3.EXCURV.Convert_Model",
			       message);
      else
	ok = CCP3::render_in_view(op, false);
    } else {
      bool changed = true;
      DLV::operation *op = EXCURV::update_model::update(atn,
							natoms, radii, nshells,
							changed, message, 256);
      if (op == 0) {
	if (changed)
	  ok = CCP3::return_info(DLV_ERROR, "CCP3.EXCURV.Update_Model",
				 message);
      } else {
	if (changed)
	  ok = CCP3::render_in_view(op, false);
	// else update_op?
      }
    }
    delete [] radii;
    delete [] natoms;
    delete [] atn;
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_EXCURV_Modules_kkr::read(OMevent_mask event_mask, int seq_num)
{
  // filename (OMXstr read)
  // run (OMXint read notify)
  // atoms (OMXstr_array write)

  /***********************/
  /* Function's Body     */
  /***********************/
  ERRverror("",ERR_NO_HEADER | ERR_PRINT,"I'm in method: CCP3_EXCURV_Modules_kkr::read\n");

  // return 1 for success
  return(1);
}
