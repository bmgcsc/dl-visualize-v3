
#include "avs/src/gulp/str_gen.hxx"
#include "avs/src/gulp/extr_gen.hxx"
#include "avs/src/gulp/save_gen.hxx"
#include "avs/src/gulp/disp_gen.hxx"
#include "avs/src/gulp/dos_gen.hxx"
#include "avs/src/gulp/vec_gen.hxx"
#include "avs/src/gulp/traj_gen.hxx"
#include "avs/src/gulp/run_gen.hxx"
#include "avs/src/gulp/path_gen.hxx"
#include "src/dlv/types.hxx"
#include "src/dlv/boost_lib.hxx"
#include "src/graphics/render_base.hxx"
#include "src/dlv/operation.hxx"
#include "src/dlv/calculation.hxx"
#include "src/dlv/op_model.hxx"
#include "src/dlv/op_admin.hxx"
#include "src/dlv/data_objs.hxx"
#include "src/dlv/job_setup.hxx"
#include "avs/src/core/render/model.hxx"
#include "avs/src/core/viewer/view.hxx"
#include "avs/src/core/messages.hxx"
#include "avs/src/core/ui/ui.hxx"
#include "src/gulp/calcs.hxx"
#include "src/gulp/struct.hxx"
#include "src/gulp/extract.hxx"
#include "src/gulp/save.hxx"
#include "src/gulp/load.hxx"
#include "src/gulp/execute.hxx"

int CCP3_GULP_Modules_load_structure::load(OMevent_mask event_mask,
					   int seq_num)
{
  // data (StructureData read req)
  char *data_file = (char *) data.file;
  char *name = (char *) data.name;
  char message[256];
  DLV::operation *op = 0;

  CCP3::show_as_busy();
  OMobj_id id = OMfind_str_subobj(OMinst_obj,
				  "DLV.Display_Objs.default_data.bond_all",
				  OM_OBJ_RD);
  int set_bonds = 0;
  OMget_int_val(id, &set_bonds);
  op = GULP::load_structure::create(name, data_file, (set_bonds == 1),
				    message, 256);
  int ok;
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR,
			   "CCP3.GULP.Model.load", message);
  else
    ok = CCP3::render_in_view(op, ((bool)(data.new_view == 1)));
  CCP3::show_as_idle();
  return ok;
}

int CCP3_GULP_Modules_extract_structure::load(OMevent_mask event_mask,
					      int seq_num)
{
  // data (StructureData read req)
  char *data_file = (char *) data.file;
  char *name = (char *) data.name;
  char message[256];
  DLV::operation *op = 0;

  OMobj_id id = OMfind_str_subobj(OMinst_obj,
				  "DLV.Display_Objs.default_data.bond_all",
				  OM_OBJ_RD);
  int set_bonds = 0;
  OMget_int_val(id, &set_bonds);
  op = GULP::extract_structure::create(name, data_file, (set_bonds == 1),
				       message, 256);
  int ok;
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.GULP.Model.extract", message);
  else {
    if (strlen(message) > 0)
      (void) CCP3::return_info(DLV_WARNING,
			       "CCP3.GULP.Model.extract", message);
    ok = CCP3::render_in_view(op, ((bool)(data.new_view == 1)));
  }
  CCP3::show_as_idle();
  return ok;
}

int CCP3_GULP_Modules_save_structure::save(OMevent_mask event_mask,
					   int seq_num)
{
  // filename (OMXstr read req)
  char *data_file = (char *) filename;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  DLV::operation *op = 0;

  CCP3::show_as_busy();
  op = GULP::save_structure::create(data_file, message, 256);
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.GULP.Model.write", message);
  else
    ok = CCP3::update_op_in_3Dview(op);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_GULP_Modules_load_bands::load(OMevent_mask event_mask, int seq_num)
{
  // filename (OMXstr read req)
  char *data_file = (char *) filename;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  DLV::operation *op = 0;

  CCP3::show_as_busy();
  op = GULP::load_phonon_bands::create(data_file, message, 256);
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.GULP.Load.phonon_bands", message);
  else
    ok = CCP3::update_op_in_3Dview(op);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_GULP_Modules_load_DOS::load(OMevent_mask event_mask, int seq_num)
{
  // filename (OMXstr read req)
  char *data_file = (char *) filename;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  DLV::operation *op = 0;

  CCP3::show_as_busy();
  op = GULP::load_phonon_DOS::create(data_file, message, 256);
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.GULP.Load.phonon_DOS", message);
  else
    ok = CCP3::update_op_in_3Dview(op);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_GULP_Modules_load_vectors::load(OMevent_mask event_mask, int seq_num)
{
  // filename (OMXstr read req)
  char *data_file = (char *) filename;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  DLV::operation *op = 0;

  CCP3::show_as_busy();
  op = GULP::load_phonon_vectors::create(data_file, message, 256);
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR,
			   "CCP3.GULP.Load.phonon_vectors", message);
  else
    ok = CCP3::update_op_in_3Dview(op);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_GULP_Modules_load_trajectory::load(OMevent_mask event_mask,
					    int seq_num)
{
  // filename (OMXstr read req)
  char *data_file = (char *) filename;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  DLV::operation *op = 0;

  CCP3::show_as_busy();
  op = GULP::load_MD_trajectory::create(data_file, message, 256);
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.GULP.Load.trajectory", message);
  else
    ok = CCP3::render_in_view(op, false);
  //ok = CCP3::update_op_in_3Dview(op);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_GULP_Modules_Run_GULP::execute(OMevent_mask event_mask, int seq_num)
{
  // data (params read req)
  // job_data (params read req)
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  char message[256];
  DLV::operation *op = 0;
  message[0] = '\0';
  GULP::control_data control(((bool) (data.general.calc_props == 1)),
			     ((bool) (data.general.gibbs_fe == 1)),
			     (int) data.general.calc_phonons,
			     (int) data.general.shrink1,
			     (int) data.general.shrink2,
			     (int) data.general.shrink3,
			     ((bool) (data.general.set_pressure == 1)),
			     (float) data.general.pressure,
			     ((bool) (data.general.set_temperature == 1)),
			     (float) data.general.temperature,
			     ((bool) (data.general.calc_eigenvectors == 1)),
			     (int) data.general.npoints,
			     (char *) data.general.bandpath);
  GULP::optimize_data opt((int) data.optimise.constraint);
  GULP::moldyn_data md((int) data.md.ensemble, (float) data.md.timestep,
		       (int) data.md.equilib, (int) data.md.prod,
		       (int) data.md.sample, (float) data.md.thermostat,
		       (float) data.md.barostat, (int) data.md.traj_step);
  DLV::job_setup_data job((char *) job_data.hostname,
			  (char *) job_data.work_dir,
			  (char *) job_data.scratch_dir, (int)job_data.nprocs,
			  (int)job_data.nnodes, (int)job_data.hours,
			  (int)job_data.memory_gb, (char *)job_data.account_id,
			  (char *)job_data.queue, (bool)job_data.is_parallel);
  if (data.potentials.libname.valid_obj()) {
    GULP::potential_lib potentials(((bool) (data.potentials.use_shells == 1)),
				   (char *) data.potentials.libname);
    op = GULP::calculate::create((int) data.calc_type, control, opt,
				 md, potentials, job,
				 (bool)(data.user_calc == 1),
				 (char *)data.user_dir, message, 256);
    if (op != 0) {
      if ((int)data.calc_type == 0)
	ok = CCP3::update_op_in_3Dview(op);
    }
    if (strlen(message) > 0)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.GULP.Execute", message);
  } else
    ok = CCP3::return_info(DLV_WARNING, "CCP3.GULP.Execute",
			   "Aborting calc - no potential selected");
  CCP3::show_as_idle();
  return OM_STAT_SUCCESS;
}

int CCP3_GULP_Modules_kpath_info::info(OMevent_mask event_mask, int seq_num)
{
  // centre (OMXint write)
  // lattice (OMXint write)
  CCP3::show_as_busy();
  int c = 0;
  int l = 0;
  DLV::operation::get_current_lattice(c, l);
  centre = c;
  lattice = l;
  CCP3::show_as_idle();
  return OM_STAT_SUCCESS;
}
