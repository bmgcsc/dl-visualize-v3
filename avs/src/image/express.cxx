
#include <cstdio>
#include "avs/src/image/load_gen.hxx"
#include "src/dlv/types.hxx"
#include "src/dlv/boost_lib.hxx"
#include "src/graphics/render_base.hxx"
#include "src/dlv/operation.hxx"
#include "src/dlv/op_model.hxx"
#include "avs/src/core/ui/ui.hxx"
#include "avs/src/core/messages.hxx"
#include "avs/src/core/render/model.hxx"
#include "avs/src/core/viewer/view.hxx"
#include "src/dlv/op_admin.hxx"
#include "src/dlv/data_objs.hxx"
#include "src/dlv/data_vol.hxx"
#include "src/image/calcs.hxx"
#include "src/image/grid3d.hxx"

int CCP3_IMAGE_Modules_load_data::load(OMevent_mask event_mask, int seq_num)
{
  // filename (OMXstr read req)
  char *data_file = (char *) filename;
  int ok = OM_STAT_SUCCESS;
  char message[256];
  DLV::operation *op = 0;
  CCP3::show_as_busy();
  op = IMAGE::load_3d_data::create(data_file, message, 256);
  if (op == 0)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.Image.Density.Load", message);
  else
    ok = CCP3::render_in_view(op, false);
  CCP3::show_as_idle();
  return ok;
}
