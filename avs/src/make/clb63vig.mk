
CXX = g++
PIC = -fPIC
#CXXFLAGS = -g -ansi -pedantic -Wall -Wno-long-long -pthread -DENABLE_DLV_JOB_THREADS -DENABLE_DLV_GRAPHICS -DDLV_USES_AVS_GRAPHICS -DXP_WIDE_API -DDLV_USES_SERIALIZE -DENABLE_ROD -I/usr/X11R6/include -I/home/clb63/CODE/AVS/DLV/motif/include

CXXFLAGS = -g -ansi -pedantic -Wall -Wno-long-long -pthread -DENABLE_DLV_JOB_THREADS -DENABLE_DLV_GRAPHICS -DDLV_USES_AVS_GRAPHICS -DENABLE_ROD -DXP_WIDE_API -DDLV_USES_SERIALIZE -I/usr/X11R6/include -I/home/clb63/CODE/AVS/DLV/motif/include

# release code
#CXXFLAGS = -g -O2 -ansi -pedantic -Wall -Wno-long-long -pthread -DENABLE_DLV_JOB_THREADS -DENABLE_DLV_GRAPHICS -DDLV_USES_AVS_GRAPHICS -DXP_WIDE_API -DDLV_USES_SERIALIZE -DDLV_RELEASE -I/home/software/include
# old non-threaded
#CXXFLAGS = -g -ansi -pedantic -Wall -DENABLE_DLV_GRAPHICS -DDLV_USES_AVS_GRAPHICS -DXP_WIDE_API -DDLV_USES_SERIALIZE -I/usr/X11R6/include
SHARED = -shared

BOOSTDIR =
BOOSTLIB = 

CCTBXDIR = /home/clb63/CODE/AVS/DLV/cctbx/include
CCTBXLIB = /home/clb63/CODE/AVS/DLV/cctbx/libcctbx.a

RODDIR = /home/clb63/CODE/AVS/DLV/trunk/rod
RODLIB = /home/clb63/CODE/AVS/DLV/trunk/rod/librod.a

# Only for DLV_DL
#BABELDIR = ../../babel/include/openbabel-2.0
#BABELLIB = /home/bgs/express/3.0/babel/lib/libopenbabel.a
#else
BABELDIR =
BABELLIB =

AVSDIR = /cse/share/avs.e/7.2.1/linux_64

GUILIB =  $(AVSDIR)/lib/linux_64/fld/Xfld.o $(AVSDIR)/lib/linux_64/ag/ag_omx.o $(AVSDIR)/lib/linux_64/motif_ui/ui_core.o

#LIBS = $(AVSDIR)/lib/linux_64/fld/Xfld.o $(AVSDIR)/lib/linux_64/ag/ag_omx.o $(AVSDIR)/lib/linux_64/motif_ui/ui_core.o -lboost_serialization-gcc-mt -lboost_thread-gcc-mt
#LIBS = -L$(BOOSTLIB) -lboost_serialization-gcc43-mt -lboost_filesystem-gcc43-mt -lboost_thread-gcc43-mt
LIBS = -lboost_serialization -lboost_filesystem -lboost_thread
#LIBS = $(AVSDIR)/lib/linux_64/fld/Xfld.o $(AVSDIR)/lib/linux_64/ag/ag_omx.o $(AVSDIR)/lib/linux_64/motif_ui/ui_core.o -L$(BOOSTLIB) -lboost_serialization-gcc

INCDIRS = -I$(CCTBXDIR) -I$(RODDIR) -I$(RODDIR)/include
AVSINCS = -I$(AVSDIR)/include -I$(AVSDIR) -I$(HOME)/CODE/AVS/DLV/trunk

#INCLUDES = -I./include -I./menu -I./plot 
