
CXX = g++
PIC = -fPIC
CXXFLAGS = -g -m32 -march=i686 -ansi -pedantic -Wall -Wno-long-long -pthread -DENABLE_DLV_JOB_THREADS -DENABLE_DLV_GRAPHICS -DDLV_USES_AVS_GRAPHICS -DXP_WIDE_API -DDLV_USES_SERIALIZE -DENABLE_ROD -DDLV_EXPLICIT_TEMPLATES
# release code
#CXXFLAGS = -g -m32 -march=i686 -O2 -ansi -pedantic -Wall -Wno-long-long -pthread -DENABLE_DLV_JOB_THREADS -DENABLE_DLV_GRAPHICS -DDLV_USES_AVS_GRAPHICS -DXP_WIDE_API -DDLV_USES_SERIALIZE -DDLV_RELEASE -DENABLE_ROD -DBOOST_DISABLE_ASSERTS  -DDLV_EXPLICIT_TEMPLATES
# old non-threaded
#CXXFLAGS = -g -ansi -pedantic -Wall -DENABLE_DLV_GRAPHICS -DDLV_USES_AVS_GRAPHICS -DXP_WIDE_API -DDLV_USES_SERIALIZE -I/usr/X11R6/include
CXXTEMPLATES = $(CXXFLAGS) -fno-implicit-templates
LD = ld
LDFLAGS = -dylib -undefined dynamic_lookup
SO = dylib

BOOSTDIR = /Users/Barry/include
BOOSTLIB = /Users/Barry/lib

CCTBXDIR = ../../../cctbx/include
CCTBXLIB = /Users/Barry/lib/libcctbx.a

RODDIR = ../../../rod
RODLIB = ../../../rod/librod.a

# Only for DLV_DL
#BABELDIR = ../../babel/include/openbabel-2.0
#BABELLIB = /home/bgs/express/3.0/babel/lib/libopenbabel.a
#else
BABELDIR =
BABELLIB =

AVSDIR = /Users/Barry/avs.e

GUILIB =  $(AVSDIR)/lib/darwin/fld/Xfld.o $(AVSDIR)/lib/darwin/ag/ag_omx.o $(AVSDIR)/lib/darwin/motif_ui/ui_core.o

#LIBS = $(AVSDIR)/lib/darwin/fld/Xfld.o $(AVSDIR)/lib/darwin/ag/ag_omx.o $(AVSDIR)/lib/darwin/motif_ui/ui_core.o -lboost_serialization -lboost_filesystem -lboost_thread
LIBS = -L$(BOOSTLIB) -lboost_serialization -lboost_filesystem -lboost_thread
#LIBS = $(AVSDIR)/lib/darwin/fld/Xfld.o $(AVSDIR)/lib/darwin/ag/ag_omx.o $(AVSDIR)/lib/darwin/motif_ui/ui_core.o -L$(BOOSTLIB) -lboost_serialization-gcc

INCDIRS = -I$(CCTBXDIR) -I$(BOOSTDIR) -I$(RODDIR) -I$(RODDIR)/include -I/sw/include
AVSINCS = -I$(AVSDIR)/include -I$(AVSDIR) -I$(HOME)/express/3.0
