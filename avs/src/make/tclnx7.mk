
CXX = g++
PIC = -fPIC
CXXFLAGS = -g -ansi -pedantic -Wall -Wno-long-long -pthread -DENABLE_DLV_JOB_THREADS -DENABLE_DLV_GRAPHICS -DDLV_USES_AVS_GRAPHICS -DXP_WIDE_API -DENABLE_ROD -DDLV_USES_SERIALIZE -DDLV_EXPLICIT_TEMPLATES -DUSE_ESCDF
# release code
#CXXFLAGS = -g -O2 -ansi -pedantic -Wall -Wno-long-long -pthread -DENABLE_DLV_JOB_THREADS -DENABLE_DLV_GRAPHICS -DDLV_USES_AVS_GRAPHICS -DXP_WIDE_API -DDLV_USES_SERIALIZE -DDLV_RELEASE -DBOOST_DISABLE_ASSERTS -DDLV_EXPLICIT_TEMPLATES
CXXTEMPLATES = $(CXXFLAGS) -fno-implicit-templates
SHARED = -shared
LD = $(CXX)
LDFLAGS = $(CXXFLAGS) $(SHARED)
SO = so

BOOSTDIR =
BOOSTLIB = /usr/local/lib64

CCTBXDIR = ../../cctbx/include
CCTBXLIB = /home/bgs/lib/libcctbx.a

RODDIR = ../../../rod/src
RODLIB = ../../../rod/librod.a

ESCDFDIR = /home/bgs/escdf/dinstall/include
ESCDFLIB = /home/bgs/escdf/dinstall/lib64/libescdf.so

# Only for DLV_DL
#BABELDIR = ../../babel/include/openbabel-2.0
#BABELLIB = /home/bgs/express/3.0/babel/lib/libopenbabel.a
#else
BABELDIR =
BABELLIB =

AVSDIR = /usr/local/64avs.e

GUILIB =  $(AVSDIR)/lib/linux_64/fld/Xfld.o $(AVSDIR)/lib/linux_64/ag/ag_omx.o $(AVSDIR)/lib/linux_64/motif_ui/ui_core.o

#LIBS = $(AVSDIR)/lib/linux_64/fld/Xfld.o $(AVSDIR)/lib/linux_64/ag/ag_omx.o $(AVSDIR)/lib/linux_64/motif_ui/ui_core.o -lboost_serialization-gcc-mt -lboost_thread-gcc-mt
LIBS = -L$(BOOSTLIB) -lboost_serialization -lboost_filesystem -lboost_thread
#LIBS = $(AVSDIR)/lib/linux_64/fld/Xfld.o $(AVSDIR)/lib/linux_64/ag/ag_omx.o $(AVSDIR)/lib/linux_64/motif_ui/ui_core.o -L$(BOOSTLIB) -lboost_serialization-gcc

INCDIRS = -I$(CCTBXDIR) -I$(RODDIR) -I$(RODDIR)/include -I$(ESCDFDIR)
AVSINCS = -I$(AVSDIR)/include -I$(AVSDIR) -I$(HOME)/express/3.0
