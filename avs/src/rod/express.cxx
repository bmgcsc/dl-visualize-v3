#include "avs/src/rod/rod_load_gen.hxx"
#include "avs/src/rod/rod_create_gen.hxx"
#include "avs/src/rod/rod_createlist_gen.hxx"
#include "avs/src/rod/rod_fit_gen.hxx"
#include "avs/src/rod/rod_fitdata_gen.hxx"
#include "avs/src/rod/rod_plot_gen.hxx"
#include "avs/src/rod/rod_lists_gen.hxx"
#include "avs/src/rod/ffactors_plot_gen.hxx"
#include "avs/src/rod/rod_view_gen.hxx"
#include "avs/src/rod/rod_save_gen.hxx"
#include "avs/src/rod/rod_model_gen.hxx"
#include "avs/src/rod/rod_labels_gen.hxx"
#include "avs/src/rod/rod_params_fit_gen.hxx"
#include "src/dlv/types.hxx"
#include "src/dlv/boost_lib.hxx"
#include "src/graphics/render_base.hxx"
#include "src/dlv/utils.hxx"
#include "src/dlv/operation.hxx"
#include "src/dlv/op_model.hxx"
#include "src/dlv/atom_model.hxx"
#include "src/dlv/model.hxx"  
#include "avs/src/core/ui/ui.hxx"
#include "avs/src/core/messages.hxx"
#include "avs/src/core/render/model.hxx"
#include "avs/src/core/viewer/view.hxx"
#include "src/dlv/op_admin.hxx"
#include "src/dlv/data_objs.hxx"
#include "src/dlv/data_vol.hxx"
#include "src/dlv/data_plots.hxx"
#include "src/dlv/data_atoms.hxx"
#include "src/rod/calcs.hxx"
#include "src/rod/data.hxx"
#include "src/rod/struct.hxx"
#include "src/rod/save.hxx"
#include "src/rod/plot.hxx"
#include "src/rod/fit.hxx"

int CCP3_ROD_Modules_load_rod_files::load(OMevent_mask event_mask, int seq_num)
{
  // load_data (LoadData read req)
  int ok = 0;
  const int mlen = 256;
  char message[mlen];
  CCP3::show_as_busy();
  //Load strucutre files
  if(load_data.file_bulk[0] != '\0'||  load_data.file_fit[0] !='\0'){  
    char *data_file1 = (char *) load_data.file_bulk;
    char *data_file3 = (char *) load_data.file_fit;
    char *name = (char *) load_data.name;
    int file_type;  //0=bulk, [1=surf, 2=bulk+surf <--not used now], 3=fit, 4=bulk+fit  
    if(data_file1[0] != '\0'){
      if(data_file3[0] != '\0')
	file_type = 4;
      else
	file_type = 0;
    }
    else{ 
      if(data_file3[0] != '\0')
	file_type = 3;
    }
    DLV::operation *op = 0;
    message[0] = '\0';
    OMobj_id id = OMfind_str_subobj(OMinst_obj,
				    "DLV.Display_Objs.default_data.bond_all",
				    OM_OBJ_RD);
    int set_bonds = 0;
    OMget_int_val(id, &set_bonds);
      op = ROD::load_structure::create(name, data_file1, data_file3, file_type,
				       set_bonds, message, mlen);
    if (op == 0)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.ROD.load.Model", message);
    else if (strlen(message) > 0)
      ok = CCP3::return_info(DLV_WARNING,
			     "CCP3.ROD.load.Model", message);
    else{
      ok = CCP3::render_in_view(op, ((bool)(load_data.new_view == 1)));
      if(file_type > 2)
	load_data.got_fit_data = true;
    }
  }
  // Load Experimental Data
  if(load_data.file_exp_data[0] != '\0'){
    char *data_file = (char *) load_data.file_exp_data;
    ok = ROD::data::read_data_file(data_file, (bool *)&params.use_scale2, 
				   message, mlen); 
    if(ok == OM_STAT_SUCCESS)
      load_data.got_exp_data = true;
    if(ok == DLV_ERROR)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.ROD.LOAD_DATA", message);
    else if(ok == DLV_WARNING)
      ok = CCP3::return_info(DLV_WARNING, "CCP3.ROD.LOAD_DATA", message);
  }
  // Load Parameter File
  if(load_data.file_par[0] != '\0'){
    char *par_file = (char *) load_data.file_par;
    ok = ROD::data::read_par_file(par_file, message, mlen); 
    if(ok == DLV_ERROR)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.ROD.LOAD_PAR_FILE", message);
    else {
      load_data.got_par_data = true;
      if(ok == DLV_WARNING){
	CCP3::return_info(DLV_WARNING, "CCP3.ROD.LOAD_PAR_FILE", message);
	ok = DLV_OK;
      }
    }
  }
  CCP3::show_as_idle();
  return ok;
}

int CCP3_ROD_Modules_list_bulks::create(OMevent_mask event_mask, int seq_num)
{
  const int mlen = 256;
  char message[mlen];
  int ok = OM_STAT_SUCCESS;
  DLV::string part1 = getenv("DLV_ROOT");
  part1 += DIR_SEP_STR;
  part1 += "data";
  part1 += DIR_SEP_STR;
  part1 += "ROD";
  DLV::string filename = part1 + DIR_SEP_STR + "structures.dat";
  FILE *ifile = fopen(filename.c_str(), "r");
  if (ifile == 0) {
    snprintf(message, mlen,
	     "Unable to open structure data, file %s", filename.c_str());
    return  DLV_ERROR;
  } 
  char buff[256], name[32];
  while (!feof(ifile)) {
    fgets(buff, 256, ifile);
    sscanf(buff, "%s", name);
    if (strcmp(name, "Bulk") == 0) {
      list_size = list_size + 1;
    }
  }
  fclose (ifile);

  DLV::string *store = new DLV::string[list_size];
  // Need to get bulk_names array working properly
  // bulk_names.set_array_size(list_size);
  // DLV::string *bulk_names1 = (DLV::string *)bulk_names.ret_array_ptr(OM_GET_ARRAY_WR);
  int i = 0;
  ifile = fopen(filename.c_str(), "r");
  while (!feof(ifile)) {
    fgets(buff, 256, ifile);
    sscanf(buff, "%s", name);
    if (strcmp(name, "Bulk") == 0) {
      fgets(buff, 256, ifile);
      sscanf(buff, "%s", name);
      store[i] = name;
      //     bulk_names1[i] = name;
      i++;
    }
  }
  fclose(ifile);
  //  bulk_names.free_array(bulk_names1);
  return ok;
}

int CCP3_ROD_Modules_create_model::create_bulk(OMevent_mask event_mask, int seq_num)
{
  // create_data 
  int ok = OM_STAT_SUCCESS;
  if(trigger_bulk != -1){
    const int mlen = 256;
    char message[mlen];
    CCP3::show_as_busy();
    //get model info
    DLV::string part1 = getenv("DLV_ROOT");
    part1 += DIR_SEP_STR;
    part1 += "data";
    part1 += DIR_SEP_STR;
    part1 += "ROD";
    DLV::string filename = part1 + DIR_SEP_STR + "structures.dat";
    FILE *ifile = fopen(filename.c_str(), "r");
    if (ifile == 0) {
      snprintf(message, mlen,
	       "Unable to open structure data, file %s", filename.c_str());
      return  DLV_ERROR;
    } 
    char buff[256], name[32];
    char *label = create_data.bulk_names;
    while (!feof(ifile)) {
      fgets(buff, 256, ifile);
      sscanf(buff, "%s", name);
      if (strcmp(name, label) == 0) 
	break;
    }
    char group[10];
    fgets(group, 256, ifile);
    int model_type, lattice, centre, setting, norigins;
    fgets(buff, 256, ifile);
    sscanf(buff, "%i %i %i %i %i", &model_type, &lattice, 
	   &centre, &setting, &norigins); 
    double a, b, c, alpha, beta, gamma;
    fgets(buff, 256, ifile);
    sscanf(buff, "%lf %lf %lf %lf %lf %lf", &a, &b, &c, &alpha, &beta, &gamma); 
    if (DLV::operation::create_model((char *)create_data.bulk_names, model_type,
				     lattice, centre,
				     group, setting,
				     norigins, a, b, c, alpha, beta, gamma,
				     message, 256) == DLV_OK)
      ok = CCP3::render_in_view(DLV::operation::get_current(),
				((bool)(create_data.new_view == 1)));
    else
      ok = OM_STAT_ERROR;
    int natoms;
    fgets(buff, 256, ifile);
    sscanf(buff, "%i", &natoms);
    int *atomic_number = new int[natoms];
    double *x = new double[natoms];
    double *y = new double[natoms];
    double *z = new double[natoms];
    for (int i=0; i<natoms; i++){
      fgets(buff, 256, ifile);
      sscanf(buff, "%i %lf %lf %lf", &atomic_number[i], &x[i], &y[i], &z[i]);
    }
    bool set_charge = false;
    float charge = 0.0;
    bool set_radius = false;
    float radius = 0.0;
    DLVreturn_type v;
    for (int i = 0; i<natoms; i++){
      bool rhomb = (model_type == 3 and lattice == 4 and centre == 1);
      v = DLV::operation::create_add_atom(atomic_number[i],
					  set_charge, charge,
					  set_radius, radius,
					  x[i], y[i], z[i],
					  rhomb, message, 256);
      if (v == DLV_OK)
	ok = DLV::operation::update_current_cell(message, 256);
      else{
	ok = OM_STAT_ERROR;
	break;
      }
    }
    if (ok != OM_STAT_SUCCESS) 
      ok = CCP3::return_info(DLV_ERROR, "CCP3rod.Model.create", message);
    else{
      create_data.created_bulk = true;
      create_data.current_bulk = selected_bulk;
    }
    create_data.created_surface = false;
    CCP3::show_as_idle(); 
    fclose(ifile);
  }
  return ok;
}

int CCP3_ROD_Modules_create_model::create_surf(OMevent_mask event_mask, int seq_num)
{
  // create_data 
  int ok = OM_STAT_SUCCESS;
  if(trigger_surf != -1){
    const int mlen = 256;
    char message[mlen];
    CCP3::show_as_busy();
    //store pending here
    DLV::operation *old_pending = DLV::operation::get_pending();
    // create bulk here 
    if (!create_data.created_bulk || selected_bulk != create_data.current_bulk)
      CCP3_ROD_Modules_create_model::create_bulk(event_mask, seq_num);
    // create surface
    int ih = (int) create_data.surf_h;
    int ik = (int) create_data.surf_k;
    int il = (int) create_data.surf_l;
    if (ih != 0 or ik != 0 or il != 0) {
      bool conv = true;
      float tol = 0.001;
      DLV::string *names = 0;
      int nlabels = 0;
      DLV::string name = (char *) create_data.bulk_names;
      ok = DLV::operation::cut_surface(name.c_str(), 
				       ih, ik, il, conv, tol,
				       names, nlabels, 
				       message, mlen);  
      if (ok == DLV_OK)
	ok = CCP3::render_in_view(DLV::operation::get_current(),
				  (bool) create_data.new_view); //needed for model completion
      else
	CCP3::return_info(DLV_ERROR, "CCP3.ROD.create_surf", message);
      delete [] names;
    } 
    else
      return CCP3::return_info(DLV_WARNING, "CCP3.ROD.cut_surf",
			       "Ignoring (0 0 0) Miller indices");
    //creating salvage
    if(create_data.nlayers > 0){
      char *label = (char *) create_data.bulk_names;
      int nl = 0;
      int min_l = 0;
      ok = DLV::operation::cut_salvage(label, nl, min_l, message, mlen);
      if (ok == DLV_OK)
	ok = CCP3::render_in_view(DLV::operation::get_current(),
				  (bool) create_data.new_view);//needed for model completion
      int nlayers = create_data.nlayers;
      ok =  DLV::operation::update_salvage(nlayers, message, mlen);
    }
    if (ok == DLV_OK)
      ok = CCP3::render_in_view(DLV::operation::get_current(),
				(bool) create_data.new_view);
    else
      ok = CCP3::return_info(DLV_ERROR,
			     "CCP3.core.Model.cut_salvage", message);
    create_data.created_surface = true;
    create_data.created_bulk = false;  //if fails dont have toupdate this   
    DLV::operation::set_pending(old_pending);  //will need to protect
    CCP3::show_as_idle(); 
  }
  return ok;
}

int CCP3_ROD_Modules_create_model::accept(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  if(trigger_accept != -1){
    CCP3::show_as_busy();
    create_data.created_bulk = false;
    create_data.created_surface = false;
    if (DLV::operation::accept_pending(false, true) != DLV_OK)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Model.accept",
			     "Problem accepting new model");
    CCP3::show_as_idle();
  }
  return ok;
}

int CCP3_ROD_Modules_create_model::cancel(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  create_data.created_bulk = false;
  create_data.created_surface = false;
  if ((int)create_data.new_view != 0 or DLV::operation::last_model()) {
    ok = CCP3::delete_3Dview(DLV::operation::get_current(), false);
    DLV::operation::detach_pending();
  }
  else {
    DLV::operation::detach_pending();  
    ok = CCP3::render_in_view(DLV::operation::get_current(), false);
  }
  if (DLV::operation::delete_pending() != DLV_OK)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Model.cancel",
			   "Problem canceling new model");
  CCP3::show_as_idle();
  return ok;
}

int CCP3_ROD_Modules_run_fit::start_no_opt(OMevent_mask event_mask, int seq_num)
{
  if(DLV::operation::get_pending() ==0){
    params.fit_method = 0;
    return start(event_mask, seq_num);
  }
  else
    return 1;
}

int CCP3_ROD_Modules_run_fit::start_lm(OMevent_mask event_mask, int seq_num)
{
  params.fit_method = 1;
  return start(event_mask, seq_num);
}

int CCP3_ROD_Modules_run_fit::start_ana(OMevent_mask event_mask, int seq_num)
{
  params.fit_method = 4;
  return start(event_mask, seq_num);
}

int CCP3_ROD_Modules_run_fit::start(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  const int mlen = 256;
  char message[mlen];
  CCP3::show_as_busy();
  float *dist; 
  if(params.ndisttot !=0)
    dist = (float *)params.dist.ret_array_ptr(OM_GET_ARRAY_WR);
  float *dist_min = (float *)params.dist_min.ret_array_ptr(OM_GET_ARRAY_RD);  
  float *dist_max = (float *)params.dist_max.ret_array_ptr(OM_GET_ARRAY_RD);
  int *dist_fit = (int *)params.dist_fit.ret_array_ptr(OM_GET_ARRAY_RD);
  float *dw1;
  if(params.ndwtot !=0)
    dw1 = (float *)params.dw1.ret_array_ptr(OM_GET_ARRAY_WR);
  float *dw1_min = (float *)params.dw1_min.ret_array_ptr(OM_GET_ARRAY_RD);  
  float *dw1_max = (float *)params.dw1_max.ret_array_ptr(OM_GET_ARRAY_RD);
  int *dw1_fit = (int *)params.dw1_fit.ret_array_ptr(OM_GET_ARRAY_RD);
  float *dw2;
  if(params.ndwtot2 !=0)
    dw2 = (float *)params.dw2.ret_array_ptr(OM_GET_ARRAY_WR);
  float *dw2_min = (float *)params.dw2_min.ret_array_ptr(OM_GET_ARRAY_RD);  
  float *dw2_max = (float *)params.dw2_max.ret_array_ptr(OM_GET_ARRAY_RD);
  int *dw2_fit = (int *)params.dw2_fit.ret_array_ptr(OM_GET_ARRAY_RD);
  float *occup;
  if(params.nocctot !=0)
    occup = (float *)params.occup.ret_array_ptr(OM_GET_ARRAY_WR);
  float *occup_min = (float *)params.occup_min.ret_array_ptr(OM_GET_ARRAY_RD);  
  float *occup_max = (float *)params.occup_max.ret_array_ptr(OM_GET_ARRAY_RD);
  int *occup_fit = (int *)params.occup_fit.ret_array_ptr(OM_GET_ARRAY_RD);
  float (*results_array)[2];
  results_array = (float (*)[2]) params.fit_results.ret_array_ptr(OM_GET_ARRAY_WR);
  DLV::operation *op;
  bool attach_op = false;
  if(params.fit_method ==0 || !load_data.got_exp_data){
    int pseudo_method = 1;
    bool no_fit = 0;
    int *dist_no_fit = new int[params.ndisttot];
    int *dw1_no_fit = new int[params.ndwtot];
    int *dw2_no_fit = new int[params.ndwtot2];
    int *occup_no_fit = new int[params.nocctot];
    for (int i=0; i<params.ndisttot; i++)
      dist_no_fit[i] = false;
    for (int i=0; i<params.ndwtot; i++)
      dw1_no_fit[i] = false;
    for (int i=0; i<params.ndwtot2; i++)
      dw2_no_fit[i] = false;
    for (int i=0; i<params.nocctot; i++)
      occup_no_fit[i] = false;
    op = ROD::rod_calc::run_fit(pseudo_method, float(params.atten), 
				float(params.scale), float(params.scale_min), 
				float(params.scale_max), no_fit,
				float(params.scale2), float(params.scale2_min), 
				float(params.scale2_max), no_fit,
				float(params.beta), float(params.beta_min), 
				float(params.beta_max), no_fit,
				float(params.sfrac), float(params.sfrac_min), 
				float(params.sfrac_max), no_fit,
				dist, dist_min, dist_max, dist_no_fit, int(params.ndisttot),
				dw1, dw1_min, dw1_max, dw1_no_fit, int(params.ndwtot),
				dw2, dw2_min, dw2_max, dw2_no_fit, int(params.ndwtot2),
				occup, occup_min, occup_max, occup_fit, int(params.nocctot),
				results_array,
				(float *)(&params.chisqr), (float *)(&params.norm), 
				(float *)(&params.quality),
				bool(load_data.got_fit_data), bool(load_data.got_exp_data),
				message, int(mlen), attach_op);
    delete[] dist_no_fit;
    delete[] dw1_no_fit;
    delete[] dw2_no_fit; 
    delete[] occup_no_fit; 
  }   
  else {
    op = ROD::rod_calc::run_fit(int(params.fit_method), float(params.atten), 
				float(params.scale), float(params.scale_min), 
				float(params.scale_max), bool(params.scale_fit),
				float(params.scale2), float(params.scale2_min), 
				float(params.scale2_max), bool(params.scale2_fit),
				float(params.beta), float(params.beta_min), 
				float(params.beta_max), bool(params.beta_fit),
				float(params.sfrac), float(params.sfrac_min), 
				float(params.sfrac_max), bool(params.sfrac_fit),
				dist, dist_min, dist_max, dist_fit, int(params.ndisttot),
				dw1, dw1_min, dw1_max, dw1_fit, int(params.ndwtot),
				dw2, dw2_min, dw2_max, dw2_fit, int(params.ndwtot2),
				occup, occup_min, occup_max, occup_fit, int(params.nocctot),
				results_array,
				(float *)(&params.chisqr), (float *)(&params.norm), 
				(float *)(&params.quality),
				bool(load_data.got_fit_data), bool(load_data.got_exp_data),
				message, int(mlen), attach_op);
  }
  if(op ==0)
    ok = CCP3::return_info(DLV_ERROR, "rod_fit",
			   "problem wuiing fit");
  else{
    //bool new_view = false;
    DLV::operation *parent = ROD::rod_plot::get_parent();
    bool first_edit = false;
    bool cancel_edit = false;
    ok = ROD::rod_calc::update_1d_plot(parent, first_edit, cancel_edit, 
				       message, mlen);
    parent = ROD::rod_ffac::get_parent();
    ok = ROD::rod_calc::update_2d_plot(parent, first_edit, cancel_edit, 
				       message, mlen);
   }
  if(params.ndisttot !=0){
    if (params.fit_method != 0)
      for (int i=0; i<params.ndisttot; i++)
	dist[i] = results_array[4+i][0];
    params.dist.free_array(dist);
    params.dist_min.free_array(dist_min);
    params.dist_max.free_array(dist_max);
    params.dist_fit.free_array(dist_fit);
  }
  if(params.ndwtot !=0){
    if (params.fit_method != 0)
      for (int i=0; i<params.ndwtot; i++)
	dw1[i] = results_array[4+params.ndisttot+i][0];
    params.dw1.free_array(dw1);
    params.dw1_min.free_array(dw1_min);
    params.dw1_max.free_array(dw1_max);
    params.dw1_fit.free_array(dw1_fit);
  }
  if(params.ndwtot2 !=0){
    if (params.fit_method != 0)
      for (int i=0; i<params.ndwtot2; i++)
	dw2[i] = results_array[4+params.ndisttot+params.ndwtot+i][0];
    params.dw2.free_array(dw2);
    params.dw2_min.free_array(dw2_min);
    params.dw2_max.free_array(dw2_max);
    params.dw2_fit.free_array(dw2_fit);
  }
  if(params.nocctot !=0){
    if (params.fit_method != 0)
      for (int i=0; i<params.nocctot; i++)
	occup[i] = results_array[4+params.ndisttot+params.ndwtot+params.ndwtot2+i][0];
    params.occup.free_array(occup);
    params.occup_min.free_array(occup_min);
    params.occup_max.free_array(occup_max);
    params.occup_fit.free_array(occup_fit);
  }
  if (params.fit_method != 0) {
    params.scale = results_array[0][0];
    params.scale2 = results_array[1][0];
    params.beta = results_array[2][0];
    params.sfrac = results_array[3][0];
  }
  params.fit_results.free_array(results_array);
  params.chisqr = *(float *)(&params.chisqr);
  params.norm = *(float *)(&params.norm);
  params.quality = *(float *)(&params.quality);
  params.show_fit_results = true;
  if(ok == DLV_ERROR)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.ROD.RUN_FIT", message);
  else if(ok == DLV_WARNING)
    ok = CCP3::return_info(DLV_WARNING, "CCP3.ROD.RUN_FIT", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_ROD_Modules_get_rod_data::start(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  if(trigger){
    const int mlen = 256;
    char message[mlen];
    int ndisttot;
    //  Check to see if rod_ids have been set for each atom; set to zero if they have not
    ok = ROD::data::set_missing_rod_ids();
    if(ok == DLV_ERROR){
      ok = CCP3::return_info(DLV_ERROR, "CCP3.ROD.GET_FIT_DATA", message);
      return ok;
    }
    // set rod data 
    int ndwtot;
    int ndwtot2;
    int nocctot;
    float scale, scale_min, scale_max;
    float scale2, scale2_min, scale2_max;
    float beta, beta_min, beta_max;
    float sfrac, sfrac_min, sfrac_max;
    bool scale_fit, scale2_fit, beta_fit, sfrac_fit, show_fit_results;
    int maxpar;
    ok = ROD::rod_calc::get_rod_data(scale, scale_min, scale_max, scale_fit,
				     scale2, scale2_min, scale2_max, scale2_fit,
				     beta, beta_min, beta_max, beta_fit,
				     sfrac, sfrac_min, sfrac_max, sfrac_fit,
				     ndisttot, ndwtot, ndwtot2, nocctot, 
				     maxpar, bool(load_data.got_par_data),
				     show_fit_results, message, mlen); 
    if(ok == DLV_ERROR){
      ok = CCP3::return_info(DLV_ERROR, "CCP3.ROD.GET_FIT_DATA", message);
      return ok;
    }
    else if(ok == DLV_WARNING)
      ok = CCP3::return_info(DLV_WARNING, "CCP3.ROD.GET_FIT_DATA", message);
    params.scale = scale;
    params.scale_min = scale_min;
    params.scale_max = scale_max;
    params.scale_fit = scale_fit;
    params.scale2 = scale2;
    params.scale2_min = scale2_min;
    params.scale2_max = scale2_max;
    params.scale2_fit = scale2_fit;
    params.beta = beta;
    params.beta_min = beta_min;
    params.beta_max = beta_max;
    params.beta_fit = beta_fit;
    params.sfrac = sfrac;
    params.sfrac_min = sfrac_min;
    params.sfrac_max = sfrac_max;
    params.sfrac_fit = sfrac_fit;
    params.show_fit_results = show_fit_results;
    params.ndisttot = ndisttot;
    params.ndwtot = ndwtot;
    params.ndwtot2 = ndwtot2;
    params.nocctot = nocctot;
    params.dist.set_array_size(maxpar);
    params.dist_min.set_array_size(maxpar);
    params.dist_max.set_array_size(maxpar);
    params.dist_fit.set_array_size(maxpar);
    params.dw1.set_array_size(maxpar);
    params.dw1_min.set_array_size(maxpar);
    params.dw1_max.set_array_size(maxpar);
    params.dw1_fit.set_array_size(maxpar);
    params.dw2.set_array_size(maxpar);
    params.dw2_min.set_array_size(maxpar);
    params.dw2_max.set_array_size(maxpar);
    params.dw2_fit.set_array_size(maxpar);
    params.occup.set_array_size(maxpar);
    params.occup_min.set_array_size(maxpar);
    params.occup_max.set_array_size(maxpar);
    params.occup_fit.set_array_size(maxpar);
    float *dist, *dist_min, *dist_max;
    float *dw1, *dw1_min, *dw1_max;
    float *dw2, *dw2_min, *dw2_max;      
    float *occup, *occup_min, *occup_max;
    int *dist_fit, *dw1_fit, *dw2_fit, *occup_fit;
    if (params.ndisttot !=0){
      dist = (float *)params.dist.ret_array_ptr(OM_GET_ARRAY_WR);
      dist_min = (float *)params.dist_min.ret_array_ptr(OM_GET_ARRAY_WR);
      dist_max = (float *)params.dist_max.ret_array_ptr(OM_GET_ARRAY_WR);
      dist_fit = (int *)params.dist_fit.ret_array_ptr(OM_GET_ARRAY_WR);
    }
    if(params.ndwtot != 0){
      dw1 = (float *)params.dw1.ret_array_ptr(OM_GET_ARRAY_WR);
      dw1_min = (float *)params.dw1_min.ret_array_ptr(OM_GET_ARRAY_WR);
      dw1_max = (float *)params.dw1_max.ret_array_ptr(OM_GET_ARRAY_WR);
      dw1_fit = (int *)params.dw1_fit.ret_array_ptr(OM_GET_ARRAY_WR);
    }
    if(params.ndwtot2 != 0){
      dw2 = (float *)params.dw2.ret_array_ptr(OM_GET_ARRAY_WR);
      dw2_min = (float *)params.dw2_min.ret_array_ptr(OM_GET_ARRAY_WR);
      dw2_max = (float *)params.dw2_max.ret_array_ptr(OM_GET_ARRAY_WR);
      dw2_fit = (int *)params.dw2_fit.ret_array_ptr(OM_GET_ARRAY_WR);
    }
    if(params.nocctot != 0){
      occup = (float *)params.occup.ret_array_ptr(OM_GET_ARRAY_WR);
      occup_min = (float *)params.occup_min.ret_array_ptr(OM_GET_ARRAY_WR);
      occup_max = (float *)params.occup_max.ret_array_ptr(OM_GET_ARRAY_WR);
      occup_fit = (int *)params.occup_fit.ret_array_ptr(OM_GET_ARRAY_WR);
    }
    // Get Values for variable sized array
    ok = ROD::rod_calc::get_rod_array_data(dist, dist_min, dist_max, dist_fit,
					   dw1, dw1_min, dw1_max, dw1_fit,
					   dw2, dw2_min, dw2_max, dw2_fit,
					   occup, occup_min, occup_max, occup_fit,
					   ndisttot, ndwtot, ndwtot2, 
					   nocctot, bool(load_data.got_par_data),
					   message, mlen); 
    if(ok == DLV_ERROR)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.ROD.GET_FIT_DATA", message);
    else if(ok == DLV_WARNING)
      ok = CCP3::return_info(DLV_WARNING, "CCP3.ROD.GET_FIT_DATA", message);
    // free Arrays
    if(params.ndisttot !=0){
      params.dist.free_array(dist);
      params.dist_min.free_array(dist_min);
      params.dist_max.free_array(dist_max);
      params.dist_fit.free_array(dist_fit);
    }
    if(params.ndwtot !=0){
      params.dw1.free_array(dw1);
      params.dw1_min.free_array(dw1_min);
      params.dw1_max.free_array(dw1_max);
      params.dw1_fit.free_array(dw1_fit);
    }
    if(params.ndwtot2 !=0){
      params.dw2.free_array(dw2);
      params.dw2_min.free_array(dw2_min);
      params.dw2_max.free_array(dw2_max);
      params.dw2_fit.free_array(dw2_fit);
    } 
    if(params.nocctot !=0){
      params.occup.free_array(occup);
      params.occup_min.free_array(occup_min);
      params.occup_max.free_array(occup_max);
      params.occup_fit.free_array(occup_fit);
    }
    int size = 2*(4+params.ndisttot+params.ndwtot
		  +params.ndwtot2+params.nocctot);
    params.fit_results.set_array_size(size);
  }
  return ok;
}

int CCP3_ROD_Modules_plot_rod::start(OMevent_mask event_mask, int seq_num)
{
  // rod_data (CalculateRodData read req)
  // trigger
  // params
  int ok = OM_STAT_SUCCESS;
  if ((int)trigger == 1) {
    const int mlen = 256;
    char message[mlen];
    message[0]  = '\0';
    DLV::operation *op;
    DLV::model *m = DLV::operation::get_current_model();
    float *dist, *dw1, *dw2, *occup;
    if (params.ndisttot !=0)
      dist = (float *)params.dist.ret_array_ptr(OM_GET_ARRAY_RD);
    if(params.ndwtot != 0)
      dw1 = (float *)params.dw1.ret_array_ptr(OM_GET_ARRAY_RD);
    if(params.ndwtot2 != 0)
      dw2 = (float *)params.dw2.ret_array_ptr(OM_GET_ARRAY_RD);
    if(params.nocctot != 0)
      occup = (float *)params.occup.ret_array_ptr(OM_GET_ARRAY_RD);
    op = ROD::rod_calc::plot_rod(m, params.atten, 
				 params.scale, params.scale2, 
				 params.beta, params.sfrac,
				 dist, dw1, dw2, occup,
				 params.ndisttot, params.ndwtot, params.ndwtot2,
				 params.nocctot,
				 rod_data.h, rod_data.k,
				 rod_data.lstart, rod_data.lend, rod_data.nl,
				 rod_data.plot_bulk, rod_data.plot_surf, 
				 rod_data.plot_both, rod_data.plot_exp, 
				 load_data.got_fit_data,
				 load_data.got_exp_data,
				 rod_data.new_view, 
				 message, mlen);
    if(params.ndisttot !=0)
      params.dist.free_array(dist);
    if(params.ndwtot !=0)
      params.dw1.free_array(dw1);
    if(params.ndwtot2 !=0)
      params.dw2.free_array(dw2);
    if(params.nocctot !=0)
      params.occup.free_array(occup);
    if (op==0)
      CCP3::return_info(DLV_ERROR, "CCP3.ROD.PLOT_ROD", message);
    else 
      CCP3::render_in_view(op, false);
  }
  return ok;
}

int CCP3_ROD_Modules_plot_ffactors::start(OMevent_mask event_mask, int seq_num)
{
  // ffac_data (CalculateRodData read req)
  // trigger
  // params
  int ok = OM_STAT_SUCCESS;
  if ((int)trigger == 1) {
    const int mlen = 256;
    char message[mlen];
    message[0]  = '\0';
    DLV::model *m = DLV::operation::get_current_model();
    float *dist, *dw1, *dw2, *occup;
    if (params.ndisttot !=0)
      dist = (float *)params.dist.ret_array_ptr(OM_GET_ARRAY_RD);
    if(params.ndwtot != 0)
      dw1 = (float *)params.dw1.ret_array_ptr(OM_GET_ARRAY_RD);
    if(params.ndwtot2 != 0)
      dw2 = (float *)params.dw2.ret_array_ptr(OM_GET_ARRAY_RD);
    if(params.nocctot != 0)
      occup = (float *)params.occup.ret_array_ptr(OM_GET_ARRAY_RD);
    DLV::operation *op = 
      ROD::rod_calc::plot_ffactors(m, params.atten, 
				   params.scale, params.scale2,
				   params.beta, params.sfrac,
				   dist, dw1, dw2, occup,
				   params.ndisttot, params.ndwtot, params.ndwtot2,
				   params.nocctot,
				   ffac_data.select_fs, 
				   ffac_data.h_start, 
				   ffac_data.k_start, ffac_data.h_end, 
				   ffac_data.k_end, ffac_data.h_step, 
				   ffac_data.k_step, ffac_data.l, 
				   ffac_data.maxq,
				   ffac_data.plot_calc, 
				   ffac_data.plot_exp,
				   load_data.got_fit_data,
				   load_data.got_exp_data,
				   ffac_data.new_view, message, mlen);
    if(params.ndisttot !=0)
      params.dist.free_array(dist);
    if(params.ndwtot !=0)
      params.dw1.free_array(dw1);
    if(params.ndwtot2 !=0)
      params.dw2.free_array(dw2);
    if(params.nocctot !=0)
      params.occup.free_array(occup);
    if(op==0)
      ok = CCP3::return_info(DLV_ERROR, "CCP3.ROD.SAVE_DATA", message);
    else
      CCP3::render_in_view(op, false);
  }
  return ok;
}

int CCP3_ROD_Modules_view_rod_files::view(OMevent_mask event_mask, int seq_num)
{
  // trigger, text
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  const int mlen = 256;
  char message[mlen];
  message[0] = '\0';
  if(trigger == 4){  //dat  
    if (DLV::check_filename(data_file, message, mlen)) {
      std::ifstream input;
      if (DLV::open_file_read(input, data_file, message, mlen)) {
	char line[512];
	int nlines = 0;
	while (!input.eof()) {
	  input.getline(line, 512);
	  int i = strlen(line);
	  if (line[i - 1] == '\n')
	    i--;
	  while (line[i - 1] == ' ' and i > 0)
	    i--;
	  line[i] = '\0';
	  OMset_array_size(text.obj_id(), nlines + 1);
	  OMset_str_array_val(text.obj_id(), nlines, line);
	  nlines++;
	}
      }
    }
  }
  else{ // not dat
    const int maxlines = 1000;
    int nlines = 0;
    char store_data[maxlines][mlen];
    bool tmp = ROD::data::initialise_model(message, mlen);
    if( tmp == false)
      return CCP3::return_info(DLV_ERROR, "CCP3.ROD.view_rod_files", message);
    ok = ROD::save_structure::buffer(store_data, trigger, nlines, message, mlen);
    for (int i=0; i<nlines; i++){
      OMset_array_size(text.obj_id(), i+1);
      OMset_str_array_val(text.obj_id(), i, store_data[i]);
    }
  }
  if (strlen(message) > 0) 
    return CCP3::return_info(DLV_ERROR, "CCP3.ROD.view_rod_files", message);
  else
    return ok;
}

int CCP3_ROD_Modules_save_rod_files::save(OMevent_mask event_mask, int seq_num)
{
  // save_data (SaveData read req)
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  const int mlen = 256;
  char message[mlen];
  int file_type;
  (void) /*bool tmp =*/ ROD::data::initialise_model(message, mlen); 
  //change return type and do error checks for above line
  //line above not needed if just writing a param file I think
  if (save_data.file_bulk[0] != '\0'){
    char *filename = (char *) save_data.file_bulk;
    file_type = 0;
    ok = ROD::save_structure::create(filename, file_type, message, mlen);
  }
  if (save_data.file_surf[0] != '\0'){
    char *filename = (char *) save_data.file_surf;
    file_type = 1;
    ok = ROD::save_structure::create(filename, file_type, message, mlen);
  }
  if (save_data.file_fit[0] != '\0'){
    char *filename = (char *) save_data.file_fit;
    file_type = 2;
    ok = ROD::save_structure::create(filename, file_type, message, mlen);
  }
  if (save_data.file_par[0] != '\0'){
    char *filename = (char *) save_data.file_par;
    file_type = 3;
    ok = ROD::save_structure::create(filename, file_type, message, mlen);
  }
  if(ok == DLV_ERROR)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.ROD.SAVE_DATA", message);
  else if(ok == DLV_WARNING)
    ok = CCP3::return_info(DLV_WARNING, "CCP3.ROD.SAVE_DATA", message);
  CCP3::show_as_idle();
  return ok;
}

//Maybe combine 6 below into one???
int CCP3_ROD_Modules_show_lists::dis_on(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  visible_dis = 1;
  return ok;
}

int CCP3_ROD_Modules_show_lists::dis_off(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  visible_dis = 0;
  return ok;
}
int CCP3_ROD_Modules_show_lists::dw1_on(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  visible_dw1 = 1;
  return ok;
}

int CCP3_ROD_Modules_show_lists::dw1_off(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  visible_dw1 = 0;
  return ok;
}

int CCP3_ROD_Modules_show_lists::dw2_on(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  visible_dw2 = 1;
  return ok;
}

int CCP3_ROD_Modules_show_lists::dw2_off(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  visible_dw2 = 0;
  return ok;
}

int CCP3_ROD_Modules_show_lists::occ_on(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  visible_occ = 1;
  return ok;
}

int CCP3_ROD_Modules_show_lists::occ_off(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  visible_occ = 0;
  return ok;
}

int CCP3_ROD_Modules_edit_model::update(OMevent_mask event_mask, int seq_num)
{
  //really min and max not needed - maybe split fit and calc_fit_quality should differ
  int ok = OM_STAT_SUCCESS;
  const int mlen = 256;
  char message[mlen];
  CCP3::show_as_busy();
  float *dist; 
  if(params.ndisttot !=0)
    dist = (float *)params.dist.ret_array_ptr(OM_GET_ARRAY_WR);
  float *dist_min = (float *)params.dist_min.ret_array_ptr(OM_GET_ARRAY_RD);  
  float *dist_max = (float *)params.dist_max.ret_array_ptr(OM_GET_ARRAY_RD);
  int *dist_fit = (int *)params.dist_fit.ret_array_ptr(OM_GET_ARRAY_RD);
  float *dw1;
  if(params.ndwtot !=0)
    dw1 = (float *)params.dw1.ret_array_ptr(OM_GET_ARRAY_WR);
  float *dw1_min = (float *)params.dw1_min.ret_array_ptr(OM_GET_ARRAY_RD);  
  float *dw1_max = (float *)params.dw1_max.ret_array_ptr(OM_GET_ARRAY_RD);
  int *dw1_fit = (int *)params.dw1_fit.ret_array_ptr(OM_GET_ARRAY_RD);
  float *dw2;
  if(params.ndwtot2 !=0)
    dw2 = (float *)params.dw2.ret_array_ptr(OM_GET_ARRAY_WR);
  float *dw2_min = (float *)params.dw2_min.ret_array_ptr(OM_GET_ARRAY_RD);  
  float *dw2_max = (float *)params.dw2_max.ret_array_ptr(OM_GET_ARRAY_RD);
  int *dw2_fit = (int *)params.dw2_fit.ret_array_ptr(OM_GET_ARRAY_RD);
  float *occup;
  if(params.nocctot !=0)
    occup = (float *)params.occup.ret_array_ptr(OM_GET_ARRAY_WR);
  float *occup_min = (float *)params.occup_min.ret_array_ptr(OM_GET_ARRAY_RD);  
  float *occup_max = (float *)params.occup_max.ret_array_ptr(OM_GET_ARRAY_RD);
  int *occup_fit = (int *)params.occup_fit.ret_array_ptr(OM_GET_ARRAY_RD);
  float (*results_array)[2];
  results_array = (float (*)[2]) params.fit_results.ret_array_ptr(OM_GET_ARRAY_WR);
  DLV::operation *op;
  int pseudo_method = 1;
  bool no_fit = 0;
  int *dist_no_fit = new int[params.ndisttot];
  int *dw1_no_fit = new int[params.ndwtot];
  int *dw2_no_fit = new int[params.ndwtot2];
  int *occup_no_fit = new int[params.nocctot];
  for (int i=0; i<params.ndisttot; i++)
    dist_no_fit[i] = false;
  for (int i=0; i<params.ndwtot; i++)
    dw1_no_fit[i] = false;
  for (int i=0; i<params.ndwtot2; i++)
    dw2_no_fit[i] = false;
  for (int i=0; i<params.nocctot; i++)
    occup_no_fit[i] = false;
  bool attach_op = false;
  op = ROD::rod_calc::run_fit(pseudo_method, float(params.atten), 
			      float(params.scale), float(params.scale_min), 
			      float(params.scale_max), no_fit,
			      float(params.scale2), float(params.scale2_min), 
			      float(params.scale2_max), no_fit,
			      float(params.beta), float(params.beta_min), 
			      float(params.beta_max), no_fit,
			      float(params.sfrac), float(params.sfrac_min), 
			      float(params.sfrac_max), no_fit,
			      dist, dist_min, dist_max, dist_no_fit, int(params.ndisttot),
			      dw1, dw1_min, dw1_max, dw1_no_fit, int(params.ndwtot),
			      dw2, dw2_min, dw2_max, dw2_no_fit, int(params.ndwtot2),
			      occup, occup_min, occup_max, occup_fit, int(params.nocctot),
			      results_array,
			      (float *)(&params.chisqr), (float *)(&params.norm), 
			      (float *)(&params.quality),
			      bool(load_data.got_fit_data), bool(load_data.got_exp_data),
			      message, int(mlen), attach_op);
  delete[] dist_no_fit;
  delete[] dw1_no_fit;
  delete[] dw2_no_fit;
  delete[] occup_no_fit;
  if(op ==0)
    ok = CCP3::return_info(DLV_ERROR, "rod_fit",
			   "problem wuiing fit");
  else{
    DLV::operation *parent = 0;
    if(edit_model_data.cancel_edit)
      parent = DLV::operation::get_editing();
    else
      parent = ROD::rod_plot::get_parent();
    ok = ROD::rod_calc::update_1d_plot(parent, bool(edit_model_data.first_edit),
				       bool(edit_model_data.cancel_edit), 
				       message, mlen);
    if(!edit_model_data.cancel_edit)
      parent = ROD::rod_ffac::get_parent();
    ok = ROD::rod_calc::update_2d_plot(parent, bool(edit_model_data.first_edit),
				       bool(edit_model_data.cancel_edit), 
				       message, mlen);
  }
  if(params.ndisttot !=0){
    if (params.fit_method != 0)
      for (int i=0; i<params.ndisttot; i++)
	dist[i] = results_array[4+i][0];
    params.dist.free_array(dist);
    params.dist_min.free_array(dist_min);
    params.dist_max.free_array(dist_max);
    params.dist_fit.free_array(dist_fit);
  }
  if(params.ndwtot !=0){
    if (params.fit_method != 0)
      for (int i=0; i<params.ndwtot; i++)
	dw1[i] = results_array[4+params.ndisttot+i][0];
    params.dw1.free_array(dw1);
    params.dw1_min.free_array(dw1_min);
    params.dw1_max.free_array(dw1_max);
    params.dw1_fit.free_array(dw1_fit);
  }
  if(params.ndwtot2 !=0){
    if (params.fit_method != 0)
      for (int i=0; i<params.ndwtot2; i++)
	dw2[i] = results_array[4+params.ndisttot+params.ndwtot+i][0];
    params.dw2.free_array(dw2);
    params.dw2_min.free_array(dw2_min);
    params.dw2_max.free_array(dw2_max);
    params.dw2_fit.free_array(dw2_fit);
  }
  if(params.nocctot !=0){
    if (params.fit_method != 0)
      for (int i=0; i<params.nocctot; i++)
	occup[i] = results_array[4+params.ndisttot+params.ndwtot+params.ndwtot2+i][0];
    params.occup.free_array(occup);
    params.occup_min.free_array(occup_min);
    params.occup_max.free_array(occup_max);
    params.occup_fit.free_array(occup_fit);
  }
  if (params.fit_method != 0) {
    params.scale = results_array[0][0];
    params.scale2 = results_array[1][0];
    params.beta = results_array[2][0];
    params.sfrac = results_array[3][0];
  }
  params.fit_results.free_array(results_array);
  params.chisqr = *(float *)(&params.chisqr);
  params.norm = *(float *)(&params.norm);
  params.quality = *(float *)(&params.quality);
  params.show_fit_results = true;
  if(ok == DLV_ERROR)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.ROD.UPDATE_MODEL", message);
  else if(ok == DLV_WARNING)
    ok = CCP3::return_info(DLV_WARNING, "CCP3.ROD.UPDATE_MODEL", message);
  CCP3::show_as_idle();
  return ok;
}

int CCP3_ROD_Modules_edit_model::edit(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  params.fit_method = 0;
  ok = update(event_mask, seq_num);
  edit_model_data.first_edit = false;
  return ok;
}

int CCP3_ROD_Modules_edit_model::cancel(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  edit_model_data.cancel_edit = true;
  params.fit_method = 0;
  DLV::operation::detach_pending();
  ok = update(event_mask, seq_num);
  ok = CCP3::render_in_view(DLV::operation::get_current(), false);
  if (DLV::operation::delete_pending() != DLV_OK)
    ok = CCP3::return_info(DLV_ERROR, "CCP3.core.Model.cancel",
			   "Problem canceling new model");
  edit_model_data.first_edit = true;
  edit_model_data.cancel_edit = false;
  CCP3::show_as_idle();
  return ok;
}

int CCP3_ROD_Modules_edit_model::accept(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  CCP3::show_as_busy();
  const int mlen = 256;
  char message[mlen];
  CCP3::show_as_busy();
  DLV::operation *op = ROD::rod_calc::accept_rod_pending(message, mlen);
  ok = CCP3::render_in_view(op, false);
  edit_model_data.first_edit = true;
  return ok;
}

/*int CCP3_ROD_Modules_edit_model::insert(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  if(trigger_insert == 0){
    params.fit_method = 0;
    ok = update(event_mask, seq_num);
  }
  return ok;
  }

int CCP3_ROD_Modules_edit_model::remove(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  if(trigger_delete == 0){
    params.fit_method = 0;
    ok = update(event_mask, seq_num);
  }
  return ok;
  }

int CCP3_ROD_Modules_edit_model::change(OMevent_mask event_mask, int seq_num)
{
  int ok = OM_STAT_SUCCESS;
  if(trigger_change == 0){
    params.fit_method = 0;
    ok = update(event_mask, seq_num);
  }
  return ok;
}*/



int CCP3_ROD_Modules_atom_labels::show(OMevent_mask event_mask, int seq_num)
{
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  const int mlen = 256;
  char message[mlen];
  ok = ROD::data::show_atom_labels(trigger_show, message, mlen);
  if (ok == DLV_ERROR)
    CCP3::return_info(DLV_ERROR, "CCP3.ROD.show_atom_labels", message);
  return ok;
}

int CCP3_ROD_Modules_atom_labels::edit(OMevent_mask event_mask, int seq_num)
{ 
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  const int mlen = 256;
  char message[mlen];
  bool all = false;  //could be useful to have this option later
  int ndw1 = params.ndwtot;
  int ndw2 = params.ndwtot2;
  int nocc = params.nocctot;
  ok = ROD::data::update_rod_props(new_dw1, use_dw1, ndw1, 
				   new_dw2, use_dw2, ndw2, 
				   new_occ, use_occ, nocc, 
				   all, message, mlen);
  if((trigger_show == 2 && use_dw1) || (trigger_show == 3 && use_dw2)
     || (trigger_show == 4 && use_occ)){
    ok = ROD::data::show_atom_labels(trigger_show, message, mlen);
    if (ok == DLV_ERROR){
      CCP3::return_info(DLV_ERROR, "CCP3.ROD.update_rod_props", message);
      return ok;
    }
  }
  if(use_dw1)
    ok = ndw1;
  else if(use_dw2)
    ok = ndw2;
  else if(use_occ)
    ok = nocc;
  else
    ok = DLV_ERROR;
  CCP3::show_as_idle();
  return ok;
}

int CCP3_ROD_Modules_atom_labels::edit_dw1(OMevent_mask event_mask, int seq_num)
{ 
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  use_dw1 = true;
  use_dw2 = false;
  use_occ = false;
  int ndw1 =  edit(event_mask, seq_num);
  if(ndw1 < 1){
    ok = DLV_ERROR;
    return ok;
  }
  if (params.ndwtot != ndw1){
    params.ndwtot = ndw1;
    float *dw1 = (float *)params.dw1.ret_array_ptr(OM_GET_ARRAY_WR);
    float *dw1_min = (float *)params.dw1_min.ret_array_ptr(OM_GET_ARRAY_WR);
    float *dw1_max = (float *)params.dw1_max.ret_array_ptr(OM_GET_ARRAY_WR);
    int *dw1_fit = (int *)params.dw1_fit.ret_array_ptr(OM_GET_ARRAY_WR);
    dw1[ndw1-1] = 0.0;//set to what it was???
    dw1_min[ndw1-1] = 0.0;
    dw1_max[ndw1-1] = 1.0;
    dw1_fit[ndw1-1] = 1;
    params.dw1.free_array(dw1);
    params.dw1_min.free_array(dw1_min);
    params.dw1_max.free_array(dw1_max);
    params.dw1_fit.free_array(dw1_fit);
  };
  // update graphs if they are being shown????
  return ok;
}

int CCP3_ROD_Modules_atom_labels::edit_dw2(OMevent_mask event_mask, int seq_num)
{ 
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  use_dw1 = false;
  use_dw2 = true;
  use_occ = false;
  int ndw2 =  edit(event_mask, seq_num);
  if(ndw2 < 1){
    ok = DLV_ERROR;
    return ok;
  }
  if (params.ndwtot2 != ndw2){
    params.ndwtot2 = ndw2;
    float *dw2 = (float *)params.dw2.ret_array_ptr(OM_GET_ARRAY_WR);
    float *dw2_min = (float *)params.dw2_min.ret_array_ptr(OM_GET_ARRAY_WR);
    float *dw2_max = (float *)params.dw2_max.ret_array_ptr(OM_GET_ARRAY_WR);
    int *dw2_fit = (int *)params.dw2_fit.ret_array_ptr(OM_GET_ARRAY_WR);
    dw2[ndw2-1] = 0.0;//set to what it was???
    dw2_min[ndw2-1] = 0.0;
    dw2_max[ndw2-1] = 1.0;
    dw2_fit[ndw2-1] = 1;
    params.dw2.free_array(dw2);
    params.dw2_min.free_array(dw2_min);
    params.dw2_max.free_array(dw2_max);
    params.dw2_fit.free_array(dw2_fit);
  };
  return ok;
}

int CCP3_ROD_Modules_atom_labels::edit_occ(OMevent_mask event_mask, int seq_num)
{ 
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  use_dw1 = false;
  use_dw2 = false;
  use_occ = true;
  int nocc =  edit(event_mask, seq_num);
  if(nocc < 1){
    ok = DLV_ERROR;
    return ok;
  }
  if (params.nocctot != nocc){
    params.nocctot = nocc;
    float *occup = (float *)params.occup.ret_array_ptr(OM_GET_ARRAY_WR);
    float *occup_min = (float *)params.occup_min.ret_array_ptr(OM_GET_ARRAY_WR);
    float *occup_max = (float *)params.occup_max.ret_array_ptr(OM_GET_ARRAY_WR);
    int *occup_fit = (int *)params.occup_fit.ret_array_ptr(OM_GET_ARRAY_WR);
    occup[nocc-1] = 1.0; //set to what it was???
    occup_min[nocc-1] = 0.0;
    occup_max[nocc-1] = 1.0;
    occup_fit[nocc-1] = 1;
    params.occup.free_array(occup);
    params.occup_min.free_array(occup_min);
    params.occup_max.free_array(occup_max);
    params.occup_fit.free_array(occup_fit);
  };
  return ok;
}

int CCP3_ROD_Modules_update_params::update_dis(OMevent_mask event_mask, int seq_num)
{ 
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  if (params.ndisttot != 0){
    int *dist_fit = (int *)params.dist_fit.ret_array_ptr(OM_GET_ARRAY_WR);
    for (int i = 0; i < params.ndisttot; i++)
      if (dist_fit[i]== 0)
	dist_fit[i] = 1;
      else
	dist_fit[i] = 0;
      params.dist_fit.free_array(dist_fit);
  }
  return ok;
}

int CCP3_ROD_Modules_update_params::update_dw1(OMevent_mask event_mask, int seq_num)
{ 
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  if (params.ndwtot != 0){
    int *dw1_fit = (int *)params.dw1_fit.ret_array_ptr(OM_GET_ARRAY_WR);
    for (int i = 0; i < params.ndwtot; i++)
      if (dw1_fit[i]== 0)
	dw1_fit[i] = 1;
      else
	dw1_fit[i] = 0;
      params.dw1_fit.free_array(dw1_fit);
  }
  return ok;
}

int CCP3_ROD_Modules_update_params::update_dw2(OMevent_mask event_mask, int seq_num)
{ 
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  if (params.ndwtot2 != 0){
    int *dw2_fit = (int *)params.dw2_fit.ret_array_ptr(OM_GET_ARRAY_WR);
    for (int i = 0; i < params.ndwtot2; i++)
      if (dw2_fit[i]== 0)
	dw2_fit[i] = 1;
      else
	dw2_fit[i] = 0;
      params.dw2_fit.free_array(dw2_fit);
  }
  return ok;
}

int CCP3_ROD_Modules_update_params::update_occ(OMevent_mask event_mask, int seq_num)
{ 
  CCP3::show_as_busy();
  int ok = OM_STAT_SUCCESS;
  if (params.nocctot != 0){
    int *occup_fit = (int *)params.occup_fit.ret_array_ptr(OM_GET_ARRAY_WR);
    for (int i = 0; i < params.nocctot; i++)
      if (occup_fit[i]== 0)
	occup_fit[i] = 1;
      else
	occup_fit[i] = 0;
      params.occup_fit.free_array(occup_fit);
  }
  return ok;
}
