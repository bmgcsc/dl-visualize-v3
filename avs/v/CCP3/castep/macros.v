
flibrary Macros<NEeditable=1,
#ifndef DLV_NO_DLLS
		dyn_libs="libCCP3castep",
#endif // DLV_NO_DLLS
		export_cxx=1,
		build_dir="avs/src/express",
		cxx_hdr_files="avs/src/express/calcs.hxx avs/src/core/fb_gen.hxx avs/src/castep/str_gen.hxx avs/src/express/model.hxx avs/src/castep/save_gen.hxx avs/src/castep/phon_gen.hxx",
		out_hdr_file="castep.hxx",
		out_src_file="castep.cxx"> {
  macro calcBase {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels=1,NEx=198.,NEy=66.>;
    link active_menus<NEportLevels=1,NEx=385.,NEy=66.>;
    link UIparent<NEportLevels=1,NEx=770.,NEy=66.>;
    int visible<NEportLevels=1,NEx=396.,NEy=143.> = 0;
    UIcmd UIcmd<NEx=198.,NEy=143.> {
      active => <-.active_menus;
      do => <-.visible;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    GMOD.instancer instancer<NEx=264.,NEy=275.> {
      Value => <-.visible;
    };
  };
  // Bascially a copy of core/UI/file_ops LoadUI
  macro loadstrUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
    CCP3.CASTEP.Modules.StructureData &data<NEx=792.,NEy=77.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
      width = 300;
      height = 300;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      title = "Load Cell File";
      ok = 0;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
    };
    UIpanel panel<NEx=374.,NEy=209.> {
      parent => <-.dialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    UIlabel UIlabel<NEx=660.,NEy=165.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y = 0;
      alignment = "center";
      width => parent.clientWidth;
      label = "Model name";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext DLVtext<NEx=671.,NEy=242.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 5;
      y => (<-.UIlabel.y + <-.UIlabel.height + 5);
      width => (parent.clientWidth - 10);
      text => <-.data.name;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle new_view<NEx=462.,NEy=451.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.FileB.y + <-.FileB.height + 5);
      width => parent.clientWidth;
      label = "Open in new viewer";
      set => <-.data.new_view;
    };
    /*CCP3.Core_Modules.Utils.FBdata FBdata<NEx=715.,NEy=352.,
      NEportLevels={0,1}> {
      filename<NEportLevels={2,0}> => <-.data.file;
      pattern = "*.cell";
      parent1<NEportLevels={2,0}> => <-.panel;
      title = "Load CASTEP cell filename";
      file_write = 0;
      x = 0;
      y => (<-.DLVtext.y + <-.DLVtext.height + 5);
      onlyButton = 0;
      ok = 0;
    };
    CCP3.Core_Modules.Utils.manageFB manageFB<NEx=704.,NEy=462.> {
      data => <-.FBdata;
      };*/
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=704.,NEy=462.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}> => <-.data.file;
      pattern = "*.cell";
      parent => <-.panel;
      title = "Load CASTEP cell filename";
      x = 0;
      y => (<-.DLVtext.y + <-.DLVtext.height + 5);
    };
    GMOD.parse_v reset_params<NEx=583.,NEy=77.> {
      v_commands = "data.new_view = 0; data.name = \"\";";
      relative => <-;
    };
  };
  calcBase Load_Structure {
    UIcmd {
      label = "Load Structure";
    };
    CCP3.CASTEP.Modules.StructureData data<NEx=627.,NEy=143.,
      NEportLevels={0,1}> {
      name = "";
      file = "";
      new_view = 0;
    };
    instancer {
      Group => <-.loadstrUI;
    };
    loadstrUI loadstrUI<NEx=528.,NEy=341.,instanced=0> {
      prefs => <-.prefs;
      data => <-.data;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
      /*manageFB {
	connect => <-.<-.visible;
	};*/
    };
    CCP3.CASTEP.Modules.load_structure load_structure<NEx=407.,NEy=484.> {
      data => <-.data;
      do_load => <-.loadstrUI.dialog.ok;
    };
  };
  loadstrUI phononUI {
    dialog {
      title = "Load CASTEP Phonons";
    };
    FileB {
      pattern = "*";
      title = "Load CASTEP Phonons filename";
    };
  };
  calcBase Phonons {
    UIcmd {
      label = "Load Phonons + Structure";
    };
    CCP3.CASTEP.Modules.StructureData data<NEx=627.,NEy=143.,
      NEportLevels={0,1}> {
      name = "";
      file = "";
      new_view = 0;
    };
    instancer {
      Group => <-.phononUI;
    };
    phononUI phononUI<NEx=528.,NEy=341.,instanced=0> {
      prefs => <-.prefs;
      data => <-.data;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
      /*manageFB {
	connect => <-.<-.visible;
	};*/
    };
    CCP3.CASTEP.Modules.load_phonons load_phonons<NEx=407.,NEy=484.> {
      data => <-.data;
      do_load => <-.phononUI.dialog.ok;
    };
  };
  macro savestrUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
      width = 300;
      height = 300;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      title = "Save CASTEP Cell file";
      ok = 0;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
    };
    UIpanel panel<NEx=374.,NEy=209.> {
      parent => <-.dialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    /*CCP3.Core_Modules.Utils.FBdata FBdata<NEx=715.,NEy=352.,
      NEportLevels={0,1}> {
      filename<NEportLevels={2,0}>;
      pattern = "*";
      parent1<NEportLevels={2,0}> => <-.panel;
      title = "Save CASTEP Cell file name";
      file_write = 1;
      x = 0;
      y = 0;
      onlyButton = 0;
      ok = 0;
    };
    CCP3.Core_Modules.Utils.manageFB manageFB<NEx=704.,NEy=462.> {
      data => <-.FBdata;
      };*/
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=704.,NEy=462.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}>;
      pattern = "*";
      parent => <-.panel;
      title = "Save CASTEP Cell file name";
      file_write = 1;
      x = 0;
      y = 0;
    };
    link fractions<NEportLevels={2,1},NEx=627.,NEy=44.>;
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle DLVtoggle<NEx=517.,NEy=341.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      x = 0;
      y => (<-.FileB.y + <-.FileB.height + 5);
      label = "Use fractional coordinates";
      set => <-.fractions;
    };
  };
  calcBase Save_Structure {
    UIcmd {
      label = "Save Structure";
    };
    instancer {
      Group => <-.savestrUI;
    };
    savestrUI savestrUI<NEx=528.,NEy=341.,instanced=0> {
      prefs => <-.prefs;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
      FileB {
	filename => <-.<-.data;
      };
      /*manageFB {
	connect => <-.<-.visible;
	};*/
      fractions => <-.fractions;
    };
    string data<NEx=627.,NEy=143.> = "";
    boolean fractions<NEportLevels=1,NEx=605.,NEy=55.> = 0;
    CCP3.CASTEP.Modules.save_cell_file save_cell_file<NEx=407.,NEy=484.> {
      filename => <-.data;
      frac_coords => fractions;
      do_save => <-.savestrUI.dialog.ok;
    };
  };
  CCP3.Core_Macros.CalcObjs.calculation CASTEP {
    UIcmdList {
      label = "CASTEP";
      order = 35;
      cmdList => {
	Load_Structure.UIcmd,
	Phonons.UIcmd,
	Save_Structure.UIcmd
      };
    };
    dialog_visible => ((Load_Structure.visible == 1) &&
		       (Phonons.visible == 1) &&
		       (Save_Structure.visible == 1));
    Load_Structure Load_Structure<NEx=110.,NEy=374.> {
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      prefs => <-.preferences;
    };
    CCP3.CASTEP.Macros.Phonons Phonons<NEx=572.,NEy=374.> {
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      prefs => <-.preferences;
    };
    CCP3.CASTEP.Macros.Save_Structure Save_Structure<NEx=319.,NEy=374.> {
      active_menus => (<-.active_menus && (<-.model_type >= 0) &&
		       (<-.model_type < 4));
      UIparent => <-.UIparent;
      prefs => <-.preferences;
    };
  };
};
