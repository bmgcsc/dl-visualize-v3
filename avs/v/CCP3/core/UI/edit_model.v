
flibrary EditModel<NEeditable=1,
		   export_cxx=1,
		   build_dir="avs/src/express/ui",
#ifdef DLV_NO_DLLS
		   dyn_libs="libCCP3dlv",
#endif // DLV_NO_DLLS
#ifdef MSDOS
		   cxx_hdr_files="nt_ui/ui_gen.h avs/src/express/ui_objs.hxx avs/src/express/ui/file_ops.hxx avs/src/core/model/edit_gen.hxx avs/src/core/model/cell_gen.hxx avs/src/core/model/mole_gen.hxx avs/src/core/model/symm_gen.hxx avs/src/core/model/latt_gen.hxx avs/src/core/model/orig_gen.hxx avs/src/core/model/vgap_gen.hxx avs/src/core/model/clus_gen.hxx avs/src/core/model/atom_gen.hxx avs/src/core/model/prop_gen.hxx avs/src/core/model/slab_gen.hxx avs/src/core/model/grp_gen.hxx avs/src/core/model/salv_gen.hxx avs/src/core/model/del_gen.hxx avs/src/core/model/insa_gen.hxx avs/src/core/model/insm_gen.hxx avs/src/core/model/move_gen.hxx avs/src/core/model/list_gen.hxx avs/src/core/model/reg_gen.hxx avs/src/core/model/wlff_gen.hxx avs/src/core/objects/lwlf_gen.hxx avs/src/core/model/mlyr_gen.hxx avs/src/core/model/geo_gen.hxx",
#else
		   cxx_hdr_files="motif_ui/ui/ui_core.h avs/src/express/ui_objs.hxx avs/src/express/ui/file_ops.hxx avs/src/core/model/edit_gen.hxx avs/src/core/model/cell_gen.hxx avs/src/core/model/mole_gen.hxx avs/src/core/model/symm_gen.hxx avs/src/core/model/latt_gen.hxx avs/src/core/model/orig_gen.hxx avs/src/core/model/vgap_gen.hxx avs/src/core/model/clus_gen.hxx avs/src/core/model/atom_gen.hxx avs/src/core/model/prop_gen.hxx avs/src/core/model/slab_gen.hxx avs/src/core/model/grp_gen.hxx avs/src/core/model/salv_gen.hxx avs/src/core/model/del_gen.hxx avs/src/core/model/insa_gen.hxx avs/src/core/model/insm_gen.hxx avs/src/core/model/move_gen.hxx avs/src/core/model/list_gen.hxx avs/src/core/model/reg_gen.hxx avs/src/core/model/wlff_gen.hxx avs/src/core/objects/lwlf_gen.hxx avs/src/core/model/mlyr_gen.hxx avs/src/core/model/geo_gen.hxx",
#endif // MSDOS
		   out_hdr_file="edit_model.hxx",
		   out_src_file="edit_model.cxx"> {
  macro baseUI {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=165.,NEy=44.>;
    link UIparent<NEportLevels={2,1},NEx=715.,NEy=44.>;
    link visible<NEportLevels={2,1},NEx=451.,NEy=44.>;
    int index<NEportLevels=1> = 0;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog DLVdialog<NEx=165.,NEy=121.> {
      parent => <-.UIparent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      ok = 0;
      okButton => <-.name_ok;
      cancel = 0;
      cancelButton = 1;
      visible => <-.visible;
#ifdef MSDOS
      y = 0;
#endif // MSDOS
    };
    UIpanel UIpanel<NEx=363.,NEy=165.> {
      parent => <-.DLVdialog;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y = 0;
      visible => <-.name_ok;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    UIpanel init_panel {
      parent => <-.DLVdialog;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y = 0;
      visible => (<-.name_ok == 0);
      width => parent.clientWidth;
      height => (<-.view.y + <-.view.height + 5);
    };
    boolean new_view<NEportLevels=1> = 0;
    string name<NEportLevels=1> = "";
    boolean name_ok => strlen(name);
    CCP3.Core_Macros.UI.UIobjs.DLVtext Info<NEx=621.,NEy=567.> {
      parent => <-.init_panel;
      y = 0;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      outputOnly = 1;
      text = "Please enter a name for the new model";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext UItext<NEx=517.,NEy=418.> {
      y => (<-.Info.y + <-.Info.height + 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      parent => <-.init_panel;
      text => <-.name;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle view<NEx=451.,NEy=484.> {
      parent => <-.init_panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y => (<-.UItext.y + <-.UItext.height + 5);
      width => parent.clientWidth;
      label = "Open in new view";
      set => <-.new_view;
    };
    GMOD.parse_v initialise<NEx=814.,NEy=319.> {
      relative => <-;
    };
  };
  baseUI cut_surfaceUI {
    DLVdialog {
      title = "Cut Surface";
      width = 400;
      height = 300;
    };
    init_panel {
      visible => ((<-.name_ok == 0) || (<-.index == 0));
    };
    initialise {
      v_commands = "index = 0; new_view = 0; name = \"\";";
    };
    UIpanel data_panel<NEx=360.,NEy=225.> {
      parent => <-.UIpanel;
      width => parent.clientWidth;
      height => (<-.tolerance.y + <-.tolerance.height + 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => (<-.name_ok && (<-.index > 0));
      y = 0;
    };
    CCP3.Core_Modules.Model.slab_params params<NEx=684.,NEy=225.,
      NEportLevels={0,1}> {
      h = 0;
      k = 0;
      l = 1;
      conventional = 1;
      termination = 0;
      nlayers = 1;
      tolerance = 0.0001;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein h<NEx=252.,NEy=324.> {
      UIparent => <-.data_panel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      flabel => "h";
      x = 5;
      y = 5;
      fval => <-.params.h;
      width => (UIparent.clientWidth / 3) - 10;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein k<NEx=468.,NEy=324.> {
      UIparent => <-.data_panel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      flabel => "k";
      x => (UIparent.clientWidth / 3) + 5;
      y = 5;
      fval => <-.params.k;
      width => (UIparent.clientWidth / 3) - 10;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein l<NEx=693.,NEy=324.> {
      UIparent => <-.data_panel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      flabel => "l";
      x => (2 * UIparent.clientWidth / 3) + 5;
      y = 5;
      fval => <-.params.l;
      width => (UIparent.clientWidth / 3) - 10;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle conventional<NEx=765.,NEy=477.> {
      parent => <-.data_panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y => (<-.h.y + <-.h.height + 5);
      width => parent.clientWidth;
      set => <-.params.conventional;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist termination<NEx=189.,NEy=396.> {
      parent => <-.data_panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y => (<-.conventional.y + <-.conventional.height + 5);
      selectedItem => <-.params.termination;
      width => parent.clientWidth;
      strings => <-.cut_slab.labels;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider tolerance<NEx=189.,NEy=468.> {
      parent => <-.data_panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y => (<-.termination.y + <-.termination.height + 5);
      min = 0.0;
      max = 0.5;
      value => <-.params.tolerance;
      decimalPoints = 4;
      width => parent.clientWidth;
    };
    CCP3.Core_Modules.Model.cut_slab cut_slab<NEx=495.,NEy=603.> {
      name => <-.name;
      new_view => <-.new_view;
      surface = 1;
      index => <-.index;
      data => <-.params;
    };
  };
  cut_surfaceUI cut_slabUI {
    DLVdialog {
      title = "Cut Slab";
      height = 350;
    };
    data_panel {
      height => (<-.nlayers.y + <-.nlayers.height + 5);
    };
    h {
      panel {
	visible => <-.<-.cut_slab.was_3D;
      };
    };
    k {
      panel {
	visible => <-.<-.cut_slab.was_3D;
      };
    };
    l {
      panel {
	visible => <-.<-.cut_slab.was_3D;
      };
    };
    conventional {
      visible => <-.cut_slab.was_3D;
    };
    termination {
      visible => <-.cut_slab.was_3D;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein nlayers<NEx=756.,NEy=405.> {
      UIparent => <-.data_panel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      y => (<-.tolerance.y + <-.tolerance.height + 5);
      fval => <-.params.nlayers;
      fmin = 1;
      flabel => "number of layers";
      width => UIparent.clientWidth;
    };
    cut_slab {
      surface = 0;
      was_3D = 1;
    };
  };
  baseUI salvageUI {
    DLVdialog {
      title = "Create Salvage";
      //height = 350;
    };
    init_panel {
      visible => ((<-.name_ok == 0) || (<-.index == 0));
    };
    initialise {
      v_commands = "index = 0; new_view = 0; name = \"\";";
    };
    CCP3.Core_Modules.Model.salvage_params params<NEx=792.,NEy=126.,
      NEportLevels={0,1}> {
      nlayers = 1;
      tolerance = 0.0001;
    };
    UIpanel data_panel<NEx=360.,NEy=225.> {
      parent => <-.UIpanel;
      width => parent.clientWidth;
      height => (<-.nlayers.y + <-.nlayers.height + 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => (<-.name_ok && (<-.index > 0));
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider tolerance<NEx=189.,NEy=468.> {
      parent => <-.data_panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y = 0;
      min = 0.0;
      max = 0.5;
      decimalPoints = 4;
      value => <-.params.tolerance;
      width => parent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein nlayers<NEx=756.,NEy=405.> {
      UIparent => <-.data_panel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      y => (<-.tolerance.y + <-.tolerance.height + 5);
      fval => <-.params.nlayers;
      fmin => cut_salvage.min_layers;
      flabel => "number of layers";
      width => UIparent.clientWidth;
    };
    CCP3.Core_Modules.Model.cut_salvage cut_salvage<NEx=666.,NEy=540.> {
      name => <-.name;
      new_view => <-.new_view;
      index => <-.index;
      data => <-.params;
    };
  };
  baseUI supercellUI {
    DLVdialog {
      title = "Supercell";
      width = 250;
      height = 250;
    };
    link model_type<NEportLevels={2,1},NEx=671.,NEy=77.>;
    CCP3.Core_Modules.Model.SuperCellParams &params<NEx=308.,NEy=77.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVifield scale_11<NEx=121.,NEy=176.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      value => params.s11;
      y = 5;
      active => (model_type > 0);
      visible => <-.name_ok;
      width => ((parent.clientWidth - 30) / 3);
      x = 5;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield scale_12<NEx=319.,NEy=176.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      value => params.s12;
      y => scale_11.y;
      active => (model_type > 1);
      width => <-.scale_11.width;
      visible => <-.name_ok;
      x => (parent.clientWidth / 3 + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield scale_13<NEx=517.,NEy=176.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      value => params.s13;
      y => scale_12.y;
      active => (model_type == 3);
      width => <-.scale_11.width;
      visible => <-.name_ok;
      x => (2 * parent.clientWidth / 3 + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield scale_21<NEx=121.,NEy=264.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      value => params.s21;
      y => ((scale_11.y + scale_11.height) + 5);
      active => (model_type > 1);
      width => <-.scale_11.width;
      visible => <-.name_ok;
      x => <-.scale_11.x;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield scale_22<NEx=319.,NEy=264.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      value => params.s22;
      y => scale_21.y;
      active => (model_type > 1);
      width => <-.scale_11.width;
      visible => <-.name_ok;
      x => <-.scale_12.x;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield scale_23<NEx=517.,NEy=264.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      value => params.s23;
      y => scale_22.y;
      active => (model_type == 3);
      width => <-.scale_11.width;
      visible => <-.name_ok;
      x => <-.scale_13.x;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield scale_31<NEx=121.,NEy=352.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      value => params.s31;
      y => ((scale_21.y + scale_21.height) + 5);
      active => (model_type == 3);
      width => <-.scale_11.width;
      visible => <-.name_ok;
      x => <-.scale_11.x;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield scale_32<NEx=319.,NEy=352.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      value => params.s32;
      y => scale_31.y;
      active => (model_type == 3);
      width => <-.scale_11.width;
      visible => <-.name_ok;
      x => <-.scale_12.x;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield scale_33<NEx=517.,NEy=352.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      value => params.s33;
      y => scale_32.y;
      active => (model_type == 3);
      width => <-.scale_11.width;
      visible => <-.name_ok;
      x => <-.scale_13.x;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle cell<NEx=154.,NEy=484.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label = "Conventional cell";
      y => (<-.scale_31.y + <-.scale_31.height + 5);
      width => <-.parent.clientWidth;
      visible => <-.name_ok;
      set => <-.params.conventional;
    };
    initialise {
      v_commands = "index = 0; new_view = 0; name = \"\";";
    };
    CCP3.Core_Modules.Model.supercell supercell<NEx=275.,NEy=550.> {
      name => <-.name;
      new_view => <-.new_view;
      index => <-.index;
      params => <-.params;
    };
  };
  baseUI insert_modelUI {
    DLVdialog {
      title = "Insert Model";
      width = 500;
      height = 300;
    };
    init_panel {
      height => (<-.models.y + <-.models.height + 5);
      visible => (<-.index == 0);
    };
    Info {
      text = "Please select a model to insert from the list\n"
	+ "and enter a name for the new model";
#ifdef MSDOS
      height => (2 * UIdata.UIfonts[0].lineHeight + 7);
      multiLine = 1;
#else
      height = 46;
#endif // MSDOS
    };
    UIpanel {
      visible => (<-.index == 3);
    };
    initialise {
      v_commands = "index = 0; new_view = 0; name = \"\";";
    };
    CCP3.Core_Modules.Model.move_params &params<NEx=639.,NEy=162.,
      NEportLevels={2,1}>;
    int model;
    CCP3.Core_Modules.Model.list_models list_models<NEx=531.,NEy=297.> {
      trigger => <-.visible;
      model_type = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist models<NEx=711.,NEy=558.> {
      parent => <-.init_panel;
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y => (<-.view.y + <-.view.height + 5);
      selectedItem => <-.model;
      strings => <-.list_models.labels;
    };
    int atom<NEportLevels=1,NEx=828.,NEy=162.> = 0;
    link nselections<NEportLevels=1,NEx=846.,NEy=207.>;
    link model_type<NEportLevels={2,1},NEx=671.,NEy=77.>;
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein set_x<NEx=253.,NEy=517.> {
      UIparent => <-.UIpanel;
      fval => <-.params.x;
      flabel => "x";
      x = 0;
      y = 5;
      width => (UIparent.clientWidth / 3);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      field {
	decimalPoints = 8;
      };
      panel {
	visible => (<-.<-.params.use_transform == 0);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein set_y<NEx=451.,NEy=517.> {
      UIparent => <-.UIpanel;
      fval => <-.params.y;
      flabel => "y";
      x => (UIparent.clientWidth / 3);
      y => <-.set_x.y;
      width => (UIparent.clientWidth / 3);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      field {
	decimalPoints = 8;
      };
      panel {
	visible => (<-.<-.params.use_transform == 0);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein set_z<NEx=649.,NEy=517.> {
      UIparent => <-.UIpanel;
      fval => <-.params.z;
      flabel => "z";
      x => (2 * UIparent.clientWidth / 3);
      y => <-.set_x.y;
      width => (UIparent.clientWidth / 3);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      field {
	decimalPoints = 8;
      };
      panel {
	visible => (<-.<-.params.use_transform == 0);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle set_frac<NEx=66.,NEy=440.> {
      parent => <-.UIpanel;
      x = 0;
      y => (<-.set_x.y + <-.set_x.height + 5);
      label = "Fractional coords";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.params.fractional;
      width => (parent.clientWidth / 2);
      visible => ((<-.params.use_transform == 0) && (<-.model_type > 0));
    };
    UIbutton calculate {
      parent => <-.UIpanel;
      label = "Calculate from Selections";
      width = 175;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      active => (<-.nselections > 0);
      x => (parent.clientWidth / 2) + 5;
      y => <-.set_frac.y;
      do = 0;
      visible => (<-.params.use_transform == 0);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle transform {
      parent => <-.UIpanel;
      x = 0;
      y => (<-.set_frac.y + <-.set_frac.height + 5);
      label = "Use Transform Editor";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.params.use_transform;
      width => (parent.clientWidth / 2);
#ifdef DLV_TRANSFORM_EDIT_BUG
      visible = 0;
#endif // DLV_TRANSFORM_EDIT_BUG
    };
    UIbutton centre {
      parent => <-.UIpanel;
      label = "Set centre";
      width = 175;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      active => (<-.nselections > 0);
      x => (parent.clientWidth / 2) + 5;
      y => <-.transform.y;
      do = 0;
      visible => (<-.params.use_transform == 0);
    };
    /*
    UIbutton Continue<NEx=360.,NEy=324.> {
#ifdef MSDOS
      parent => <-.UIpanel;
#else
      parent => <-.DLVdialog;
#endif // MSDOS
      label = "Continue Insertions";
      width = 150;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => (<-.name_ok && (<-.index > 0));
      do = 0;
    };
    GMOD.parse_v reset<NEx=252.,NEy=441.> {
      v_commands = "index = 0; name = \"\";";
      trigger => <-.Continue.do;
      on_inst = 0;
    };
    */
    CCP3.Core_Modules.Model.insert_model insert_model<NEx=504.,NEy=630.> {
      name => <-.name;
      new_view => <-.new_view;
      selection => <-.model;
      index => <-.index;
      data => <-.params;
      //fractional => <-.params.fractional;
      //x => <-.params.x;
      //y => <-.params.y;
      //z => <-.params.z;
      calc => <-.calculate.do;
      centre => <-.centre.do;
      //use_editor => <-.params.use_transform;
      //do_more => <-.Continue.do;
    };
  };
  baseUI insert_atomUI {
    DLVdialog {
      title = "Insert Atom";
      width = 600;
#ifdef MSDOS
      height = 480;
#else
      height = 450;
#endif // MSDOS
    };
    initialise {
      v_commands = "index = 0; new_view = 0; name = \"\";";
    };
    CCP3.Core_Modules.Model.move_params &params<NEx=639.,NEy=162.,
      NEportLevels={2,1}>;
    int atom<NEportLevels=1,NEx=828.,NEy=162.> = 0;
    link nselections<NEportLevels=1,NEx=846.,NEy=207.>;
    link model_type<NEportLevels={2,1},NEx=671.,NEy=77.>;
    CCP3.Core_Macros.UI.File_Ops.periodic_table periodic_table<NEx=253.,
                                                               NEy=286.> {
      preferences => <-.preferences;
      UIpanel {
	parent => <-.<-.UIpanel;
	x => (parent.clientWidth - width) / 2;
	y = 0;
      };
      atom<NEportLevels={2,1}> => <-.atom;
    };
    /*
    CCP3.Core_Modules.Model.atom_defaults atom_defaults<NEx=605.,NEy=286.> {
      trigger => <-.atom;
      charge => <-.charge;
      radius => <-.radius;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle need_charge<NEx=66.,NEy=363.> {
      parent => <-.UIpanel;
      x = 0;
      y => <-.set_charge.y;
      label = "set charge";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.use_charge;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider set_charge<NEx=253.,NEy=363.> {
      parent => <-.UIpanel;
      value => <-.charge;
      min = -8;
      max = 8;
      title = "charge";
      x => (<-.need_charge.width + 5);
      y => (<-.periodic_table.UIpanel.y + <-.periodic_table.UIpanel.height +5);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => <-.use_charge;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle need_radius<NEx=66.,NEy=440.> {
      parent => <-.UIpanel;
      x = 0;
      y => <-.set_radius.y;
      label = "set radius";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.use_radius;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider set_radius<NEx=253.,NEy=440.> {
      parent => <-.UIpanel;
      value => <-.radius;
      min = 0.02;
      max = 3.0;
      title = "radius";
      x => (<-.need_radius.width + 5);
      y => (<-.set_charge.y + <-.set_charge.height + 5);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => <-.use_radius;
    };
    */
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein set_x<NEx=253.,NEy=517.> {
      UIparent => <-.UIpanel;
      fval => <-.params.x;
      flabel => "x";
      x = 0;
      y => (<-.periodic_table.UIpanel.y + <-.periodic_table.UIpanel.height +5);
      //y => (<-.set_radius.y + <-.set_radius.height + 5);
      width => (UIparent.clientWidth / 3);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      field {
	decimalPoints = 8;
      };
      panel {
	visible => (<-.<-.params.use_transform == 0);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein set_y<NEx=451.,NEy=517.> {
      UIparent => <-.UIpanel;
      fval => <-.params.y;
      flabel => "y";
      x => (UIparent.clientWidth / 3);
      y => <-.set_x.y;
      width => (UIparent.clientWidth / 3);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      field {
	decimalPoints = 8;
      };
      panel {
	visible => (<-.<-.params.use_transform == 0);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein set_z<NEx=649.,NEy=517.> {
      UIparent => <-.UIpanel;
      fval => <-.params.z;
      flabel => "z";
      x => (2 * UIparent.clientWidth / 3);
      y => <-.set_x.y;
      width => (UIparent.clientWidth / 3);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      field {
	decimalPoints = 8;
      };
      panel {
	visible => (<-.<-.params.use_transform == 0);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle set_frac<NEx=66.,NEy=440.> {
      parent => <-.UIpanel;
      x = 0;
      y => (<-.set_x.y + <-.set_x.height + 5);
      label = "Fractional coords";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.params.fractional;
      width => (parent.clientWidth / 2);
      visible => ((<-.params.use_transform == 0) && (<-.model_type > 0));
    };
    UIbutton calculate {
      parent => <-.UIpanel;
      label = "Calculate from Selections";
      width = 175;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      active => (<-.nselections > 0);
      x => (parent.clientWidth / 2) + 5;
      y => <-.set_frac.y;
      do = 0;
      visible => (<-.params.use_transform == 0);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle transform {
      parent => <-.UIpanel;
      x = 0;
      y => (<-.set_frac.y + <-.set_frac.height + 5);
      label = "Use Transform Editor";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.params.use_transform;
      width => parent.clientWidth;
#ifdef DLV_TRANSFORM_EDIT_BUG
      visible = 0;
#endif // DLV_TRANSFORM_EDIT_BUG
    };
    UIbutton Continue<NEx=360.,NEy=324.> {
#ifdef MSDOS
      parent => <-.UIpanel;
      x => (parent.clientWidth - width) / 2;
      y => (parent.clientHeight - height - 5);
#else
      parent => <-.DLVdialog;
#endif // MSDOS
      label = "Continue Insertions";
      width = 150;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => (<-.name_ok && (<-.index > 0));
      do = 0;
    };
    GMOD.parse_v reset<NEx=252.,NEy=441.> {
      v_commands = "index = 0; name = \"\";";
      trigger => <-.Continue.do;
      on_inst = 0;
      relative => <-;
    };
    CCP3.Core_Modules.Model.insert_atom insert_atom<NEx=297.,NEy=576.> {
      name => <-.name;
      new_view => <-.new_view;
      index => <-.index;
      atom_type => <-.atom;
      data => <-.params;
      //fractional => <-.params.fractional;
      //x => <-.params.x;
      //y => <-.params.y;
      //z => <-.params.z;
      calc => <-.calculate.do;
      //use_editor => <-.params.use_transform;
      do_more => <-.Continue.do;
    };
  };
  baseUI move_atomUI {
    DLVdialog {
      title = "Move Atom";
      width = 400;
#ifdef MSDOS
      height = 270;
#else
      height = 225;
#endif // MSDOS
      okButton => (<-.index == 3);
      okLabelString = "Done";
    };
    link model_type;
    link nselections<NEportLevels=1,NEx=846.,NEy=207.>;
    initialise {
      v_commands = "index = 0; new_view = 0; name = \"\";";
    };
    init_panel {
      visible => (<-.index == 0);
    };
    Info {
      text = "Please select the atom(s) to move\n"
	+ "and enter a name for the new model";
#ifdef MSDOS
      height => (2 * UIdata.UIfonts[0].lineHeight + 7);
      multiLine = 1;
#else
      height = 46;
#endif // MSDOS
    };
    UIpanel {
      visible => (<-.index == 3);
    };
    CCP3.Core_Modules.Model.move_params &params<NEx=639.,NEy=162.,
      NEportLevels={2,1}>;
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox displacements<NEx=540.,NEy=270.> {
      parent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y = 5;
      title => "Coordinates";
      labels => {"Absolute", "Displacements"};
      UIradioBox {
	orientation = "horizontal";
      };
      selectedItem => <-.params.displacements;
      width => parent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein set_x<NEx=253.,NEy=517.> {
      UIparent => <-.UIpanel;
      fval => <-.params.x;
      flabel => "x";
      x = 0;
      y => (<-.displacements.y + <-.displacements.height + 5);
      width => (UIparent.clientWidth / 3);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      field {
	decimalPoints = 8;
      };
      panel {
	visible => (<-.<-.params.use_transform == 0);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein set_y<NEx=451.,NEy=517.> {
      UIparent => <-.UIpanel;
      fval => <-.params.y;
      flabel => "y";
      x => (UIparent.clientWidth / 3);
      y => <-.set_x.y;
      width => (UIparent.clientWidth / 3);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      field {
	decimalPoints = 8;
      };
      panel {
	visible => (<-.<-.params.use_transform == 0);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein set_z<NEx=649.,NEy=517.> {
      UIparent => <-.UIpanel;
      fval => <-.params.z;
      flabel => "z";
      x => (2 * UIparent.clientWidth / 3);
      y => <-.set_x.y;
      width => (UIparent.clientWidth / 3);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      field {
	decimalPoints = 8;
      };
      panel {
	visible => (<-.<-.params.use_transform == 0);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle set_frac<NEx=66.,NEy=440.> {
      parent => <-.UIpanel;
      x = 0;
      y => (<-.set_x.y + <-.set_x.height + 5);
      label = "Fractional coords";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.params.fractional;
      width => (parent.clientWidth / 2);
      visible => ((<-.params.use_transform == 0) && (<-.model_type > 0));
    };
    UIbutton calculate {
      parent => <-.UIpanel;
      label = "Calculate from Selections";
      width = 175;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      active => (<-.nselections > 0);
      x => (parent.clientWidth / 2) + 5;
      y => <-.set_frac.y;
      do = 0;
      visible => (<-.params.use_transform == 0);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle transform {
      parent => <-.UIpanel;
      x = 0;
#ifdef MSDOS
      y => (<-.calculate.y + <-.calculate.height + 5);
#else
      y => (<-.set_frac.y + <-.set_frac.height + 5);
#endif // MSDOS
      label = "Use Transform Editor";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.params.use_transform;
      width => (parent.clientWidth / 2);
#ifdef DLV_TRANSFORM_EDIT_BUG
      visible = 0;
#endif // DLV_TRANSFORM_EDIT_BUG
    };
    UIbutton centre {
      parent => <-.UIpanel;
      label = "Set centre";
      width = 175;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      active => (<-.nselections > 0);
      x => (parent.clientWidth / 2) + 5;
      y => <-.transform.y;
      do = 0;
      visible => (<-.params.use_transform == 0);
    };
    CCP3.Core_Modules.Model.move_atom move_atom<NEx=711.,NEy=630.> {
      name => <-.name;
      new_view => <-.new_view;
      nselects => <-.nselections;
      index => <-.index;
      data => <-.params;
      //fractional => <-.params.fractional;
      //x => <-.params.x;
      //y => <-.params.y;
      //z => <-.params.z;
      calc => <-.calculate.do;
      centre => <-.centre.do;
      //use_editor => <-.params.use_transform;
      do_more => <-.Continue.do;
    };
    UIbutton Continue {
#ifdef MSDOS
      parent => <-.UIpanel;
      x => (parent.clientWidth - width) / 2;
      y => (parent.clientHeight - height - 5);
#else
      parent => <-.DLVdialog;
#endif // MSDOS
      label = "Continue Moving";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => (<-.name_ok && (<-.index > 0));
      width = 120;
    };
    /*GMOD.parse_v reset<NEx=252.,NEy=441.> {
      v_commands = "index = 0; name = \"\";";
      trigger => <-.Continue.do;
      on_inst = 0;
      relative => <-;
      };*/
  };
  baseUI change_atomUI {
    DLVdialog {
      title = "Change Atom";
      width = 510;
#ifdef MSDOS
      height = 400;
#else
      height = 350;
#endif // MSDOS
      okButton => (<-.name_ok && (<-.index > 0));
    };
    init_panel {
      visible => ((<-.name_ok == 0) || (<-.index == 0));
      height => (<-.all_atoms.y + <-.all_atoms.height + 5);
    };
    Info {
      text = "Please select the atom(s) to change\n"
	+ "and enter a name for the new model";
#ifdef MSDOS
      height => (2 * UIdata.UIfonts[0].lineHeight + 7);
      multiLine = 1;
#else
      height = 46;
#endif // MSDOS
    };
    initialise {
      v_commands = "index = 0; new_view = 0; name = \"\"; all = 0;";
    };
    int atom<NEportLevels=1,NEx=621.,NEy=162.> = 0;
    boolean all<NEportLevels=1,NEx=261.,NEy=324.> = 0;
    link nselections<NEportLevels=1,NEx=846.,NEy=207.>;
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle all_atoms<NEx=774.,NEy=423.> {
      parent => <-.init_panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.view.y + <-.view.height + 5);
      width => parent.clientWidth;
      label = "Apply to all atoms of selected type";
      set => <-.all;
    };
    CCP3.Core_Macros.UI.File_Ops.periodic_table table<NEx=387.,NEy=279.> {
      preferences => <-.preferences;
      UIpanel {
	parent => <-.<-.UIpanel;
	visible => (<-.<-.name_ok && (<-.<-.index > 0));
      };
      atom<NEportLevels={2,1}> => <-.atom;
    };
    CCP3.Core_Modules.Model.edit_atom edit_atom<NEx=180.,NEy=567.> {
      name => <-.name;
      nselect => <-.nselections;
      new_view => <-.new_view;
      all_atoms => <-.all;
      index => <-.index;
      atom => <-.atom;
      do_more => <-.Continue.do;
    };
    UIbutton Continue {
#ifdef MSDOS
      parent => <-.UIpanel;
      x => (parent.clientWidth - width) / 2;
      y => (parent.clientHeight - height - 5);
#else
      parent => <-.DLVdialog;
#endif // MSDOS
      label = "Continue Change";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => (<-.name_ok && (<-.index > 0));
      width = 120;
    };
    GMOD.parse_v reset {
      v_commands = "index = 0; name = \"\";";
      trigger => <-.Continue.do;
      on_inst = 0;
      relative => <-;
    };
  };
  baseUI atom_propsUI {
    DLVdialog {
      title = "Atom Properties";
      okButton => (<-.name_ok && (<-.index > 0));
      width = 400;
      height = 500;
    };
    CCP3.Core_Modules.Model.atom_data params<NEx=594.,NEy=154.>;
    boolean all = 0;
    link nselections<NEportLevels=1,NEx=846.,NEy=207.>;
    init_panel {
      visible => ((<-.name_ok == 0) || (<-.index == 0));
      height => (<-.all_atoms.y + <-.all_atoms.height + 5);
    };
    Info {
      text = "Please select the atom(s) to modify\n"
	+ "and enter a name for the new model";
#ifdef MSDOS
      height => (2 * UIdata.UIfonts[0].lineHeight + 7);
      multiLine = 1;
#else
      height = 46;
#endif // MSDOS
    };
    initialise {
      v_commands = "index = 0; new_view = 0; name = \"\"; all = 0;";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle all_atoms<NEx=846.,NEy=441.> {
      parent => <-.init_panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.view.y + <-.view.height + 5);
      width => parent.clientWidth;
      label = "Apply to all atoms of selected type";
      set => <-.all;
    };
    UIpanel props_panel {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
      visible => (<-.name_ok && (<-.index > 0));
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle need_charge<NEx=66.,NEy=363.> {
      parent => <-.props_panel;
      x = 0;
      y = 0;
      label = "set charge";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.params.set_charge;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider set_charge<NEx=253.,NEy=363.> {
      parent => <-.props_panel;
      value => <-.params.charge;
      min = -8;
      max = 8;
      title = "charge";
      x => (<-.need_charge.width + 5);
      y => <-.need_charge.y;
      //y => (<-.need_charge.y + <-.need_charge.height +5);
      width => (parent.clientWidth - x - 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => <-.params.set_charge;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle need_radius<NEx=66.,NEy=440.> {
      parent => <-.props_panel;
      x = 0;
      y => (<-.set_charge.y + <-.set_charge.height + 5);
      label = "set radius";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.params.set_radius;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider set_radius<NEx=253.,NEy=440.> {
      parent => <-.props_panel;
      value => <-.params.radius;
      min = 0.02;
      max = 3.0;
      title = "radius";
      x => (<-.need_radius.width + 5);
      y => <-.need_radius.y;
      width => (parent.clientWidth - x - 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => <-.params.set_radius;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle need_colour {
      parent => <-.props_panel;
      x = 0;
      y => (<-.set_radius.y + <-.set_radius.height + 5);
      label = "set colour";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.params.set_colour;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider set_r<NEx=253.,NEy=517.> {
      parent => <-.props_panel;
      value => <-.params.red;
      min = 0.0;
      max = 1.0;
      title = "red";
      x => (<-.need_colour.width + 5);
      y => <-.need_colour.y;
      width => (parent.clientWidth - x - 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => <-.params.set_colour;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider set_g<NEx=451.,NEy=517.> {
      parent => <-.props_panel;
      value => <-.params.green;
      min = 0.0;
      max = 1.0;
      title = "green";
      x => set_r.x;
      y => (<-.set_r.y + <-.set_r.height + 5);
      width => (parent.clientWidth - x - 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => <-.params.set_colour;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider set_b<NEx=649.,NEy=517.> {
      parent => <-.props_panel;
      value => <-.params.blue;
      min = 0.0;
      max = 1.0;
      title = "blue";
      x => set_r.x;
      y => (<-.set_g.y + <-.set_g.height + 5);
      width => (parent.clientWidth - x - 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => <-.params.set_colour;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle need_spin<NEx=243.,NEy=324.> {
      parent => <-.props_panel;
      y => (<-.set_b.y + <-.set_b.height + 5);
      label = "set spin";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.params.set_spin;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox set_spin<NEx=729.,NEy=585.> {
      parent => <-.props_panel;
      selectedItem => <-.params.spin;
      title => "spin";
      x => (<-.need_spin.x + <-.need_spin.width + 5);
      y => <-.need_spin.y;
      labels => { "None", "Alpha", "Beta" };
      width => (parent.clientWidth - x - 5);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      visible => <-.params.set_spin;
    };
    CCP3.Core_Modules.Model.edit_atom_props edit_props<NEx=414.,NEy=612.> {
      name => <-.name;
      nselect => <-.nselections;
      new_view => <-.new_view;
      index => <-.index;
      data => <-.params;
      all_atoms => <-.all;
   };
  };
  baseUI delete_atomUI {
    DLVdialog {
      title = "Delete Atom";
      okButton => (<-.name_ok && (<-.index > 0));
      okLabelString = "Done";
      width = 500;
    };
    UIpanel {
      visible => (<-.name_ok && (<-.index > 0));
    };
    init_panel {
      visible => ((<-.name_ok == 0) || (<-.index == 0));
    };
    initialise {
      v_commands = "index = 0; new_view = 0; name = \"\";";
    };
    link nselections<NEportLevels=1,NEx=846.,NEy=207.>;
    UIbutton Continue<NEx=360.,NEy=324.> {
#ifdef MSDOS
      parent => <-.UIpanel;
      x => (parent.clientWidth - width) / 2;
      y => (parent.clientHeight - height - 5);
#else
      parent => <-.DLVdialog;
#endif // MSDOS
      label = "Continue Deleting";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => (<-.name_ok && (<-.index > 0));
    };
    CCP3.Core_Modules.Model.delete_atom delete_atom<NEx=639.,NEy=558.> {
      name => <-.name;
      nselect => <-.nselections;
      new_view => <-.new_view;
      index => <-.index;
      do_more => <-.Continue.do;
    };
    GMOD.parse_v reset<NEx=252.,NEy=441.> {
      v_commands = "index = 0; name = \"\";";
      trigger => <-.Continue.do;
      on_inst = 0;
      relative => <-;
    };
  };
  baseUI latticeUI {
    DLVdialog {
      title = "Lattice";
      height = 300;
    };
    CCP3.Core_Modules.Model.LatticeParams &params<NEx=308.,NEy=77.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein a<NEx=121.,NEy=176.> {
      UIparent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      flabel = "a";
      fval => params.a;
      fmin = 0.5;
      y = 5;
      width => UIparent.clientWidth;
      panel {
	visible => (<-.<-.name_ok && <-.<-.params.use_a);
      };
      field {
	decimalPoints = 5;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein b {
      UIparent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      flabel = "b";
      fval => params.b;
      fmin = 0.5;
      y => ((a.y + a.height) + 5);
      width => UIparent.clientWidth;
      panel {
	visible => (<-.<-.name_ok && <-.<-.params.use_b);
      };
      field {
	decimalPoints = 5;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein c {
      UIparent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      flabel = "c";
      fval => params.c;
      fmin = 0.5;
      y => ((b.y + b.height) + 5);
      width => UIparent.clientWidth;
      panel {
	visible => (<-.<-.name_ok && <-.<-.params.use_c);
      };
      field {
	decimalPoints = 5;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein alpha {
      UIparent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      flabel = "alpha";
      fval => params.alpha;
      fmin = 1.0;
      fmax = 179.0;
      y => ((c.y + c.height) + 5);
      width => UIparent.clientWidth;
      panel {
	visible => (<-.<-.name_ok && <-.<-.params.use_alpha);
      };
      field {
	decimalPoints = 3;
      };					      
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein beta {
      UIparent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      flabel = "beta";
      fval => params.beta;
      fmin = 1.0;
      fmax = 179.0;
      y => ((alpha.y + alpha.height) + 5);
      width => UIparent.clientWidth;
      panel {
	visible => (<-.<-.name_ok && <-.<-.params.use_beta);
      };
      field {
	decimalPoints = 3;
      }; 
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein gamma {
      UIparent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      flabel = "gamma";
      fval => params.gamma;
      fmin = 1.0;
      fmax = 179.0;
      y => ((beta.y + beta.height) + 5);
      width => UIparent.clientWidth;
      panel {
	visible => (<-.<-.name_ok && <-.<-.params.use_gamma);
      };
      field {
	decimalPoints = 3;
      };
    };
    initialise {
      v_commands = "index = 0; new_view = 0; name = \"\";";
    };
    CCP3.Core_Modules.Model.edit_lattice edit_lattice<NEx=275.,NEy=550.> {
      name => <-.name;
      new_view => <-.new_view;
      index => <-.index;
      params => <-.params;
    };
  };
  baseUI vacuum_gapUI {
    DLVdialog {
      title = "Insert Vacuum Gap";
    };
    CCP3.Core_Modules.Model.VacuumParams &params<NEx=308.,NEy=77.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein c_axis<NEx=209.,NEy=264.> {
      UIparent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      flabel = "c axis length";
      fval => params.length;
      fmin = 1.0;
      y = 5;
      width => UIparent.clientWidth;
      panel {
	visible => <-.<-.name_ok;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein shift<NEx=429.,NEy=264.> {
      UIparent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      flabel => "slab position";
      fval => params.position;
      y => ((c_axis.y + c_axis.height) + 5);
      width => UIparent.clientWidth;
      panel {
	visible => <-.<-.name_ok;
      };
    };
    initialise {
      v_commands = "index = 0; new_view = 0; name = \"\";";
    };
    CCP3.Core_Modules.Model.vacuum_gap vacuum_gap<NEx=275.,NEy=550.> {
      name => <-.name;
      new_view => <-.new_view;
      index => <-.index;
      params => <-.params;
    };
  };
  baseUI delete_symUI {
    DLVdialog {
      title = "Delete Symmetry";
    };
    initialise {
      v_commands = "index = 0; new_view = 0; name = \"\";";
    };
    CCP3.Core_Modules.Model.del_symmetry del_sym<NEx=275.,NEy=550.> {
      name => <-.name;
      new_view => <-.new_view;
      index => <-.index;
    };
  };
  baseUI cut_clusterUI {
    DLVdialog {
      title = "Cut Cluster";
      width = 300;
      height = 250;
      okButton => (<-.name_ok && (<-.index > 0));
    };
    CCP3.Core_Modules.Model.ClusterParams &params<NEx=308.,NEy=77.,
      NEportLevels={2,0}>;
    int nselections<NEportLevels={2,1},NEx=638.,NEy=165.>;
    init_panel {
      visible => ((<-.name_ok == 0) || (<-.index == 0));
    };
    Info {
      text = "Please select the central atom for the cluster\n"
	+ "and enter a name for the new model";
#ifdef MSDOS
      height => (2 * UIdata.UIfonts[0].lineHeight + 7);
      multiLine = 1;
#else
      height = 46;
#endif // MSDOS
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider neighbours<NEx=187.,NEy=253.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      min = 1;
      max = 25;
      value => <-.params.neighbours;
      y = 5;
      visible => ((<-.name_ok > 0) && (<-.index > 0));
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider radius<NEx=407.,NEy=253.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      min = 1.0;
      max = 25.0;
      value => <-.params.radius;
      y => (<-.neighbours.y + <-.neighbours.height + 5);
      visible => ((<-.name_ok > 0) && (<-.index > 0));
    };
    initialise {
      v_commands = "index = 0; new_view = 0; name = \"\";";
    };
    CCP3.Core_Modules.Model.cut_cluster cut_cluster<NEx=275.,NEy=550.> {
      name => <-.name;
      nselect => <-.nselections;
      new_view => <-.new_view;
      index => <-.index;
      params => <-.params;
    };
  };
  baseUI shift_originUI {
    DLVdialog {
      title = "Shift Origin";
      width = 300;
      height = 350;
    };
    CCP3.Core_Modules.Model.OriginParams &params<NEx=308.,NEy=77.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein x<NEx=121.,NEy=176.> {
      UIparent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      flabel = "x";
      fval => params.x;
      y = 5;
      width => UIparent.clientWidth;
      panel {
	visible => <-.<-.name_ok;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein y<NEx=121.,NEy=276.> {
      UIparent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      flabel = "y";
      fval => params.y;
      y => ((<-.x.y + <-.x.height) + 5);
      width => UIparent.clientWidth;
      panel {
	visible => <-.<-.name_ok;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein z<NEx=121.,NEy=376.> {
      UIparent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      flabel = "z";
      fval => params.z;
      y => ((<-.y.y + <-.y.height) + 5);
      width => UIparent.clientWidth;
      panel {
	visible => <-.<-.name_ok;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle DLVtoggle<NEx=132.,NEy=484.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.params.fractional;
      label = "Fractional shifts";
      width => parent.clientWidth;
      y => ((<-.z.y + <-.z.height) + 5);
      visible => <-.name_ok;
    };
    UIbutton atoms<NEx=429.,NEy=286.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label = "Update from selected atoms";
      y => ((<-.DLVtoggle.y + <-.DLVtoggle.height) + 5);
      x => (parent.clientWidth / 2) - 100;
      width = 200;
      visible => <-.name_ok;
      do = 0;
    };
    UIbutton symm<NEx=429.,NEy=352.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label = "Minimise translations";
      y => ((<-.atoms.y + <-.atoms.height) + 5);
      x => (parent.clientWidth / 2) - 100;
      width = 200;
      visible => <-.name_ok;
      do = 0;
    };
    initialise {
      v_commands = "index = 0; new_view = 0; name = \"\";";
    };
    CCP3.Core_Modules.Model.shift_origin shift_origin<NEx=275.,NEy=550.> {
      name => <-.name;
      new_view => <-.new_view;
      index => <-.index;
      params => <-.params;
      do_selection => <-.atoms.do;
      do_symmetry => <-.symm.do;
    };
  };
  baseUI view_moleculeUI {
    DLVdialog {
      title = "View to Molecule";
    };
    initialise {
      v_commands = "index = 0; new_view = 0; name = \"\";";
    };
    CCP3.Core_Modules.Model.to_molecule molecule<NEx=275.,NEy=550.> {
      name => <-.name;
      new_view => <-.new_view;
      index => <-.index;
    };
  };
  baseUI groupUI {
    link nselections<NEportLevels=1,NEx=846.,NEy=207.>;
    string gpname<NEportLevels=1> = "";
    boolean others<NEportLevels=1> = 0;
    string bulk_label = "Remainder";
    DLVdialog {
      width = 300;
      title = "Group Atoms";
      okButton => (strlen(gpname) > 0) && (nselections > 0);
    };
    view {
      visible = 0;
    };
    Info {
      text = "Please enter a label for the atom group";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext NameGP {
      y = 0;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      parent => <-.UIpanel;
      text => <-.gpname;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle all_others {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y => (<-.NameGP.y + <-.NameGP.height + 5);
      width => parent.clientWidth;
      label = "Label all other atoms";
      set => <-.others;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext bulk {
      y => (<-.all_others.y + <-.all_others.height + 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      parent => <-.UIpanel;
      text => <-.bulk_label;
      visible => <-.others;
    };
    GMOD.parse_v initialise<NEx=814.,NEy=319.> {
      relative => <-;
      v_commands = "name = \"\"; gpname = \"\"; bulk_label = \"Remainder\";"
	+ " index = 0; others = 0;";
    };
    GMOD.parse_v action {
      relative => <-;
      v_commands = "index = 3;";
      trigger => name;
      on_inst = 0;
    };
  };
  baseUI regionUI {
    DLVdialog {
      width = 300;
      height = 400;
      title = "Define Cluster region";
      //okButton = 1;
    };
    view {
      visible = 0;
    };
    Info {
      text = "Please select a name for the cluster partitioning";
    };
    group data<NEx=657.,NEy=72.> {
      int nregions<NEportLevels=1> = 2;
      group regions[nregions] {
	float radius<NEportLevels=1> = 0.0;
      };
    };
    int current<NEportLevels=1,NEx=936.,NEy=216.> = 0;
    int array<NEportLevels=1,NEx=846.,NEy=99.>[] =>
      init_array((data.nregions - 1),1,(data.nregions - 1));
    string string<NEportLevels=1,NEx=729.,NEy=162.>[] =>
      str_format("region %d",array);
    CCP3.Core_Macros.UI.UIobjs.DLViSlider regions<NEx=504.,NEy=261.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      value => <-.data.nregions;
      min = 2.;
      max = 6.;
      x = 0;
      y = 0;
      width => parent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox select_region<NEx=738.,NEy=261.> {
      parent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      labels => <-.string;
      selectedItem => <-.current;
      y => (<-.regions.y + <-.regions.height + 5);
      width => parent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider radius<NEx=378.,NEy=360.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      value => <-.data.regions[(<-.current + 1)].radius;
      min => <-.data.regions[<-.current].radius;
      max = 30.;
      y => (<-.select_region.y + <-.select_region.height + 5);
      width => parent.clientWidth;
    };
    GMOD.parse_v initialise<NEx=814.,NEy=319.> {
      relative => <-;
      v_commands = "name = \"\"; index = 0;";
    };
    GMOD.parse_v action {
      relative => <-;
      v_commands = "index = 3; data.regions.radius = 0.0;";
      trigger => name;
      on_inst = 0;
    };
  };
  baseUI wulffUI {
    DLVdialog {
      title = "Wulff Crystallite";
      width = 300;
      height = 250;
      okButton => (<-.name_ok && (<-.index > 0));
    };
    CCP3.Core_Modules.Model.WulffParams &params<NEx=308.,NEy=77.,
      NEportLevels={2,0}>;
    init_panel {
      visible => ((<-.name_ok == 0) || (<-.index == 0));
      height => (<-.plots.y + <-.plots.height + 5);
    };
    Info {
      text = "Please select the wulff plot to generate from\n"
	+ "and enter a name for the new model";
#ifdef MSDOS
      height => (2 * UIdata.UIfonts[0].lineHeight + 7);
      multiLine = 1;
#else
      height = 46;
#endif // MSDOS
    };
    int model;
    CCP3.Core_Modules.Objects.list_wulff list_wulff<NEx=531.,NEy=297.> {
      trigger => <-.visible;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist plots<NEx=711.,NEy=558.> {
      parent => <-.init_panel;
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y => (<-.view.y + <-.view.height + 5);
      selectedItem => <-.model;
      strings => <-.list_wulff.labels;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider size<NEx=407.,NEy=253.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => ((parent.clientWidth * 11) / 12);
      min = 5.0;
      max = 250.0;
      value => <-.params.max_dim;
      y = 5;
      visible => ((<-.name_ok > 0) && (<-.index > 0));
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein size_typein {
      slider => <-.size;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      valEditor {
	UI {
	  editor_shell {
	    x => ;
	    y => ;
	  };
	};
      };
    };
    initialise {
      v_commands = "index = 0; new_view = 0; model = ; name = \"\";";
    };
    CCP3.Core_Modules.Model.wulff wulff<NEx=275.,NEy=550.> {
      name => <-.name;
      new_view => <-.new_view;
      index => <-.index;
      selection => <-.model;
      params => <-.params;
    };
  };
  baseUI multilayerUI {
    DLVdialog {
      title = "Create multilayer";
      width = 300;
      height = 250;
    };
    init_panel {
      height => (<-.slabs.y + <-.slabs.height + 5);
    };
    CCP3.Core_Modules.Model.list_models list_models<NEx=531.,NEy=297.> {
      trigger => <-.visible;
      model_type = 2;
    };
    int model;
    CCP3.Core_Macros.UI.UIobjs.DLVlist slabs<NEx=711.,NEy=558.> {
      parent => <-.init_panel;
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y => (<-.view.y + <-.view.height + 5);
      selectedItem => <-.model;
      strings => <-.list_models.labels;
    };
    float distance = 2.0;
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein space<NEx=209.,NEy=264.> {
      UIparent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      flabel = "slab spacing";
      fval => <-.distance;
      fmin = 0.1;
      fmax = 10.0;
      width => UIparent.clientWidth;
      y = 5;
      panel {
	visible => <-.<-.name_ok;
      };
    };
    initialise {
      v_commands = "index = 0; new_view = 0; name = \"\";";
    };
    CCP3.Core_Modules.Model.multilayer multilayer<NEx=275.,NEy=550.> {
      name => <-.name;
      new_view => <-.new_view;
      index => <-.index;
      selection => <-.model;
      space => <-.distance;
    };
  };
#ifdef DLV_DL
  baseUI fill_geometryUI {
    DLVdialog {
      title = "Fill Shape";
      width = 300;
      height = 250;
    };
    init_panel {
      height => (<-.shapes.y + <-.shapes.height + 5);
    };
    CCP3.Core_Modules.Model.list_models list_models<NEx=531.,NEy=297.> {
      trigger => <-.visible;
      model_type = 8;
    };
    int model;
    CCP3.Core_Macros.UI.UIobjs.DLVlist shapes<NEx=711.,NEy=558.> {
      parent => <-.init_panel;
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y => (<-.view.y + <-.view.height + 5);
      selectedItem => <-.model;
      strings => <-.list_models.labels;
    };
    //CCP3.Core_Modules.Model.VacuumParams &params<NEx=308.,NEy=77.,
    // NEportLevels={2,0}>;
    /*
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein space<NEx=209.,NEy=264.> {
      UIparent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      flabel = "slab spacing";
      //fval => params.length;
      fmin = 1.0;
      y = 5;
      width => UIparent.clientWidth;
      panel {
	visible => <-.<-.name_ok;
      };
    };
    */
    initialise {
      v_commands = "index = 0; new_view = 0; name = \"\";";
    };
    CCP3.Core_Modules.Model.fill_geometry fill_geometry<NEx=275.,NEy=550.> {
      name => <-.name;
      new_view => <-.new_view;
      index => <-.index;
      selection => <-.model;
      //space => <-.distance;
    };
  };
#endif // DLV_DL
  macro wrapper {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=198.,NEy=55.>;
    link active_menu<NEportLevels={2,1},NEx=440.,NEy=55.>;
    int visible<NEportLevels=1,NEx=671.,NEy=55.> = 0;
    link UIparent<NEportLevels={2,1},NEx=858.,NEy=55.>;
    int ok<NEportLevels=1,NEx=231.,NEy=209.> = 0;
    int cancel<NEportLevels=1,NEx=528.,NEy=220.> = 0;
    UIcmd UIcmd<NEx=198.,NEy=121.> {
      //label = "Load Model";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      active => <-.active_menu;
      do => <-.visible;
    };
    GMOD.instancer instancer<NEx=517.,NEy=165.> {
      Value => <-.visible;
    };
    CCP3.Core_Modules.Model.edit_model complete_model<NEx=275.,NEy=550.> {
      trigger => <-.ok;
    };
    CCP3.Core_Modules.Model.cancel_model cancel_model<NEx=605.,NEy=550.> {
      trigger => <-.cancel;
    };
  };
  wrapper CutSurface {
    UIcmd {
      label = "Cut Surface";
    };
    instancer {
      Group => cut_surfaceUI;
    };
    cut_surfaceUI cut_surfaceUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      UIparent => <-.UIparent;
      visible => <-.visible;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
    };
    cancel_model {
      index => <-.cut_surfaceUI.index;
      new_view => <-.cut_surfaceUI.new_view;
    };
  };
  wrapper CutSlab {
    UIcmd {
      label = "Cut Slab";
    };
    instancer {
      Group => cut_slabUI;
    };
    cut_slabUI cut_slabUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      UIparent => <-.UIparent;
      visible => <-.visible;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
    };
    cancel_model {
      index => <-.cut_slabUI.index;
      new_view => <-.cut_slabUI.new_view;
    };
  };
  wrapper Salvage {
    UIcmd {
      label = "Create Salvage";
    };
    instancer {
      Group => salvageUI;
    };
    salvageUI salvageUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      UIparent => <-.UIparent;
      visible => <-.visible;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
    };
    cancel_model {
      index => <-.salvageUI.index;
      new_view => <-.salvageUI.new_view;
    };
  };
  wrapper Supercell {
    UIcmd {
      label = "Supercell";
    };
    link model_type<NEportLevels=1>;
    CCP3.Core_Modules.Model.SuperCellParams params<NEportLevels={0,1}> {
      s11 = 1;
      s12 = 0;
      s13 = 0;
      s21 = 0;
      s22 = 1;
      s23 = 0;
      s31 = 0;
      s32 = 0;
      s33 = 1;
      conventional = 0;
    };
    instancer {
      Group => supercellUI;
    };
    supercellUI supercellUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      model_type => <-.model_type;
      params => <-.params;
      UIparent => <-.UIparent;
      visible => <-.visible;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
    };
    cancel_model {
      index => <-.supercellUI.index;
      new_view => <-.supercellUI.new_view;
    };
  };
  wrapper InsertModel {
    UIcmd {
      label = "Insert Model";
    };
    instancer {
      Group => insert_modelUI;
    };
    link model_type<NEportLevels=1>;
    link nselections<NEportLevels=1,NEx=747.,NEy=162.>;
    CCP3.Core_Modules.Model.move_params params<NEx=774.,NEy=171.,
      NEportLevels={0,1}> {
      fractional = 0;
      x = 0.0;
      y = 0.0;
      z = 0.0;
      use_transform = 0;
    };
    insert_modelUI insert_modelUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      UIparent => <-.UIparent;
      visible => <-.visible;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
      params => <-.params;
      model_type => <-.model_type;
      nselections => <-.nselections;
    };
    cancel_model {
      index => <-.insert_modelUI.index;
      new_view => <-.insert_modelUI.new_view;
    };
  };
  wrapper InsertAtom {
    UIcmd {
      label = "Insert Atom";
    };
    link model_type<NEportLevels=1>;
    link nselections<NEportLevels=1,NEx=747.,NEy=162.>;
    instancer {
      Group => insert_atomUI;
    };
    CCP3.Core_Modules.Model.move_params params<NEx=774.,NEy=171.,
      NEportLevels={0,1}> {
      fractional = 0;
      x = 0.0;
      y = 0.0;
      z = 0.0;
      use_transform = 0;
    };
    insert_atomUI insert_atomUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      UIparent => <-.UIparent;
      visible => <-.visible;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
      model_type => <-.model_type;
      params => <-.params;
    };
    cancel_model {
      index => <-.insert_atomUI.index;
      new_view => <-.insert_atomUI.new_view;
    };
  };
  wrapper MoveAtom {
    UIcmd {
      label = "Move Atom";
    };
    instancer {
      Group => move_atomUI;
    };
    link model_type<NEportLevels=1>;
    link nselections<NEportLevels=1,NEx=747.,NEy=162.>;
    CCP3.Core_Modules.Model.move_params params<NEx=774.,NEy=171.,
      NEportLevels={0,1}> {
      fractional = 0;
      x = 0.0;
      y = 0.0;
      z = 0.0;
      use_transform = 0;
      displacements = 0;
    };
    move_atomUI move_atomUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      UIparent => <-.UIparent;
      visible => <-.visible;
      nselections => <-.nselections;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
      params => <-.params;
      model_type => <-.model_type;
    };
    cancel_model {
      index => <-.move_atomUI.index;
      new_view => <-.move_atomUI.new_view;
    };
  };
  wrapper ChangeAtom {
    UIcmd {
      label = "Change Atom";
    };
    instancer {
      Group => change_atomUI;
    };
    link nselections<NEportLevels=1,NEx=747.,NEy=162.>;
    change_atomUI change_atomUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      UIparent => <-.UIparent;
      visible => <-.visible;
      nselections => <-.nselections;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
    };
    cancel_model {
      index => <-.change_atomUI.index;
      new_view => <-.change_atomUI.new_view;
    };
  };
  wrapper AtomProps {
    UIcmd {
      label = "Atom Properties";
    };
    instancer {
      Group => atom_propsUI;
    };
    link nselections<NEportLevels=1,NEx=747.,NEy=162.>;
    atom_propsUI atom_propsUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      UIparent => <-.UIparent;
      visible => <-.visible;
      nselections => <-.nselections;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
    };
    cancel_model {
      index => <-.atom_propsUI.index;
      new_view => <-.atom_propsUI.new_view;
    };
  };
  wrapper DeleteAtom {
    UIcmd {
      label = "Delete Atom";
    };
    instancer {
      Group => delete_atomUI;
    };
    link nselections<NEportLevels=1,NEx=747.,NEy=162.>;
    delete_atomUI delete_atomUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      UIparent => <-.UIparent;
      visible => <-.visible;
      nselections => <-.nselections;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
    };
    cancel_model {
      index => <-.delete_atomUI.index;
      new_view => <-.delete_atomUI.new_view;
    };
  };
  wrapper Lattice {
    UIcmd {
      label = "Lattice";
    };
    link model_type<NEportLevels=1>;
    CCP3.Core_Modules.Model.LatticeParams params<NEportLevels={2,0}> {
      a = 1.0;
      b = 1.0;
      c = 1.0;
      alpha = 90.0;
      beta = 90.0;
      gamma = 90.0;
      use_a = 1;
      use_b = 1;
      use_c = 1;
      use_alpha = 1;
      use_beta = 1;
      use_gamma = 1;
    };
    instancer {
      Group => latticeUI;
    };
    latticeUI latticeUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      params => <-.params;
      UIparent => <-.UIparent;
      visible => <-.visible;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
    };
    cancel_model {
      index => <-.latticeUI.index;
      new_view => <-.latticeUI.new_view;
    };
  };
  wrapper Vacuum {
    UIcmd {
      label = "Slab to 3D";
    };
    link model_type<NEportLevels=1>;
    CCP3.Core_Modules.Model.VacuumParams params<NEportLevels={2,0}> {
      length = 20.0;
      position = 0.0;
    };
    instancer {
      Group => vacuum_gapUI;
    };
    vacuum_gapUI vacuum_gapUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      params => <-.params;
      UIparent => <-.UIparent;
      visible => <-.visible;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
    };
    cancel_model {
      index => <-.vacuum_gapUI.index;
      new_view => <-.vacuum_gapUI.new_view;
    };
  };
  wrapper Delete_Sym {
    UIcmd {
      label = "Delete Symmetry";
    };
    instancer {
      Group => delete_symUI;
    };
    delete_symUI delete_symUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      UIparent => <-.UIparent;
      visible => <-.visible;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
    };
    cancel_model {
      index => <-.delete_symUI.index;
      new_view => <-.delete_symUI.new_view;
    };
  };
  wrapper Cut_Cluster {
    UIcmd {
      label = "Cut Cluster";
    };
    CCP3.Core_Modules.Model.ClusterParams params<NEportLevels={2,0}> {
      neighbours = 1;
      radius = 5.0;
    };
    instancer {
      Group => cut_clusterUI;
    };
    link nselections<NEportLevels={2,1},NEx=748.,NEy=165.>;
    cut_clusterUI cut_clusterUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      params => <-.params;
      UIparent => <-.UIparent;
      visible => <-.visible;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
      nselections => <-.nselections;
    };
    cancel_model {
      index => <-.cut_clusterUI.index;
      new_view => <-.cut_clusterUI.new_view;
    };
  };
  wrapper Shift_Origin {
    UIcmd {
      label = "Shift Origin";
    };
    link model_type<NEportLevels=1>;
    CCP3.Core_Modules.Model.OriginParams params<NEportLevels={2,0}> {
      x = 0.0;
      y = 0.0;
      z = 0.0;
      fractional = 0;
    };
    instancer {
      Group => shift_originUI;
    };
    shift_originUI shift_originUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      params => <-.params;
      UIparent => <-.UIparent;
      visible => <-.visible;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
    };
    cancel_model {
      index => <-.shift_originUI.index;
      new_view => <-.shift_originUI.new_view;
    };
  };
  wrapper Molecule_View {
    UIcmd {
      label = "View to Molecule";
    };
    link model_type<NEportLevels=1>;
    instancer {
      Group => view_moleculeUI;
    };
    view_moleculeUI view_moleculeUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      UIparent => <-.UIparent;
      visible => <-.visible;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
    };
    cancel_model {
      index => <-.view_moleculeUI.index;
      new_view => <-.view_moleculeUI.new_view;
    };
  };
  macro Atom_Group {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=198.,NEy=55.>;
    link active_menu<NEportLevels={2,1},NEx=440.,NEy=55.>;
    int visible<NEportLevels=1,NEx=671.,NEy=55.> = 0;
    link UIparent<NEportLevels={2,1},NEx=858.,NEy=55.>;
    link nselections<NEportLevels={2,1},NEx=748.,NEy=165.>;
    int ok<NEportLevels=1,NEx=231.,NEy=209.> = 0;
    int cancel<NEportLevels=1,NEx=528.,NEy=220.> = 0;
    UIcmd UIcmd<NEx=198.,NEy=121.> {
      label = "Group atoms";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      active => <-.active_menu;
      do => <-.visible;
    };
    GMOD.instancer instancer<NEx=517.,NEy=165.> {
      Value => <-.visible;
      Group => groupUI;
    };
    groupUI groupUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      UIparent => <-.UIparent;
      visible => <-.visible;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
      nselections => <-.nselections;
    };
    CCP3.Core_Modules.Model.add_atom_group add_atom_group<NEx=275.,NEy=550.> {
      trigger => <-.ok;
      label => <-.groupUI.name;
      name => <-.groupUI.gpname;
      all_atoms => <-.groupUI.others;
      bulk => <-.groupUI.bulk_label;
    };
  };
  wrapper Wulff_Crystal {
    UIcmd {
      label = "Wulff Crystal";
      active => <-.active_menu && (<-.model_type == 3);
    };
    link model_type<NEportLevels=1>;
    CCP3.Core_Modules.Model.WulffParams params<NEportLevels={2,0}> {
      max_dim = 20.0;
    };
    instancer {
      Group => wulffUI;
    };
    wulffUI wulffUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      params => <-.params;
      UIparent => <-.UIparent;
      visible => <-.visible;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
    };
    cancel_model {
      index => <-.wulffUI.index;
      new_view => <-.wulffUI.new_view;
    };
  };
  wrapper Multilayer {
    UIcmd {
      label = "Multilayer";
    };
    link model_type<NEportLevels=1>;
    instancer {
      Group => multilayerUI;
    };
    multilayerUI multilayerUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      UIparent => <-.UIparent;
      visible => <-.visible;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
    };
    cancel_model {
      index => <-.multilayerUI.index;
      new_view => <-.multilayerUI.new_view;
    };
  };
#ifdef DLV_DL
  wrapper Fill_Shape {
    UIcmd {
      label = "Fill Geometry";
    };
    link model_type<NEportLevels=1>;
    //CCP3.Core_Modules.Model.VacuumParams params<NEportLevels={2,0}> {
    //  length = 0.0;
    //  position = 1.0;
    //};
    instancer {
      Group => fill_geometryUI;
    };
    fill_geometryUI fill_geometryUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      //params => <-.params;
      UIparent => <-.UIparent;
      visible => <-.visible;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
    };
    cancel_model {
      index => <-.fill_geometryUI.index;
      new_view => <-.fill_geometryUI.new_view;
    };
  };
#endif // DLV_DL
  macro Cluster_Group {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=198.,NEy=55.>;
    link active_menu<NEportLevels={2,1},NEx=440.,NEy=55.>;
    int visible<NEportLevels=1,NEx=671.,NEy=55.> = 0;
    link UIparent<NEportLevels={2,1},NEx=858.,NEy=55.>;
    int ok<NEportLevels=1,NEx=231.,NEy=209.> = 0;
    int cancel<NEportLevels=1,NEx=528.,NEy=220.> = 0;
    link model_type<NEportLevels=1>;
    UIcmd UIcmd<NEx=198.,NEy=121.> {
      label = "Cluster Regions";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      active => (<-.active_menu && <-.model_type == 0);
      do => <-.visible;
    };
    GMOD.instancer instancer<NEx=517.,NEy=165.> {
      Value => <-.visible;
      Group => regionUI;
    };
    regionUI regionUI<NEx=363.,NEy=286.,instanced=0> {
      preferences => <-.preferences;
      UIparent => <-.UIparent;
      visible => <-.visible;
      DLVdialog.ok => <-.<-.ok;
      DLVdialog.cancel => <-.<-.cancel;
    };
    CCP3.Core_Modules.Model.add_cluster_region add<NEx=275.,NEy=550.> {
      radii => <-.regionUI.data.regions.radius;
      n => <-.regionUI.data.nregions;
      label => <-.regionUI.name;
      ok => <-.ok;
      remove => <-.cancel;
    };
  };
  macro menu_edit {
    UIcmdList Structure<NEx=143.,NEy=198.> {
      cmdList => {
	<-.CutSurface.UIcmd,
	<-.CutSlab.UIcmd,
	<-.Salvage.UIcmd,
	<-.Supercell.UIcmd,
	<-.Vacuum.UIcmd,
	<-.InsertModel.UIcmd,
	<-.InsertAtom.UIcmd,
	<-.MoveAtom.UIcmd,
	<-.DeleteAtom.UIcmd,
	<-.Lattice.UIcmd,
	<-.Shift_Origin.UIcmd,
	<-.Delete_Sym.UIcmd,
	<-.Cut_Cluster.UIcmd,
	<-.Molecule_View.UIcmd,
	<-.ChangeAtom.UIcmd,
	<-.Wulff_Crystal.UIcmd,
	<-.Multilayer.UIcmd,
#ifdef DLV_DL
	<-.Fill_Shape.UIcmd,
#endif // DLV_DL
	<-.AtomProps.UIcmd,
	<-.Atom_Group.UIcmd,
	<-.Cluster_Group.UIcmd
      };
    };
    int model_type<NEportLevels=1,NEx=440.,NEy=121.>;
    link preferences<NEportLevels={2,1},NEx=627.,NEy=66.>;
    link active_menu<NEportLevels={2,1},NEx=143.,NEy=55.>;
    int dialog_visible<NEportLevels=1,NEx=396.,NEy=55.> =>
      ((CutSlab.visible == 1) || (Supercell.visible == 1) ||
       (InsertAtom.visible == 1) || (MoveAtom.visible == 1) ||
       (DeleteAtom.visible == 1) || (AtomProps.visible == 1) ||
       (Vacuum.visible == 1) || (Delete_Sym.visible == 1) ||
       (Cut_Cluster.visible == 1) || (Shift_Origin.visible == 1) ||
       (CutSurface.visible == 1) || (ChangeAtom.visible == 1) ||
       (Atom_Group.visible == 1) || (Salvage.visible == 1) ||
       (Molecule_View.visible == 1) || (Lattice.visible == 1) ||
       (InsertModel.visible == 1) || (Cluster_Group.visible == 1) ||
       (Wulff_Crystal.visible == 1) || (Multilayer.visible == 1)
#ifdef DLV_DL
       || (Fill_Shape.visible == 1)
#endif // DLV_DL
       );
    link UIparent<NEportLevels={2,1},NEx=825.,NEy=66.>;
    link nselections<NEportLevels=1,NEx=451.,NEy=198.>;
    CCP3.Core_Macros.UI.EditModel.CutSlab CutSlab<NEx=154.,NEy=330.> {
      preferences => <-.preferences;
      active_menu => (<-.active_menu && (model_type == 3 || model_type == 4));
      UIparent => <-.UIparent;
    };
    CCP3.Core_Macros.UI.EditModel.Supercell Supercell<NEx=374.,NEy=330.> {
      preferences => <-.preferences;
      active_menu => (<-.active_menu && (model_type > 0));
      UIparent => <-.UIparent;
      model_type => <-.model_type;
    };
    CCP3.Core_Macros.UI.EditModel.InsertAtom InsertAtom<NEx=154.,NEy=418.> {
      preferences => <-.preferences;
      active_menu => <-.active_menu;
      UIparent => <-.UIparent;
      model_type => <-.model_type;
      nselections => <-.nselections;
    };
    CCP3.Core_Macros.UI.EditModel.MoveAtom MoveAtom<NEx=385.,NEy=418.> {
      preferences => <-.preferences;
      active_menu => <-.active_menu;
      UIparent => <-.UIparent;
      model_type => <-.model_type;
      nselections => <-.nselections;
    };
    CCP3.Core_Macros.UI.EditModel.DeleteAtom DeleteAtom<NEx=594.,NEy=418.> {
      preferences => <-.preferences;
      active_menu => <-.active_menu;
      UIparent => <-.UIparent;
      nselections => <-.nselections;
    };
    CCP3.Core_Macros.UI.EditModel.AtomProps AtomProps<NEx=792.,NEy=418.> {
      preferences => <-.preferences;
      active_menu => <-.active_menu;
      UIparent => <-.UIparent;
      nselections => <-.nselections;
    };
    CCP3.Core_Macros.UI.EditModel.Lattice Lattice<NEx=594.,NEy=341.> {
      preferences => <-.preferences;
      active_menu => (<-.active_menu && (model_type > 0));
      UIparent => <-.UIparent;
      model_type => <-.model_type;
    };
    CCP3.Core_Macros.UI.EditModel.Vacuum Vacuum<NEx=154.,NEy=495.> {
      preferences => <-.preferences;
      active_menu => (<-.active_menu && (model_type == 2));
      UIparent => <-.UIparent;
      model_type => <-.model_type;
    };
    CCP3.Core_Macros.UI.EditModel.Delete_Sym Delete_Sym<NEx=385.,NEy=495.> {
      preferences => <-.preferences;
      active_menu => <-.active_menu;
      UIparent => <-.UIparent;
    };
    CCP3.Core_Macros.UI.EditModel.Cut_Cluster Cut_Cluster<NEx=594.,NEy=495.> {
      preferences => <-.preferences;
      active_menu => <-.active_menu;
      UIparent => <-.UIparent;
      nselections => <-.nselections;
    };
    CCP3.Core_Macros.UI.EditModel.Shift_Origin Shift_Origin<NEx=792.,NEy=495.>{
      preferences => <-.preferences;
      active_menu => <-.active_menu;
      UIparent => <-.UIparent;
      model_type => <-.model_type;
    };
    CCP3.Core_Macros.UI.EditModel.Molecule_View Molecule_View<NEx=803.,
      NEy=330.> {
      preferences => <-.preferences;
      active_menu => (<-.active_menu && (model_type > 0));
      UIparent => <-.UIparent;
      model_type => <-.model_type;
    };
    CCP3.Core_Macros.UI.EditModel.ChangeAtom ChangeAtom<NEx=387.,NEy=567.> {
      preferences => <-.preferences;
      active_menu => <-.active_menu;
      UIparent => <-.UIparent;
      nselections => <-.nselections;
    };
    CCP3.Core_Macros.UI.EditModel.CutSurface CutSurface<NEx=153.,NEy=567.> {
      preferences => <-.preferences;
      active_menu => (<-.active_menu && (model_type == 3));
      UIparent => <-.UIparent;
    };
    CCP3.Core_Macros.UI.EditModel.Atom_Group Atom_Group<NEx=792.,NEy=567.> {
      preferences => <-.preferences;
      active_menu => <-.active_menu;
      UIparent => <-.UIparent;
      nselections => <-.nselections;
    };
    CCP3.Core_Macros.UI.EditModel.Salvage Salvage<NEx=603.,NEy=567.> {
      preferences => <-.preferences;
      active_menu => (<-.active_menu && (model_type == 4));
      UIparent => <-.UIparent;
    };
    CCP3.Core_Macros.UI.EditModel.InsertModel InsertModel<NEx=153.,NEy=630.> {
      preferences => <-.preferences;
      active_menu => <-.active_menu;
      UIparent => <-.UIparent;
      model_type => <-.model_type;
      nselections => <-.nselections;
    };
    CCP3.Core_Macros.UI.EditModel.Cluster_Group Cluster_Group<NEx=387.,
      NEy=630.> {
      preferences => <-.preferences;
      active_menu => <-.active_menu;
      UIparent => <-.UIparent;
      model_type => <-.model_type;
    };
    CCP3.Core_Macros.UI.EditModel.Wulff_Crystal Wulff_Crystal<NEx=972.,
							      NEy=333.> {
      preferences => <-.preferences;
      active_menu => <-.active_menu;
      UIparent => <-.UIparent;
      model_type => <-.model_type;
    };
    CCP3.Core_Macros.UI.EditModel.Multilayer Multilayer<NEx=981.,NEy=414.> {
      preferences => <-.preferences;
      active_menu => (<-.active_menu && (model_type == 2));
      active_menu => <-.active_menu;
      UIparent => <-.UIparent;
      model_type => <-.model_type;
    };
#ifdef DLV_DL
    CCP3.Core_Macros.UI.EditModel.Fill_Shape Fill_Shape<NEx=972.,NEy=495.> {
      preferences => <-.preferences;
      active_menu => (<-.active_menu && (model_type == 3));
      UIparent => <-.UIparent;
      model_type => <-.model_type;
    };
#endif // DLV_DL
  };
};
