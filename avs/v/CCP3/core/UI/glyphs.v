
flibrary Glyphs<NEeditable=1> {
  macro baseUI {
    link parent<NEportLevels={2,1},NEx=110.,NEy=33.>;
    link visible<NEportLevels={2,1},NEx=341.,NEy=33.>;
    boolean convcell<NEportLevels={0,1}> = 0;
    CCP3.Core_Macros.Core.preferences &prefs<NEx=561.,NEy=33.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog DLVdialog<NEx=231.,NEy=121.> {
      parent => <-.parent;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      //ok = 0;
      cancel = 0;
      okButton => <-.active;
      cancelButton = 1;
      width = 350;
      height = 400;
      visible => <-.visible;
      title = "Create Volume Region";
#ifdef MSDOS
      y = 100;
#endif // MSDOS
    };
    string label = "";
    UIpanel UIpanel<NEx=440.,NEy=176.> {
      parent => <-.DLVdialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => parent.clientHeight;
      x = 0;
      y = 0;
    };
    UIframe base_frame<NEx=836.,NEy=187.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => (<-.Name.y + <-.Name.height + 5);
      x = 0;
      y = 0;
      visible => (<-.active == 0);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle space<NEx=847.,NEy=275.> {
      parent => <-.base_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y = 0;
      set = 0;
      visible => (<-.active == 0);
      label = "k space";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtextTypein Name<NEx=286.,NEy=649.> {
      UIparent => <-.base_frame;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      width => (UIparent.clientWidth - create.width - 10);
      y => (space.y + space.height + 5);
      visible => (<-.active == 0);
      slabel => "Name";
      stext => <-.label;
    };
    boolean show => (strlen(label) > 0);
    int active = 0;
    UIbutton create<NEx=847.,NEy=220.> {
      parent => <-.base_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => Name.y;
      x => (parent.clientWidth - width - 5);
      do => <-.active;
      visible => (<-.show && (<-.active == 0));
    };
    GMOD.parse_v reset_data {
      relative => <-;
      on_inst = 0;
      trigger => <-.active;
    };
  };
  macro edit_common {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=198.,NEy=66.>;
    link active_menus<NEportLevels={2,1},NEx=385.,NEy=66.>;
    link UIparent<NEportLevels={2,1},NEx=770.,NEy=66.>;
    int visible<NEportLevels=1,NEx=396.,NEy=143.> = 0;
    link model_type;
    int ok = 0;
    UIcmd UIcmd<NEx=198.,NEy=143.> {
      active => (<-.active_menus && model_type > -1);
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      do => <-.visible;
    };
    GMOD.instancer instancer<NEx=264.,NEy=275.> {
      Value => <-.visible;
    };
  };
  edit_common edit_base { 
    int create<NEportLevels=1,NEx=572.,NEy=99.> = 0;
    GMOD.parse_v init_data<NEx=759.,NEy=341.> {
      v_commands = "label = \"\"; active = 0;";
      //relative => <-;
      trigger => <-.visible;
      on_inst = 0;
      active = 1;
    };
  };
  baseUI PointUI {
    DLVdialog {
      title = "Create Point";
    };
    float a<NEportLevels={0,1}> = 0.0;
    float b<NEportLevels={0,1}> = 0.0;
    float c<NEportLevels={0,1}> = 0.0;
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein x<NEx=198.,NEy=286.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => (<-.base_frame.y + <-.base_frame.height + 5);
      panel {
	visible => <-.<-.active;
      };
      width => UIparent.clientWidth;
      fval => <-.a;
      flabel => "x";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein y<NEx=198.,NEy=363.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => (<-.x.y + <-.x.height + 5);
      panel {
	visible => <-.<-.active;
      };
      width => UIparent.clientWidth;
      fval => <-.b;
      flabel => "y";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein z<NEx=198.,NEy=429.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => (<-.y.y + <-.y.height + 5);
      panel {
	visible => <-.<-.active;
      };
      width => UIparent.clientWidth;
      fval => <-.c;
      flabel => "z";
    };
    UIbutton fractional<NEx=286.,NEy=594.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.z.y + <-.z.height + 5);
      do = 0;
      visible => <-.active;
    };
    UIbutton cartesian<NEx=473.,NEy=594.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => (parent.clientWidth / 2);
      y => <-.fractional.y;
      do = 0;
      visible => <-.active;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle conventional<NEx=693.,NEy=451.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.fractional.y + <-.fractional.height + 5);
      set => <-.convcell;
      visible => <-.active;
      label = "Conventional Lattice";
      width => parent.clientWidth;
    };
    reset_data {
      v_commands = "a = 0.0; b = 0.0; c = 0.0; convcell = 0;";
    };
    CCP3.Core_Modules.Objects.update_point update_point<NEx=748.,NEy=638.> {
      fractional => <-.fractional.do;
      cartesian => <-.cartesian.do;
      x => <-.a;
      y => <-.b;
      z => <-.c;
      conventional => <-.convcell;
    };
  };
  edit_base edit_point {
    UIcmd {
      label = "Point";
    };
    CCP3.Core_Modules.Objects.create_point create_point<NEx=440.,NEy=418.> {
      name => <-.PointUI.label;
      space => <-.PointUI.space.set;
      trigger => <-.create;
      reject => <-.PointUI.DLVdialog.cancel;
      ok => <-.ok;
    };
    instancer {
      Group => <-.PointUI;
    };
    init_data {
      relative => <-.PointUI;
    };
    CCP3.Core_Macros.UI.Glyphs.PointUI PointUI<NEx=561.,NEy=275.,instanced=0> {
      DLVdialog {
	ok => <-.<-.ok;
      };
      parent => <-.UIparent;
      visible => <-.visible;
      prefs => <-.prefs;
      active => <-.create;
      fractional {
	visible => (<-.active && (<-.<-.model_type > 0));
      };
      conventional {
	visible => (<-.active && (<-.<-.model_type > 0));
      };
    };
  };
  baseUI LineUI {
    DLVdialog {
      title = "Create Line";
      height = 425;
    };
    link model_type;
    UIpanel<NEx=108.,NEy=180.>;
    base_frame<NEx=630.,NEy=180.>;
    space<NEx=792.,NEy=234.>;
    Name<NEx=630.,NEy=234.>;
    create<NEx=945.,NEy=234.>;
    UIframe obj_frame<NEx=297.,NEy=180.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      y = 0;
      height => parent.clientHeight;
      visible => <-.active;
    };
    int parent_obj = 0;
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle parent_data<NEx=216.,NEy=234.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.copy.y + <-.copy.height + 5);
      set => parent_obj;
      visible => <-.active;
      label = "Use parent lattice";
      width => parent.clientWidth / 2;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle conventional<NEx=405.,NEy=234.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => parent.clientWidth / 2;
      y => (<-.copy.y + <-.copy.height + 5);
      set => convcell;
      visible => (<-.active && (<-.model_type > 1));
      label = "Conventional Lattice";
      width => parent.clientWidth / 2;
    };
    UIframe coord_frame<NEx=216.,NEy=306.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => (<-.fractional.y + <-.fractional.height + 5);
      x = 0;
      y => (<-.conventional.y + <-.conventional.height + 5);
      visible => <-.active;
    };
    int frac = 0;
    float xval = 0.0;
    float yval = 0.0;
    float zval = 1.0;
    UIlabel coords<NEx=405.,NEy=306.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "Coordinates";
      x = 0;
      y = 5;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle fractional<NEx=585.,NEy=306.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.set_o.y + <-.set_o.height + 5);
      set => <-.frac;
      visible => (<-.active && (<-.model_type > 1));
      label = "Fractional coordinates";
      width => parent.clientWidth / 2;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield x<NEx=405.,NEy=351.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 50;
      x => (parent.clientWidth / 2);
      y => <-.coords.y;
      visible => (<-.active);
      value => <-.xval;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield y<NEx=585.,NEy=351.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 50;
      x => (2 * parent.clientWidth / 3);
      y => <-.coords.y;
      visible => (<-.active);
      value => <-.yval;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield z<NEx=765.,NEy=351.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 50;
      x => (5 * parent.clientWidth / 6);
      y => <-.coords.y;
      visible => (<-.active);
      value => <-.zval;
    };
    UIbutton set_o<NEx=765.,NEy=306.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.coords.y + <-.coords.height + 5);
      label = "Set O";
      do = 0;
    };
    UIbutton set_a<NEx=936.,NEy=306.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.coords.y + <-.coords.height + 5);
      x => 2 * parent.clientWidth / 3;
      label = "Set A";
      do = 0;
    };
    UIframe UIframe<NEx=216.,NEy=396.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => (<-.set_origin.y + <-.set_origin.height + 5);
      x = 0;
      y => (<-.coord_frame.y + <-.coord_frame.height);
      visible => <-.active;
    };
    UIlabel atoms<NEx=774.,NEy=396.> {
      parent => <-.UIframe;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      label = "Set from selected atoms";
      y = 5;
    };
    UIbutton set_OA<NEx=405.,NEy=396.> {
      parent => <-.UIframe;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => parent.clientWidth / 3;
      y => (<-.atoms.y + <-.atoms.height + 5);
      label = "Set OA";
      do = 0;
    };
    UIbutton atoms_a<NEx=585.,NEy=396.> {
      parent => <-.UIframe;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => 2 * parent.clientWidth / 3;
      y => <-.set_OA.y;
      label = "Set A";
      do = 0;
    };
    UIbutton set_origin<NEx=936.,NEy=396.> {
      parent => <-.UIframe;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => <-.set_OA.y;
      label = "Set O";
      do = 0;
    };
    int hindex = 0;
    int kindex = 0;
    int lindex = 1;
    CCP3.Core_Macros.UI.UIobjs.DLVifield h<NEx=585.,NEy=450.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 30;
      x => (parent.clientWidth / 2);
      y => <-.miller_AB.y;
      visible => (<-.active && ((parent_obj == 1) || (<-.model_type > 1)));
      value => <-.hindex;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield k<NEx=756.,NEy=450.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 30;
      x => (2 * parent.clientWidth / 3);
      y => <-.miller_AB.y;
      visible => (<-.active && ((parent_obj == 1) || (<-.model_type > 1)));
      value => <-.kindex;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield l<NEx=918.,NEy=450.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => (5 * parent.clientWidth / 6);
      y => <-.miller_AB.y;
      width = 30;
      visible => (<-.active && ((parent_obj == 1) || (<-.model_type > 2)));
      value => <-.lindex;
    };
    UIbutton miller_AB<NEx=405.,NEy=450.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.UIframe.y + <-.UIfrmae.height + 5);
      do = 0;
      label = "Miller OA";
      visible => (<-.active && ((<-.parent_obj == 1) || (<-.model_type > 1)));
    };
    UIframe copy<NEx=216.,NEy=522.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      y = 0;
      height => (<-.copy_from.y + <-.copy_from.height + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist list<NEx=585.,NEy=522.> {
      parent => <-.copy;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      y = 5;
    };
    UIbutton copy_from<NEx=756.,NEy=522.> {
      parent => <-.copy;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.list.y + <-.list.height + 5);
      label = "Copy from object";
      width = 150;
      x => (parent.clientWidth - width) / 2; 
      visible => (array_size(<-.list.strings) > 0);
      do = 0;
    };
    reset_data {
      v_commands = "hindex = 0; kindex = 0; lindex = 1; convcell = 1;"
	+ " parent_obj = 0;";
    };
    CCP3.Core_Modules.Objects.update_line update_line<NEx=616.,NEy=616.> {
      parent_lattice => <-.parent_obj;
      object => <-.list.selectedItem;
      copy_from => <-.copy_from.do;
      coord_o => <-.set_o.do;
      coord_a => <-.set_a.do;
      x => <-.xval;
      y => <-.yval;
      z => <-.zval;
      fractional => <-.frac;
      atom_oa => <-.set_OA.do;
      atom_o => <-.set_origin.do;
      atom_a => <-.atoms_a.do;
      miller => <-.miller_AB.do;
      h => <-.hindex;
      k => <-.kindex;
      l => <-.lindex;
      conventional => <-.convcell;
    };
  };
  edit_base edit_line {
    UIcmd {
      label = "Line";
    };
    CCP3.Core_Modules.Objects.create_line create_line<NEx=440.,NEy=418.> {
      name => <-.LineUI.label;
      space => <-.LineUI.space.set;
      trigger => <-.create;
      reject => <-.LineUI.DLVdialog.cancel;
      ok => <-.ok;
    };
    instancer {
      Group => <-.LineUI;
    };
    init_data {
      relative => <-.LineUI;
    };
    CCP3.Core_Macros.UI.Glyphs.LineUI LineUI<NEx=561.,NEy=275.,instanced=0> {
      DLVdialog {
	ok => <-.<-.ok;
      };
      parent => <-.UIparent;
      visible => <-.visible;
      prefs => <-.prefs;
      active => <-.create;
      model_type => <-.model_type;
    };
  };
  baseUI PlaneUI {
    DLVdialog<NEx=72.,NEy=117.> {
      title = "Create Plane";
      height = 550;
    };
    convcell<NEx=927.,NEy=36.>;
    label<NEx=747.,NEy=36.>;
    UIpanel<NEx=234.,NEy=162.>;
    UIframe obj_frame<NEx=234.,NEy=216.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => parent.clientHeight;
      x = 0;
      y = 0;
      visible => <-.active;
    };
    base_frame<NEx=495.,NEy=162.>;
    space<NEx=873.,NEy=162.>;
    create<NEx=684.,NEy=162.>;
    UIframe copy_frame<NEx=423.,NEy=216.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      y = 0;
      height => (<-.copy_from.y + <-.copy_from.height + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist list<NEx=648.,NEy=216.> {
      parent => <-.copy_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      y = 5;
    };
    UIbutton copy_from<NEx=873.,NEy=216.> {
      parent => <-.copy_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.list.y + <-.list.height + 5);
      label = "Copy from object";
      width = 150;
      x => (parent.clientWidth - width) / 2; 
      visible => (array_size(<-.list.strings) > 0);
      do = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle use_parent<NEx=513.,NEy=261.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.copy_frame.y + <-.copy_frame.height + 5);
      visible => <-.active;
      label = "Use parent lattice";
      width => parent.clientWidth / 2;
      set = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle conventional<NEx=747.,NEy=261.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => parent.clientWidth / 2;
      y => <-.use_parent.y;
      set => convcell;
      visible => (<-.active && (<-.model_type > 1));
      label = "Conventional Lattice";
      width => parent.clientWidth / 2;
    };
    float xcoord<NEportLevels=1,NEx=9.,NEy=342.> = 0.0;
    float ycoord<NEportLevels=1,NEx=9.,NEy=387.> = 0.0;
    float zcoord<NEportLevels=1,NEx=9.,NEy=441.> = 0.0;
    boolean frac<NEportLevels=1,NEx=9.,NEy=495.> = 0;
    UIframe coord_frame<NEx=153.,NEy=297.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => (<-.fractional.y + <-.fractional.height + 5);
      x = 0;
      y => (<-.conventional.y + <-.conventional.height + 5);
      visible => <-.active;
    };
    UIlabel coord_label<NEx=153.,NEy=342.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "Coordinates";
      x = 0;
      y = 5;
    };
    UIbutton coord_a<NEx=324.,NEy=342.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.coord_label.y + <-.coord_label.height + 5);
      x => parent.clientWidth / 3;
      label = "Set A";
      do = 0;
    };
    UIbutton coord_b<NEx=495.,NEy=342.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.coord_label.y + <-.coord_label.height + 5);
      x => 2 * parent.clientWidth / 3;
      label = "Set B";
      do = 0;
    };
    UIbutton coord_o<NEx=675.,NEy=342.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.coord_label.y + <-.coord_label.height + 5);
      label = "Set O";
      do = 0;
    };
    UIbutton coord_centre<NEx=864.,NEy=342.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => 2 * parent.clientWidth / 3;
      y => (<-.coord_o.y + <-.coord_o.height + 5);
      label = "Set Centre";
      do = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield x<NEx=324.,NEy=297.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 50;
      x => (parent.clientWidth / 2);
      y => <-.coord_label.y;
      visible => (<-.active);
      value => <-.xcoord;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield y<NEx=495.,NEy=297.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 50;
      x => (2 * parent.clientWidth / 3);
      y => <-.coord_label.y;
      visible => (<-.active);
      value => <-.ycoord;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield z<NEx=675.,NEy=297.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 50;
      x => (5 * parent.clientWidth / 6);
      y => <-.coord_label.y;
      visible => (<-.active);
      value => <-.zcoord;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle fractional<NEx=864.,NEy=297.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.coord_o.y + <-.coord_o.height + 5);
      set => <-.frac;
      visible => (<-.active && (<-.model_type > 1));
      label = "Fractional coordinates";
      width => parent.clientWidth / 2;
    };
    UIframe select_frame<NEx=153.,NEy=396.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => (<-.set_origin.y + <-.set_origin.height + 5);
      x = 0;
      y => (<-.coord_frame.y + <-.coord_frame.height);
      visible => <-.active;
    };
    UIlabel select_label<NEx=162.,NEy=441.> {
      parent => <-.select_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      label = "Set from selected atoms";
      y = 5;
    };
    UIbutton set_BOA<NEx=864.,NEy=396.> {
      parent => <-.select_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.select_label.y + <-.select_label.height + 5);
      x = 0;
      visible => <-.active;
      do = 0;
    };
    UIbutton set_OA<NEx=495.,NEy=441.> {
      parent => <-.select_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => <-.set_BOA.y;
      x => parent.clientWidth / 3;
      do = 0;
    };
    UIbutton set_OB<NEx=324.,NEy=441.> {
      parent => <-.select_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => <-.set_BOA.y;
      x => 2 * parent.clientWidth / 3;
      do = 0;
    };
    UIbutton set_a<NEx=675.,NEy=441.> {
      parent => <-.select_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => <-.set_origin.y;
      x => parent.clientWidth / 3;
      label = "Set A";
      do = 0;
    };
    UIbutton set_b<NEx=864.,NEy=441.> {
      parent => <-.select_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => <-.set_origin.y;
      x => 2 * parent.clientWidth / 3;
      label = "Set B";
      do = 0;
    };
    UIbutton set_origin<NEx=324.,NEy=396.> {
      parent => <-.select_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.set_BOA.y + <-.set_BOA.height + 5);
      x = 0;
      label = "Set O";
      do = 0;
      visible => <-.active;
    };
    UIbutton set_centre<NEx=495.,NEy=396.> {
      parent => <-.select_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.set_origin.y + <-.set_origin.height + 5);
      x => parent.clientWidth / 3;
      do = 0;
      label = "Set Centre";
      visible => <-.active;
    };
    link model_type<NEx=54.,NEy=234.>;
    int hindex<NEx=243.,NEy=81.> = 0;
    int kindex<NEx=414.,NEy=81.> = 0;
    int lindex<NEx=594.,NEy=72.> = 1;
    UIframe miller_frame<NEx=153.,NEy=495.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => (<-.cartesian_vectors.y + <-.cartesian_vectors.height + 5);
      x = 0;
      y => (<-.select_frame.y + <-.select_frame.height);
      visible => <-.active;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield h<NEx=495.,NEy=495.> {
      parent => <-.miller_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 30;
      x => (parent.clientWidth / 2);
      y => <-.miller_AB.y;
      visible => (<-.active && ((use_parent.set == 1) || (<-.model_type > 1)));
      value => <-.hindex;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield k<NEx=675.,NEy=495.> {
      parent => <-.miller_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 30;
      x => (2 * parent.clientWidth / 3);
      y => <-.miller_AB.y;
      visible => (<-.active && ((use_parent.set == 1) || (<-.model_type > 1)));
      value => <-.kindex;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield l<NEx=864.,NEy=495.> {
      parent => <-.miller_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => (5 * parent.clientWidth / 6);
      y => <-.miller_AB.y;
      width = 30;
      visible => (<-.active && ((use_parent.set == 1) || (<-.model_type > 2)));
      value => <-.lindex;
    };
    UIbutton miller_AB<NEx=324.,NEy=495.> {
      parent => <-.miller_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 120;
      x = 0;
      y = 5;
      do = 0;
      visible => (<-.active && ((use_parent.set == 1) || (<-.model_type > 1)));
    };
    UIbutton align_lattice<NEx=324.,NEy=540.> {
      parent => <-.miller_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.h.y + <-.h.height + 5);
      width = 120;
      do = 0;
      visible => (<-.active  && (<-.model_type > 0));
    };
    UIbutton align_cartesian<NEx=495.,NEy=540.> {
      parent => <-.miller_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => (parent.clientWidth / 2);
      y => <-.align_lattice.y;
      width = 120;
      do = 0;
      visible => <-.active;
    };
    UIbutton lattice_vectors<NEx=675.,NEy=540.> {
      parent => <-.miller_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.align_lattice.y + <-.align_lattice.height + 5);
      width = 120;
      do = 0;
      visible => (<-.active && (<-.model_type > 0));
    };
    UIbutton cartesian_vectors<NEx=864.,NEy=540.> {
      parent => <-.miller_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => (parent.clientWidth / 2);
      y => <-.lattice_vectors.y;
      do = 0;
      width = 120;
      visible => <-.active;
    };
    int use_transform<NEx=801.,NEy=81.> = 0;
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle transform<NEx=153.,NEy=567.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.miller_frame.y + <-.miller_frame.height + 5);
      set => <-.use_transform;
      label = "Use Transformation Editor";
      width => parent.clientWidth;
#ifdef DLV_TRANSFORM_EDIT_BUG
      visible = 0;
#else
      visible => <-.active;
#endif // DLV_TRANSFORM_EDIT_BUG
    };
    reset_data {
      v_commands = "hindex = 0; kindex = 0; lindex = 1; convcell = 1;"
	+ " use_transform = 0;";
    };
    CCP3.Core_Modules.Objects.update_plane update_plane<NEx=627.,NEy=649.> {
      object => <-.list.selectedItem;
      copy_from => <-.copy_from.do;
      oa => <-.set_OA.do;
      ob => <-.set_OB.do;
      a => <-.set_a.do;
      b => <-.set_b.do;
      origin => <-.set_origin.do;
      centre => <-.set_centre.do;
      miller => <-.miller_AB.do;
      h => <-.hindex;
      k => <-.kindex;
      l => <-.lindex;
      align_lattice => <-.align_lattice.do;
      align_xyz => <-.align_cartesian.do;
      lattice => <-.lattice_vectors.do;
      cartesian => <-.cartesian_vectors.do;
      conventional => <-.convcell;
      boa => <-.set_BOA.do;
      x => <-.xcoord;
      y => <-.ycoord;
      z => <-.zcoord;
      pos_a => <-.coord_a.do;
      pos_b => <-.coord_b.do;
      pos_o => <-.coord_o.do;
      pcentre => <-.coord_centre.do;
      frac => <-.frac;
      parent_obj => <-.use_parent.set;
    };
    CCP3.Core_Modules.Objects.edit_object edit_object<NEx=891.,NEy=585.> {
      trigger => <-.use_transform;
      active => <-.active;
    };
  };
  edit_base edit_plane {
    UIcmd {
      label = "Plane";
    };
    CCP3.Core_Modules.Objects.create_plane create_plane<NEx=440.,NEy=418.> {
      name => <-.PlaneUI.label;
      space => <-.PlaneUI.space.set;
      trigger => <-.create;
      reject => <-.PlaneUI.DLVdialog.cancel;
      ok => <-.ok;
    };
    instancer {
      Group => <-.PlaneUI;
    };
    init_data {
      relative => <-.PlaneUI;
    };
    CCP3.Core_Macros.UI.Glyphs.PlaneUI PlaneUI<NEx=561.,NEy=275.,instanced=0> {
      DLVdialog {
	ok => <-.<-.ok;
      };
      parent => <-.UIparent;
      visible => <-.visible;
      prefs => <-.prefs;
      active => <-.create;
      model_type => <-.model_type;
    };
  };
  baseUI BoxUI {
    DLVdialog {
      title = "Create Volume Region";
      height = 600;
    };
    convcell<NEx=927.,NEy=36.>;
    label<NEx=747.,NEy=36.>;
    UIpanel<NEx=234.,NEy=162.>;
    UIframe obj_frame<NEx=234.,NEy=216.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => parent.clientHeight;
      x = 0;
      y = 0;
      visible => <-.active;
    };
    base_frame<NEx=495.,NEy=162.>;
    space<NEx=873.,NEy=162.>;
    create<NEx=684.,NEy=162.>;
    UIframe copy_frame<NEx=423.,NEy=216.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      y = 0;
      height => (<-.copy_from.y + <-.copy_from.height + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist list<NEx=648.,NEy=216.> {
      parent => <-.copy_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      y = 5;
    };
    UIbutton copy_from<NEx=873.,NEy=216.> {
      parent => <-.copy_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.list.y + <-.list.height + 5);
      label = "Copy from object";
      width = 150;
      x => (parent.clientWidth - width) / 2; 
      visible => (array_size(<-.list.strings) > 0);
      do = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle use_parent<NEx=513.,NEy=261.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.copy_frame.y + <-.copy_frame.height + 5);
      visible => <-.active;
      label = "Use parent lattice";
      width => parent.clientWidth / 2;
      set = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle conventional<NEx=747.,NEy=261.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => parent.clientWidth / 2;
      y => <-.use_parent.y;
      set => convcell;
      visible => (<-.active && (<-.model_type > 1));
      label = "Conventional Lattice";
      width => parent.clientWidth / 2;
    };
    float xcoord<NEportLevels=1,NEx=9.,NEy=342.> = 0.0;
    float ycoord<NEportLevels=1,NEx=9.,NEy=387.> = 0.0;
    float zcoord<NEportLevels=1,NEx=9.,NEy=441.> = 0.0;
    boolean frac<NEportLevels=1,NEx=9.,NEy=495.> = 0;
    UIframe coord_frame<NEx=153.,NEy=297.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => (<-.fractional.y + <-.fractional.height + 5);
      x = 0;
      y => (<-.conventional.y + <-.conventional.height + 5);
      visible => <-.active;
    };
    UIlabel coord_label<NEx=153.,NEy=342.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "Coordinates";
      x = 0;
      y = 5;
    };
    UIbutton coord_a<NEx=324.,NEy=342.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.coord_label.y + <-.coord_label.height + 5);
      x = 0;
      label = "Set A";
      do = 0;
    };
    UIbutton coord_b<NEx=495.,NEy=342.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.coord_label.y + <-.coord_label.height + 5);
      x => parent.clientWidth / 3;
      label = "Set B";
      do = 0;
    };
    UIbutton coord_c<NEx=1017.,NEy=324.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => <-.coord_a.y;
      x => 2 * parent.clientWidth / 3;
      label = "Set C";
      do = 0;
    };
    UIbutton coord_o<NEx=675.,NEy=342.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.coord_a.y + <-.coord_a.height + 5);
      x = 0;
      label = "Set O";
      do = 0;
    };
    UIbutton coord_centre<NEx=864.,NEy=342.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => 2 * parent.clientWidth / 3;
      y => <-.coord_o.y;
      label = "Set Centre";
      do = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield x<NEx=324.,NEy=297.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 50;
      x => (parent.clientWidth / 2);
      y => <-.coord_label.y;
      visible => (<-.active);
      value => <-.xcoord;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield y<NEx=495.,NEy=297.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 50;
      x => (2 * parent.clientWidth / 3);
      y => <-.coord_label.y;
      visible => (<-.active);
      value => <-.ycoord;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield z<NEx=675.,NEy=297.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 50;
      x => (5 * parent.clientWidth / 6);
      y => <-.coord_label.y;
      visible => (<-.active);
      value => <-.zcoord;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle fractional<NEx=864.,NEy=297.> {
      parent => <-.coord_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.coord_o.y + <-.coord_o.height + 5);
      set => <-.frac;
      visible => (<-.active && (<-.model_type > 1));
      label = "Fractional coordinates";
      width => parent.clientWidth / 2;
    };
    UIframe select_frame<NEx=153.,NEy=396.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => (<-.set_origin.y + <-.set_origin.height + 5);
      x = 0;
      y => (<-.coord_frame.y + <-.coord_frame.height);
      visible => <-.active;
    };
    UIlabel select_label<NEx=162.,NEy=441.> {
      parent => <-.select_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      label = "Set from selected atoms";
      y = 5;
    };
    UIbutton set_OA<NEx=495.,NEy=441.> {
      parent => <-.select_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.select_label.y + <-.select_label.height + 5);
      x = 0;
      do = 0;
    };
    UIbutton set_OB<NEx=324.,NEy=441.> {
      parent => <-.select_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => <-.set_OA.y;
      x => parent.clientWidth / 3;
      do = 0;
    };
    UIbutton set_OC<NEx=864.,NEy=396.> {
      parent => <-.select_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => 2 * parent.clientWidth / 3;
      y => <-.set_OA.y;
      visible => <-.active;
      do = 0;
    };
    UIbutton set_a<NEx=675.,NEy=441.> {
      parent => <-.select_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.set_OA.y + <-.set_OA.height + 5);
      x = 0;
      label = "Set A";
      do = 0;
    };
    UIbutton set_b<NEx=864.,NEy=441.> {
      parent => <-.select_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => <-.set_a.y;
      x => parent.clientWidth / 3;
      label = "Set B";
      do = 0;
    };
    UIbutton set_c<NEx=675.,NEy=396.> {
      parent => <-.select_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => <-.set_a.y;
      x => 2 * parent.clientWidth / 3;
      label = "Set C";
      do = 0;
    };
    UIbutton set_origin<NEx=324.,NEy=396.> {
      parent => <-.select_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.set_a.y + <-.set_a.height + 5);
      x = 0;
      label = "Set O";
      do = 0;
      visible => <-.active;
    };
    UIbutton set_centre<NEx=495.,NEy=396.> {
      parent => <-.select_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => <-.set_origin.y;
      x => 2 * parent.clientWidth / 3;
      do = 0;
      label = "Set Centre";
      visible => <-.active;
    };
    link model_type<NEx=54.,NEy=234.>;
    int hindex<NEx=243.,NEy=81.> = 0;
    int kindex<NEx=414.,NEy=81.> = 0;
    int lindex<NEx=594.,NEy=72.> = 1;
    UIframe miller_frame<NEx=153.,NEy=495.> {
      parent => <-.obj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => (<-.cartesian_vectors.y + <-.cartesian_vectors.height + 5);
      x = 0;
      y => (<-.select_frame.y + <-.select_frame.height);
      visible => <-.active;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield h<NEx=495.,NEy=495.> {
      parent => <-.miller_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 30;
      x => (parent.clientWidth / 2);
      y => <-.miller_AB.y;
      visible => (<-.active && ((use_parent.set == 1) || (<-.model_type > 1)));
      value => <-.hindex;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield k<NEx=675.,NEy=495.> {
      parent => <-.miller_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 30;
      x => (2 * parent.clientWidth / 3);
      y => <-.miller_AB.y;
      visible => (<-.active && ((use_parent.set == 1) || (<-.model_type > 1)));
      value => <-.kindex;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield l<NEx=864.,NEy=495.> {
      parent => <-.miller_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => (5 * parent.clientWidth / 6);
      y => <-.miller_AB.y;
      width = 30;
      visible => (<-.active && ((use_parent.set == 1) || (<-.model_type > 2)));
      value => <-.lindex;
    };
    UIbutton miller_AB<NEx=324.,NEy=495.> {
      parent => <-.miller_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 120;
      x = 0;
      y = 5;
      do = 0;
      visible => (<-.active && ((use_parent.set == 1) || (<-.model_type > 1)));
    };
    UIbutton align_lattice<NEx=324.,NEy=540.> {
      parent => <-.miller_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.h.y + <-.h.height + 5);
      width = 120;
      do = 0;
      visible => (<-.active  && (<-.model_type > 0));
    };
    UIbutton align_cartesian<NEx=495.,NEy=540.> {
      parent => <-.miller_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => (parent.clientWidth / 2);
      y => <-.align_lattice.y;
      width = 120;
      do = 0;
      visible => <-.active;
    };
    UIbutton lattice_vectors<NEx=675.,NEy=540.> {
      parent => <-.miller_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.align_lattice.y + <-.align_lattice.height + 5);
      width = 120;
      do = 0;
      visible => (<-.active && (<-.model_type > 0));
    };
    UIbutton cartesian_vectors<NEx=864.,NEy=540.> {
      parent => <-.miller_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => (parent.clientWidth / 2);
      y => <-.lattice_vectors.y;
      do = 0;
      width = 120;
      visible => <-.active;
    };
    int use_transform = 0;
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle transform<NEx=324.,NEy=585.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.miller_frame.y + <-.miller_frame.height + 5);
      set => <-.use_transform;
      label = "Use Transformation Editor";
      width => parent.clientWidth;
#ifdef DLV_TRANSFORM_EDIT_BUG
      visible = 0;
#else
      visible => <-.active;
#endif // DLV_TRANSFORM_EDIT_BUG
    };
    reset_data {
      v_commands = "hindex = 0; kindex = 0; lindex = 1; convcell = 1;"
	+ " use_transform = 0;";
    };
    CCP3.Core_Modules.Objects.update_box update_box<NEx=627.,NEy=649.> {
      object => <-.list.selectedItem;
      copy_from => <-.copy_from.do;
      oa => <-.set_OA.do;
      ob => <-.set_OB.do;
      oc => <-.set_OC.do;
      a => <-.set_a.do;
      b => <-.set_b.do;
      c => <-.set_c.do;
      origin => <-.set_origin.do;
      centre => <-.set_centre.do;
      miller => <-.miller_AB.do;
      h => <-.hindex;
      k => <-.kindex;
      l => <-.lindex;
      align_lattice => <-.align_lattice.do;
      align_xyz => <-.align_cartesian.do;
      lattice => <-.lattice_vectors.do;
      cartesian => <-.cartesian_vectors.do;
      conventional => <-.convcell;
      x => <-.xcoord;
      y => <-.ycoord;
      z => <-.zcoord;
      pos_a => <-.coord_a.do;
      pos_b => <-.coord_b.do;
      pos_o => <-.coord_o.do;
      pos_c => <-.coord_c.do;
      pcentre => <-.coord_centre.do;
      frac => <-.frac;
      parent_obj => <-.use_parent.set;
    };
    CCP3.Core_Modules.Objects.edit_object edit_object<NEx=891.,NEy=585.> {
      trigger => <-.use_transform;
      active => <-.active;
    };
  };
  edit_base edit_box {
    UIcmd {
      label = "Volume";
    };
    CCP3.Core_Modules.Objects.create_box create_box<NEx=440.,NEy=418.> {
      name => <-.BoxUI.label;
      space => <-.BoxUI.space.set;
      trigger => <-.create;
      reject => <-.BoxUI.DLVdialog.cancel;
      ok => <-.ok;
    };
    instancer {
      Group => <-.BoxUI;
    };
    init_data {
      relative => <-.BoxUI;
    };
    CCP3.Core_Macros.UI.Glyphs.BoxUI BoxUI<NEx=561.,NEy=275.,instanced=0> {
      DLVdialog {
	ok => <-.<-.ok;
      };
      parent => <-.UIparent;
      visible => <-.visible;
      prefs => <-.prefs;
      active => <-.create;
      model_type => <-.model_type;
    };
  };
  baseUI SphereUI {
    DLVdialog {
      title = "Create Sphere";
    };
    UIbutton set_origin<NEx=286.,NEy=363.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.base_frame.y + <-.base_frame.height + 5);
      do = 0;
      visible => <-.active;
    };
    float radius;
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider rad<NEx=286.,NEy=451.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      y => <-.set_origin.y + <-.set_origin.height + 5;
      value => <-.radius;
      min = 0.01;
      max = 30.0;
      title = "radius";
      visible => <-.active;
    };
    reset_data {
      v_commands = "radius = 1.0;";
    };
    CCP3.Core_Modules.Objects.update_sphere update_sphere<NEx=627.,NEy=649.> {
      origin => <-.set_origin.do;
      radius => <-.radius;
    };
  };
  edit_base edit_sphere {
    UIcmd {
      label = "Sphere";
    };
    CCP3.Core_Modules.Objects.create_sphere create_sphere<NEx=440.,NEy=418.> {
      name => <-.SphereUI.label;
      space = 0;
      //space => <-.SphereUI.space.set;
      trigger => <-.create;
      reject => <-.SphereUI.DLVdialog.cancel;
      ok => <-.ok;
    };
    instancer {
      Group => <-.SphereUI;
    };
    init_data {
      relative => <-.SphereUI;
    };
    SphereUI SphereUI<NEx=561.,NEy=275.,instanced=0> {
      DLVdialog {
	ok => <-.<-.ok;
      };
      space {
	visible = 0;
      };
      parent => <-.UIparent;
      visible => <-.visible;
      prefs => <-.prefs;
      active => <-.create;
      //model_type => <-.model_type;
    };
  };
  baseUI WulffUI {
    DLVdialog {
      title = "Create Wulff Plot";
      height = 500;
    };
    CCP3.Core_Modules.Objects.wulff_data &data<NEx=704.,NEy=99.,
      NEportLevels={2,0}>;
    space {
      visible = 0;
    };
    link model_type;
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein h<NEx=286.,NEy=451.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      width => (UIparent.clientWidth / 3) - 10;
      x = 5;
      y = 5;
      panel {
	visible => <-.<-.active;
      };
      field {
	width = 30;
      };
      fval => <-.data.h;
      flabel = "h";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein k {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      width => (UIparent.clientWidth / 3) - 10;
      x => (UIparent.clientWidth / 3) + 5;
      y => <-.h.y;
      panel {
	visible => <-.<-.active;
      };
      field {
	width = 30;
      };
      fval => <-.data.k;
      flabel = "k";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein l {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      x => (2 * UIparent.clientWidth / 3) + 5;
      width => (UIparent.clientWidth / 3) - 10;
      panel {
	visible => <-.<-.active;
      };
      field {
	width = 30;
      };
      y => <-.h.y;
      fval => <-.data.l;
      flabel = "l";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein energy<NEx=132.,NEy=374.> {
      UIparent => <-.DLVdialog;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel = "Surface Energy";
      fval = <-.data.energy;
      fmin = 0.01;
      panel {
	visible => <-.<-.active;
      };
      width => UIparent.clientWidth;
      y => (<-.h.y + <-.h.height + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider r<NEx=234.,NEy=558.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.energy.y + <-.energy.height + 5);
      min = 0.0;
      max = 1.0;
      value => <-.data.r;
      visible => <-.active;
      title = "red";
      width => parent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider g<NEx=423.,NEy=558.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.r.y + <-.r.height + 5);
      min = 0.0;
      max = 1.0;
      value => <-.data.g;
      visible => <-.active;
      title = "green";
      width => parent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider b<NEx=639.,NEy=558.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.g.y + <-.g.height + 5);
      min = 0.0;
      max = 1.0;
      value => <-.data.b;
      visible => <-.active;
      title = "blue";
      width => parent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle conventional<NEx=693.,NEy=451.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.b.y + <-.b.height + 5);
      set => <-.data.conventional;
      visible => <-.active;
      label = "Conventional Lattice";
      width => parent.clientWidth;
    };
    reset_data {
      v_commands = "data.h = 0; data.k = 0; data.l = 1; data.energy = 1.0;";
    };
    UIbutton add<NEx=473.,NEy=451.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.conventional.y + <-.conventional.height + 5);
      label = "Add Plane";
      do = 0;
      visible => <-.active;
    };
    UIbutton load {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.conventional.y + <-.conventional.height + 5);
      x => parent.clientWidth / 2;
      label = "Load File";
      do = 0;
      visible => <-.active;
    };
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}> => <-.data.filename;
      pattern = "*";
      parent => <-.UIpanel;
      title = "Load CRYSTAL Structure filename";
      x = 0;
      y => (<-.add.y + <-.add.height + 5);
      UIpanel {
	visible => <-.<-.active;
      };
    };
  };
  edit_base wulff {
    UIcmd {
      label = "Wulff Plot";
      active => (<-.active_menus && model_type == 3);
    };
    CCP3.Core_Modules.Objects.wulff_data data<NEx=770.,NEy=187.,
      NEportLevels={0,1}> {
      h = 0;
      k = 0;
      l = 1;
      r = 1.0;
      g = 1.0;
      b = 1.0;
      energy = 1.0;
      conventional = 1;
      filename = "";
    };
    CCP3.Core_Modules.Objects.create_wulff create_wulff<NEx=440.,NEy=418.> {
      name => <-.WulffUI.label;
      trigger => <-.create;
      do_add => <-.WulffUI.add.do;
      do_load => <-.WulffUI.load.do;
      data => <-.data;
      reject => <-.WulffUI.DLVdialog.cancel;
      ok => <-.ok;
    };
    instancer {
      Group => <-.WulffUI;
    };
    init_data {
      relative => <-.WulffUI;
    };
    CCP3.Core_Macros.UI.Glyphs.WulffUI WulffUI<NEx=561.,NEy=275.,instanced=0> {
      DLVdialog {
	ok => <-.<-.ok;
      };
      parent => <-.UIparent;
      visible => <-.visible;
      prefs => <-.prefs;
      active => <-.create;
      model_type => <-.model_type;
      data => <-.data;
    };
  };
  macro LEEDUI {
    link parent<NEportLevels={2,1},NEx=110.,NEy=33.>;
    link visible<NEportLevels={2,1},NEx=341.,NEy=33.>;
    CCP3.Core_Macros.Core.preferences &prefs<NEx=561.,NEy=33.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog DLVdialog<NEx=231.,NEy=121.> {
      parent => <-.parent;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      cancel = 0;
      okButton = 1;
      cancelButton = 1;
      visible => <-.visible;
      title = "Create LEED Pattern";
#ifdef MSDOS
      y = 100;
#endif // MSDOS
    };
    UIpanel UIpanel<NEx=440.,NEy=176.> {
      parent => <-.DLVdialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => parent.clientHeight;
      x = 0;
      y = 0;
    };
    int data<NEx=704.,NEy=99.> = 0;
    link model_type;
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle domains<NEx=693.,NEy=451.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y = 0;
      set => <-.data;
      label = "Show Domains";
      width => parent.clientWidth;
    };
    GMOD.parse_v reset_data {
      v_commands = "domains = 0;";
      relative => <-;
      on_inst = 0;
      trigger => <-.visible;
    };
  };
  edit_common leed {
    UIcmd {
      label = "LEED pattern";
      active => (<-.active_menus && (model_type == 2 || model_type == 4));
    };
    CCP3.Core_Modules.Objects.create_leed create_leed<NEx=440.,NEy=418.> {
      trigger => <-.visible;
      domains => <-.LEEDUI.data;
      reject => <-.LEEDUI.DLVdialog.cancel;
      ok => <-.ok;
    };
    instancer {
      Group => <-.LEEDUI;
    };
    /*init_data {
      relative => <-.LEEDUI;
      };*/
    CCP3.Core_Macros.UI.Glyphs.LEEDUI LEEDUI<NEx=561.,NEy=275.,instanced=0> {
      DLVdialog {
	ok => <-.<-.ok;
      };
      parent => <-.UIparent;
      visible => <-.visible;
      prefs => <-.prefs;
      //active => <-.create;
      model_type => <-.model_type;
    };
  };
  macro GlyphsUI {
    link preferences<NEportLevels={2,1},NEx=231.,NEy=77.>;
    int active_menus<NEportLevels={2,1},NEx=429.,NEy=77.>;
    link model_type<NEportLevels={2,1},NEx=110.,NEy=143.>;
    int dialog_visible<NEportLevels=1,NEx=660.,NEy=77.> =>
      ((edit_point.visible == 1) || (edit_line.visible == 1) ||
       (edit_plane.visible == 1) || (edit_box.visible == 1) ||
       (leed.visible == 1) || (wulff.visible == 1) ||
       (edit_sphere.visible == 1));
    link UIparent<NEportLevels={2,1},NEx=858.,NEy=77.>;
    CCP3.Core_Macros.UI.Glyphs.edit_point edit_point<NEx=462.,NEy=253.> {
      prefs => <-.preferences;
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      model_type => <-.model_type;
    };
    CCP3.Core_Macros.UI.Glyphs.edit_line edit_line<NEx=627.,NEy=253.> {
      prefs => <-.preferences;
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      model_type => <-.model_type;
    };
    CCP3.Core_Macros.UI.Glyphs.edit_plane edit_plane<NEx=803.,NEy=253.> {
      prefs => <-.preferences;
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      model_type => <-.model_type;
    };
    CCP3.Core_Macros.UI.Glyphs.edit_box edit_box<NEx=275.,NEy=253.> {
      prefs => <-.preferences;
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      model_type => <-.model_type;
    };
    CCP3.Core_Macros.UI.Glyphs.wulff wulff<NEx=286.,NEy=341.> {
      prefs => <-.preferences;
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      model_type => <-.model_type;
    };
    CCP3.Core_Macros.UI.Glyphs.leed leed<NEx=477.,NEy=342.> {
      prefs => <-.preferences;
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      model_type => <-.model_type;
    };
    CCP3.Core_Macros.UI.Glyphs.edit_sphere edit_sphere<NEx=675.,NEy=342.> {
      prefs => <-.preferences;
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      model_type => <-.model_type;
    };
  };
};
