
flibrary Menus<NEeditable=1> {
  macro menu_list {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=88.,NEy=33.>;
    UIcmdList UIcmdList<NEportLevels={0,2},NEx=198.,NEy=88.>;
    // Doesn't have fontAttributes!
  };
  menu_list File_menu {
    UIcmdList {
      label = "&File";
      cmdList => {
	<-.load_separator,
	<-.exit_separator
      };
    };
    UImenuSeparator load_separator<NEx=363.,NEy=187.> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    UImenuSeparator exit_separator<NEx=374.,NEy=517.> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
  };
  menu_list Edit_menu {
    UIcmdList {
      label = "&Edit";
    };
  };
  menu_list Display_menu {
    UIcmdList {
      label = "&Display";
    };
  };
  menu_list GUIs_menu<hconnect=2> {
    UIcmdList+GMOD.hconnect {
      label = "&Calculate";
      direction = 1;
      accept = "calcCmds";
      accept_name = "cmdList";
    };
  };
  menu_list View_menu {
    UIcmdList {
      label = "&Viewers";
    };
  };
  menu_list Prefs_menu {
    UIcmdList {
      label = "&Preferences";
    };
  };
  macro Help_menu {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=198.,NEy=22.>;
    UIhelpCmdList UIhelpCmdList<NEportLevels={0,2},NEx=165.,NEy=99.> {
      label = "&Help";
      cmdList => {
	<-.About,
	<-.Documentation
      };
      // Doesn't have fontAttributes!
    };
    UIcmd About<NEx=253.,NEy=176.> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    UIcmd Documentation<NEx=253.,NEy=297.> {
      label => "Documentation";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    UIinfoDialog AboutDialog<NEx=462.,NEy=176.> {
      visible => <-.About.do;
      parent => <-.parent;
      message = "DL Visualize v3.0\nMain Author B.G.Searle\nSTFC Daresbury Laboratory";
      title => "About DL Visualize";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      cancelButton = 0;
      okLabelString = "Close";
    };
    link parent<NEportLevels={2,1},NEx=462.,NEy=66.>;
    string urlfile<NEportLevels=1,NEx=774.,NEy=72.> =>
      (getenv("DLVRC") + "/helpurl");
    group url<NEx=765.,NEy=216.> {
      string url<NEportLevels={1,2}>;
    };
    GMOD.load_v_script load_v_script<NEx=765.,NEy=144.> {
      filename => <-.urlfile;
      relative => <-.url;
    };
    CCP3.Core_Modules.Utils.browse browse<NEx=765.,NEy=468.> {
      trigger => <-.Documentation.do;
      run = 0;
      monitor = 0;
      browser => <-.browse_cmd;
      url => <-.url.url;
    };
#ifdef USE_LINUX
    // could search for BrowserApplication in ~/.kde/share/config/kdeglobals
    GMOD.shell_command shell_command<NEx=468.,NEy=288.> {
      command = "gconftool-2 -g /desktop/gnome/url-handlers/http/command";
      on_inst = 1;
    };
    string string<NEportLevels=1,NEx=468.,NEy=360.>[] =>
      str_array(shell_command.stdout_string, " ");
    string+nres browser<NEx=468.,NEy=405.> =>
      switch((array_size(string)>0)+1, "firefox", string[0]);
    string browse_cmd<NEportLevels=1,NEx=468.,NEy=450.> =>
      browser + " -new-window ";
#else
    string browse_cmd<NEportLevels=1,NEx=468.,NEy=414.>;
#endif // linux
  };
  macro Menu_bar<hconnect=2> {
    link preferences<NEportLevels={2,1},NEx=341.,NEy=33.>;
    UIcmdList menus<NEportLevels={0,2},NEx=143.,NEy=110.> {
      cmdList => {
	<-.File_menu.UIcmdList,
	<-.Edit_menu.UIcmdList,
	<-.Display_menu.UIcmdList,
	<-.GUIs_menu.UIcmdList,
	<-.View_menu.UIcmdList,
	<-.Prefs_menu.UIcmdList,
	<-.Help_menu.UIhelpCmdList
      };
      // Doesn't have fontAttributes!
    };
    CCP3.Core_Macros.UI.Menus.File_menu File_menu<NEx=374.,NEy=165.> {
      preferences => <-.preferences;
    };
    CCP3.Core_Macros.UI.Menus.Edit_menu Edit_menu<NEx=374.,NEy=242.> {
      preferences => <-.preferences;
    };
    CCP3.Core_Macros.UI.Menus.Display_menu Display_menu<NEx=374.,NEy=319.> {
      preferences => <-.preferences;
    };
    GUIs_menu GUIs_menu<hconnect=2,NEx=374.,NEy=396.> {
      preferences => <-.preferences;
    };
    CCP3.Core_Macros.UI.Menus.View_menu View_menu<NEx=539.,NEy=440.> {
      preferences => <-.preferences;
    };
    CCP3.Core_Macros.UI.Menus.Prefs_menu Prefs_menu<NEx=374.,NEy=484.> {
      preferences => <-.preferences;
    };
    CCP3.Core_Macros.UI.Menus.Help_menu Help_menu<NEx=374.,NEy=561.> {
      preferences => <-.preferences;
      parent<NEportLevels={3,1}>;
    };
  };
};
