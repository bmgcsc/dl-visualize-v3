
flibrary UIobjs<NEeditable=1,
		export_cxx=1,
		build_dir="avs/src/express",
#ifdef DLV_NO_DLLS
		dyn_libs="libCCP3dlv",
#endif // DLV_NO_DLLS
#ifdef MSDOS
		cxx_hdr_files="avs/src/core/fb_gen.hxx nt_ui/ui_gen.h avs/src/express/misc.hxx",
#else
		cxx_hdr_files="avs/src/core/fb_gen.hxx motif_ui/ui/ui_core.h avs/src/express/misc.hxx",
#endif // MSDOS
		out_hdr_file="ui_objs.hxx",
		out_src_file="ui_objs.cxx"> {
  UItemplateDialog DLVdialog;
  UIradioBoxLabel DLVradioBox {
    link color;
    link fontAttributes<NEportLevels=1,NEx=440.,NEy=22.>;
    UIpanel {
      &color => <-.color;
      &fontAttributes => <-.fontAttributes;
    };
    UIlabel {
      &color => <-.color;
      &fontAttributes => <-.fontAttributes;
    };
    UIradioBox {
      &color => <-.color;
      &fontAttributes => <-.fontAttributes;
    };
  };
  UIoptionBoxLabel DLVoptionBox {
    link color;
    link fontAttributes<NEportLevels=1,NEx=440.,NEy=22.>;
    UIpanel {
      &color => <-.color;
      &fontAttributes => <-.fontAttributes;
    };
    UIlabel {
      &color => <-.color;
      &fontAttributes => <-.fontAttributes;
    };
    UIoptionBox {
      &color => <-.color;
      &fontAttributes => <-.fontAttributes;
    };
  };
  UItext DLVtext {
    updateMode = 7;
#ifndef MSDOS
    height = 34;
#endif // MSDOS
  };
  UItextTypein DLVtextType {
    link color;
    link fontAttributes<NEportLevels=1,NEx=451.,NEy=22.>;
    int width;
    int height => panel.height;
    int visible;
    int x = 0;
    int y;
    panel {
      &color => <-.color;
      &fontAttributes => <-.fontAttributes;
      width => <-.width;
      visible => <-.visible;
      x => <-.x;
      y => <-.y;
    };
    text_label {
      &color => <-.color;
      &fontAttributes => <-.fontAttributes;
    };
    text {
      &color => <-.color;
      &fontAttributes => <-.fontAttributes;
    };
  };
  DLVtextType DLVtextTypein {
    text {
      updateMode = 7;
#ifndef MSDOS
      height = 34;
#endif // MSDOS
    };
  };
  UItoggle DLVtoggle;
  UIlist DLVlist;
  UImultiList DLVmultiList;
  UIslider DLVfSlider {
#ifdef MSDOS
    height = 36;
#endif // MSDOS
  };
  UIslider DLViSlider {
    mode = "integer";
#ifdef MSDOS
    height = 36;
#endif // MSDOS
  };
  UIfield DLVffield {
    updateMode = 7;
  };
  UIfield DLVifield {
    updateMode = 7;
    mode = "integer";
  };
  UIfieldTypein DLVfieldType {
    link color;
    link fontAttributes<NEportLevels=1,NEx=451.,NEy=22.>;
    panel {
      &color => <-.color;
      &fontAttributes => <-.fontAttributes;
    };
    label {
      &color => <-.color;
      &fontAttributes => <-.fontAttributes;
    };
    field {
      &color => <-.color;
      &fontAttributes => <-.fontAttributes;
    };
  };
  DLVfieldType DLVffieldTypein {
    field {
      updateMode = 7;
    };
  };
  DLVfieldType DLVifieldTypein {
    field {
      mode = "integer";
      updateMode = 7;
    };
  };
  VUIslider_typein DLVslider_typein {
    link color;
    link fontAttributes<NEportLevels=1,NEx=495.,NEy=132.>;
    button {
      &color => <-.color;
      &fontAttributes => <-.fontAttributes;
    };
    valEditor {
      UI {
	editor_shell {
	  &color => <-.<-.<-.color;
	  &fontAttributes => <-.<-.<-.fontAttributes;
	  parent+nres => <-.<-.<-.silder.parent;
	  // Often these are (0,0) within some other panel.
	  //x+nres => <-.<-.<-.silder.parent.x;
	  //y+nres => <-.<-.<-.silder.parent.y;
	};
	close_button {
	  &color => <-.<-.<-.color;
	  &fontAttributes => <-.<-.<-.fontAttributes;
	};
	value_label {
	  &color => <-.<-.<-.color;
	  &fontAttributes => <-.<-.<-.fontAttributes;
	};
	value_field {
	  updateMode = 7;
	  &color => <-.<-.<-.color;
	  &fontAttributes => <-.<-.<-.fontAttributes;
	};
	min_label {
	  &color => <-.<-.<-.color;
	  &fontAttributes => <-.<-.<-.fontAttributes;
	};
	min_field {
	  updateMode = 7;
	  &color => <-.<-.<-.color;
	  &fontAttributes => <-.<-.<-.fontAttributes;
	};
	max_label {
	  &color => <-.<-.<-.color;
	  &fontAttributes => <-.<-.<-.fontAttributes;
	};
	max_field {
	  updateMode = 7;
	  &color => <-.<-.<-.color;
	  &fontAttributes => <-.<-.<-.fontAttributes;
	};
	precision_label {
	  &color => <-.<-.<-.color;
	  &fontAttributes => <-.<-.<-.fontAttributes;
	};
	precision_field {
	  &color => <-.<-.<-.color;
	  &fontAttributes => <-.<-.<-.fontAttributes;
	};
      };
    };
    width => ((slider.parent.clientWidth - slider.width) - 2);
  };
  UIscrolledWindow DLVscrolledWindow;
  macro DLVshell {
    link color;
    link fontAttributes<NEportLevels=1,NEx=627.,NEy=55.>;
    string title<NEportLevels=1,NEx=352.,NEy=33.>;
    UIshell UIshell<NEx=132.,NEy=88.> {
      visible<NEportLevels={3,0}>;
      showStatusBar = 0;
      title => <-.title;
      &color => <-.color;
      &fontAttributes => <-.fontAttributes;
    };
    UIpanel panel<NEx=385.,NEy=154.> {
      parent => <-.UIshell;
      width => <-.UIshell.clientWidth;
      height => <-.UIshell.clientHeight;
      &color => <-.color;
      &fontAttributes => <-.fontAttributes;
    };
    UIframe UIpanel<NEx=693.,NEy=220.,NEportLevels={0,2}> {
      parent => <-.panel;
      width => <-.panel.clientWidth;
      height => (<-.panel.clientHeight - 45);
      &color => <-.color;
      &fontAttributes => <-.fontAttributes;
    };
    UIbutton close<NEx=495.,NEy=220.> {
      parent => <-.panel;
      x => ((<-.panel.clientWidth - .width) / 2);
      y => (<-.panel.clientHeight - 40);
      height = 35;
      width = 80;
      label => "Close";
      &color => <-.color;
      &fontAttributes => <-.fontAttributes;
    };
    GMOD.parse_v parse_v<NEx=462.,NEy=308.> {
      v_commands = "UIshell.visible = 0;";
      trigger => <-.close.do;
      on_inst = 0;
      relative => <-;
    };
  };
  macro DirectoryBrowser {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=253.,NEy=55.>;
    link location<NEportLevels={2,1},NEx=341.,NEy=143.>;
    string title<NEportLevels=1,NEx=561.,NEy=143.>;
    int y<NEportLevels=1,NEx=572.,NEy=55.>;
    UIpanel UIpanel<NEx=110.,NEy=209.> {
      parent<NEportLevels={3,0}>;
      width => parent.clientWidth;
      x = 0;
      y => <-.y;
#ifdef MSDOS
      height => <-.Browse.height;
#else
      height => <-.name.height;
#endif // MSDOS
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext name<NEx=264.,NEy=297.> {
      parent => <-.UIpanel;
      x = 5;
      width => ((parent.clientWidth - Browse.width) - 15);
      text => <-.location;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
#ifdef MSDOS
      y => ((parent.height - .height) / 2);
#else
      y = 0;
#endif // MSDOS
    };
    UIbutton Browse<NEx=264.,NEy=396.> {
      parent => <-.UIpanel;
      x => (name.width + 10);
#ifdef MSDOS
      y = 0;
#else
      y => ((parent.height - .height) / 2);
#endif // MSDOS
      y => ((<-.name.height - .height) / 2);
      label => "Browse...";
      width = 100;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      do = 0;
    };
    UIdirectoryDialog UIdirectoryDialog<NEx=517.,NEy=473.> {
      parent => <-.UIpanel;
      visible => <-.Browse.do;
      title => <-.title;
      filename => <-.location;
      okButton = 1;
      ok = 0;
      cancelButton = 1;
      cancel = 0;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
  };
  /*macro FileBrowser {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=253.,NEy=55.>;
    CCP3.Core_Modules.Utils.FBdata &FBdata<NEx=539.,NEy=77.> {
      parent1<NEportLevels={0,2}>;
      parent2<NEportLevels={0,2}>;
      filename<NEportLevels={0,2}>;
      title<NEportLevels={0,2}>;
      pattern<NEportLevels={0,2}>;
      file_write<NEportLevels={0,2}>;
      x<NEportLevels={0,2}>;
      y<NEportLevels={0,2}>;
      height<NEportLevels={0,2}>;
      ok<NEportLevels={0,2}>;
    };
    UIpanel UIpanel<NEx=110.,NEy=209.> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      parent => <-.FBdata.parent1;
      y => <-.FBdata.y;
      x => <-.FBdata.x;
      width+nres => parent.clientWidth;
      height => <-.FBdata.height;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext DLVtext<NEx=264.,NEy=297.> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      parent => <-.UIpanel;
      text => <-.FBdata.filename;
      x = 5;
      y = 0;
      width => ((parent.clientWidth - UIbutton1.width) - 15);
    };
    int visible<NEportLevels=1,NEx=638.,NEy=297.> = 0;
    //int visible<NEportLevels=1,NEx=638.,NEy=297.> => copy_on_change.output;
    UIbutton UIbutton1<NEx=264.,NEy=396.> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      parent => <-.UIpanel;
      x => (DLVtext.width + 10);
      y => ((<-.DLVtext.height - .height) / 2);
      label => "Browse...";
      width = 100;
      do = 0;
    };
    UIbutton UIbutton2<NEx=726.,NEy=407.> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      parent+nres => <-.FBdata.parent2;
      label => <-.FBdata.label;
#ifdef MSDOS
      height => <-.FBdata.height;
      x => <-.FBdata.x;
      y => <-.FBdata.y;
      width => <-.FBdata.width;
#endif // MSDOS
      do = 0;
    };
    GMOD.parse_v parse_v<NEx=319.,NEy=528.> {
      v_commands = "visible = 1;";
      trigger => (<-.UIbutton1.do | <-.UIbutton2.do);
      on_inst = 0;
      relative => <-;
    };
    UIfileDialog UIfileDialog<NEx=517.,NEy=473.> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => <-.visible;
      title => <-.FBdata.title;
      parent => <-.UIpanel;
      isModal = 1;
      okButton = 1;
      cancelButton = 1;
      ok => <-.FBdata.ok;
      cancel = 0;
      filename => <-.FBdata.filename;
      confirmFileWrite => <-.FBdata.file_write;
      searchPattern => <-.FBdata.pattern;
    };
    };*/
  macro FileBase {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},NEx=253.,NEy=55.>;
    int x<NEportLevels=1,NEx=110.,NEy=99.>;
    int y<NEportLevels=1,NEx=297.,NEy=99.>;
    link parent<NEportLevels={2,1},NEx=451.,NEy=55.>;
    string filename<NEportLevels=1,NEx=869.,NEy=99.> = "";
    string title<NEportLevels=1,NEx=253.,NEy=143.>;
    string pattern<NEportLevels=1,NEx=451.,NEy=143.> = "*";
    boolean file_write<NEportLevels=1,NEx=660.,NEy=143.> = 0;
    string label<NEportLevels=1,NEx=847.,NEy=143.> = "Browse...";
#ifdef MSDOS // Todo - set for MSDOS?
    int height<NEportLevels=1,NEx=484.,NEy=99.> = 34;
#else // height matches DLVtext object height
    int height<NEportLevels=1,NEx=484.,NEy=99.> = 34;
#endif // MSDOS
    int visible<NEportLevels=1,NEx=638.,NEy=297.> = 0;
    UIbutton UIbutton<NEx=264.,NEy=396.> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label => <-.label;
      do => <-.visible;
    };
    UIfileDialog UIfileDialog<NEx=517.,NEy=473.> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => <-.visible;
      title => <-.title;
      isModal = 1;
      okButton = 1;
      cancelButton = 1;
      ok = 0;
      cancel = 0;
      filename => <-.filename;
      confirmFileWrite => <-.file_write;
      searchPattern => <-.manageFB.pattern;
    };
    CCP3.Core_Modules.Utils.manageFB manageFB<NEx=484.,NEy=363.> {
      trigger => <-.visible;
      filename => <-.filename;
      wildcard => <-.pattern;
      funit = "fort.*";
      use_funit = 0;
      pattern = "*";
    };
  };
  FileBase FileSave {
    int width;
    UIbutton {
      parent => <-.parent;
#ifdef MSDOS
      //height => <-.height;
      x => <-.x;
      y => <-.y;
      width => <-.width;
#endif // MSDOS
    };
    UIfileDialog {
      parent => <-.parent;
    };
  };
  FileBase FileBrowser {
    UIpanel UIpanel<NEx=110.,NEy=209.> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      parent => <-.parent;
      y => <-.y;
      x => <-.x;
      width+nres => parent.clientWidth;
      height => <-.height;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext DLVtext<NEx=264.,NEy=297.> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      parent => <-.UIpanel;
      text => <-.filename;
      x = 5;
      y => ((parent.height - .height) / 2);
      width => ((parent.clientWidth - UIbutton.width) - 15);
    };
    UIbutton {
      parent => <-.UIpanel;
      x => (DLVtext.width + 10);
      y => ((parent.height - .height) / 2);
      width = 100;
    };
    UIfileDialog {
      parent => <-.UIpanel;
    };
  };
  macro DirBase {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},NEx=253.,NEy=55.>;
    int x<NEportLevels=1,NEx=110.,NEy=99.>;
    int y<NEportLevels=1,NEx=297.,NEy=99.>;
    link parent<NEportLevels={2,1},NEx=451.,NEy=55.>;
    string filename<NEportLevels=1,NEx=869.,NEy=99.> = "";
    string title<NEportLevels=1,NEx=253.,NEy=143.>;
    string label<NEportLevels=1,NEx=847.,NEy=143.> = "Browse...";
    string pattern<NEportLevels=1,NEx=451.,NEy=143.> = "*";
#ifdef MSDOS // Todo - set for MSDOS?
    int height<NEportLevels=1,NEx=484.,NEy=99.> = 34;
#else // height matches DLVtext object height
    int height<NEportLevels=1,NEx=484.,NEy=99.> = 34;
#endif // MSDOS
    int visible<NEportLevels=1,NEx=638.,NEy=297.> = 0;
    UIbutton UIbutton<NEx=264.,NEy=396.> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label => <-.label;
      do => <-.visible;
    };
    UIdirectoryDialog UIdirectoryDialog<NEx=517.,NEy=473.> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => <-.visible;
      title => <-.title;
      isModal = 1;
      okButton = 1;
      cancelButton = 1;
      ok = 0;
      cancel = 0;
      filename => <-.filename;
      searchPattern => <-.manageFB.pattern;
    };
    CCP3.Core_Modules.Utils.manageFB manageFB<NEx=484.,NEy=363.> {
      trigger => <-.visible;
      filename => <-.filename;
      wildcard => <-.pattern;
      //funit = "fort.*";
      //use_funit = 0;
      pattern = "*";
    };
  };
  DirBase DirSave {
    UIpanel UIpanel<NEx=110.,NEy=209.> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      parent => <-.parent;
      y => <-.y;
      x => <-.x;
      width+nres => parent.clientWidth;
      height => <-.height;
      visible => <-.visible;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext DLVtext<NEx=264.,NEy=297.> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      parent => <-.UIpanel;
      text => <-.filename;
      x = 5;
      y => ((parent.height - .height) / 2);
      width => ((parent.clientWidth - UIbutton.width) - 15);
    };
    UIbutton {
      parent => <-.UIpanel;
      x => (DLVtext.width + 10);
      y => ((parent.height - .height) / 2);
      width = 100;
    };
    UIdirectoryDialog {
      parent => <-.UIpanel;
    };
  };
  macro FilePanel {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},NEx=253.,NEy=55.>;
    int x<NEportLevels=1,NEx=110.,NEy=99.>;
    int y<NEportLevels=1,NEx=297.,NEy=99.>;
    link parent<NEportLevels={2,1},NEx=451.,NEy=55.>;
    string filename<NEportLevels=1,NEx=869.,NEy=99.> = "";
    string title<NEportLevels=1,NEx=253.,NEy=143.>;
    string pattern<NEportLevels=1,NEx=451.,NEy=143.> = "*";
    boolean file_write<NEportLevels=1,NEx=660.,NEy=143.> = 0;
    string label<NEportLevels=1,NEx=847.,NEy=143.> = "Browse...";
#ifdef MSDOS // Todo - set for MSDOS?
    int height<NEportLevels=1,NEx=484.,NEy=99.> = 34;
#else // height matches DLVtext object height
    int height<NEportLevels=1,NEx=484.,NEy=99.> = 34;
#endif // MSDOS
    int visible<NEportLevels=1,NEx=638.,NEy=297.> = 0;
    UIpanel UIpanel<NEx=110.,NEy=209.> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      parent => <-.parent;
      y => <-.y;
      x => <-.x;
      width+nres => parent.clientWidth;
      height => <-.height;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext DLVtext<NEx=264.,NEy=297.> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      parent => <-.UIpanel;
      text => <-.filename;
      x = 5;
      y => ((parent.height - height) / 2);
      width => ((parent.clientWidth - UIbutton.width) - 15);
    };
    UIbutton UIbutton<NEx=264.,NEy=396.> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label => <-.label;
      do => <-.visible;
      parent => <-.UIpanel;
      x => (DLVtext.width + 10);
      y => ((parent.height - .height) / 2);
      width = 100;
    };
    UIfileDialog UIfileDialog<NEx=517.,NEy=473.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => <-.visible;
      title => <-.title;
      isModal = 1;
      okButton = 1;
      cancelButton = 1;
      ok = 0;
      cancel = 0;
      filename => <-.filename;
      confirmFileWrite => <-.file_write;
      searchPattern => <-.pattern;
    };
  };
};
