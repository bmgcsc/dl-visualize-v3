
flibrary Prefs<NEeditable=1> {
  macro config {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1}>;
    int visible = 0;
    int active_menu;
    link UIparent;
    UIcmd menu<NEx=9.,NEy=117.> {
      active => <-.active_menu;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label => "Configure menu";
      do => <-.visible;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVdialog DLVdialog<NEx=154.,NEy=165.> {
      visible => <-.visible;
      parent => <-.UIparent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      okButton = 1;
      ok = 0;
      cancelButton = 1;
      cancel = 0;
      okLabelString = "Save";
      width = 400;
      height = 350;
#ifdef MSDOS
      y = 0;
#endif // MSDOS
      title = "Configure calculation menu";
    };
    UIpanel panel<NEx=300.,NEy=165.> {
      parent => <-.DLVdialog;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle CRYSTAL<NEx=297.,NEy=216.> {
      parent => <-.panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y = 0;
      width => (parent.clientWidth / 2);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle EXCURV<NEx=459.,NEy=216.> {
      parent => <-.panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y => ((<-.CRYSTAL_prefs.y + <-.CRYSTAL_prefs.height) + 5);
      width => (parent.clientWidth / 2);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle GULP<NEx=621.,NEy=216.> {
      parent => <-.panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y => ((<-.EXCURV.y + <-.EXCURV.height) + 5);
      width => (parent.clientWidth / 2);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle ROD<NEx=774.,NEy=216.> {
      parent => <-.panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y => ((<-.GULP.y + <-.GULP.height) + 5);
      width => (parent.clientWidth / 2);
    };
    /*
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle CDS<NEx=850.,NEy=216.> {
      parent => <-.panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y => ((<-.ROD.y + <-.ROD.height) + 5);
      width => (parent.clientWidth / 2);
    };
    */
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle ONETEP<NEx=927.,NEy=261.> {
      parent => <-.panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y => ((<-.ROD.y + <-.ROD.height) + 5);
      width => (parent.clientWidth / 2);
    };
    UIbutton CRYSTAL_prefs<NEx=297.,NEy=450.> {
      x => (parent.clientWidth / 2);
      y => ((<-.CRY21.y + <-.CRY21.height) + 5);
      visible => (<-.CRY98.set | <-.CRY03.set | <-.CRY06.set |
		  <-.CRY09.set | <-.CRY14.set | <-.CRY17.set | <-.CRY21.set);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      parent => <-.panel;
      label = "CRYSTAL location";
      width = 150;
    };
    UIbutton EXCURV_prefs<NEx=468.,NEy=450.> {
      x => (parent.clientWidth / 2);
      y => <-.EXCURV.y;
      visible => <-.EXCURV.set;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      parent => <-.panel;
      label = "EXCURV location";
      width = 150;
    };
    UIbutton GULP_prefs<NEx=630.,NEy=450.> {
      x => (parent.clientWidth / 2);
      y => <-.GULP.y;
      visible => <-.GULP.set;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      parent => <-.panel;
      label = "GULP location";
      width = 150;
    };
    UIbutton ONETEP_prefs<NEx=918.,NEy=450.> {
      x => (parent.clientWidth / 2);
      y => <-.ONETEP.y;
      visible => <-.ONETEP.set;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      parent => <-.panel;
      label = "ONETEP location";
      width = 150;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle CRY98<NEx=297.,NEy=315.> {
      visible => <-.CRYSTAL.set;
      parent => <-.panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x => (parent.clientWidth / 2);
      y = 0;
      width => (parent.clientWidth / 2) - 10;
      label = "CRYSTAL98";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle CRY03<NEx=459.,NEy=315.> {
      visible => <-.CRYSTAL.set;
      parent => <-.panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x => (parent.clientWidth / 2);
      y => (<-.CRY98.y + <-.CRY98.height);
      width => (parent.clientWidth / 2) - 10;
      label = "CRYSTAL03";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle CRY06<NEx=621.,NEy=315.> {
      visible => <-.CRYSTAL.set;
      parent => <-.panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x => (parent.clientWidth / 2);
      y => (<-.CRY03.y + <-.CRY03.height);
      width => (parent.clientWidth / 2) - 10;
      label = "CRYSTAL06";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle CRY09<NEx=774.,NEy=315.> {
      visible => <-.CRYSTAL.set;
      parent => <-.panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x => (parent.clientWidth / 2);
      y => (<-.CRY06.y + <-.CRY06.height);
      width => (parent.clientWidth / 2) - 10;
      label = "CRYSTAL09";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle CRY14<NEx=936.,NEy=315.> {
      visible => <-.CRYSTAL.set;
      parent => <-.panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x => (parent.clientWidth / 2);
      y => (<-.CRY09.y + <-.CRY09.height);
      width => (parent.clientWidth / 2) - 10;
      label = "CRYSTAL14";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle CRY17<NEx=1100.,NEy=315.> {
      visible => <-.CRYSTAL.set;
      parent => <-.panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x => (parent.clientWidth / 2);
      y => (<-.CRY14.y + <-.CRY14.height);
      width => (parent.clientWidth / 2) - 10;
      label = "CRYSTAL17";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle CRY17<NEx=1300.,NEy=315.> {
      visible => <-.CRYSTAL.set;
      parent => <-.panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x => (parent.clientWidth / 2);
      y => (<-.CRY17.y + <-.CRY17.height);
      width => (parent.clientWidth / 2) - 10;
      label = "CRYSTAL21";
    };
    GMOD.parse_v parse_v<NEx=639.,NEy=117.> {
      v_commands = "CRYSTAL.set = CRY98.set | CRY03.set | CRY06.set"
	+ " | CRY09.set | CRY14.set | CRY17.set | CRY21.set;";
      trigger => <-.visible;
      on_inst = 0;
      relative => <-;
    };
    CCP3.Core_Modules.Prefs.save save<NEx=54.,NEy=324.> {
      trigger => <-.DLVdialog.ok;
    };
    CCP3.Core_Modules.Prefs.open_panel open_crystal<NEx=297.,NEy=504.> {
      trigger => <-.CRYSTAL_prefs.do;
      name = "CRYSTAL";
    };
    CCP3.Core_Modules.Prefs.open_panel open_excurv<NEx=468.,NEy=504.> {
      trigger => <-.EXCURV_prefs.do;
      name = "EXCURV";
    };
    CCP3.Core_Modules.Prefs.open_panel open_gulp<NEx=630.,NEy=504.> {
      trigger => <-.GULP_prefs.do;
      name = "GULP";
    };
    CCP3.Core_Modules.Prefs.open_panel open_onetep<NEx=918.,NEy=504.> {
      trigger => <-.ONETEP_prefs.do;
      name = "ONETEP";
   };
  };
  macro bonds {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1}>;
    int visible = 0;
    int active_menu;
    link UIparent;
    UIcmd menu<NEx=9.,NEy=117.> {
      active => <-.active_menu;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label => "Bonds";
      do => <-.visible;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVdialog DLVdialog<NEx=154.,NEy=165.> {
      visible => <-.visible;
      parent => <-.UIparent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      okButton = 1;
      ok = 0;
      cancelButton = 0;
      cancel = 0;
      okLabelString = "Close";
#ifdef MSDOS
      y = 0;
#endif // MSDOS
      title = "Configure default bonds";
    };
    UIpanel panel<NEx=300.,NEy=165.> {
      parent => <-.DLVdialog;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle bonds<NEx=297.,NEy=216.> {
      parent => <-.panel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y = 0;
      width => parent.clientWidth;
      label = "Bond all atoms";
    };
  };
  macro menu_prefs {
    link preferences<NEportLevels={2,1},NEx=363.,NEy=77.>;
    int active_menus<NEportLevels={2,1},NEx=572.,NEy=77.>;
    int dialog_visible<NEportLevels=1,NEx=781.,NEy=77.> =>
      ((config.visible == 1) || (bonds.visible == 1));
    link UIparent<NEportLevels={2,1},NEx=759.,NEy=44.>;
    CCP3.Core_Macros.UI.Prefs.config config<NEx=374.,NEy=187.> {
      preferences => <-.preferences;
      active_menu => <-.active_menus;
      //UIparent => <-.UIparent;
    };
    CCP3.Core_Macros.UI.Prefs.bonds bonds<NEx=576.,NEy=189.> {
      preferences => <-.preferences;
      active_menu => <-.active_menus;
    };
  };
  macro prefsUI {
    link preferences<NEportLevels={2,1},NEx=550.,NEy=55.>;
    int active_menus<NEportLevels={2,1},NEx=308.,NEy=132.>;
    int dialog_visible<NEportLevels=1,NEx=539.,NEy=132.> =>
      (menu_prefs.dialog_visible == 1);
    link UIparent<NEportLevels={2,1},NEx=770.,NEy=55.>;
    CCP3.Core_Macros.UI.Prefs.menu_prefs menu_prefs<NEx=605.,NEy=253.> {
      preferences => <-.preferences;
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
    };
  };
};
