
flibrary Project<NEeditable=1> {
  macro dirUI {
    CCP3.Core_Macros.UI.UIobjs.DLVdialog DLVdialog<NEx=154.,NEy=165.> {
      visible => <-.visible;
      parent => <-.UIparent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      okButton = 1;
      ok = 0;
      cancelButton = 1;
      cancel = 0;
#ifdef MSDOS
      y = 0;
#endif // MSDOS
    };
    link UIparent<NEportLevels={2,1},NEx=275.,NEy=55.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=473.,NEy=55.>;
    link visible<NEportLevels={2,1},NEx=99.,NEy=55.>;
    UIpanel UIpanel<NEx=374.,NEy=198.> {
      parent => <-.DLVdialog;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    UIlabel label2<NEx=594.,NEy=261.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      label = "Location (directory)";
    };
    int y<NEportLevels=1,NEx=671.,NEy=209.> => (label2.y + label2.height + 5);
    GMOD.parse_v parse_v<NEx=627.,NEy=374.>;
  };
  dirUI nameUI {
    DLVdialog {
      okButton => (strlen(<-.name) > 0);
    };
    link name<NEportLevels={2,1},NEx=649.,NEy=55.>;
    UIlabel label1<NEx=198.,NEy=270.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      y = 0;
      label = "Project Name";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext DLVtext<NEx=396.,NEy=264.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      y => (<-.label1.y + <-.label1.height + 5);
      text => <-.name;
    };
    label2 {
      y => ((DLVtext.y + DLVtext.height) + 5);
    };
    //y => ((DLVtext.y + DLVtext.height) + 5);
  };
  dirUI loadUI {
    //y = 0;
    CCP3.Core_Modules.Project.check_save check_save<NEx=572.,NEy=132.> {
      test => <-.visible;
      warn = 0;
    };
    UIwarningDialog UIwarningDialog<NEx=869.,NEy=198.> {
      parent => <-.UIparent;
      visible => <-.check_save.warn;
      isModal = 1;
      okButton = 1;
      ok = 0;
      cancelButton = 0;
      title = "Warning";
      message = "Project contains unsaved data.\n"
	+ "If you continue this will be lost";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    label2 {
      y = 0;
    };
  };
  nameUI newUI {
    CCP3.Core_Modules.Project.check_save check_save<NEx=572.,NEy=132.> {
      test => visible;
      warn = 0;
    };
    UIwarningDialog UIwarningDialog<NEx=869.,NEy=198.> {
      parent => <-.UIparent;
      visible => <-.check_save.warn;
      isModal = 1;
      okButton = 1;
      cancelButton = 0;
      ok = 0;
      title = "Warning";
      message = "Project contains unsaved data.\n"
	+ "If you continue this will be lost";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle DLVtoggle<NEx=429.,NEy=396.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      label = "Use current defaults";
    };
  };
  macro dir_ops {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=242.,NEy=33.>;
    int active_menu<NEportLevels={2,1},NEx=561.,NEy=33.>;
    link UIparent<NEportLevels={2,1},NEx=759.,NEy=33.>;
    link user_project<NEportLevels={2,1},NEx=396.,NEy=33.>;
    int visible<NEportLevels=1,NEx=704.,NEy=187.> =>
      ((vis_new == 1) || (vis_load == 1) || (vis_save_as == 1));
    string name<NEportLevels=1,NEx=187.,NEy=187.> = "";
    string directory<NEportLevels=1,NEx=451.,NEy=187.> = "";
    UIcmd create<NEx=176.,NEy=99.> {
      active => <-.active_menu;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label => "New Project";
      do => <-.vis_new;
    };
    UIcmd load<NEx=374.,NEy=99.> {
      active => <-.active_menu;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label => "Load Project";
      do => <-.vis_load;
    };
    UIcmd save<NEx=572.,NEy=99.> {
      active => (<-.active_menu && (user_project == 1));
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label => "Save Project";
    };
    UIcmd save_as<NEx=759.,NEy=99.> {
      active => <-.active_menu;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label => "Save Project As";
      do => <-.vis_save_as;
    };
    int vis_new<NEportLevels=1,NEx=176.,NEy=308.> = 0;
    int vis_load<NEportLevels=1,NEx=363.,NEy=308.> = 0;
    int vis_save_as<NEportLevels=1,NEx=561.,NEy=308.> = 0;
    GMOD.instancer instance_new<NEx=176.,NEy=418.> {
      Value => <-.vis_new;
      Group => <-.newUI;
    };
    GMOD.instancer instance_load<NEx=374.,NEy=418.> {
      Value => <-.vis_load;
      Group => <-.loadUI;
    };
    GMOD.instancer instance_save_as<NEx=561.,NEy=418.> {
      Value => <-.vis_save_as;
      Group => <-.saveUI;
    };
    GMOD.instancer instance_dir<NEx=770.,NEy=308.> {
      Value => <-.visible;
      Group => <-.DirBrowser;
    };
    newUI newUI<NEx=176.,NEy=528.,instanced=0> {
      UIparent => <-.UIparent;
      preferences => <-.preferences;
      visible => <-.vis_new;
      name => <-.name;
      DLVdialog {
	title = "New Project";
      };
      parse_v {
	v_commands = "DirParent = newUI.UIpanel; DirTitle = \"New Project Directory\"; DirY = newUI.y;";
	relative => <-.<-;
      };
      DLVtoggle {
	y => (<-.<-.DirY + <-.<-.DirBrowser.UIpanel.height + 5);
	visible => (<-.<-.user_project == 1);
	set => <-.<-.inherit_prefs;
      };
    };
    CCP3.Core_Macros.UI.Project.loadUI loadUI<NEx=374.,NEy=528.,instanced=0> {
      UIparent => <-.UIparent;
      preferences => <-.preferences;
      visible => <-.vis_load;
      DLVdialog {
	title = "Load Project";
	okButton => (strlen(<-.<-.directory) > 0);
      };
      parse_v {
	v_commands = "DirParent = loadUI.UIpanel; DirTitle = \"Load Project Directory\"; DirY = loadUI.y;";
	relative => <-.<-;
      };
    };
    CCP3.Core_Macros.UI.Project.nameUI saveUI<NEx=572.,NEy=528.,instanced=0> {
      UIparent => <-.UIparent;
      preferences => <-.preferences;
      visible => <-.vis_save_as;
      name => <-.name;
      DLVdialog {
	title = "Save Project As";
      };
      parse_v {
	v_commands = "DirParent = saveUI.UIpanel; DirTitle = \"Save Project Directory\"; DirY = saveUI.y;";
	relative => <-.<-;
      };
    };
    int DirY<NEportLevels=1,NEx=583.,NEy=363.>;
    string DirTitle<NEportLevels=1,NEx=374.,NEy=363.>;
    link DirParent<NEportLevels=1,NEx=187.,NEy=363.>;
    CCP3.Core_Macros.UI.UIobjs.DirectoryBrowser DirBrowser<NEx=781.,NEy=407.,
      instanced=0> {
      preferences => <-.preferences;
      location => <-.directory;
      UIpanel {
	parent => <-.<-.DirParent;
      };
      y => <-.DirY;
      title => <-.DirTitle;
    };
    boolean inherit_prefs<NEportLevels=1,NEx=55.,NEy=242.> = 0;
    link atom_prefs<NEportLevels=1,NEx=385.,NEy=242.>;
    link display_prefs<NEportLevels=1,NEx=627.,NEy=242.>;
    CCP3.Core_Modules.Project.project project<NEx=473.,NEy=616.> {
      run_new => <-.newUI.DLVdialog.ok;
      run_load => <-.loadUI.DLVdialog.ok;
      run_save => <-.save.do;
      run_save_as => <-.saveUI.DLVdialog.ok;
      name => <-.name;
      directory => <-.directory;
      inherit => <-.inherit_prefs;
      atoms => <-.atom_prefs;
      display => <-.display_prefs;
    };
  };
  macro subUI {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=209.,NEy=44.>;
    link visible<NEportLevels={2,1},NEx=407.,NEy=44.>;
    link UIparent<NEportLevels={2,1},NEx=660.,NEy=44.>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog DLVdialog<NEx=165.,NEy=187.> {
      visible => <-.visible;
      parent => <-.UIparent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      title = "Create Sub Project";
      ok = 0;
      cancelButton = 1;
      cancel = 0;
#ifdef MSDOS
      y = 0;
#endif // MSDOS
    };
    UIpanel UIpanel<NEx=374.,NEy=264.> {
      parent => <-.DLVdialog;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    UIlabel name_label<NEx=726.,NEy=297.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      y = 0;
      label = "Sub-Project Name";
      alignment = "center";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext project_name<NEx=572.,NEy=341.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      y => (<-.name_label.y + name_label.height + 5);
    };
    UIlabel dir_label<NEx=737.,NEy=396.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      y => (<-.project_name.y + project_name.height + 5);
      label = "Directory Name";
      alignment = "center";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext directory<NEx=572.,NEy=440.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      y => (<-.dir_label.y + dir_label.height + 5);
    };
  };
  macro sub_project {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=242.,NEy=33.>;
    int active_menu<NEportLevels={2,1},NEx=539.,NEy=33.>;
    link UIparent<NEportLevels={2,1},NEx=759.,NEy=33.>;
    int visible<NEportLevels=1,NEx=528.,NEy=110.> = 0;
    string name<NEportLevels=1,NEx=506.,NEy=209.> = "";
    string directory<NEportLevels=1,NEx=704.,NEy=209.> = "";
    link user_project<NEportLevels=1,NEx=737.,NEy=110.>;
    GMOD.instancer instancer<NEx=242.,NEy=220.> {
      Value => <-.visible;
      Group => <-.subUI;
    };
    UIcmd menu<NEx=176.,NEy=99.> {
      label = "Create Sub-Project";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      active => (<-.active_menu && (user_project == 1));
      do => <-.visible;
    };
    subUI subUI<NEx=462.,NEy=264.,instanced=0> {
      visible => <-.visible;
      preferences => <-.preferences;
      UIparent => <-.UIparent;
      DLVdialog {
	okButton => (strlen(<-.<-.name) > 0);
      };
      project_name {
	text => <-.<-.name;
      };
      directory {
	text => <-.<-.directory;
      };
    };
    CCP3.Core_Modules.Project.subproject subproject<NEx=506.,NEy=396.> {
      make => <-.subUI.DLVdialog.ok;
      name => <-.name;
      directory => <-.directory;
    };
  };
  macro selectUI {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=231.,NEy=66.>;
    link UIparent<NEportLevels={2,1},NEx=440.,NEy=66.>;
    link visible<NEportLevels={2,1},NEx=638.,NEy=66.>;
    link show_all<NEportLevels=1,NEx=748.,NEy=165.>;
    string op_list<NEportLevels=1,NEx=836.,NEy=319.>[] = {"None","AND","OR"};
    string filter_choices<NEportLevels=1,NEx=99.,NEy=352.>[] = {
      "None",
      "Subprojects",
      "Geometry Changes",
      "Load Models",
      "Load Data",
      "Calculations"
    };
    mlink items<NEportLevels={2,1},NEx=539.,NEy=165.>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog DLVdialog<NEx=176.,NEy=176.> {
      visible => <-.visible;
      parent => <-.UIparent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      title = "Select Project Item";
      ok = 0;
      okButton = 0;
      //okLabelString = "Display";
      cancelButton = 1;
      cancel = 0;
      cancelLabelString = "Close";
      width = 400;
      height = 450;
#ifdef MSDOS
      y = 0;
#endif // MSDOS
    };
    UIpanel UIpanel<NEx=341.,NEy=231.> {
      parent => <-.DLVdialog;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    UIlist UIlist<NEx=517.,NEy=308.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y = 0;
      width => parent.clientWidth;
      strings => <-.items;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle all<NEx=517.,NEy=385.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label = "Show all";
      width => ((parent.clientWidth / 2) - 10);
      x = 5;
      y => (<-.UIlist.y + <-.UIlist.height + 5);
      set => <-.show_all;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox option1<NEx=319.,NEy=451.> {
      parent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      x = 5;
      y => (<-.expand.y + <-.expand.height + 10);
      width => (parent.clientWidth / 2) - 65;
      labels => <-.filter_choices;
      visible => (<-.show_all == 0);
      UIpanel {
	visible => <-.visible;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox option2<NEx=561.,NEy=451.> {
      parent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      y => <-.option1.y;
      x => (parent.clientWidth / 2) + 60;
      width => (parent.clientWidth / 2) - 65;
      labels => <-.filter_choices;
      visible => (<-.show_all == 0);
      UIpanel {
	visible => <-.visible;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox operation<NEx=803.,NEy=451.> {
      parent => <-.UIpanel;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      y => <-.option1.y;
      width = 100;
      x => (parent.clientWidth / 2) - 50;
      labels => <-.op_list;
      visible => (<-.show_all == 0);
      UIpanel {
	visible => <-.visible;
      };
    };
    UIbutton filter<NEx=517.,NEy=550.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label = "Filter";
      width = 100;
      y => (<-.option1.y + <-.option1.height + 5);
      x => <-.operation.x;
      visible => (<-.show_all == 0);
    };
    UIbutton expand<NEx=726.,NEy=550.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label = "Expand Selection";
      width => ((parent.clientWidth / 2) - 10);
      x => ((parent.clientWidth / 2) + 5);
      y => <-.all.y;
      visible => (<-.show_all == 0);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle view<NEx=363.,NEy=605.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label = "Open in New Window";
      width => parent.clientWidth;
      x = 0;
      y => (<-.filter.y + <-.filter.height + 5);
    };
    UIbutton display {
#ifdef MSDOS
      parent => <-.UIpanel;
      x = 5;
      y => parent.clientHeight - height - 10;
#else
      parent => <-.DLVdialog;
#endif // MSDOS
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label = "Display";
      do = 0;
    };
  };
  macro Select {
    link UIparent<NEportLevels={2,1},NEx=264.,NEy=55.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=462.,NEy=55.>;
    link active_menus<NEportLevels={2,1},NEx=660.,NEy=55.>;
    link model_type<NEportLevels={2,1},NEx=880.,NEy=121.>;
    int visible<NEportLevels=1,NEx=473.,NEy=110.> = 0;
    int show_all<NEportLevels=1,NEx=176.,NEy=198.> = 0;
    int expand<NEportLevels=1,NEx=385.,NEy=198.> = 0;
    int filter<NEportLevels=1,NEx=583.,NEy=198.> = 0;
    int op<NEportLevels=1,NEx=803.,NEy=198.> = 2;
    int op1<NEportLevels=1,NEx=231.,NEy=264.> = 1;
    int op2<NEportLevels=1,NEx=462.,NEy=264.> = 2;
    int current<NEportLevels=1,NEx=704.,NEy=121.>;
    boolean new_view<NEportLevels=1,NEx=869.,NEy=55.> = 0;
    GMOD.instancer instancer<NEx=275.,NEy=341.> {
      Value => <-.visible;
      Group => <-.selectUI;
    };
    UIcmd UIcmd<NEx=176.,NEy=132.> {
      label = "Select";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      active => (<-.active_menus && (<-.model_type >= 0));
      do => <-.visible;
    };
    selectUI selectUI<NEx=528.,NEy=341.,instanced=0> {
      preferences => <-.preferences;
      UIparent => <-.UIparent;
      visible => <-.visible;
      show_all => <-.show_all;
      items => <-.select.items;
      //DLVdialog {
      //	okButton => (is_valid(<-.<-.current));
      //};
      UIlist {
	selectedItem => <-.<-.current;
      };
      option1 {
	selectedItem => <-.<-.op1;
	x = 0;
      };
      option2 {
	selectedItem => <-.<-.op2;
	visible => ((<-.show_all == 0) && (<-.<-.op > 0));
      };
      operation {
	selectedItem => <-.<-.op;
      };
      filter {
	do => <-.<-.filter;
      };
      expand {
	do => <-.<-.expand;
      };
      view {
	set => <-.<-.new_view;
      };
      display {
       visible => (is_valid(<-.<-.current));
      };
    };
    CCP3.Core_Modules.Project.select select<NEx=429.,NEy=451.> {
      do_list => <-.visible;
      do_expand => <-.expand;
      do_filter => <-.filter;
      do_all => <-.show_all;
      do_select => <-.selectUI.display.do;
      option1 => <-.op1;
      option2 => <-.op2;
      op => <-.op;
      new_view => <-.new_view;
      selection => <-.current;
    };
  };
  macro menu_file {
    link preferences<NEportLevels={2,1},NEx=363.,NEy=77.>;
    int active_menus<NEportLevels={2,1},NEx=572.,NEy=77.>;
    int dialog_visible<NEportLevels=1,NEx=781.,NEy=77.> =>
      ((sub_project.visible == 1) || /*(Select.visible == 1) ||*/
       (dir_ops.visible == 1));
    link UIparent<NEportLevels={2,1},NEx=759.,NEy=44.>;
    boolean user_project<NEportLevels=1,NEx=143.,NEy=132.> = 0;
    link model_type<NEportLevels=1,NEx=363.,NEy=22.>;
    CCP3.Core_Macros.UI.Project.dir_ops dir_ops<NEx=374.,NEy=187.> {
      preferences => <-.preferences;
      active_menu => <-.active_menus;
      UIparent => <-.UIparent;
      user_project => <-.user_project;
    };
    CCP3.Core_Macros.UI.Project.sub_project sub_project<NEx=374.,NEy=275.> {
      preferences => <-.preferences;
      active_menu => <-.active_menus;
      UIparent => <-.UIparent;
      user_project => <-.user_project;
    };
    CCP3.Core_Macros.UI.Project.Select Select<NEx=374.,NEy=440.> {
      UIparent => <-.UIparent;
      preferences => <-.preferences;
      active_menus => <-.active_menus;
      model_type => <-.model_type;
    };
  };
  macro projectUI {
    link project_name<NEportLevels={2,1},NEx=308.,NEy=55.>;
    link preferences<NEportLevels={2,1},NEx=550.,NEy=55.>;
    int active_menus<NEportLevels={2,1},NEx=308.,NEy=132.>;
    int dialog_visible<NEportLevels=1,NEx=539.,NEy=132.> =>
      (menu_file.dialog_visible == 1);
    link UIparent<NEportLevels={2,1},NEx=770.,NEy=55.>;
    CCP3.Core_Macros.UI.Project.menu_file menu_file<NEx=605.,NEy=253.> {
      preferences => <-.preferences;
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
    };
  };
};
