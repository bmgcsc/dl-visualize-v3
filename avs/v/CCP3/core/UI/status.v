
flibrary Status<NEeditable=1> {
  macro Status_bar {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=495.,NEy=33.>;
    link parent<NEportLevels={2,1},NEx=132.,NEy=110.>;
    AU.AUstatus_bar AUstatus_bar<NEx=275.,NEy=220.> {
      parent => <-.parent;
      x = 5;
      y+nres => (parent.clientHeight - 34);
      width+nres => ((parent.clientWidth / 2) - 10);
      UIframe {
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      ModuleName {
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      Progress {
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      StopGo {
	StopGo {
	  &color => <-.<-.<-.preferences.colour;
	  &fontAttributes => <-.<-.<-.preferences.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	  toolTip {
	    enable = "never";
	  };
#endif // DLV_TOOLTIP_OK
	};
      };
      OnOffStatus {
	OnOffStatus {
	  &color => <-.<-.<-.preferences.colour;
	  &fontAttributes => <-.<-.<-.preferences.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	  toolTip {
	    enable = "never";
	  };
#endif // DLV_TOOLTIP_OK
	};
      };
    };
  };
};
