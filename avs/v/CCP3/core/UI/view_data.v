
flibrary ViewData<NEeditable=1> {
  macro textUI {
    CCP3.Renderers.Points.text_params &params<NEx=594.,NEy=110.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=363.,NEy=44.>;
    link parent<NEportLevels={2,1},NEx=594.,NEy=44.>;
    int visible<NEportLevels=1,NEx=121.,NEy=33.>;
    UIpanel UIpanel<NEx=77.,NEy=99.> {
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (DLVtext.y + DLVtext.height + 5);
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext DLVtext<NEx=253.,NEy=297.> {
      parent => <-.UIpanel;
      text => <-.params.text;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 5;
      y = 5;
      width => (parent.clientWidth - 10);
      //rows = 1;
      //multiLine = 0;
      outputOnly = 1;
    };
  };
  macro realUI {
    CCP3.Renderers.Points.real_params &params<NEx=594.,NEy=110.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=363.,NEy=44.>;
    link parent<NEportLevels={2,1},NEx=594.,NEy=44.>;
    int visible<NEportLevels=1,NEx=121.,NEy=33.>;
    UIpanel UIpanel<NEx=77.,NEy=99.> {
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => parent.clientHeight;
      x = 0;
      y = 0;
    };
    string format => str_format("%s%1d%s", "%.", params.precision, "f");
    CCP3.Core_Macros.UI.UIobjs.DLVtext DLVvalue<NEx=253.,NEy=297.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 5;
      y = 5;
      width => (parent.clientWidth - 10);
      outputOnly = 1;
      text => str_format(<-.format, <-.params.value);
    };
    /*CCP3.Core_Macros.UI.UIobjs.DLVffield DLVvalue<NEx=253.,NEy=297.> {
      parent => <-.UIpanel;
      value => <-.params.value;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 5;
      y = 5;
      width => (parent.clientWidth - 10);
      outputOnly = 1;
      decimalPoints => <-.params.precision;
      };*/
    CCP3.Core_Macros.UI.UIobjs.DLViSlider DLViSlider<NEx=253.,NEy=396.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      x = 0;
      y => (<-.DLVvalue.y + <-.DLVvalue.height + 5);
      min = 3;
      max = 16;
      value => <-.params.precision;
      title = "precision";
    };
  };
  macro fileUI {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=363.,NEy=44.>;
    link parent<NEportLevels={2,1},NEx=594.,NEy=44.>;
    int visible<NEportLevels=1,NEx=121.,NEy=33.>;
    UIpanel UIpanel<NEx=77.,NEy=99.> {
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => parent.clientHeight;
      x = 0;
      y = 0;
    };
  };
  macro bond_lengthUI {
    CCP3.Renderers.Points.atom_bond_params+nres &params<NEx=594.,NEy=110.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=363.,NEy=44.>;
    link parent<NEportLevels={2,1},NEx=594.,NEy=44.>;
    int visible<NEportLevels=1,NEx=121.,NEy=33.>;
    UIpanel UIpanel<NEx=77.,NEy=99.> {
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (<-.DLVfSlider.y + <-.DLVfSlider.height + 10);
      x = 0;
      y = 0;
    };
    UIlabel Flabel {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y = 5;
      width => parent.clientWidth / 2;
      label = "Formula:";
      visible => is_valid(<-.params.formula);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext Formula {
      parent => <-.UIpanel;
      text => <-.params.formula;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x => parent.clientWidth / 2;
      y = 5;
      width => parent.clientWidth / 2;
      outputOnly = 1;
      visible => is_valid(<-.params.formula);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle DLVtoggle<NEx=253.,NEy=187.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label = "Display in 3D viewer";
      width => parent.clientWidth;
      set => <-.params.in_viewer;
      x = 0;
      y => (<-.Formula.y + <-.Formula.height + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext DLVtext<NEx=253.,NEy=297.> {
      parent => <-.UIpanel;
      text => <-.params.text;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.DLVtoggle.y + <-.DLVtoggle.height + 5);
      width => parent.clientWidth;
      rows = 4;
      multiLine = 1;
      outputOnly = 1;
      visible => (<-.params.has_offset == 0);
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider DLViSlider<NEx=253.,NEy=396.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      x = 0;
      y => (<-.DLVtext.y + <-.DLVtext.height + 5);
      min = 1;
      max = 9;
      value => <-.params.precision;
      title = "precision";
      visible => (<-.params.has_offset == 0);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist DLVlist<NEx=253.,NEy=297.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.DLVtoggle.y + <-.DLVtoggle.height + 5);
      width => parent.clientWidth;
      strings => <-.params.text_list;
      visible => (<-.params.has_offset == 1);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider DLVfSlider<NEx=253.,NEy=396.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      x = 0;
      y => (<-.DLVlist.y + <-.DLVlist.height + 5);
      min = 0.0;
      max = 2.0;
      value => <-.params.offset;
      title = "Viewer text offset";
      visible => (<-.params.has_offset == 1);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext main_panel_text<NEx=522.,NEy=396.> {
      //parent => <-.UIpanel;
      text => <-.params.panel_text;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      width => parent.clientWidth;
      rows = 1;
      outputOnly = 1;
      visible = 1;
    };
  };
  macro AtomVectorUI {
    CCP3.Core_Macros.Core.preferences &preferences;
    link use_colour;
    link scale;
    UIpanel UIpanel<NEportLevels={2,1}> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (<-.scale_slider.y + <-.scale_slider.height + 5);
      x = 0;
      y = 0;
    };
    /*CCP3.Core_Macros.UI.UIobjs.DLVtoggle magnitude {
      parent => <-.UIpanel;
      x = 0;
      y = 0;
      set => <-.use_colour;
      width => parent.clientWidth;
      label = "colour from data";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      };*/
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider scale_slider {
      //y => ((<-.magnitude.y + <-.magnitude.height) + 4);
      y = 0;
      width => ((<-.UIpanel.clientWidth * 11) / 12);
      parent => <-.UIpanel;
      min = 0.01;
      max = 50.0;
      value<NEportLevels={2,0}> => scale;
      title => "scale factor";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein scale_slider_typein {
      slider => <-.scale_slider;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
    };
  };
  macro PointUI {
    CCP3.Renderers.Points.point_params &params<NEx=594.,NEy=110.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=363.,NEy=44.>;
    link parent<NEportLevels={2,1},NEx=594.,NEy=44.>;
    int visible<NEportLevels=1,NEx=121.,NEy=33.>;
    UIpanel UIpanel<NEx=77.,NEy=99.> {
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (<-.blue_slider.y + <-.blue_slider.height + 5);
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider red_slider<NEx=451.,NEy=473.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.red;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider green_slider<NEx=451.,NEy=539.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.red_slider.y + <-.red_slider.height + 5);
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.green;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider blue_slider<NEx=451.,NEy=605.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.green_slider.y + <-.green_slider.height + 5);
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.blue;
    };
  };
  macro LineUI {
    CCP3.Renderers.Lines.line_params &params<NEx=594.,NEy=110.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=363.,NEy=44.>;
    link parent<NEportLevels={2,1},NEx=594.,NEy=44.>;
    int visible<NEportLevels=1,NEx=121.,NEy=33.>;
    UIpanel UIpanel<NEx=77.,NEy=99.> {
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (<-.blue_slider.y + <-.blue_slider.height + 5);
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider red_slider<NEx=451.,NEy=473.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.red;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider green_slider<NEx=451.,NEy=539.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.red_slider.y + <-.red_slider.height + 5);
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.green;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider blue_slider<NEx=451.,NEy=605.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.green_slider.y + <-.green_slider.height + 5);
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.blue;
    };
  };
  macro PlaneUI {
    CCP3.Renderers.Planes.plane_params &params<NEx=594.,NEy=110.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=363.,NEy=44.>;
    link parent<NEportLevels={2,1},NEx=594.,NEy=44.>;
    int visible<NEportLevels=1,NEx=121.,NEy=33.>;
    UIpanel UIpanel<NEx=77.,NEy=99.> {
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (<-.blue_slider.y + <-.blue_slider.height + 5);
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider opacity<NEx=451.,NEy=396.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.opacity;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider red_slider<NEx=451.,NEy=473.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.opacity.y + <-.opacity.height + 5);
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.red;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider green_slider<NEx=451.,NEy=539.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.red_slider.y + <-.red_slider.height + 5);
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.green;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider blue_slider<NEx=451.,NEy=605.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.green_slider.y + <-.green_slider.height + 5);
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.blue;
    };
  };
  macro BoxUI {
    CCP3.Renderers.Volumes.box_params &params<NEx=594.,NEy=110.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=363.,NEy=44.>;
    link parent<NEportLevels={2,1},NEx=594.,NEy=44.>;
    int visible<NEportLevels=1,NEx=121.,NEy=33.>;
    UIpanel UIpanel<NEx=77.,NEy=99.> {
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
#ifdef MSDOS
      height => (<-.blue_slider.y + <-.blue_slider.height + 5);
#else
      height = 350;
#endif // MSDOS
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle line_choice<NEx=286.,NEy=220.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      set => <-.params.lines;
      label = "Draw lines";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle surface_choice<NEx=528.,NEy=220.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.line_choice.y + <-.line_choice.height + 5);
      width => parent.clientWidth;
      set => <-.params.planes;
      label = "Draw surfaces";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle label_choice {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.surface_choice.y + <-.surface_choice.height + 5);
      width => parent.clientWidth;
      set => <-.params.labels;
      label = "Show labels";
      visible => params.has_labels;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider opacity<NEx=451.,NEy=396.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => switch (<-.params.has_labels+1,
		   (<-.surface_choice.y + <-.surface_choice.height + 5),
		   (<-.label_choice.y + <-.label_choice.height + 5));
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.opacity;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider red_slider<NEx=451.,NEy=473.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.opacity.y + <-.opacity.height + 5);
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.red;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider green_slider<NEx=451.,NEy=539.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.red_slider.y + <-.red_slider.height + 5);
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.green;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider blue_slider<NEx=451.,NEy=605.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.green_slider.y + <-.green_slider.height + 5);
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.blue;
    };
  };
  // Todo - mainly a copy of BoxUI except for params
  macro WulffUI {
    CCP3.Renderers.Volumes.wulff_params &params<NEx=594.,NEy=110.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=363.,NEy=44.>;
    link parent<NEportLevels={2,1},NEx=594.,NEy=44.>;
    int visible<NEportLevels=1,NEx=121.,NEy=33.>;
    UIpanel UIpanel<NEx=77.,NEy=99.> {
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
#ifdef MSDOS
      //height => (<-.DLVlist.y + <-.DLVlist.height + 5);
      height => (<-.vertices.y + <-.vertices.height + 5);
#else
      //height = 500;
      height = 375;
#endif // MSDOS
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle line_choice<NEx=286.,NEy=220.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      set => <-.params.lines;
      label = "Draw lines";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle surface_choice<NEx=528.,NEy=220.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.line_choice.y + <-.line_choice.height + 5);
      width => parent.clientWidth;
      set => <-.params.planes;
      label = "Draw surfaces";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle label_choice {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.surface_choice.y + <-.surface_choice.height + 5);
      width => parent.clientWidth;
      set => <-.params.labels;
      label = "Show labels";
      visible => params.has_labels;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider opacity<NEx=451.,NEy=396.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => switch (<-.params.has_labels+1,
		   (<-.surface_choice.y + <-.surface_choice.height + 5),
		   (<-.label_choice.y + <-.label_choice.height + 5));
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.opacity;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider red_slider<NEx=451.,NEy=473.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.opacity.y + <-.opacity.height + 5);
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.red;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider green_slider<NEx=451.,NEy=539.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.red_slider.y + <-.red_slider.height + 5);
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.green;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider blue_slider<NEx=451.,NEy=605.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.green_slider.y + <-.green_slider.height + 5);
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.blue;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle vertices<NEx=684.,NEy=396.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.blue_slider.y + <-.blue_slider.height + 5);
      width => parent.clientWidth;
      label = "Show vertices";
      set => params.vertex_display;
    };
    /*
    CCP3.Core_Macros.UI.UIobjs.DLVlist DLVlist<NEx=253.,NEy=297.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.vertices.y + <-.vertices.height + 5);
      width => parent.clientWidth;
      strings => <-.params.text_list;
      visible => (<-.params.vertex_display == 1);
    };
    */
  };
  macro SphUI {
    CCP3.Renderers.Volumes.sphere_params &params<NEx=594.,NEy=110.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=363.,NEy=44.>;
    link parent<NEportLevels={2,1},NEx=594.,NEy=44.>;
    int visible<NEportLevels=1,NEx=121.,NEy=33.>;
    UIpanel UIpanel<NEx=77.,NEy=99.> {
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
#ifdef MSDOS
      height => (<-.subdivisions.y + <-.subdivisions.height + 5);
#else
      height = 310;
#endif // MSDOS
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider opacity<NEx=451.,NEy=396.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.opacity;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider red_slider<NEx=451.,NEy=473.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.opacity.y + <-.opacity.height + 5);
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.red;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider green_slider<NEx=451.,NEy=539.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.red_slider.y + <-.red_slider.height + 5);
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.green;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider blue_slider<NEx=451.,NEy=605.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.green_slider.y + <-.green_slider.height + 5);
      width => parent.clientWidth;
      min = 0.;
      max = 1.;
      value => <-.params.blue;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider subdivisions {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y => (<-.blue_slider.y + <-.blue_slider.height + 5);
      width => parent.clientWidth;
      min = 4;
      max = 64;
      value => <-.params.subdivisions;
    };
  };
  macro volume_rUI {
    CCP3.Renderers.Volumes.vr_params &params<NEx=594.,NEy=110.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=363.,NEy=44.>;
    link parent<NEportLevels={2,1},NEx=594.,NEy=44.>;
    UIpanel UIpanel {
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
#ifdef MSDOS
      height => (<-.alt_object.y + <-.alt_object.height + 5);
#else
      height = 450;
#endif // MSDOS
      x = 0;
      y = 0;
    };
    macro modes_ui {
      UIframe modes_frame {
	y = 0;
	width => <-.<-.UIpanel.clientWidth;
	height => ((vol_rb.y + vol_rb.height) + 8);
	parent<NEportLevels={3,0}> => <-.<-.UIpanel;
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIlabel modes_label {
	x = 4;
	y = 0;
	width => (modes_frame.clientWidth - 8);
	parent => modes_frame;
	label => "Modes";
	alignment = "center";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoption inherit {
	label => "Inherit";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoption none {
	label => "None";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoption no_light {
	label => "No Lighting";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoption flat {
	label => "Flat Shading";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoption gouraud {
	label => "Gouraud Shading";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoptionMenu surf_rb {
	y => ((modes_label.y + modes_label.height) + 4);
	parent => modes_frame;
	label => "Surface";
	cmdList => {
	  inherit,none,no_light,flat,gouraud
	};
	alignment = "left";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
	selectedItem => <-.<-.params.surface;
      };
      UIoption vol_inherit {
	label => "Inherit";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoption vol_none {
	label => "None";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoption btf {
	label => "BTF texture (OpenGL)";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoption ray_tracer {
	label => "Ray tracer";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoptionMenu vol_rb {
	y => ((surf_rb.y + surf_rb.height) + 4);
	parent => modes_frame;
	label => "Volume";
	cmdList => {vol_inherit,
		    vol_none,btf,ray_tracer};
	alignment = "left";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
	selectedItem => <-.<-.params.volume;
      };
    };
    macro props_ui {
      UIframe props_frame {
	y => ((modes_ui.modes_frame.y + modes_ui.modes_frame.height) + 4);
	width => UIpanel.clientWidth;
	height => ((sfp_emit.y + sfp_emit.height) + 8);
	parent<NEportLevels={3,0}> => UIpanel;
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIlabel props_label {
	x = 4;
	y = 0;
	width => (props_frame.clientWidth - 8);
	parent => props_frame;
	label => "Properties";
	alignment = "center";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoption interp_pt {
	label => "Point";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoption interp_tri {
	label => "Trilinear";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoptionMenu interp_rb {
	y => ((props_label.y + props_label.height) + 4);
	active => switch(((<-.<-.params.ray_algo == 0) + 1),0,1);
	parent => props_frame;
	label => "Interpolation";
	cmdList => {interp_pt,
		    interp_tri};
	alignment = "left";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
	selectedItem => <-.<-.params.voxel;
      };
      UIoption ray_direct {
	label => "Direct Composite";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoption ray_ave {
	label => "Average Value";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoption ray_max {
	label => "Maximum Value";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoption ray_dist {
	label => "Distance to Max";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoption ray_sfp {
	label => "SFP";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoptionMenu ray_rb {
	y => ((interp_rb.y + interp_rb.height) + 4);
	parent => props_frame;
	label => "Ray Algorithm";
	cmdList => {
	  ray_direct,ray_ave,ray_max,ray_dist,ray_sfp
	};
	alignment = "left";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
	selectedItem => <-.<-.params.ray_algo;
      };
      UIoption dist_glob {
	label => "Global";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoption dist_view {
	label => "View";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoption dist_ray {
	label => "Ray";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
      };
      UIoptionMenu dist_rb {
	y => ((ray_rb.y + ray_rb.height) + 4);
	active => switch(((<-.<-.params.ray_algo == 3) + 1),0,1);
	parent => props_frame;
	label => "Distance Normalize";
	cmdList => {dist_glob,
		    dist_view,dist_ray};
	alignment = "left";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
	selectedItem => <-.<-.params.ray_norm;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVtoggle fat_ray {
	y => ((dist_rb.y + dist_rb.height) + 4);
	parent => props_frame;
	label => "Fat Ray";
	set => ;
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
	set => <-.<-.params.fat_ray;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVfSlider sfp_absorb {
	y => ((fat_ray.y + fat_ray.height) + 4);
	active => switch(((<-.<-.params.ray_algo == 4) + 1),0,1);
	parent => props_frame;
	min = 0.;
	max = 1.;
	title => "SFP Absorption";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
	value => <-.<-.params.absorb;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVfSlider sfp_emit {
	y => ((sfp_absorb.y + sfp_absorb.height) + 4);
	active => switch(((<-.<-.params.ray_algo == 4) + 1),0,1);
	parent => props_frame;
	min = 0.;
	max = 1.;
	title => "SFP Emission";
	&color => <-.<-.preferences.colour;
	&fontAttributes => <-.<-.preferences.fontAttributes;
	value => <-.<-.params.emit;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle alt_object {
      y => ((<-.props_ui.props_frame.y + <-.props_ui.props_frame.height) + 6);
      width => parent.clientWidth;
      parent => <-.UIpanel;
      label => "Alternate Object";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.params.alt;
    };
  };
  macro leedUI {
    CCP3.Renderers.Plots.leed_params &params<NEx=594.,NEy=110.>;
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=363.,NEy=44.>;
    link parent<NEportLevels={2,1},NEx=594.,NEy=44.>;
    int visible<NEportLevels=1,NEx=121.,NEy=33.>;
    UIpanel UIpanel<NEx=77.,NEy=99.> {
      visible => <-.visible;
      parent => <-.parent;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (DLVtext.y + DLVtext.height + 5);
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle DLVtoggle<NEx=253.,NEy=297.> {
      parent => <-.UIpanel;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 5;
      y = 5;
      label = "Colour Domains";
      width => parent.clientWidth;
      set => <-.params.domains;
    };
  };
  macro IsoBaseUI {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=363.,NEy=66.>;
    ilink in_fld<NEportLevels={2,1}>;
    CCP3.Renderers.Volumes.CCP3IsoParam &param<NEportLevels={2,0}>;
    DVnode_data_labels DVnode_data_labels {
      in => <-.in_fld;
    };
    UIpanel UIpanel<NEportLevels={2,1}> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (<-.UIiso_level.y + <-.UIiso_level.height + 5);
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider UIiso_level {
      y = 4;
      parent => <-.UIpanel;
      min+nres => in_fld.node_data[param.iso_component].min;
      max+nres => in_fld.node_data[param.iso_component].max;
      value<NEportLevels={2,0}> => param.iso_level;
      title => "iso level";
      decimalPoints = 2;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => (parent.clientWidth * 11) / 12;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein iso_level_typein<NEx=528.,
      NEy=220.> {
      slider => <-.UIiso_level;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      valEditor {
	UI {
	  min_field {
	    min+nres => <-.<-.<-.<-.UIiso_level.min;
	  };
	  max_field {
	    max+nres => <-.<-.<-.<-.UIiso_level.max;
	  };
	  // This fixes bug, don't know why
	  editor_shell {
	    x => ;
	    y => ;
	  };
	};
      };
    };
  };
  IsoBaseUI IsoUI {
    UIpanel {
      height => (<-.max_colour.y + <-.max_colour.height + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle base_colour {
      parent => <-.UIpanel;
      y => ((<-.UIiso_level.y + <-.UIiso_level.height) + 4);
      set => param.color;
      label => "Colour data from dataset";
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => (param.complex == 0);
    };
    UIlabel colour_label {
      parent => <-.UIpanel;
      width => parent.clientWidth;
      y => ((<-.base_colour.y + <-.base_colour.height) + 4);
      visible => (param.color == 1 && param.complex == 0);
      label = "colour dataset";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist UIoptionBoxLabel {
      parent => <-.UIpanel;
      strings<NEportLevels={2,0}> => <-.DVnode_data_labels.labels;
      selectedItem<NEportLevels={2,0}> => param.colour_data;
      y => ((<-.colour_label.y + <-.colour_label.height) + 4);
      visible => (param.color == 1 && param.complex == 0);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider min_colour<NEx=99.,NEy=627.> {
      parent => <-.UIpanel;
      min+nres => in_fld.node_data[param.colour_data].min;
      max+nres => param.max_colour_val;
      value<NEportLevels={2,0}> => param.min_colour_val;
      title => "min colour value";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => (parent.clientWidth * 11) / 12;
      y => ((<-.UIoptionBoxLabel.y + <-.UIoptionBoxLabel.height) + 4);
      visible => (param.color == 1 && param.complex == 0);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider max_colour<NEx=605.,NEy=627.> {
      parent => <-.UIpanel;
      min+nres => param.min_colour_val;
      max+nres => in_fld.node_data[param.colour_data].max;
      value<NEportLevels={2,0}> => param.max_colour_val;
      title => "max colour value";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => (parent.clientWidth * 11) / 12;
      y => ((<-.min_colour.y + <-.max_colour.height) + 4);
      visible => (param.color == 1 && param.complex == 0);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein min_colour_typein<NEx=308.,
      NEy=627.> {
      slider => <-.min_colour;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      visible => (param.color == 1 && param.complex == 0);
      valEditor {
	UI {
	  min_field {
	    min+nres => <-.<-.<-.<-.min_colour.min;
	  };
	  max_field {
	    max+nres => <-.<-.<-.<-.min_colour.max;
	  };
	  // This fixes bug, don't know why
	  editor_shell {
	    x => ;
	    y => ;
	  };
	};
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein max_colour_typein<NEx=803.,
      NEy=627.> {
      slider => <-.max_colour;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      visible => (param.color == 1 && param.complex == 0);
      valEditor {
	UI {
	  min_field {
	    min+nres => <-.<-.<-.<-.max_colour.min;
	  };
	  max_field {
	    max+nres => <-.<-.<-.<-.max_colour.max;
	  };
	  // This fixes bug, don't know why
	  editor_shell {
	    x => ;
	    y => ;
	  };
	};
      };
    };
  };
  macro ShrinkUI {
    ilink in_fld;
    DV_Param_separate_cells &param<NEportLevels={2,0}>;
    CCP3.Core_Macros.Core.preferences &preferences;
    UIpanel UIpanel<NEportLevels={2,1}> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (<-.max_level.y + <-.max_level.height + 5);
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider shrink_slider {
      y = 0;
      width => ((<-.UIpanel.clientWidth * 11) / 12);
      parent => <-.UIpanel;
      min = 0.;
      max = 1.;
      value<NEportLevels={2,0}> => param.scale;
      title => "scale factor";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein shrink_slider_typein {
      slider => <-.shrink_slider;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Renderers.Volumes.SCellsParam &cparam<NEportLevels={2,0}>;
    DVnode_data_labels DVnode_data_labels {
      in => <-.in_fld;
      int+nres ncomp => in.nnode_data;
    };
    UIlabel colour_label {
      parent => <-.UIpanel;
      width => parent.clientWidth;
      y => ((<-.shrink_slider.y + <-.shrink_slider.height) + 5);
      label = "colour data";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist UIoptionBoxLabel {
      parent => <-.UIpanel;
      strings<NEportLevels={2,0}> => DVnode_data_labels.labels;
      selectedItem<NEportLevels={2,0}> => cparam.map;
      y => ((<-.colour_label.y + <-.colour_label.height) + 4);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider min_level {
      y => ((<-.UIoptionBoxLabel.y + <-.UIoptionBoxLabel.height) + 4);
      width => ((<-.UIpanel.clientWidth * 11) / 12);
      parent => <-.UIpanel;
      min+nres => cache(in_fld.node_data[cparam.contour_comp].min);
      max+nres => cache(in_fld.node_data[cparam.contour_comp].max);
      value<NEportLevels={2,0}> => cparam.level_min;
      title => "min level";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein min_level_typein {
      slider => <-.min_level;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      valEditor {
        UI {
          min_field {
            min+nres =>
      cache(<-.<-.<-.<-.in_fld.node_data[<-.<-.<-.<-.cparam.contour_comp].min);
          };
          max_field {
            max+nres =>
      cache(<-.<-.<-.<-.in_fld.node_data[<-.<-.<-.<-.cparam.contour_comp].max);
          };
        };
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider max_level {
      y => ((<-.min_level.y + <-.min_level.height) + 4);
      width => ((<-.UIpanel.clientWidth * 11) / 12);
      parent => <-.UIpanel;
      min+nres => cache(in_fld.node_data[cparam.contour_comp].min);
      max+nres => cache(in_fld.node_data[cparam.contour_comp].max);
      value<NEportLevels={2,0}> => cparam.level_max;
      title => "max level";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein max_level_typein {
      slider => <-.max_level;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      valEditor {
        UI {
          min_field {
            min+nres =>
     cache(<-.<-.<-.<-.in_fld.node_data[<-.<-.<-.<-.cparam.contour_comp].min);
          };
          max_field {
            max+nres =>
     cache(<-.<-.<-.<-.in_fld.node_data[<-.<-.<-.<-.cparam.contour_comp].max);
          };
        };
      };
    };
  };
  macro SphereUI {
    ilink in_fld;
    CCP3.Core_Macros.Core.preferences &preferences;
    UIpanel UIpanel<NEportLevels={2,1}> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (<-.scale_slider.y + <-.scale_slider.height + 5);
      x = 0;
      y = 0;
    };
    DVnode_data_labels DVnode_data_labels {
      in => <-.in_fld;
      int+nres ncomp => in.nnode_data;
    };
    int component;
    CCP3.Renderers.Volumes.SphereParam &param<NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle magnitude {
      parent => <-.UIpanel;
      x = 0;
      y = 0;
      set => <-.param.mag_comp;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    UIlabel colour_label {
      parent => <-.UIpanel;
      width => parent.clientWidth;
      y => ((<-.magnitude.y + <-.magnitude.height) + 4);
      label = "colour data";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist map {
      parent => <-.UIpanel;
      strings<NEportLevels={2,0}> => DVnode_data_labels.labels;
      selectedItem<NEportLevels={2,0}> => param.map;
      y => ((<-.colour_label.y + <-.colour_label.height) + 4);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle map_magnitude {
      parent => <-.UIpanel;
      y => ((<-.map.y + <-.map.height) + 4);
      width => parent.clientWidth;
      set => <-.param.mag_map;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider scale_slider {
      y => ((<-.map_magnitude.y + <-.map_magnitude.height) + 4);
      width => ((<-.UIpanel.clientWidth * 11) / 12);
      parent => <-.UIpanel;
      min = 0.01;
      max = 10.0;
      value<NEportLevels={2,0}> => param.scale;
      title => "scale factor";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein scale_slider_typein {
      slider => <-.scale_slider;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
    };
  };
  macro StreamlineUI {
    ilink in_fld;
    CCP3.Core_Macros.Core.preferences &preferences;
    CCP3.Renderers.Volumes.StreamlineParam &param<NEportLevels={2,0}>;
    ilink UIpanel;
    DVnode_data_labels DVnode_data_labels {
      in => in_fld;
      int+nres ncomp => in.nnode_data;
    };
    UIpanel UIpanel<NEportLevels={2,1}> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (<-.colour_toggle.y + <-.colour_toggle.height + 5);
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist planes {
      parent => <-.UIpanel;
      selectedItem => <-.param.plane;
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox UIradioBoxLabel {
      parent => <-.UIpanel;
      labels<NEportLevels={2,0}> => DVnode_data_labels.labels;
      &selectedItem => param.component;
      title => "velocity vector";
      y => ((<-.planes.y + <-.planes.height) + 4);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox UIradioBoxLabel_dir {
      parent => <-.UIpanel;
      labels => { "forward", "backward" };
      &selectedItem => param.forw_back;
      title => "Direction";
      y => ((<-.UIradioBoxLabel.y + <-.UIradioBoxLabel.height) + 4);
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider nseg_slider {
      y => ((<-.UIradioBoxLabel_dir.y + <-.UIradioBoxLabel_dir.height) + 4);
      parent => <-.UIpanel;
      width => parent.clientWidth;
      max = 16.;
      value<NEportLevels={2,0}> => param.nseg;
      title => "nsegments";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider max_seg_slider {
      y => ((<-.nseg_slider.y + <-.nseg_slider.height) + 4);
      parent => <-.UIpanel;
      width => parent.clientWidth;
      min = 1.;
      max = 10000.;
      value<NEportLevels={2,0}> => param.max_seg;
      title => "max segments";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider order_slider {
      y => ((<-.max_seg_slider.y + <-.max_seg_slider.height) + 4);
      parent => <-.UIpanel;
      width => parent.clientWidth;
      min = 1.;
      max = 4.;
      value<NEportLevels={2,0}> => param.order;
      title => "order";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider vel_slider {
      y => ((<-.order_slider.y + <-.order_slider.height) + 4);
      width => ((parent.clientWidth * 11) / 12);
      parent => <-.UIpanel;
      value<NEportLevels={2,0}> => param.min_vel;
      title => "min_velocity";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein vel_slider_typein {
      slider => vel_slider;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle colour_toggle {
      y => ((<-.vel_slider.y + <-.vel_slider.height) + 4);
      width => parent.clientWidth;
      parent => <-.UIpanel;
      label => "Colour";
      set<NEportLevels={2,0}> => param.color;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Modules.ViewData.update_display update<NEx=207.,NEy=405.> {
      plane => <-.param.plane;
    };
  };
  macro contourUI_base {
    ilink in_fld;
    CCP3.Core_Macros.Core.preferences &preferences;
    int visible2D<NEportLevels=1,NEx=539.,NEy=77.>;
    UIpanel UIpanel<NEportLevels={2,1}> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle vis2d<NEx=792.,NEy=495.> {
      parent => <-.UIpanel;
      x => (parent.clientWidth - width) / 2;
      y = 0;
      label = "2D View";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => <-.visible2D;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider nleveles {
      y => ((<-.vis2d.y + <-.vis2d.height) + 5);
      width => ((<-.UIpanel.clientWidth * 11) / 12);
      parent => <-.UIpanel;
      min = 1.;
      title => "number of contours";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider iso_level_min {
      y => ((<-.nleveles.y + <-.nleveles.height) + 5);
      width => ((<-.UIpanel.clientWidth * 11) / 12);
      parent => <-.UIpanel;
      title => "min level";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein iso_level_min_typein {
      slider => iso_level_min;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider iso_level_max {
      y => ((<-.iso_level_min.y + <-.iso_level_min.height) + 5);
      width => ((<-.UIpanel.clientWidth * 11) / 12);
      parent => <-.UIpanel;
      title => "max level";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein iso_level_max_typein {
      slider => iso_level_max;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
    };
  };
  contourUI_base iso_shadeUI {
    CCP3.Renderers.Planes.IsolineParam &param<NEportLevels={2,0}>;
    nleveles {
      value<NEportLevels={2,0}> => param.ncontours;
    };
    iso_level_min {
      min+nres => cache(in_fld.node_data[param.contour_comp].min);
      max+nres => cache(in_fld.node_data[param.contour_comp].max);
      value<NEportLevels={2,0}> => param.level_min;
    };
    iso_level_min_typein {
      valEditor {
        UI {
          max_field {
            max+nres =>
       cache(<-.<-.<-.<-.in_fld.node_data[<-.<-.<-.<-.param.contour_comp].max);
          };
        };
      };
    };
    iso_level_max {
      min+nres => cache(in_fld.node_data[param.contour_comp].min);
      max+nres => cache(in_fld.node_data[param.contour_comp].max);
      value<NEportLevels={2,0}> => param.level_max;
    };
    iso_level_max_typein {
      valEditor {
        UI {
          min_field {
            min+nres =>
      cache(<-.<-.<-.<-.in_fld.node_data[<-.<-.<-.<-.param.contour_comp].min);
          };
        };
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle UItoggle<NEx=253.,NEy=319.> {
      parent => <-.UIpanel;
      y => ((<-.iso_level_max.y + <-.iso_level_max.height) + 5);
      label = "Log scale";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      set => param.log;
    };
  };
  iso_shadeUI isolineUI {
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle colour_toggle {
      y => ((<-.UItoggle.y + <-.UItoggle.height) + 5);
      parent => <-.UIpanel;
      label => "Color";
      set<NEportLevels={2,0}> => param.color;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    UIpanel {
      height => ((<-.colour_toggle.y + <-.colour_toggle.height) + 5);
    };
  };
  contourUI_base scontourUI {
    DVnode_data_labels DVnode_data_labels<NEx=242.,NEy=209.> {
      in => <-.in_fld;
    };
    CCP3.Renderers.Planes.ContourParam &param<NEportLevels={2,0}>;
    UIpanel {
      height => (<-.UIoptionBoxLabel.y + <-.UIoptionBoxLabel.height + 5);
    };
    nleveles {
      value<NEportLevels={2,0}> => param.ncontours;
    };
    iso_level_min {
      min+nres => cache(in_fld.node_data[param.contour_comp].min);
      max+nres => cache(in_fld.node_data[param.contour_comp].max);
      value<NEportLevels={2,0}> => param.level_min;
    };
    iso_level_min_typein {
      valEditor {
        UI {
          max_field {
            max+nres =>
       cache(<-.<-.<-.<-.in_fld.node_data[<-.<-.<-.<-.param.contour_comp].max);
          };
        };
      };
    };
    iso_level_max {
      min+nres => cache(in_fld.node_data[param.contour_comp].min);
      max+nres => cache(in_fld.node_data[param.contour_comp].max);
      value<NEportLevels={2,0}> => param.level_max;
    };
    iso_level_max_typein {
      valEditor {
        UI {
          min_field {
            min+nres =>
      cache(<-.<-.<-.<-.in_fld.node_data[<-.<-.<-.<-.param.contour_comp].min);
          };
        };
      };
    };
    UIlabel colour_label {
      parent => <-.UIpanel;
      width => parent.clientWidth;
      y => ((<-.iso_level_max.y + <-.iso_level_max.height) + 5);
      label = "colour data";
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist UIoptionBoxLabel {
      parent => <-.UIpanel;
      strings<NEportLevels={2,0}> => <-.DVnode_data_labels.labels;
      selectedItem<NEportLevels={2,0}> => param.colour_data;
      y => ((<-.colour_label.y + <-.colour_label.height) + 5);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
  };
  macro surfaceplotUI {
    ilink in_fld;
    CCP3.Core_Macros.Core.preferences &preferences;
    string DVnode_data_labels<NEportLevels={2,1}>[];
    DV_Param_surf_plot &plot_data;
    UIpanel UIpanel<NEportLevels={2,1}> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      height => (<-.UItoggle.y + <-.UItoggle.height + 5);
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider surface_scale {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      y = 0;
      parent => <-.UIpanel;
      min = -10.0;
      max = 10.0;
      value<NEportLevels={2,0}> => plot_data.scale;
      title => "surface scale";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVslider_typein surface_scale_typein {
      slider => surface_scale;
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
    };
  };
  macro PlotUI {
    CCP3.Renderers.Plots.plot_params &params<NEx=363.,NEy=286.>;
    int display<NEportLevels=1,NEx=638.,NEy=385.> =>
      params.plot_vis[params.plot_select];
    CCP3.Core_Macros.Core.preferences &preferences;
    UIpanel UIpanel<NEportLevels={2,1}> {
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
#ifdef MSDOS
      height => (<-.line_width.y + <-.line_width.height + 5);
#else
      height = 200;
#endif // MSDOS
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist plot_list<NEx=253.,NEy=341.> {
      parent => <-.UIpanel;
      y = 5;
      selectedItem => <-.params.plot_select;
      strings => <-.params.components;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle UItoggle<NEx=286.,NEy=396.> {
      parent => <-.UIpanel;
      y => ((<-.plot_list.y + <-.plot_list.height) + 4);
      label => "line visible";
      set => <-.display;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider line_width<NEx=477.,NEy=477.> {
      parent => <-.UIpanel;
      width => parent.clientWidth;
      min = 0.01;
      max = 5.0;
      value => params.width;
    };
  };
  macro data_panels {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels=1,
      NEx=308.,NEy=44.>;
    int visible<NEportLevels=1,NEx=550.,NEy=121.>;
    int nthreads<NEportLevels=1,NEx=306.,NEy=90.>;
    UIframe UIframe<NEx=99.,NEy=121.> {
      parent<NEportLevels={3,0}>;
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    UIbutton Delete<NEx=132.,NEy=209.> {
      parent => <-.UIframe;
      y = 5;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => is_valid(<-.<-.display_object);
      do = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle Visible<NEx=341.,NEy=209.> {
      parent => <-.UIframe;
      y => Delete.y;
      x => (parent.clientWidth / 2);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => is_valid(<-.<-.display_object);
      set => <-.visible;
    };
    CCP3.Core_Modules.ViewData.delete_display delete_display<NEx=143.,NEy=275.>
    {
      trigger => <-.Delete.do;
    };
    CCP3.Core_Modules.ViewData.hide_display hide_display<NEx=627.,NEy=209.> {
      visible => <-.visible;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVscrolledWindow scroll<NEx=121.,NEy=344.> {
      parent => <-.UIframe;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      width => parent.clientWidth;
      x = 0;
      visible => (is_valid(<-.<-.display_object) && <-.visible);
      y => (<-.Delete.y + <-.Delete.height + 5);
      height => (parent.clientHeight - y);
    };
    int+nres type_info<NEportLevels=1,NEx=517.,NEy=44.>;
    CCP3.Core_Macros.UI.ViewData.textUI textUI<NEx=121.,NEy=539.,instanced=0> {
      preferences => <-.preferences;
      visible => (<-.type_info == 14);
      parent => <-.scroll;
      //UIpanel {
      //	parent => <-.<-.scroll;
      //};
    };
    GMOD.instancer instance_text<NEx=121.,NEy=484.> {
      Value => (<-.type_info == 14);
      Group => textUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.realUI realUI<NEx=121.,NEy=638.,instanced=0> {
      preferences => <-.preferences;
      visible => (<-.type_info == 15);
      UIpanel {
	parent => <-.<-.scroll;
      };
    };
    GMOD.instancer instance_real<NEx=121.,NEy=583.> {
      Value => (<-.type_info == 15);
      Group => realUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.bond_lengthUI bondsUI<NEx=121.,NEy=429.> {
      preferences => <-.preferences;
      visible => (<-.type_info == 8);
      UIpanel {
	parent => <-.<-.scroll;
      };
    };
    CCP3.Core_Macros.UI.ViewData.IsoUI IsoUI<NEx=803.,NEy=220.,instanced=0> {
      preferences => <-.preferences;
      UIpanel {
	parent => <-.<-.scroll;
	visible => (<-.<-.type_info == 0);
      };
    };
    GMOD.instancer iso_instance<NEx=803.,NEy=132.> {
      Value => (<-.type_info == 0);
      Group => IsoUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.IsoBaseUI IsoWavefnUI<NEx=957.,NEy=220.> {
      preferences => <-.preferences;
      UIpanel {
	parent => <-.<-.scroll;
	visible => (<-.<-.type_info == 21);
      };
    };
    GMOD.instancer instance_wavefn<NEx=957.,NEy=165.> {
      Value => (<-.type_info == 21);
      Group => IsoWavefnUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.ShrinkUI ShrinkUI<NEx=803.,NEy=341.,
      instanced=0> {
      preferences => <-.preferences;
      UIpanel {
	parent => <-.<-.scroll;
	visible => (<-.<-.type_info == 1);
      };
    };
    GMOD.instancer cells_instance<NEx=803.,NEy=286.> {
      Value => (<-.type_info == 1);
      Group => ShrinkUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.SphereUI SphereUI<NEx=803.,NEy=451.,
      instanced=0> {
      preferences => <-.preferences;
      UIpanel {
	parent => <-.<-.scroll;
	visible => (<-.<-.type_info == 2);
      };
    };
    GMOD.instancer spheres_instance<NEx=803.,NEy=396.> {
      Value => (<-.type_info == 2);
      Group => SphereUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.StreamlineUI StreamlineUI<NEx=803.,NEy=649.,
      instanced=0> {
      preferences => <-.preferences;
      UIpanel {
	parent => <-.<-.scroll;
	visible => (<-.<-.type_info == 5);
      };
    };
    GMOD.instancer stream_instance<NEx=803.,NEy=594.> {
      Value => (<-.type_info == 5);
      Group => StreamlineUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.isolineUI AGisolineUI<NEx=627.,NEy=341.,
      instanced=0> {
      preferences => <-.preferences;
      UIpanel {
	parent => <-.<-.scroll;
	visible => (<-.<-.type_info == 6);
      };
    };
    GMOD.instancer AG_instance<NEx=627.,NEy=286.> {
      Value => (<-.type_info == 6);
      Group => AGisolineUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.isolineUI isolineUI<NEx=627.,NEy=429.,
      instanced=0> {
      preferences => <-.preferences;
      UIpanel {
	parent => <-.<-.scroll;
	visible => (<-.<-.type_info == 9);
      };
#ifndef DLV_DL
      vis2d {
	visible = 0;
      };
#endif // DLV_DL
    };
    GMOD.instancer isoline_instance<NEx=627.,NEy=385.> {
      Value => (<-.type_info == 9);
      Group => isolineUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.isolineUI PNisolineUI<NEx=627.,NEy=517.,
      instanced=0> {
      preferences => <-.preferences;
      UIpanel {
	parent => <-.<-.scroll;
	visible => (<-.<-.type_info == 10);
      };
    };
    GMOD.instancer PN_instance<NEx=627.,NEy=473.> {
      Value => (<-.type_info == 10);
      Group => PNisolineUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.scontourUI solidUI<NEx=627.,NEy=594.,
      instanced=0> {
      preferences => <-.preferences;
      UIpanel {
	parent => <-.<-.scroll;
	visible => (<-.<-.type_info == 11);
      };
    };
    GMOD.instancer solid_instance<NEx=627.,NEy=550.> {
      Value => (<-.type_info == 11);
      Group => solidUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.iso_shadeUI shadedUI<NEx=627.,NEy=671.,
      instanced=0> {
      preferences => <-.preferences;
      UIpanel {
	parent => <-.<-.scroll;
	visible => (<-.<-.type_info == 12);
	height => (<-.UItoggle.y + <-.UItoggle.height + 5);
      };
      vis2d {
	visible = 0;
      };
    };
    GMOD.instancer shaded_instance<NEx=627.,NEy=627.> {
      Value => (<-.type_info == 12);
      Group => shadedUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.surfaceplotUI surfplotUI<NEx=462.,NEy=638.,
      instanced=0> {
      preferences => <-.preferences;
      UIpanel {
	parent => <-.<-.scroll;
	visible => (<-.<-.type_info == 13);
      };
    };
    GMOD.instancer surf_instance<NEx=462.,NEy=583.> {
      Value => (<-.type_info == 13);
      Group => surfplotUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.PlotUI PlotUI<NEx=462.,NEy=341.,instanced=0> {
      preferences => <-.preferences;
      UIpanel {
	parent => <-.<-.scroll;
	visible => (<-.<-.type_info == 7);
      };
    };
    GMOD.instancer plot_instance<NEx=462.,NEy=286.> {
      Value => (<-.type_info == 7);
      Group => <-.PlotUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.fileUI fileUI<NEx=286.,NEy=539.,instanced=0> {
      preferences => <-.preferences;
      UIpanel {
	parent => <-.<-.scroll;
	visible => (<-.<-.type_info == 16);
      };
    };
    GMOD.instancer instance_file<NEx=286.,NEy=484.> {
      Value => (<-.type_info == 16);
      Group => <-.fileUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.BoxUI boxUI<NEx=803.,NEy=539.,instanced=0> {
      preferences => <-.preferences;
      parent => <-.scroll;
      visible => (<-.type_info == 17);
    };
    GMOD.instancer instance_box<NEx=803.,NEy=495.> {
      Value => (<-.type_info == 17);
      Group => <-.boxUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.PointUI pointUI<NEx=286.,NEy=638.,
      instanced=0> {
      preferences => <-.preferences;
      parent => <-.scroll;
      visible => (<-.type_info == 20);
    };
    GMOD.instancer instance_point<NEx=286.,NEy=583.> {
      Value => (<-.type_info == 20);
      Group => <-.pointUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.LineUI lineUI<NEx=462.,NEy=429.,instanced=0> {
      preferences => <-.preferences;
      parent => <-.scroll;
      visible => (<-.type_info == 19);
    };
    GMOD.instancer instance_line<NEx=462.,NEy=385.> {
      Value => (<-.type_info == 19);
      Group => <-.lineUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.PlaneUI planeUI<NEx=462.,NEy=528.,
      instanced=0> {
      preferences => <-.preferences;
      parent => <-.scroll;
      visible => (<-.type_info == 18);
    };
    GMOD.instancer instance_plane<NEx=462.,NEy=473.> {
      Value => (<-.type_info == 18);
      Group => <-.planeUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.AtomVectorUI AtomVectorUI<NEx=286.,NEy=429.,
      instanced=0> {
      preferences => <-.preferences;
      UIpanel {
	parent => <-.<-.scroll;
	visible => (<-.<-.type_info == 22);
      };
    };
    GMOD.instancer instance_av<NEx=286.,NEy=385.> {
      Value => (<-.type_info == 22);
      Group => <-.AtomVectorUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.leedUI leedUI<NEx=972.,NEy=342.,instanced=0> {
      preferences => <-.preferences;
      UIpanel {
	parent => <-.<-.scroll;
	visible => (<-.<-.type_info == 24);
      };
    };
    GMOD.instancer leed_instance<NEx=963.,NEy=279.> {
      Value => (<-.type_info == 24);
      Group => <-.leedUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.SphUI SphUI<NEx=972.,NEy=396.,instanced=0> {
      preferences => <-.preferences;
      parent => <-.scroll;
      visible => (<-.type_info == 25);
    };
    GMOD.instancer leed_instance<NEx=963.,NEy=279.> {
      Value => (<-.type_info == 25);
      Group => <-.SphUI;
      active = 2;
    };
    WulffUI WulffUI<NEx=972.,NEy=450.,instanced=0> {
      preferences => <-.preferences;
      parent => <-.scroll;
      visible => (<-.type_info == 26);
    };
    GMOD.instancer instance_wulff<NEx=972.,NEy=495.> {
      Value => (<-.type_info == 26);
      Group => <-.WulffUI;
      active = 2;
    };
    CCP3.Core_Macros.UI.ViewData.volume_rUI volume_rUI<NEx=972.,NEy=540.,
						       instanced=0> {
      preferences => <-.preferences;
      parent => <-.scroll;
      visible => (<-.type_info == 3);
    };
    GMOD.instancer instancer<NEx=972.,NEy=576.> {
      Value => (<-.type_info == 3);
      Group => <-.volume_rUI;
      active = 2;
    };
  };
  macro displayUI {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels=1,
      NEx=275.,NEy=22.>;
    CCP3.Core_Macros.UI.UIobjs.DLVshell DLVshell<NEx=66.,NEy=110.> {
      color => <-.preferences.colour;
      fontAttributes => <-.preferences.fontAttributes;
      title = "Display Data";
      UIshell {
	height = 750;
      };
    };
    UIlabel title<NEx=187.,NEy=187.> {
      parent => <-.DLVshell.UIpanel;
      y = 0;
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label = "Data Objects";
    };
    link data_list<NEportLevels=1,NEx=286.,NEy=99.>;
    link data_object<NEportLevels={0,1},NEx=440.,NEy=99.>;
    CCP3.Core_Macros.UI.UIobjs.DLVlist data_choices<NEx=363.,NEy=374.> {
      parent => <-.DLVshell.UIpanel;
      y => ((<-.title.y + <-.title.height) + 5);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      selectedItem => <-.data_object;
      strings+nres => <-.data_list.label;
    };
    int+nres select_method =>
      ((16 * data_list[data_object].vector[sub_list.selectedItem])
       + data_type + 2);
    // Not ideal, but at least it works.
    GMOD.parse_v set_sub_object<NEx=176.,NEy=440.> {
      v_commands =
	"sub_list.selectedItem => data_list[data_object].selection;";
      trigger => <-.data_object;
      relative => <-;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist sub_list<NEx=363.,NEy=451.> {
      parent => <-.DLVshell.UIpanel;
      y => ((<-.data_choices.y + <-.data_choices.height) + 5);
      visible+nres => (array_size(<-.data_list[data_object].data_list) > 1);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      //selectedItem+nres => <-.data_list[<-.data_object].selection;
      strings+nres => <-.data_list[<-.data_object].data_list;
    };
    link data_type<NEx=590.,NEy=99.> => data_list[data_object].type_data;
    string null_methods<NEportLevels=1>[] = {
      "Null"
    };
    string text_methods<NEportLevels=1,NEx=11.,NEy=198.>[] = {
      "Show"
    };
    string point_methods<NEportLevels=1,NEx=11.,NEy=242.>[] = {
      "Show"
    };
    string atom_methods<NEportLevels=1,NEx=11.,NEy=286.>[] = {
      "Show"
    };
    string line_methods<NEportLevels=1,NEx=11.,NEy=330.>[] = {
      "Vectors"
    };
    string plot_methods<NEportLevels=1,NEx=11.,NEy=374.>[] = {
      "Plot", "Separate spins"
    };
    string plane_methods<NEportLevels=1,NEx=11.,NEy=418.>[] = {
      "Draw"
    };
#ifdef DLV_DL
    // Todo - surface plot needs testing
    string surface_scalar<NEportLevels=1,NEx=11.,NEy=462.>[] = {
      "Contours",
      "AVS Contours",
      "Shaded Contours",
      "Solid Contours",
      "P/N Contours",
      "Surface Plot"
    };
    string surface_scal_unstr<NEportLevels=1>[] = {
      "AVS Contours",
      "Shaded Contours",
      "Solid Contours",
      "P/N Contours",
      "Surface Plot"
    };
    string surface_vector<NEportLevels=1,NEx=11.,NEy=506.>[] = {
      "Vectors",
      "Streamlines"
    };
    string volume_scal_render<NEportLevels=1>[] = {
      "Isosurface",
      "Cells",
      "Spheres",
      "Volume"
    };
    string volume_scalar<NEportLevels=1,NEx=11.,NEy=550.>[] = {
      "Isosurface",
      "Cells",
      "Spheres"
    };
#else
    string surface_scalar<NEportLevels=1,NEx=11.,NEy=462.>[] = {
      "Contours",
      "AVS Contours",
      "Shaded Contours"
    };
    string surface_scal_unstr<NEportLevels=1>[] = {
      "AVS Contours",
      "Shaded Contours"
    };
    string surface_vector<NEportLevels=1,NEx=11.,NEy=506.>[] = {
      "Vectors",
      "Streamlines"
    };
    string volume_scal_render<NEportLevels=1>[] = {
      "Isosurface"
    };
    string volume_scalar<NEportLevels=1,NEx=11.,NEy=550.>[] = {
      "Isosurface"
    };
#endif // DLV_DL
    string volume_vector<NEportLevels=1,NEx=11.,NEy=594.>[] = {
      "Vectors",
      "Streamlines"
    };
    mlink method_list<NEportLevels=1,NEx=11.,NEy=638.> =>
      switch(select_method, , text_methods, point_methods, atom_methods,
	     line_methods, plot_methods, plane_methods, surface_scalar,
	     surface_scal_unstr, volume_scalar, volume_scal_render,
	     text_methods, text_methods, text_methods, text_methods,
	     text_methods, text_methods, /*vec*/ null_methods, null_methods,
	     null_methods, null_methods, null_methods, null_methods,
	     surface_vector, surface_vector, volume_vector, null_methods,
	     null_methods, null_methods, null_methods, null_methods,
	     null_methods, null_methods);
    int data_method<NEportLevels=1,NEx=740.,NEy=99.>;
    UIlabel method_title<NEx=363.,NEy=528.> {
      parent => <-.DLVshell.UIpanel;
      y => ((<-.sub_list.y + <-.sub_list.height) + 5);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      label = "Display As";
      visible => (is_valid(<-.sub_list.selectedItem) &&
		  (array_size(<-.method_list) > 1));
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist display_methods<NEx=363.,NEy=605.> {
      parent => <-.DLVshell.UIpanel;
      y => ((<-.method_title.y + <-.method_title.height) + 5);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      selectedItem => <-.data_method;
      visible => (is_valid(<-.sub_list.selectedItem) &&
		  (array_size(<-.method_list) > 1));
      strings => <-.method_list;
    };
    UIbutton delete_object<NEx=891.,NEy=324.> {
      parent => <-.DLVshell.UIpanel;
      x => (parent.clientWidth - width - 10);
      y => <-.Display.y;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible+nres => (is_valid(<-.data_object) &&
		       (array_size(<-.data_list[data_object].data_list) > 0));
      label = "Delete";
    };
    CCP3.Core_Modules.ViewData.delete_data delete_data<NEx=891.,NEy=387.> {
      trigger => <-.delete_object.do;
      object => <-.data_object;
    };
    UIbutton Display<NEx=858.,NEy=187.> {
      parent => <-.DLVshell.UIpanel;
      x = 10;
      y => ((<-.display_methods.y + <-.display_methods.height) + 5);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => (is_valid(<-.sub_list.selectedItem) &&
		  (is_valid(<-.data_method) || array_size(method_list) == 1));
    };
    CCP3.Core_Modules.ViewData.display_data display_data<NEx=891.,NEy=253.> {
      render => <-.Display.do;
      object => <-.data_object;
      component+nres => <-.data_list[<-.data_object].selection;
      //display_type => <-.data_method;
      display_type => switch ((<-.data_method < array_size(<-.method_list)) + 1,
			       0, <-.data_method);
    };
    CCP3.Renderers.Base.display_list &display_list[];
    int display_object;
    CCP3.Core_Macros.UI.UIobjs.DLVlist display_objects<NEx=682.,NEy=605.> {
      parent => <-.DLVshell.UIpanel;
      y => ((<-.Display.y + <-.Display.height) + 5);
      width => parent.clientWidth;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      visible => array_size(<-.display_list) > 0;
      strings => display_list.label;
      selectedItem => <-.display_object;
    };
    CCP3.Core_Macros.UI.ViewData.data_panels data_panels<NEx=836.,NEy=462.> {
      UIframe {
	parent => <-.<-.DLVshell.UIpanel;
	y => (<-.<-.display_objects.y + <-.<-.display_objects.height + 5);
	height => (parent.clientHeight - y);
      };
      preferences => <-.preferences;
      type_info => <-.display_list[<-.display_object].display_type;
      delete_display {
	//object => <-.<-.display_object;
	object+nres => <-.<-.display_list[<-.<-.display_object].index;
      };
      hide_display {
	object+nres => <-.<-.display_list[<-.<-.display_object].index;
      };
      StreamlineUI {
	update {
	  object+nres => <-.<-.<-.display_list[<-.<-.<-.display_object].index;
	};
      };
    };
    CCP3.Core_Modules.ViewData.select_display select_display<NEx=836.,NEy=649.>
    {
      object+nres => display_list[<-.display_object].index;
    };
  };
  macro menu_display {
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
      NEx=649.,NEy=88.>;
    int panel_open<NEportLevels=1,NEx=385.,NEy=33.> = 0;
    UIoption data<NEx=165.,NEy=110.> {
      label = "Data";
      set => <-.panel_open;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    //GMOD.instancer instancer<NEx=605.,NEy=165.> {
    //  Value => <-.panel_open;
    //  Group => displayUI;
    //};
    CCP3.Core_Macros.UI.ViewData.displayUI displayUI<NEx=352.,NEy=176.> {
      //instanced=0> {
      preferences => <-.preferences;
      DLVshell {
	UIshell {
	  visible => <-.<-.<-.panel_open;
	};
      };
    };
  };
};
