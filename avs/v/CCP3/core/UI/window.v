
flibrary Window<NEeditable=1> {
  macro app_window<hconnect=2> {
    CCP3.Core_Macros.Core.preferences &preferences<NEx=143.,NEy=33.>;
    UIapp UIapp<NEx=88.,NEy=121.> {
      //height = 224;
#ifdef MSDOS
      height = 180;
#else
      height = 160;
#endif // MSDOS
      width = 768;
      visible = 1;
      menu => <-.Menu_bar.menus;
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
      cancel = 0;
    };
    int model_type<NEportLevels=1,NEx=550.,NEy=44.> = -1;
    int active_menus<NEportLevels=1,NEx=451.,NEy=110.> =>
      ((projectUI.dialog_visible == 0) &&
       (ModelUI.dialog_visible == 0) &&
       (prefsUI.dialog_visible == 0));
    string project_name<NEportLevels=1,NEx=374.,NEy=44.> = "Unnamed";
    CCP3.Core_Macros.UI.Menus.Menu_bar Menu_bar<NEx=330.,NEy=176.> {
      preferences => <-.preferences;
      File_menu {
	UIcmdList {
	  cmdList => {
	    <-.<-.<-.projectUI.menu_file.dir_ops.create,
	    <-.<-.<-.projectUI.menu_file.sub_project.menu,
	    <-.<-.<-.ModelUI.menu_file.Create_Model.UIcmd,
	    <-.<-.<-.projectUI.menu_file.dir_ops.load,
	    <-.<-.<-.ModelUI.menu_file.Load_Model.load_menu,
	    <-.load_separator,
	    <-.<-.<-.projectUI.menu_file.Select.UIcmd,	    
	    <-.<-.<-.projectUI.menu_file.dir_ops.save,
	    <-.<-.<-.projectUI.menu_file.dir_ops.save_as,
	    <-.<-.<-.ModelUI.menu_file.Save_Model.save_menu,
	    <-.exit_separator,
	    <-.<-.<-.exit_app.UIcmd
	  };
	};
      };
      Edit_menu {
	UIcmdList {
	  cmdList => {
	    <-.<-.<-.ModelUI.menu_edit.Structure,
	    <-.<-.<-.DataUI.menu_edit.data,
	    <-.<-.<-.GlyphsUI.edit_point.UIcmd,
	    <-.<-.<-.GlyphsUI.edit_line.UIcmd,
	    <-.<-.<-.GlyphsUI.edit_plane.UIcmd,
	    <-.<-.<-.GlyphsUI.edit_box.UIcmd,
	    <-.<-.<-.GlyphsUI.edit_sphere.UIcmd,
	    <-.<-.<-.GlyphsUI.wulff.UIcmd,
	    <-.<-.<-.GlyphsUI.leed.UIcmd
	  };
	};
      };
      Display_menu {
	UIcmdList {
	  cmdList => {
	    <-.<-.<-.JobUI.menu_display.UIoption,
	    <-.<-.<-.ModelUI.menu_display.k_space.UIcmd,
	    <-.<-.<-.ModelUI.menu_display.Structure
	  };
	};
      };
      Prefs_menu {
	UIcmdList {
	  cmdList => {
	    <-.<-.<-.JobUI.menu_prefs.UIoption,
	    <-.<-.<-.prefsUI.menu_prefs.config.menu,
	    <-.<-.<-.prefsUI.menu_prefs.bonds.menu
	  };
	};
      };
      Help_menu {
	parent => <-.<-.UIapp;
      };
    };
    CCP3.Core_Macros.UI.Tools.Tool_bar Tool_bar<NEx=330.,NEy=253.,hconnect=2> {
      preferences => <-.preferences;
      parent => <-.UIapp;
    };
    UIpanel panel<NEx=330.,NEy=341.,NEportLevels={0,2}> {
      parent => <-.UIapp;
      x = 0;
      y => (<-.Tool_bar.tool_bar.height + <-.Tool_bar.tool_bar.y);
      width => parent.clientWidth;
      height => (parent.clientHeight - y - <-.Status_bar.AUstatus_bar.height);
      &color => <-.preferences.colour;
      &fontAttributes => <-.preferences.fontAttributes;
    };
    CCP3.Core_Macros.UI.Status.Status_bar Status_bar<NEx=330.,NEy=440.> {
      preferences => <-.preferences;
      parent => <-.UIapp;
    };
#ifndef DLV_BGS
    CCP3.Core_Macros.UI.Error.error_handler error_handler<NEx=352.,NEy=121.> {
      preferences => <-.preferences;
    };
#endif // DLV_BGS
    CCP3.Core_Macros.UI.Exit.exit_app exit_app<NEx=99.,NEy=561.> {
      preferences => <-.preferences;
      exit_check => (UIcmd.do | <-.UIapp.cancel);
      user_project => <-.projectUI.menu_file.user_project;
      parse_v_app {
	relative => <-.<-;
      };
      UIcmd {
	label = "Exit";
      };
    };
    /*CCP3.Core_Macros.UI.UIobjs.FileBrowser FileBrowser<NEx=693.,NEy=561.> {
      preferences => <-.preferences;
      };*/
    CCP3.Core_Macros.UI.Project.projectUI projectUI<NEx=693.,NEy=121.> {
      project_name => <-.project_name;
      preferences => <-.preferences;
      active_menus => <-.active_menus;
      UIparent => <-.UIapp;
      menu_file {
	model_type => <-.<-.model_type;
	/*dir_ops {
	  save {
	    active => (strcmp(<-.<-.<-.<-.project_name, "Unnamed") != 0);
	  };
	  };*/
      };
    };
    CCP3.Core_Macros.UI.Model.ModelUI ModelUI<NEx=693.,NEy=231.> {
      preferences => <-.preferences;
      active_menus => <-.active_menus;
      UIparent => <-.UIapp;
      model_type => <-.model_type;
    };
    CCP3.Core_Macros.UI.Data.DataUI DataUI<NEx=693.,NEy=308.> {
      preferences => <-.preferences;
      model_type => <-.model_type;
      menu_display {
	displayUI {
	  data_panels {
	    bondsUI {
	      main_panel_text {
		parent => <-.<-.<-.<-.<-.<-.panel;
	      };
	    };
	  };
	};
      };
    };
    CCP3.Core_Macros.UI.Jobs.JobUI JobUI<NEx=693.,NEy=385.> {
      preferences => <-.preferences;
      //parent => <-.UIapp;
    };
    CCP3.Core_Macros.UI.Glyphs.GlyphsUI GlyphsUI<NEx=693.,NEy=473.> {
      preferences => <-.preferences;
      active_menus => <-.active_menus;
      model_type => <-.model_type;
      UIparent => <-.UIapp;
    };
    CCP3.Core_Macros.UI.Prefs.prefsUI prefsUI<NEx=882.,NEy=117.> {
      preferences => <-.preferences;
      active_menus => <-.active_menus;
      UIparent => <-.UIapp;
    };
  };
};
