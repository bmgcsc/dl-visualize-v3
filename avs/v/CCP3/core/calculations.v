
flibrary Calculations<NEeditable=1,compile_subs=0> {
  group programs {
    boolean CRYSTAL98 = 0;
    boolean CRYSTAL03 = 0;
    boolean CRYSTAL06 = 0;
    boolean CRYSTAL09 = 0;
    boolean CRYSTAL14 = 0;
    boolean CRYSTAL17 = 0;
    boolean CRYSTAL21 = 0;
    boolean CRYSTAL_DEV = 0;
    boolean _CRYSTAL => (CRYSTAL98 || CRYSTAL03 || CRYSTAL06 ||
			 CRYSTAL09 || CRYSTAL14 || CRYSTAL17 ||
			 CRYSTAL21 || CRYSTAL_DEV);
    boolean GULP = 0;
    boolean EXCURV = 0;
    boolean SCF_KKR = 0;
    boolean K_P = 0;
    boolean CASTEP = 0;
    boolean CDS = 0;
    boolean GROWL = 0;
    //boolean PDB = 0;
    boolean ROD = 0;
    boolean CHEMSHELL = 0;
    boolean ONETEP = 0;
    boolean IMAGE = 0;
  };
  group object_data {
    int create_line<NEportLevels=1>;
    int create_plane<NEportLevels=1>;
    int create_volume<NEportLevels=1>;
    int list_lines<NEportLevels=1>;
    int list_planes<NEportLevels=1>;
    int list_volumes<NEportLevels=1>;
    string lines<NEportLevels=1>[];
    string planes<NEportLevels=1>[];
    string volumes<NEportLevels=1>[];
  };
  macro create_objects<NEx=297.,NEy=99.> {
    CCP3.Core_Macros.Calculations.object_data &data<NEx=176.,NEy=77.,
      NEportLevels={2,0}>;
    link model_type<NEportLevels={2,1},NEx=495.,NEy=77.>;
    GMOD.parse_v activate_lines<NEx=176.,NEy=209.> {
      trigger => <-.data.create_line;
      on_inst = 0;
    };
    GMOD.parse_v activate_planes<NEx=396.,NEy=209.> {
      trigger => <-.data.create_plane;
      on_inst = 0;
    };
    GMOD.parse_v activate_volumes<NEx=627.,NEy=209.> {
      trigger => <-.data.create_volume;
      on_inst = 0;
    };
    CCP3.Core_Modules.Objects.list_lines list_lines<NEx=176.,NEy=308.> {
      trigger => (<-.model_type || <-.data.list_lines);
      labels => <-.data.lines;
    };
    CCP3.Core_Modules.Objects.list_planes list_planes<NEx=396.,NEy=308.> {
      trigger => (<-.model_type || <-.data.list_planes);
      labels => <-.data.planes;
    };
    CCP3.Core_Modules.Objects.list_volumes list_volumes<NEx=627.,NEy=308.> {
      trigger => (<-.model_type || <-.data.list_volumes);
      labels => <-.data.volumes;
    };
  };
  macro config_calcs {
    programs programs<NEx=385.,NEy=176.> {
      CRYSTAL<NEportLevels={0,2}>;
      GULP<NEportLevels={0,2}>;
      EXCURV<NEportLevels={0,2}>;
      CASTEP<NEportLevels={0,2}>;
      SCF_KKR<NEportLevels={0,2}>;
      K_P<NEportLevels={0,2}>;
      CDS<NEportLevels={0,2}>;
      GROWL<NEportLevels={0,2}>;
      //PDB<NEportLevels={0,2}>;
      ROD<NEportLevels={0,2}>;					  
      CHEMSHELL<NEportLevels={0,2}>;					  
      ONETEP<NEportLevels={0,2}>;					  
      IMAGE<NEportLevels={0,2}>;					  
    };
    link job_data<NEportLevels=1,NEx=649.,NEy=176.>;
    CCP3.Core_Macros.Calculations.object_data object_data<NEx=22.,NEy=132.,
      NEportLevels={0,1}> {
      create_line = 0;
      create_plane = 0;
      create_volume = 0;
      //list_lines = 0;
      //list_planes = 0;
      //list_volumes = 0;
    };
    mlink atom_groups<NEx=918.,NEy=153.>;
    CCP3.Core_Macros.Calculations.create_objects create_objects<NEx=198.,
      NEy=132.> {
      data => <-.object_data;
      model_type => <-.model_type;
    };
    CCP3.Calcs.CRYSTAL.create_ui create_crystal<NEx=154.,NEy=451.,
      instanced=0> {
      make => <-.programs._CRYSTAL;
    };
    CCP3.Calcs.EXCURV.create_ui create_excurv<NEx=561.,NEy=451.,
      instanced=0> {
      make => <-.programs.EXCURV;
    };
    CCP3.Calcs.GULP.create_ui create_gulp<NEx=363.,NEy=451.,
      instanced=0> {
      make => <-.programs.GULP;
    };
    CCP3.Calcs.IMAGE.create_ui create_image<NEx=765.,NEy=621.,
      instanced=0> {
      make => <-.programs.IMAGE;
    };
    CCP3.Calcs.CASTEP.create_ui create_castep<NEx=770.,NEy=451.,
      instanced=0> {
      make => <-.programs.CASTEP;
    };
    //CCP3.Calcs.KKR.create_ui create_KKR<NEx=154.,NEy=627.,instanced=0> {
    //  make => <-.programs.SCF_KKR;
    //};
    CCP3.Calcs.K_P.create_ui create_K_P<NEx=363.,NEy=627.,instanced=0> {
      make => <-.programs.K_P;
    };
    CCP3.Calcs.ROD.create_ui create_ROD<instanced=0> {
      make => <-.programs.ROD;
    };		      
    CCP3.Calcs.CHEMSHELL.create_ui create_ChemShell<NEx=576.,NEy=621.,
      instanced=0> {
      make => <-.programs.CHEMSHELL;
    };
    CCP3.Calcs.ONETEP.create_ui create_onetep<NEx=954.,NEy=396.,
      instanced=0> {
      make => <-.programs.ONETEP;
    };
    CCP3.Core_Macros.UI.CDS.CDS CDS<NEx=918.,NEy=207.,instanced=0> {
      preferences => <-.preferences;
      active_menu => <-.active_menus;
      UIparent => <-.UIparent;
      //bond_all => <-.bond_all;
    };
    GMOD.instancer instance_CRYSTAL<NEx=154.,NEy=374.> {
      Value => <-.programs._CRYSTAL;
      Group => <-.create_crystal;
    };
    GMOD.instancer instance_GULP<NEx=363.,NEy=374.> {
      Value => <-.programs.GULP;
      Group => <-.create_gulp;
    };
    GMOD.instancer instance_EXCURV<NEx=561.,NEy=374.> {
      Value => <-.programs.EXCURV;
      Group => <-.create_excurv;
    };
    GMOD.instancer instance_CASTEP<NEx=770.,NEy=374.> {
      Value => <-.programs.CASTEP;
      Group => <-.create_castep;
    };
    //GMOD.instancer instance_KKR<NEx=154.,NEy=561.> {
    //  Value => <-.programs.SCF_KKR;
    //  Group => <-.create_KKR;
    //};
    GMOD.instancer instance_K_P<NEx=363.,NEy=561.> {
      Value => <-.programs.K_P;
      Group => <-.create_K_P;
    };
    GMOD.instancer instance_ROD<NEx=945.,NEy=450.> {
      Value => <-.programs.ROD;
      Group => <-.create_ROD;
    };		      
    GMOD.instancer instance_chemshell<NEx=576.,NEy=558.> {
      Value => <-.programs.CHEMSHELL;
      Group => <-.create_ChemShell;
    };
    GMOD.instancer instance_ONETEP<NEx=954.,NEy=342.> {
      Value => <-.programs.ONETEP;
      Group => <-.create_onetep;
    };
    GMOD.instancer instance_IM<NEx=765.,NEy=558.> {
      Value => <-.programs.IMAGE;
      Group => <-.create_image;
    };
    GMOD.instancer use_CDS<NEx=189.,NEy=549.> {
      Value => <-.programs.CDS;
      Group => <-.CDS;
    };
    GMOD.load_v_script load_v_script<NEx=385.,NEy=99.> {
      filename => <-.file;
      relative => <-.programs;
    };
    link preferences<NEportLevels=1,NEx=638.,NEy=99.>;
    link active_menus<NEportLevels=1,NEx=638.,NEy=44.>;
    link model_type<NEportLevels=1,NEx=407.,NEy=33.>;
#ifdef DLV_BABEL
    CCP3.Babel.Macros.Babel Babel<NEx=864.,NEy=180.> {
      dialog_visible = 0;
      preferences => <-.preferences;
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
    };
    int dialog_visible<NEportLevels=1,NEx=869.,NEy=99.> =>
      ((crystal_dialog == 1) || (gulp_dialog == 1) ||
       (excurv_dialog == 1) || (castep_dialog == 1) ||
       (kkr_dialog == 1) || (k_p_dialog == 1) || (rod_dialog == 1) ||
       (chemshell_dialog == 1) || (onetep_dialog == 1) ||
       (image_dialog == 1) || (Babel.dialog_visible == 1));
#else
    int dialog_visible<NEportLevels=1,NEx=869.,NEy=99.> =>
      ((crystal_dialog == 1) || (gulp_dialog == 1) ||
       (excurv_dialog == 1) || (castep_dialog == 1) ||
       (kkr_dialog == 1) || (k_p_dialog == 1) || (rod_dialog == 1) ||
       (chemshell_dialog == 1) || (onetep_dialog == 1) ||
       (image_dialog == 1));
#endif // DLV_BABEL
    link UIparent<NEportLevels=1,NEx=869.,NEy=44.>;
    string file<NEportLevels=1,NEx=165.,NEy=77.>;
    int crystal_dialog<NEx=154.,NEy=286.> = 0;
    int gulp_dialog<NEx=363.,NEy=286.> = 0;
    int excurv_dialog<NEx=561.,NEy=286.> = 0;
    int castep_dialog<NEx=770.,NEy=286.> = 0;
    int kkr_dialog<NEportLevels=1,NEx=154.,NEy=506.> = 0;
    int k_p_dialog<NEportLevels=1,NEx=363.,NEy=506.> = 0;
    int rod_dialog<NEx=621.,NEy=234.> = 0;
    int chemshell_dialog<NEportLevels=1,NEx=774.,NEy=504.> = 0;
    int onetep_dialog<NEportLevels=1,NEx=945.,NEy=288.> = 0;
    int image_dialog<NEportLevels=1,NEx=198.,NEy=243.> = 0;
    int versions<NEportLevels=1,NEx=154.,NEy=200.>[] => {
      programs.CRYSTAL98,
      programs.CRYSTAL03,
      programs.CRYSTAL06,
      programs.CRYSTAL09,
      programs.CRYSTAL14,
      programs.CRYSTAL17,
      programs.CRYSTAL21,
      programs.CRYSTAL_DEV
    };
  };
};
