
flibrary Core<NEeditable=1,compile_subs=0> {
  group preferences {
    UIcolor colour;
    UIfontAttributes fontAttributes;
    string+read+opt AGFont;
  };
  macro DLVcore {
    preferences preferences<NEx=418.,NEy=143.>;
    string prefs_file<NEportLevels=1,NEx=418.,NEy=44.> =>
      (environment.DLVRC + "/prefs.v");
    GMOD.load_v_script load_v_script<NEx=418.,NEy=99.> {
      filename => <-.prefs_file;
      relative => <-.preferences;
    };
    group environment<NEx=121.,NEy=33.> {
      string DLVRC<NEportLevels=1> => getenv("DLVRC");
    };
    string atom_prefs_file<NEportLevels=1,NEx=110.,NEy=121.> =>
      (environment.DLVRC + "/atoms.def");
    string display_file<NEportLevels=1,NEx=627.,NEy=121.> =>
      (environment.DLVRC + "/display.def");
    CCP3.Core_Modules.Init.initialise initialise<NEx=220.,NEy=187.> {
      atom_file => <-.atom_prefs_file;
      display_prefs => <-.display_file;
    };
    string thread_env<NEportLevels=1,NEx=81.,NEy=288.> =>
      getenv("DLV_NUM_THREADS");
    GMOD.string_to_val string_to_val<NEx=81.,NEy=342.> {
      str => <-.thread_env;
    };
    int nthreads<NEportLevels=1,NEx=81.,NEy=396.> =>
      switch(((array_size(string_to_val.vals) > 0) + 1), 1,
	     string_to_val.vals[0]);
  };
};
