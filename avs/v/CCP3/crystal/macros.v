
flibrary Macros<NEeditable=1,
#ifndef DLV_NO_DLLS
		dyn_libs="libCCP3crystal",
#endif // DLV_NO_DLLS
		export_cxx=1,
		build_dir="avs/src/express",
		cxx_hdr_files="avs/src/core/str_gen.hxx avs/src/core/map_gen.hxx avs/src/core/env_gen.hxx avs/src/crystal/load_gen.hxx avs/src/core/fb_gen.hxx avs/src/crystal/str_gen.hxx avs/src/crystal/save_gen.hxx avs/src/crystal/ex_gen.hxx avs/src/crystal/run_gen.hxx avs/src/crystal/curw_gen.hxx avs/src/crystal/wfn_gen.hxx avs/src/express/crystal_base.hxx avs/src/express/crystal_props.hxx",
		out_hdr_file="crystal.hxx",
		out_src_file="crystal.cxx"> {
  // Bascially a copy of core/UI/file_ops LoadUI
  macro loadstrUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
    CCP3.CRYSTAL.Modules.StructureData &data<NEx=792.,NEy=77.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
      width = 300;
      height = 300;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      title = "Load CRYSTAL Structure";
      ok = 0;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
#ifdef MSDOS
      y = 0;
#endif // MSDOS
    };
    UIpanel panel<NEx=374.,NEy=209.> {
      parent => <-.dialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    UIlabel UIlabel<NEx=660.,NEy=165.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y = 0;
      alignment = "center";
      width => parent.clientWidth;
      label = "Model name";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext DLVtext<NEx=671.,NEy=242.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 5;
      y => (<-.UIlabel.y + <-.UIlabel.height + 5);
      width => (parent.clientWidth - 10);
      text => <-.data.name;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle new_view<NEx=462.,NEy=451.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.FileB.y + <-.FileB.height + 5);
      width => parent.clientWidth;
      label = "Open in new viewer";
      set => <-.data.new_view;
    };
    /*CCP3.Core_Modules.Utils.FBdata FBdata<NEx=715.,NEy=352.,
      NEportLevels={0,1}> {
      filename<NEportLevels={2,0}> => <-.data.file;
      pattern = "*.str";
      parent1<NEportLevels={2,0}> => <-.panel;
      title = "Load CRYSTAL Structure filename";
      file_write = 0;
      x = 0;
      y => (<-.DLVtext.y + <-.DLVtext.height + 5);
      onlyButton = 0;
      ok = 0;
    };
    CCP3.Core_Modules.Utils.manageFB manageFB<NEx=704.,NEy=462.> {
      data => <-.FBdata;
      };*/
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=704.,NEy=462.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}> => <-.data.file;
      pattern = "*.str";
      parent => <-.panel;
      title = "Load CRYSTAL Structure filename";
      x = 0;
      y => (<-.DLVtext.y + <-.DLVtext.height + 5);
      manageFB {
	funit = "fort.34";
	use_funit => <-.<-.raw_file.set;
      };
    };
#ifdef DLV_DL
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle coord_type<NEx=462.,NEy=352.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.raw_file.y + <-.raw_file.height + 5);
      width => parent.clientWidth;
      label = "Fractional coordinates";
      set => <-.data.fractional;
    };
#endif // DLV_DL
    GMOD.parse_v reset_params<NEx=583.,NEy=77.> {
      v_commands = "data.new_view = 0; data.name = \"\";";
      relative => <-;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle raw_file<NEx=522.,NEy=549.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      //x => (parent.clientWidth - width) / 2;
      x = 0;
      y => (<-.new_view.y + <-.new_view.height + 5);
      width => parent.clientWidth;
      //width = 150;
      label = "Use Fortran Unit";
      set = 0;
    };
  };
  CCP3.CRYSTAL.Base.calcBase Load_Structure {
    UIcmd {
      label = "Load Structure";
    };
    CCP3.CRYSTAL.Modules.StructureData data<NEx=627.,NEy=143.,
      NEportLevels={0,1}> {
      name = "";
      file = "";
      new_view = 0;
      fractional = 0;
    };
    instancer {
      Group => <-.loadstrUI;
    };
    loadstrUI loadstrUI<NEx=528.,NEy=341.,instanced=0> {
      prefs => <-.prefs;
      data => <-.data;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
      /*manageFB {
	connect => <-.<-.visible;
	};*/
    };
    CCP3.CRYSTAL.Modules.load_structure load_structure<NEx=407.,NEy=484.> {
      data => <-.data;
      do_load => <-.loadstrUI.dialog.ok;
    };
  };
  macro savestrUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
      width = 300;
      height = 300;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      title = "Save CRYSTAL str file";
      ok = 0;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
#ifdef MSDOS
      y = 0;
#endif // MSDOS
    };
    UIpanel panel<NEx=374.,NEy=209.> {
      parent => <-.dialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    /*CCP3.Core_Modules.Utils.FBdata FBdata<NEx=715.,NEy=352.,
      NEportLevels={0,1}> {
      filename<NEportLevels={2,0}>;
      pattern = "*";
      parent1<NEportLevels={2,0}> => <-.panel;
      title = "Save CRYSTAL str file name";
      file_write = 1;
      x = 0;
      y = 0;
      onlyButton = 0;
      ok = 0;
    };
    CCP3.Core_Modules.Utils.manageFB manageFB<NEx=704.,NEy=462.> {
      data => <-.FBdata;
      };*/
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=704.,NEy=462.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}>;
      pattern = "*";
      parent => <-.panel;
      title = "Save CRYSTAL str file name";
      file_write = 1;
      x = 0;
      y = 0;
    };
#ifdef DLV_DL
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle DLVtoggle<NEx=495.,NEy=363.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.FileB.y + <-.FileB.height + 5);
      width => parent.clientWidth;
      label = "Save Geometry";
      set => <-.geometry;
    };
#endif // DLV_DL
    link geometry<NEportLevels=1,NEx=583.,NEy=55.>;
  };
  CCP3.CRYSTAL.Base.calcBase Save_Structure {
    UIcmd {
      label = "Save Structure";
    };
    instancer {
      Group => <-.savestrUI;
    };
    savestrUI savestrUI<NEx=528.,NEy=341.,instanced=0> {
      prefs => <-.prefs;
      dialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
      FileB {
	filename => <-.<-.data;
      };
      /*manageFB {
	connect => <-.<-.visible;
	};*/
      geometry => <-.geometry;
    };
    string data<NEx=627.,NEy=143.> = "";
    boolean geometry<NEportLevels=1,NEx=814.,NEy=143.> = 0;
    CCP3.CRYSTAL.Modules.save_str_file save_str_file<NEx=407.,NEy=484.> {
      filename => <-.data;
      geometry => <-.geometry;
      do_save => <-.savestrUI.dialog.ok;
    };
  };
  macro LoadUI {
    link selection<NEportLevels={2,1},NEx=297.,NEy=33.>;
    link filename<NEportLevels={2,1},NEx=605.,NEy=33.>;
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=825.,NEy=33.>;
    link load<NEportLevels={2,1},NEx=462.,NEy=33.>;
    string file_type_list<NEportLevels=1,NEx=451.,NEy=132.>[] = {
      "3D Charge Density/Potential",
      "3D Wannier Function",
      "3D E(k)",
      "Charge Density Slice",
      "Potential Slice",
      "Wannier Slice",
      "Density of States",
      "Band Structure",
      "Mulliken Population",
      "Phonons",
      "Bloch Function",
      "TD-DFT plot"
      //"State Occupancy",
    };
    CCP3.Core_Macros.UI.UIobjs.DLVdialog UItemplateDialog<NEx=110.,NEy=132.> {
      width = 300;
      height = 440;
      title = "Load CRYSTAL Property";
      ok => <-.load;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
#ifdef MSDOS
      y = 100;
#endif // MSDOS
    };
    UIpanel UIpanel<NEx=253.,NEy=198.> {
      parent => <-.UItemplateDialog;
      width => <-.parent.clientWidth;
      height => parent.clientHeight;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox DLVradioBox<NEx=297.,NEy=341.> {
      parent => <-.UIpanel;
      title = "Property File type";
      labels => <-.select_str_arr.out_strings;
      selectedItem => <-.selectedItem;
      width => parent.clientWidth;
      y = 0;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
    };
    /*CCP3.Core_Modules.Utils.FBdata FBdata<NEx=715.,NEy=297.,
      NEportLevels={0,1}> {
      filename<NEportLevels={2,0}> => <-.filename;
      parent1<NEportLevels={2,0}> => <-.UIpanel;
      title = "CRYSTAL Property filename";
      x = 0;
      y => ((<-.DLVradioBox.y + <-.DLVradioBox.height) + 5);
      pattern = "*";
      file_write = 0;
      onlyButton = 0;
      ok = 0;
    };
    CCP3.Core_Modules.Utils.manageFB manageFB<NEx=715.,NEy=396.> {
      data => <-.FBdata;
      };*/
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=715.,NEy=396.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}> => <-.filename;
      parent => <-.UIpanel;
      title = "CRYSTAL Property filename";
      x = 0;
      y => ((<-.DLVradioBox.y + <-.DLVradioBox.height) + 5);
      pattern = "*";
      manageFB {
	funit => <-.<-.fortran_units[<-.<-.selection];
	use_funit => <-.<-.raw_file.set;
      };
    };
    boolean allowed_props<NEportLevels=1,NEx=649.,NEy=132.>[8][12] = {
      { 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0 }, // 0 eigvecs
      { 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0 }, // 0
      { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0 }, // 1
      { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0 }, // 1
      { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, // 1
      { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, // 1
      { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, // 1
      { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }  // 1
    };
    string filename_list<NEportLevels=1,NEx=450.,NEy=225.>[];
    string fortran_units<NEportLevels=1,NEx=450.,NEy=279.>[] = {
      "fort.31", "fort.31", "fort.31",
      "fort.25", "fort.25", "fort.25",
      "DOSS.DAT", "BAND.DAT", "PPAN.DAT",
      "phonons.dlv", "fort.35", ""
    };
    link version<NEportLevels={2,1},NEx=693.,NEy=77.>;
    CCP3.Core_Modules.Utils.select_str_arr select_str_arr<NEx=484.,NEy=264.> {
      selections => <-.sub;
      in_strings => <-.file_type_list;
    };
    CCP3.Core_Modules.Utils.map_selection map_selection<NEx=495.,NEy=407.> {
      selections => <-.sub;
      selected_item => <-.selectedItem;
      index => <-.selection;
    };
    int selectedItem<NEportLevels=1,NEx=132.,NEy=264.> = 0;
    boolean sub<NEportLevels=1,NEx=649.,NEy=198.>[] => allowed_props[version];
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle raw_file {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      //x => (parent.clientWidth - width) / 2;
      x = 0;
      y => (<-.FileB.y + <-.FileB.height + 5);
      width => parent.clientWidth;
      //width = 150;
      label = "Use Fortran Unit";
      set = 0;
    };
  };
  CCP3.CRYSTAL.Base.baseUI extractUI {
    CCP3.Core_Macros.UI.UIobjs.DLVdialog UItemplateDialog<NEx=242.,NEy=132.> {
      parent => <-.UIparent;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      visible => <-.visible;
      width = 300;
      height = 300;
      title = "Extract Structure";
      //ok = 0;
      //okButton = 1;
      cancel = 0;
      cancelButton = 1;
#ifdef MSDOS
      y = 100;
#endif // MSDOS
    };
    UIpanel UIpanel<NEx=319.,NEy=220.> {
      parent => <-.UItemplateDialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox select<NEx=242.,NEy=319.> {
      parent => <-.UIpanel;
      y = 0;
      x = 0;
      title = "Select File";
      width => parent.clientWidth;
      labels => { "Input file", "Structure File" };
      color => <-.prefs.colour;
      selectedItem = 0;
      fontAttributes => <-.prefs.fontAttributes;
    };
    /*CCP3.Core_Modules.Utils.FBdata FBdata<NEportLevels={0,1}> {
      filename<NEportLevels={2,0}>;
      pattern = "*.inp";
      parent1<NEportLevels={2,0}> => <-.UIpanel;
      title = "Extract CRYSTAL Structure filename";
      file_write = 0;
      x = 0;
      y => (<-.select.y + <-.select.height + 5);
      onlyButton = 0;
      ok = 0;
    };
    CCP3.Core_Modules.Utils.manageFB manageFB {
      connect => (<-.visible && (<-.select.selectedItem == 0));
      data => <-.FBdata;
      };*/
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FBinput {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}>;
      pattern = "*.inp";
      parent => <-.UIpanel;
      title = "Extract CRYSTAL Structure filename";
      x = 0;
      y => (<-.select.y + <-.select.height + 5);
      UIpanel {
	visible => (<-.<-.select.selectedItem == 0);
      };
    };
    /*CCP3.Core_Modules.Utils.FBdata STRdata<NEportLevels={0,1}> {
      filename<NEportLevels={2,0}>;
      pattern = "*.str";
      parent1<NEportLevels={2,0}> => <-.UIpanel;
      title = "Extract CRYSTAL Structure filename";
      file_write = 0;
      x = 0;
      y => (<-.select.y + <-.select.height + 5);
      onlyButton = 0;
      ok = 0;
    };
    CCP3.Core_Modules.Utils.manageFB manageSTR {
      connect => (<-.visible && (<-.select.selectedItem == 1));
      data => <-.STRdata;
      };*/
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FBstr {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}>;
      pattern = "*.str";
      parent => <-.UIpanel;
      title = "Extract CRYSTAL Structure filename";
      x = 0;
      y => (<-.select.y + <-.select.height + 5);
      UIpanel {
	visible => (<-.<-.select.selectedItem == 1);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle view {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.FileB.y + <-.FileB.height + 5);
      width => parent.clientWidth;
      label = "Open in new viewer";
      //set => <-.data.new_view;
    };
    GMOD.parse_v parse_v<NEx=682.,NEy=220.>;
  };
  CCP3.CRYSTAL.Base.calcBase extract {
    int run<NEportLevels=1,NEx=539.,NEy=242.> = 0;
    string filename<NEportLevels=1,NEx=561.,NEy=187.>;
    string structure<NEportLevels=1,NEx=770.,NEy=187.>;
    UIcmd {
      label = "Extract Structure";
      active => <-.active_menus;
    };
    link job_data<NEx=770.,NEy=132.,NEportLevels={2,1}>;
    int new_view<NEportLevels=1,NEx=638.,NEy=308.> = 0;
    //link version<NEportLevels={2,1},NEx=803.,NEy=110.>;
    CCP3.CRYSTAL.Macros.extractUI extractUI<NEx=495.,NEy=374.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => <-.visible;
      UItemplateDialog {
	okButton => is_valid(<-.<-.filename);
	ok => <-.<-.run;
      };
      FBinput {
	filename => <-.<-.filename;
      };
      FBstr {
	filename => <-.<-.structure;
      };
      view {
	set => <-.<-.new_view;
      };
      parse_v {
	v_commands = "new_view = 0; structure = ;";
	relative => <-.<-;
      };
    };
    instancer {
      Group => extractUI;
    };
    CCP3.CRYSTAL.Modules.Structure Structure {
      run => <-.run;
      filename => <-.filename;
      strname => <-.structure;
      new_view => <-.new_view;
      job_data => <-.job_data;
    };
  };
  CCP3.CRYSTAL.Base.baseUI runUI {
    CCP3.Core_Macros.UI.UIobjs.DLVdialog UItemplateDialog<NEx=242.,NEy=132.> {
      parent => <-.UIparent;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      visible => <-.visible;
      width = 300;
      height = 300;
      title = "Run Input File";
      //ok = 0;
      //okButton = 1;
      cancel = 0;
      cancelButton = 1;
#ifdef MSDOS
      y = 100;
#endif // MSDOS
    };
    UIpanel UIpanel<NEx=319.,NEy=220.> {
      parent => <-.UItemplateDialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox select<NEx=242.,NEy=319.> {
      parent => <-.UIpanel;
      y = 0;
      x = 0;
      title = "Select File";
      width => parent.clientWidth;
      labels => { "Input file", "Structure File" };
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
    };
    /*CCP3.Core_Modules.Utils.FBdata FBdata<NEportLevels={0,1}> {
      filename<NEportLevels={2,0}>;
      pattern = "*.inp";
      parent1<NEportLevels={2,0}> => <-.UIpanel;
      title = "Run CRYSTAL Input filename";
      file_write = 0;
      x = 0;
      y => (<-.select.y + <-.select.height + 5);
      onlyButton = 0;
      ok = 0;
    };
    CCP3.Core_Modules.Utils.manageFB manageFB {
      connect => (<-.visible && (<-.select.selectedItem == 0));
      data => <-.FBdata;
      };*/
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FBinput {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}>;
      pattern = "*";
      parent => <-.UIpanel;
      title = "Run CRYSTAL Input filename";
      x = 0;
      y => (<-.select.y + <-.select.height + 5);
      UIpanel {
	visible => (<-.<-.select.selectedItem == 0);
      };
    };
    /*CCP3.Core_Modules.Utils.FBdata STRdata<NEportLevels={0,1}> {
      filename<NEportLevels={2,0}>;
      pattern = "*.str";
      parent1<NEportLevels={2,0}> => <-.UIpanel;
      title = "Run CRYSTAL Input filename";
      file_write = 0;
      x = 0;
      y => (<-.select.y + <-.select.height + 5);
      onlyButton = 0;
      ok = 0;
    };
    CCP3.Core_Modules.Utils.manageFB manageSTR {
      connect => (<-.visible && (<-.select.selectedItem == 1));
      data => <-.STRdata;
      };*/
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FBstr {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}>;
      pattern = "*";
      parent => <-.UIpanel;
      title = "Run CRYSTAL Input filename";
      x = 0;
      y => (<-.select.y + <-.select.height + 5);
      UIpanel {
	visible => (<-.<-.select.selectedItem == 1);
      };
    };
    GMOD.parse_v parse_v<NEx=682.,NEy=220.>;
  };
  CCP3.CRYSTAL.Base.calcBase run_input {
    int run<NEportLevels=1,NEx=539.,NEy=242.> = 0;
    string filename<NEportLevels=1,NEx=561.,NEy=187.>;
    string structure<NEportLevels=1,NEx=770.,NEy=187.>;
    link use_mpp<NEportLevels={2,1},NEx=594.,NEy=121.>;
    link job_data<NEx=869.,NEy=242.,NEportLevels={2,1}>;
    UIcmd {
      label = "Run Input";
      active => (<-.active_menus && (<-.version > 0));
    };
    link version<NEportLevels={2,1},NEx=803.,NEy=110.>;
    CCP3.CRYSTAL.Macros.runUI runUI<NEx=495.,NEy=374.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => <-.visible;
      UItemplateDialog {
	okButton => is_valid(<-.<-.filename);
	ok => <-.<-.run;
      };
      FBinput {
	filename => <-.<-.filename;
      };
      FBstr {
	filename => <-.<-.structure;
      };
      parse_v {
	v_commands = "structure = ;";
	relative => <-.<-;
      };
    };
    instancer {
      Group => runUI;
    };
    CCP3.CRYSTAL.Modules.Run_File Run_File {
      run => <-.run;
      filename => <-.filename;
      strname => <-.structure;
      job_data => <-.job_data;
      use_mpp => <-.use_mpp;
    };
  };
  CCP3.CRYSTAL.Base.baseUI analyseUI {
    CCP3.Core_Macros.UI.UIobjs.DLVdialog UItemplateDialog<NEx=242.,NEy=132.> {
      parent => <-.UIparent;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      visible => <-.visible;
      width = 300;
      height = 325;
      title = "Analyse Previous Job";
      ok = 0;
      //okButton = 1;
      cancel = 0;
      cancelButton = 1;
#ifdef MSDOS
      y = 100;
#endif // MSDOS
    };
    UIpanel UIpanel<NEx=319.,NEy=220.> {
      parent => <-.UItemplateDialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}>;
      pattern = "*.scf";
      parent => <-.UIpanel;
      title = "Analyse CRYSTAL Wavefunction filename";
      x = 0;
      y = 0;
      manageFB {
	funit = "fort.98";
	use_funit => <-.<-.raw_file.set;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle new_view<NEx=462.,NEy=451.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.FileB.y + <-.FileB.height + 5);
      width => parent.clientWidth;
      label = "Open in new viewer";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle raw_file {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.new_view.y + <-.new_view.height + 5);
      width => parent.clientWidth;
      label = "Use Fortran Unit";
      set = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle tddft_also {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.is_binary.y + <-.is_binary.height + 5);
      width => parent.clientWidth;
      label = "Include TD-DFT restart";
      visible => (<-.version > 7);
    };
    CCP3.Core_Macros.UI.UIobjs.FileBrowser tddftFB {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}>;
      pattern = "*.RESTART";
      parent => <-.UIpanel;
      title = "Attach CRYSTAL TD-DFT file";
      UIpanel {
	visible => <-.<-.tddft_also.set;
      };
      x = 0;
      y => (<-.tddft_also.y + <-.tddft_also.height + 5);
      //manageFB {
      //	funit = "TDDFT.RESTART";
      //use_funit = false;
      //};
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext message<NEx=225.,NEy=558.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.raw_file.y + <-.raw_file.height + 5);
      width => parent.clientWidth;
      text = "Binary files created on a different machine may "
	+ "not be compatible with this machine. Only use a "
	+ "binary wavefunction if you are sure it is safe.";
      rows = 4;
      outputOnly = 1;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle is_binary<NEx=702.,NEy=567.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.message.y + <-.message.height + 5);
      width => parent.clientWidth;
      label = "Wavefunction is binary";
    };
  };
  CCP3.CRYSTAL.Base.calcBase analyse_wfn {
    int run<NEportLevels=1,NEx=539.,NEy=242.> = 0;
    string filename<NEportLevels=1,NEx=561.,NEy=187.> = "";
    string tddft<NEportLevels=1> = "";
    int new_view<NEportLevels=1,NEx=759.,NEy=187.> = 0;
    int do_tddft = 0;
    int is_binary = 0;
    link job_data<NEportLevels={2,1},NEx=770.,NEy=121.>;
    link version;
    UIcmd {
      label = "Analyse Previous Job";
      active => <-.active_menus;
    };
    //link version<NEportLevels={2,1},NEx=803.,NEy=110.>;
    CCP3.CRYSTAL.Macros.analyseUI analyseUI<NEx=495.,NEy=374.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => <-.visible;
      UItemplateDialog {
	okButton => is_valid(<-.<-.filename);
	ok => <-.<-.run;
      };
      FileB {
	filename => <-.<-.filename;
      };
      new_view {
	set => <-.<-.new_view;
      };
      tddft_also {
	set => <-.<-.do_tddft;
      };
      tddftFB {
	filename => <-.<-.tddft;
      };
      is_binary {
	set => <-.<-.is_binary;
      };
    };
    instancer {
      Group => analyseUI;
    };
    CCP3.CRYSTAL.Modules.Wavefn Wavefn {
      run => <-.run;
      filename => <-.filename;
      new_view => <-.new_view;
      job_data => <-.job_data;
      version => <-.version;
      tddft_also => <-.do_tddft;
      tddft_file => <-.tddft;
      binary_wvfn => is_binary;
    };
  };
  CCP3.CRYSTAL.Base.calcBase current_wfn {
    int run<NEportLevels=1,NEx=539.,NEy=242.> = 0;
    link job_data<NEportLevels={2,1},NEx=770.,NEy=121.>;
    link version;
    UIcmd {
      label = "Analyse Current Wavefunction";
      active => <-.active_menus;
      do = 0;
    };
    CCP3.CRYSTAL.Modules.Analyse Analyse<NEx=440.,NEy=385.> {
      run => <-.UIcmd.do;
      job_data => <-.job_data;
      version => <-.version;
    };
  };
  CCP3.CRYSTAL.Base.calcBase Load_File {
    UIcmd {
      label = "Load Property";
      active => (<-.active_menus &&
		 (<-.model_type >= 0) && (<-.model_type < 4));
    };
    int file_type<NEportLevels=1,NEx=550.,NEy=187.> = 0;
    string filename<NEportLevels=1,NEx=781.,NEy=187.> = "";
    int do_load<NEportLevels=1,NEx=330.,NEy=187.> = 0;
    link version<NEportLevels={2,1},NEx=803.,NEy=110.>;
    link model_type<NEportLevels=1,NEx=814.,NEy=55.>;
    CCP3.CRYSTAL.Macros.LoadUI LoadUI<NEx=495.,NEy=374.,instanced=0> {
      selection => <-.file_type;
      filename => <-.filename;
      prefs => <-.prefs;
      load => <-.do_load;
      version => <-.version;
      UItemplateDialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
      /*manageFB {
	connect => <-.<-.visible;
	};*/
    };
    instancer {
      Group => <-.LoadUI;
    };
    CCP3.CRYSTAL.Modules.load_data load_data<NEx=462.,NEy=506.> {
      file_type => <-.file_type;
      filename => <-.filename;
      do_load => <-.do_load;
      version => <-.version;
    };
  };
  macro Properties<NEx=396.,NEy=143.> {
    UIcmdList UIcmdList<NEx=231.,NEy=132.> {
      label = "Properties";
      cmdList => {
	DOS.UIcmd,
	Bands.UIcmd,
	DOSbands.UIcmd,
	Mulliken.UIcmd,
	cd_slice.UIcmd,
	pot_slice.UIcmd,
	wan_slice.UIcmd,
	grid_3d.UIcmd,
	brillouin3D.UIcmd,
	dlv3D.UIcmd,
	bloch.UIcmd,
	wannier3D.UIcmd,
	tddft_3d.UIcmd,
	topond_3d.UIcmd,
	//orbitals.UIcmd,
	Options.UIcmd,
	Load_File.UIcmd
      };
    };
    link active_menus<NEportLevels=1,NEx=440.,NEy=66.>;
    int active_wvfn<NEportLevels=1,NEx=440.,NEy=110.> =>
      (active_menus && is_valid(wvfn.wavefn_set) && (wvfn.wavefn_set == 1));
    link UIparent<NEportLevels=1,NEx=660.,NEy=66.>;
    link prefs<NEportLevels=1,NEx=847.,NEy=66.>;
    int dialog_visible<NEportLevels=1,NEx=572.,NEy=132.> =>
      ((DOS.visible == 1) || (Bands.visible == 1) || (DOSbands.visible == 1) ||
       (Mulliken.visible == 1) || (cd_slice.visible == 1) ||
       (pot_slice.visible == 1) || (wan_slice.visible == 1) ||
       (grid_3d.visible == 1) || (brillouin3D.visible == 1) ||
       (dlv3D.visible == 1) || (wannier3D.visible == 1) ||
       (bloch.visible == 1) || (tddft_3d.visible == 1) ||
       (topond_3d.visible == 1) ||
       (orbitals.visible == 1) || (Load_File.visible == 1));
    link version<NEportLevels={2,1},NEx=792.,NEy=132.>;
    link model_type<NEportLevels={2,1},NEx=242.,NEy=66.>;
    link job_data<NEportLevels={2,1},NEx=891.,NEy=187.>;
    link objects<NEportLevels={2,1},NEx=660.,NEy=11.>;
    CCP3.CRYSTAL.Modules.Density_params Density<NEx=253.,NEy=187.,
      NEportLevels={0,1}>;
    CCP3.CRYSTAL.Modules.NewK_params NewK<NEx=451.,NEy=187.,
      NEportLevels={0,1}>;
    CCP3.Core_Modules.Data.band_paths band_paths<NEx=693.,NEy=187.,
      NEportLevels={0,1}>;
    CCP3.Core_Modules.Data.CRYSTAL_Wavefn_info &wvfn<NEportLevels={2,1},
      NEx=44.,NEy=187.>;
    CCP3.CRYSTAL.Props.grid_3d grid_3d<NEx=561.,NEy=440.> {
      prefs => <-.prefs;
      active_menus => (<-.active_wvfn && (model_type >= 0));
      UIparent => <-.UIparent;
      version => <-.version;
      model_type => <-.model_type;
      Density_params => <-.Density;
      NewK_params => <-.NewK;
      job_data => <-.job_data;
    };
    CCP3.CRYSTAL.Props.topond_3d topond_3d<NEx=441.,NEy=440.> {
      prefs => <-.prefs;
      active_menus => (<-.active_wvfn && (version > 3));
      UIparent => <-.UIparent;
      version => <-.version;
      model_type => <-.model_type;
      Density_params => <-.Density;
      NewK_params => <-.NewK;
      wvfn => <-.wvfn;
      topond3dUI {
	spin {
	  visible => <-.wvfn.spin;
	};
	elf {
	  labels => switch(<-.wvfn.spin+1, <-.string_ns, <-.string_spin);
	};
      };
      job_data => <-.job_data;
    };
    CCP3.CRYSTAL.Props.cd_slice cd_slice<NEx=341.,NEy=253.> {
      prefs => <-.prefs;
      active_menus => (<-.active_wvfn && (model_type >= 0));
      UIparent => <-.UIparent;
      Density_params => <-.Density;
      NewK_params => <-.NewK;
      version => <-.version;
      job_data => <-.job_data;
      objects => <-.objects;
    };
    CCP3.CRYSTAL.Props.pot_slice pot_slice<NEx=550.,NEy=253.> {
      prefs => <-.prefs;
      active_menus => (<-.active_wvfn && (model_type >= 0));
      UIparent => <-.UIparent;
      Density_params => <-.Density;
      NewK_params => <-.NewK;
      version => <-.version;
      job_data => <-.job_data;
      objects => <-.objects;
    };
    CCP3.CRYSTAL.Props.DOS DOS<NEx=121.,NEy=341.> {
      prefs => <-.prefs;
      active_menus => (<-.active_wvfn && (model_type > 0));
      UIparent => <-.UIparent;
      model_type => <-.model_type;
      Density_params => <-.Density;
      NewK_params => <-.NewK;
      wvfn => <-.wvfn;
      version => <-.version;
      job_data => <-.job_data;
      dosUI {
	parse_v#4 {
	  v_commands = "Options.visible = 1;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.CRYSTAL.Props.Bands Bands<NEx=341.,NEy=341.> {
      prefs => <-.prefs;
      active_menus => (<-.active_wvfn && (model_type > 0));
      UIparent => <-.UIparent;
      model_type => <-.model_type;
      Density_params => <-.Density;
      NewK_params => <-.NewK;
      version => <-.version;
      bandUI {
	paths => <-.<-.band_paths;
      };
      wvfn => <-.wvfn;
      job_data => <-.job_data;
    };
    CCP3.CRYSTAL.Props.DOSbands DOSbands<NEx=781.,NEy=341.> {
      prefs => <-.prefs;
      active_menus => (<-.active_wvfn && (model_type > 0));
      UIparent => <-.UIparent;
      version => <-.version;
      model_type => <-.model_type;
      Density_params => <-.Density;
      NewK_params => <-.NewK;
      wvfn => <-.wvfn;
      job_data => <-.job_data;
      dosBandUI {
	parse_v#4 {
	  v_commands = "Options.visible = 1;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.CRYSTAL.Props.Mulliken Mulliken<NEx=561.,NEy=341.> {
      prefs => <-.prefs;
      active_menus => (<-.active_wvfn && (model_type >= 0));
      UIparent => <-.UIparent;
      Density_params => <-.Density;
      NewK_params => <-.NewK;
      version => <-.version;
      job_data => <-.job_data;
    };
    CCP3.CRYSTAL.Props.wan_slice wan_slice<NEx=770.,NEy=253.> {
      prefs => <-.prefs;
      active_menus => (<-.active_wvfn && (model_type >= 0) &&
		       (version > 1));
      UIparent => <-.UIparent;
      version => <-.version;
      model_type => <-.model_type;
      job_data => <-.job_data;
      wvfn => <-.wvfn;
      Density_params => <-.Density;
      newk => <-.NewK;
      objects => <-.objects;
    };
    CCP3.CRYSTAL.Props.brillouin3D brillouin3D<NEx=121.,NEy=440.> {
      prefs => <-.prefs;
      active_menus => (<-.active_wvfn && (model_type > 0));
      UIparent => <-.UIparent;
      version => <-.version;
      model_type => <-.model_type;
      wvfn => <-.wvfn;
      job_data => <-.job_data;
      newk => <-.NewK;
      Density_params => <-.Density;
    };
    CCP3.CRYSTAL.Props.dlv3D dlv3D<NEx=341.,NEy=440.> {
      prefs => <-.prefs;
      active_menus => (<-.active_wvfn && (version > 1));
      UIparent => <-.UIparent;
      version => <-.version;
      model_type => <-.model_type;
      wvfn => <-.wvfn;
      job_data => <-.job_data;
      newk => <-.NewK;
      Density_params => <-.Density;
      objects => <-.objects;
    };
    CCP3.CRYSTAL.Props.bloch bloch<NEx=121.,NEy=495.> {
      prefs => <-.prefs;
      active_menus => (<-.active_wvfn && (version > 1));
      UIparent => <-.UIparent;
      model_type => <-.model_type;
      version => <-.version;
      job_data => <-.job_data;
      wvfn => <-.wvfn;
      Density_params => <-.Density;
      newk => <-.NewK;
      objects => <-.objects;
    };
    CCP3.CRYSTAL.Props.orbitals orbitals<NEx=121.,NEy=253.> {
      prefs => <-.prefs;
      active_menus => (<-.active_wvfn && (model_type >= 0));
      UIparent => <-.UIparent;
      version => <-.version;
      model_type => <-.model_type;
      job_data => <-.job_data;
    };
    CCP3.CRYSTAL.Props.wannier3D wannier3D<NEx=781.,NEy=440.> {
      prefs => <-.prefs;
      active_menus => (<-.active_wvfn && (model_type >= 0) &&
		       (version > 1));
      UIparent => <-.UIparent;
      version => <-.version;
      model_type => <-.model_type;
      job_data => <-.job_data;
      wvfn => <-.wvfn;
      Density_params => <-.Density;
      newk => <-.NewK;
      objects => <-.objects;
    };
    CCP3.CRYSTAL.Props.tddft_3d tddft_3d<NEx=342.,NEy=495.> {
      prefs => <-.prefs;
      active_menus => (<-.active_wvfn && is_valid(wvfn.tddft_done) &&
		       (wvfn.tddft_done == 1) && (model_type >= 0) &&
		       (<-.version > 7));
      UIparent => <-.UIparent;
      version => <-.version;
      model_type => <-.model_type;
      Density_params => <-.Density;
      NewK_params => <-.NewK;
      job_data => <-.job_data;
    };
    CCP3.CRYSTAL.Props.Options Options<NEx=374.,NEy=550.> {
      prefs => <-.prefs;
      active_menus => <-.active_wvfn;
      UIparent => <-.UIparent;
      wvfn => <-.wvfn;
      NewK_params => <-.NewK;
      Density_params => <-.Density;
    };
    CCP3.CRYSTAL.Macros.Load_File Load_File<NEx=737.,NEy=572.> {
      prefs => <-.prefs;
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      version => <-.version;
      model_type => <-.model_type;
    };
  };
  macro prefsUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=748.,NEy=66.>;
    link visible<NEportLevels={2,1},NEx=154.,NEy=66.>;
    link location<NEportLevels={2,1},NEx=748.,NEy=143.>;
    link crystal_version<NEportLevels={2,1},NEx=484.,NEy=66.>;
    CCP3.Core_Macros.UI.UIobjs.DLVshell DLVshell<NEx=374.,NEy=154.> {
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      title = "CRYSTAL Preferences";
      UIshell {
	visible => <-.<-.visible;
      };
    };
    CCP3.Core_Modules.Utils.select_str_arr select_str_arr<NEx=209.,NEy=242.> {
      in_strings => <-.names;
    };
    string names<NEportLevels=1,NEx=22.,NEy=187.>[] = {
      "CRYSTAL98",
      "CRYSTAL03",
      "CRYSTAL06",
      "CRYSTAL09",
      "CRYSTAL14",
      "CRYSTAL17",
      "CRYSTAL21",
      "CRYSTAL_dev"
    };
    string parallel_bins<NEportLevels=1,NEx=330.,NEy=374.>[] = {
      "Data Replicated (Pcrystal)","Massively Parallel (MPP)"};
    link pchoice<NEportLevels={2,1},NEx=517.,NEy=374.>;
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox UIradioBoxLabel<NEx=165.,NEy=448.> {
      parent => <-.DLVshell.UIpanel;
      selectedItem<NEportLevels={2,2}> = 0;
      title => "Version";
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels+nres => <-.select_str_arr.out_strings;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox Parallel<NEx=407.,NEy=451.> {
      parent => <-.DLVshell.UIpanel;
      y => ((UIradioBoxLabel.y + UIradioBoxLabel.height) + 5);
      selectedItem => <-.pchoice;
      title => "Parallel Binary";
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      visible => (<-.crystal_version > 1);
      labels => <-.parallel_bins;
    };
    UIlabel label<NEx=385.,NEy=308.> {
      y => ((Parallel.y + Parallel.height) + 5);
      width => parent.clientWidth;
      parent => <-.DLVshell.UIpanel;
      label => "Location of CRYSTAL executables";
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DirectoryBrowser DBrowser<NEx=726.,NEy=363.> {
      preferences => <-.prefs;
      y => ((label.y + label.height) + 5);
      location => <-.location;
      title = "CRYSTAL Binary location";
      UIpanel {
	parent => <-.<-.DLVshell.UIpanel;
      };
    };
  };
  macro Prefs {
    link selection<NEportLevels=1,NEx=429.,NEy=143.>;
    link location<NEx=693.,NEy=143.,NEportLevels={0,1}>;
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels=1,NEx=693.,NEy=44.>;
    link UIparent<NEportLevels=1,NEx=187.,NEy=77.>;
    UIoption Version<NEx=440.,NEy=33.> {
      label = "Preferences";
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    GMOD.instancer instancer {
      Value => <-.Version.set;
      Group => <-.prefsUI;
    };
    prefsUI prefsUI<NEx=396.,NEy=330.,instanced=0> {
      prefs => <-.prefs;
      visible => <-.Version.set;
      location => <-.location;
      crystal_version => <-.selection;
      DLVshell {
	UIshell {
	  parent => <-.<-.<-.UIparent;
	};
      };
      select_str_arr {
	selections => <-.<-.bool_versions;
      };
    };
    // Needs to be here to get version correct when first loads.
    link bool_versions<NEportLevels={1,1},NEx=121.,NEy=187.>;
    CCP3.Core_Modules.Utils.map_selection map_selection<NEx=198.,NEy=484.> {
      selections => <-.bool_versions;
      selected_item => <-.prefsUI.UIradioBoxLabel.selectedItem;
      index => <-.prefsUI.crystal_version;
    };
  };
  CCP3.Core_Macros.CalcObjs.calculation CRYSTAL {
    UIcmdList {
      label = "CRYSTAL";
      order = 11;
      direction = 1;
      cmdList => {
	Load_Structure.UIcmd,
	extract.UIcmd,
	SCF_calc.SubMenu,
	current_wfn.UIcmd,
	analyse_wfn.UIcmd,
	Properties.UIcmdList,
	run_input.UIcmd,
	Save_Structure.UIcmd,
	Prefs.Version
      };
    };
    dialog_visible => ((Load_Structure.visible == 1) ||
		       (Save_Structure.visible == 1) ||
		       (extract.visible == 1) ||
		       (SCF_calc.visible == 1) ||
		       (run_input.visible == 1) ||
		       (analyse_wfn.visible == 1) ||
		       (Properties.dialog_visible == 1));
    group Defaults<NEx=473.,NEy=187.> {
      int Version<NEportLevels={1,2}> = 0;
      boolean parallel_binary<NEportLevels={0,2}> = 0;
    };
    string exec_location<NEportLevels=1,NEx=308.,NEy=187.> = "";
    CCP3.Core_Modules.Calcs.exec_environ exec_environ<NEx=308.,NEy=264.> {
      location => <-.exec_location;
      default_var = "DLV_DEF_CRYSTAL";
      search_path = 1;
      main_var = "DLV_CRYSTAL";
      binary = "crystal";
    };
    link objects<NEportLevels={2,1},NEx=693.,NEy=165.>;
    link nselections<NEportLevels=1,NEx=882.,NEy=162.>;
    Load_Structure Load_Structure<NEx=110.,NEy=374.> {
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      prefs => <-.preferences;
      loadstrUI {
	FileB {
	  pattern => <-.<-.<-.file_extensions.structure;
	};
      };
    };
    CCP3.CRYSTAL.Macros.Save_Structure Save_Structure<NEx=319.,NEy=374.> {
      active_menus => (<-.active_menus && (<-.model_type >= 0) &&
		       (<-.model_type < 4));
      UIparent => <-.UIparent;
      prefs => <-.preferences;
    };
    CCP3.CRYSTAL.Macros.extract extract<NEx=319.,NEy=462.> {
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      prefs => <-.preferences;
      job_data => <-.job_data;
      extractUI {
	FBinput {
	  pattern => <-.<-.<-.file_extensions.input;
	};
	FBstr {
	  pattern => <-.<-.<-.file_extensions.structure;
	};
      };
    };
    CCP3.CRYSTAL.SCF.SCF_calc SCF_calc<NEx=528.,NEy=374.> {
      active_menus => (<-.active_menus && (model_type >= 0) &&
		       (<-.model_type < 4));
      UIparent => <-.UIparent;
      prefs => <-.preferences;
      version => <-.Defaults.Version;
      model_type => <-.model_type;
      use_mpp => <-.Defaults.parallel_binary;
      job_data => <-.job_data;
      scfUI {
	NebUI {
	  nselections => <-.<-.<-.nselections;
	};
      };
    };
    CCP3.CRYSTAL.Macros.run_input run_input<NEx=737.,NEy=374.> {
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      prefs => <-.preferences;
      version => <-.Defaults.Version;
      use_mpp => <-.Defaults.parallel_binary;
      job_data => <-.job_data;
      runUI {
	FBinput {
	  pattern => <-.<-.<-.file_extensions.input;
	};
	FBstr {
	  pattern => <-.<-.<-.file_extensions.structure;
	};
      };
    };
    CCP3.CRYSTAL.Macros.analyse_wfn analyse_wfn<NEx=110.,NEy=462.> {
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      prefs => <-.preferences;
      job_data => <-.job_data;
      version => <-.Defaults.Version;
      analyseUI {
	FileB {
	  pattern => <-.<-.<-.file_extensions.wavefunction;
	};
      };
    };
    CCP3.CRYSTAL.Macros.current_wfn current_wfn<NEx=517.,NEy=462.> {
      active_menus => (<-.active_menus && is_valid(Properties.wvfn.scf_done) &&
		       (Properties.wvfn.scf_done == 1));
      job_data => <-.job_data;
      version => <-.Defaults.Version;
    };
    Prefs Prefs<NEx=836.,NEy=539.> {
      prefs => <-.preferences;
      selection => <-.Defaults.Version;
      location => <-.exec_location;
      prefsUI {
	pchoice => <-.<-.Defaults.parallel_binary;
      };
    };
    GMOD.load_v_script load_v_script<NEx=99.,NEy=198.> {
      filename+nres => <-.<-.<-.DLVcore.environment.DLVRC + "/crystal";
      relative => <-.file_extensions;
    };
    group file_extensions<NEx=99.,NEy=270.> {
      string input<NEportLevels=1> = "*.inp";
      string structure<NEportLevels=1> = "*.str";
      string wavefunction<NEportLevels=1> = "*.scf";
      string volume_data = "*";
      string slice_data = "*";
      string doss_data = "*";
      string band_data = "*";
      string mulliken_data = "*";
      string phonon_data = "*";
      string bloch_data = "*";
    };
    CCP3.CRYSTAL.Macros.Properties Properties<NEx=572.,NEy=550.> {
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      prefs => <-.preferences;
      version => <-.Defaults.Version;
      model_type => <-.model_type;
      //wvfn => <-.analyse_wfn.wfn;
      job_data => <-.job_data;
      objects => <-.objects;
      Load_File {
	LoadUI {
	  filename_list => {
	    <-.<-.<-.file_extensions.volume_data,
	    <-.<-.<-.file_extensions.volume_data,
	    <-.<-.<-.file_extensions.volume_data,
	    <-.<-.<-.file_extensions.slice_data,
	    <-.<-.<-.file_extensions.slice_data,
	    <-.<-.<-.file_extensions.slice_data,
	    <-.<-.<-.file_extensions.doss_data,
	    <-.<-.<-.file_extensions.band_data,
	    <-.<-.<-.file_extensions.mulliken_data,
	    <-.<-.<-.file_extensions.phonon_data,
	    <-.<-.<-.file_extensions.bloch_data
	  };
	  FileB {
	    pattern => <-.filename_list[<-.selection];
	  };
	};
      };
      DOS {
	nselections => <-.<-.nselections;
      };
      DOSbands {
	nselections => <-.<-.nselections;
      };
    };
  };
};
