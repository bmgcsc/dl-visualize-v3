
flibrary Props<NEeditable=1,
#ifndef DLV_NO_DLLS
	       dyn_libs="libCCP3crystal",
#endif // DLV_NO_DLLS
	       export_cxx=1,
	       build_dir="avs/src/express",
	       cxx_hdr_files="avs/src/express/data.hxx avs/src/crystal/grid_gen.hxx avs/src/crystal/echg_gen.hxx avs/src/crystal/potm_gen.hxx avs/src/crystal/dos_gen.hxx avs/src/crystal/band_gen.hxx avs/src/crystal/bdos_gen.hxx avs/src/crystal/ppan_gen.hxx avs/src/express/crystal_scf.hxx avs/src/crystal/dlv3_gen.hxx avs/src/crystal/bloc_gen.hxx avs/src/crystal/wn3d_gen.hxx avs/src/core/objects/lbox_gen.hxx avs/src/crystal/bril_gen.hxx avs/src/crystal/wn2d_gen.hxx avs/src/crystal/td3d_gen.hxx avs/src/crystal/topo3d_gen.hxx",
	       out_hdr_file="crystal_props.hxx",
	       out_src_file="crystal_props.cxx"> {
  group PROP_limits {
    // Todo - are these correct for 06?
    int max_DOS_npoints = 200;
    int max_DOS_npoly = 25;
    int max_BAND_npoints = 1000;
    int max_mull_neigh = 10;
    float min_eigen = 0.00001;
    float max_eigen = 10;
  };
  CCP3.CRYSTAL.Base.baseUI props_baseUI {
    CCP3.Core_Macros.UI.UIobjs.DLVdialog UItemplateDialog<NEx=187.,NEy=88.> {
      parent => <-.UIparent;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      visible => <-.visible;
      ok<NEportLevels={1,2}>;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
#ifdef MSDOS
      y = 100;
#endif // MSDOS
      okLabelString = "Run";
    };
    UIpanel UIpanel<NEx=264.,NEy=143.> {
      parent => <-.UItemplateDialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle user_job {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      label = "I want to run this job without DLV";
    };
    CCP3.Core_Macros.UI.UIobjs.DirSave FileB {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}>;
      pattern = "*";
      title = "Run Property Job in Dir";
      //file_write = 1;
      parent => <-.UIpanel;
      //x => ((parent.clientWidth - width) / 2);
      //label = "Save Input";
      y => ((user_job.y + user_job.height) + 5);
    };    
  };
  props_baseUI grid3dUI {
    UItemplateDialog<NEx=187.,NEy=88.> {
      width = 350;
      height = 500;
      title = "CRYSTAL 3D Charge Density";
      okButton => ((<-.version < 2) ||
		   (<-.version >= 2 &&
		    (<-.params.calc_charge || <-.params.calc_potential)));

    };
    link version<NEportLevels=1,NEx=781.,NEy=22.>;
    CCP3.CRYSTAL.Modules.Grid3D_Data &params<NEx=506.,NEy=77.>;
    link model_type<NEportLevels={2,1},NEx=352.,NEy=77.>;
    string label<NEportLevels=1,NEx=682.,NEy=143.> = "n points";
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Npoints<NEx=187.,NEy=209.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => <-.label;
      fmin = 2;
      fval => <-.params.npoints;
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox UIradioBoxLabel<NEx=440.,NEy=209.> {
      parent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => <-.string;
      width => parent.clientWidth;
      x = 0;
      y => ((<-.pot_tolerance.y + <-.pot_tolerance.height) + 5);
      visible => (<-.model_type < 3);
      selectedItem => <-.params.non_periodic;
      UIpanel {
	visible => <-.visible;
      };
      UIlabel {
	label = "Range or Scale";
      };
    };
    string string<NEportLevels=1,NEx=517.,NEy=143.>[] = {
      "Specify Scale",
      "Specify Range"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield x_min<NEx=187.,NEy=275.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 100;
      y => x_label.y;
      width = 80;
      visible => (<-.model_type == 0);
      value => <-.params.x_min;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield x_max<NEx=385.,NEy=275.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 200;
      y => x_label.y;
      width = 80;
      visible => ((<-.UIradioBoxLabel.selectedItem == 1)
		  && (<-.model_type == 0));
      value => <-.params.x_max;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield y_min<NEx=187.,NEy=341.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 100;
      y => y_label.y;
      width = 80;
      visible => ((<-.model_type >= 0) && (<-.model_type < 2));
      value => <-.params.y_min;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield y_max<NEx=385.,NEy=341.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 200;
      y => y_label.y;
      width = 80;
      visible => ((<-.UIradioBoxLabel.selectedItem == 1)
		  && (<-.model_type < 2) && (<-.model_type >= 0));
      value => <-.params.y_max;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield z_min<NEx=187.,NEy=407.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 100;
      y => z_label.y;
      width = 80;
      visible => ((<-.model_type >= 0) && (<-.model_type < 3));
      value => <-.params.z_min;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield z_max<NEx=385.,NEy=407.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 200;
      y => z_label.y;
      width = 80;
      visible => ((<-.UIradioBoxLabel.selectedItem == 1) &&
		  (<-.model_type < 3) && (<-.model_type >= 0));
      value => <-.params.z_max;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle UItoggle<NEx=605.,NEy=407.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.z_label.y + <-.z_label.height) + 5);
      width => parent.clientWidth;
      label = "Conventional Cell";
      visible => ((<-.version > 0) && (<-.model_type == 3));
      set => <-.params.convcell;
    };
    UIlabel x_label<NEx=22.,NEy=275.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.UIradioBoxLabel.y + <-.UIradioBoxLabel.height) + 5);
      x = 0;
      width = 80;
      label = "x";
      visible => (<-.model_type == 0);
    };
    UIlabel y_label<NEx=22.,NEy=341.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.x_label.y + <-.x_label.height) + 5);
      x = 0;
      width = 80;
      label = "y";
      visible => ((<-.model_type >= 0) && (<-.model_type < 2));
    };
    UIlabel z_label<NEx=22.,NEy=418.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.y_label.y + <-.y_label.height) + 5);
      x = 0;
      width = 80;
      label = "z";
      visible => ((<-.model_type >= 0) && (<-.model_type < 3));
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle charge<NEx=242.,NEy=506.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      x = 5;
      y => ((<-.Npoints.y + <-.Npoints.height) + 5);
      visible => (<-.version > 1);
      set => <-.params.calc_charge;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle potential<NEx=484.,NEy=506.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      x = 5;
      y => ((<-.charge.y + <-.charge.height) + 5);
      visible => (<-.version > 1);
      set => <-.params.calc_potential;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider pot_tolerance<NEx=484.,NEy=572.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      x = 0;
      y => ((<-.potential.y + <-.potential.height) + 5);
      visible => ((<-.version > 1) && <-.params.calc_potential);
      value => <-.params.pot_itol;
    };
    user_job {
      y => ((UItoggle.y + UItoggle.height) + 10);
      set => <-.params.user_calc;
    };
    FileB {
      UIpanel {
	visible => <-.<-.params.user_calc;
      };
      filename => <-.params.user_dir;
    };
  };
  CCP3.CRYSTAL.Base.calcBase grid_3d {
    UIcmd {
      label = "3D Charge Density/Potential";
      active => <-.active_menus;
    };
    link version<NEportLevels={2,1},NEx=803.,NEy=110.>;
    link model_type<NEportLevels=1,NEx=700.,NEy=60.>;
    link job_data<NEportLevels={2,1},NEx=627.,NEy=143.>;
    CCP3.CRYSTAL.Modules.Density_params &Density_params<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.NewK_params &NewK_params<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.Grid3D_Data params<NEportLevels={0,1}> {
      calc_charge = 1;
      calc_potential = 0;
      tddft_prop = 0;
      excitation = 0;
      npoints = 20;
      non_periodic = 0;
      convcell = 0;
      pot_itol = 5;
      x_min = 1.0;
      x_max = 1.0;
      y_min = 1.0;
      y_max = 1.0;
      z_min = 1.0;
      z_max = 1.0;
      user_calc = 0;
      user_dir = "";
    };
    grid3dUI grid3dUI<NEx=495.,NEy=374.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => <-.visible;
      version => <-.version;
      model_type => <-.model_type;
      params => <-.params;
    };
    instancer {
      Group => grid3dUI;
    };
    CCP3.CRYSTAL.Modules.Grid3D Grid3D {
      run => <-.grid3dUI.UItemplateDialog.ok;
      data => <-.params;
      DMparams => <-.Density_params;
      NewK => <-.NewK_params;
      version => <-.version;
      job_data => <-.job_data;
    };
  };
  props_baseUI topond3dUI {
    UItemplateDialog<NEx=187.,NEy=88.> {
      width = 350;
      height = 575;
      title = "CRYSTAL 3D Topond plot";
      okButton => (<-.version >= 4);
    };
    link version<NEportLevels=1,NEx=781.,NEy=22.>;
    CCP3.CRYSTAL.Modules.Topond3D_Data &params<NEx=506.,NEy=77.>;
    link model_type<NEportLevels={2,1},NEx=352.,NEy=77.>;
    link wvfn<NEportLevels={2,1}>;
    string label<NEportLevels=1,NEx=682.,NEy=143.> = "n points";
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Npoints<NEx=187.,NEy=209.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => <-.label;
      fmin = 2;
      fval => <-.params.npoints;
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield x_min<NEx=187.,NEy=275.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 100;
      y => x_label.y;
      width = 80;
      visible => (<-.model_type == 0);
      value => <-.params.x_min;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield x_max<NEx=385.,NEy=275.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 200;
      y => x_label.y;
      width = 80;
      visible => (<-.model_type == 0);
      value => <-.params.x_max;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield y_min<NEx=187.,NEy=341.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 100;
      y => y_label.y;
      width = 80;
      visible => ((<-.model_type >= 0) && (<-.model_type < 2));
      value => <-.params.y_min;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield y_max<NEx=385.,NEy=341.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 200;
      y => y_label.y;
      width = 80;
      visible => ((<-.model_type < 2) && (<-.model_type >= 0));
      value => <-.params.y_max;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield z_min<NEx=187.,NEy=407.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 100;
      y => z_label.y;
      width = 80;
      visible => ((<-.model_type >= 0) && (<-.model_type < 3));
      value => <-.params.z_min;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield z_max<NEx=385.,NEy=407.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 200;
      y => z_label.y;
      width = 80;
      visible => ((<-.model_type < 3) && (<-.model_type >= 0));
      value => <-.params.z_max;
    };
    /*
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle UItoggle<NEx=605.,NEy=407.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.z_label.y + <-.z_label.height) + 5);
      width => parent.clientWidth;
      label = "Conventional Cell";
      visible => ((<-.version > 0) && (<-.model_type == 3));
      set => <-.params.convcell;
    };
    */
    UIlabel x_label<NEx=22.,NEy=275.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.elf.y + <-.elf.height) + 10);
      x = 0;
      width = 80;
      label = "x";
      visible => (<-.model_type == 0);
    };
    UIlabel y_label<NEx=22.,NEy=341.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.x_label.y + <-.x_label.height) + 5);
      x = 0;
      width = 80;
      label = "y";
      visible => ((<-.model_type >= 0) && (<-.model_type < 2));
    };
    UIlabel z_label<NEx=22.,NEy=418.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.y_label.y + <-.y_label.height) + 5);
      x = 0;
      width = 80;
      label = "z";
      visible => ((<-.model_type >= 0) && (<-.model_type < 3));
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle charge<NEx=242.,NEy=506.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      x = 5;
      y => ((<-.Npoints.y + <-.Npoints.height) + 5);
      label = "Electron density";
      set => <-.params.calc_electrons;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle spin<NEx=484.,NEy=506.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      x = 5;
      y => ((<-.charge.y + <-.charge.height) + 5);
      label = "Spin density";
      set => <-.params.calc_spin;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle laplacian<NEx=711.,NEy=504.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      x = 5;
      y => ((<-.spin.y + <-.spin.height) + 5);
      label = "Laplacian of the electron density";
      set => <-.params.calc_laplacian;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle nlaplacian<NEx=252.,NEy=567.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      x = 5;
      y => ((<-.laplacian.y + <-.laplacian.height) + 5);
      label = "Negative laplacian of the electron density";
      set => <-.params.calc_negative;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle gradient<NEx=495.,NEy=567.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      x = 5;
      y => ((<-.nlaplacian.y + <-.nlaplacian.height) + 5);
      label = "Magnitude of the gradient of the electron density";
      set => <-.params.calc_gradient;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle HamEk<NEx=720.,NEy=567.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      x = 5;
      y => ((<-.gradient.y + <-.gradient.height) + 5);
      label = "Hamiltonian kinetic energy density";
      set => <-.params.calc_hamiltonian;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle LEk<NEx=261.,NEy=639.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      x = 5;
      y => ((<-.HamEk.y + <-.HamEk.height) + 5);
      label = "Lagrangian kinetic energy density";
      set => <-.params.calc_lagrangian;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle virial<NEx=495.,NEy=639.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      x = 5;
      y => ((<-.LEk.y + <-.LEk.height) + 5);
      label = "Virial field density";
      set => <-.params.calc_virial;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox elf<NEx=720.,NEy=639.> {
      parent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      //labels => <-.string_ns;
      width => parent.clientWidth;
      x = 0;
      y => ((<-.virial.y + <-.virial.height) + 5);
      //visible => (<-.model_type < 3);
      selectedItem => <-.params.calc_elf;
      UIpanel {
	visible => <-.visible;
      };
      UIlabel {
	label = "Becke electron localization function";
      };
    };
    string string_ns<NEportLevels=1,NEx=517.,NEy=143.>[] = {
      "None",
      "ELF"
    };
    string string_spin<NEportLevels=1,NEx=517.,NEy=183.>[] = {
      "None",
      "alpha ELF",
      "beta ELF"
    };
    user_job {
      //y => ((UItoggle.y + UItoggle.height) + 10);
      y => ((z_label.y + z_label.height) + 10);
      set => <-.params.user_calc;
    };
    FileB {
      UIpanel {
	visible => <-.<-.params.user_calc;
      };
      filename => <-.params.user_dir;
    };
  };
  CCP3.CRYSTAL.Base.calcBase topond_3d {
    UIcmd {
      label = "3D Topond";
      active => <-.active_menus;
    };
    link version<NEportLevels={2,1},NEx=803.,NEy=110.>;
    link model_type<NEportLevels=1,NEx=700.,NEy=60.>;
    link wvfn<NEportLevels={2,1}>;
    link job_data<NEportLevels={2,1},NEx=627.,NEy=143.>;
    CCP3.CRYSTAL.Modules.Density_params &Density_params<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.NewK_params &NewK_params<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.Topond3D_Data params<NEportLevels={0,1}> {
      calc_electrons = 0;
      calc_spin = 0;
      calc_laplacian = 0;
      calc_negative = 0;
      calc_gradient = 0;
      calc_hamiltonian = 0;
      calc_lagrangian = 0;
      calc_virial = 0;
      calc_elf = 0;
      npoints = 20;
      //convcell = 0;
      x_min = 0.0;
      x_max = 1.0;
      y_min = 0.0;
      y_max = 1.0;
      z_min = 0.0;
      z_max = 1.0;
      user_calc = 0;
      user_dir = "";
    };
    topond3dUI topond3dUI<NEx=495.,NEy=374.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => <-.visible;
      version => <-.version;
      model_type => <-.model_type;
      wvfn => <-.wvfn;
      params => <-.params;
    };
    instancer {
      Group => topond3dUI;
    };
    CCP3.CRYSTAL.Modules.Topond3D Topond3D {
      run => <-.topond3dUI.UItemplateDialog.ok;
      data => <-.params;
      DMparams => <-.Density_params;
      NewK => <-.NewK_params;
      version => <-.version;
      job_data => <-.job_data;
    };
  };
  props_baseUI cdSliceUI {
    UItemplateDialog<NEx=231.,NEy=132.> {
      width = 350;
      height = 300;
      title = "CRYSTAL 2D Charge Density";
      okButton => is_valid(params.selection);
    };
    string points<NEportLevels=1,NEx=429.,NEy=132.> = "N Points";
    CCP3.CRYSTAL.Modules.CD2D_params &params<NEx=33.,NEy=121.,
      NEportLevels={0,1}>;
    CCP3.Core_Macros.Calculations.object_data &objects<NEx=803.,NEy=132.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein npoints<NEx=418.,NEy=264.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => <-.points;
      fmin = 2;
      fval => <-.params.npoints;
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist slice<NEx=231.,NEy=264.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      selectedItem => <-.params.selection;
      strings => <-.objects.planes;
      visible => (array_size(strings) > 0);
      y => ((<-.npoints.y + <-.npoints.height) + 5);
    };
    UIbutton make_grid<NEx=88.,NEy=341.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.slice.y + <-.slice.height + 5);
      do => <-.objects.create_plane;
      label = "Create Grid";
    };
    GMOD.parse_v init_bands {
      v_commands = "params.selection = ;";
      //trigger => <-.wvfn.nbands;
      on_inst = 1;
      relative => <-;
    };
    user_job {
      y => ((make_grid.y + make_grid.height) + 10);
      set => <-.params.user_calc;
    };
    FileB {
      UIpanel {
	visible => <-.<-.params.user_calc;
      };
      filename => <-.params.user_dir;
    };
  };
  CCP3.CRYSTAL.Base.calcBase cd_slice {
    UIcmd {
      label = "2D Charge Density";
      active => <-.active_menus;
    };
    link job_data<NEportLevels={2,1},NEx=627.,NEy=143.>;
    CCP3.CRYSTAL.Modules.Density_params &Density_params<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.NewK_params &NewK_params<NEportLevels={2,1}>;
    //link model_type<NEportLevels=1,NEx=814.,NEy=55.>;
    CCP3.CRYSTAL.Modules.CD2D_params params<NEportLevels={0,1}> {
      npoints = 20;
      user_calc = 0;
      user_dir = "";
    };
    link version<NEportLevels=1,NEx=781.,NEy=22.>;
    link objects<NEportLevels={2,1},NEx=583.,NEy=66.>;
    cdSliceUI cdSliceUI<NEx=495.,NEy=374.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => <-.visible;
      params => <-.params;
      objects => <-.objects;
    };
    instancer {
      Group => cdSliceUI;
    };
    CCP3.CRYSTAL.Modules.CDensity_2D CDensity_2D<NEx=231.,NEy=451.> {
      run => <-.cdSliceUI.UItemplateDialog.ok;
      data => <-.params;
      DMparams => <-.Density_params;
      NewK => <-.NewK_params;
      version => <-.version;
      job_data => <-.job_data;
    };
  };
  props_baseUI potSliceUI {
    UItemplateDialog<NEx=231.,NEy=132.> {
      width = 350;
      height = 390;
      title = "CRYSTAL 2D Potential";
      okButton => is_valid(params.selection);
    };
    CCP3.CRYSTAL.Modules.POT_params &params<NEx=33.,NEy=121.,
      NEportLevels={0,1}>;
    CCP3.Core_Macros.Calculations.object_data &objects<NEx=803.,NEy=132.,
      NEportLevels={2,0}>;
    string points<NEportLevels=1,NEx=429.,NEy=132.> = "N Points";
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein npoints<NEx=418.,NEy=264.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => <-.points;
      fmin = 2;
      fval => <-.params.npoints;
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist slice<NEx=231.,NEy=264.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      selectedItem => <-.params.selection;
      strings => <-.objects.planes;
      visible => (array_size(strings) > 0);
      y => ((<-.npoints.y + <-.npoints.height) + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein l<NEx=176.,NEy=341.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "max multipole L";
      fmin = 2;
      fval => <-.params.multipoles;
      x = 0;
      y => ((<-.make_grid.y + <-.make_grid.height) + 5);
      width => UIparent.clientWidth;
      label {
	width = 150;
      };
      field {
	x = 150;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein tol<NEx=429.,NEy=341.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Penetration tolerance";
      fmin = 2;
      fval => <-.params.tolerance;
      x = 0;
      y => ((<-.l.y + <-.l.height) + 5);
      width => UIparent.clientWidth;
      label {
	width = 150;
      };
      field {
	x = 150;
      };
    };
    UIbutton make_grid {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "Create Grid";
      do => <-.objects.create_plane;
      y => ((slice.y + slice.height) + 5);
    };
    GMOD.parse_v init_bands {
      v_commands = "params.selection = ;";
      //trigger => <-.wvfn.nbands;
      on_inst = 1;
      relative => <-;
    };
    user_job {
      y => ((tol.y + tol.height) + 10);
      set => <-.params.user_calc;
    };
    FileB {
      UIpanel {
	visible => <-.<-.params.user_calc;
      };
      filename => <-.params.user_dir;
    };
  };
  CCP3.CRYSTAL.Base.calcBase pot_slice {
    UIcmd {
      label = "2D Potential";
      active => <-.active_menus;
    };
    link job_data<NEportLevels={2,1},NEx=627.,NEy=143.>;
    link version<NEportLevels=1,NEx=781.,NEy=22.>;
    CCP3.CRYSTAL.Modules.Density_params &Density_params<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.NewK_params &NewK_params<NEportLevels={2,1}>;
    //link model_type<NEportLevels=1,NEx=814.,NEy=55.>;
    link objects<NEportLevels={2,1},NEx=583.,NEy=66.>;
    CCP3.CRYSTAL.Modules.POT_params params<NEportLevels={0,1}> {
      npoints = 20;
      multipoles = 4;
      tolerance = 5;
      user_calc = 0;
      user_dir = "";
    };
    potSliceUI potSliceUI<NEx=495.,NEy=374.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => <-.visible;
      params => <-.params;
      objects => <-.objects;
    };
    instancer {
      Group => potSliceUI;
    };
    CCP3.CRYSTAL.Modules.Potential_2D Potential_2D<NEx=231.,NEy=451.> {
      run => <-.potSliceUI.UItemplateDialog.ok;
      data => <-.params;
      DMparams => <-.Density_params;
      NewK => <-.NewK_params;
      version => <-.version;
      job_data => <-.job_data;
    };
  };
  props_baseUI dosUI {
    UItemplateDialog<NEx=187.,NEy=143.> {
      width = 350;
      height = 750;
      title = "CRYSTAL Density of States";
      ok<NEportLevels={1,2}>;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox type<NEx=165.,NEy=275.> {
      parent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => <-.plist;
      selectedItem => <-.params.projection;
    };
    string plist<NEportLevels=1,NEx=33.,NEy=209.>[] = {
      "Total Density",
      "Atom Projection",
      "Orbital Projection"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle viewer<NEx=803.,NEy=440.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.type.y + <-.type.height) + 5);
      /* not currently used
	 visible => (<-.params.projection == 1); */
      visible = 0;
      set => <-.params.in_viewer;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein npoints<NEx=231.,NEy=330.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      fmin = 2;
      fval => <-.params.npoints;
      flabel => <-.points_label;
      y => ((<-.viewer.y + <-.viewer.height) + 5);
      width => (UIparent.clientWidth - 125);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle use_energy<NEx=945.,NEy=243.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      label = "Set Energy Range";
      y => ((<-.npoints.y + <-.npoints.height) + 5);
      set => <-.params.energy_limits;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein energy_min<NEx=945.,NEy=306.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      fval => params.emin;
      //fmin => 1;
      fmax => params.emax;
      flabel => "Min Energy";
      width => (UIparent.clientWidth - 125);
      y => ((<-.use_energy.y + <-.use_energy.height) + 5);
      panel {
	visible => <-.<-.params.energy_limits;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein energy_max<NEx=945.,NEy=369.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      fval => params.emax;
      fmin => params.emin;
      //fmax => params.emax;
      flabel => "Max Energy";
      width => (UIparent.clientWidth - 125);
      y => ((<-.energy_min.y + <-.energy_min.height) + 5);
      panel {
	visible => <-.<-.params.energy_limits;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Bmin<NEx=187.,NEy=385.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      fval => params.bmin;
      fmin => 1;
      fmax => params.bmax;
      flabel => <-.bn_label;
      width => (UIparent.clientWidth - 125);
      y => ((<-.use_energy.y + <-.use_energy.height) + 5);
      panel {
	visible => (<-.<-.params.energy_limits == 0);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Bmax<NEx=407.,NEy=385.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      fval => params.bmax;
      fmin => params.bmin;
      fmax => wvfn.nbands;
      flabel => <-.bx_label;
      width => (UIparent.clientWidth - 125);
      y => ((<-.Bmin.y + <-.Bmin.height) + 5);
      panel {
	visible => (<-.<-.params.energy_limits == 0);
      };
    };
    /*UIbutton MinB_Plot {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 120;
      x => (parent.clientWidth - 125);
      y => Bmin.y;
      label = "Import Selection";
    };
    UIbutton MaxB_Plot {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 120;
      x => (parent.clientWidth - 125);
      y => Bmax.y;
      label = "Import Selection";
    };
    TodoGMOD.parse_v set_MinB {
      trigger => <-.MinB_Plot.do;
      on_inst = 0;
      relative => <-;
    };
    GMOD.parse_v set_MaxB {
      trigger => <-.MaxB_Plot.do;
      on_inst = 0;
      relative => <-;
      };*/
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein npolynomials<NEx=517.,
                                                            NEy=330.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      fval => <-.params.npoly;
      fmin = 2;
      flabel => <-.poly_label;
      y => ((<-.Valence.y + <-.Valence.height) + 5);
    };
    GMOD.parse_v set_projections {
      v_commands = "params.nprojections = 0; add.active = 1;"
	+ " accept.active = 0; UImultiList.active = 0; params.labels = ;";
      trigger => <-.visible;
      on_inst = 0;
      relative => <-;
    };
    GMOD.parse_v init_bands {
      v_commands = "params.bmin = 1; params.bmax = wvfn.nbands;"
	+ " params.projection = 0; add.active = 1; accept.active = 0;";
      trigger => <-.wvfn.nbands;
      on_inst = 1;
      relative => <-;
    };
    CCP3.CRYSTAL.Modules.DOS_params &params<NEx=473.,NEy=143.>;
    int model_type<NEportLevels={2,1},NEx=77.,NEy=44.>;
    GMOD.parse_v parse_v<NEx=627.,NEy=385.> {
      v_commands = "params.bmin = wvfn.vband_min;"
	+ " params.bmax = wvfn.vband_max;";
      trigger => <-.Valence.do;
      on_inst = 0;
      relative => <-;
    };
    CCP3.Core_Modules.Data.CRYSTAL_Wavefn_info &wvfn<NEx=660.,NEy=88.,
      NEportLevels={2,0}>;
    UIbutton Valence<NEx=33.,NEy=385.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.Bmax.y + <-.Bmax.height) + 5);
      visible => (<-.params.energy_limits == 0);
    };
    UIbutton UIbutton<NEx=385.,NEy=253.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "Modify K points";
      width = 200;
      x => ((parent.clientWidth - width) - 5);
      y => Valence.y;
    };
    GMOD.parse_v parse_v#4<NEx=418.,NEy=297.> {
      trigger => <-.UIbutton.do;
      on_inst = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVscrolledWindow proj_frame<NEx=77.,NEy=451.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      visible => (<-.params.projection > 0);
      width => parent.clientWidth;
      y => ((<-.FileB.y + <-.FileB.height) + 5);
      height => (((parent.clientHeight - <-.FileB.y)
		  - <-.FileB.height) - 5);
    };
    string message1 = "Click \"New Projection\" to create a new projection. Select the atoms to project on to in the viewer. Click \"Accept\" to complete the projection.";
    string message2 = "Click \"New Projection\" to create a new projection. Select the atoms in the viewer, then select the orbitals from the list. Click \"Accept\" to complete the projection.";
    CCP3.Core_Macros.UI.UIobjs.DLVtext message {
      parent => <-.proj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.virtualWidth;
#ifdef MSDOS
      height = 80;
#endif // MSDOS
      rows = 5;
      y = 0;
      outputOnly = 1;
      multiLine = 1;
      text => switch (<-.params.projection, <-.message1, <-.message2);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist projection_list {
      parent => <-.proj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.message.y + <-.message.height) + 5);
      width => parent.virtualWidth;
      strings => <-.params.labels;
    };
    UIbutton add<NEx=165.,NEy=506.> {
      parent => <-.proj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 5;
      y => ((<-.projection_list.y + <-.projection_list.height) + 5);
      visible => (<-.params.nprojections < 10);
      width = 120;
      label = "New Projection";
      do = 0;
    };
    GMOD.parse_v parse_v#2 {
      v_commands = "add.active = 0; accept.active = 1;"
	+ " UImultiList.active = 1;";
      trigger => <-.add.do;
      on_inst = 0;
      relative => <-;
    };
    UIbutton accept<NEx=385.,NEy=506.> {
      parent => <-.proj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => ((parent.virtualWidth / 2) + 5);
      y => add.y;
      visible => (<-.params.nprojections < 10);
      active = 0;
      width = 120;
      label = "Accept Projection";
      do = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle all_atoms<NEx=627.,NEy=451.> {
      parent => <-.proj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => ((<-.add.y + <-.add.height) + 5);
      set => <-.params.project_all;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle expand_atoms<NEx=627.,NEy=451.> {
      parent => <-.proj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => ((<-.all_atoms.y + <-.all_atoms.height) + 5);
      visible => (<-.params.projection == 2);
      set => <-.params.expand_atoms;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle expand_states<NEx=627.,NEy=451.> {
      parent => <-.proj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => ((<-.expand_atoms.y + <-.expand_atoms.height) + 5);
      visible => (<-.params.projection == 2);
      set => <-.params.expand_states;
    };
    GMOD.parse_v parse_v#3 {
      v_commands = "accept.active = 0; UImultiList.active = 0;"
	+ " add.active = 1;";
      trigger => <-.accept.do;
      on_inst = 0;
      relative => <-;
    };
    //int selections<NEportLevels=1>[];
    UImultiList UImultiList<NEx=253.,NEy=583.> {
      parent => <-.proj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.virtualWidth;
      y => ((<-.expand_states.y + <-.expand_states.height) + 5);
      height = 150;
      visible => (<-.params.projection == 2);
      active = 0;
      &selectedItems => <-.params.orbitals;
    };
    string points_label<NEportLevels=1,NEx=22.,NEy=253.> = "n points";
    string bn_label<NEportLevels=1,NEx=11.,NEy=286.> = "Start Band";
    string bx_label<NEportLevels=1,NEx=22.,NEy=319.> = "End Band";
    string poly_label<NEportLevels=1,NEx=682.,NEy=286.> = "n polynomials";
    user_job {
      y => ((npolynomials.y + npolynomials.height) + 10);
      set => <-.params.user_calc;
    };
    FileB {
      UIpanel {
	visible => <-.<-.params.user_calc;
      };
      filename => <-.params.user_dir;
    };
  };
  CCP3.CRYSTAL.Base.calcBase DOS {
    UIcmd {
      label = "Density of States";
      active => <-.active_menus;
    };
    link model_type<NEportLevels=1,NEx=814.,NEy=55.>;
    link version<NEportLevels={2,1},NEx=803.,NEy=110.>;
    link job_data<NEportLevels={2,1},NEx=627.,NEy=143.>;
    CCP3.CRYSTAL.Modules.Density_params &Density_params<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.NewK_params &NewK_params<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.DOS_params params {
      projection = 0;
      nprojections = 0;
      in_viewer = 1;
      npoints = 200;
      bmin = 0;
      bmax = 0;
      npoly = 6;
      energy_limits = 0;
      project_all = 0;
      expand_atoms = 0;
      expand_states = 0;
      user_calc = 0;
      user_dir = "";
    };
    link wvfn<NEportLevels={2,1},NEx=770.,NEy=154.>;
    link nselections;
    dosUI dosUI<NEx=495.,NEy=374.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      model_type => <-.model_type;
      params => <-.params;
      visible => <-.visible;
      wvfn => <-.wvfn;
      UImultiList {
	strings => <-.<-.density.labels;
      };
    };
    instancer {
      Group => dosUI;
      active = 1; // add/accept.active doesn't seem to work right otherwise
    };
    CCP3.CRYSTAL.Modules.density_of_states density<NEx=561.,NEy=253.> {
      make => <-.visible;
      init => <-.params.projection;
      atoms => <-.params.expand_atoms;
      states => <-.params.expand_states;
      nselections => <-.nselections;
      add => <-.dosUI.accept.do;
      run => <-.dosUI.UItemplateDialog.ok;
      cancel => <-.dosUI.UItemplateDialog.cancel;
      data => <-.params;
      DMparams => <-.Density_params;
      NewK => <-.NewK_params;
      version => <-.version;
      job_data => <-.job_data;
    };
  };
  props_baseUI bandUI {
    int model_type<NEportLevels={2,1},NEx=165.,NEy=55.>;
    CCP3.Core_Modules.Data.CRYSTAL_Wavefn_info &wvfn<NEx=561.,NEy=121.,
      NEportLevels={2,1}>;
    UItemplateDialog<NEx=187.,NEy=253.> {
      width = 350;
      height = 500;
      title = "CRYSTAL Band Structure";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Bmin<NEx=209.,NEy=385.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => <-.bn_label;
      fval => params.bmin;
      fmin => 1;
      fmax => params.bmax;
      y => ((<-.npoints.y + <-.npoints.height) + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Bmax<NEx=429.,NEy=385.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => <-.bx_label;
      fval => params.bmax;
      fmin => params.bmin;
      fmax => wvfn.nbands;
      y => ((<-.Bmin.y + <-.Bmin.height) + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein npoints<NEx=583.,NEy=330.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => <-.plabel;
      y = 0;
      fval => params.npoints;
      fmin => 2;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext path<NEx=264.,NEy=462.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.Valence.y + <-.Valence.height) + 5);
      text => params.path;
      //resizeToText = 1;
      multiLine = 1;
      rows = 8;
      width => parent.clientWidth;
    };
    UIbutton Valence {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.Bmax.y + <-.Bmax.height) + 5);
    };
    UIbutton show_path {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => Valence.y;
      x => parent.clientWidth - width - 5;
      do = 0;
    };
    GMOD.parse_v set_valence {
      v_commands = "params.bmin = wvfn.vband_min;"
	+ " params.bmax = wvfn.vband_max;";
      trigger => <-.Valence.do;
      on_inst = 0;
      relative => <-;
    };
    GMOD.parse_v init_bands {
      v_commands = "params.bmin = 1; params.bmax = wvfn.nbands;";
      trigger => <-.wvfn.nbands;
      on_inst = 1;
      relative => <-;
    };
    user_job {
      y => ((path.y + path.height) + 10);
      set => <-.params.user_calc;
    };
    FileB {
      UIpanel {
	visible => <-.<-.params.user_calc;
      };
      filename => <-.params.user_dir;
    };
    string plabel<NEportLevels=1,NEx=33.,NEy=253.> = "n points";
    string bn_label<NEportLevels=1,NEx=33.,NEy=308.> = "Start Band";
    string bx_label<NEportLevels=1,NEx=33.,NEy=363.> = "End Band";
    CCP3.CRYSTAL.Modules.BandParams &params<NEx=231.,NEy=176.,
      NEportLevels={0,1}>;
    CCP3.Core_Modules.Data.band_paths &paths;
    string curpath<NEx=660.,NEy=231.> => switch ((is_valid(model_type) + 1), "?", switch ((model_type + 1), "?", "?", paths.path2D, paths.path3D.lattices[wvfn.lattice].centers[wvfn.centre].path));
    GMOD.parse_v parse_v<NEx=649.,NEy=385.> {
      //v_commands = "params.path = curpath;";
      trigger => <-.curpath;
      on_inst = 1;
      //relative => <-;
    };
  };
  CCP3.CRYSTAL.Base.calcBase Bands {
    UIcmd {
      label = "Band Structure";
      active => <-.active_menus;
    };
    link model_type<NEportLevels=1,NEx=814.,NEy=55.>;
    link job_data<NEportLevels={2,1},NEx=627.,NEy=143.>;
    CCP3.CRYSTAL.Modules.Density_params &Density_params<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.NewK_params &NewK_params<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.BandParams params<NEportLevels={0,1}> {
      path = "?";
      npoints = 50;
      bmin = 0;
      bmax = 0;
      user_calc = 0;
      user_dir = "";
    };
    link wvfn<NEportLevels={2,1},NEx=627.,NEy=143.>;
    link version<NEportLevels={2,1},NEx=803.,NEy=110.>;
    bandUI bandUI<NEx=495.,NEy=374.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => <-.visible;
      params => <-.params;
      model_type => <-.model_type;
      wvfn => <-.wvfn;
      parse_v {
	v_commands = "params.path = bandUI.curpath;";
	relative => <-.<-;
      };
    };
    instancer {
      Group => bandUI;
    };
    CCP3.CRYSTAL.Modules.band_structure band_structure {
      run => <-.bandUI.UItemplateDialog.ok;
      display => <-.bandUI.show_path.do;
      data => <-.params;
      DMparams => <-.Density_params;
      NewK => <-.NewK_params;
      version => <-.version;
      job_data => <-.job_data;
    };
  };
  props_baseUI dosBandUI {
    int model_type<NEportLevels={2,1},NEx=165.,NEy=55.>;
    CCP3.Core_Modules.Data.CRYSTAL_Wavefn_info &wvfn<NEx=561.,NEy=121.,
      NEportLevels={2,1}>;
    UItemplateDialog<NEx=187.,NEy=253.> {
      width = 350;
      height = 700;
      title = "CRYSTAL Bands and DOS";
    };
    CCP3.CRYSTAL.Modules.BandParams &Bparams<NEx=231.,NEy=176.,
      NEportLevels={0,1}>;
    CCP3.CRYSTAL.Modules.DOS_params &Dparams<NEx=473.,NEy=143.>;
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Bmin<NEx=209.,NEy=385.> {
      UIparent => <-.UIpanel;
      width => UIparent.clientWidth;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => <-.bn_label;
      fval => Bparams.bmin;
      fmin => 1;
      fmax => Bparams.bmax;
      y => ((<-.npoints.y + <-.npoints.height) + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Bmax<NEx=429.,NEy=385.> {
      UIparent => <-.UIpanel;
      width => UIparent.clientWidth;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => <-.bx_label;
      fval => Bparams.bmax;
      fmin => Bparams.bmin;
      fmax => wvfn.nbands;
      y => ((<-.Bmin.y + <-.Bmin.height) + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein npoints<NEx=583.,NEy=330.> {
      UIparent => <-.UIpanel;
      width => UIparent.clientWidth;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => <-.plabel;
      y = 0;
      fval => Bparams.npoints;
      fmin => 2;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtext path<NEx=264.,NEy=462.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.Valence.y + <-.Valence.height) + 5);
      text => Bparams.path;
      //resizeToText = 1;
      multiLine = 1;
      rows = 8;
      width => parent.clientWidth;
    };
    UIbutton Valence {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.Bmax.y + <-.Bmax.height) + 5);
    };
    GMOD.parse_v set_valence {
      v_commands = "Bparams.bmin = wvfn.vband_min;"
	+ " Bparams.bmax = wvfn.vband_max; Dparams.bmin = wvfn.vband_min;"
	+ " Dparams.bmax = wvfn.vband_max;";
      trigger => <-.Valence.do;
      on_inst = 0;
      relative => <-;
    };
    GMOD.parse_v init_bands {
      v_commands = "Bparams.bmin = 1; Bparams.bmax = wvfn.nbands;"
	+ " Dparams.bmin = 1; Dparams.bmax = wvfn.nbands;"
	+ " add.active = 1; accept.active = 0;";
      trigger => <-.wvfn.nbands;
      on_inst = 1;
      relative => <-;
    };
    string plabel<NEportLevels=1,NEx=33.,NEy=253.> = "n Band points";
    string bn_label<NEportLevels=1,NEx=33.,NEy=308.> = "Start Band";
    string bx_label<NEportLevels=1,NEx=33.,NEy=363.> = "End Band";
    CCP3.Core_Modules.Data.band_paths &paths;
    string curpath<NEx=660.,NEy=231.> => switch ((is_valid(model_type) + 1), "?", switch ((model_type + 1), "?", "?", paths.path2D, paths.path3D.lattices[wvfn.lattice].centers[wvfn.centre].path));
    GMOD.parse_v parse_v<NEx=649.,NEy=385.> {
      //v_commands = "params.path = curpath;";
      trigger => <-.curpath;
      on_inst = 1;
      //relative => <-;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox type<NEx=165.,NEy=275.> {
      parent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => <-.plist;
      selectedItem => <-.Dparams.projection;
      y => (<-.path.y + <-.path.height + 5);
    };
    string plist<NEportLevels=1,NEx=33.,NEy=209.>[] = {
      "Total Density",
      "Atom Projection",
      "Orbital Projection"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle viewer<NEx=803.,NEy=440.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.type.y + <-.type.height) + 5);
      /* not currently used
	 visible => (<-.Dparams.projection == 1); */
      visible = 0;
      set => <-.Dparams.in_viewer;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Dpoints<NEx=231.,NEy=330.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      fmin = 2;
      fval => <-.Dparams.npoints;
      flabel => <-.points_label;
      y => ((<-.viewer.y + <-.viewer.height) + 5);
      width => (UIparent.clientWidth - 125);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein npolynomials<NEx=517.,
                                                            NEy=330.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      fval => <-.Dparams.npoly;
      fmin = 2;
      flabel => <-.poly_label;
      y => ((<-.Dpoints.y + <-.Dpoints.height) + 5);
    };
    GMOD.parse_v set_projections {
      v_commands = "Dparams.nprojections = 0; add.active = 1;"
	+ " accept.active = 0; UImultiList.active = 0; Dparams.labels = ;";
      trigger => <-.visible;
      on_inst = 0;
      relative => <-;
    };
    CCP3.Core_Modules.Data.CRYSTAL_Wavefn_info &wvfn<NEx=660.,NEy=88.,
      NEportLevels={2,0}>;
    UIbutton UIbutton<NEx=385.,NEy=253.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "Modify K points";
      width = 200;
      x => ((parent.clientWidth - width) - 5);
      y => Valence.y;
    };
    GMOD.parse_v parse_v#4<NEx=418.,NEy=297.> {
      trigger => <-.UIbutton.do;
      on_inst = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVscrolledWindow proj_frame<NEx=77.,NEy=451.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      visible => (<-.Dparams.projection > 0);
      width => parent.clientWidth;
      y => ((<-.FileB.y + <-.FileB.height) + 5);
      height => (((parent.clientHeight - <-.FileB.y)
		  - <-.FileB.height) - 5);
    };
    string message1 = "Click \"New Projection\" to create a new projection. Select the atoms to project on to in the viewer. Click \"Accept\" to complete the projection.";
    string message2 = "Click \"New Projection\" to create a new projection. Select the atoms in the viewer, then select the orbitals from the list. Click \"Accept\" to complete the projection.";
    CCP3.Core_Macros.UI.UIobjs.DLVtext message {
      parent => <-.proj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.virtualWidth;
#ifdef MSDOS
      height = 80;
#endif // MSDOS
      rows = 5;
      y = 0;
      outputOnly = 1;
      multiLine = 1;
      text => switch (<-.Dparams.projection, <-.message1, <-.message2);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist projection_list {
      parent => <-.proj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.message.y + <-.message.height) + 5);
      width => parent.virtualWidth;
      strings => <-.Dparams.labels;
    };
    UIbutton add<NEx=165.,NEy=506.> {
      parent => <-.proj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 5;
      y => ((<-.projection_list.y + <-.projection_list.height) + 5);
      visible => (<-.Dparams.nprojections < 10);
      width = 120;
      label = "New Projection";
    };
    GMOD.parse_v parse_v#2 {
      v_commands = "add.active = 0; accept.active = 1;"
	+ " UImultiList.active = 1;";
      trigger => <-.add.do;
      on_inst = 0;
      relative => <-;
    };
    UIbutton accept<NEx=385.,NEy=506.> {
      parent => <-.proj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => ((parent.virtualWidth / 2) + 5);
      y => add.y;
      visible => (<-.Dparams.nprojections < 10);
      active = 0;
      width = 120;
      label = "Accept Projection";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle all_atoms<NEx=627.,NEy=451.> {
      parent => <-.proj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => ((<-.add.y + <-.add.height) + 5);
      set => <-.Dparams.project_all;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle expand_atoms<NEx=627.,NEy=451.> {
      parent => <-.proj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => ((<-.all_atoms.y + <-.all_atoms.height) + 5);
      visible => (<-.Dparams.projection == 2);
      set => <-.Dparams.expand_atoms;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle expand_states<NEx=627.,NEy=451.> {
      parent => <-.proj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => ((<-.expand_atoms.y + <-.expand_atoms.height) + 5);
      visible => (<-.Dparams.projection == 2);
      set => <-.Dparams.expand_states;
    };
    GMOD.parse_v parse_v#3 {
      v_commands = "accept.active = 0; UImultiList.active = 0;"
	+ " add.active = 1;";
      trigger => <-.accept.do;
      on_inst = 0;
      relative => <-;
    };
    //int selections<NEportLevels=1>[];
    UImultiList UImultiList<NEx=253.,NEy=583.> {
      parent => <-.proj_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.virtualWidth;
      y => ((<-.expand_states.y + <-.expand_states.height) + 5);
      height = 150;
      visible => (<-.Dparams.projection == 2);
      active = 0;
      &selectedItems => <-.Dparams.orbitals;
    };
    string points_label<NEportLevels=1,NEx=22.,NEy=253.> = "n points";
    string bn_label<NEportLevels=1,NEx=11.,NEy=286.> = "Start Band";
    string bx_label<NEportLevels=1,NEx=22.,NEy=319.> = "End Band";
    string poly_label<NEportLevels=1,NEx=682.,NEy=286.> = "n polynomials";
    user_job {
      y => ((npolynomials.y + npolynomials.height) + 10);
      set => <-.Dparams.user_calc;
      };
    FileB {
      UIpanel {
	visible => <-.<-.Dparams.user_calc;
      };
      filename => <-.Dparams.user_dir;
    };
  };
  CCP3.CRYSTAL.Base.calcBase DOSbands {
    UIcmd {
      label = "Bands and DOS";
      active => <-.active_menus;
    };
    link version<NEportLevels={2,1},NEx=803.,NEy=110.>;
    link model_type<NEportLevels=1,NEx=814.,NEy=55.>;
    link wvfn<NEportLevels={2,1},NEx=627.,NEy=143.>;
    link job_data<NEportLevels={2,1},NEx=627.,NEy=143.>;
    link nselections;
    CCP3.CRYSTAL.Modules.Density_params &Density_params<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.NewK_params &NewK_params<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.BandParams Bparams<NEportLevels={0,1}> {
      path = "?";
      npoints = 50;
      bmin = 0;
      bmax = 0;
      user_calc = 0;
      user_dir = "";
    };
    CCP3.CRYSTAL.Modules.DOS_params Dparams {
      projection = 0;
      nprojections = 0;
      in_viewer = 1;
      npoints = 200;
      bmin = 0;
      bmax = 0;
      npoly = 6;
      energy_limits = 0;
      project_all = 0;
      expand_atoms = 0;
      expand_states = 0;
      user_calc = 0;
      user_dir = "";
    };
    dosBandUI dosBandUI<NEx=495.,NEy=374.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => <-.visible;
      model_type => <-.model_type;
      wvfn => <-.wvfn;
      parse_v {
	v_commands = "Bparams.path = dosBandUI.curpath;";
	relative => <-.<-;
      };
      Bparams => <-.Bparams;
      Dparams => <-.Dparams;
      UImultiList {
	strings => <-.<-.bands_and_dos.labels;
      };
    };
    instancer {
      Group => dosBandUI;
    };
    CCP3.CRYSTAL.Modules.bands_and_dos bands_and_dos<NEx=649.,NEy=484.> {
      make => <-.visible;
      init => <-.Dparams.projection;
      atoms => <-.Dparams.expand_atoms;
      states => <-.Dparams.expand_states;
      nselections => <-.nselections;
      add => <-.dosBandUI.accept.do;
      run => <-.dosBandUI.UItemplateDialog.ok;
      cancel => <-.dosBandUI.UItemplateDialog.cancel;
      Bdata => <-.Bparams;
      Ddata => <-.Dparams;
      DMparams => <-.Density_params;
      NewK => <-.NewK_params;
      version => <-.version;
      job_data => <-.job_data;
    };
  };
  props_baseUI mullikenUI {
    UItemplateDialog<NEx=231.,NEy=165.> {
      width = 350;
      height = 200;
      title = "CRYSTAL Mulliken Analysis";
    };
    link neighbours<NEportLevels=1,NEx=440.,NEy=77.>;
    link rotate<NEportLevels=1,NEx=702.,NEy=189.>;
    CCP3.CRYSTAL.Modules.user_job_data &data<NEportLevels={2,1}>;
    string label<NEportLevels=1,NEx=484.,NEy=187.> = "N Neighbours";
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein UIfieldTypein<NEx=275.,
							     NEy=341.> {
      UIparent => <-.UIpanel;
      flabel => <-.label;
      fmin = 1;
      fval => <-.neighbours;
      color => <-.prefs.colour;
      y = 0;
      fontAttributes => <-.prefs.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle DLVtoggle<NEx=648.,NEy=342.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((UIfieldTypein.y + UIfieldTypein.height) + 10);
      width => parent.clientWidth;
      label = "Rotate orbitals from selected atoms";
      set => <-.rotate;
      visible => (<-.version > 2);
    };
    user_job {
      y => ((DLVtoggle.y + DLVtoggle.height) + 10);
      set => <-.data.user_calc;
    };
    FileB {
      UIpanel {
	visible => <-.<-.data.user_calc;
      };
      filename => <-.data.user_dir;
    };
  };
  CCP3.CRYSTAL.Base.calcBase Mulliken {
    UIcmd {
      label = "Mulliken Analysis";
      active => <-.active_menus;
    };
    int neighbours = 3;
    boolean rotate = 0;
    link job_data<NEportLevels={2,1},NEx=627.,NEy=143.>;
    link version<NEportLevels={2,1},NEx=803.,NEy=110.>;
    CCP3.CRYSTAL.Modules.user_job_data data<NEportLevels={1,1}> {
      user_calc = 0;
      user_dir = "";
    };
    CCP3.CRYSTAL.Modules.Density_params &Density_params<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.NewK_params &NewK_params<NEportLevels={2,1}>;
    mullikenUI mullikenUI<NEx=495.,NEy=374.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      neighbours => <-.neighbours;
      visible => <-.visible;
      rotate => <-.rotate;
    };
    instancer {
      Group => mullikenUI;
    };
    CCP3.CRYSTAL.Modules.mulliken mulliken {
      run => <-.mullikenUI.UItemplateDialog.ok;
      data => <-.neighbours;
      rotate => <-.rotate;
      user_data => <-.data;
      DMparams => <-.Density_params;
      NewK => <-.NewK_params;
      version => <-.version;
      job_data => <-.job_data;
    };
  };
  props_baseUI wanSliceUI {
    UItemplateDialog<NEx=231.,NEy=165.> {
      width = 350;
      height = 520;
      okButton => (is_valid(<-.<-.data.grid));
      title = "CRYSTAL 2D Wannier functions";
    };
    CCP3.Core_Modules.Data.CRYSTAL_Wavefn_info &wvfn<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.wann3D_data &data<NEx=847.,NEy=22.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.Calculations.object_data &objects<NEx=495.,NEy=66.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Npoints<NEx=187.,NEy=209.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "n points";
      fmin = 2;
      fval => <-.data.npoints;
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist grids<NEx=660.,NEy=209.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.Npoints.y + <-.Npoints.height + 5);
      width => parent.clientWidth;
      visible => (array_size(strings) > 0);
      strings => <-.objects.planes;
      selectedItem => <-.data.grid;
    };
    UIbutton make_grid<NEx=847.,NEy=209.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.grids.y + <-.grids.height + 5);
      do => <-.objects.create_plane;
      label = "Create Grid";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle symmetry<NEx=605.,NEy=407.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.make_grid.y + <-.make_grid.height) + 5);
      width => <-.parent.clientWidth;
      label = "Symmetrise wannier functions";
      set => <-.data.symmetrize;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein min_band<NEx=209.,NEy=385.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Min Band";
      fval => <-.data.min_band;
      fmin => 1;
      fmax => <-.data.max_band;
      y => (<-.symmetry.y + <-.symmetry.height + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein max_band<NEx=209.,NEy=485.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Max Band";
      fval => <-.data.max_band;
      fmin => <-.data.min_band;
      fmax => wvfn.nbands;
      y => ((<-.min_band.y + <-.min_band.height) + 5);
    };
    UIbutton Valence {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.max_band.y + <-.max_band.height) + 5);
      do = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider stars<NEx=484.,NEy=572.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      x = 0;
      y => ((<-.Valence.y + <-.Valence.height) + 5);
      value => <-.data.gstars;
      min = 1;
      max = 9;
      title = "Stars of Lattice Vectors";
    };
    GMOD.parse_v set_valence {
      v_commands = "data.min_band = wvfn.vband_min;"
	+ " data.max_band = wvfn.vband_max;";
      trigger => <-.Valence.do;
      on_inst = 0;
      relative => <-;
    };
    GMOD.parse_v init_bands {
      v_commands = "data.min_band = 1; data.max_band = wvfn.nbands;"
	+ " data.grid = ;";
      trigger => <-.wvfn.nbands;
      on_inst = 1;
      relative => <-;
    };
    user_job {
      y => ((stars.y + stars.height) + 10);
      set => <-.data.user_calc;
    };
    FileB {
      UIpanel {
	visible => <-.<-.data.user_calc;
      };
      filename => <-.data.user_dir;
    };
  };
  CCP3.CRYSTAL.Base.calcBase wan_slice {
    UIcmd {
      label = "2D Wannier slice";
      active => <-.active_menus;
    };
    link version<NEportLevels={2,1},NEx=803.,NEy=110.>;
    link model_type<NEportLevels=1,NEx=572.,NEy=66.>;
    link job_data<NEportLevels={2,1},NEx=627.,NEy=143.>;
    link wvfn<NEportLevels={2,1}>;
    link objects<NEportLevels={2,1},NEx=407.,NEy=209.>;
    CCP3.CRYSTAL.Modules.Density_params &Density_params<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.NewK_params &newk<NEx=825.,NEy=176.,
      NEportLevels={0,1}>;
    CCP3.CRYSTAL.Modules.wann3D_data data<NEx=627.,NEy=198.,
      NEportLevels={0,1}> {
      npoints = 20;
      min_band = 0;
      max_band = 0;
      symmetrize = 0;
      gstars = 3;
      user_calc = 0;
      user_dir = "";
    };
    wanSliceUI wanSliceUI<NEx=495.,NEy=374.,instanced=0> {
      prefs => <-.prefs;
      data => <-.data;
      UIparent => <-.UIparent;
      visible => <-.visible;
      wvfn => <-.wvfn;
      objects => <-.objects;
    };
    instancer {
      Group => wanSliceUI;
    };
    CCP3.CRYSTAL.Modules.wannier2D wannier<NEx=726.,NEy=374.> {
      run => <-.wanSliceUI.UItemplateDialog.ok;
      data => <-.data;
      DMparams => <-.Density_params;
      NewK => <-.newk;
      version => <-.version;
      job_data => <-.job_data;
    };
  };
  props_baseUI wannierUI {
    UItemplateDialog<NEx=231.,NEy=165.> {
      width = 350;
      height = 560;
      okButton => (<-.data.gen_grid == 1) ||
	((<-.data.gen_grid == 0) && (is_valid(<-.data.grid)));
      title = "CRYSTAL 3D Wannier functions";
    };
    CCP3.Core_Modules.Data.CRYSTAL_Wavefn_info &wvfn<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.wann3D_data &data<NEx=847.,NEy=22.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.Calculations.object_data &objects<NEx=495.,NEy=66.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Npoints<NEx=187.,NEy=209.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "n points";
      fmin = 2;
      fval => <-.data.npoints;
      x = 0;
      y => (<-.use_points.y + <-.use_points.height + 5);
      panel {
	visible => (<-.<-.data.use_points == 1);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein spacing {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "grid spacing";
      fmin = 0.00001;
      fmax = 2.0;
      fval => <-.data.spacing;
      x = 0;
      y => (<-.use_points.y + <-.use_points.height + 5);
      panel {
	visible => (<-.<-.data.use_points == 0);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle use_points {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 5;
      y = 0;
      width => parent.clientWidth;
      label = "set number of points";
      set => <-.data.use_points;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle grid_gen {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.Npoints.y + <-.Npoints.height + 5);
      x = 5;
      width => parent.clientWidth;
      label = "Automatically generate grid";
      set => <-.data.gen_grid;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein region {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "wannier size";
      fmin = 0.1;
      fmax = 10.0;
      fval => <-.data.region;
      x = 0;
      y => (<-.grid_gen.y + <-.grid_gen.height + 5);
      panel {
	visible => (<-.<-.data.gen_grid == 1);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist grids<NEx=660.,NEy=209.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.grid_gen.y + <-.grid_gen.height + 5);
      width => parent.clientWidth;
      visible => ((<-.data.gen_grid == 0) && (array_size(strings) > 0));
      strings => <-.objects.volumes;
      selectedItem => <-.data.grid;
    };
    UIbutton make_grid<NEx=847.,NEy=209.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      visible => (<-.data.gen_grid == 0);
      y => (<-.grids.y + <-.grids.height + 5);
      do => <-.objects.create_volume;
      label = "Create Grid";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle symmetry<NEx=605.,NEy=407.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.make_grid.y + <-.make_grid.height) + 5);
      width => <-.parent.clientWidth;
      label = "Symmetrise wannier functions";
      set => <-.data.symmetrize;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein min_band<NEx=209.,NEy=385.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Min Band";
      fval => <-.data.min_band;
      fmin => 1;
      fmax => <-.data.max_band;
      y => (<-.symmetry.y + <-.symmetry.height + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein max_band<NEx=209.,NEy=485.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Max Band";
      fval => <-.data.max_band;
      fmin => <-.data.min_band;
      fmax => wvfn.nbands;
      y => ((<-.min_band.y + <-.min_band.height) + 5);
    };
    UIbutton Valence {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.max_band.y + <-.max_band.height) + 5);
      do = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider stars<NEx=484.,NEy=572.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      x = 0;
      y => ((<-.Valence.y + <-.Valence.height) + 5);
      value => <-.data.gstars;
      min = 1;
      max = 9;
      title = "Stars of Lattice Vectors";
    };
    GMOD.parse_v set_valence {
      v_commands = "data.min_band = wvfn.vband_min;"
	+ " data.max_band = wvfn.vband_max;";
      trigger => <-.Valence.do;
      on_inst = 0;
      relative => <-;
    };
    GMOD.parse_v init_bands {
      v_commands = "data.min_band = 1; data.max_band = wvfn.nbands;"
	+ " data.grid = ;";
      trigger => <-.wvfn.nbands;
      on_inst = 1;
      relative => <-;
    };
    user_job {
      y => ((<-.stars.y + <-.stars.height) + 10);
      set => <-.data.user_calc;
    };
    FileB {
      UIpanel {
	visible => <-.<-.data.user_calc;
      };
      filename => <-.data.user_dir;
    };
  };
  CCP3.CRYSTAL.Base.calcBase wannier3D {
    UIcmd {
      label = "3D Wannier function";
      active => <-.active_menus;
    };
    link version<NEportLevels={2,1},NEx=803.,NEy=110.>;
    link model_type<NEportLevels=1,NEx=572.,NEy=66.>;
    link job_data<NEportLevels={2,1},NEx=627.,NEy=143.>;
    link wvfn<NEportLevels={2,1}>;
    link objects<NEportLevels={2,1},NEx=407.,NEy=209.>;
    CCP3.CRYSTAL.Modules.Density_params &Density_params<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.NewK_params &newk<NEx=825.,NEy=176.,
      NEportLevels={0,1}>;
    CCP3.CRYSTAL.Modules.wann3D_data data<NEx=627.,NEy=198.,
      NEportLevels={0,1}> {
      npoints = 20;
      min_band = 0;
      max_band = 0;
      spacing = 0.2;
      region = 2.0;
      symmetrize = 0;
      gen_grid = 1;
      use_points = 1;
      gstars = 3;
      user_calc = 0;
      user_dir = "";
    };
    wannierUI wannierUI<NEx=495.,NEy=374.,instanced=0> {
      prefs => <-.prefs;
      data => <-.data;
      UIparent => <-.UIparent;
      visible => <-.visible;
      wvfn => <-.wvfn;
      objects => <-.objects;
    };
    instancer {
      Group => wannierUI;
    };
    CCP3.CRYSTAL.Modules.wannier3D wannier<NEx=726.,NEy=374.> {
      run => <-.wannierUI.UItemplateDialog.ok;
      data => <-.data;
      DMparams => <-.Density_params;
      NewK => <-.newk;
      version => <-.version;
      job_data => <-.job_data;
    };
  };
  props_baseUI brillouinUI {
    UItemplateDialog<NEx=231.,NEy=165.> {
      width = 350;
      height = 320;
      okButton = 1;
      title = "CRYSTAL Band 3D";
    };
    CCP3.Core_Modules.Data.CRYSTAL_Wavefn_info &wvfn<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.brillouin_data &data<NEx=847.,NEy=22.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein min_band<NEx=209.,NEy=385.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Min Band";
      fval => <-.data.min_band;
      fmin => 1;
      fmax => <-.data.max_band;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein max_band<NEx=209.,NEy=485.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Max Band";
      fval => <-.data.max_band;
      fmin => <-.data.min_band;
      fmax => wvfn.nbands;
      y => ((<-.min_band.y + <-.min_band.height) + 5);
    };
    UIbutton Valence {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.max_band.y + <-.max_band.height) + 5);
      do = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider shrink<NEx=484.,NEy=572.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      x = 0;
      y => ((<-.Valence.y + <-.Valence.height) + 5);
      value => <-.data.shrink;
      min = 2;
      max = 128;
      title = "K space shrinking factor";
    };
    GMOD.parse_v set_valence {
      v_commands = "data.min_band = wvfn.vband_min;"
	+ " data.max_band = wvfn.vband_max;";
      trigger => <-.Valence.do;
      on_inst = 0;
      relative => <-;
    };
    GMOD.parse_v init_bands {
      v_commands = "data.min_band = 1; data.max_band = wvfn.nbands;";
      trigger => <-.wvfn.nbands;
      on_inst = 1;
      relative => <-;
    };
    user_job {
      y => ((shrink.y + shrink.height) + 10);
      set => <-.data.user_calc;
    };
    FileB {
      UIpanel {
	visible => <-.<-.data.user_calc;
      };
      filename => <-.data.user_dir;
    };
  };
  CCP3.CRYSTAL.Base.calcBase brillouin3D {
    UIcmd {
      label = "E(k) 3D";
      active => <-.active_menus;
    };
    link version<NEportLevels={2,1},NEx=803.,NEy=110.>;
    link model_type<NEportLevels=1,NEx=572.,NEy=66.>;
    link job_data<NEportLevels={2,1},NEx=627.,NEy=143.>;
    link wvfn<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.Density_params &Density_params<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.NewK_params &newk<NEx=825.,NEy=176.,
      NEportLevels={0,1}>;
    CCP3.CRYSTAL.Modules.brillouin_data data<NEx=627.,NEy=198.,
      NEportLevels={0,1}> {
      min_band = 0;
      max_band = 0;
      shrink = 16;
      user_calc = 0;
      user_dir = "";
    };
    brillouinUI brillouinUI<NEx=495.,NEy=374.,instanced=0> {
      prefs => <-.prefs;
      data => <-.data;
      UIparent => <-.UIparent;
      visible => <-.visible;
      wvfn => <-.wvfn;
    };
    instancer {
      Group => brillouinUI;
    };
    CCP3.CRYSTAL.Modules.brillouin brillouin<NEx=726.,NEy=374.> {
      run => <-.brillouinUI.UItemplateDialog.ok;
      data => <-.data;
      DMparams => <-.Density_params;
      NewK => <-.newk;
      version => <-.version;
      job_data => <-.job_data;
    };
  };
  props_baseUI dlv3DUI {
    UItemplateDialog<NEx=231.,NEy=165.> {
      width = 350;
      height = 720;
      title = "CRYSTAL 3D Grids";
      okButton => ((<-.data.use_points == 0) || is_valid(<-.data.box));
    };
    link version;
    CCP3.Core_Modules.Data.CRYSTAL_Wavefn_info &wvfn<NEportLevels={2,1},
      NEx=286.,NEy=66.>;
    CCP3.CRYSTAL.Modules.dlv_3D_data &data<NEx=847.,NEy=22.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.Calculations.object_data &objects<NEx=495.,NEy=66.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Npoints<NEx=187.,NEy=209.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "n points";
      fmin = 2;
      fval => <-.data.npoints;
      x = 0;
      y => (<-.use_points.y + <-.use_points.height + 5);
      panel {
	visible => (<-.<-.data.use_points == 1);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein spacing {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "grid spacing";
      fmin = 0.00001;
      fmax = 2.0;
      fval => <-.data.spacing;
      x = 0;
      y => (<-.use_points.y + <-.use_points.height + 5);
      panel {
	visible => (<-.<-.data.use_points == 0);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle use_points {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 5;
      y = 0;
      width => parent.clientWidth;
      label = "set number of points";
      set => <-.data.use_points;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist grids<NEx=660.,NEy=209.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.Npoints.y + <-.Npoints.height + 5);
      width => parent.clientWidth;
      visible => (array_size(strings) > 0);
      //strings => <-.list_volumes.labels;
      strings => <-.objects.volumes;
      selectedItem => <-.data.box;
    };
    UIbutton make_grid<NEx=847.,NEy=209.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.grids.y + <-.grids.height + 5);
      do => <-.objects.create_volume;
      label = "Create Grid";
    };
    /*GMOD.parse_v parse_v<NEx=847.,NEy=275.> {
      v_commands = "objects.create_volume = 1;";
      trigger => <-.make_grid.do;
      on_inst = 0;
      relative => <-.;
      };*/
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle charge<NEx=429.,NEy=209.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.make_grid.y + <-.make_grid.height + 5);
      width => parent.clientWidth;
      set => <-.data.calc_charge;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle spin<NEx=429.,NEy=275.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.charge.y + <-.charge.height + 5);
      width => parent.clientWidth;
      visible => <-.wvfn.spin;
      set => <-.data.calc_spin;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle potential<NEx=429.,NEy=341.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.spin.y + <-.spin.height + 5);
      width => parent.clientWidth;
      set => <-.data.calc_pot;
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider multi_tolerance<NEx=484.,NEy=572.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      x = 0;
      y => ((<-.potential.y + <-.potential.height) + 5);
      visible => (<-.data.calc_pot);
      value => <-.data.pol_tol;
      min = 2;
      max = 6;
      title = "Multipole tolerance";
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider pot_tolerance<NEx=484.,NEy=572.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      x = 0;
      y => ((<-.multi_tolerance.y + <-.multi_tolerance.height) + 5);
      visible => (<-.data.calc_pot);
      value => <-.data.pot_tol;
      min = 2;
      max = 15;
      title = "Potential tolerance";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle efield<NEx=429.,NEy=407.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.pot_tolerance.y + <-.pot_tolerance.height + 5);
      width => parent.clientWidth;
      set => <-.data.calc_field;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle density<NEx=429.,NEy=473.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.efield.y + <-.efield.height + 5);
      width => parent.clientWidth;
      set => <-.data.calc_density;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein min_band<NEx=209.,NEy=385.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      panel {
	visible => <-.<-.data.calc_density;
      };
      flabel => "Min Band";
      fval => <-.data.min_band;
      fmin => 1;
      fmax => <-.data.max_band;
      y => (<-.density.y + <-.density.height + 5);
      x = 0;
      panel {
	width => <-.UIparent.clientWidth / 2;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein max_band<NEx=209.,NEy=485.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      panel {
	visible => <-.<-.data.calc_density;
      };
      flabel => "Max Band";
      fval => <-.data.max_band;
      fmin => <-.data.min_band;
      fmax => wvfn.nbands;
      y => <-.min_band.y;
      x => UIparent.clientWidth / 2;
      panel {
	width => <-.UIparent.clientWidth / 2;
      };
    };
    UIbutton Valence {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      visible => <-.data.calc_density;
      y => ((<-.max_band.y + <-.max_band.height) + 5);
      do = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle ldr<NEx=621.,NEy=342.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.max_band.y + <-.max_band.height + 5);
      width => parent.clientWidth;
      set => <-.data.calc_ldr;
      label = "TD-DFT Linear response density";
      visible => ((<-.version > 3) && (<-.wvfn.tddft_done == 1));
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle particle<NEx=621.,NEy=405.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.ldr.y + <-.ldr.height + 5);
      width => parent.clientWidth;
      set => <-.data.calc_particle;
      label = "TD-DFT Particle";
      visible => ((<-.version > 3) && (<-.wvfn.tddft_done == 1));
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle hole<NEx=621.,NEy=477.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.particle.y + <-.particle.height + 5);
      width => parent.clientWidth;
      set => <-.data.calc_hole;
      visible => ((<-.version > 3) && (<-.wvfn.tddft_done == 1));
      label = "TD-DFT Hole";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle hpoverlap<NEx=621.,NEy=537.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.hole.y + <-.hole.height + 5);
      width => parent.clientWidth;
      set => <-.data.calc_overlap;
      visible => ((<-.version > 3) && (<-.wvfn.tddft_done == 1));
      label = "TD-DFT Hole-Particle Overlap";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein minexcite<NEx=837.,NEy=297.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      panel {
	visible => (<-.<-.data.calc_ldr || <-.<-.data.calc_particle ||
		    <-.<-.data.calc_hole || <-.<-.data.calc_overlap);
      };
      flabel => "Min Excitation";
      fval => <-.data.min_excite;
      fmin => 1;
      //fmax => data.max_excite;
      y => ((<-.hpoverlap.y + <-.hpoverlap.height) + 5);
      x = 0;
      panel {
	width => <-.UIparent.clientWidth / 2;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein maxexcite<NEx=837.,NEy=360.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      panel {
	visible => (<-.<-.data.calc_ldr || <-.<-.data.calc_particle ||
		    <-.<-.data.calc_hole || <-.<-.data.calc_overlap);
      };
      flabel => "Max Excitation";
      fval => <-.data.max_excite;
      fmin => <-.data.min_excite;
      //fmax => wvfn.nbands;
      y => <-.minexcite.y;
      x => UIparent.clientWidth / 2;
      panel {
	width => <-.UIparent.clientWidth / 2;
      };
    };
    GMOD.parse_v set_valence {
      v_commands = "data.min_band = wvfn.vband_min;"
	+ " data.max_band = wvfn.vband_max;";
      trigger => <-.Valence.do;
      on_inst = 0;
      relative => <-;
    };
    GMOD.parse_v init_bands {
      v_commands = "data.min_band = 1; data.max_band = wvfn.nbands;"
	+ " data.calc_spin = 0; data.box = ;";
      trigger => <-.wvfn.nbands;
      on_inst = 1;
      relative => <-;
    };
    //CCP3.Core_Modules.Objects.list_volumes list_volumes<NEx=649.,NEy=121.>;
    user_job {
      y => ((Valence.y + Valence.height) + 10);
      set => <-.data.user_calc;
    };
    FileB {
      UIpanel {
	visible => <-.<-.data.user_calc;
      };
      filename => <-.data.user_dir;
    };
  };
  CCP3.CRYSTAL.Base.calcBase dlv3D {
    UIcmd {
      label = "3D Grid";
      active => <-.active_menus;
    };
    link version<NEportLevels={2,1},NEx=803.,NEy=110.>;
    link model_type<NEportLevels=1,NEx=572.,NEy=66.>;
    link job_data<NEportLevels={2,1},NEx=627.,NEy=143.>;
    link wvfn<NEportLevels={2,1}>;
    link objects<NEportLevels={2,1},NEx=407.,NEy=209.>;
    CCP3.CRYSTAL.Modules.Density_params &Density_params<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.NewK_params &newk<NEx=825.,NEy=176.,
      NEportLevels={0,1}>;
    CCP3.CRYSTAL.Modules.dlv_3D_data data<NEx=627.,NEy=198.,
      NEportLevels={0,1}> {
      npoints = 20;
      spacing = 0.2;
      calc_charge = 0;
      calc_spin = 0;
      calc_pot = 0;
      calc_field = 0;
      calc_density = 0;
      use_points = 1;
      box = ;
      pol_tol = 5;
      pot_tol = 5;
      min_band = 0;
      max_band = 0;
      calc_ldr = 0;
      calc_particle = 0;
      calc_hole = 0;
      calc_overlap = 0;
      min_excite = 1;
      max_excite = 1;
      user_calc = 0;
      user_dir = "";
    };
    dlv3DUI dlv3DUI<NEx=495.,NEy=374.,instanced=0> {
      prefs => <-.prefs;
      data => <-.data;
      UIparent => <-.UIparent;
      visible => <-.visible;
      wvfn => <-.wvfn;
      objects => <-.objects;
      version => <-.version;
    };
    instancer {
      Group => dlv3DUI;
    };
    CCP3.CRYSTAL.Modules.dlv3D dlv3D<NEx=726.,NEy=374.> {
      run => <-.dlv3DUI.UItemplateDialog.ok;
      data => <-.data;
      DMparams => <-.Density_params;
      NewK => <-.newk;
      version => <-.version;
      job_data => <-.job_data;
    };
  };
  props_baseUI blochUI {
    UItemplateDialog<NEx=231.,NEy=165.> {
      width = 350;
      height = 520;
      okButton => (is_valid(<-.<-.data.grid) &&
		   (array_size(<-.data.kpoints) > 0));
      title = "CRYSTAL Bloch wavefunction";
    };
    CCP3.Core_Modules.Data.CRYSTAL_Wavefn_info &wvfn<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.bloch_data &data<NEx=847.,NEy=22.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Npoints<NEx=187.,NEy=209.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "n points";
      fmin = 2;
      fval => <-.data.npoints;
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.Calculations.object_data &objects<NEx=286.,NEy=77.,
      NEportLevels={2,0}>;
    //CCP3.Core_Modules.Objects.list_volumes list_volumes<NEx=781.,NEy=132.>;
    CCP3.Core_Macros.UI.UIobjs.DLVlist grids<NEx=660.,NEy=209.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.Npoints.y + <-.Npoints.height + 5);
      width => parent.clientWidth;
      visible => (array_size(strings) > 0);
      //strings => <-.list_volumes.labels;
      strings => <-.objects.volumes;
      selectedItem => <-.data.grid;
    };
    UIbutton make_grid<NEx=847.,NEy=209.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.grids.y + <-.grids.height + 5);
      do => <-.objects.create_volume;
      label = "Create Grid";
    };
    /*GMOD.parse_v parse_v<NEx=847.,NEy=275.> {
      //v_commands = 
      trigger => <-.make_grid.do;
      on_inst = 0;
      //relative => ;
      };*/
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein min_band<NEx=209.,NEy=385.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Min Band";
      fval => <-.data.min_band;
      fmin => 1;
      fmax => <-.data.max_band;
      y => (<-.make_grid.y + <-.make_grid.height + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein max_band<NEx=209.,NEy=485.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Max Band";
      fval => <-.data.max_band;
      fmin => <-.data.min_band;
      fmax => wvfn.nbands;
      y => ((<-.min_band.y + <-.min_band.height) + 5);
    };
    UIbutton Valence {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.max_band.y + <-.max_band.height) + 5);
      do = 0;
    };
    GMOD.parse_v set_valence {
      v_commands = "data.min_band = wvfn.vband_min;"
	+ " data.max_band = wvfn.vband_max;";
      trigger => <-.Valence.do;
      on_inst = 0;
      relative => <-;
    };
    GMOD.parse_v init_bands {
      v_commands = "data.min_band = 1; data.max_band = wvfn.nbands;"
	+ "data.kpoints = ; data.grid = ;";
      trigger => <-.wvfn.nbands;
      on_inst = 1;
      relative => <-;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVmultiList kpoints {
      parent => <-.UIpanel;
      width => parent.clientWidth;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.Valence.y + <-.Valence.height) + 5);
      strings+nres => <-.wvfn.kpoint_labels;
      selectedItems => <-.data.kpoints;
    };
    user_job {
      y => ((kpoints.y + kpoints.height) + 10);
      set => <-.data.user_calc;
    };
    FileB {
      UIpanel {
	visible => <-.<-.data.user_calc;
      };
      filename => <-.data.user_dir;
    };
  };
  CCP3.CRYSTAL.Base.calcBase bloch {
    UIcmd {
      label = "Bloch Wavefunction";
      active => <-.active_menus;
    };
    link version<NEportLevels={2,1},NEx=803.,NEy=110.>;
    link model_type<NEportLevels=1,NEx=572.,NEy=66.>;
    link job_data<NEportLevels={2,1},NEx=627.,NEy=143.>;
    link wvfn<NEportLevels={2,1}>;
    link objects<NEx=770.,NEy=22.,NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.Density_params &Density_params<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.NewK_params &newk<NEx=825.,NEy=176.,
      NEportLevels={0,1}>;
    CCP3.CRYSTAL.Modules.bloch_data data<NEx=627.,NEy=198.,
      NEportLevels={0,1}> {
      npoints = 20;
      grid = ;
      min_band = 0;
      max_band = 0;
      user_calc = 0;
      user_dir = "";
    };
    blochUI blochUI<NEx=495.,NEy=374.,instanced=0> {
      prefs => <-.prefs;
      data => <-.data;
      UIparent => <-.UIparent;
      visible => <-.visible;
      wvfn => <-.wvfn;
      objects => <-.objects;
    };
    instancer {
      Group => blochUI;
    };
    CCP3.CRYSTAL.Modules.bloch bloch<NEx=726.,NEy=374.> {
      run => <-.blochUI.UItemplateDialog.ok;
      data => <-.data;
      DMparams => <-.Density_params;
      NewK => <-.newk;
      version => <-.version;
      job_data => <-.job_data;
    };
  };
  props_baseUI tddft3dUI {
    UItemplateDialog<NEx=187.,NEy=88.> {
      width = 350;
      height = 500;
      title = "CRYSTAL TD-DFT densities";
    };
    link version<NEportLevels=1,NEx=781.,NEy=22.>;
    CCP3.CRYSTAL.Modules.Grid3D_Data &params<NEx=506.,NEy=77.>;
    link model_type<NEportLevels={2,1},NEx=352.,NEy=77.>;
    string label<NEportLevels=1,NEx=682.,NEy=143.> = "n points";
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Npoints<NEx=187.,NEy=209.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => <-.label;
      fmin = 2;
      fval => <-.params.npoints;
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox UIradioBoxLabel<NEx=440.,NEy=209.> {
      parent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => <-.string;
      width => parent.clientWidth;
      x = 0;
      y => ((<-.excitation.y + <-.excitation.height) + 5);
      visible => (<-.model_type < 3);
      selectedItem => <-.params.non_periodic;
      UIpanel {
	visible => <-.visible;
      };
      UIlabel {
	label = "Range or Scale";
      };
    };
    string string<NEportLevels=1,NEx=517.,NEy=143.>[] = {
      "Specify Scale",
      "Specify Range"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield x_min<NEx=187.,NEy=275.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 100;
      y => x_label.y;
      width = 80;
      visible => (<-.model_type == 0);
      value => <-.params.x_min;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield x_max<NEx=385.,NEy=275.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 200;
      y => x_label.y;
      width = 80;
      visible => ((<-.UIradioBoxLabel.selectedItem == 1)
		  && (<-.model_type == 0));
      value => <-.params.x_max;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield y_min<NEx=187.,NEy=341.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 100;
      y => y_label.y;
      width = 80;
      visible => ((<-.model_type >= 0) && (<-.model_type < 2));
      value => <-.params.y_min;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield y_max<NEx=385.,NEy=341.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 200;
      y => y_label.y;
      width = 80;
      visible => ((<-.UIradioBoxLabel.selectedItem == 1)
		  && (<-.model_type < 2) && (<-.model_type >= 0));
      value => <-.params.y_max;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield z_min<NEx=187.,NEy=407.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 100;
      y => z_label.y;
      width = 80;
      visible => ((<-.model_type >= 0) && (<-.model_type < 3));
      value => <-.params.z_min;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffield z_max<NEx=385.,NEy=407.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 200;
      y => z_label.y;
      width = 80;
      visible => ((<-.UIradioBoxLabel.selectedItem == 1) &&
		  (<-.model_type < 3) && (<-.model_type >= 0));
      value => <-.params.z_max;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle UItoggle<NEx=605.,NEy=407.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.z_label.y + <-.z_label.height) + 5);
      width => <-.parent.clientWidth;
      label = "Conventional Cell";
      visible => ((<-.version > 0) && (<-.model_type == 3));
      set => <-.params.convcell;
    };
    UIlabel x_label<NEx=22.,NEy=275.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.UIradioBoxLabel.y + <-.UIradioBoxLabel.height) + 5);
      x = 0;
      width = 80;
      label = "x";
      visible => (<-.model_type == 0);
    };
    UIlabel y_label<NEx=22.,NEy=341.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.x_label.y + <-.x_label.height) + 5);
      x = 0;
      width = 80;
      label = "y";
      visible => ((<-.model_type >= 0) && (<-.model_type < 2));
    };
    UIlabel z_label<NEx=22.,NEy=418.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((<-.y_label.y + <-.y_label.height) + 5);
      x = 0;
      width = 80;
      label = "z";
      visible => ((<-.model_type >= 0) && (<-.model_type < 3));
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox calc_type<NEx=242.,NEy=506.> {
      parent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels = {
	"Linear Density Response",
	"Particle",
	"Hole",
	"Hole-Particle Overlap"
      };
      width => parent.clientWidth;
      x = 0;
      y => ((<-.Npoints.y + <-.Npoints.height) + 5);
      selectedItem => <-.params.tddft_prop;
      UIlabel {
	label = "TD-DFT property";
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein excitation<NEx=711.,NEy=243.> {
      UIparent => <-.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      width => UIparent.clientWidth;
      x = 0;
      y => ((<-.calc_type.y + <-.calc_type.height) + 5);
      fmin = 1;
      fval => <-.params.excitation;
      flabel => "Excitation number";
    };
    user_job {
      y => ((UItoggle.y + UItoggle.height) + 10);
      set => <-.params.user_calc;
    };
    FileB {
      UIpanel {
	visible => <-.<-.params.user_calc;
      };
      filename => <-.params.user_dir;
    };
  };
  CCP3.CRYSTAL.Base.calcBase tddft_3d {
    UIcmd {
      label = "TD-DFT Density";
      active => (<-.active_menus && <-.version > 3);
    };
    link version<NEportLevels={2,1},NEx=803.,NEy=110.>;
    link model_type<NEportLevels=1,NEx=700.,NEy=60.>;
    link job_data<NEportLevels={2,1},NEx=627.,NEy=143.>;
    CCP3.CRYSTAL.Modules.Density_params &Density_params<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.NewK_params &NewK_params<NEportLevels={2,1}>;
    CCP3.CRYSTAL.Modules.Grid3D_Data params<NEportLevels={0,1}> {
      calc_charge = 0;
      calc_potential = 0;
      tddft_prop = 0;
      excitation = 1;
      npoints = 20;
      non_periodic = 0;
      convcell = 0;
      pot_itol = 5;
      x_min = 1.0;
      x_max = 1.0;
      y_min = 1.0;
      y_max = 1.0;
      z_min = 1.0;
      z_max = 1.0;
      user_calc = 0;
      user_dir = "";
    };
    tddft3dUI tddft3dUI<NEx=495.,NEy=374.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => <-.visible;
      version => <-.version;
      model_type => <-.model_type;
      params => <-.params;
    };
    instancer {
      Group => tddft3dUI;
    };
    CCP3.CRYSTAL.Modules.TDDFT3D TDDFT3D {
      run => <-.tddft3dUI.UItemplateDialog.ok;
      data => <-.params;
      DMparams => <-.Density_params;
      NewK => <-.NewK_params;
      version => <-.version;
      job_data => <-.job_data;
    };
  };
  // Todo - ANBD?
  props_baseUI orbitalUI {
    // Todo
  };
  CCP3.CRYSTAL.Base.calcBase orbitals {
    UIcmd {
      label = "Orbitals";
      active => <-.active_menus;
    };
    // Todo - need wavefunction to be available
    link version<NEportLevels={2,1},NEx=803.,NEy=110.>;
    link model_type<NEportLevels=1,NEx=814.,NEy=55.>;
    link job_data<NEportLevels={2,1},NEx=627.,NEy=143.>;
    orbitalUI orbitalUI<NEx=495.,NEy=374.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => <-.visible;
    };
    instancer {
      Group => orbitalUI;
    };
    // Todo
  };
  CCP3.CRYSTAL.SCF.baseUI optionsUI {
    CCP3shell<NEx=297.,NEy=77.> {
      title = "Property Calculation Options";
      UIshell {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
	height = 600;
      };
    };
    UIframe NEWK_frame<NEx=99.,NEy=143.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height = 300;
      x = 0;
      y = 0;
    };
    UIframe PBAN_frame<NEx=286.,NEy=209.> {
      parent => <-.Projections;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      x = 0;
      y => ((UIradioBoxLabel.y + UIradioBoxLabel.height) + 5);
      visible => (<-.Density_params.projection == 2);
    };
    UIframe PDIDE_frame<NEx=671.,NEy=209.> {
      parent => <-.Projections;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      x = 0;
      y => ((UIradioBoxLabel.y + UIradioBoxLabel.height) + 5);
      visible => (<-.Density_params.projection == 3);
    };
    UIframe Projections<NEx=297.,NEy=143.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => ((NEWK_frame.y + NEWK_frame.height) + 4);
      width => parent.clientWidth;
      height => (parent.clientHeight - y);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox UIradioBoxLabel<NEx=99.,NEy=209.> {
      parent => <-.Projections;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => <-.string;
      width => parent.clientWidth;
      x = 0;
      y = 0;
      selectedItem => <-.Density_params.projection;
      title => "Density Matrix Projection";
    };
    string string<NEportLevels=1,NEx=539.,NEy=77.>[] = {
      "Use SCF output",
      "Atomic Superposition",
      "Project over Bands",
      "Project over Energy range"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle Run_NEWK<NEx=99.,NEy=264.> {
      parent => <-.NEWK_frame;
      set => NewK_params.calc;
      x = 0;
      y = 0;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle New_Shrink<NEx=99.,NEy=319.> {
      parent => <-.NEWK_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      visible => NewK_params.calc;
      y => ((Run_NEWK.y + Run_NEWK.height) + 5);
      width => parent.clientWidth;
      label = "New K point net";
      set => NewK_params.new_shrink;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle Asym_Shrink<NEx=99.,NEy=374.> {
      parent => <-.NEWK_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      visible => (NewK_params.calc & NewK_params.new_shrink);
      y => ((New_Shrink.y + New_Shrink.height) + 5);
      width => parent.clientWidth;
      label = "Asymmetric K point net";
      set => NewK_params.asym_shrink;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein IS1<NEx=77.,NEy=429.> {
      UIparent => <-.NEWK_frame;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "IS1";
      fmin = 1;
      fval => NewK_params.is1;
      width => UIparent.clientWidth;
      y => ((Asym_Shrink.y + Asym_Shrink.height) + 5);
      panel {
	visible => <-.<-.NewK_params.new_shrink;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein IS2<NEx=275.,NEy=429.> {
      UIparent => <-.NEWK_frame;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "IS2";
      fmin = 1;
      fval => NewK_params.is2;
      width => UIparent.clientWidth;
      y => ((IS1.y + IS1.height) + 5);
      panel {
	visible => (<-.<-.NewK_params.new_shrink &&
		    <-.<-.NewK_params.asym_shrink);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein IS3<NEx=473.,NEy=429.> {
      UIparent => <-.NEWK_frame;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "IS3";
      fmin = 1;
      fval => NewK_params.is3;
      width => UIparent.clientWidth;
      y => ((IS2.y + IS2.height) + 5);
      panel {
	visible => (<-.<-.NewK_params.new_shrink &&
		    <-.<-.NewK_params.asym_shrink);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein ISP<NEx=77.,NEy=484.> {
      UIparent => <-.NEWK_frame;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      fmin = 1;
      fval => NewK_params.isp;
      width => UIparent.clientWidth;
      y => ((IS3.y + IS3.height) + 5);
      flabel => "Gilat Net";
      panel {
	visible => <-.<-.NewK_params.new_shrink;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein Emin<NEx=671.,NEy=275.> {
      UIparent => <-.PDIDE_frame;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Min Energy";
      fval => Density_params.emin;
      width => (UIparent.clientWidth - 125);
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein Emax<NEx=671.,NEy=341.> {
      UIparent => <-.PDIDE_frame;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Max Energy";
      fval => Density_params.emax;
      width => (UIparent.clientWidth - 125);
      x = 0;
      y => ((Emin.y + Emin.height) + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Bmin<NEx=286.,NEy=264.> {
      UIparent => <-.PBAN_frame;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Min band";
      fmin = 1;
      fmax => wvfn.nbands;
      fval => Density_params.bmin;
      width => (UIparent.clientWidth - 125);
      x = 0;
      y = 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Bmax<NEx=286.,NEy=319.> {
      UIparent => <-.PBAN_frame;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Max band";
      fmin = 1;
      fmax => wvfn.nbands;
      fval => Density_params.bmax;
      width => (UIparent.clientWidth - 125);
      x = 0;
      y => ((Bmin.y + Bmin.height) + 5);
    };
    UIbutton Valence<NEx=286.,NEy=374.> {
      parent => <-.PBAN_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      y => ((Bmax.y + Bmax.height) + 5);
    };
    UIbutton MinE_Plot {
      parent => <-.PDIDE_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 120;
      x => (parent.clientWidth - 125);
      y => Emin.y;
      label = "Import Selection";
    };
    GMOD.parse_v set_MinE {
      trigger => <-.MinE_Plot.do;
      on_inst = 0;
      relative => <-;
    };
    UIbutton MaxE_Plot {
      parent => <-.PDIDE_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 120;
      x => (parent.clientWidth - 125);
      y => Emax.y;
      label = "Import Selection";
    };
    GMOD.parse_v set_MaxE {
      trigger => <-.MaxE_Plot.do;
      on_inst = 0;
      relative => <-;
    };
    UIbutton MinB_Plot {
      parent => <-.PBAN_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 120;
      x => (parent.clientWidth - 125);
      y => Bmin.y;
      label = "Import Selection";
    };
    /* Todo */
    GMOD.parse_v set_MinB {
      trigger => <-.MinB_Plot.do;
      on_inst = 0;
      relative => <-;
    };
    UIbutton MaxB_Plot {
      parent => <-.PBAN_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 120;
      x => (parent.clientWidth - 125);
      y => Bmax.y;
      label = "Import Selection";
    };
    GMOD.parse_v set_MaxB {
      trigger => <-.MaxB_Plot.do;
      on_inst = 0;
      relative => <-;
    };
    UItoggle Fermi<NEx=275.,NEy=484.> {
      parent => <-.NEWK_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      visible => NewK_params.calc;
      width => parent.clientWidth;
      y => ((ISP.y + ISP.height) + 5);
      set => NewK_params.calc_fermi;
    };
    CCP3.Core_Modules.Data.CRYSTAL_Wavefn_info &wvfn<NEx=616.,NEy=22.,
      NEportLevels={2,0}>;
    CCP3.CRYSTAL.Modules.NewK_params &NewK_params<NEx=33.,NEy=33.,
      NEportLevels={2,0}>;
    CCP3.CRYSTAL.Modules.Density_params &Density_params<NEx=209.,NEy=33.,
      NEportLevels={2,0}>;
    GMOD.parse_v parse_v<NEx=473.,NEy=374.> {
      v_commands = "Density_params.bmin = wvfn.vband_min;"
	+ " Density_params.bmax = wvfn.vband_max;";
      trigger => <-.Valence.do;
      on_inst = 1;
      relative => <-;
    };
  };
  // almost CCP3.CRYSTAL.Base.calcBase
  macro Options {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels=1,NEx=198.,NEy=66.>;
    link active_menus<NEportLevels=1,NEx=385.,NEy=66.>;
    link UIparent<NEportLevels=1,NEx=770.,NEy=66.>;
    int visible<NEportLevels=1,NEx=396.,NEy=143.> = 0;
    link wvfn<NEportLevels={2,1}>;
    link NewK_params<NEportLevels={2,1}>;
    link Density_params<NEportLevels={2,1}>;
    UIoption UIcmd<NEx=198.,NEy=143.> {
      active => <-.active_menus;
      label = "Options";
      set => <-.visible;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    GMOD.instancer instancer<NEx=264.,NEy=275.> {
      Value => <-.visible;
      Group => optionsUI;
    };
    optionsUI optionsUI<instanced=0> {
      prefs => <-.prefs;
      visible => <-.visible;
      UIparent => <-.UIparent;
      wvfn => <-.wvfn;
      NewK_params => <-.NewK_params;
      Density_params => <-.Density_params;
    };
  };
};
