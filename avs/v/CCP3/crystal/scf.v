
flibrary SCF<NEeditable=1,
#ifndef DLV_NO_DLLS
	     dyn_libs="libCCP3crystal",
#endif // DLV_NO_DLLS
	     export_cxx=1,
	     build_dir="avs/src/express",
	     cxx_hdr_files="avs/src/express/crystal_base.hxx avs/src/crystal/scf_gen.hxx avs/src/crystal/neb_gen.hxx avs/src/crystal/neb_list_gen.hxx avs/src/crystal/neb_view_gen.hxx avs/src/crystal/neb_reset_gen.hxx avs/src/crystal/neb_hide_gen.hxx avs/src/crystal/neb_edit_gen.hxx avs/src/crystal/neb_create_gen.hxx avs/src/crystal/neb_resetim_gen.hxx avs/src/crystal/neb_viewimage_gen.hxx avs/src/crystal/neb_edimage_gen.hxx avs/src/crystal/neb_write91_gen.hxx avs/src/core/model/move_gen.hxx avs/src/core/model/can_gen.hxx avs/src/crystal/neb_editmod_gen.hxx avs/src/crystal/neb_accept_gen.hxx avs/src/express/ui/edit_model.hxx",
  out_hdr_file="crystal_scf.hxx",
  out_src_file="crystal_scf.cxx"> {
  group SCF_limits {
    int min_converge = 4;
    int max_converge = 10;
    int min_tola = 4;
    int max_tola = 10;
    int min_tolb = 10;
    int max_tolb = 16;
    int min_shrink = 1;
    int max_shrink = 48;
    int min_gilat = 2;
    int max_gilat = 96;
    int max_shift = 20;
    int min_cycles = 10;
    int max_cycles = 1000;
    int min_etol = 4;
    int max_etol = 8;
    float min_xtol = 0.001;
    float max_xtol = 1.0;
  };
  CCP3.CRYSTAL.Base.simple_baseUI baseUI {
    int visible<NEportLevels={2,1},NEx=495.,NEy=22.>;
    CCP3.Core_Macros.UI.UIobjs.DLVshell CCP3shell {
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      UIshell {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
    };
  };
  baseUI BasisUI {
    link version;
    CCP3shell<NEx=253.,NEy=187.> {
      title = "CRYSTAL SCF Basis Sets";
      UIshell {
	height = 400;
      };
    };
    string basis_env<NEportLevels=1,NEx=473.,NEy=187.> => getenv("DLV_BASIS");
    string default_dir<NEportLevels=1,NEx=660.,NEy=187.> =>
      (getenv("DLV_ROOT") + "/data/CRYSTAL");
    string basis_dir<NEportLevels=1,NEx=561.,NEy=253.> =>
      switch((is_valid(basis_env) + 1),default_dir,basis_env);
    int set_default<NEportLevels=1,NEx=671.,NEy=429.> = 0;
    string filename = "";
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox Default_Basis<NEx=209.,NEy=275.> {
      parent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => switch (<-.version+1, <-.bases, <-.bases, <-.bases, <-.bases,
			<-.bases14, <-.bases17, <-.bases17);
      selectedItem => Params.basis;
      width => parent.clientWidth;
    };
    string bases<NEportLevels=1,NEx=33.,NEy=165.>[] = {
      "STO-3G", "3-21G", "6-21G"
    };
    string bases14<NEportLevels=1,NEx=33.,NEy=165.>[] = {
      "STO-3G", "3-21G", "6-21G", "POB-DZVP", "POB-DZVPP", "POB-TZVP"
    };
    // Aren't there more than this in the code?
    string bases17<NEportLevels=1,NEx=33.,NEy=165.>[] = {
      "STO-3G", "3-21G", "6-21G", "POB-DZVP", "POB-DZVPP", "POB-TZVP",
      "STO-6G", "6-31G**", "DEF2-TZVP"
    };
    CCP3.CRYSTAL.Modules.BasisParams &Params<NEx=473.,NEy=99.,
                                             NEportLevels={2,0}>;
    GMOD.copy_on_change copy_on_change<NEx=561.,NEy=319.> {
      input => <-.basis_dir;
    };
    string filter => copy_on_change.output;
    CCP3.Core_Macros.UI.UIobjs.FilePanel FileB<NEx=407.,NEy=429.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}> => <-.filename;
      pattern => <-.filter + "/*";
      parent => <-.CCP3shell.UIpanel;
      title = "CRYSTAL basis set";
      x = 0;
      y => (<-.Default_Basis.y + <-.Default_Basis.height + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle DLVtoggle<NEx=407.,NEy=506.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      label = "Default basis for atom type";
      y => (<-.FileB.y + <-.FileB.height + 5);
      set => <-.set_default;
    };
    UIbutton inherit<NEx=407.,NEy=583.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "Inherit Basis";
      y => (<-.DLVtoggle.y + <-.DLVtoggle.height + 5);
      do = 0;
      visible = 0; // Todo
    };
  };
  baseUI HamiltonianUI {
    CCP3shell<NEx=253.,NEy=154.> {
      title = "CRYSTAL SCF Hamiltonian";
      UIshell {
	height = 750;
      };
    };
    CCP3.CRYSTAL.Modules.HamiltonianParams &Params<NEx=506.,NEy=55.,
                                                   NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox Hamiltonian<NEx=154.,NEy=209.> {
      parent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => <-.Hamiltonian_List;
      width => parent.clientWidth;
      selectedItem => Params.Htype;
    };
    string Hamiltonian_List<NEportLevels=1,NEx=33.,NEy=143.>[] = {
      "Hartree-Fock",
      "Density Functional"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox Options<NEx=154.,NEy=264.> {
      parent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => <-.option_list;
      width => parent.clientWidth;
      y => ((Hamiltonian.y + Hamiltonian.height) + 5);
      selectedItem => Params.Hoptions;
    };
    string HF_options<NEportLevels=1,NEx=517.,NEy=99.>[] = {
      "Restricted HF",
      "Restricted Open Shell HF",
      "Unrestricted HF"
    };
    string DFT_options<NEportLevels=1,NEx=517.,NEy=154.>[] = {
      "Unpolarised",
      "Spin Polarised"
    };
    string option_list<NEportLevels=1,NEx=517.,NEy=209.>[] =>
      switch ((Hamiltonian.selectedItem + 1), HF_options, DFT_options);
    CCP3.Core_Macros.UI.UIobjs.DLVscrolledWindow DFT_info<NEx=22.,NEy=308.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      y => ((Use_Spins.y + Use_Spins.height) + 5);
      height => (parent.clientHeight - y);
      visible => (Params.Htype == 1);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox Functional<NEx=143.,NEy=352.> {
      parent => <-.DFT_info;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => <-.Functionals;
      width => parent.clientWidth;
      y = 0;
      selectedItem => Params.functional;
    };
    string Functionals98[] = {
      "Correlation + Exchange"
    };
    string Functionals03[] = {
      "Correlation + Exchange",
      "B3LYP",
      "B3PW"
    };
    string Functionals09[] = {
      "Correlation + Exchange",
      "B3LYP",
      "B3PW",
      "PBE0",
      "SOGGAXC"
    };
    string Functionals14[] = {
      "Correlation + Exchange",
      "B3LYP",
      "B3PW",
      "PBE0",
      "PBEsol0",
      "B1WC",
      "WC1LYP",
      "B97H",
      "PBE0-13",
      "SVWN",
      "PBEXC",
      "PBESOLXC",
      "SOGGAXC",
      "HSE06",
      "HSEsol",
      "HISS",
      "RSHXLDA",
      "wB97",
      "wB97X",
      "LC-wBLYP",
      "LC-wPBE",
      "LC-wPBEsol"
    };
    string Functionals17[] = {
      "Correlation + Exchange",
      "B3LYP",
      "B3PW",
      "PBE0",
      "PBEsol0",
      "B1WC",
      "WC1LYP",
      "B97H",
      "PBE0-13",
      "SVWN",
      "PBEXC",
      "PBESOLXC",
      "SOGGAXC",
      "HSE06",
      "HSEsol",
      "HISS",
      "RSHXLDA",
      "wB97",
      "wB97X",
      "LC-wBLYP",
      "LC-wPBE",
      "LC-wPBEsol",
      "LC-BLYP",
      "CAM-B3LYP",
      "SC-BLYP",
      "M06L",
      "M05",
      "M06",
      "B2PLYP",
      "B2GPPLYP",
      "BLYP-D3",
      "PBE-D3",
      "B3LYP-D3",
      "PBE0-D3",
      "HSE06-D3",
      "HSEsol-D3",
      "M06-D3"
    };
    string Functionals<NEportLevels=1,NEx=517.,NEy=297.>[] =>
      switch ((version+1), Functionals98, Functionals03, Functionals03,
	      Functionals09, Functionals14, Functionals17, Functionals17);
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox Correlation<NEx=143.,NEy=396.> {
      parent => <-.DFT_info;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => <-.corr_list;
      width => (parent.clientWidth / 2);
      y => ((Functional.y + Functional.height) + 5);
      visible => (Params.functional == 0);
      selectedItem => Params.correlation;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox Exchange<NEx=143.,NEy=451.> {
      parent => <-.DFT_info;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => <-.exchange_list;
      width => (parent.clientWidth / 2);
      x => (parent.clientWidth / 2);
      y => Correlation.y;
      visible => (Params.functional == 0);
      selectedItem => Params.exchange;
    };
    string corr_list03<NEportLevels=1,NEx=517.,NEy=385.>[] = {
      "None","PZ","PWLSD","VWN","VBH","P86","PWGGA","LYP","PBE"
    };
    string corr_list09<NEportLevels=1,NEx=517.,NEy=385.>[] = {
      "None","PZ","PWLSD","VWN","VBH","P86","PWGGA","LYP","PBE","PBESOL","WL"
    };
    string corr_list[] => switch ((version+1), corr_list03,
				  corr_list03, corr_list03,
				  corr_list09, corr_list09, corr_list09,
				  corr_list09);
    string exchange_list03<NEportLevels=1,NEx=517.,NEy=451.>[] = {
      "Hartree-Fock","LDA","VBH","BECKE","PWGGA","PBE"
    };
    string exchange_list06<NEportLevels=1>[] = {
      "Hartree-Fock","LDA","VBH","BECKE","PWGGA","PBE","WCGGA"
    };
    string exchange_list09<NEportLevels=1>[] = {
      "Hartree-Fock","LDA","VBH","BECKE","PWGGA","PBE","WCGGA","PBESOL","SOGGA"
    };
    string exchange_list14<NEportLevels=1>[] = {
      "Hartree-Fock","LDA","VBH","BECKE","PWGGA","PBE","WCGGA","PBESOL","SOGGA",
      "mPW91"
    };
    string exchange_list[] => switch ((version+1), exchange_list03,
				      exchange_list03, exchange_list06,
				      exchange_list09, exchange_list14,
				      exchange_list14, exchange_list14);
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle Hybrid<NEx=11.,NEy=495.> {
      parent => <-.DFT_info;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((Correlation.y + Correlation.height) + 5);
      visible => ((Params.functional == 0) & (Params.exchange > 0));
      set => Params.hybrid;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle Use_Spins<NEx=330.,NEy=264.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((Options.y + Options.height) + 5);
      visible => ((Params.Hoptions != 0) & (version > 0));
      set => Params.use_spins;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield Mixing<NEx=198.,NEy=495.> {
      parent => <-.DFT_info;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => Hybrid.y;
      x => (parent.clientWidth / 2);
      visible => ((Params.functional == 0) & (Params.hybrid == 1));
      value => Params.mixing;
      min = 0;
      max = 100;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox DFT_type {
      parent => <-.DFT_info;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => {"Numerical Grids", "Fitted Grids"};
      width => parent.clientWidth;
      y => ((Hybrid.y + Hybrid.height) + 5);
      visible => (<-.version < 2);
      selectedItem => Params.dft_grids;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox Auxiliary_Basis<NEx=330.,NEy=396.> {
      parent => <-.DFT_info;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => switch(Params.dft_grids+1, <-.grids,<-.bases);
      width => parent.clientWidth;
      title => switch(Params.dft_grids+1, "Numerical Grid", "Auxiliary Basis");
      y => switch ((<-.version+1),
		   ((Hybrid.y + Hybrid.height) + 5),
		   ((DFT_type.y + DFT_type.height) + 5),
		   ((DFT_type.y + DFT_type.height) + 5),
		   ((DFT_type.y + DFT_type.height) + 5),
		   ((DFT_type.y + DFT_type.height) + 5),
		   ((DFT_type.y + DFT_type.height) + 5),
		   ((DFT_type.y + DFT_type.height) + 5));
      selectedItem => Params.aux_basis;
    };
    string bases<NEportLevels=1,NEx=517.,NEy=253.>[] = {
      "4 S type", "8 S type","12 S type"
    };
    string grids03[] = {
      "Default", "Large", "Extra Large"
    };
    string grids09[] = {
      "Default", "Large", "Extra Large", "XXL"
    };
    string grids<NEportLevels={1,1},NEx=517.,NEy=506.>[] =>
      switch(((version>2)+1), grids03, grids09);
    link version<NEx=660.,NEy=55.>;
  };
  baseUI TolerancesUI {
    link version;
    CCP3shell<NEx=253.,NEy=187.> {
      title = "CRYSTAL SCF Tolerances";
      UIshell {
	width = 300;
	height = 540;
      };
    };
    CCP3.CRYSTAL.Modules.TolParams &Params<NEx=495.,NEy=99.,
                                           NEportLevels={2,0}>;
    UIlabel Integrals<NEx=165.,NEy=286.> {
      parent => <-.Int_frame;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y = 0;
      width => parent.clientWidth;
      label = "Integral Tolerances";
    };
    UIframe Int_frame<NEx=165.,NEy=242.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      y = 0;
      height => ((<-.ITOL5.y + <-.ITOL5.height) + 5);
    };
    UIframe SCF_frame<NEx=385.,NEy=242.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => ((Density.y + Density.height) + 5);
      y => <-.Int_frame.height + 5;
    };
    string scf_c98[] = {
      "Energy or Eigenvalues"
    };
    string scf_c03[] = {
      "Energy or Eigenvalues",
      "Energy",
      "Density Matrix"
    };
    string scf_c06[] = {
      "Energy",
      "Density Matrix"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox SCFconv<NEx=385.,NEy=297.> {
      parent => <-.SCF_frame;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => switch ((version + 1), <-.scf_c98, <-.scf_c03,
			<-.scf_c06, <-.scf_c06, <-.scf_c06, <-.scf_c06,
			<-.scf_c06);
      width => parent.clientWidth;
      selectedItem => Params.method;
      y = 0;
      UIlabel {
	label = "SCF Convergence";
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Eigenvalues<NEx=385.,NEy=363.> {
      UIparent => <-.SCF_frame;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Eigenvalues";
      fval => Params.eigenval;
      y => ((SCFconv.y + SCFconv.height) + 5);
      panel {
	visible => (<-.<-.version < 2) && (<-.<-.Params.method == 0);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Energy<NEx=385.,NEy=418.> {
      UIparent => <-.SCF_frame;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Energy";
      fval => Params.energy;
      y => ((Eigenvalues.y + Eigenvalues.height) + 5);
      panel {
	visible => (((<-.<-.version == 1) && (<-.<-.Params.method < 2)) ||
		    (<-.<-.Params.method == 0));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Density {
      UIparent => <-.SCF_frame;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Density";
      fval => Params.deltap;
      y => ((Energy.y + Energy.height) + 5);
      panel {
	visible => (((<-.<-.version == 1) && (<-.<-.Params.method == 2)) ||
		    ((<-.<-.version > 1) && (<-.<-.Params.method == 1)));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein ITOL1<NEx=165.,NEy=330.> {
      UIparent => <-.Int_frame;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Itol1";
      fval => Params.itol1;
      y => ((Integrals.y + Integrals.height) + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein ITOL2<NEx=165.,NEy=374.> {
      UIparent => <-.Int_frame;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Itol2";
      fval => Params.itol2;
      y => ((ITOL1.y + ITOL1.height) + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein ITOL3<NEx=165.,NEy=418.> {
      UIparent => <-.Int_frame;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Itol3";
      fval => Params.itol3;
      y => ((ITOL2.y + ITOL2.height) + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein ITOL4<NEx=165.,NEy=462.> {
      UIparent => <-.Int_frame;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Itol4";
      fval => Params.itol4;
      y => ((ITOL3.y + ITOL3.height) + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein ITOL5<NEx=165.,NEy=506.> {
      UIparent => <-.Int_frame;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Itol5";
      fval => Params.itol5;
      y => ((ITOL4.y + ITOL4.height) + 5);
    };
  };
  baseUI K_PointsUI {
    CCP3shell<NEx=253.,NEy=187.> {
      title = "CRYSTAL SCF K Points";
      UIshell {
	width = 300;
      };
    };
    CCP3.CRYSTAL.Modules.KPointParams &Params<NEx=495.,NEy=99.,
                                              NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle Anisotropic_Net<NEx=242.,NEy=253.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y = 0;
      set => Params.asym;
      width => parent.clientWidth;
    };
    UIlabel shrink<NEx=187.,NEy=308.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => labels[<-.Params.asym];
      width => parent.clientWidth;
      y => ((Anisotropic_Net.y + Anisotropic_Net.height) + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield is1<NEx=187.,NEy=363.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => ((shrink.y + shrink.height) + 5);
      value => Params.is1;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield is2<NEx=187.,NEy=418.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => (parent.clientWidth / 3);
      y => is1.y;
      visible => Params.asym;
      value => Params.is2;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield is3<NEx=187.,NEy=473.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => (2 * parent.clientWidth / 3);
      y => is1.y;
      visible => Params.asym;
      value => Params.is3;
    };
    UIlabel Gilat_Net<NEx=407.,NEy=308.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      y => ((is1.y + is1.height) + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifield isp<NEx=407.,NEy=363.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => ((Gilat_Net.y + Gilat_Net.height) + 5);
      value => Params.isp;
    };
    string labels<NEportLevels=1,NEx=517.,NEy=231.>[] = {
      "Isotropic K Point Net","Anisotropic K Point Net"
    };
  };
  baseUI ConvergenceUI {
    CCP3shell<NEx=253.,NEy=187.> {
      title = "CRYSTAL SCF Convergence";
      UIshell {
	width = 300;
	height = 680;
      };
    };
    CCP3.CRYSTAL.Modules.ConvergeParams &Params<NEx=495.,NEy=99.,
                                                NEportLevels={2,0}>;
    UIframe method<NEx=110.,NEy=242.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => ((Mixing.y + Mixing.height) + 5);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox Convergence_Method<NEx=99.,NEy=319.> {
      parent => <-.method;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => <-.method_list;
      width => parent.clientWidth;
      selectedItem => Params.method;
    };
    string methods_v4<NEportLevels=1,NEx=22.,NEy=88.>[] = {
      "Linear Mixing"
    };
    string methods_v5<NEportLevels=1,NEx=22.,NEy=132.>[] = {
      "Linear Mixing",
      "Anderson",
    };
    link version<NEportLevels=1,NEx=671.,NEy=99.>;
    int show_spin;
    string method_list<NEportLevels=1,NEx=22.,NEy=165.>[] =>
      switch ((version + 1), methods_v4, methods_v5, methods_v5, methods_v5,
	      methods_v5, methods_v5, methods_v5);
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Mixing<NEx=99.,NEy=374.> {
      UIparent => <-.method;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((Convergence_Method.y + Convergence_Method.height) + 5);
      width => UIparent.clientWidth;
      flabel => "Mixing";
      fmin => 1;
      fmax => 99;
      fval => Params.mixing;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein cycles<NEx=330.,NEy=319.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((Mixing.y + Mixing.height) + 10);
      width => UIparent.clientWidth;
      flabel => "Max SCF cycles";
      fval => Params.maxcycles;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle levshift<NEx=330.,NEy=374.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((cycles.y + cycles.height) + 5);
      width => parent.clientWidth;
      label = "Use Level Shifting";
      set => Params.use_levshift;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein shift_value<NEx=330.,NEy=429.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      width => UIparent.clientWidth;
      y => ((levshift.y + levshift.height) + 5);
      flabel => "Level Shift (in 0.1H)";
      fmin => 0;
      fval => Params.levshift;
      panel {
	visible => Params.use_levshift;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle lock_shift<NEx=330.,NEy=484.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((shift_value.y + shift_value.height) + 5);
      width => parent.clientWidth;
      label = "Lock level shift";
      visible => Params.use_levshift;
      set => Params.lock_levshift;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle smear<NEx=550.,NEy=319.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((lock_shift.y + lock_shift.height) + 5);
      width => parent.clientWidth;
      label = "Fermi Smearing";
      set => Params.use_smearing;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein smear_temp<NEx=550.,NEy=374.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((smear.y + smear.height) + 5);
      width => UIparent.clientWidth;
      flabel => "Fermi Temperature (H)";
      fmin => 0;
      fval => Params.fermi_smear;
      panel {
	visible => Params.use_smearing;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle spinlock<NEx=550.,NEy=429.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((smear_temp.y + smear_temp.height) + 5);
      width => parent.clientWidth;
      label = "Spin Locking";
      visible => <-.show_spin;
      set => Params.spinlock;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein spin<NEx=550.,NEy=473.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((spinlock.y + spinlock.height) + 5);
      width => UIparent.clientWidth;
      flabel => "Lock Spin to";
      fval => Params.spin;
      panel {
	visible => (<-.<-.show_spin & <-.<-.Params.spinlock);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein spin_cycles<NEx=550.,NEy=517.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((spin.y + spin.height) + 5);
      width => UIparent.clientWidth;
      flabel => "N Cycles to lock spin";
      fmin => 0;
      fval => Params.spin_cycles;
      panel {
	visible => (<-.<-.show_spin & <-.<-.Params.spinlock);
      };
    };
  };
  baseUI OptimizeUI {
    CCP3shell<NEx=253.,NEy=187.> {
      title = "CRYSTAL SCF Optimise";
      UIshell {
	height = 500;
	width = 500;	       
      };
    };
    CCP3.CRYSTAL.Modules.OptParams &Params<NEx=495.,NEy=99.,
                                           NEportLevels={2,0}>;
    link version<NEportLevels={2,1},NEx=770.,NEy=121.>;
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox Optimiser {
      parent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => {"Standard", "DL-FIND"};
      width => parent.clientWidth;
      selectedItem => Params.optimiser;
      UIpanel.visible => (<-.<-.version > 2);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein etol<NEx=165.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((gtol.y + gtol.height) + 5);
      width => UIparent.clientWidth;
      flabel => "Energy Tolerance";
      fval => Params.etol;
      panel.visible => <-.<-.Params.optimiser == 0;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein gtol<NEx=165.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((Optimiser.y + Optimiser.height) + 5);
      width => UIparent.clientWidth;
      flabel => "Gradient Tolerance";
      fval => Params.gtol;
      field {
	decimalPoints = 5;
      };									
    };	     
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein xtol<NEx=165.,NEy=363.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((etol.y + etol.height) + 5);
      width => UIparent.clientWidth;
      flabel => "Step Tolerance";
      fval => Params.xtol;
      panel.visible => <-.<-.Params.optimiser == 0;
      field {
	decimalPoints = 5;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox opt_type<NEx=165.,NEy=253.> {
      parent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      title = "Type of Optimisation";
      labels => {"Optimise Atoms", "Optimise Cell", "Optimise Atoms+Cell"};
      y => ((xtol.y + xtol.height) + 5);
      width => parent.clientWidth;
      selectedItem => Params.opttype;
      UIpanel.visible => ((<-.<-.version > 1) && (<-.<-.Params.optimiser == 0));	       
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox dlf_type<NEx=165.,NEy=253.> {
      parent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => {"Stochastic Search", "Genetic Algorithm"};
      title = "Optimisation Method";
      y => ((Optimiser.y + Optimiser.height) + 5);
      width => parent.clientWidth/2;
      selectedItem => Params.dlf_type;
      UIpanel {
	  visible => ((version > 1) && (Params.optimiser == 1));
      };
    };

    CCP3.Core_Macros.UI.UIobjs.DLVradioBox ssopt_type<NEx=165.,NEy=253.> {
      parent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      title = "Stochastic Search Method";
      labels => {"Uniform", "Force-Direction Bias", "Force Bias"};
      y => ((Optimiser.y + Optimiser.height) + 5);
      x => parent.clientWidth/2;
      width => parent.clientWidth/2;
      selectedItem => Params.dlf_sstype;
      UIpanel {
	  visible => (version > 1) && (Params.optimiser == 1) && 
	      (Params.dlf_type==0);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein dlf_pop<NEx=165.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((ssopt_type.y + ssopt_type.height) + 5);
      width => UIparent.clientWidth/2;
      flabel => "Population";
      fval => Params.dlf_pop;
      field {
	  x=> (<-.panel.width/3*2);
	  width => (<-.panel.width/3);
      };
      label {
	  width => (<-.panel.width/3*2);
      };
      panel {
	  width => UIparent.clientWidth/2;
	  visible => ((version > 1) && (Params.optimiser == 1));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein dlf_initpop<NEx=165.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((ssopt_type.y + ssopt_type.height) + 5);
      x=> UIparent.clientWidth/2;
      flabel => "Initial Population";
      fval => Params.dlf_initpop;
      field {
	  x=> (<-.panel.width/3*2);
	  width => (<-.panel.width/3);
      };
      label {
	  width => (<-.panel.width/3*2);
      };
      panel {
	  width => UIparent.clientWidth/2;
	  visible => ((version > 1) && (Params.optimiser == 1) &&
	      (Params.dlf_type == 1));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein dlf_radius<NEx=165.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((dlf_pop.y + dlf_pop.height) + 5);
      width => UIparent.clientWidth/3;
      flabel => "Radius";
      fval => Params.dlf_radius;
      field {
	  x=> (<-.panel.width/2);
	  width => (<-.panel.width/2);
      };
      label {
	  width => (<-.panel.width/2);
      };
      panel {
	  width => UIparent.clientWidth/3;
	  visible => ((version > 1) && (Params.optimiser == 1));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein dlf_contractradius<NEx=165.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((dlf_pop.y + dlf_pop.height) + 5);
      x=> UIparent.clientWidth/3;
      flabel => "Contraction";
      fval => Params.dlf_contractradius;
      field {
	  x=> (<-.panel.width/2);
	  width => (<-.panel.width/2);
      };
      label {
	  width => (<-.panel.width/2);
      };
      panel {
	  width => UIparent.clientWidth/3;
	  visible => ((version > 1) && (Params.optimiser == 1) && 
		      (Params.dlf_type == 0));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein dlf_minradius<NEx=165.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((dlf_pop.y + dlf_pop.height) + 5);
      x=> UIparent.clientWidth/3*2;
      flabel => "Minimum Radius";
      fval => Params.dlf_minradius;
      field {
	  x=> (<-.panel.width/2);
	  width => (<-.panel.width/2);
	  decimalPoints = 6;
      };
      label {
	  width => (<-.panel.width/2);
      };
      panel {
	  width => UIparent.clientWidth/3;
	  visible => ((version > 1) && (Params.optimiser == 1) &&
	      (Params.dlf_type == 0));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein dlf_cycles<NEx=165.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((dlf_radius.y + dlf_radius.height) + 5);
      flabel => "Maximum Number of Cycles";
      fval => Params.dlf_cycles;
      field {
	  x=> (<-.panel.width/3*2);
	  width => (<-.panel.width/3);
      };
      label {
	  width => (<-.panel.width/3*2);
      };
      panel {
	  width => UIparent.clientWidth;
	  visible => ((version > 1) && (Params.optimiser == 1));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein dlf_mutation<NEx=165.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((dlf_cycles.y + dlf_cycles.height) + 5);
      width => UIparent.clientWidth/2;
      flabel => "Mutation Rate";
      fval => Params.dlf_mutation;
      field {
	  x=> (<-.panel.width/3*2);
	  width => (<-.panel.width/3);
      };
      label {
	  width => (<-.panel.width/3*2);
      };
      panel {
	  width => UIparent.clientWidth/2;
	  visible => ((version > 1) && (Params.optimiser == 1) &&
	      (Params.dlf_type == 1));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein dlf_death<NEx=165.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((dlf_cycles.y + dlf_cycles.height) + 5);
      x=> UIparent.clientWidth/2;
      flabel => "Death Rate";
      fval => Params.dlf_death;
      field {
	  x=> (<-.panel.width/3*2);
	  width => (<-.panel.width/3);
      };
      label {
	  width => (<-.panel.width/3*2);
      };
      panel {
	  width => UIparent.clientWidth/2;
	  visible => ((version > 1) && (Params.optimiser == 1)&&
	      (Params.dlf_type == 1));
      };
    };

    /*CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein gstep<NEx=165.,NEy=418.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((opt_type.y + opt_type.height) + 5);
      width => UIparent.clientWidth;
      flabel => "Gradient Step";
      fval => Params.gstep;
      panel {
	visible => (<-.Params.optimiser == 1);
      };
      field {
	decimalPoints = 4;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein scale<NEx=165.,NEy=418.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((gstep.y + gstep.height) + 5);
      width => UIparent.clientWidth;
      flabel => "Initial gradient scaling";
      fval => Params.iscale;
      panel {
	visible => (<-.Params.optimiser == 1);
      };
      };*/
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox dlf_type<NEx=165.,NEy=253.> {
      parent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => {"Stochastic Search", "Genetic Algorithm"};
      title = "Optimisation Method";
      y => ((gtol.y + gtol.height) + 5);
      width => parent.clientWidth/2;
      selectedItem => Params.dlf_type;
      UIpanel {
	  visible => ((version > 2) && (Params.optimiser == 1));
      };
    };	
   CCP3.Core_Macros.UI.UIobjs.DLVradioBox ssopt_type<NEx=165.,NEy=253.> {
      parent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      title = "Stochastic Search Method";
      labels => {"Uniform", "Force-Direction Bias", "Force Bias"};
      y => ((gtol.y + gtol.height) + 5);
      x => parent.clientWidth/2;
      width => parent.clientWidth/2;
      selectedItem => Params.dlf_sstype;
//neccesary??
      UIpanel {
	  visible => (version > 2) && (Params.optimiser == 1) && 
	      (Params.dlf_type==0);
      };
    };
   CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein dlf_pop<NEx=165.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((ssopt_type.y + ssopt_type.height) + 5);
      width => UIparent.clientWidth/2;
      flabel => "Population";
      fval => Params.dlf_pop;
      field {
	  x=> (<-.panel.width/3*2);
	  width => (<-.panel.width/3);
      };
      label {
	  width => (<-.panel.width/3*2);
      };
      panel {
	  width => UIparent.clientWidth/2;
	  visible => ((version > 2) && (Params.optimiser == 1));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein dlf_initpop<NEx=165.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((ssopt_type.y + ssopt_type.height) + 5);
      x=> UIparent.clientWidth/2;
      flabel => "Initial Population";
      fval => Params.dlf_initpop;
      field {
	  x=> (<-.panel.width/3*2);
	  width => (<-.panel.width/3);
      };
      label {
	  width => (<-.panel.width/3*2);
      };
      panel {
	  width => UIparent.clientWidth/2;
	  visible => ((version > 2) && (Params.optimiser == 1) &&
	      (Params.dlf_type == 1));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein dlf_scalef{
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((ssopt_type.y + ssopt_type.height) + 5);
      x=> UIparent.clientWidth/2;
      flabel => "Scale Factor";
      fval => Params.dlf_scalef;
      field {
	  x=> (<-.panel.width/3*2);
	  width => (<-.panel.width/3);
      };
      label {
	  width => (<-.panel.width/3*2);
      };
      panel {
	  width => UIparent.clientWidth/2;
	  visible => ((version > 2) && (Params.optimiser == 1) &&
	      (Params.dlf_type == 0));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein dlf_radius<NEx=165.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((dlf_pop.y + dlf_pop.height) + 5);
      width => UIparent.clientWidth/3;
      flabel => "Radius";
      fval => Params.dlf_radius;
      field {
	  x=> (<-.panel.width/2);
	  width => (<-.panel.width/2);
      };
      label {
	  width => (<-.panel.width/2);
      };
      panel {
	  width => UIparent.clientWidth/3;
	  visible => ((version > 2) && (Params.optimiser == 1));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein dlf_contractradius<NEx=165.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((dlf_pop.y + dlf_pop.height) + 5);
      x=> UIparent.clientWidth/3;
      flabel => "Contraction";
      fval => Params.dlf_contractradius;
      field {
	  x=> (<-.panel.width/2);
	  width => (<-.panel.width/2);
      };
      label {
	  width => (<-.panel.width/2);
      };
      panel {
	  width => UIparent.clientWidth/3;
	  visible => ((version > 2) && (Params.optimiser == 1) && 
		      (Params.dlf_type == 0));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein dlf_minradius<NEx=165.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((dlf_pop.y + dlf_pop.height) + 5);
      x=> UIparent.clientWidth/3*2;
      flabel => "Min. Radius";
      fval => Params.dlf_minradius;
      field {
	  x=> (<-.panel.width/2);
	  width => (<-.panel.width/2);
	  decimalPoints = 6;
      };
      label {
	  width => (<-.panel.width/2);
      };
      panel {
	  width => UIparent.clientWidth/3;
	  visible => ((version > 2) && (Params.optimiser == 1) &&
	      (Params.dlf_type == 0));
      };
    };
   CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein dlf_cycles<NEx=165.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((dlf_radius.y + dlf_radius.height) + 5);
      flabel => "Max. no. of cycles";
      fval => Params.dlf_cycles;
      field {
	  x=> (<-.panel.width/2);
	  width => (<-.panel.width/2);
      };
      label {
	  width => (<-.panel.width/2);
      };
      panel {
	  width => UIparent.clientWidth/2;
	  visible => ((version > 2) && (Params.optimiser == 1));
      };
    };		     
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein dlf_reset<NEx=165.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((dlf_pop.y + dlf_pop.height) + 5);
      x=> UIparent.clientWidth/3;
      flabel => "No. of cycles before pop. reset";
      fval => Params.dlf_reset;
      field {
	  x=> (<-.panel.width/3*2);
	  width => (<-.panel.width/3);
      };
      label {
	  width => (<-.panel.width/3*2);
      };
      panel {
	  width => UIparent.clientWidth/3*2;
	     visible => ((version > 2) && (Params.optimiser == 1) &&
			 (Params.dlf_type == 1));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein dlf_nsaves<NEx=165.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((dlf_radius.y + dlf_radius.height) + 5);
      x=> UIparent.clientWidth/2;
      flabel => "No. Structures to Save";
      fval => Params.dlf_nsaves;
      field {
	  x=> (<-.panel.width/3*2);
	  width => (<-.panel.width/3);
      };
      label {
	  width => (<-.panel.width/3*2);
      };
      panel {
	  width => UIparent.clientWidth/2;
	  visible => ((version > 2) && (Params.optimiser == 1) 
		      &&(Params.dlf_type == 1));
      };
    };		     
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein dlf_mutation<NEx=165.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((dlf_cycles.y + dlf_cycles.height) + 5);
      width => UIparent.clientWidth/2;
      flabel => "Mutation Rate";
      fval => Params.dlf_mutation;
      field {
	  x=> (<-.panel.width/3*2);
	  width => (<-.panel.width/3);
      };
      label {
	  width => (<-.panel.width/3*2);
      };
      panel {
	  width => UIparent.clientWidth/2;
	  visible => ((version > 2) && (Params.optimiser == 1) &&
	      (Params.dlf_type == 1));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein dlf_death<NEx=165.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((dlf_cycles.y + dlf_cycles.height) + 5);
      x=> UIparent.clientWidth/2;
      flabel => "Death Rate";
      fval => Params.dlf_death;
      field {
	  x=> (<-.panel.width/3*2);
	  width => (<-.panel.width/3);
      };
      label {
	  width => (<-.panel.width/3*2);
      };
      panel {
	  width => UIparent.clientWidth/2;
	  visible => ((version > 2) && (Params.optimiser == 1)&&
	      (Params.dlf_type == 1));
      };
    };
  };
  baseUI PrintUI {
    CCP3shell<NEx=253.,NEy=187.> {
      title = "CRYSTAL SCF Print";
    };
    CCP3.CRYSTAL.Modules.PrintParams &Params<NEx=495.,NEy=99.,
                                             NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle mulliken<NEx=253.,NEy=264.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y = 0;
      width => parent.clientWidth;
      set => Params.calc_mulliken;
      label = "Population Analysis";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle exchange<NEx=253.,NEy=341.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((mulliken.y + mulliken.height) + 5);
      width => parent.clientWidth;
      set => Params.calc_exchange;
      label = "Exchange Energy";
    };
  };
  baseUI JobUI {
    link version;
    CCP3shell<NEx=253.,NEy=187.> {
      title = "CRYSTAL SCF Job";
      UIshell {
	width = 450;
	height = 500;
      };
    };
    CCP3.CRYSTAL.Modules.JobParams &Params<NEx=495.,NEy=99.,
                                           NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle direct<NEx=198.,NEy=253.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y = 0;
      width => parent.clientWidth;
      visible => (version > 0);
      set => Params.direct;
      label = "Direct SCF";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle mono<NEx=198.,NEy=308.> {
      visible => (Params.direct & (version > 0));
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((direct.y + direct.height) + 5);
      width => parent.clientWidth;
      set => Params.mondirect;
      label = "Direct Mono SCF";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein biesplit<NEx=429.,NEy=253.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => switch((version + 1), 0, ((mono.y + mono.height) + 5),
		  ((mono.y + mono.height) + 5), ((mono.y + mono.height) + 5),
		  ((mono.y + mono.height) + 5), ((mono.y + mono.height) + 5),
		  ((mono.y + mono.height) + 5));
      width => UIparent.clientWidth;
      flabel => "Split Bielectronic Integrals";
      fval => Params.biesplit;
      panel {
	visible => (Params.direct != 1);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein monsplit<NEx=429.,NEy=308.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((biesplit.y + biesplit.height) + 5);
      width => UIparent.clientWidth;
      flabel => "Split Monoelectronic integrals";
      fval => Params.monsplit;
      panel {
	visible => !((Params.mondirect == 1) & (Params.direct == 1));
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle gradients {
      visible => (version > 0);
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((monsplit.y + monsplit.height) + 5);
      width => parent.clientWidth;
      set => Params.gradcalc;
      label = "Calculate Gradients";
    };
    string restart_options<NEportLevels=1>[] = {
      "No restart",
      "Restart from Fock Matrix",
      "Restart from Density Matrix"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox restart<NEx=198.,NEy=429.> {
      parent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => ((gradients.y + gradients.height) + 5);
      width => parent.clientWidth;
      selectedItem => Params.restart;
      labels => <-.restart_options;
      UIlabel {
	label = "Restart";
      };
    };
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=407.,NEy=429.> {
      preferences => <-.prefs;
      UIpanel {
	visible => (<-.<-.Params.restart > 0);
      };
      filename<NEportLevels={2,0}> => <-.Params.fock_file;
      pattern = "*";
      parent => <-.CCP3shell.UIpanel;
      title = "CRYSTAL restart filename";
      x = 0;
      y => (<-.restart.y + <-.restart.height + 5);
    };
  };
  baseUI PhononUI {
    CCP3shell<NEx=253.,NEy=187.> {
      title = "CRYSTAL Phonon Input";
      UIshell {
	width = 300;
	height = 350;
      };
    };
    link+nres model_type<NEportLevels={2,1},NEx=270.,NEy=72.>;
    link version<NEportLevels={2,1},NEx=891.,NEy=27.>;
    CCP3.CRYSTAL.Modules.PhononParams &params<NEx=495.,NEy=77.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein stepsize<NEx=363.,NEy=286.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y = 0;
      flabel => "Step size";
      fmin = 0.001;
      fmax = 0.1;
      fval => <-.params.step;
      field {
	decimalPoints = 3;
      };
      width => UIparent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox DLVradioBox<NEx=363.,NEy=363.> {
      parent => <-.CCP3shell.UIpanel;
      labels => <-.deriv_labels;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      y => (<-.stepsize.y + <-.stepsize.height + 5);
      selectedItem => <-.params.deriv;
      width => parent.clientWidth;
      title = "Derivative method";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle DLVtoggle<NEx=363.,NEy=440.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      set => <-.params.intensity;
      y => (<-.DLVradioBox.y + <-.DLVradioBox.height + 5);
      width => parent.clientWidth;
      label = "Calculate intensities";
    };
    string deriv_labels<NEportLevels=1,NEx=627.,NEy=286.>[] = {
      "Difference quotient", "Central difference"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle dispersion<NEx=621.,NEy=378.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      set => <-.params.dispersion;
      y => (<-.DLVtoggle.y + <-.DLVtoggle.height + 5);
      width => parent.clientWidth;
      label = "Calculate Dispersion";
      visible => (version > 2);
    };
    macro cell<NEx=621.,NEy=441.> {
      link model_type<NEportLevels={2,1},NEx=855.,NEy=27.> => <-.model_type;
      link parent<NEportLevels={2,1},NEx=261.,NEy=36.> => <-.CCP3shell.UIpanel;
      CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},NEx=612.,
					       NEy=27.> => <-.prefs;
      CCP3.Core_Macros.UI.UIobjs.DLVifield scale_11<NEx=121.,NEy=176.> {
	parent => <-.parent;
	&color => <-.prefs.colour;
	&fontAttributes => <-.prefs.fontAttributes;
	value => params.cell[0][0];
	y => (<-.<-.dispersion.height + <-.<-.dispersion.y +  5);
	active => (model_type > 0);
	visible => <-.<-.params.dispersion;
	width => ((parent.clientWidth - 30) / 3);
	x = 5;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVifield scale_12<NEx=319.,NEy=176.> {
	parent => <-.parent;
	&color => <-.prefs.colour;
	&fontAttributes => <-.prefs.fontAttributes;
	value => params.cell[0][1];
	y => scale_11.y;
	active => (model_type > 1);
	width => <-.scale_11.width;
	visible => <-.<-.params.dispersion;
	x => (parent.clientWidth / 3 + 5);
      };
      CCP3.Core_Macros.UI.UIobjs.DLVifield scale_13<NEx=517.,NEy=176.> {
	parent => <-.parent;
	&color => <-.prefs.colour;
	&fontAttributes => <-.prefs.fontAttributes;
	value => params.cell[0][2];
	y => scale_12.y;
	active => (model_type == 3);
	width => <-.scale_11.width;
	visible => <-.<-.params.dispersion;
	x => (2 * parent.clientWidth / 3 + 5);
      };
      CCP3.Core_Macros.UI.UIobjs.DLVifield scale_21<NEx=121.,NEy=264.> {
	parent => <-.parent;
	&color => <-.prefs.colour;
	&fontAttributes => <-.prefs.fontAttributes;
	value => params.cell[1][0];
	y => ((scale_11.y + scale_11.height) + 5);
	active => (model_type > 1);
	width => <-.scale_11.width;
	visible => <-.<-.params.dispersion;
	x => <-.scale_11.x;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVifield scale_22<NEx=319.,NEy=264.> {
	parent => <-.parent;
	&color => <-.prefs.colour;
	&fontAttributes => <-.prefs.fontAttributes;
	value => params.cell[1][1];
	y => scale_21.y;
	active => (model_type > 1);
	width => <-.scale_11.width;
	visible => <-.<-.params.dispersion;
	x => <-.scale_12.x;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVifield scale_23<NEx=517.,NEy=264.> {
	parent => <-.parent;
	&color => <-.prefs.colour;
	&fontAttributes => <-.prefs.fontAttributes;
	value => params.cell[1][2];
	y => scale_22.y;
	active => (model_type == 3);
	width => <-.scale_11.width;
	visible => <-.<-.params.dispersion;
	x => <-.scale_13.x;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVifield scale_31<NEx=121.,NEy=352.> {
	parent => <-.parent;
	&color => <-.prefs.colour;
	&fontAttributes => <-.prefs.fontAttributes;
	value => params.cell[2][0];
	y => ((scale_21.y + scale_21.height) + 5);
	active => (model_type == 3);
	width => <-.scale_11.width;
	visible => <-.<-.params.dispersion;
	x => <-.scale_11.x;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVifield scale_32<NEx=319.,NEy=352.> {
	parent => <-.parent;
	&color => <-.prefs.colour;
	&fontAttributes => <-.prefs.fontAttributes;
	value => params.cell[2][1];
	y => scale_31.y;
	active => (model_type == 3);
	width => <-.scale_11.width;
	visible => <-.<-.params.dispersion;
	x => <-.scale_12.x;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVifield scale_33<NEx=517.,NEy=352.> {
	parent => <-.parent;
	&color => <-.prefs.colour;
	&fontAttributes => <-.prefs.fontAttributes;
	value => params.cell[2][2];
	y => scale_32.y;
	active => (model_type == 3);
	width => <-.scale_11.width;
	visible => <-.<-.params.dispersion;
	x => <-.scale_13.x;
      };
    };
  };
  CCP3.CRYSTAL.SCF.baseUI NebUI<NEx=729.,NEy=450.> {
    link version<NEx=660.,NEy=55.>;
    link job_data<NEx=803.,NEy=187.,NEportLevels={2,1}>;
    CCP3shell<NEx=253.,NEy=187.> {
      title = "CRYSTAL Nudged Elastic Band Input";
      UIshell {
	width  => 500;
	height => 900;
      };
    };
    CCP3.CRYSTAL.Modules.NebParams &params<NEx=279.,NEy=81.>;
    CCP3.CRYSTAL.Modules.SCFParams &scfparams<NEx=270.,NEy=54.> {
      Hamiltonian<NEportLevels={0,2}>;
      Basis<NEportLevels={0,2}>;
      KPoints<NEportLevels={0,2}>;
      Tolerances<NEportLevels={0,2}>;
      Convergence<NEportLevels={0,2}>;
      Optimize<NEportLevels={0,2}>;
      Print<NEportLevels={0,2}>;
      Job<NEportLevels={0,2}>;
      phonon<NEportLevels={0,2}>;
      neb<NEportLevels={0,2}>;
    };
    CCP3.CRYSTAL.Modules.nebtol nebtol<NEx=216.,NEy=324.>{
      convergence_int=> (<-.params.convergence_int);
      tol_energy=> (<-.params.tol_energy);
      tol_step=> (<-.params.tol_step);
      tol_grad=> (<-.params.tol_grad);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Nimages<NEx=18.,NEy=144.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Number of images";
      fmin => 1;
      fmax => 99;
      fval => <-.params.nimages;
      x = 0;
      y = 0;
       panel<NEx=459.,NEy=387.> {
	  width => ((2 * parent.clientWidth) / 3);
	  active => ((Adapt.set==0)&&(!Reset_Images.active));
      };
      label {
	  width => (<-.panel.width / 3);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Niters<NEx=18.,NEy=180.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Max Iterations";
      fmin => 1;
      fval => <-.params.iters;
      x = 0;
      y => (Nimages.y + Nimages.height + 5);
      panel<NEx=459.,NEy=387.> {
	  width => ((2 * parent.clientWidth) / 3);
      };
      label {
	  width => (<-.panel.width / 3);
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein Climb<NEx=18.,NEy=216.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Climb after";
      fval => <-.params.climb;
      fmin => 1;
      fmax => params.iters;
      y => (Niters.y + Niters.height + 5);
      panel<NEx=459.,NEy=387.> {
	  width => ((2 * parent.clientWidth) / 3);
      };
      label {
	  width => (<-.panel.width / 3);
      };
    };
    UIlabel Iters_climb<NEx=18.,NEy=252.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => (2*parent.clientWidth/3);
      y => (Niters.y + Niters.height + 7);
      width => parent.clientWidth/3;
      label = "iterations";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein Start_Energy<NEx=18.,NEy=288.>
    {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Energy of First Structure";
      fval => <-.params.start_ene;
      fmax => 0.;
      panel.width => ((2 * parent.clientWidth) / 3);
      label.width => (<-.panel.width / 2);
      x => (panel.width/2);
      y => (Climb.y + Climb.height + 5);
      field.decimalPoints = 6;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein Final_Energy<NEx=18.,NEy=324.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Energy of Final Structure";
      fval => <-.params.final_ene;
      fmax => 0.;
      panel.width => ((2 * parent.clientWidth) / 3);
      label.width => (<-.panel.width / 2);
      x => (panel.width/2);
      y => (Start_Energy.y + Start_Energy.height + 5);
      field.decimalPoints = 6;
    };
    UIbutton Select_str2<NEx=414.,NEy=180.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Select Final Structure";
      active=> (!(<-.params.end_model+1));
      y => (Start_Energy.y + Start_Energy.height + 0);
      width => parent.clientWidth/3;
    };
    UIbutton Reset_str2<NEx=414.,NEy=216.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Reset Final Structure";
      active=> !<-.Select_str2.active;
      y => (Start_Energy.y + Start_Energy.height + 20);
      width => parent.clientWidth/3;
    };
    CCP3.CRYSTAL.Modules.nebview nebview<NEx=414.,NEy=288.>{
      trigger => <-.NebEndStrUI.DLVdialog.ok;
      mapping=> <-.params.mapping;
      mapping_status = 0;
      natoms => <-.params.natoms;
      first_model => <-.params.first_model;
      end_model => <-.NebEndStrUI.models.selectedItem;
      updated_end_model => <-.params.end_model;			    
      };
    CCP3.CRYSTAL.Modules.nebreset nebreset<NEx=414.,NEy=252.>{
	trigger => <-.Reset_str2.do;
	nimages => <-.params.nimages;
	end_model => <-.params.end_model;
	first_model => <-.params.first_model;
	make91 => <-.params.make91;
	images_dir => <-.params.images_dir;
	};
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle Hide_atoms<NEx=414.,NEy=369.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((Final_Energy.y + Final_Energy.height) + 5);
      active => <-.params.end_model+1;
      width => parent.clientWidth/3;
      label = "Hide selected atoms";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVfSlider Atom_dist<NEx=414.,NEy=405.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth/3;
      title = "Maximum distance moved";
      x => parent.clientWidth/3;
      y => ((<-.Final_Energy.y + <-.Final_Energy.height) + 5);
      min = 0.0;
      max = 10.0;
      active => Hide_atoms.set;
      value => <-.params.dist;
    };
    CCP3.CRYSTAL.Modules.nebhide nebhide<NEx=414.,NEy=324.>{
	trigger => ((Hide_atoms.set)||(params.dist));
	dist => <-.params.dist;
	end_model => <-.params.end_model;
	mapping => <-.params.mapping;
      };
    UIbutton Edit_mappings<NEx=414.,NEy=450.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Create new mapping";
      active => ((<-.params.end_model+1)&&(!Create_Images.do));
      x => 2*parent.clientWidth/3;
      y => (Final_Energy.y + Final_Energy.height + 5);
      width => parent.clientWidth/3;
    };
    CCP3.CRYSTAL.Modules.nebedit nebedit<NEx=594.,NEy=396.>{
	trigger => <-.Edit_mappings.do;
	dist => <-.params.dist;
	hide_atoms => Hide_atoms.set;
	end_model => <-.params.end_model;
	mapping => <-.params.mapping;  
      };
    UIbutton Create_Images<NEx=594.,NEy=144.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Create Images";
      active => ((<-.params.end_model+1) && 
		 (!<-.params.images_button));
      width => parent.clientWidth/4;
      y => (Atom_dist.y + Atom_dist.height + 5);      
    };
    UIbutton Reset_Images<NEx=594.,NEy=144.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Reset Images";
      active => ((<-.params.end_model+1) && 
		 (<-.params.images_button));
      width => parent.clientWidth/4;
      y => (Create_Images.y + Create_Images.height);      
    };						    
    CCP3.CRYSTAL.Modules.nebcreate nebcreate<NEx=594.,NEy=180.>{
	trigger => <-.Create_Images.do;
	data => <-.scfparams;
	job_data => <-.job_data;
      };
    CCP3.CRYSTAL.Modules.nebresetim nebresetim{
	trigger => <-.Reset_Images.do;
	images_button => <-.params.images_button;
	make91 => <-.params.make91;
	nimages => <-.params.nimages;				       
      };						    
    CCP3.Core_Macros.UI.UIobjs.DLViSlider Select_Image<NEx=594.,NEy=216.> {
      parent => <-.CCP3shell.UIpanel; //reset view_image.do if this changes
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      title => "Select Image";
      min => 1;
      max => <-.params.nimages;
      value=>showValue;
      width => ((parent.clientWidth) / 2);
      x => (parent.clientWidth/4);
      y => (Atom_dist.y + Atom_dist.height + 5);
      active => (<-.Reset_Images.active);
    };
    GMOD.parse_v parse_v<NEx=319.,NEy=77.> {
      v_commands = "View_Image.do= 0;";
      trigger => <-.Select_Image.value;
      on_inst = 0;
      relative => <-;				    
    };
    UIbutton View_Image<NEx=594.,NEy=252.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "View Image";
      width => parent.clientWidth/4;
      x => 3*parent.clientWidth/4;
      y => (Atom_dist.y + Atom_dist.height + 5);
      active => (<-.Reset_Images.active);      
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle Edit_Image<NEx=594.,NEy=324.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => 3*parent.clientWidth/4;
      y => (View_Image.y + View_Image.height + 5);
      active => <-.View_Image.do; //reset this on closure - or open it again??
      width => parent.clientWidth/3;
      label = "Edit Image";
    };
    CCP3.CRYSTAL.Modules.nebviewimage nebviewimage<NEx=594.,NEy=288.>{
	trigger => <-.View_Image.do;
	image => <-.Select_Image.value;
	job_data => <-.job_data;
	images_dir => <-.params.images_dir;
      };
    CCP3.CRYSTAL.Modules.nebedimage nebedimage<NEx=594.,NEy=360.>{
	trigger => <-.Edit_Image.set;
	image => <-.Select_Image.value;
      };
    
    link nselections<NEportLevels=1,NEx=495.,NEy=99.>;
    macro MoveAtom<NEx=306.,NEy=503.> {
      CCP3.Core_Macros.Core.preferences &preferences<NEportLevels={2,1},
	  NEx=198.,NEy=55.>;
	link active_menu<NEportLevels={2,1},NEx=440.,NEy=55.> = 1;
	int visible<NEportLevels=1,NEx=671.,NEy=55.> => <-.Edit_Image.set;
	link UIparent<NEportLevels={2,1},NEx=858.,NEy=55.> => <-.CCP3shell.UIpanel;
	int ok<NEportLevels=1,NEx=231.,NEy=209.> = 0;
	int cancel<NEportLevels=1,NEx=528.,NEy=220.> = 0;
	link nselections<NEportLevels={2,1},NEx=747.,NEy=162.> => <-.nselections; 
	link model_type<NEportLevels=1> => 0;  //<-.model_type; HACK
	GMOD.instancer instancer<NEx=517.,NEy=165.> {
	  Group => move_atomUI;
	  Value => <-.visible;
	};
	CCP3.CRYSTAL.Modules.edit_model edit_model<NEx=275.,NEy=550.> {
	  trigger_ok => <-.ok;
	  trigger_cancel => <-.cancel;
	  image => <-.<-.Select_Image.value;
	};
	CCP3.CRYSTAL.Modules.move_atom move_atom<NEx=711.,NEy=630.> {
	  trigger => <-.move_atomUI.Continue.do;
	  image => <-.<-.Select_Image.value;
	};
	CCP3.Core_Modules.Model.move_params params<NEx=774.,NEy=171.,
	  NEportLevels={0,1}> {
	  fractional = 0;
	  x = 0.0;
	  y = 0.0;
	  z = 0.0;
	  use_transform = 0;
	};
	CCP3.Core_Macros.UI.EditModel.move_atomUI move_atomUI<NEx=363.,NEy=286.,instanced=0> {
	  preferences => <-.<-.prefs;
	  UIparent => <-.<-.CCP3shell.UIpanel;
	  visible => <-.visible;
	  nselections => <-.nselections;
	  DLVdialog.ok => <-.<-.ok;
	  DLVdialog.cancel => <-.<-.cancel;
	  params => <-.params;
	  model_type => <-.model_type;
	};
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox Spr_Str<NEx=18.,NEy=360.> {
	parent => <-.CCP3shell.UIpanel;
	color => <-.prefs.colour;
	fontAttributes => <-.prefs.fontAttributes;
	labels => <-.separation_list;
	y => (Select_Image.y + Select_Image.height + 5);
	selectedItem => params.separation_int;
	title => "Image Separation Method";
	width => parent.clientWidth;
    };
    string separation_list<NEportLevels=1,NEx=18.,NEy=396.>[] = {
	"String",
	"Spring"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox Opt_method<NEx=18.,NEy=432.> {
	parent => <-.CCP3shell.UIpanel;
	color => <-.prefs.colour;
	fontAttributes => <-.prefs.fontAttributes;
	labels => <-.optimisation_list;
	y => (Spr_Str.y + Spr_Str.height + 5);
	selectedItem => params.optimiser_int;
	title => "Optimisation Method";
	width => parent.clientWidth;
    };
    string optimisation_list<NEportLevels=1,NEx=33.,NEy=143.>[] = {
    	"L-BFGS",
	"Damped MD"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox Conv_method<NEx=18.,NEy=504.> {
	parent => <-.CCP3shell.UIpanel;
	color => <-.prefs.colour;
	fontAttributes => <-.prefs.fontAttributes;
	labels => <-.conv_list;
	y => (Opt_method.y + Opt_method.height + 5);
	selectedItem => params.convergence_int;
	title => "Convergence Criteria";
	width => parent.clientWidth;
    };
    string conv_list<NEportLevels=1,NEx=216.,NEy=288.>[] = {
	"High Tolerances",
	"Low Tolerances",
	"User Defined"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein Tol_Energy<NEx=216.,NEy=144.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Energy Tolerance";
      fmin = 0.0;
      fval => <-.params.tol_energy;
      panel{
	  width => ((2 * parent.clientWidth) / 3);
	  active => (params.convergence_int == 2);
      };
      label {
	  width => (<-.panel.width / 2);
      };
      field.decimalPoints = 5;
      x => (panel.width / 2);
      y => (Opt_method.y + 1.5*Opt_method.height);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein Tol_Step<NEx=216.,NEy=180.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Step size Tolerance";
      fval => <-.params.tol_step;
      fmin = 0.0;
      panel<NEx=459.,NEy=387.> {
	  width => ((2 * parent.clientWidth) / 3);
	  active => (params.convergence_int == 2);
      };
      label {
	  width => (<-.panel.width / 2);
      };
      field.decimalPoints = 5;
      x => (panel.width / 2);
      y => (Tol_Energy.y + Tol_Energy.height);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein Tol_Force<NEx=216.,NEy=216.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Force Tolerance";
      fval => <-.params.tol_grad;
      fmin = 0.0;
      panel<NEx=459.,NEy=387.> {
	  width => ((2 * parent.clientWidth) / 3);
	  active => (params.convergence_int == 2);
      };
      label {
	  width => (<-.panel.width / 2);
      };
      field.decimalPoints = 5;
      x => (panel.width / 2);
      y => (Tol_Step.y + Tol_Step.height);
    };
    UIlabel Options<NEx=216.,NEy=369.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((Tol_Force.y + Tol_Force.height) + 5);  
      width => parent.clientWidth;
      label = "Additional Options";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle Restart<NEx=216.,NEy=405.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((Options.y + Options.height) + 5);
      width => parent.clientWidth;
      label = "Restart from Previous Calculation";  
      set => params.restart;
    };
    CCP3.Core_Macros.UI.UIobjs.FileBrowser File_end<NEx=216.,NEy=252.> {
      preferences => <-.prefs;
      UIpanel {
        visible => params.restart;
        active => params.restart;
	width => (parent.clientWidth / 2);
      };
      filename<NEportLevels={2,0}> => <-.params.restart_file;
      pattern = "*";
      parent => <-.CCP3shell.UIpanel;
      title = "NEB restart filename";
      y => ((<-.Options.y + <-.Options.height) + 5);
      x => (parent.clientWidth / 2);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle Adapt<NEx=216.,NEy=441.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((Restart.y + Restart.height) + 5);
      width => parent.clientWidth;
      label = "Use Adaptive Images (No. of images must equal 3)";
      active => (params.nimages==3);  
      set => (params.adapt); 
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle Symmpath<NEx=216.,NEy=477.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((Adapt.y + Adapt.height) + 5);
      width => parent.clientWidth;
      label = "Symmetrical Energy Path";  
      set => params.symmpath;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle Usestrsym<NEx=216.,NEy=513.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((Symmpath.y + Symmpath.height) + 5);
      width => parent.clientWidth;
      label = "Use Structure's Symmetry";  
      set => params.usestrsym;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle Nocalc<NEx=216.,NEy=513.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((Usestrsym.y + Usestrsym.height) + 5);
      width => parent.clientWidth;
      label = "No NEB Optimisation - Test Run only";
      set => params.nocalc;
    };
    //File_end<NEx=702.,NEy=459.>;
    CCP3.CRYSTAL.Base.simple_baseUI NebEndStrUI<NEx=414.,NEy=144.> {
      prefs => <-.prefs;
      UIparent<NEportLevels={2,1},NEx=486.,NEy=72.> => <-.CCP3shell.UIpanel;
      int visible<NEportLevels={2,1},NEx=450.,NEy=144.> => <-.Select_str2.do;
      CCP3.Core_Macros.UI.UIobjs.DLVdialog DLVdialog<NEportLevels={2,1},NEx=378.,NEy=225.> {
	parent => <-.UIparent;
	visible => <-.visible;
	title = "Select End Structure";
	x = 300;
	y = 300;
	width = 350;
	height = 150;
      };
      UIpanel UIpanel<NEx=558.,NEy=288.> {
	parent => <-.DLVdialog;
      };
      CCP3.CRYSTAL.Modules.list_neb_models list_neb_models<NEx=450.,NEy=380.> {
        trigger => <-.visible;
	first_model=> <-.<-.params.first_model;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVlist models<NEx=333.,NEy=370.> {
        parent => <-.UIpanel;
        width => parent.clientWidth;
        &color => <-.prefs.colour;
        &fontAttributes => <-.prefs.fontAttributes;
        strings => <-.list_neb_models.labels;
      };
    };
  };
  baseUI cphfUI {
    CCP3shell<NEx=253.,NEy=187.> {
      title = "CRYSTAL CPHF Input";
    };
    CCP3.CRYSTAL.Modules.CPHFParams &Params<NEx=495.,NEy=99.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein mixing<NEx=165.,NEy=330.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Mixing";
      fval => Params.mixing;
      y = 5;
      width => UIparent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein cycles<NEx=165.,NEy=374.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Maximum Cycles";
      fval => Params.cycles;
      y => ((mixing.y + mixing.height) + 5);
      width => UIparent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein alpha<NEx=165.,NEy=418.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Convergence tolerance";
      fval => Params.alpha;
      y => ((cycles.y + cycles.height) + 5);
      width => UIparent.clientWidth;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein udik<NEx=165.,NEy=462.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Eigenvalue tolerance";
      fval => Params.udik;
      y => ((alpha.y + alpha.height) + 5);
      width => UIparent.clientWidth;
    };
  };
  baseUI tddftUI {
    CCP3shell<NEx=253.,NEy=187.> {
      title = "CRYSTAL TD-DFT Input";
      UIshell {
	height = 450;
      };
    };
    CCP3.CRYSTAL.Modules.TDDFTParams &Params<NEx=495.,NEy=99.,
      NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle scf<NEx=459.,NEy=315.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      label = "TD-DFT/TD-HF calculation";
      y = 5;
      set => <-.Params.do_scf;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle tamm_dancoff<NEx=657.,NEy=315.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      label = "Tamm-Dancoff";
      y => (<-.scf.y + <-.scf.height + 5);
      set => <-.Params.tamm_dancoff;
      visible => <-.Params.do_scf;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle jdos<NEx=459.,NEy=234.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      label = "Calculate joint density of states";
      y => (<-.tamm_dancoff.y + <-.tamm_dancoff.height + 5);
      set => <-.Params.calc_jdos;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle pes<NEx=666.,NEy=234.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      label = "Calculate one-particle spectrum";
      y => (<-.jdos.y + <-.jdos.height + 5);
      set => <-.Params.calc_pes;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle direct<NEx=468.,NEy=387.> {
      parent => <-.CCP3shell.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      label = "Use fast response";
      y => (<-.pes.y + <-.pes.height + 5);
      set => <-.Params.fastresponse;
      visible => <-.Params.do_scf;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox diag_method<NEx=468.,NEy=459.> {
      parent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => { "Direct", "Davidson" };
      y => (direct.y + direct.height + 5);
      selectedItem => <-.Params.diag;
      title => "Diagonalisation Method";
      width => parent.clientWidth;
      visible => <-.Params.do_scf;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein dv_state<NEx=630.,NEy=459.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Lowest Excitation";
      fval => Params.davidson_state;
      fmin = 1;
      y => (<-.diag_method.y + <-.diag_method.height + 5);
      width => UIparent.clientWidth;
      panel {
	visible => (<-.<-.Params.diag == 1) && <-.<-.Params.do_scf;

      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein dv_iters<NEx=846.,NEy=459.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Max iterations";
      fval => Params.davidson_niters;
      fmin = 5;
      y => (<-.dv_state.y + <-.dv_state.height + 5);
      width => UIparent.clientWidth;
      panel {
	visible => (<-.<-.Params.diag == 1) && <-.<-.Params.do_scf;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein dv_space<NEx=630.,NEy=522.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Subspace size";
      fval => Params.davidson_space;
      fmin = 10;
      y => (<-.dv_iters.y + <-.dv_iters.height + 5);
      width => UIparent.clientWidth;
      panel {
	visible => (<-.<-.Params.diag == 1) && <-.<-.Params.do_scf;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein dv_convgerge<NEx=846.,NEy=522.> {
      UIparent => <-.CCP3shell.UIpanel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Convergence";
      fval => Params.davidson_conv;
      y => (<-.dv_space.y + <-.dv_space.height + 5);
      width => UIparent.clientWidth;
      panel {
	visible => (<-.<-.Params.diag == 1) && <-.<-.Params.do_scf;
      };
    };
  };
  CCP3.CRYSTAL.Base.baseUI scfUI {
    SCF_limits limits;
    CCP3.CRYSTAL.Modules.SCFParams &SCFParams<NEx=517.,NEy=99.> {
      Hamiltonian<NEportLevels={0,2}>;
      Basis<NEportLevels={0,2}>;
      KPoints<NEportLevels={0,2}>;
      Tolerances<NEportLevels={0,2}>;
      Convergence<NEportLevels={0,2}>;
      Optimize<NEportLevels={0,2}>;
      Print<NEportLevels={0,2}>;
      Job<NEportLevels={0,2}>;
      phonon<NEportLevels={0,2}>;
      neb<NEportLevels={0,2}>;
      cphf<NEportLevels={0,2}>;
      tddft<NEportLevels={0,2}>;
    };
    int model_has_spin<NEx=506.,NEy=55.> = 0;
    int select_spin<NEx=704.,NEy=55.> =>
      switch ((SCFParams.Hamiltonian.Htype + 1),
	      switch ((model_has_spin + 1), 0, 2),model_has_spin);
    GMOD.parse_v parse_v<NEx=319.,NEy=77.> {
      /*v_commands = "visible_ham = 0; visible_basis = 0; visible_tol = 0;"
	+ " visible_k = 0; visible_conv = 0; visible_job = 0; visible_opt = 0;"
	+ " visible_print = 0; visible_phon = 0; visible_neb = 0;"
	+ " SCFParams.Hamiltonian.Hoptions = select_spin;"
	+ " SCFParams.Hamiltonian.use_spins = model_has_spin;";*/
      trigger => <-.visible;
      on_inst = 1;
      //relative => <-;
    };
    link version;
    link job_data<NEx=803.,NEy=187.,NEportLevels={2,1}>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog UItemplateDialog<NEx=275.,NEy=165.> {
      parent => <-.UIparent;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      visible => <-.visible;
      title = "CRYSTAL SCF calculation";
      width = 400;
      height = 300;
      ok<NEportLevels={1,2}> = 0;
      okButton => (!(<-.NEB.visible && (!(<-.SCFParams.neb.end_model + 1))));
      okLabelString = "Run";
      cancel = 0;
      cancelButton = 1;
#ifdef MSDOS
      y = 0;
#endif // MSDOS
    };
    UIpanel UIpanel<NEx=286.,NEy=242.> {
      parent => <-.UItemplateDialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    UIbutton Hamiltonian<NEx=33.,NEy=363.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 150;
      y = 5;
      do => <-.visible_ham;
    };
    UIbutton Basis_Sets<NEx=33.,NEy=407.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => (parent.clientWidth / 2);
      y => Hamiltonian.y;
      width = 150;
      do => <-.visible_basis;
    };
    UIbutton Tolerances<NEx=198.,NEy=407.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((Hamiltonian.y + Hamiltonian.height) + 5);
      width = 150;
      do => <-.visible_tol;
    };
    UIbutton K_Points<NEx=374.,NEy=407.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width = 150;
      y => ((Tolerances.y + Tolerances.height) + 5);
      active => (<-.<-.<-.model_type > 0);
      do => <-.visible_k;
    };
    UIbutton SCF<NEx=539.,NEy=407.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "SCF Convergence";
      y => Tolerances.y;
      x => (parent.clientWidth / 2);
      width = 150;
      do => <-.visible_conv;
    };
    UIbutton Print<NEx=374.,NEy=363.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "Print Options";
      x => (parent.clientWidth / 2);
      y => K_Points.y;
      width = 150;
      do => <-.visible_print;
    };
    UIbutton Job<NEx=198.,NEy=363.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((K_Points.y + K_Points.height) + 5);
      width = 150;
      do => <-.visible_job;
    };
    UIbutton Optimise<NEx=539.,NEy=363.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => (parent.clientWidth / 2);
      y => Job.y;
      visible => ((version > 0) && (SCFParams.task == 1));
      width = 150;
      do => <-.visible_opt;
    };
    UIbutton Phonon<NEx=693.,NEy=363.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => Optimise.x;
      y => Optimise.y;
      visible => ((version > 1) && (SCFParams.task == 2));
      width = 150;
      do => <-.visible_phon;
    };
    UIbutton NEB<NEx=693.,NEy=407.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => Optimise.x;
      y => Optimise.y;
      visible => ((version > 2) && (SCFParams.task == 3));
      width = 150;
      do => <-.visible_neb;
    };
    UIbutton CPHF {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => Optimise.x;
      y => Optimise.y;
      visible => ((version > 2) && (SCFParams.task == 4));
      width = 150;
      do => <-.visible_cphf;
    };
    UIbutton TDDFT {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => Optimise.x;
      y => Optimise.y;
      visible => ((version > 3) && (SCFParams.task == 5));
      width = 150;
      do => <-.visible_tddft;
    };
    GMOD.instancer instance_basis<NEx=869.,NEy=55.> {
      Value => (<-.visible_basis && <-.visible);
      Group => BasisUI;
    };
    GMOD.instancer instance_ham<NEx=869.,NEy=110.> {
      Value => (<-.visible_ham && <-.visible);
      Group => <-.HamiltonianUI;
    };
    GMOD.instancer instance_tol<NEx=869.,NEy=165.> {
      Value => (<-.visible_tol && <-.visible);
      Group => TolerancesUI;
    };
    GMOD.instancer instance_k<NEx=869.,NEy=220.> {
      Value => (<-.visible_k && <-.visible);
      Group => K_PointsUI;
    };
    GMOD.instancer instance_conv<NEx=869.,NEy=275.> {
      Value => (<-.visible_conv && <-.visible);
      Group => ConvergenceUI;
    };
    GMOD.instancer instance_print<NEx=869.,NEy=330.> {
      Value => (<-.visible_print && <-.visible);
      Group => PrintUI;
    };
    GMOD.instancer instance_job<NEx=869.,NEy=385.> {
      Value => (<-.visible_job && <-.visible);
      Group => JobUI;
    };
    GMOD.instancer instance_opt<NEx=869.,NEy=440.> {
      Value => (<-.visible_opt && <-.visible);
      Group => OptimizeUI;
    };
    GMOD.instancer instance_phon<NEx=869.,NEy=484.> {
      Value => (<-.visible_phon && <-.visible);
      Group => PhononUI;
    };
    GMOD.instancer instance_neb<NEx=869.,NEy=539.> {
      Value => (<-.visible_neb && <-.visible);
      Group => NebUI;
    };
    GMOD.instancer instance_cphf {
      Value => (<-.visible_cphf && <-.visible);
      Group => cphfUI;
    };
    GMOD.instancer instance_tddft {
      Value => (<-.visible_tddft && <-.visible);
      Group => tddftUI;
    };
    int visible_basis<NEportLevels=1,NEx=33.,NEy=572.> = 0;
    int visible_ham<NEportLevels=1,NEx=198.,NEy=572.> = 0;
    int visible_tol<NEportLevels=1,NEx=374.,NEy=572.> = 0;
    int visible_k<NEportLevels=1,NEx=539.,NEy=572.> = 0;
    int visible_conv<NEportLevels=1,NEx=704.,NEy=572.> = 0;
    int visible_print<NEportLevels=1,NEx=33.,NEy=627.> = 0;
    int visible_job<NEportLevels=1,NEx=198.,NEy=627.> = 0;
    int visible_opt<NEportLevels=1,NEx=374.,NEy=627.> = 0;
    int visible_phon<NEportLevels=1,NEx=539.,NEy=627.> = 0;
    int visible_neb<NEportLevels=1,NEx=704.,NEy=627.> = 0;
    int visible_cphf<NEportLevels=1> = 0;
    int visible_tddft<NEportLevels=1> = 0;
    CCP3.CRYSTAL.SCF.BasisUI BasisUI<NEx=55.,NEy=451.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => ((<-.visible == 1) && (<-.visible_basis == 1));
      Params => <-.SCFParams.Basis;
      version => <-.version;
      CCP3shell {
	parse_v {
	  v_commands = "visible_basis = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.CRYSTAL.SCF.HamiltonianUI HamiltonianUI<NEx=231.,NEy=451.,
                                                        instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => ((<-.visible == 1) && (<-.visible_ham == 1));
      Params => <-.SCFParams.Hamiltonian;
      version => <-.version;
      CCP3shell {
	parse_v {
	  v_commands = "visible_ham = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.CRYSTAL.SCF.TolerancesUI TolerancesUI<NEx=407.,NEy=451.,
                                                      instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => ((<-.visible == 1) && (<-.visible_tol == 1));
      Params => <-.SCFParams.Tolerances;
      version => <-.version;
      Eigenvalues {
	fmin => <-.<-.limits.min_converge;
	fmax => <-.<-.limits.max_converge;
      };
      Energy {
	fmin => <-.<-.limits.min_converge;
	fmax => <-.<-.limits.max_converge;
      };
      Density {
	fmin => <-.<-.limits.min_converge;
	fmax => <-.<-.limits.max_converge;
      };
      ITOL1 {
	fmin => <-.<-.limits.min_tola;
	fmax => <-.<-.limits.max_tola;
      };
      ITOL2 {
	fmin => <-.<-.limits.min_tola;
	fmax => <-.<-.limits.max_tola;
      };
      ITOL3 {
	fmin => <-.<-.limits.min_tola;
	fmax => <-.<-.limits.max_tola;
      };
      ITOL4 {
	fmin => <-.<-.limits.min_tola;
	fmax => <-.<-.limits.max_tola;
      };
      ITOL5 {
	fmin => <-.<-.limits.min_tolb;
	fmax => <-.<-.limits.max_tolb;
      };
      CCP3shell {
	parse_v {
	  v_commands = "visible_tol = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.CRYSTAL.SCF.K_PointsUI K_PointsUI<NEx=583.,NEy=451.,
                                                  instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => ((<-.visible == 1) && (<-.visible_k == 1) &&
		  (<-.<-.<-.model_type > 0));
      Params => <-.SCFParams.KPoints;
      is1 {
	min => <-.<-.limits.min_shrink;
	max => <-.<-.limits.max_shrink;
      };
      is2 {
	min => <-.<-.limits.min_shrink;
	max => <-.<-.limits.max_shrink;
      };
      is3 {
	min => <-.<-.limits.min_shrink;
	max => <-.<-.limits.max_shrink;
      };
      isp {
	min => <-.<-.limits.min_gilat;
	max => <-.<-.limits.max_gilat;
      };
      CCP3shell {
	parse_v {
	  v_commands = "visible_k = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.CRYSTAL.SCF.ConvergenceUI ConvergenceUI<NEx=55.,NEy=506.,
                                                        instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => ((<-.visible == 1) && (<-.visible_conv == 1));
      show_spin => (<-.SCFParams.Hamiltonian.Hoptions > 0);
      Params => <-.SCFParams.Convergence;
      version => <-.version;
      cycles {
	fmin => <-.<-.limits.min_cycles;
	fmax => <-.<-.limits.max_cycles;
      };
      shift_value {
	fmax => <-.<-.limits.max_shift;
      };
      CCP3shell {
	parse_v {
	  v_commands = "visible_conv = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.CRYSTAL.SCF.PrintUI PrintUI<NEx=407.,NEy=506.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => ((<-.visible == 1) && (<-.visible_print == 1));
      Params => <-.SCFParams.Print;
      exchange {
	visible => ((<-.<-.SCFParams.Hamiltonian.Htype == 0) && (<-.<-.SCFParams.Hamiltonian.Hoptions > 0));
      };
      CCP3shell {
	parse_v {
	  v_commands = "visible_print = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.CRYSTAL.SCF.JobUI JobUI<NEx=583.,NEy=506.,instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => ((<-.visible == 1) && (<-.visible_job == 1));
      Params => <-.SCFParams.Job;
      version => <-.version;
      CCP3shell {
	parse_v {
	  v_commands = "visible_job = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.CRYSTAL.SCF.OptimizeUI OptimizeUI<NEx=242.,NEy=506.,
                                                  instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => ((<-.visible == 1) && (<-.visible_opt == 1));
      Params => <-.SCFParams.Optimize;
      version => <-.version;
      etol {
	fmin => <-.<-.limits.min_etol;
	fmax => <-.<-.limits.max_etol;
      };
      xtol {
	fmin => <-.<-.limits.min_xtol;
	fmax => <-.<-.limits.max_xtol;
      };
      CCP3shell {
	parse_v {
	  v_commands = "visible_opt = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.CRYSTAL.SCF.PhononUI PhononUI<NEx=720.,NEy=506.,
                                              instanced=0> {
      model_type => <-.<-.<-.model_type;
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => ((<-.visible == 1) && (<-.visible_phon == 1));
      params => <-.SCFParams.phonon;
      version => <-.version;
      CCP3shell {
	parse_v {
	  v_commands = "visible_phon = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.CRYSTAL.SCF.NebUI NebUI<instanced=0> {
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => ((<-.visible == 1) && (<-.visible_neb == 1));
      params => <-.SCFParams.neb;
      scfparams => <-.SCFParams;
      job_data => <-.job_data;
      CCP3shell {
	parse_v {
	  v_commands = "visible_neb = 0; NebEndStrUI.DLVdialog.ok = 0;";
	  relative => <-.<-.<-;
	};
	CCP3.CRYSTAL.Modules.nebwrite91 nebwrite91<NEx=639.,NEy=191.>{
	    trigger => <-.close.do;
	    make91 => <-.<-.params.make91;
	    data => <-.<-.scfparams;
	    job_data => <-.<-.job_data;
	};
      };
    };
    CCP3.CRYSTAL.SCF.cphfUI cphfUI<instanced=0> {
      //model_type => <-.<-.<-.model_type;
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => ((<-.visible == 1) && (<-.visible_cphf == 1));
      Params => <-.SCFParams.cphf;
      //version => <-.version;
      CCP3shell {
	parse_v {
	  v_commands = "visible_cphf = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.CRYSTAL.SCF.tddftUI tddftUI<instanced=0> {
      //model_type => <-.<-.<-.model_type;
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      visible => ((<-.visible == 1) && (<-.visible_tddft == 1));
      Params => <-.SCFParams.tddft;
      //version => <-.version;
      CCP3shell {
	parse_v {
	  v_commands = "visible_tddft = 0;";
	  relative => <-.<-.<-;
	};
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle wavefn<NEx=44.,NEy=341.> {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      y => ((Phonon.y + Phonon.height) + 5);
      label = "Automatically Analyse Wavefunction";
      set => <-.SCFParams.analyse;
      visible = 0; // Todo
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle user_job {
      parent => <-.UIpanel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      width => parent.clientWidth;
      label = "I want to run this job without DLV";
      y => ((wavefn.y + wavefn.height) + 10);
      set => <-.SCFParams.user_calc;
    };
    CCP3.Core_Macros.UI.UIobjs.DirSave FileB {
      preferences => <-.prefs;
      filename => <-.SCFParams.user_dir;
      pattern = "*";
      title = "Run SCF Job in Dir";
      parent => <-.UIpanel;
      y => ((user_job.y + user_job.height) + 5);
      UIpanel {
	visible => <-.<-.SCFParams.user_calc;
      };
    };
  };
  CCP3.CRYSTAL.Base.calcUIBase SCF_calc {
    UIcmd<NEx=11.,NEy=209.> {
      label = "Single Point Energy";
      active => (<-.active_menus && (<-.model_type >= 0));
      do = 0;
    };
    UIcmdList SubMenu<NEx=198.,NEy=143.> {
      label = "SCF Calculation";
      cmdList => switch(version+1, { UIcmd }, { UIcmd, UIopt },
	  { UIcmd, UIopt, UIphonon},
	{ UIcmd, UIopt, UIphonon, UIcphf },
	{ UIcmd, UIopt, UIphonon, UIneb, UIcphf },
	{ UIcmd, UIopt, UIphonon, UIneb, UIcphf },
	{ UIcmd, UIopt, UIphonon, UIneb, UIcphf, UItddft } );
    };
    UIcmd UIopt<NEx=176.,NEy=209.> {
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "Structure Optimisation";
      active => (<-.active_menus && (<-.model_type >= 0));
      do = 0;
    };
    UIcmd UIphonon<NEx=341.,NEy=209.> {
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "Phonons";
      active => (<-.active_menus && (<-.model_type >= 0));
      do = 0;
    };
    UIcmd UIneb<NEx=506.,NEy=209.> {
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "CI-NEB Transition State";
      active => (<-.active_menus && (<-.model_type >= 0));
      do = 0;
    };
    UIcmd UIcphf<NEx=693.,NEy=225.> {
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "CPHF/KS";
      active => (<-.active_menus && (<-.model_type >= 0));
      do = 0;
    };
    UIcmd UItddft {
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label = "TD-DFT";
      active => (<-.active_menus && (<-.model_type >= 0));
      do = 0;
    };
    GMOD.parse_v do_energy<NEx=11.,NEy=330.> {
      v_commands = "SCFParams.task = 0; visible = 1;";
      trigger => <-.UIcmd.do;
      on_inst = 0;
      relative => <-;
    };
    GMOD.parse_v do_opt<NEx=165.,NEy=330.> {
      v_commands = "SCFParams.task = 1; visible = 1;";
      trigger => <-.UIopt.do;
      on_inst = 0;
      relative => <-;
    };
    GMOD.parse_v do_phonon<NEx=341.,NEy=330.> {
      v_commands = "SCFParams.task = 2; visible = 1;";
      trigger => <-.UIphonon.do;
      on_inst = 0;
      relative => <-;
    };
    GMOD.parse_v do_neb<NEx=506.,NEy=330.> {
      v_commands = "SCFParams.task = 3; visible = 1;";
      trigger => <-.UIneb.do;
      on_inst = 0;
      relative => <-;
    };
    GMOD.parse_v do_cphf<NEx=684.,NEy=333.> {
      v_commands = "SCFParams.task = 4; visible = 1;";
      trigger => <-.UIcphf.do;
      on_inst = 0;
      relative => <-;
    };
    GMOD.parse_v do_tddft<NEx=684.,NEy=333.> {
      v_commands = "SCFParams.task = 5; visible = 1;";
      trigger => <-.UItddft.do;
      on_inst = 0;
      relative => <-;
    };
    link version<NEportLevels={2,1},NEx=803.,NEy=110.>;
    link model_type<NEportLevels={2,1},NEx=814.,NEy=55.>;
    link use_mpp<NEportLevels={2,1},NEx=594.,NEy=187.>;
    link job_data<NEx=803.,NEy=187.,NEportLevels={2,1}>;
    CCP3.CRYSTAL.SCF.scfUI scfUI<NEx=495.,NEy=374.,instanced=0> {
      visible => <-.visible;
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      SCFParams => <-.SCFParams;
      version => <-.version;
      job_data => <-.job_data;
      parse_v {
	v_commands = "scfUI.visible_ham = 0; scfUI.visible_basis = 0; "
	  + "scfUI.visible_tol = 0; scfUI.visible_k = 0; "
	  + "scfUI.visible_conv = 0; scfUI.visible_job = 0; "
	  + "scfUI.visible_opt = 0; scfUI.visible_print = 0; "
	  + "scfUI.visible_phon = 0; scfUI.visible_neb = 0; "
	  + "scfUI.visible_cphf = 0; "
	  + "SCFParams.Hamiltonian.Hoptions = scfUI.select_spin; "
	  + "SCFParams.Hamiltonian.use_spins = scfUI.model_has_spin; "
	  + "SCFParams.neb.end_model = -1; SCFParams.neb.first_model = -1; "
          + "SCFParams.neb.images_button = 0; SCFParams.neb.finalstr_button = 0;"
          + "SCFParams.neb.make91 = 0; SCFParams.neb.restart = 0;"
          + "SCFParams.neb.restart_file = ;";
	relative => <-.<-;
      };
    };
    instancer {
      Group => scfUI;
    };
    CCP3.CRYSTAL.Modules.SCFParams SCFParams<NEportLevels={0,1}> {
      task = 0;
      analyse = 0; // Todo
      Hamiltonian {
	Htype = 0;
	Hoptions = 0;
	use_spins = 0;
	functional = 0;
	correlation = 0;
	exchange = 0;
	dft_grids = 0;
	aux_basis = 2;
	hybrid = 0;
	mixing = 100;
      };
      Basis {
	basis = 0;
      };
      KPoints {
	asym = 0;
	is1 = 8;
	is2 = 8;
	is3 = 8;
	isp = 16;
      };
      Tolerances {
	itol1 = 7;
	itol2 = 7;
	itol3 = 7;
	itol4 = 7;
	itol5 = 14;
	method = 0;
	eigenval = 6;
	energy = 7;
	deltap = 6;
      };
      Convergence {
	method = 0;
	mixing = 30;
	maxcycles = 50;
	use_levshift = 0;
	levshift = 10;
	lock_levshift = 1;
	use_smearing = 0;
	fermi_smear = 0.02;
	spinlock = 0;
	spin = 0;
	spin_cycles = 10;
      };
      Optimize {
	optimiser = 0;
	opttype = 0;
	etol = 7;
	xtol = 0.0012;
	gtol = 0.00045;	
	iscale = 5.0;
	dlf_type = 0;
	dlf_sstype = 0;
	dlf_initpop = 100;
	dlf_pop = 25;
	dlf_radius = 1.0;
	dlf_minradius = 0.000001;
	dlf_contractradius = 0.9;
	dlf_cycles = 100;
	dlf_nsaves = 1;
	dlf_mutation = 0.9;
	dlf_death = 0.9;
	dlf_scalef = 1.0;
	dlf_reset = 500;
      };
      Print {
	calc_mulliken = 0;
	calc_exchange = 0;
      };
      Job {
	direct = 1;
	mondirect = 1;
	deltap = 0;
	dp_tol = 6;
	biesplit = 1;
	monsplit = 1;
	gradcalc = 0;
	restart = 0;
	fock_file = "";
      };
      phonon {
	step = 0.005;
	deriv = 1;
	intensity = 0;
	dispersion = 0;
	cell = { {1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
      };
      neb {
	nimages = 3;
	iters = 100;
	climb = 10;
	start_ene = 0.0;
	final_ene = 0.0;
	separation_int = 0;
	optimiser_int = 0;
	convergence_int = 0;
	tol_energy = 0.0001;
	tol_step = 0.0018;
	tol_grad = 0.00045;
	symmpath = 0;
	usestrsym = 0;
	adapt = 0;
	restart = 0;
	restart_file = ""; 
	end_model = -1;
	first_model = -1;
	natoms = 0;
	dist= 0.0;
	make91 = 0;
	images_dir = "";
	change_nim = 0;
	finalstr_button = 0;
	images_button=0;
	nocalc = 0;
      };
      cphf {
	mixing = 0;
	cycles = 100;
	alpha = 4;
	udik = 6;
      };
      tddft {
	do_scf = 1;
	tamm_dancoff = 0;
	calc_jdos = 1;
	calc_pes = 1;
	fastresponse = 1;
	diag = 0;
	davidson_state = 1;
	davidson_niters = 100;
	davidson_space = 100;
	davidson_conv = 1e-6;
      };
      user_calc = 0;
      user_dir = "";
    };
    CCP3.CRYSTAL.Modules.Run_SCF Run_SCF {
      make => <-.visible;
      run => <-.scfUI.UItemplateDialog.ok;
      cancel => <-.scfUI.UItemplateDialog.cancel;
      inherit => <-.scfUI.BasisUI.inherit.do;
      data => <-.SCFParams;
      file => <-.scfUI.BasisUI.filename;
      use_default => <-.scfUI.BasisUI.set_default;
      version => <-.version;
      job_data => <-.job_data;
      use_mpp => <-.use_mpp;
    };
  };
};
