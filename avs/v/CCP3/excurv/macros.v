
flibrary Macros<NEeditable=1,
#ifndef DLV_NO_DLLS
		dyn_libs="libCCP3excurv",
#endif // DLV_NO_DLLS
		export_cxx=1,
		build_dir="avs/src/express",
		cxx_hdr_files="avs/src/express/calcs.hxx avs/src/core/env_gen.hxx avs/src/express/model.hxx avs/src/excurv/end_gen.hxx avs/src/excurv/expt_gen.hxx avs/src/excurv/pot_gen.hxx avs/src/excurv/plot_gen.hxx avs/src/excurv/save_gen.hxx avs/src/excurv/ref_gen.hxx avs/src/excurv/list_gen.hxx avs/src/excurv/kw_gen.hxx avs/src/excurv/ek_gen.hxx avs/src/excurv/ms_gen.hxx avs/src/excurv/err_gen.hxx avs/src/excurv/info_gen.hxx avs/src/excurv/make_gen.hxx avs/src/excurv/str_gen.hxx",
		out_hdr_file="excurv.hxx",
		out_src_file="excurv.cxx"> {
  // Import Stanko's code from DLV v2.5
  macro file_locator<NEx=264.,NEy=341.> {
    //BGS
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=330.,NEy=33.>;
    //BGS - Todo use prefs
    GMOD.copy_on_change copy_on_change<NEx=110.,NEy=33.> {
      on_inst = 0;
    };
    UItemplateDialog Read_Experimental_Data<NEx=33.,NEy=77.> {
      title => "Read Experimental Data";
      width = 408;
      height = 343;
      visible => <-.copy_on_change.output;
      ok = 0;
      cancel = 0;
      okButton => is_valid(<-.expt_data.file_name);
      cancelButton = 1;
    };
    UIpanel UIpanel {
      parent => <-.Read_Experimental_Data;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    UIlabel file_label<NEx=858.,NEy=88.> {
      parent => <-.UIpanel;
      label => "File Name";
      y = 5;
      x => ((parent.clientWidth - .width) / 2);
    };
    //BGS - what else?
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=506.,NEy=242.> {
      parent => <-.UIpanel;
      x = 0;
      y => (<-.file_label.height + 5);
      pattern = "*";
      filename => <-.expt_data.file_name;
      title = "EXAFS data file";
    };
    /*UItext file_text<NEx=506.,NEy=242.> {
      parent => <-.UIpanel;
      text => <-.expt_data.file_name;
      y => (<-.file_label.height + 5);
      x = 5;
      width => ((parent.clientWidth - <-.UIbutton.width) - 15);
      updateMode = 7;
    };
    UIbutton UIbutton<NEx=528.,NEy=22.> {
      parent => <-.UIpanel;
      label => "Browse";
      y => ((<-.file_label.y + <-.file_label.height) + 4);
      x => ((parent.clientWidth - width) - 5);
    };
    UIfileDialog read_file_location<NEx=638.,NEy=99.> {
      visible => <-.UIbutton.do;
      filename => <-.expt_data.file_name;
      cancel = 0;
      ok = 0;
      okButton = 1;
      cancelButton = 1;
      searchPattern = "*";
      };*/
    //BGS
    UIfieldTypein Samp_freq<NEx=847.,NEy=462.> {
      UIparent => <-.UIpanel;
      flabel => "Sampling Frequency";
      fval => <-.expt_data.sampling_frequency;
      fmin = 1;
      x = 5;
      label {
	width => (3 * <-.panel.width / 4);
	alignment = "left";
      };
      field {
	x => ((<-.label.width) + 5);
	width => ((<-.panel.width - <-.label.width) - 10);
	mode = "integer";
	updateMode = 7;
      };
      //y => ((<-.file_text.y + <-.file_text.height) + 5);
      y => ((<-.FileB.y + <-.FileB.height) + 5);
    };
    UIradioBoxLabel UIradioBoxLabel<NEx=297.,NEy=242.> {
      parent => <-.UIpanel;
      labels => <-.string;
      selectedItem => <-.expt_data.selected_button;
      title => "File Format";
      UIlabel {
	x = 5;
	alignment = "left";
      };
      y => ((<-.Samp_freq.y + <-.Samp_freq.height) + 5);
    };
    string string<NEportLevels=1,NEx=913.>[] = {
      "ecabs/exbrook (12)",
      "exback/exspline (32)",
      "Other"
    };
    UIfieldTypein UIfieldTypein<NEx=209.,NEy=308.> {
      UIparent => <-.UIpanel;
      flabel => "X Column";
      fval => <-.expt_data.x_column;
      fmin = 1;
      fmax = 9;
      panel {
	visible => (<-.<-.expt_data.selected_button == 2);
      };
      field {
	mode = "integer";
	updateMode = 7;
      };
      x = 5;
      y => ((<-.UIradioBoxLabel.y + UIradioBoxLabel.height) + 5);
      width => ((UIparent.clientWidth / 2) - 10);
    };
    UIfieldTypein UIfieldTypein#1<NEx=55.,NEy=506.> {
      UIparent => <-.UIpanel;
      flabel => "Y Column";
      fval => <-.expt_data.y_column;
      fmin = 1;
      fmax = 9;
      panel {
	visible => (<-.<-.expt_data.selected_button == 2);
      };
      field {
	mode = "integer";
	updateMode = 7;
      };
      x => ((UIparent.clientWidth / 2) + 5);
      y => <-.UIfieldTypein.y;
      width => ((UIparent.clientWidth / 2) - 10);
    };
    CCP3.EXCURV.Modules.expt_data_type expt_data<NEx=198.,NEy=110.,NEportLevels={0,2}> {
      file_name = "";
      sampling_frequency = 1;
      selected_button = 0;
      x_column = 1;
      y_column = 2;
    };
  };
  macro EXCURV_main<NEx=121.,NEy=132.> {
    //BGS
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=561.,NEy=55.>;
    link UIparent<NEportLevels={2,1},NEx=352.,NEy=55.>;
    //BGS - Todo use prefs?
    CCP3.Version2.Macros.Misc.CCP3shell CCP3shell<NEx=33.,NEy=11.> {
      UIshell {
	title => "DL_EXCURV";
	width = 600;
	height = 740;
	cancel = 0;
	visible => <-.shell_vis.output;
	parent => <-.<-.UIparent;
      };
      int close_check => (close.do | UIshell.cancel);
      GMOD.copy_on_change copy_on_change {
	input => <-.close_check;
	on_inst = 0;
	output = 0;
      };
      UIquestionDialog UIquestionDialog<NEx=231.,NEy=242.> {
	visible => <-.copy_on_change.output;
	message = "Do you really want to terminate EXCURV?";
	ok<NEportLevels={1,2}> = 0;
	cancel = 0;
	okButton = 1;
	cancelButton = 1;
	title = "Close EXCURV calculation";
	parent => <-.UIshell;
      };
      GMOD.parse_v parse_cancel {
	v_commands = "UIshell.visible = 1;";
	trigger => <-.UIquestionDialog.cancel;
	on_inst = 0;
	relative => <-;
      };
      parse_v {
	trigger => <-.UIquestionDialog.ok;
      };
      GMOD.copy_on_change shell_vis<NEx=176.,NEy=33.>;
    };
    UIlabel UIlabel<NEx=473.,NEy=11.> {
      parent => <-.CCP3shell.UIpanel;
      label => "DL_EXCURV User Interface. Set up the number of atoms and the number of shells in your system.";
      y = 0;
      height => 30;
      width => parent.clientWidth;
    };
    UIbutton Kinematic_button<NEx=209.,NEy=396.> {
      visible => <-.int;
      parent => <-.CCP3shell.UIpanel;
      label => "Refine";
      x => ((.parent.clientWidth - .width) - 10);
      width = 150;
      y => <-.Potential_button.y;
    };
    /*UIbutton Dynamic_button<NEx=594.,NEy=550.> {
      visible => <-.int;
      parent => <-.CCP3shell.UIpanel;
      label => "Dynamic Refinement";
      x => ((.parent.clientWidth - .width) - 10);
      width = 150;
      y => <-.Potential_button.y;
      };*/
    int int<NEportLevels=1,NEx=110.,NEy=550.>;
    UIbutton Potential_button<NEx=33.,NEy=308.> {
      parent => <-.CCP3shell.UIpanel;
      do = 0;
      y => (.parent.clientHeight - 34);
      x = 10;
      width = 150;
      label = "Generate Phase Shifts";
      //visible => (<-.emitter_atom != 0);
    };
    UIbutton Read_Expt_Data<NEx=726.,NEy=132.,NEportLevels={0,1}> {
      parent => <-.CCP3shell.UIpanel;
      label => "Read Experimental Data";
      x = 5;
      //y => ((<-.Edge_Box_Selector.y + <-.Edge_Box_Selector.height) + 10);
      y => ((<-.Central_Atom_Type.y + <-.Central_Atom_Type.height) + 20);
      width = 160;
    };
    UIradioBoxLabel UIradioBoxLabel_Model<NEx=616.,NEy=605.> {
      parent => <-.CCP3shell.UIpanel;
      //labels => <-.string#2;
      //selectedItem => <-.<-.model_source;
      title => "Select Input Model";
      UIlabel {
	alignment = "left";
      };
      x = 5;
      y => ((<-.UIlabel.y + <-.UIlabel.height) + 10);
    };
    string shell_model[] = {
      "Create Shell Model"
    };
    string model_options<NEportLevels={1,1},NEx=715.,NEy=682.>[] = {
      "Create Shell model", "Import Cluster Model"
    };
    UIlabel Central_Atom_Labele<NEx=462.,NEy=143.> {
      parent => <-.CCP3shell.UIpanel;
      label => "Central Atom";
      x => <-.Central_Atom_Type.x;
      y => ((<-.UIradioBoxLabel_Model.y +
	     <-.UIradioBoxLabel_Model.height) + 10);
      //y => ((<-.UIlabel.y + <-.UIlabel.height) + 10);
      width => (<-.Central_Atom_Type.width);
      alignment = "left";
    };
    int edge = 0;
    UIradioBoxLabel Edge_Box_Selector<NEx=825.,NEy=462.> {
      parent => <-.CCP3shell.UIpanel;
      labels => <-.string;
      selectedItem = <-.edge;
      title => "Select Edge";
      UIlabel {
	alignment = "left";
      };
      UIradioBox {
	y => ((<-.UIlabel.y + <-.UIlabel.height) + 10);
	height => <-.<-.Central_Atom_Type.height;
      };
      width = 75;
      x => ((<-.Central_Atom_Type.x + <-.Central_Atom_Type.width) + 20);
      y => <-.Central_Atom_Labele.y;
    };
    string Kedge[] = {
      "K"
    };
    string K_to_L1[] = {
      "K", "L1"
    };
    string K_to_L3[] = {
      "K", "L1", "L2", "L3"
    };
    string K_to_M1[] = {
      "K", "L1", "L2", "L3", "M1"
    };
    string K_to_M3[] = {
      "K", "L1", "L2", "L3", "M1", "M2", "M3"
    };
    string all_edges[] = {
      "K","L1","L2","L3","M1","M2","M3","M4","M5"
    };
    string string<NEportLevels=1,NEx=825.,NEy=539.>[] =>
      switch ((emitter_atom > 2) + 1, Kedge,
	(switch ((emitter_atom > 4) + 1, K_to_L1,
	  (switch ((emitter_atom > 10) + 1, K_to_L3,
	    (switch ((emitter_atom > 12) + 1, K_to_M1,
	      switch ((emitter_atom > 20) + 1, K_to_M3, all_edges))))))));
    UIlist Central_Atom_Type<NEx=264.,NEy=187.> {
      parent => <-.CCP3shell.UIpanel;
      //height => <-.Edge_Box_Selector.UIradioBox.height;
      x = 5;
      y => ((<-.Central_Atom_Labele.y + <-.Central_Atom_Labele.height) + 10);
#ifdef MSDOS
      height = 144;
#else
      height = 216;
#endif // MSDOS
      width = 90;
      selectedItem => <-.emitter_atom;
    };
    int emitter_atom<NEportLevels=1,NEx=77.,NEy=187.> = 0;
    int k_weight = 3;
    UIradioBoxLabel UIradioBoxLabel_weight<NEx=363.,NEy=594.> {
      parent => <-.CCP3shell.UIpanel;
      labels => <-.string#1;
      title = "Set Weighting";
      selectedItem => <-.k_weight;
      UIradioBox {
         y => ((<-.UIlabel.y + <-.UIlabel.height) + 10);
         x => <-.UIlabel.x;
      };
      UIlabel {
         alignment = "left";
      };
      //visible => (<-.emitter_atom != 0);
      width = 130;
      x => ((<-.Edge_Box_Selector.x + <-.Edge_Box_Selector.width) + 20);
      y => <-.Central_Atom_Labele.y;
    };
    string string#1<NEportLevels={1,1},NEx=385.,NEy=649.>[] = {
      "None","k","k^2","k^3"
    };
    float emin = 3.0;
    float emax = 3000.0;
    float kmin = 0.8874;
    float kmax = 28.060;
    UIfieldTypein UIfieldTypein_emin<NEx=506.,NEy=528.> {
      UIparent => <-.CCP3shell.UIpanel;
      flabel => "Set Emin";
      fmin => 3.0;
      fmax => <-.emax;
      fval => <-.emin;
      label {
	alignment = "left";
      };
      x => <-.Read_Expt_Data.x;
      y => ((<-.Read_Expt_Data.y + <-.Read_Expt_Data.height) + 20);
    };
    UIfieldTypein UIfieldTypein_emax<NEx=528.,NEy=484.> {
      UIparent => <-.CCP3shell.UIpanel;
      flabel => "Set Emax";
      fmin => <-.emin;
      fmax => 3000.0;
      fval => <-.emax;
      label {
	alignment = "left";
      };
      x => <-.UIfieldTypein_emin.x;
      y => ((<-.UIfieldTypein_emin.y + <-.UIfieldTypein_emin.height) + 10);
    };
    UIfieldTypein UIfieldTypein_kmin<NEx=561.,NEy=440.> {
      UIparent => <-.CCP3shell.UIpanel;
      flabel => "Set k min";
      fmin => 0.8874;
      fmax => <-.kmax;
      fval => <-.kmin;
      label {
	alignment = "left";
      };
      x => <-.UIfieldTypein_emax.x;
      y => ((<-.UIfieldTypein_emax.y + <-.UIfieldTypein_emax.height) + 10);
    };
    UIfieldTypein UIfieldTypein_kmax<NEx=594.,NEy=385.> {
      UIparent => <-.CCP3shell.UIpanel;
      flabel => "Set k max";
      fmin => <-.kmin;
      fmax => 28.060;
      fval => <-.kmax;
      label {
	alignment = "left";
      };
      x => <-.UIfieldTypein_kmin.x;
      y => ((<-.UIfieldTypein_kmin.y + <-.UIfieldTypein_kmin.height) + 10);
    };
    // BGS
    CCP3.Core_Macros.UI.UIobjs.FileSave FileB<NEx=77.,NEy=495.> {
      parent => <-.CCP3shell.UIpanel;
      x => ((.parent.clientWidth - .width) / 2);
      y => <-.Potential_button.y;
      pattern = "*";
      file_write = 1;
      title = "Save Parameters";
      label = "Save Parameters";
    };
    /*UIbutton UIbutton_Save_Par<NEx=77.,NEy=495.> {
      parent => <-.CCP3shell.UIpanel;
      label = "Save Parameters";
      x => ((.parent.clientWidth - .width) / 2);
      //y => ((<-.UIfieldTypein_kmax.y + <-.UIfieldTypein_kmax.height) + 20);
      y => <-.Potential_button.y;
      width = 150;
    };
    UIfileDialog UIfileDialog_Save_Par<NEx=0.,NEy=627.> {
      visible => <-.UIbutton_Save_Par.do;
      title = "Save Parameters";
      ok = 0;
      cancel = 0;
      okButton = 1;
      cancelButton = 1;
      confirmFileWrite = 1;
      };*/
    //BGS
  };
  macro plot_spectra<NEx=385.,NEy=407.> {
    //BGS
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=594.,NEy=110.>;
    //BGS - Todo, use prefs?
    UIbutton Plt_Data<NEx=583.,NEy=462.> {
      x => ((<-.Print_Frame.width / 2) - (width / 2));
      width = 130;
      y => ((parent.clientHeight - height) - 40);
      parent => <-.Print_Frame;
      label => "Plot Data";
    };
    UIframe Print_Frame<NEx=99.,NEy=209.> {
      parent<NEportLevels={3,0}>;
      x => ((parent.clientWidth - .width) - 5);
      width => ((parent.clientWidth / 2.5) - 5);
      //height<NEdisplayMode="open"> => ((<-.<-.Potential_button.y - 5) - .y);
    };
    UIlabel Title<NEx=473.,NEy=44.> {
      parent => <-.Print_Frame;
      label => "Plot Data";
      x = 0;
      y = 5;
      width => parent.clientWidth;
    };
    UIradioBoxLabel xaxis_choice<NEx=33.,NEy=462.> {
      parent => <-.Print_Frame;
      labels => <-.string;
      selectedItem => <-.int#1;
      title => "X Axis";
      UIlabel<NEx=198.,NEy=517.> {
	x = 0;
	alignment = "center";
	fontAttributes {
#ifdef MSDOS
	  weight = "bold";
#else
	  height = 16;
#endif // MSDOS
	};
      };
      y => ((<-.Title.y + <-.Title.height) + 5);
      width => parent.clientWidth;
    };
    string string<NEportLevels=1,NEx=44.,NEy=407.>[] = {
      "Energy",
      "Wave Number",
      "Distance"
    };
    UIlabel yaxis_choice<NEx=165.,NEy=33.> {
      parent => <-.Print_Frame;
      label => "Y Axis";
      x = 0;
      y => ((<-.xaxis_choice.y + <-.xaxis_choice.height) + 10);
      alignment = "center";
      fontAttributes {
#ifdef MSDOS
	weight = "bold";
#else
	height = 16;
#endif // MSDOS
      };
      width => parent.clientWidth;
    };
    UItoggle UItoggle<NEx=341.,NEy=154.>[array_size(string#1)] {
      parent => <-.Print_Frame;
      label => <-.string#1[index_of(<-.UItoggle)];
#ifdef SOLARIS
      height => 29; // default 24 +5
      y => (<-.int#2 + ((height) * index_of(<-.UItoggle)));
#else
      y => (<-.int#2 + ((height + 5) * index_of(<-.UItoggle)));
#endif // SOLARIS
      active => <-.int[<-.int#1][index_of(<-.UItoggle)];
      x = 5;
      width => parent.clientWidth;
      set => <-.selections[index_of(<-.UItoggle)];
    };
    string string#1<NEportLevels=1,NEx=363.,NEy=264.>[] = {
      "EXAFS(experiment)",
      "EXAFS(theory)",
      "EXAFS(experiment)*k^n",
      "EXAFS(theory)*k^n",
      "Fourier Transform (experiment)",
      "Fourier Transform (theory)",
      "Sine Transform (experiment)",
      "Sine Transform (theory)",
      "Backscattering Magnitude",
      "Backscattering Phase",
      "Phase of Backscattering Factor",
      "EXAFS (difference)"
    };
    int int<NEportLevels=1,NEx=550.,NEy=264.>[array_size(string)][array_size(string#1)] = {
      { 1,1,0,0,0,0,0,0,0,0,0,0 },
      { 0,0,1,1,0,0,0,0,0,0,0,0 },
      { 0,0,0,0,1,1,0,0,0,0,0,0 }
    };
    int int#1<NEportLevels=1,NEx=451.,NEy=440.> = 1;
    int int#2<NEportLevels=1,NEx=242.,NEy=462.> =>
      ((yaxis_choice.y + yaxis_choice.height) + 5);
    int selections[] = { 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0 };
    int bool_select[] => (int[int#1] & selections);
    //BGS
    CCP3.Core_Macros.UI.UIobjs.FileSave FileB<NEx=528.,NEy=550.> {
      parent => <-.Print_Frame;
      pattern = "*";
      filename => (getenv("PWD") + "/exout.dat");
      file_write = 1;
      title = "Save Spectra";
      label = "Save Spectra";
      x => ((<-.Print_Frame.width / 2) - (width / 2));
      y => ((<-.Plt_Data.y + <-.Plt_Data.height) + 10);
    };
    /*UIbutton UIbutton_Print<NEx=528.,NEy=550.> {
      parent => <-.Print_Frame;
      label = "Save Spectra";
      x => ((<-.Print_Frame.width / 2) - (width / 2));
      y => ((<-.Plt_Data.y + <-.Plt_Data.height) + 10);
      width = 130;
    };
    UIfileDialog UIfileDialog_Print2file<NEx=286.,NEy=561.> {
      visible => <-.UIbutton_Print.do;
      title = "Save Spectra";
      //#ifndef MSDOS // Todo - possible problems on Windows
      filename => (getenv("PWD") + "/exout.dat");
      //#else
      //filename = "exout.dat";
      //#endif // MSDOS
      ok = 0;
      cancel = 0;
      okButton = 1;
      cancelButton = 1;
      confirmFileWrite = 1;
      };*/
    //BGS
  };
  macro Potential_panel {
    //BGS
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=264.,NEy=44.>;
    link UIparent<NEportLevels={2,1},NEx=440.,NEy=44.>;
    //BGS - Todo, use prefs
    GMOD.copy_on_change copy_on_change<NEx=88.,NEy=33.> {
      on_inst = 0;
    };
    UItemplateDialog Potential_generation<NEx=33.,NEy=70.> {
      visible => <-.copy_on_change.output;
      ok<NEportLevels={1,3}> = 0;
      cancel = 0;
      width = 500;
      height = 600;
      okButton => ((is_valid(<-.data.central_neighb) &
		    is_valid(<-.atom_types)) & is_valid(<-.neighbours));
      cancelButton = 1;
      parent => <-.UIparent;
    };
    UIpanel panel {
      parent => <-.Potential_generation;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    UIframe UIframe<NEx=198.,NEy=99.> {
      parent => <-.panel;
      y => ((<-.UItext.y + <-.UItext.height) + 5);
      width => .parent.clientWidth;
      height => (.parent.clientHeight - .y);
    };
    /*UIlabel UIlabel<NEx=176.,NEy=-55.> {
      parent => <-.panel;
      label => "DL_EXCURV Potential Generation";
      y = 5;
      width => parent.clientWidth;
      alignment = "center";
      };*/
    macro input_atom_number<NEx=385.,NEy=99.> {
      UIfieldTypein UIfieldTypein<NEx=275.,NEy=407.> {
	UIparent<NEportLevels={3,1}> => <-.<-.UIframe;
	flabel => <-.string;
	fval => <-.<-.data.no_atom_types;
	fmin => <-.int#1;
	fmax => <-.int#2;
	x = 5;
	width => UIparent.clientWidth;
	y => ((<-.<-.UItoggle_set_V.y + <-.<-.UItoggle_set_V.height) + 15);
	label {
	  y => 5;
	  alignment = "left";
	};
	field {
	  x => (<-.<-.<-.UIlist_CA_neigh.x - <-.x);
	  //x => (<-.label.x + <-.label.width);
	  width => 80;
	  y = 5;
	  mode = "integer";
	  updateMode = 7;
	};
      };
      string string<NEportLevels={1,1},NEx=11.,NEy=110.> =
					 "Number of Other Atom Types";
      int int#1<NEportLevels=1,NEx=385.,NEy=33.> = 0;
      int int#2<NEportLevels=1,NEx=583.,NEy=242.> = 10;
    };
    UIlabel Atom_Type_Lable<NEx=517.,NEy=209.> {
      parent => <-.UIframe;
      label => <-.string;
      x = 5;
      y = 15;
      width => (parent.clientWidth / 1.5);
      alignment = "left";
    };
    string string<NEportLevels=1,NEx=275.,NEy=242.>;
    string string#1<NEportLevels=1,NEx=275.,NEy=187.>[];
    UIradioBoxLabel Pot_Method_Calc<NEx=748.,NEy=154.> {
      parent => <-.UIframe;
      labels => <-.string#2;
      selectedItem => <-.data.method;
      title => "Select Method for Potential Calculations";
      UIlabel {
	alignment = "left";
      };
      label_cmd {
	width => parent.clientWidth;
	x = 5;
	y => ((<-.UIlist_CA_neigh.y + <-.UIlist_CA_neigh.height) + 15);
      };
    };
    string string#2<NEportLevels=1,NEx=616.,NEy=44.>[] = {
      "No Corrections",
      "1s Core Hole (K-edge) (relaxed approximation)",
      "1s Core Hole (K-edge) (Z = Z+1 approximation)"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVscrolledWindow UIscrolledWindow_pot<NEx=682.,
      NEy=209.> {
      parent => <-.UIframe;
      x = 0;
      y => (<-.input_atom_number.UIfieldTypein.y
	    + <-.input_atom_number.UIfieldTypein.height);
      width => parent.clientWidth;
      height => (parent.clientHeight - .y);
#ifdef MSDOS
      virtualWidth = 470;
      virtualHeight => (80 * <-.data.no_atom_types) + 25;
#endif // MSDOS
    };
    int atom_types[data.no_atom_types] => data.atom_pairs.atom_type;
    int neighbours[data.no_atom_types] => data.atom_pairs.neighbour;
    macro macro<NEx=638.,NEy=286.>[data.no_atom_types] {
      UIlist UIlist<NEx=308.,NEy=187.> {
	parent => <-.parent;
	x => <-.<-.UIlabel_type.x;
	y => <-.UIlist#1.y;
	selectedItem => <-.<-.data.atom_pairs[index_of(<-.<-.macro)].atom_type;
      };
      link parent<NEportLevels={2,1},NEx=132.,NEy=88.> =>
				       <-.UIscrolledWindow_pot;
      UIlist UIlist#1<NEx=473.,NEy=187.> {
	parent => <-.parent;
	strings => <-.<-.string#4;
	x => <-.<-.UIlabel_neigh.x;
	y => (<-.<-.y + ((height + 10) * index_of(<-.<-.macro)));
	selectedItem => <-.<-.data.atom_pairs[index_of(<-.<-.macro)].neighbour;
      };
      UIlabel UIlabel<NEx=55.,NEy=220.> {
	parent => <-.parent;
	label => <-.string;
	x => <-.<-.UIlabel_atom.x;
	y => <-.UIlist#1.y;
	alignment = "left";
      };
      int index<NEportLevels=1,NEx=66.,NEy=319.> => (index_of(<-.macro) + 1);
      string string<NEx=66.,NEy=374.> => index;
      string atom_symbol;
    };
    UIlabel UIlabel_atom<NEx=594.,NEy=374.> {
      parent => <-.UIscrolledWindow_pot;
      label => "Atom";
      x = 5;
      y = 5;
      width => ((.parent.clientWidth / 4) - .x);
      alignment = "left";
    };
    UIlabel UIlabel_type<NEx=429.,NEy=374.> {
      parent => <-.UIscrolledWindow_pot;
      label => "Type";
      x => ((<-.UIlabel_atom.x + <-.UIlabel_atom.width) + 5);
      y => <-.UIlabel_atom.y;
      width => ((.parent.clientWidth / 4) - 5);
      alignment = "left";
    };
    UIlabel UIlabel_neigh<NEx=253.,NEy=374.> {
      parent => <-.UIscrolledWindow_pot;
      label => "Neighbour";
      x => <-.UIlist_CA_neigh.x;
      y = 5;
      width => ((.parent.clientWidth / 4) - 5);
      alignment = "left";
    };
    int y<NEportLevels=1,NEx=99.,NEy=374.> =>
      ((.UIlabel_atom.y + .UIlabel_atom.height) + 5);
    UIlist UIlist_CA_neigh<NEx=418.,NEy=11.> {
      parent => <-.UIframe;
      strings => <-.string#4;
      x => ((<-.Atom_Type_Lable.x + <-.Atom_Type_Lable.width) + 5);
      y = 15;
      selectedItem => <-.data.central_neighb;
    };
    string string#3<NEportLevels={1,2},NEx=253.,NEy=418.>[] =>
					 macro.atom_symbol;
    string string#4<NEportLevels=1,NEx=253.,NEy=462.>[];
    UItoggle UItoggle_set_V<NEx=583.,NEy=462.> {
      parent => <-.UIframe;
      label => "Set Constant V";
      y => ((<-.Pot_Method_Calc.y + <-.Pot_Method_Calc.height) + 15);
      x = 5;
      width = 150;
      visible => <-.Potential_generation.ok;
      set => <-.data.constant_V0;
    };
    // Don't like the way the arrays are done here
    CCP3.EXCURV.Modules.pot_data_type data<NEx=88.,NEy=220.,NEportLevels={0,2}> {
      central_neighb<NEportLevels={0,2}>;
      no_atom_types<NEportLevels={0,2}> = 0;
      //atom_types<NEportLevels={0,2}> => <-.atom_types;
      //neighbours<NEportLevels={0,2}> => <-.neighbours;
      constant_V0<NEportLevels={0,2}> = 0;
      method<NEportLevels={0,2}> = 2;
    };
    UItext UItext<NEx=737.,NEy=0.> {
      parent => <-.panel;
      text = " Set the neighbour atom for the central atom\n and for the other atom types (if there are any).";
      y = 5;
      multiLine = 1;
      rows = 2;
      //resizeToText = 1;
      outputOnly = 1;
      width => parent.clientWidth;
      //fontAttributes {
      //   weight = "medium";
      //};
    };
  };
  macro MS_Paremeters{
    UIframe UIframe_MS_Parameters<NEx=-176.,NEy=-22.> {
      parent => <-.UItemplateDialog_MS_Parameters;
      y = 0;
      width => <-.UItemplateDialog_MS_Parameters.clientWidth;
      height => <-.UItemplateDialog_MS_Parameters.clientHeight;
    };
    UItemplateDialog UItemplateDialog_MS_Parameters<NEx=-187.,NEy=-143.> {
      title => "Multiple Scattering Parameters";
      x = 0;
      y = 0;
      width = 500;
      height = 700;
      okButton = 1;
      cancelButton = 0;
      ok = 0;
    };
    UIradioBoxLabel UIradioBoxLabel_Theory<NEx=132.,NEy=-88.> {
      parent => <-.UIframe_MS_Parameters;
      labels => <-.string_CalcMethod;
      UIradioBox {
	y => ((<-.UIlabel.y + <-.UIlabel.height) + 10);
	x => <-.UIlabel.x;
      };
      UIlabel {
	alignment = "left";
      };
      width = 130;
      x = 20;
      title => "Multiple Scattering Theory";
    };
    string string_CalcMethod<NEportLevels={1,1},NEx=143.,NEy=-143.>[] = {
      "Curved Wave Theory",
      "Small Atom Theory"
    };
    UIfieldTypein input_plmax<NEx=154.,NEy=308.> {
      UIparent => <-.UIframe_MS_Parameters;
      flabel => <-.string;
      fmin => 0.01;
      fmax => 100.;
      field {
	x => (<-.label.x + <-.label.width);
	width => 70;
	updateMode = 7;
      };
      width = 400;
      label {
	width => 300;
	alignment = "left";
      };
      x = 20;
      y => (<-.input_plmin.y + <-.input_plmin.height);
    };
    string string<NEportLevels=1,NEx=352.,NEy=253.> =
      "Maximum Path lenght (PLMAX)";
    UIfieldTypein input_atmax<NEx=165.,NEy=88.> {
      UIparent => <-.UIframe_MS_Parameters;
      flabel => <-.string#1;
      fmin => 0.01;
      fmax => 100.;
      field {
	x => (<-.label.x + <-.label.width);
	width => 70;
	updateMode = 7;
      };
      width = 400;
      label {
	width => 300;
	alignment = "left";
      };
      x = 20;
      y => ((<-.UIradioBoxLabel_Theory.y
	     + <-.UIradioBoxLabel_Theory.height) + 20);
    };
    UIfieldTypein input_plmin<NEx=165.,NEy=198.> {
      UIparent => <-.UIframe_MS_Parameters;
      flabel => <-.string#2;
      fmin => 0.0;
      fmax => 100.;
      field {
	x => (<-.label.x + <-.label.width);
	width => 70;
	updateMode = 7;
      };
      width = 400;
      label {
	width => 300;
	alignment = "left";
      };
      x = 20;
      y => (<-.input_atmax.y + <-.input_atmax.height);
    };
    UIfieldTypein input_dlmax<NEx=165.,NEy=407.> {
      UIparent => <-.UIframe_MS_Parameters;
      flabel => <-.string#3;
      fmin => 0.0;
      fmax => 100.;
      field {
	x => (<-.label.x + <-.label.width);
	width => 70;
	updateMode = 7;
      };
      width = 400;
      label {
	width => 300;
	alignment = "left";
      };
      x = 20;
      y => (<-.input_plmax.y + <-.input_plmax.height);
    };
    UIfieldTypein input_tlmax<NEx=165.,NEy=484.> {
      UIparent => <-.UIframe_MS_Parameters;
      flabel => <-.string#4;
      fmin => 0.0;
      fmax => 100.;
      field {
	x => (<-.label.x + <-.label.width);
	width => 70;
	updateMode = 7;
      };
      width = 400;
      label {
	width => 300;
	alignment = "left";
      };
      x = 20;
      y => (<-.input_dlmax.y + <-.input_dlmax.height);
    };
    UIfieldTypein input_minang<NEx=616.,NEy=77.> {
      UIparent => <-.UIframe_MS_Parameters;
      flabel => <-.string#5;
      fmin => -100;
      fmax => 100.;
      field {
	x => (<-.label.x + <-.label.width);
	width => 70;
	updateMode = 7;
      };
      width = 400;
      label {
	width => 300;
	alignment = "left";
      };
      x = 20;
      y => (<-.input_tlmax.y + <-.input_tlmax.height);
    };
    string string#1<NEportLevels=1,NEx=352.,NEy=33.> =
      "Max. No. of Atoms per Path (ATMAX)";
    string string#2<NEportLevels=1,NEx=352.,NEy=143.> =
      "Minimum Path lenght (PLMIN)";
    string string#3<NEportLevels=1,NEx=363.,NEy=363.> =
      "Angular Momentum for Double Scattering Paths (DLMAX)";
    string string#4<NEportLevels=1,NEx=363.,NEy=440.> =
      "Angular Momentum for Higher Order Paths (TLMAX)";
    string string#5<NEportLevels=1,NEx=836.,NEy=22.> =
      "Minimal Angle of Angular Momentum (MINANG)";
    UIfieldTypein input_minmag<NEx=627.,NEy=198.> {
      flabel => <-.string#6;
      fmin => 0.0;
      fmax => 100.;
      field {
	x => (<-.label.x + <-.label.width);
	width => 70;
	updateMode = 7;
      };
      width = 400;
      label {
	width => 300;
	alignment = "left";
      };
      UIparent => <-.UIframe_MS_Parameters;
      x = 20;
      y => (<-.input_minang.y + <-.input_minang.height);
    };
    string string#6<NEportLevels=1,NEx=858.,NEy=154.> =
      "Minimal Amplitude of Angular Momentum (MINMAG)";
    UIfieldTypein input_omin<NEx=649.,NEy=330.> {
      UIparent => <-.UIframe_MS_Parameters;
      flabel => <-.string#7;
      fmin => 0.01;
      fmax => 100.;
      field {
	x => (<-.label.x + <-.label.width);
	width => 70;
	updateMode = 7;
      };
      width = 400;
      label {
	width => 300;
	alignment = "left";
      };
      x = 20;
      y => (<-.input_minmag.y + <-.input_minmag.height);
    };
    UIfieldTypein input_omax<NEx=638.,NEy=451.> {
      UIparent => <-.UIframe_MS_Parameters;
      flabel => <-.string#8;
      fmin => 0.01;
      fmax => 100.;
      field {
	x => (<-.label.x + <-.label.width);
	width => 70;
	updateMode = 7;
      };
      width = 400;
      label {
	width => 300;
	alignment = "left";
      };
      x = 20;
      y => (<-.input_omin.y + <-.input_omin.height);
    };
    UIfieldTypein input_numax<NEx=638.,NEy=572.> {
      UIparent => <-.UIframe_MS_Parameters;
      flabel => <-.string#9;
      fmin => 0.01;
      fmax => 100.;
      field {
	x => (<-.label.x + <-.label.width);
	width => 70;
	updateMode = 7;
      };
      width = 400;
      label {
	width => 300;
	alignment = "left";
      };
      x = 20;
      y => (<-.input_omax.y + <-.input_omax.height);
    };
    UIfieldTypein input_output<NEx=638.,NEy=649.> {
      UIparent => <-.UIframe_MS_Parameters;
      flabel => <-.string#10;
      fmin => 0.0;
      fmax => 100.;
      field {
	x => (<-.label.x + <-.label.width);
	width => 70;
	updateMode = 7;
      };
      width = 400;
      label {
	width => 300;
	alignment = "left";
      };
      x = 20;
      y => (<-.input_numax.y + <-.input_numax.height);
    };
    string string#7<NEportLevels=1,NEx=891.,NEy=297.> =
      "Minimal Order of Scattering (OMIN)";
    string string#8<NEportLevels=1,NEx=902.,NEy=407.> =
      "Maximum Order of Scattering (OMAX)";
    string string#9<NEportLevels=1,NEx=913.,NEy=539.> = "NUMAX";
    string string#10<NEportLevels=1,NEx=946.,NEy=627.> = "OUTPUT";
  };
  macro Kinematic_Panel {
    //BGS
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=330.,NEy=33.>;
    link UIparent<NEportLevels={2,1},NEx=583.,NEy=33.>;
    //BGS - Todo, use prefs?
    GMOD.copy_on_change copy_on_change<NEx=-143.,NEy=275.> {
      output = 0;
      on_inst = 0;
    };
    CCP3.Version2.Macros.Misc.CCP3shell Kinematic_Refinement<NEx=22.,NEy=396.> {
      UIshell {
	title => "Refinement";
	width = 900;
	height = 750;
	visible => <-.<-.copy_on_change.output;
	parent => <-.<-.UIparent;
      };
    };
    int int_mdel_display_shell_model<NEportLevels=1,NEx=-594.,NEy=-11.>;
    CCP3.EXCURV.Macros.MS_Paremeters ms_panel<NEx=803.,NEy=374.> {
      UIframe_MS_Parameters<NEy=77.>;
      UItemplateDialog_MS_Parameters {
	visible => <-.<-.UIbutton_MSParameters.do;
      };
      input_plmax {
	fval => <-.<-.data.max_path_length;
      };
      input_atmax {
	fval => <-.<-.data.max_atoms_path;
      };
      input_plmin {
	fval => <-.<-.data.float_plmin;
      };
      input_dlmax {
	fval => <-.<-.data.int_dlmax;
      };
      input_tlmax {
	fval => <-.<-.data.int_tlmax;
      };
      input_minang {
	fval => <-.<-.data.float_minang;
      };
      input_minmag {
	fval => <-.<-.data.float_minmag;
      };
      input_omin {
	fval => <-.<-.data.int_omin;
      };
      input_omax {
	fval => <-.<-.data.int_omax;
      };
      input_numax {
	fval => <-.<-.data.int_numax;
      };
      input_output {
	fval => <-.<-.data.float_output;
      };
      UIradioBoxLabel_Theory {
	selectedItem => <-.<-.data.theory;
      };
    };
    /*UItemplateDialog Kinematic_Refinement<NEx=22.,NEy=396.> {
      visible<NEportLevels={3,0}> => <-.<-.EXCURV_main.Kinematic_button.do;
      title => "Kinematic Refinement";
      width = 600;
      height = 700;
      okButton = 1;
      cancelButton = 1;
      ok = 0;
      cancel = 0;
      };*/
    macro input_per_shell<NEx=561.,NEy=550.>[data.number_of_shells] {
      int Min_shells<NEportLevels=1,NEx=121.,NEy=473.> = 1;
      int Max_shell<NEportLevels=1,NEx=132.,NEy=528.> = 5;
      link link<NEportLevels={2,1},NEx=66.,NEy=22.> => <-.UIscrolledWindow;
      UItoggle Refine_number<NEx=297.,NEy=99.> {
	x => <-.atoms_per_shell.x;
	y => ((<-.atoms_per_shell.y + <-.atoms_per_shell.height) + 5);
	parent => <-.link;
	label = "Refine";
	active => ((<-.<-.data.user_defined_ref == 1) &
		   (<-.<-.data.multiple_scattering == 0) & 
                   (<-.<-.int_mdel_display_shell_model == 1));
	set => <-.<-.data.shells[index_of(<-.<-.input_per_shell)].refine_number;
      };
      UItoggle Refine_radius<NEx=583.,NEy=99.> {
	x => <-.radius.x;
	y => <-.Refine_number.y;
	parent => <-.link;
	label = "Refine";
	active => (<-.<-.data.user_defined_ref == 1);
	set => <-.<-.data.shells[index_of(<-.<-.input_per_shell)].refine_radius;
      };
      UItoggle Refine_debye<NEx=814.,NEy=99.> {
	x => <-.dw_factor.x;
	y => <-.Refine_number.y;
	parent => <-.link;
	label = "Refine";
	active => (<-.<-.data.user_defined_ref == 1);
	set => <-.<-.data.shells[index_of(<-.<-.input_per_shell)].refine_dw;
      };
      UIlabel shell_no_label<NEx=836.,NEy=-66.> {
	parent => <-.link;
	label => <-.string#4;
	y => ((index_of(<-.<-.input_per_shell) * <-.int) + 15);
	width => (<-.<-.shell_number.width - .x);
	x = 10;
	alignment = "left";
      };
      int int<NEportLevels=1,NEx=484.,NEy=506.> => (.atom_type.height + 10);
      int index<NEx=66.,NEy=110.> => (index_of(<-.input_per_shell) + 1);
      string str_index<NEx=66.,NEy=187.> => .index;
      string string#4<NEportLevels=1,NEx=858.,NEy=-143.> => .str_index;
      UIfield atoms_per_shell<NEx=121.,NEy=407.> {
	parent => <-.link;
	x => <-.<-.number_atoms.x;
	y => <-.shell_no_label.y;
	//width = 40;
	//mode = "integer";
	min => 0.0;
	max => 100.0;
	value => <-.<-.data.shells[index_of(<-.<-.input_per_shell)].number_of_atoms;
	updateMode = 7;
	active => ((<-.<-.data.multiple_scattering == 0) &
		   (<-.<-.int_mdel_display_shell_model == 1));
	decimalPoints = 2;
      };
      UIfield radius<NEx=484.,NEy=407.> {
	parent => <-.link;
	x => <-.<-.radius.x;
	y => <-.shell_no_label.y;
	//width = 40;
	value => <-.<-.data.shells[index_of(<-.<-.input_per_shell)].radius;
	updateMode = 7;
	decimalPoints = 5;
      };
      UIfield dw_factor<NEx=726.,NEy=407.> {
	parent => <-.link;
	x => <-.<-.dw_factor.x;
	y => <-.shell_no_label.y;
	//width = 40;
	value => <-.<-.data.shells[index_of(<-.<-.input_per_shell)].debye_waller;
	updateMode = 7;
	decimalPoints = 5;
      };
      UIlist atom_type<NEx=957.,NEy=407.> {
	parent => <-.link;
	x => <-.<-.type_atom.x;
	y => <-.shell_no_label.y;
	//strings => <-.<-.<-.Potential_panel.string#3;
	selectedItem => <-.<-.data.shells[index_of(<-.<-.input_per_shell)].atom_type;
      };
    };
    CCP3.Core_Macros.UI.UIobjs.DLVscrolledWindow UIscrolledWindow<NEx=187.,
      NEy=352.> {
      x = 7;
      y = 0;
      width => (.parent.clientWidth - 10);
      parent => <-.UIframe;
      height => .parent.clientHeight;
#ifdef MSDOS
      virtualWidth = 870;
      virtualHeight => <-.height_of_scrolled_window;
#endif // MSDOS
    };
    int height_of_scrolled_window<NEportLevels=1,NEx=-539.> =>
      ((data.number_of_shells * 78) + 15);
    /*UIpanel UIpanel<NEx=242.,NEy=440.> {
      parent => <-.Kinematic_Refinement;
      x = 5;
      y = 5;
      width => (.parent.clientWidth - .x);
      height => .parent.clientHeight;
      };*/
    UIframe UIframe<NEx=121.,NEy=154.> {
      parent => <-.Kinematic_Refinement.UIpanel;
      y => (<-.shell_number.y + <-.shell_number.height);
      width => (.parent.clientWidth - 5);
      height => (((.parent.clientHeight / 2) - .y) - 3);
    };
    macro centre_atom<NEx=528.,NEy=605.> {
      UIlabel centre_atom_label<NEx=352.,NEy=253.> {
	parent => <-.link;
	//y = 5;
	width => (parent.clientWidth / 4);
	alignment = "left";
	y => ((<-.<-.UIbutton_MSParameters.y + <-.<-.UIbutton_MSParameters.height) + 10);
	x = 5;
      };
      link link<NEportLevels={2,1},NEx=132.,NEy=44.> => <-.Kinematic_Refinement.UIpanel;
      UIfieldTypein centre_atom_debye_input<NEx=121.,NEy=253.> {
	UIparent => <-.link;
	flabel => <-.string;
	fval => <-.<-.data.central_dw;
	label {
	  alignment = "left";
	  width => <-.field.x;
	};
	width => 210; //(field.x + field.width + 15);
	field {
	  x => 125;
	  width => 70;
	  updateMode = 7;
	  decimalPoints = 5;
	};
	x => ((<-.<-.dw_factor.x + <-.<-.UIscrolledWindow.x) - field.x);
	y => <-.centre_atom_label.y;
      };
      string string<NEportLevels=1,NEx=165.,NEy=176.> = "Debye-Waller factor";
      UItoggle UItoggle<NEx=561.,NEy=253.> {
	parent => <-.link;
	label => "Refine";
	y => <-.centre_atom_debye_input.y;
	x => ((<-.centre_atom_debye_input.x + <-.centre_atom_debye_input.width) + 20);
	alignment = "left";
	active => <-.<-.UItoggle_user.set;
	set => <-.<-.data.refine_cdw;
      };
    };
    macro input_shell_number<NEx=616.,NEy=495.> {
      UIfieldTypein UIfieldTypein<NEx=209.,NEy=253.> {
	UIparent => <-.<-.Kinematic_Refinement.UIpanel;
	flabel => <-.string;
	fval => <-.<-.data.number_of_shells;
	fmin => <-.min_shell;
	fmax => <-.<-.data.max_shells;
	field {
	  mode = "integer";
	  x => 125;
	  width => 70;
	  updateMode = 7;
	};
	label {
	  height => 16;
	  y => 0;
	  width => 120;
	  alignment = "left";
	};
	x = 5;
	y => (<-.<-.centre_atom.centre_atom_debye_input.y + <-.<-.centre_atom.centre_atom_debye_input.height);
      };
      string string<NEportLevels={1,1},NEx=11.,NEy=143.> = "Number of Shells";
      int min_shell<NEportLevels=1,NEx=385.,NEy=33.> = 1;
    };
    UIlabel shell_number<NEx=-66.,NEy=748.> {
      parent => <-.Kinematic_Refinement.UIpanel;
      label => "Shell Number";
      y => (<-.input_shell_number.UIfieldTypein.y + <-.input_shell_number.UIfieldTypein.height);
      width => (.parent.clientWidth / 5);
      alignment = "left";
    };
    UIlabel radius<NEx=297.,NEy=748.> {
      parent => <-.Kinematic_Refinement.UIpanel;
      label => "Radius";
      x => (<-.number_atoms.x + <-.number_atoms.width);
      y => (<-.input_shell_number.UIfieldTypein.y + <-.input_shell_number.UIfieldTypein.height);
      width => (.parent.clientWidth / 5);
      alignment = "center";
    };
    UIlabel dw_factor<NEx=451.,NEy=748.> {
      parent => <-.Kinematic_Refinement.UIpanel;
      label => "Debye-Waller factor";
      x => (<-.radius.x + <-.radius.width);
      y => (<-.input_shell_number.UIfieldTypein.y + <-.input_shell_number.UIfieldTypein.height);
      width => (.parent.clientWidth / 5);
      alignment = "center";
    };
    UIlabel number_atoms<NEx=99.,NEy=748.> {
      parent => <-.Kinematic_Refinement.UIpanel;
      label => "Number of Atoms";
      x => (<-.shell_number.x + <-.shell_number.width);
      y => (<-.input_shell_number.UIfieldTypein.y + <-.input_shell_number.UIfieldTypein.height);
      width => (.parent.clientWidth / 5);
      alignment = "center";
      active => ((<-.data.multiple_scattering == 0) & 
		 (<-.int_mdel_display_shell_model == 1));
    };
    UIlabel type_atom<NEx=671.,NEy=748.> {
      parent => <-.Kinematic_Refinement.UIpanel;
      label => "Type of Atom";
      x => (<-.dw_factor.x + <-.dw_factor.width);
      y => (<-.input_shell_number.UIfieldTypein.y + <-.input_shell_number.UIfieldTypein.height);
      width => (.parent.clientWidth / 5);
      alignment = "left";
    };
    UIbutton run_refinement<NEx=-407.> {
      label => "Run Refinement";
      x => (((3 * parent.clientWidth) / 4) - (.width / 2));
      parent => <-.Kinematic_Refinement.UIpanel;
      y => <-.UIbutton_update.y;
      width = 150;
      alignment = "center";
      do = 0;
      visible => ((<-.data.user_defined_ref == 1 || <-.UItoggle_na.set == 1 ||
		   <-.UItoggle_r.set == 1 || <-.UItoggle_dw.set == 1) &
		  is_valid(<-.data.shells.atom_type));
    };
    UItoggle UItoggle_user<NEx=-253.,NEy=858.> {
      parent => <-.Kinematic_Refinement.UIpanel;
      label => "User Defined Refinement";
      x = 5;
      y => ((<-.UIframe.y + <-.UIframe.height) + 10);
      width => (parent.clientWidth / 5);
      active => (((<-.UItoggle_dw.set == 0) & (<-.UItoggle_na.set == 0)) &
		 (<-.UItoggle_r.set == 0));
      set => <-.data.user_defined_ref;
    };
    UItoggle UItoggle_dw<NEx=-66.,NEy=858.> {
      parent => <-.Kinematic_Refinement.UIpanel;
      label => "All Debye-Waller Factors";
      y => ((<-.UIframe.y + <-.UIframe.height) + 10);
      x => ((<-.UItoggle_user.x + <-.UItoggle_user.width) + 10);
      width => (parent.clientWidth / 5);
      alignment = "left";
      active => (<-.data.user_defined_ref == 0);
      set => <-.data.refine_all_dw;
    };
    UItoggle UItoggle_na<NEx=121.,NEy=858.> {
      parent => <-.Kinematic_Refinement.UIpanel;
      label => "All Number of Atoms";
      x => (<-.UItoggle_dw.x + <-.UItoggle_dw.width);
      y => <-.UItoggle_dw.y;
      width => (parent.clientWidth / 5);
      alignment = "left";
      active => ((<-.data.user_defined_ref == 0) &
		 (<-.data.multiple_scattering == 0) & 
                 (<-.int_mdel_display_shell_model == 1));
      set => <-.data.refine_all_atom_num;
    };
    UItoggle UItoggle_r<NEx=363.,NEy=858.> {
      parent => <-.Kinematic_Refinement.UIpanel;
      label => "All Radii";
      width => (parent.clientWidth / 5);
      alignment = "left";
      y => <-.UItoggle_dw.y;
      x => (<-.UItoggle_na.x + <-.UItoggle_na.width);
      active => (<-.data.user_defined_ref == 0);
      set => <-.data.refine_all_radii;
    };
    /*UItoggle UItoggle_PHI<NEx=539.,NEy=946.> {
      label => "All PHI values";
      x => (<-.UItoggle_r.x + <-.UItoggle_r.width);
      y => <-.UItoggle_dw.y;
      width => (parent.clientWidth / 4);
      active => ((<-.UItoggle_user.set == 0) & (<-.UItoggle_na.set == 0));
    };
    UItoggle UItoggle_XY<NEx=363.,NEy=935.> {
      label => "All XY values";
      y => ((<-.UItoggle_dw.y + <-.UItoggle_dw.height) + 10);
      width => (parent.clientWidth / 4);
      active => (((((<-.UItoggle_user.set == 0) & (<-.UItoggle_dw.set == 0)) & (<-.UItoggle_na.set == 0)) & (<-.UItoggle_r.set == 0)) & (<-.UItoggle_PHI.set == 0));
      };*/
    UItoggle UItoggle_ef<NEx=176.,NEy=935.> {
      parent => <-.Kinematic_Refinement.UIpanel;
      label => "Fermi Energy";
      y => ((<-.UIframe.y + <-.UIframe.height) + 10);
      width => (parent.clientWidth / 5);
      x => (<-.UItoggle_r.x + <-.UItoggle_r.width);
      set => <-.data.refine_efermi;
    };
    UIbutton UIbutton_update<NEx=-517.,NEy=638.> {
      parent => <-.Kinematic_Refinement.UIpanel;
      label => "Update";
      x => ((parent.clientWidth / 4) - (.width / 2));
      y => ((<-.macro.UItext_fermi.y + <-.macro.UItext_fermi.height) + 20);
      width = 150;
      alignment = "center";
      do = 0;
      visible => (is_valid(<-.data.shells.atom_type));
    };
    UIbutton UIbutton_Resume<NEx=-572.,NEy=737.> {
      parent => <-.Kinematic_Refinement.UIpanel;
      label => "Resume";
      x => (((2 * parent.clientWidth) / 4) - (.width / 2));
      y => <-.UIbutton_update.y;
      width = 150;
      do = 0;
      visible => <-.run_refinement.do;
    };
    UIslider UIslider_step<NEx=-517.,NEy=935.,NEportLevels={0,1}> {
      parent => <-.Kinematic_Refinement.UIpanel;
      value => <-.data.step_size;
      title => "Step Size";
      y => (<-.UIradioBoxLabel_step.y + 10);
      x => ((<-.UIradioBoxLabel_step.x + <-.UIradioBoxLabel_step.width) + 10);
      width => ((((11 * .parent.clientWidth) / 12) - x) - 10);
      min = 10;
      max = 300;
      mode = "integer";
      increment = 50;
    };
    VUIslider_typein VUIslider_typein_step<NEx=-308.,NEy=1012.> {
      slider => <-.UIslider_step;
      x => ((<-.UIslider_step.x + <-.UIslider_step.width) + 2);
      //y => (<-.UIslider_step.y + (<-.UIslider_step.height / 2));
      width => ((<-.Kinematic_Refinement.UIpanel.clientWidth / 12) - 5);
      valEditor {
	UI {
	  value_field {
	    updateMode = 7;
	  };
	  min_field {
	    updateMode = 7;
	  };
	  max_field {
	    updateMode = 7;
	  };
	};
      };
    };
    UIslider UIslider_iter<NEx=-88.,NEy=1012.,NEportLevels={0,1}> {
      parent => <-.Kinematic_Refinement.UIpanel;
      value => <-.data.number_of_iterations;
      title => "Number of Iterations";
      width => ((((11 * .parent.clientWidth) / 12) - x) - 10);
      x => <-.UIslider_step.x;
      y => ((<-.UIslider_step.y + <-.UIslider_step.height) + 10);
      min = 1;
      max = 200;
      mode = "integer";
      increment = 50;
    };
    VUIslider_typein VUIslider_typein_iter<NEx=88.,NEy=1056.> {
      slider => <-.UIslider_iter;
      x => ((<-.UIslider_iter.x + <-.UIslider_iter.width) + 2);
      //y => (<-.UIslider_iter.y + (<-.UIslider_iter.height / 2));
      width => ((<-.Kinematic_Refinement.UIpanel.clientWidth / 12) - 5);
      valEditor {
	UI {
	  value_field {
	    updateMode = 7;
	  };
	  min_field {
	    updateMode = 7;
	  };
	  max_field {
	    updateMode = 7;
	  };
	};
      };
    };
    UIradioBoxLabel UIradioBoxLabel_step<NEx=-396.,NEy=440.> {
      parent => <-.Kinematic_Refinement.UIpanel;
      labels => <-.string;
      selectedItem = 0;
      title => "Step Size";
      UIradioBox {
	height => UIdata.UIfonts[0].lineHeight;
	orientation = "horizontal";
      };
      width = 250;
      x = 5;
      y => ((<-.UItoggle_ef.y + <-.UItoggle_ef.height) + 10);
    };
    string string<NEportLevels=1,NEx=-451.,NEy=352.>[] = {
      "Coarse",
      "Medium",
      "Fine"
    };
    GMOD.parse_v parse_v<NEx=-462.,NEy=297.> {
      v_commands = "UIslider_step.value = switch((UIradioBoxLabel_step.selectedItem + 1),50,150,250);";
      trigger => <-.UIradioBoxLabel_step.selectedItem;
      on_inst = 0;
      relative => <-;
    };
    UItoggle UItoggle_correlations<NEx=-616.,NEy=1001.> {
      parent => <-.Kinematic_Refinement.UIpanel;
      label = "Calculate Correlations";
      x = 5;
      y => ((<-.UIslider_iter.y + <-.UIslider_iter.height) + 10);
      width = 250;
      set => <-.data.correlations;
    };
    UItoggle UItoggle_MS<NEx=418.,NEy=286.> {
      visible => (<-.int_mdel_display_shell_model == 0);
      parent => <-.Kinematic_Refinement.UIpanel;
      label => "Multiple Scattering";
      x = 5;
      y = 5;
      width = 200;
      set => <-.data.multiple_scattering;
    };
    UIbutton UIbutton_convert<NEx=-275.,NEy=165.> {
      visible => ((<-.data.multiple_scattering == 0) &&
		  (<-.int_mdel_display_shell_model == 0));
      parent => <-.Kinematic_Refinement.UIpanel;
      label => "Convert to Shell Model";
      x => (((2 * parent.clientWidth) / 4) - (.width / 2));
      y => ((<-.UIbutton_update.y + <-.UIbutton_update.height) + 10);
      width = 200;
    };
    CCP3.EXCURV.Modules.refinement_data data<NEx=726.,NEy=110.,NEportLevels={0,2}> {
      number_of_shells = 1;
      step_size = 50;
      number_of_iterations = 20;
      central_dw = 0.01;
      refine_cdw = 0;
      user_defined_ref = 0;
      refine_all_atom_num = 0;
      refine_all_radii = 0;
      refine_all_dw = 0;
      refine_efermi = 0;
      correlations = 0;
      fermi_energy = 0.0;
      fit_factor = "";
      R_factor = "";
      multiple_scattering = 0;
      max_atoms_path = 3;
      max_path_length = 10.0;
      symmetry = "";
      float_plmin = 0.0;
      float_minang = -1.;
      float_minmag = 0.0;
      int_dlmax = 25;
      int_tlmax = 9;
      int_numax = 2;
      int_omin = 1;
      int_omax = 3;
      float_output = 0.0;
      theory = 1;
    };
    UIbutton UIbutton_MSParameters<NEx=77.,NEy=55.> {
      visible => ((<-.data.multiple_scattering == 1) &&
		  (<-.int_mdel_display_shell_model == 0));
      parent => <-.Kinematic_Refinement.UIpanel;
      label => "MS Parameters";
      x => (<-.UItoggle_MS.x + <-.UItoggle_MS.width);
      y = 10;
    };
    macro macro<NEx=539.,NEy=1001.> {
      link link<NEportLevels={2,1},NEx=121.,NEy=132.> => <-.Kinematic_Refinement.UIpanel;
      UIfield UItext_fermi<NEx=99.,NEy=308.> {
	parent => <-.link;
	x => ((1 * parent.clientWidth) / 6);
	y => ((<-.<-.UItoggle_correlations.y + <-.<-.UItoggle_correlations.height) + 20);
	width = 75;
	value => <-.<-.data.fermi_energy;
      };
      UItext UItext_fitnes<NEx=341.,NEy=308.> {
	parent => <-.link;
	x => ((3 * parent.clientWidth) / 6);
	y => UItext_fermi.y;
	width = 75;
	outputOnly = 1;
	text => <-.<-.data.fit_factor;
      };
      UItext UItext_rfac<NEx=583.,NEy=308.> {
	parent => <-.link;
	x => ((5 * parent.clientWidth) / 6);
	y => UItext_fermi.y;
	width = 75;
	outputOnly = 1;
	text => <-.<-.data.R_factor;
      };
      UIlabel UIlabel_fermi<NEx=99.,NEy=429.> {
	parent => <-.link;
	label => "Fermi Energy  ";
	y => UItext_fermi.y;
	width = 95;
	alignment = "right";
      };
      UIlabel UIlabel_fitnes<NEx=341.,NEy=429.> {
	parent => <-.link;
	label => "Fitness ";
	x => ((2 * parent.clientWidth) / 6);
	y => UItext_fermi.y;
	width = 95;
	alignment = "right";
      };
      UIlabel UIlabel_rfac<NEx=594.,NEy=429.> {
	parent => <-.link;
	label => "R - factor  ";
	x => ((4 * parent.clientWidth) / 6);
	y => UItext_fermi.y;
	width = 95;
	alignment = "right";
      };
    };
    GMOD.parse_v popup<NEx=418.,NEy=77.> {
      v_commands = "UIlabel.label = \"EXCURV is busy - Please Wait\"; UIworkingDialog.title = \"EXCURV is busy\"; UIworkingDialog.visible = 1;";
      trigger => (run_refinement.do & UIbutton_Resume.do);
      on_inst = 0;
      relative => <-;
      sync = 1;
    };
    UItemplateDialog UIworkingDialog<NEx=440.,NEy=132.> {
      parent => <-.Kinematic_Refinement.UIpanel;
      x => parent.x + 100;
      y => parent.y + 100;
      width = 250;
      isModal = 1;
      autoUnmanage = 1;
      okButton = 0;
      cancelButton = 0;
      title = "EXCURV is busy";
    };
    UIpanel busy_panel {
      parent => <-.UIworkingDialog;
      x = 0;
      y = 0;
    };
    UIlabel UIlabel<NEx=748.,NEy=198.> {
      parent => <-.busy_panel;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      label = "EXCURV is busy - Please Wait";
      fontAttributes {
	weight = "bold";
      };
    };
  };
  macro excurvUI {
    //BGS
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=209.,NEy=55.>;
    link UIparent<NEportLevels={2,1},NEx=517.,NEy=55.>;
    //BGS Todo - use prefs
    int model_source = 0;
    int model_type;
    CCP3.EXCURV.Macros.EXCURV_main EXCURV_main<NEx=154.,NEy=209.> {
      //BGS
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      //BGS
      emitter_atom<NEportLevels={1,3}>;
      edge<NEportLevels={1,3}>;
      UIradioBoxLabel_Model {
	labels => switch (((<-.<-.model_type == 0) + 1),
			  <-.shell_model, <-.model_options);
	selectedItem => <-.<-.model_source;
      };
      Read_Expt_Data {
	visible => ((<-.emitter_atom != 0) || (<-.<-.model_source == 1));
      };
      Central_Atom_Labele {
	visible => (<-.<-.model_source == 0);
      };
      Central_Atom_Type {
	visible => (<-.<-.model_source == 0);
	strings => <-.<-.atoms;
      };
    };
    CCP3.EXCURV.Macros.file_locator file_locator<NEx=363.,NEy=209.> {
      //BGS
      prefs => <-.prefs;
      Read_Experimental_Data {
	parent => <-.<-.EXCURV_main.CCP3shell.UIshell;
      };
      //BGS
    };
    CCP3.EXCURV.Macros.plot_spectra plot {
      prefs => <-.prefs;
      Print_Frame {
	parent => <-.<-.EXCURV_main.CCP3shell.UIpanel;
	y => <-.<-.EXCURV_main.UIradioBoxLabel_Model.y;
	height => ((<-.<-.EXCURV_main.Potential_button.y - 5) - .y);
	visible => (<-.<-.file_locator.Read_Experimental_Data.ok);
      };
    };
    Potential_panel Potential_panel<NEx=132.,NEy=363.> {
      prefs => <-.prefs;
      UIparent => <-.EXCURV_main.CCP3shell;
      string =>
	(((("Central atom: " + string#1[0]) + "       Edge:  ")
	  + <-.EXCURV_main.Edge_Box_Selector.selectedString)
	 + "               Neighbour:");
      string#1 =>
	str_array(<-.atoms[<-.EXCURV_main.emitter_atom]," ");
      input_atom_number {
	UIfieldTypein {
	  field {
            active => (<-.<-.<-.<-.model_source == 0);
	  };
	};
      };
      macro {
	UIlist {
	  strings => <-.<-.<-.atoms;
	  active => (<-.<-.<-.model_source == 0);
	};
	UIlist#1 {
	  active => (<-.<-.<-.model_source == 0);
	};
	UIlabel {
	  active => (<-.<-.<-.model_source == 0);
	};
	atom_symbol => <-.<-.atoms[<-.data.atom_pairs[index_of(<-.macro)].atom_type];
      };
      UIlabel_atom {
	active => (<-.<-.model_source == 0);
      };
      UIlabel_type {
	active => (<-.<-.model_source == 0);
      };
      UIlabel_neigh {
	active => (<-.<-.model_source == 0);
      };
      UIlist_CA_neigh {
	active => (<-.<-.model_source == 0);
      };
      string#4 =>
	concat_array((<-.atoms[<-.EXCURV_main.emitter_atom] + "*"),
		     string#3);
    };
    Kinematic_Panel Kinematic_Panel<NEx=396.,NEy=550.> {
      prefs => <-.prefs;
      int_mdel_display_shell_model => (<-.model_type == 7);
      UIparent => <-.EXCURV_main.CCP3shell;
      input_per_shell {
	atom_type {
	  strings => <-.<-.<-.Potential_panel.string#3;
	  active => (<-.<-.<-.model_source == 0);
	};
      };
      centre_atom {
	centre_atom_label {
	  label => ("Central Atom:    " +
		    <-.<-.<-.Potential_panel.string#1[0]);
	};
      };
      type_atom {
	active => (<-.<-.model_source == 0);
      };
    };
    string atoms<NEportLevels=1>[] = {
      "?",
      "H   1",
      "He  2",
      "Li  3",
      "Be  4",
      "B   5",
      "C   6",
      "N   7",
      "O   8",
      "F   9",
      "Ne 10",
      "Na 11",
      "Mg 12",
      "Al 13",
      "Si 14",
      "P  15",
      "S  16",
      "Cl 17",
      "Ar 18",
      "K  19",
      "Ca 20",
      "Sc 21",
      "Ti 22",
      "V  23",
      "Cr 24",
      "Mn 25",
      "Fe 26",
      "Co 27",
      "Ni 28",
      "Cu 29",
      "Zn 30",
      "Ga 31",
      "Ge 32",
      "As 33",
      "Se 34",
      "Br 35",
      "Kr 36",
      "Rb 37",
      "Sr 38",
      "Y  39",
      "Zr 40",
      "Nb 41",
      "Mo 42",
      "Tc 43",
      "Ru 44",
      "Rh 45",
      "Pd 46",
      "Ag 47",
      "Cd 48",
      "In 49",
      "Sn 50",
      "Sb 51",
      "Te 52",
      "I  53",
      "Xe 54",
      "Cs 55",
      "Ba 56",
      "La 57",
      "Ce 58",
      "Pr 59",
      "Nd 60",
      "Pm 61",
      "Sm 62",
      "Eu 63",
      "Gd 64",
      "Tb 65",
      "Dy 66",
      "Ho 67",
      "Er 68",
      "Tm 69",
      "Yb 70",
      "Lu 71",
      "Hf 72",
      "Ta 73",
      "W  74",
      "Re 75",
      "Os 76",
      "Ir 77",
      "Pt 78",
      "Au 79",
      "Hg 80",
      "Tl 81",
      "Pb 82",
      "Bi 83",
      "Po 84",
      "At 85",
      "Rn 86",
      "Fr 87",
      "Ra 88",
      "Ac 89",
      "Th 90",
      "Pa 91",
      "U  92"
    };
  };
  macro excurv {
    //BGS start
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=385.,NEy=22.>;
    int visible<NEportLevels=1,NEx=583.,NEy=22.> = 0;
    link UIparent<NEportLevels={2,1},NEx=781.,NEy=22.>;
    link job_data<NEportLevels={2,1},NEx=770.,NEy=99.>;
    int+nres atom_types<NEportLevels=1,NEx=171.,NEy=216.>[] =>
      excurvUI.Potential_panel.data.atom_pairs.atom_type;
    //BGS end
    UIcmd UIcmd {
      label = "DL_EXCURV";
      do => <-.visible;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    GMOD.instancer instancer {
      Value => <-.visible;
      Group => <-.excurvUI;
      //active = 2;
    };
    excurvUI excurvUI<instanced=0> {
      //BGS
      prefs => <-.prefs;
      UIparent => <-.UIparent;
      //BGS
      EXCURV_main {
	CCP3shell {
	  /*UIshell {
	    visible => <-.<-.<-.<-.UIcmd.do;
	    };*/
	  shell_vis {
	    input => <-.<-.<-.<-.visible;
	  };
	  parse_v {
	    v_commands = "visible = 0;";
	    relative => <-.<-.<-.<-;
	  };
	};
	Potential_button {
	  visible => (<-.<-.<-.read_expt.prog_ok &&
		      <-.<-.<-.excurv_errors.expt_ok);
	};
	int => <-.<-.excurv_errors.potential_ok;
	UIfieldTypein_emin {
	  panel {
	    visible => (<-.<-.<-.<-.read_expt.prog_ok &&
			<-.<-.<-.<-.excurv_errors.expt_ok);
	  };
	};
	UIfieldTypein_emax {
	  panel {
	    visible => (<-.<-.<-.<-.read_expt.prog_ok &&
			<-.<-.<-.<-.excurv_errors.expt_ok);
	  };
	};
	UIfieldTypein_kmin {
	  panel {
	    visible => (<-.<-.<-.<-.read_expt.prog_ok &&
			<-.<-.<-.<-.excurv_errors.expt_ok);
	  };
	};
	UIfieldTypein_kmax {
	  panel {
	    visible => (<-.<-.<-.<-.read_expt.prog_ok &&
			<-.<-.<-.<-.excurv_errors.expt_ok);
	  };
	};
	//	UIbutton_Save_Par {
	FileB {
	  UIbutton {
	  visible => (<-.<-.<-.read_expt.prog_ok &&
		      <-.<-.<-.excurv_errors.expt_ok &&
		      (is_valid(<-.<-.Kinematic_Panel.data.shells.atom_type)));
	  };
	};
      };
      file_locator {
	copy_on_change {
	  input => ((<-.<-.<-.visible == 1) &
		    (<-.<-.EXCURV_main.Read_Expt_Data.do == 1));
	};
      };
      plot {
	Print_Frame {
	  visible => (<-.<-.<-.read_expt.prog_ok &&
		      <-.<-.<-.excurv_errors.expt_ok);
	};
      };
      Potential_panel {
	copy_on_change {
	  input => ((<-.<-.<-.visible == 1) &
		    (<-.<-.EXCURV_main.Potential_button.do == 1));
	};
      };
      Kinematic_Panel {
	copy_on_change {
	  input => ((<-.<-.<-.visible == 1) &
		    (<-.<-.EXCURV_main.Kinematic_button.do == 1));
	};
      };
    };
    CCP3.EXCURV.Modules.close_excurv close_excurv<NEx=154.,NEy=352.> {
      //trigger => <-.UIcmd.do;
      trigger => <-.excurvUI.EXCURV_main.CCP3shell.UIquestionDialog.ok;
    };
    CCP3.EXCURV.Modules.read_expt read_expt<NEx=319.,NEy=352.> {
      trigger => <-.excurvUI.file_locator.Read_Experimental_Data.ok;
      kweight => <-.excurvUI.EXCURV_main.k_weight;
      atom_type => <-.excurvUI.EXCURV_main.emitter_atom;
      edge => <-.excurvUI.EXCURV_main.Edge_Box_Selector.selectedString;
      data => <-.excurvUI.file_locator.expt_data;
      prog_ok = 0;
      job_data => <-.job_data;
    };
    CCP3.EXCURV.Modules.plot plot<NEx=517.,NEy=352.> {
      do_plot => <-.excurvUI.plot.Plt_Data.do;
      do_print => <-.excurvUI.plot.FileB.UIfileDialog.ok;
      filename => <-.excurvUI.plot.FileB.filename;
      x => <-.excurvUI.plot.int#1;
      y => <-.excurvUI.plot.bool_select;
    };
    CCP3.EXCURV.Modules.pot_phase pot_phase<NEx=715.,NEy=352.> {
      trigger => <-.excurvUI.Potential_panel.Potential_generation.ok;
      atom_type => <-.excurvUI.EXCURV_main.emitter_atom;
      data => <-.excurvUI.Potential_panel.data;
    };
    CCP3.EXCURV.Modules.refine refine<weight=2,NEx=341.,NEy=429.> {
      do_update => <-.excurvUI.Kinematic_Panel.UIbutton_update.do;
      do_resume => <-.excurvUI.Kinematic_Panel.UIbutton_Resume.do;
      do_refine => <-.excurvUI.Kinematic_Panel.run_refinement.do;
      do_list => <-.excurvUI.EXCURV_main.Kinematic_button.do;
      nshells => data.number_of_shells;
      do_import => <-.excurv_errors.potential_ok;
      visible => <-.excurvUI.Kinematic_Panel.Kinematic_Refinement.UIshell.visible;
      cluster => <-.excurvUI.model_source;
      data => <-.excurvUI.Kinematic_Panel.data;
      complete = 0;
    };
    GMOD.parse_v popdown {
      v_commands = "excurvUI.Kinematic_Panel.UIlabel.label = \"EXCURV has finished\"; excurvUI.Kinematic_Panel.UIworkingDialog.title = \"EXCURV has finished\"; excurvUI.Kinematic_Panel.UIworkingDialog.visible = 0;";
      trigger => <-.refine_data;
      //trigger => <-.refine.complete;
      on_inst = 0;
      relative => <-;
    };
    CCP3.EXCURV.Modules.set_params set_params<NEx=715.,NEy=517.> {
      params => <-.refine_data;
      data => <-.excurvUI.Kinematic_Panel.data;
    };
    string refine_data<NEportLevels=1,NEx=550.,NEy=451.>;
    CCP3.EXCURV.Modules.set_kweight set_kweight<NEx=792.,NEy=440.> {
      kweight => <-.excurvUI.EXCURV_main.k_weight;
    };
    CCP3.EXCURV.Modules.set_minmax set_minmax<NEx=396.,NEy=506.> {
      emin => <-.excurvUI.EXCURV_main.emin;
      emax => <-.excurvUI.EXCURV_main.emax;
      kmin => <-.excurvUI.EXCURV_main.kmin;
      kmax => <-.excurvUI.EXCURV_main.kmax;
    };
    CCP3.EXCURV.Modules.set_multiple_scat ms<NEx=132.,NEy=528.> {
      ms => <-.excurvUI.Kinematic_Panel.data.multiple_scattering;
      symmetry => <-.excurvUI.Kinematic_Panel.data.symmetry;
      model_type+nres => <-.<-.model_type;
    };
    GMOD.parse_v parse_v<NEx=484.,NEy=88.> {
      v_commands = "excurvUI.EXCURV_main.Read_Expt_Data.do = 0; excurvUI.EXCURV_main.Potential_button.do = 0; excurvUI.Potential_panel.Potential_generation.ok = 0; excurvUI.file_locator.Read_Experimental_Data.ok = 0; excurvUI.EXCURV_main.Kinematic_button.do = 0; read_expt.prog_ok = 0; excurv_errors.potential_ok = 0; excurv_errors.expt_ok = 1; excurvUI.model_source = 0; excurvUI.Kinematic_Panel.data.multiple_scattering = 0; excurvUI.Kinematic_Panel.UIbutton_convert.do = 0; excurvUI.Kinematic_Panel.data.max_shells = 16; excurvUI.Potential_panel.data.constant_V0 = 0;";
      trigger => <-.visible;
      on_inst = 0;
      relative => <-;
    };
    CCP3.EXCURV.Modules.save_refine save_refine<NEx=528.,NEy=572.> {
      do_print => <-.excurvUI.EXCURV_main.FileB.UIfileDialog.ok;
      filename => <-.excurvUI.EXCURV_main.FileB.filename;
    };
    string error_string;
    int expt_error = 0;
    int pot_error = 0;
    CCP3.EXCURV.Modules.excurv_errors excurv_errors<NEx=308.,NEy=561.> {
      params => <-.error_string;
      do_potential => <-.pot_error;
      do_expt => <-.expt_error;
      potential_ok = 0;
      expt_ok = 1;
    };
    CCP3.EXCURV.Modules.excurv_cluster excurv_cluster<NEx=176.,NEy=462.> {
      trigger => <-.excurvUI.model_source;
      visible => <-.visible;
      // Todo in => <-.parent_model;
      atom_type => <-.excurvUI.EXCURV_main.emitter_atom;
      pot_data => <-.excurvUI.Potential_panel.data;
      ref_data => <-.excurvUI.Kinematic_Panel.data;
    };
    // Todo CCP3.CCP3Types.model+nres &parent_model<NEx=715.,NEy=88.>;
    /*GMOD.parse_v set_model {
      v_commands = "excurvUI.model_type = switch((is_valid(parent_model.model_type)+1), -1, switch(is_valid(parent_model.display.shell_model)+1, -2, (parent_model.model_type + parent_model.display.shell_model)));";
      trigger => <-.visible;
      on_inst = 0;
      relative => <-;
      };*/
    CCP3.EXCURV.Modules.create_model create_model<NEx=759.,NEy=594.> {
      atom => <-.excurvUI.EXCURV_main.emitter_atom;
      model_build => <-.excurvUI.model_source;
    };
    CCP3.EXCURV.Modules.excurv_model excurv_model<NEx=385.,NEy=627.> {
      convert => <-.excurvUI.Kinematic_Panel.UIbutton_convert.do;
      visible => <-.visible;
      potentials => <-.pot_error;
      atom => <-.excurvUI.EXCURV_main.emitter_atom;
      atom_types => <-.atom_types;
      data => <-.excurvUI.Kinematic_Panel.data;
    };
  };
  // End of import
  macro prefsUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=748.,NEy=66.>;
    link visible<NEportLevels={2,1},NEx=154.,NEy=66.>;
    link location<NEportLevels={2,1},NEx=748.,NEy=143.>;
    CCP3.Core_Macros.UI.UIobjs.DLVshell DLVshell<NEx=374.,NEy=154.> {
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      title = "EXCURV Preferences";
      UIshell {
	visible => <-.<-.visible;
      };
    };
    UIlabel label<NEx=385.,NEy=308.> {
      y => ((UIradioBoxLabel.y + UIradioBoxLabel.height) + 5);
      width => parent.clientWidth;
      parent => <-.DLVshell.UIpanel;
      label => "Location of EXCURV installation";
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DirectoryBrowser DBrowser<NEx=726.,NEy=363.> {
      preferences => <-.prefs;
      y => ((label.y + label.height) + 5);
      location => <-.location;
      title = "EXCURV install location";
      UIpanel {
	parent => <-.<-.DLVshell.UIpanel;
      };
    };
  };
  macro Prefs {
    link location<NEx=693.,NEy=143.,NEportLevels={0,1}>;
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels=1,NEx=693.,NEy=44.>;
    link UIparent<NEportLevels=1,NEx=187.,NEy=77.>;
    UIoption UIoption<NEx=440.,NEy=33.> {
      label = "Preferences";
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    GMOD.instancer instancer {
      Value => <-.UIoption.set;
      Group => <-.prefsUI;
    };
    prefsUI prefsUI<NEx=396.,NEy=330.,instanced=0> {
      prefs => <-.prefs;
      visible => <-.UIoption.set;
      location => <-.location;
      DLVshell {
	UIshell {
	  parent => <-.<-.<-.UIparent;
	};
      };
    };
  };
  CCP3.Core_Macros.CalcObjs.calculation EXCURV {
    UIcmdList {
      label = "EXCURV";
      order = 13;
      cmdList => {
	execute.UIcmd,
	Prefs.UIoption
      };
    };
    dialog_visible => (execute.visible == 1);
    string exec_location<NEx=308.,NEy=187.,NEportLevels={1,1}> = "";
    CCP3.Core_Modules.Calcs.exec_environ exec_environ<NEx=308.,NEy=264.> {
      location => <-.exec_location;
      default_var = "DLV_DEF_EXCURV";
      search_path = 0;
      main_var = "DLV_EXCURV";
      binary = "dl_excurv";
    };
    CCP3.EXCURV.Macros.excurv execute<NEx=594.,NEy=330.> {
      prefs => <-.preferences;
      UIparent => <-.UIparent;
      excurvUI {
	model_type => <-.<-.model_type;
      };
      job_data => <-.job_data;
    };
    Prefs Prefs<NEx=836.,NEy=539.> {
      prefs => <-.preferences;
      location => <-.exec_location;
    };
  };
};
