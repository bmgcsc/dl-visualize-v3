
flibrary Macros<NEeditable=1,
#ifndef DLV_NO_DLLS
		dyn_libs="libCCP3im",
#endif // DLV_NO_DLLS
		export_cxx=1,
		build_dir="avs/src/express",
		cxx_hdr_files="avs/src/express/calcs.hxx avs/src/core/fb_gen.hxx avs/src/image/load_gen.hxx",
		out_hdr_file="image.hxx",
		out_src_file="image.cxx"> {
  macro calcBase {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels=1,NEx=198.,NEy=66.>;
    link active_menus<NEportLevels=1,NEx=385.,NEy=66.>;
    link UIparent<NEportLevels=1,NEx=770.,NEy=66.>;
    int visible<NEportLevels=1,NEx=396.,NEy=143.> = 0;
    UIcmd UIcmd<NEx=198.,NEy=143.> {
      active => <-.active_menus;
      do => <-.visible;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    GMOD.instancer instancer<NEx=264.,NEy=275.> {
      Value => <-.visible;
    };
  };
  macro LoadUI {
    link selection<NEportLevels={2,1},NEx=297.,NEy=33.>;
    link filename<NEportLevels={2,1},NEx=605.,NEy=33.>;
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=825.,NEy=33.>;
    link load<NEportLevels={2,1},NEx=462.,NEy=33.>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog UItemplateDialog<NEx=110.,NEy=132.> {
      width = 300;
      height = 400;
      title = "Load Density";
      ok => <-.load;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    UIpanel UIpanel<NEx=253.,NEy=198.> {
      parent => <-.UItemplateDialog;
      width => <-.parent.clientWidth;
      height => parent.clientHeight;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    /*CCP3.Core_Modules.Utils.FBdata FBdata<NEx=715.,NEy=297.,
      NEportLevels={0,1}> {
      filename<NEportLevels={2,0}> => <-.filename;
      parent1<NEportLevels={2,0}> => <-.UIpanel;
      title = "CRYSTAL Property filename";
      x = 0;
      y = 0;
      pattern = "*";
      file_write = 0;
      onlyButton = 0;
      ok = 0;
    };
    CCP3.Core_Modules.Utils.manageFB manageFB<NEx=715.,NEy=396.> {
      data => <-.FBdata;
      };*/
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=715.,NEy=396.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}> => <-.filename;
      parent => <-.UIpanel;
      title = "CRYSTAL Property filename";
      x = 0;
      y = 0;
      pattern = "*";
    };
  };
  calcBase Load_File {
    UIcmd {
      label = "Load Density";
      active => <-.active_menus;
    };
    string filename<NEportLevels=1,NEx=781.,NEy=187.> = "";
    int do_load<NEportLevels=1,NEx=330.,NEy=187.> = 0;
    link model_type<NEportLevels=1,NEx=814.,NEy=55.>;
    LoadUI LoadUI<NEx=495.,NEy=374.,instanced=0> {
      filename => <-.filename;
      prefs => <-.prefs;
      load => <-.do_load;
      UItemplateDialog {
	parent => <-.<-.UIparent;
	visible => <-.<-.visible;
      };
    };
    instancer {
      Group => <-.LoadUI;
    };
    CCP3.IMAGE.Modules.load_data load_data<NEx=462.,NEy=506.> {
      filename => <-.filename;
      do_load => <-.do_load;
    };
  };
  CCP3.Core_Macros.CalcObjs.calculation Image {
    UIcmdList {
      label = "image";
      order = 32;
      cmdList => {
	Load_File.UIcmd
      };
    };
    dialog_visible => ((Load_File.visible == 1));
    CCP3.IMAGE.Macros.Load_File Load_File<NEx=352.,NEy=374.> {
      active_menus => <-.active_menus;
      UIparent => <-.UIparent;
      prefs => <-.preferences;
      model_type => <-.model_type;
    };
  };
};
