
flibrary Macros<NEeditable=1,
#ifndef DLV_NO_DLLS
		dyn_libs="libCCP3kkr",
#endif // DLV_NO_DLLS
		export_cxx=1,
		build_dir="avs/src/express",
		cxx_hdr_files="avs/src/express/calcs.hxx avs/src/core/env_gen.hxx avs/src/express/misc.hxx",
		out_hdr_file="kkr.hxx",
		out_src_file="kkr.cxx"> {
  macro calcBase {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels=1,NEx=198.,NEy=66.>;
    link active_menus<NEportLevels=1,NEx=385.,NEy=66.>;
    link UIparent<NEportLevels=1,NEx=770.,NEy=66.>;
    int visible<NEportLevels=1,NEx=396.,NEy=143.> = 0;
    UIcmd UIcmd<NEx=198.,NEy=143.> {
      active => <-.active_menus;
      do => <-.visible;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    GMOD.instancer instancer<NEx=264.,NEy=275.> {
      Value => <-.visible;
    };
  };
  macro prefsUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=748.,NEy=66.>;
    link visible<NEportLevels={2,1},NEx=154.,NEy=66.>;
    link location<NEportLevels={2,1},NEx=748.,NEy=143.>;
    CCP3.Core_Macros.UI.UIobjs.DLVshell DLVshell<NEx=374.,NEy=154.> {
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      title = "SCF-KKR Preferences";
      UIshell {
	visible => <-.<-.visible;
      };
    };
    UIlabel label<NEx=385.,NEy=308.> {
      y => ((UIradioBoxLabel.y + UIradioBoxLabel.height) + 5);
      width => parent.clientWidth;
      parent => <-.DLVshell.UIpanel;
      label => "Location of KKR executables";
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    CCP3.Core_Macros.UI.UIobjs.DirectoryBrowser DBrowser<NEx=726.,NEy=363.> {
      preferences => <-.prefs;
      y => ((label.y + label.height) + 5);
      location => <-.location;
      title = "KKR location";
      UIpanel {
	parent => <-.<-.DLVshell.UIpanel;
      };
    };
  };
  macro Prefs {
    link location<NEx=693.,NEy=143.,NEportLevels={0,1}>;
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels=1,NEx=693.,NEy=44.>;
    link UIparent<NEportLevels=1,NEx=187.,NEy=77.>;
    UIoption UIoption<NEx=440.,NEy=33.> {
      label = "Preferences";
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
    };
    GMOD.instancer instancer {
      Value => <-.UIoption.set;
      Group => <-.prefsUI;
    };
    prefsUI prefsUI<NEx=396.,NEy=330.,instanced=0> {
      prefs => <-.prefs;
      visible => <-.UIoption.set;
      location => <-.location;
      DLVshell {
	UIshell {
	  parent => <-.<-.<-.UIparent;
	};
      };
    };
  };
  CCP3.Core_Macros.CalcObjs.calculation KKR {
    UIcmdList {
      label = "SCF-KKR";
      order = 40;
      cmdList => {
	Prefs.UIoption
      };
    };
    dialog_visible = 0;
    string exec_location<NEx=308.,NEy=187.,NEportLevels={1,1}> = "";
    CCP3.Core_Modules.Calcs.exec_environ exec_environ<NEx=308.,NEy=264.> {
      location => <-.exec_location;
      default_var = "DLV_DEF_KKR";
      search_path = 0;
      main_var = "DLV_KKR";
      binary = "gulp"; // Todo
    };
    Prefs Prefs<NEx=836.,NEy=539.> {
      prefs => <-.preferences;
      location => <-.exec_location;
    };
  };
};
