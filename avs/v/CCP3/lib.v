
flibrary CCP3 {
  "../avs/v/CCP3/modules/core/modules.v" Core_Modules;
  "../avs/v/CCP3/core/macros.v" Core_Macros;
  "../avs/v/CCP3/viewers/viewers.v" Viewers;
  "../avs/v/CCP3/render/render.v" Renderers;
#ifdef DLV_RELEASE
  flibrary CRYSTAL {
  };
  flibrary EXCURV {
  };
  flibrary GULP {
  };
  flibrary ONETEP {
  };
  //flibrary KKR {
  //};
  flibrary K_P {
  };
  flibrary ROD {
  };
  flibrary CHEMSHELL {
  };
  flibrary CASTEP {
  };
  flibrary IMAGE {
  };
#else
  flibrary CRYSTAL {
    "../avs/v/CCP3/modules/crystal/modules" Modules;
    "../avs/v/CCP3/crystal/base" Base;
    "../avs/v/CCP3/crystal/scf" SCF;
    "../avs/v/CCP3/crystal/props" Props;
    "../avs/v/CCP3/crystal/macros" Macros;
  };
  flibrary EXCURV {
    "../avs/v/CCP3/modules/excurv/modules" Modules;
    "../avs/v/CCP3/excurv/macros" Macros;
  };
  flibrary GULP {
    "../avs/v/CCP3/modules/gulp/modules" Modules;
    "../avs/v/CCP3/gulp/macros" Macros;
  };
  flibrary ONETEP {
    "../avs/v/CCP3/modules/onetep/modules" Modules;
    "../avs/v/CCP3/onetep/macros" Macros;
  };
  //flibrary KKR {
  //  "../avs/v/CCP3/modules/kkr/modules" Modules;
  //  "../avs/v/CCP3/kkr/macros" Macros;
  //};
  flibrary K_P {
    "../avs/v/CCP3/modules/k.p/modules" Modules;
    "../avs/v/CCP3/k.p/macros" Macros;
  };
  flibrary ROD {
    "../avs/v/CCP3/modules/rod/modules" Modules;
    "../avs/v/CCP3/rod/macros" Macros;
   "../avs/v/CCP3/rod/calc" Calc;
    "../avs/v/CCP3/rod/read" Read;
    "../avs/v/CCP3/rod/create" Create;
    "../avs/v/CCP3/rod/view" View;
    "../avs/v/CCP3/rod/write" Write;
    "../avs/v/CCP3/rod/model" Model;
    "../avs/v/CCP3/rod/params" Params;
    "../avs/v/CCP3/rod/fit" Fit;
    "../avs/v/CCP3/rod/plot" Plot;
  };
  flibrary CHEMSHELL {
    "../avs/v/CCP3/modules/chemshell/modules" Modules;
    "../avs/v/CCP3/chemshell/macros" Macros;
  };
  flibrary CASTEP {
    "../avs/v/CCP3/modules/castep/modules" Modules;
    "../avs/v/CCP3/castep/macros" Macros;
  };
  flibrary IMAGE {
    "../avs/v/CCP3/modules/image/modules" Modules;
    "../avs/v/CCP3/image/macros" Macros;
  };
#endif // DLV_RELEASE
  flibrary Calcs {
    "../avs/v/CCP3/modules/crystal/create.v" CRYSTAL;
    "../avs/v/CCP3/modules/excurv/create.v" EXCURV;
    "../avs/v/CCP3/modules/gulp/create.v" GULP;
    "../avs/v/CCP3/modules/onetep/create.v" ONETEP;
    //"../avs/v/CCP3/modules/kkr/create.v" KKR;
    "../avs/v/CCP3/modules/k.p/create.v" K_P;
    "../avs/v/CCP3/modules/rod/create.v" ROD;
    "../avs/v/CCP3/modules/chemshell/create.v" CHEMSHELL;
    "../avs/v/CCP3/modules/image/create.v" IMAGE;
    "../avs/v/CCP3/modules/castep/create.v" CASTEP;
  };
#ifdef DLV_BABEL
  flibrary Babel {
    "../avs/v/CCP3/modules/babel/modules" Modules;
    "../avs/v/CCP3/babel/macros" Macros;
  };
#endif // DLV_BABEL
  flibrary Version2 {
    "../avs/v/CCP3/modules/old/modules.v" Modules;
    "../avs/v/CCP3/old/macros.v" Macros;
  };
};
