
flibrary CASTEP<NEeditable=1
#ifndef DLV_NO_DLLS
		,dyn_libs="libCCP3castep"
#endif // DLV_NO_DLLS
		> {
  module create_ui<build_dir="avs/src/castep",
    out_src_file="castep_gen.cxx",
    out_hdr_file="castep_gen.hxx",
    need_objs="CCP3.CASTEP CCP3.Core_Macros.CalcObjs.calculation",
    src_file="create_xp.cxx"> {
    cxxmethod+notify_inst create<status=1>(make+req);
    int make<NEportLevels={2,0}>;
  };
};
