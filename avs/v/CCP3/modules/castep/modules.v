
flibrary Modules<NEeditable=1
#ifndef DLV_NO_DLLS
		 ,dyn_libs="libCCP3castep"
#endif // DLV_NO_DLLS
		 > {
  // Kind of a copy of CRYSTAL? Good? Bad?
  group StructureData {
    string name;
    string file;
    boolean new_view;
  };
  module load_structure<build_dir="avs/src/castep",
    out_src_file="str_gen.cxx",
    out_hdr_file="str_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req load<status=1>(data+read+req,
				 do_load+notify+req);
    StructureData &data<NEportLevels={2,0}>;
    int do_load<NEportLevels={2,0}>;
  };
  module load_phonons<build_dir="avs/src/castep",
    out_src_file="phon_gen.cxx",
    out_hdr_file="phon_gen.hxx",
    cxx_hdr_files="str_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req load<status=1>(data+read+req,
				 do_load+notify+req);
    StructureData &data<NEportLevels={2,0}>;
    int do_load<NEportLevels={2,0}>;
  };
  module save_cell_file<build_dir="avs/src/castep",
    out_src_file="save_gen.cxx",
    out_hdr_file="save_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req save<status=1>(filename+read+req,
				 frac_coords+read+req,
				 do_save+notify+req);
    string filename<NEportLevels={2,0}>;
    int frac_coords<NEportLevels={2,0}>;
    int do_save<NEportLevels={2,0}>;
  };
};
