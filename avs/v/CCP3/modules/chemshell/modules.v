
flibrary Modules<NEeditable=1
#ifndef DLV_NO_DLLS
		 ,dyn_libs="libCCP3chemshell"
#endif // DLV_NO_DLLS
		 > {
  group StructureData {
    string name;
    string file;
    boolean new_view;
  };
  module load_structure<build_dir="avs/src/chemshell",
    out_src_file="str_gen.cxx",
    out_hdr_file="str_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req load<status=1>(data+read+req,
				 do_load+notify+req);
    StructureData &data<NEportLevels={2,0}>;
    int do_load<NEportLevels={2,0}>;
  };
  module save_pun_file<build_dir="avs/src/chemshell",
    out_src_file="save_gen.cxx",
    out_hdr_file="save_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req save<status=1>(filename+read+req,
				 label+read+req,
				 do_save+notify+req);
    string filename<NEportLevels={2,0}>;
    string label<NEportLevels={2,0}>;
    int do_save<NEportLevels={2,0}>;
  };
};
