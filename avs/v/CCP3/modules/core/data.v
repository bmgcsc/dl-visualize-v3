
flibrary Data<NEeditable=1,
#ifndef DLV_NO_DLLS
	      dyn_libs="libCCP3dlv",
#endif // DLV_NO_DLLS
	      export_cxx=1,
	      build_dir="avs/src/express",
	      out_hdr_file="data.hxx",
	      out_src_file="data.cxx"> {
  group ONETEP_scf_info {
    boolean scf_done;
  };
  group CRYSTAL_Wavefn_info {
    int nbands;
    int vband_min;
    int vband_max;
    int lattice;
    int centre;
    int wavefn_set;
    boolean spin;
    int n_kpoints;
    string kpoint_labels[n_kpoints];
    boolean scf_done;
    boolean tddft_done;
  };
  group band_paths {
    string path2D = "0   0   0 G\n1/2 0   0\n1/2 1/2 0\n0   0   0 G";
    group path3D {
      group lattices[] {
	group centers[] {
	  string path<NEportLevels=1>;
	};
      };
    } = {
      lattices = {
	// Triclinic
	{
	  centers = {
	    {
	      path = "0 0 0 G\n1 0 0"
	    }
	  }
	},
	// Monoclinic (b)
	{
	  centers = {
	    {
	      path = " 0   0   1/2 Z\n 0   0   0   G\n 0   1/2 0   Y\n-1/2 1/2 0   A\n-1/2 0   0   B\n-1/2 0   1/2 D\n-1/2 1/2 1/2 E\n 0   1/2 1/2 C"
	    },
	    {
	      path = "0 0 0 G\n1 0 0"
	    },
	    {
	      path = "0 0 0 G\n1 0 0"
	    },
	    {
	      path = " 0   0   1/2 Z\n 0   0   0   G\n 0   1/2 0   Y\n-1/2 1/2 0   A\n-1/2 0   0   B\n-1/2 0   1/2 D\n-1/2 1/2 1/2 E\n 0   1/2 1/2 C"
	    },
	    {
	      path = "0 0 0 G\n1 0 0"
	    },
	    {
	      path = "0 0 0 G\n1 0 0"
	    }
	  }
	},
	// Orthorhombic
	{
	  centers = {
	    {
	      path = " 0   0   0   G\n 0   0   1/2 Z\n-1/2 0   1/2 T\n-1/2 0   0   Y\n-1/2 1/2 0   S\n 0   1/2 0   X\n 0   1/2 1/2 U\n-1/2 1/2 1/2 R"
	    },
	    {
	      path = " 0   0   0   G\n 0   0   1/2 Z\n-1/2 0   1/2 T\n-1/2 0   0   Y\n-1/2 1/2 0   S\n 0   1/2 0   X\n 0   1/2 1/2 U\n-1/2 1/2 1/2 R"
	    },
	    {
	      path = " 0   0   0   G\n 0   0   1/2 Z\n-1/2 0   1/2 T\n-1/2 0   0   Y\n-1/2 1/2 0   S\n 0   1/2 0   X\n 0   1/2 1/2 U\n-1/2 1/2 1/2 R"
	    },
	    {
	      path = " 0   0   0   G\n 0   0   1/2 Z\n-1/2 0   1/2 T\n-1/2 0   0   Y\n-1/2 1/2 0   S\n 0   1/2 0   X\n 0   1/2 1/2 U\n-1/2 1/2 1/2 R"
	    },
	    {
	      path = " 0   0   0   G\n 0   0   1/2 Z\n-1/2 0   1/2 T\n-1/2 0   0   Y\n-1/2 1/2 0   S\n 0   1/2 0   X\n 0   1/2 1/2 U\n-1/2 1/2 1/2 R"
	    },
	    {
	      path = " 0   0   0   G\n 0   0   1/2 Z\n-1/2 0   1/2 T\n-1/2 0   0   Y\n-1/2 1/2 0   S\n 0   1/2 0   X\n 0   1/2 1/2 U\n-1/2 1/2 1/2 R"
	    }
	  }
	},
	// Tetragonal
	{
	  centers = {
	    {
	      path = "0   0   1/2 Z\n1/2 1/2 1/2 A\n1/2 1/2 0   M\n0   0   0   G\n0   0   1/2 Z\n0   1/2 1/2 R"
	    },
	    {
	      path = "0 0 0 G\n1 0 0"
	    },
	    {
	      path = "0 0 0 G\n1 0 0"
	    },
	    {
	      path = "0 0 0 G\n1 0 0"
	    },
	    {
	      path = "0 0 0 G\n1 0 0"
	    },
	    {
	      path = "1/2 1/2 -1/2 Z\n0   0    0   G\n0   0    1/2 X\n1/4 1/4  1/4 P\n0   1/2    0 N\n0   0    0   G"
	    }
	  }
	},
	// Trigonal
	{
	  centers = {
	    {
	      path = " 0   0   0   G\n 0   0   1/2 A\n-1/3 2/3 1/2 H\n-1/3 2/3 0   K\n 0   0   0   G\n 0   1/2 0   M\n 0   1/2 1/2 L\n-1/3 2/3 1/2 H"
	    },
	    {
	      path = "0 0 0 G\n1 0 0"
	    },
	    {
	      path = "0 0 0 G\n1 0 0"
	    },
	    {
	      path = "0 0 0 G\n1 0 0"
	    },
	    {
	      path = "0 0 0 G\n1 0 0"
	    },
	    {
	      path = "0 0 0 G\n1 0 0"
	    },
	    {
	      path = "1/2 1/2 0   F\n0   0   0   G\n1/2 1/2 1/2 Z"
	    }
	  }
	},
	// Hexagonal
	{
	  centers = {
	    {
	      path = " 0   0   0   G\n 0   0   1/2 A\n-1/3 2/3 1/2 H\n-1/3 2/3 0   K\n 0   0   0   G\n 0   1/2 0   M\n 0   1/2 1/2 L\n-1/3 2/3 1/2 H"
	    }
	  }
	},
	// Cubic
	{
	  centers = {
	    {
	      path = "0   0   0 G\n0   0   1/2 Z\n1/2 1/2 1/2 R\n1/2 1/2 0   M\n0   0   0   G"
	    },
	    {
	      path = "0 0 0 G\n1 0 0"
	    },
	    {
	      path = "0 0 0 G\n1 0 0"
	    },
	    {
	      path = "0 0 0 G\n1 0 0"
	    },
	    {
	      path = "1/2 1/4 3/4 W\n1/2 1/2 1/2 L\n0   0   0   G\n1/2 0   1/2 X\n1/2 1/4 3/4 W\n3/8 3/8 3/4 K"
	    },
	    {
	      path = "0    0   0   G\n1/2 -1/2 1/2 H\n0    0   1/2 N\n1/4 1/4 1/4 P\n0    0   0   G\n0    0   1/2 N"
	    }
	  }
	}
      }
    };
  };
};
