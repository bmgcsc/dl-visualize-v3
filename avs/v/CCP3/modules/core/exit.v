
flibrary Exit<NEeditable=1> {
  module finalise<build_dir="avs/src/core",
    src_file="express.cxx",
    out_src_file="exit_gen.cxx",
    out_hdr_file="exit_gen.hxx"> {
    cxxmethod+req save(finish+notify,
		       named+read+req,
		       update+write);
    int finish<NEportLevels={2,0}>;
    int named<NEportLevels={2,0}>;
    int update<NEportLevels={0,2}>;
  };
};
