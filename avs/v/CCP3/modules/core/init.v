
flibrary Init<NEeditable=1> {
  module initialise<build_dir="avs/src/core",
    src_file="express.cxx",
    out_src_file="init_gen.cxx",
    out_hdr_file="init_gen.hxx"> {
    cxxmethod+notify_inst+req init(atom_file+read+req,
				   display_prefs+read+req,
				   default_project+read+req);
    string atom_file<NEportLevels={2,0}>;
    string display_prefs<NEportLevels={2,0}>;
    string default_project<NEportLevels={2,0}>;
  };
};
