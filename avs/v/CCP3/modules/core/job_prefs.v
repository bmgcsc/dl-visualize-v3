
flibrary JobPrefs<NEeditable=1,
#ifndef DLV_NO_DLLS
		  dyn_libs="libCCP3dlv",
#endif // DLV_NO_DLLS
		  export_cxx=1,
		  build_dir="avs/src/core/jobs",
		  out_hdr_file="data.hxx",
		  out_src_file="data.cxx"> {
  group job_data {
    string work_dir;
    string scratch_dir;
    boolean is_parallel;
    int nprocs;
    int hours;
    string hostname;
    string account_id;
    int nnodes;
    int memory_gb;
    string queue;
  };
};
