
flibrary Model<NEeditable=1> {
  module periodic_table<build_dir="avs/src/core/model",
    out_src_file="ptbl_gen.cxx",
    out_hdr_file="ptbl_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req select(array+read+write+req+notify,
			 current+read+write);
    int array<NEportLevels={2,0}>[];
    int current<NEportLevels={2,0}>;
  };  
  group model_data {
    string name;
    int model_type;
    boolean new_view;
    int lattice;
    int centre;
    string group;
    int group_id;
    int setting;
    int norigins;
    double a;
    double b;
    double c;
    double alpha;
    double beta;
    double gamma;
  };
  group atom_data {
    int atomic_number;
    boolean set_charge;
    int charge;
    boolean set_radius;
    float radius;
    double x;
    double y;
    double z;
    boolean set_colour;
    float red;
    float green;
    float blue;
    boolean set_spin;
    int spin;
  };
  module create<build_dir="avs/src/core/model",
    out_src_file="make_gen.cxx",
    out_hdr_file="make_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req create(trigger+read+req+notify,
			 index+read+write+req,
			 model+read);
    int trigger<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
    model_data &model<NEportLevels={2,0}>;
    atom_data &atom<NEportLevels={2,0}>;
  };
  module atom_defaults<build_dir="avs/src/core/model",
    out_src_file="def_gen.cxx",
    out_hdr_file="def_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req set(trigger+read+req+notify,
		      charge+write,
		      radius+write);
    cxxmethod+req get_radius(trigger+read+req,
			     charge+read+req+notify,
			     radius+write);
    int trigger<NEportLevels={2,0}>;
    int charge<NEportLevels={2,0}>;
    float radius<NEportLevels={2,0}>;
  };
  module complete_model<build_dir="avs/src/core/model",
    out_src_file="fin_gen.cxx",
    out_hdr_file="fin_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req finish(trigger+req+notify,
			 bond_all+req+read);
    int trigger<NEportLevels={2,0}>;
    int bond_all<NEportLevels={2,0}>;
  };
  module cancel_model<build_dir="avs/src/core/model",
    out_src_file="can_gen.cxx",
    out_hdr_file="can_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req cancel(trigger+req+notify,
			 index+read+req,
			 new_view+read+req,
			 model_type+write);
    int trigger<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    int model_type<NEportLevels={2,0}>;
  };
  group LoadStructData {
    string name;
    string filename;
    int filetype;
    string type_string;
    boolean new_view;
    int pdb_model;
    int pdb_conformer;
    boolean cube_model;
  };
  module check_pdb<build_dir="avs/src/core/model",
    out_src_file="pdb_gen.cxx",
    out_hdr_file="pdb_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req pdb_models(filename+read+req+notify,
			     file_type+read+req,
			     model+read+write+req,
			     nmodels+write);
    cxxmethod+req pdb_conformers(filename+read+req,
				 model+read+req+notify,
				 nconformers+write);
    string filename<NEportLevels={2,0}>;
    int file_type<NEportLevels={2,0}>;
    int model<NEportLevels={2,0}>;
    int nmodels<NEportLevels={2,0}>;
    int nconformers<NEportLevels={2,0}>;
  };
  module load<build_dir="avs/src/core/model",
    src_file="express.cxx",
    out_src_file="load_gen.cxx",
    need_objs="CCP3.Viewers.Scenes.DLV3Dscene CCP3.Renderers.Base.atoms CCP3.Renderers.Model.Model_R CCP3.Renderers.Model.Model_K CCP3.Renderers.Points.atom_bond_text CCP3.Renderers.Model.Shell CCP3.Renderers.Model.Outline",
    out_hdr_file="load_gen.hxx"> {
    cxxmethod+req load<status=1>(run+notify,
				 data+read+req,
				 nconformers+read+req,
				 bond_all+read+req);
    int run<NEportLevels={2,0}>;
    LoadStructData &data<NEportLevels={2,0}>;
    int nconformers<NEportLevels={2,0}>;
    int bond_all<NEportLevels={2,0}>;
  };
  module edit_model<build_dir="avs/src/core/model",
    out_src_file="edit_gen.cxx",
    out_hdr_file="edit_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req finish(trigger+req+notify);
    int trigger<NEportLevels={2,0}>;
  };
  group SuperCellParams {
    int s11;
    int s12;
    int s13;
    int s21;
    int s22;
    int s23;
    int s31;
    int s32;
    int s33;
    boolean conventional;
  };
  module supercell<build_dir="avs/src/core/model",
    out_src_file="cell_gen.cxx",
    out_hdr_file="cell_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(name+notify+read+req,
			new_view+read+req,
			index+write,
			params+write);
    cxxmethod+req update<status=1>(params+read+notify+req);
    string name<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
    SuperCellParams &params<NEportLevels={2,0}>;
  };
  module del_symmetry<build_dir="avs/src/core/model",
    out_src_file="symm_gen.cxx",
    out_hdr_file="symm_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(name+notify+read+req,
			new_view+read+req,
			index+write);
    string name<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
  };
  module to_molecule<build_dir="avs/src/core/model",
    out_src_file="mole_gen.cxx",
    out_hdr_file="mole_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(name+notify+read+req,
			new_view+read+req,
			index+write);
    string name<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
  };
  group LatticeParams {
    double a;
    double b;
    double c;
    double alpha;
    double beta;
    double gamma;
    boolean use_a;
    boolean use_b;
    boolean use_c;
    boolean use_alpha;
    boolean use_beta;
    boolean use_gamma;
  };
  module edit_lattice<build_dir="avs/src/core/model",
    out_src_file="latt_gen.cxx",
    out_hdr_file="latt_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(name+notify+read+req,
			new_view+read+req,
			index+write,
			params+write);
    cxxmethod+req update<status=1>(params+read+notify+req);
    string name<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
    LatticeParams &params<NEportLevels={2,0}>;
  };
  group OriginParams {
    double x;
    double y;
    double z;
    boolean fractional;
  };
  module shift_origin<build_dir="avs/src/core/model",
    out_src_file="orig_gen.cxx",
    out_hdr_file="orig_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(name+notify+read+req,
			new_view+read+req,
			index+write);
    cxxmethod+req position<status=1>(params+read+notify+req);
    cxxmethod+req atoms<status=1>(do_selection+notify+req,
				  params+read+write);
    cxxmethod+req symmetry<status=1>(do_symmetry+notify+req,
				     params+read+write);
    string name<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
    OriginParams &params<NEportLevels={2,0}>;
    int do_selection<NEportLevels={2,0}>;
    int do_symmetry<NEportLevels={2,0}>;
  };
  group VacuumParams {
    double length;
    double position;
  };
  module vacuum_gap<build_dir="avs/src/core/model",
    out_src_file="vgap_gen.cxx",
    out_hdr_file="vgap_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(name+notify+read+req,
			new_view+read+req,
			index+write,
			params+write);
    cxxmethod+req position<status=1>(params+read+notify+req);
    string name<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
    VacuumParams &params<NEportLevels={2,0}>;
  };
  group ClusterParams {
    int neighbours;
    float radius;
  };
  module cut_cluster<build_dir="avs/src/core/model",
    out_src_file="clus_gen.cxx",
    out_hdr_file="clus_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(name+notify+read+req,
			nselect+notify+read+req,
			new_view+read+req,
			index+write,
			params+write);
    cxxmethod+req position<status=1>(params+read+notify+req);
    string name<NEportLevels={2,0}>;
    int nselect<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
    ClusterParams &params<NEportLevels={2,0}>;
  };
  module edit_atom<build_dir="avs/src/core/model",
    out_src_file="atom_gen.cxx",
    out_hdr_file="atom_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(name+notify+read+req,
			nselect+notify+read+req,
			new_view+read+req,
			all_atoms+read+req,
			index+write,
			atom+write);
    cxxmethod+req update<status=1>(atom+read+notify+req,
				   all_atoms+read+req);
    cxxmethod+req accept<status=1>(do_more+notify+req);
    string name<NEportLevels={2,0}>;
    int nselect<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    boolean all_atoms<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
    int atom<NEportLevels={2,0}>;
    int do_more<NEportLevels={2,0}>;
  };
  module edit_atom_props<build_dir="avs/src/core/model",
    out_src_file="prop_gen.cxx",
    out_hdr_file="prop_gen.hxx",
    cxx_hdr_files="avs/src/core/model/make_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(name+notify+read+req,
			nselect+notify+read+req,
			new_view+read+req,
			index+write,
			data+write,
			all_atoms+read+req);
    cxxmethod+req update<status=1>(data+read+notify+req+write,
				   all_atoms+read+req);
    string name<NEportLevels={2,0}>;
    int nselect<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
    atom_data &data<NEportLevels={2,0}>;
    boolean all_atoms<NEportLevels={2,0}>;
  };
  group slab_params {
    int h;
    int k;
    int l;
    int conventional;
    int termination;
    int nlayers;
    float tolerance;
  };
  module cut_slab<build_dir="avs/src/core/model",
    out_src_file="slab_gen.cxx",
    out_hdr_file="slab_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(name+notify+read+req,
			new_view+read+req,
			surface+read+req,
			index+write,
			data+write,
			was_3D+write);
    cxxmethod+req update_hkl<status=1>(surface+read+req,
				       h+read+notify+req,
				       k+read+notify+req,
				       l+read+notify+req,
				       conventional+read+notify+req,
				       tolerance+read+req,
				       termination+write,
				       nlayers+write,
				       labels+write);
    cxxmethod+req update_tol<status=1>(surface+read+req,
				       tolerance+read+notify+req,
				       nlayers+write,
				       labels+write);
    cxxmethod+req update_term<status=1>(index+read+req,
					surface+read+req,
					tolerance+read+req,
					termination+read+notify+req,
					nlayers+read+req);
    cxxmethod+req update_layers<status=1>(surface+read+req,
					  tolerance+read+req,
					  termination+read+req,
					  nlayers+read+notify+req);
    string name<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    boolean surface;
    int index<NEportLevels={2,0}>;
    slab_params &data<NEportLevels={2,0}>;
    int h => data.h;
    int k => data.k;
    int l => data.l;
    int conventional => data.conventional;
    float tolerance => data.tolerance;
    int termination => data.termination;
    int nlayers => data.nlayers;
    string labels<NEportLevels={2,0}>[];
    boolean was_3D<NEportLevels={2,0}>;
  };
  module add_atom_group<build_dir="avs/src/core/model",
    out_src_file="grp_gen.cxx",
    out_hdr_file="grp_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req update<status=1>(trigger+read+req+notify,
				   label+read+req,
				   name+read+req,
				   all_atoms+read+req,
				   bulk+read+req);
    int trigger<NEportLevels={2,0}>;
    string label<NEportLevels={2,0}>;
    string name<NEportLevels={2,0}>;
    boolean all_atoms<NEportLevels={2,0}>;
    string bulk<NEportLevels={2,0}>;
  };
  group salvage_params {
    int nlayers;
    float tolerance;
  };
  module cut_salvage<build_dir="avs/src/core/model",
    out_src_file="salv_gen.cxx",
    out_hdr_file="salv_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(name+notify+read+req,
			new_view+read+req,
			index+write,
			data+write,
			min_layers+write);
    cxxmethod+req update_tol<status=1>(tolerance+read+notify+req);
    cxxmethod+req update_layers<status=1>(tolerance+read+req,
					  nlayers+read+notify+req);
    string name<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
    salvage_params &data<NEportLevels={2,0}>;
    float tolerance => data.tolerance;
    int nlayers => data.nlayers;
    int min_layers<NEportLevels={2,0}>;
  };
  module delete_atom<build_dir="avs/src/core/model",
    out_src_file="del_gen.cxx",
    out_hdr_file="del_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(name+notify+read+req,
			nselect+notify+read+req,
			new_view+read+req,
			index+write);
    //cxxmethod+req update<status=1>(trigger+notify+req);
    cxxmethod+req accept<status=1>(do_more+notify+req);
    string name<NEportLevels={2,0}>;
    int nselect<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
    int do_more<NEportLevels={2,0}>;
  };
  group move_params {
    //boolean displace;
    boolean fractional;
    double x;
    double y;
    double z;
    boolean use_transform;
    boolean displacements;
  };
  module insert_atom<build_dir="avs/src/core/model",
    out_src_file="insa_gen.cxx",
    out_hdr_file="insa_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(name+notify+read+req,
			new_view+read+req,
			index+write,
			atom_type+write,
			fractional+write,
			x+read+write,
			y+read+write,
			z+read+write,
			use_editor+write);
    cxxmethod+req update<status=1>(atom_type+read+notify+req,
				   fractional+read+req,
				   x+read+req+notify,
				   y+read+req+notify,
				   z+read+req+notify);
    cxxmethod+req change<status=1>(fractional+notify+read+req,
				   x+write,
				   y+write,
				   z+write);
    cxxmethod+req generate<status=1>(calc+notify+req,
				     x+write,
				     y+write,
				     z+write);
    cxxmethod+req edit<status=1>(use_editor+notify+read+req,
				 x+write,
				 y+write,
				 z+write);
    cxxmethod+req accept<status=1>(do_more+notify+req);
    string name<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
    int atom_type<NEportLevels={2,0}>;
    move_params &data<NEportLevels={2,0}>;
    boolean fractional => data.fractional;
    double x => data.x;
    double y => data.y;
    double z => data.z;
    int calc<NEportLevels={2,0}>;
    boolean use_editor => data.use_transform;
    int do_more<NEportLevels={2,0}>;
  };
  // Todo - a lot like insert_atom? common stuff?
  module insert_model<build_dir="avs/src/core/model",
    out_src_file="insm_gen.cxx",
    out_hdr_file="insm_gen.hxx",
    cxx_hdr_files="avs/src/core/model/insa_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(name+notify+read+req,
			new_view+read+req,
			selection+read+req,
			index+write,
			fractional+write,
			x+read+write,
			y+read+write,
			z+read+write,
			use_editor+write);
    cxxmethod+req update<status=1>(fractional+read+req,
				   x+read+req+notify,
				   y+read+req+notify,
				   z+read+req+notify);
    cxxmethod+req change<status=1>(fractional+notify+read+req,
				   x+write,
				   y+write,
				   z+write);
    cxxmethod+req generate<status=1>(calc+notify+req,
				     x+write,
				     y+write,
				     z+write);
    cxxmethod+req position<status=1>(centre+notify+req,
				     x+write,
				     y+write,
				     z+write);
    cxxmethod+req edit<status=1>(use_editor+notify+read+req,
				 x+write,
				 y+write,
				 z+write);
    //cxxmethod+req accept<status=1>(do_more+notify+req);
    string name<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
    int selection<NEportLevels={2,0}>;
    move_params &data<NEportLevels={2,0}>;
    boolean fractional => data.fractional;
    double x => data.x;
    double y => data.y;
    double z => data.z;
    int calc<NEportLevels={2,0}>;
    int centre<NEportLevels={2,0}>;
    boolean use_editor => data.use_transform;
    //int do_more<NEportLevels={2,0}>;
  };
  module move_atom<build_dir="avs/src/core/model",
    out_src_file="move_gen.cxx",
    out_hdr_file="move_gen.hxx",
    cxx_hdr_files="avs/src/core/model/insa_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(name+notify+read+req,
			new_view+read+req,
			nselects+read+req+notify,
			index+write,
			fractional+write,
			displacements+write,
			x+read+write,
			y+read+write,
			z+read+write,
			use_editor+write);
    cxxmethod+req update<status=1>(fractional+read+req,
				   displacements+read+req,
				   x+read+req+notify,
				   y+read+req+notify,
				   z+read+req+notify);
    cxxmethod+req change<status=1>(fractional+notify+read+req,
				   displacements+notify+read+req,
				   x+write,
				   y+write,
				   z+write);
    cxxmethod+req generate<status=1>(calc+notify+req,
				     x+write,
				     y+write,
				     z+write);
    cxxmethod+req position<status=1>(centre+notify+req,
				     x+write,
				     y+write,
				     z+write);
    cxxmethod+req edit<status=1>(use_editor+notify+read+req,
				 x+write,
				 y+write,
				 z+write);
    cxxmethod+req accept<status=1>(name+write,
				   index+write,
				   do_more+notify+req);
    string name<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
    int nselects<NEportLevels={2,0}>;
    move_params &data<NEportLevels={2,0}>;
    boolean fractional => data.fractional;
    double x => data.x;
    double y => data.y;
    double z => data.z;
    boolean displacements => data.displacements;
    int calc<NEportLevels={2,0}>;
    int centre<NEportLevels={2,0}>;
    boolean use_editor => data.use_transform;
    int do_more<NEportLevels={2,0}>;
  };
  module list_models<build_dir="avs/src/core/model",
    out_src_file="list_gen.cxx",
    out_hdr_file="list_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req+notify_inst start(trigger+notify+read+req,
				    model_type+read+req,
				    labels+write);
    int trigger<NEportLevels={2,0}>;
    int model_type;
    string labels<NEportLevels={0,2}>[];
  };
  module add_cluster_region<build_dir="avs/src/core/model",
    out_src_file="reg_gen.cxx",
    out_hdr_file="reg_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req update<status=1>(radii+read+req+notify,
				   n+read+req,
				   label+read+req);
    cxxmethod+req accept<status=1>(ok+notify,
				   label+read+req);
    cxxmethod+req cancel<status=1>(remove+notify,
				   label+read+req);
    float radii<NEportLevels={2,0}>[];
    int n<NEportLevels={2,0}>;
    int ok<NEportLevels={2,0}>;
    int remove<NEportLevels={2,0}>;
    string label<NEportLevels={2,0}>;
  };
  module cds_control<build_dir="avs/src/core/model",
    out_src_file="cds_gen.cxx",
    out_hdr_file="cds_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+notify_inst+notify_deinst init(visible+req+read);
    cxxmethod+req read(trigger+req+notify,
		       new_view+read+req,
		       bond_all+read+req);
    int visible<NEportLevels={2,0}>;
    int trigger<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    int bond_all<NEportLevels={2,0}>;
  };
  module save<build_dir="avs/src/core/model",
    src_file="express.cxx",
    cxx_hdr_files="avs/src/core/model/load_gen.hxx",
    out_src_file="save_gen.cxx",
    out_hdr_file="save_gen.hxx"> {
    cxxmethod+req save<status=1>(run+notify,
				 data+read+req);
    int run<NEportLevels={2,0}>;
    LoadStructData &data<NEportLevels={2,0}>;
  };
  group WulffParams {
    float max_dim;
  };
  module wulff<build_dir="avs/src/core/model",
    out_src_file="wlff_gen.cxx",
    out_hdr_file="wlff_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(name+notify+read+req,
			new_view+read+req,
			selection+read+req,
			index+write,
			params+write);
    cxxmethod+req scale<status=1>(params+read+notify+req);
    string name<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    int selection<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
    WulffParams &params<NEportLevels={2,0}>;
  };
  module multilayer<build_dir="avs/src/core/model",
    out_src_file="mlyr_gen.cxx",
    out_hdr_file="mlyr_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(name+notify+read+req,
			new_view+read+req,
			selection+read+req,
			index+write,
			space+write);
    cxxmethod+req position(space+notify+read+req);
    string name<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    int selection<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
    float space<NEportLevels={2,0}>;
  };
  module fill_geometry<build_dir="avs/src/core/model",
    out_src_file="geo_gen.cxx",
    out_hdr_file="geo_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req start(name+notify+read+req,
			new_view+read+req,
			selection+read+req,
			index+write);
    string name<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    int selection<NEportLevels={2,0}>;
    int index<NEportLevels={2,0}>;
  };
};
