
flibrary Prefs<NEeditable=1> {
  module save<build_dir="avs/src/core/prefs",
    src_file="express.cxx",
    out_src_file="save_gen.cxx",
    out_hdr_file="save_gen.hxx"> {
    cxxmethod+req save<status=1>(trigger+notify);
    int trigger<NEportLevels={2,0}>;
  };
  module open_panel<build_dir="avs/src/core/prefs",
    src_file="express.cxx",
    out_src_file="open_gen.cxx",
    out_hdr_file="open_gen.hxx"> {
    cxxmethod+req open<status=1>(trigger+notify,
				 name+read+req);
    int trigger<NEportLevels={2,0}>;
    string name;
  };
};
