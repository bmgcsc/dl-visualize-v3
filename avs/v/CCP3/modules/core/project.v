
flibrary Project<NEeditable=1> {
  module subproject<build_dir="avs/src/core",
    src_file="express.cxx",
    out_src_file="sub_gen.cxx",
    out_hdr_file="sub_gen.hxx"> {
    cxxmethod+req create(make+notify,
			 name+read+req,
			 directory+read+req);
    int make<NEportLevels={2,0}>;
    string name<NEportLevels={2,0}>;
    string directory<NEportLevels={2,0}>;
  };
  module select<build_dir="avs/src/core",
    src_file="express.cxx",
    out_src_file="sel_gen.cxx",
    out_hdr_file="sel_gen.hxx"> {
    cxxmethod+req list<status=1>(do_list+notify+read+req,
				 do_filter+notify,
				 do_all+notify+read+req,
				 option1+read+req,
				 option2+read+req,
				 op+read+req,
				 selection+write,
				 indices+read+write,
				 items+write);
    cxxmethod+req expand<status=1>(do_all+read+req,
				   option1+read+req,
				   option2+read+req,
				   op+read+req,
				   do_expand+notify,
				   selection+read+req,
				   indices+read+write,
				   items+write);
    cxxmethod+req select<status=1>(do_select+notify,
				   selection+read+req,
				   new_view+read+req,
				   indices+read);
    int do_list<NEportLevels={2,0}>;
    int do_filter<NEportLevels={2,0}>;
    int do_all<NEportLevels={2,0}>;
    int option1<NEportLevels={2,0}>;
    int option2<NEportLevels={2,0}>;
    int op<NEportLevels={2,0}>;
    int do_expand<NEportLevels={2,0}>;
    int do_select<NEportLevels={2,0}>;
    int selection<NEportLevels={2,0}>;
    int new_view<NEportLevels={2,0}>;
    int indices[];
    string items<NEportLevels={0,2}>[];
  };
  module project<build_dir="avs/src/core",
    src_file="express.cxx",
    out_src_file="proj_gen.cxx",
    out_hdr_file="proj_gen.hxx"> {
    cxxmethod+req create<status=1>(run_new+notify,
				   name+read+req,
				   directory+read+req,
				   inherit+read+req,
				   atoms+read+req,
				   display+read+req);
    cxxmethod+req load<status=1>(run_load+notify,
				 directory+read+req);
    cxxmethod+req save<status=1>(run_save+notify);
    cxxmethod+req save_as<status=1>(run_save_as+notify,
				    name+read+req,
				    directory+read+req);
    int run_new<NEportLevels={2,0}>;
    int run_load<NEportLevels={2,0}>;
    int run_save<NEportLevels={2,0}>;
    int run_save_as<NEportLevels={2,0}>;
    string name<NEportLevels={2,0}>;
    string directory<NEportLevels={2,0}>;
    int inherit<NEportLevels={2,0}>;
    string atoms<NEportLevels={2,0}>;
    string display<NEportLevels={2,0}>;
  };
  module check_save<build_dir="avs/src/core",
    src_file="express.cxx",
    out_src_file="save_gen.cxx",
    out_hdr_file="save_gen.hxx"> {
    cxxmethod+notify_inst check<status=1>(test+notify+read+req,
					  warn+write);
    int test<NEportLevels={2,0}>;
    int warn<NEportLevels={2,0}>;
  };
};
