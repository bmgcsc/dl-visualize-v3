
flibrary EXCURV<NEeditable=1
#ifndef DLV_NO_DLLS
		,dyn_libs="libCCP3excurv"
#endif // DLV_NO_DLLS
		> {
  module create_ui<build_dir="avs/src/excurv",
    out_src_file="excurv_gen.cxx",
    out_hdr_file="excurv_gen.hxx",
    //need_objs="CCP3.EXCURV.Macros.EXCURV CCP3.Renderers.Base.shells",
    need_objs="CCP3.EXCURV  CCP3.Core_Macros.CalcObjs.calculation CCP3.Renderers.Base.shells CCP3.Core_Modules.Calcs.exec_environ",
    src_file="create_xp.cxx"> {
    cxxmethod+notify_inst create<status=1>(make+req);
    int make<NEportLevels={2,0}>;
  };
};
