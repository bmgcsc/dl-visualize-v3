
flibrary GULP<NEeditable=1
#ifndef DLV_NO_DLLS
	      ,dyn_libs="libCCP3gulp"
#endif // DLV_NO_DLLS
	      > {
  module create_ui<build_dir="avs/src/gulp",
    out_src_file="gulp_gen.cxx",
    out_hdr_file="gulp_gen.hxx",
    //need_objs="CCP3.GULP.Macros.GULP",
    need_objs="CCP3.GULP CCP3.Core_Macros.CalcObjs.calculation CCP3.Core_Modules.Calcs.exec_environ CCP3.Core_Modules.Data.band_paths",
    src_file="create_xp.cxx"> {
    cxxmethod+notify_inst create<status=1>(make+req);
    int make<NEportLevels={2,0}>;
  };
};
