
flibrary Modules<NEeditable=1
#ifndef DLV_NO_DLLS
		 ,dyn_libs="libCCP3gulp"
#endif // DLV_NO_DLLS
		 > {
  // Kind of a copy of CRYSTAL? Good? Bad?
  group StructureData {
    string name;
    string file;
    boolean new_view;
  };
  module load_structure<build_dir="avs/src/gulp",
    out_src_file="str_gen.cxx",
    out_hdr_file="str_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req load<status=1>(data+read+req,
				 do_load+notify+req);
    StructureData &data<NEportLevels={2,0}>;
    int do_load<NEportLevels={2,0}>;
  };
  module extract_structure<build_dir="avs/src/gulp",
    cxx_hdr_files="avs/src/gulp/str_gen.hxx",
    out_src_file="extr_gen.cxx",
    out_hdr_file="extr_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req load<status=1>(data+read+req,
				 do_load+notify+req);
    StructureData &data<NEportLevels={2,0}>;
    int do_load<NEportLevels={2,0}>;
  };
  module load_bands<build_dir="avs/src/gulp",
    out_src_file="disp_gen.cxx",
    out_hdr_file="disp_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req load<status=1>(filename+read+req,
				 do_load+notify+req);
    string filename<NEportLevels={2,0}>;
    int do_load<NEportLevels={2,0}>;
  };
  module load_DOS<build_dir="avs/src/gulp",
    out_src_file="dos_gen.cxx",
    out_hdr_file="dos_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req load<status=1>(filename+read+req,
				 do_load+notify+req);
    string filename<NEportLevels={2,0}>;
    int do_load<NEportLevels={2,0}>;
  };
  module load_vectors<build_dir="avs/src/gulp",
    out_src_file="vec_gen.cxx",
    out_hdr_file="vec_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req load<status=1>(filename+read+req,
				 do_load+notify+req);
    string filename<NEportLevels={2,0}>;
    int do_load<NEportLevels={2,0}>;
  };
  module load_trajectory<build_dir="avs/src/gulp",
    out_src_file="traj_gen.cxx",
    out_hdr_file="traj_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req load<status=1>(filename+read+req,
				 do_load+notify+req);
    string filename<NEportLevels={2,0}>;
    int do_load<NEportLevels={2,0}>;
  };
  module save_structure<build_dir="avs/src/gulp",
    out_src_file="save_gen.cxx",
    out_hdr_file="save_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req save<status=1>(filename+read+req,
				 do_save+notify+req);
    string filename<NEportLevels={2,0}>;
    int do_save<NEportLevels={2,0}>;
  };
  group gen_params {
    int calc_props;
    int gibbs_fe;
    int calc_phonons;
    int set_pressure;
    float pressure;
    int set_temperature;
    float temperature;
    int shrink1;
    int shrink2;
    int shrink3;
    int calc_eigenvectors;
    int npoints;
    string bandpath;
  };
  group opt_params {
    int constraint;
  };
  group MD_params {
    int ensemble;
    int equilib;
    int prod;
    float timestep;
    int sample;
    float thermostat;
    float barostat;
    int traj_step;
  };
  group lib_params {
    string libname;
    int use_shells;
  };
  group params {
    int calc_type;
    gen_params general;
    opt_params optimise;
    MD_params md;
    lib_params potentials;
    boolean user_calc;
    string user_dir;
  };
  module Run_GULP<build_dir="avs/src/gulp",
    out_src_file="run_gen.cxx",
    out_hdr_file="run_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx",
    src_file="express.cxx"> {
    cxxmethod+req execute<status=1>(run+notify+req,
				    data+read+req,
				    job_data+read+req);
    int run<NEportLevels={2,0}>;
    params &data<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
  module kpath_info<build_dir="avs/src/gulp",
    out_src_file="path_gen.cxx",
    out_hdr_file="path_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req+notify_inst info<status=1>(model_type+notify,
					     centre+write,
					     lattice+write);
    int model_type<NEportLevels={2,0}>;
    int centre<NEportLevels={0,2}>;
    int lattice<NEportLevels={0,2}>;
  };
};
