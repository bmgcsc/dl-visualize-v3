
flibrary IMAGE<NEeditable=1
#ifndef DLV_NO_DLLS
	     ,dyn_libs="libCCP3im"
#endif // DLV_NO_DLLS
	     > {
  module create_ui<build_dir="avs/src/image",
    out_src_file="im_gen.cxx",
    out_hdr_file="im_gen.hxx",
    need_objs="CCP3.IMAGE CCP3.Core_Macros.CalcObjs.calculation CCP3.Renderers.Base.outline",
    src_file="create_xp.cxx"> {
    cxxmethod+notify_inst create<status=1>(make+req);
    int make<NEportLevels={2,0}>;
  };
};
