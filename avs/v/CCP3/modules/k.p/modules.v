
flibrary Modules<NEeditable=1
#ifndef DLV_NO_DLLS
		 ,dyn_libs="libCCP3k_p"
#endif // DLV_NO_DLLS
		 > {
  // Kind of a copy of CRYSTAL? Good? Bad?
  group StructureData {
    string name;
    string file;
    boolean new_view;
  };
  module load_structure<build_dir="avs/src/k.p",
    out_src_file="str_gen.cxx",
    out_hdr_file="str_gen.hxx",
    need_objs="CCP3.Renderers.Model.Outline",
    src_file="express.cxx"> {
    cxxmethod+req load<status=1>(data+read+req,
				 do_load+notify+req);
    StructureData &data<NEportLevels={2,0}>;
    int do_load<NEportLevels={2,0}>;
  };
  
  group LoadData{
    
    string filename;
    int N_x; 
    int N_y;
    int N_z;
    float L_x;
    float L_y;
    float L_z;
  
  };
  module load_data<build_dir="avs/src/k.p",
    out_src_file="load_gen.cxx",
    out_hdr_file="load_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req load<status=1>(data+read+req,
				 do_load+notify+req);
    LoadData &data<NEportLevels={2,0}>;
    int do_load<NEportLevels={2,0}>;
  };

  group InputData{
        
    float base;
    float height;
    int shape;
    float c;
    boolean WL;
    int n;
    float WLthickness;
    boolean lat;
    int dim;
    int LatType;
    int a1, a2, a3; 
    int dir;
  };
  module create_structure<build_dir="avs/src/k.p",
    out_src_file="mak_gen.cxx",
    out_hdr_file="mak_gen.hxx",
    need_objs="CCP3.Renderers.Model.Outline",
    src_file="express.cxx"> {
    cxxmethod+req create<status=1>(data+read+req,
				 do_load+notify+req);
    InputData &data<NEportLevels={2,0}>;
    int do_load<NEportLevels={2,0}>;
  };
  module axis<build_dir="avs/src/k.p",
    out_src_file="axis_gen.cxx",
    out_hdr_file="axis_gen.hxx",
    src_file="express.cxx"> {
      cxxmethod axis(do_load+notify+req);
      int do_load<NEportLevels={2,0}>;
   };		    
};
