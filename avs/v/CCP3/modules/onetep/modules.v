
flibrary Modules<NEeditable=1
#ifndef DLV_NO_DLLS
		 ,dyn_libs="libCCP3onetep"
#endif // DLV_NO_DLLS
		 > {
  group StructureData {
    string name;
    string file;
    boolean new_view;
  };
  module load_structure<build_dir="avs/src/onetep",
    out_src_file="str_gen.cxx",
    out_hdr_file="str_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req load<status=1>(data+read+req,
				 do_load+notify+req);
    StructureData &data<NEportLevels={2,0}>;
    int do_load<NEportLevels={2,0}>;
  };
  module load_data<build_dir="avs/src/onetep",
    out_src_file="load_gen.cxx",
    out_hdr_file="load_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req load<status=1>(file_type+read+req,
				 filename+read+req,
				 load_model+read+req,
				 do_load+notify+req);
    int file_type<NEportLevels={2,0}>;
    string filename<NEportLevels={2,0}>;
    int load_model<NEportLevels={2,0}>;
    int do_load<NEportLevels={2,0}>;
  };
  module save_cell_file<build_dir="avs/src/onetep",
    out_src_file="save_gen.cxx",
    out_hdr_file="save_gen.hxx",
    src_file="express.cxx"> {
    cxxmethod+req save<status=1>(filename+read+req,
				 do_save+notify+req);
    string filename<NEportLevels={2,0}>;
    int do_save<NEportLevels={2,0}>;
  };
  group basis_data {
    int ngwfs;
    float radius;
    string filename;
  };
  group TolParams {
    float cutoff;
    float grid;
    float kernel;
  };
  group HamiltonianParams {
    int spin_value;
    boolean set_spin;
    boolean unrestricted;
    int dispersion;
    int functional;
  };
  group PropParams {
    int homo_plot;
    int lumo_plot;
    int num_eigs;
    boolean do_dos;
    float dos_smear;
    boolean density_plot;
    boolean mulliken_calc;
    float mulliken_neighbours;
  };
  group OptParams {
    int method;
    int max_iter;
    float energy_tol;
    float force_tol;
    float disp_tol;
  };
  group SCFParams {
    int task;
    //int analyse;
    TolParams Tolerances;
    HamiltonianParams Hamiltonian;
    PropParams Properties;
    OptParams Optimize;
    /*
    PrintParams Print;
    JobParams Job;
    NebParams neb;
    */
    boolean user_calc;
    string user_dir;
  };
  module Run_SCF<build_dir="avs/src/onetep",
    out_src_file="scf_gen.cxx",
    out_hdr_file="scf_gen.hxx",
    cxx_hdr_files="avs/src/core/jobs/data.hxx",
    src_file="express.cxx"> {
    cxxmethod+req create<status=1>(make+notify+req,
				   data+read+req);
    cxxmethod+req inherit_pseudo<status=1>(inherit+notify);
    cxxmethod+req add_pseudo<status=1>(bdata+read+req,
				       use_default+read+req,
				       all_pseudos+write,
				       do_pseudo+notify+req);
    cxxmethod+req execute<status=1, weight=2>(run+notify+req,
					      data+read+req,
					      job_data+read+req);
    cxxmethod+req stop<status=1, weight=2>(cancel+notify+req);
    int make<NEportLevels={2,0}>;
    int run<NEportLevels={2,0}>;
    int cancel<NEportLevels={2,0}>;
    int inherit<NEportLevels={2,0}>;
    SCFParams &data<NEportLevels={2,0}>;
    basis_data &bdata<NEportLevels={2,0}>;
    int use_default<NEportLevels={2,0}>;
    int all_pseudos<NEportLevels={0,2}>;    
    int do_pseudo<NEportLevels={2,0}>;
    CCP3.Core_Modules.JobPrefs.job_data &job_data<NEportLevels={2,0}>;
  };
};
