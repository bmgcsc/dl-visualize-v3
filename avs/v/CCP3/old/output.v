
flibrary OutputView<NEeditable=1> {
  // Todo - more DLV UIobjs?, espec filebrowser?
  macro OutputVRML<NEx=297.,NEy=121.> {
    CCP3.Core_Macros.Core.preferences &prefs;
    GMOD.instancer instancer {
      Group => <-.OutputVRML;
      active = 2;
    };
    GDM.OutputVRML OutputVRML<instanced=0> {
      link prefs => <-.prefs;
      macro vrmlUI<NEx=66,NEy=231> {
	CCP3.Core_Macros.Core.preferences &prefs => <-.prefs;
	VUIFileSelector vrmlFile {
	  parent => <-.CCP3shell.UIpanel;
	  link color => <-.prefs.colour;
	  link fontAttributes => <-.prefs.fontAttributes;
	  x = 4;
	  y = 4;
	  width => (<-.CCP3shell.UIpanel.clientWidth - 8);
	  title = "VRML File";
	  file<NEportLevels={3,0}> => <-.<-.vrmlOutput.file;
	  filetype = "wrl";
	  fileFrame {
	    &color => <-.color;
	    &fontAttributes => <-.fontAttributes;
	  };
	  fileTitle {
	    &color => <-.color;
	    &fontAttributes => <-.fontAttributes;
	  };
	  fileText {
	    updateMode = 2;
	    &color => <-.color;
	    &fontAttributes => <-.fontAttributes;
	  };
	  fileBrowse {
	    &color => <-.color;
	    &fontAttributes => <-.fontAttributes;
	  };
	  fileFB {
	    &color => <-.color;
	    &fontAttributes => <-.fontAttributes;
	  };
	};
	VUIRadioBox vrmlRB {
	  parent => <-.CCP3shell.UIpanel;
	  link color => <-.prefs.colour;
	  link fontAttributes => <-.prefs.fontAttributes;
	  options => {"VRML 1","VRML 2"};
	  x => <-.vrmlFile.x;
	  y+nres => ((<-.vrmlFile.y + <-.vrmlFile.height) + 4);
	  width => <-.vrmlFile.width;
	  selectedItem<NEportLevels={3,0}> => <-.<-.vrmlOutput.protocol;
	  label = "Protocol";
	  RBframe {
	    &color => <-.color;
	    &fontAttributes => <-.fontAttributes;
	  };
	  RBtitle {
	    &color => <-.color;
	    &fontAttributes => <-.fontAttributes;
	  };
	  RBradioBox {
	    &color => <-.color;
	    &fontAttributes => <-.fontAttributes;
	  };
	};
	VUIField vrmlFloatP {
	  parent => <-.CCP3shell.UIpanel;
	  link color => <-.prefs.colour;
	  link fontAttributes => <-.prefs.fontAttributes;
	  x => <-.vrmlRB.x;
	  y+nres => ((<-.vrmlRB.y + <-.vrmlRB.height) + 4);
	  width => (<-.vrmlRB.width + 1);
	  label = "Float Precision";
	  value<NEportLevels={3,0}> => <-.<-.vrmlOutput.floatPrec;
	  min = 1.;
	  max = 8.;
	  mode = 1;
	  UIframe {
	    &color => <-.color;
	    &fontAttributes => <-.fontAttributes;
	  };
	  UIfield {
	    &color => <-.color;
	    &fontAttributes => <-.fontAttributes;
	  };
	  UIlabel {
	    &color => <-.color;
	    &fontAttributes => <-.fontAttributes;
	  };
	};
	VUIField vrmlColorP {
	  parent => <-.CCP3shell.UIpanel;
	  link color => <-.prefs.colour;
	  link fontAttributes => <-.prefs.fontAttributes;
	  x => <-.vrmlFloatP.x;
	  y+nres => ((<-.vrmlFloatP.y + <-.vrmlFloatP.height) + 4);
	  width => (<-.vrmlFloatP.width - 1);
	  label = "Color Precision";
	  value<NEportLevels={3,0}> => <-.<-.vrmlOutput.colorPrec;
	  min = 1.;
	  max = 8.;
	  mode = 1;
	  UIframe {
	    &color => <-.color;
	    &fontAttributes => <-.fontAttributes;
	  };
	  UIfield {
	    &color => <-.color;
	    &fontAttributes => <-.fontAttributes;
	  };
	  UIlabel {
	    &color => <-.color;
	    &fontAttributes => <-.fontAttributes;
	  };
	};
	VUIToggle vrmlIndent {
	  parent => <-.CCP3shell.UIpanel;
	  link color => <-.prefs.colour;
	  link fontAttributes => <-.prefs.fontAttributes;
	  x => <-.vrmlColorP.x;
	  y+nres => ((<-.vrmlColorP.y + <-.vrmlColorP.height) + 4);
	  width => <-.vrmlColorP.width;
	  set<NEportLevels={3,0}> => <-.<-.vrmlOutput.indent;
	  label = "Indent format";
	  ToggleFrame {
	    &color => <-.color;
	    &fontAttributes => <-.fontAttributes;
	  };
	  Toggle {
	    &color => <-.color;
	    &fontAttributes => <-.fontAttributes;
	  };
	};
	VUIToggle vrmlDynamic {
	  parent => <-.CCP3shell.UIpanel;
	  link color => <-.prefs.colour;
	  link fontAttributes => <-.prefs.fontAttributes;
	  x => <-.vrmlIndent.x;
	  y+nres => ((<-.vrmlIndent.y + <-.vrmlIndent.height) + 4);
	  width => <-.vrmlIndent.width;
	  set<NEportLevels={3,0}> => <-.<-.vrmlView.mode;
	  label = "Dynamic";
	  ToggleFrame {
	    &color => <-.color;
	    &fontAttributes => <-.fontAttributes;
	  };
	  Toggle {
	    &color => <-.color;
	    &fontAttributes => <-.fontAttributes;
	  };
	};
	UIbutton refreshButton {
	  x => <-.vrmlDynamic.x;
	  y => ((<-.vrmlDynamic.y + <-.vrmlDynamic.height) + 4);
	  width => <-.vrmlDynamic.width;
	  visible => (1 - <-.vrmlDynamic.set);
	  parent => <-.CCP3shell.UIpanel;
	  label => "Write VRML";
	  do = 0;
	  &color => <-.prefs.colour;
	  &fontAttributes => <-.prefs.fontAttributes;
	};
	GMOD.copy_on_change copy_on_change {
	  trigger => <-.<-.vrmlOutput.file;
	  input = 1;
	  output = 0;
	  on_inst = 0;
	};
	GMOD.parse_v doVRML {
	  v_commands = "$push\n            vrmlView.renderer = 6;\n          $pop\n          vrmlView.refresh = 1;";
	  trigger => (<-.refreshButton.do | <-.copy_on_change.output);
	  on_inst = 0;
	  relative => <-.<-;
	};
	CCP3.Core_Macros.UI.UIobjs.DLVshell CCP3shell<NEx=594.,NEy=22.> {
	  color => <-.prefs.colour;
	  fontAttributes => <-.prefs.fontAttributes;
	  title = "Save VRML";
	  UIshell {
	    height = 450;
	  };
	};
      };
    };
  };
  macro OutputImage<NEx=275.,NEy=121.> {
    CCP3.Core_Macros.Core.preferences &prefs;
    GMOD.instancer instancer<NEx=407.,NEy=121.> {
      Group => <-.macro;
      active = 2;
    };
    macro macro<instanced=0,NEx=198.,NEy=165.,need_objs="UImessageDialog"> {
      link prefs => <-.prefs;
      GDM.OutputField OutputField<NEx=198.,NEy=209.> {
	CCP3.Core_Macros.Core.preferences &prefs => <-.prefs;
	view_in<NEportLevels={3,1}>;
	macro output_field<NEhelpTopic="old_OutputField"> {
	  CCP3.Core_Macros.Core.preferences &prefs => <-.prefs;
	  UIrenderView+nres &render_view<NEportLevels={2,1}> =>
	    <-.view_in.render_view;
	  GDview_templ &view_in<NEportLevels={2,1}> => <-.view_in;
	  CCP3.Core_Macros.UI.UIobjs.DLViSlider UIslider {
	    y = 5;
	    parent => <-.link;
	    max = 4096;
	    min = 128;
	    value = 512;
	    title => "Width";
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  CCP3.Core_Macros.UI.UIobjs.DLViSlider UIslider#1 {
	    y => ((<-.UIslider.y + <-.UIslider.height) + 5);
	    parent => <-.link;
	    max = 4096;
	    min = 128;
	    value = 512;
	    title => "Height";
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  CCP3.Core_Macros.UI.UIobjs.DLVslider_typein slider1 {
	    slider => <-.UIslider;
	    color => <-.prefs.colour;
	    fontAttributes => <-.prefs.fontAttributes;
	    // This fixes bug - don't know why
	    valEditor {
	      UI {
		editor_shell {
		  x => ;
		  y => ;
		};
	      };
	    };
	  };
	  CCP3.Core_Macros.UI.UIobjs.DLVslider_typein slider2 {
	    slider => <-.UIslider#1;
	    color => <-.prefs.colour;
	    fontAttributes => <-.prefs.fontAttributes;
	    // This fixes bug - don't know why
	    valEditor {
	      UI {
		editor_shell {
		  x => ;
		  y => ;
		};
	      };
	    };
	  };
	  UIrenderView UIrenderView {
	    y = 0;
	    width => <-.UIslider.value;
	    height => <-.UIslider#1.value;
	    visible = 0;
	    parent => <-.render_view.parent;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  BestVirtPal VirtPal {
	    vclass => view_in.virtpal.vclass;
	    vid => view_in.virtpal.vid;
	    share = 0;
	    gamma = 1.;
	    cube_size => view_in.virtpal.cube_size;
	  };
	  DefaultFieldOutput Output<export_all=3> {
	    type<NEvisible=0>;
	    output<NEportLevels={0,4}>;
	  };
	  DefaultView View {
	    back_col => view_in.back_col;
	    light_info => view_in.light_info;
	    cameras => view_in.cameras;
	    output => <-.Output;
	    blend_mode => view_in.blend_mode;
	    handle => <-.UIrenderView.handle;
	    virtpal => <-.VirtPal;
	    output_enabled = 1;
	    refresh => (<-.<-.<-.Write_Image.write_image_ui.write_oneshot.do |
	                <-.copy_on_change.output);
	    mode = "Manual";
	  };
	  GMOD.copy_on_change copy_on_change<NEx=220.,NEy=165.> {
	    trigger => <-.<-.<-.Write_Image.write_image_ui.filename;
	    input = 1;
	    output = 0;
	    on_inst = 0;
	  };
	  link link<NEportLevels={3,1},NEx=363.,NEy=55.> =>
					 <-.<-.CCP3shell.UIpanel;
	};
      };
      //MODS.Write_Image Write_Image<NEx=330.,NEy=308.> {
      macro Write_Image<NEx=330.,NEy=308.> {
	link prefs => <-.prefs;
	ilink in<export_all=2> => <-.OutputField.output_field.Output.output;
	GMOD.parse_v parse_v<NEx=308.,NEy=77.> {
	  v_commands = "write_image_ui.file_browser.filename = ;";
	  on_inst = 0;
	  relative => <-;
	};
	macro write_image_ui {
	  CCP3.Core_Macros.Core.preferences &prefs => <-.prefs;
	  link panel<NEportLevels={3,1}> => <-.<-.CCP3shell.UIpanel;
	  UIlabel Image_Filename {
	    y = 110;
	    width = 200;
	    parent => <-.panel;
	    alignment = "left";
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UItext file_name {
	    y => ((<-.Image_Filename.y + <-.Image_Filename.height) + 5);
	    width = 170;
	    height = 34;
	    parent => panel;
	    text => <-.filename;
	    showLastPosition = 1;
	    updateMode = 3;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIbutton visible {
	    x => ((<-.file_name.x + <-.file_name.width) + 5);
	    y => <-.file_name.y;
	    width = 75;
	    height => <-.file_name.height;
	    parent => <-.panel;
	    label => "Browse...";
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIbutton write_oneshot {
	    y => ((<-.visible.y + <-.visible.height) + 8);
	    width => <-.<-.<-.CCP3shell.UIpanel.clientWidth;
	    parent => <-.panel;
	    label => "Write File";
	    visible = 0;
	    do = 0;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UItoggle overwrite_toggle {
	    y => ((<-.write_oneshot.y + <-.write_oneshot.height) + 4);
	    width => <-.<-.<-.CCP3shell.UIpanel.clientWidth;
	    parent => <-.panel;
	    label => "Overwrite";
	    set<NEportLevels={2,0}>;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIfileSB file_browser {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	    title => "Write IMAGE Filename";
	    searchPattern = "";
	    GMOD.copy_on_change copy_on_change {
	      trigger => <-.<-.visible.do;
	      input => <-.<-.visible.do;
	      output => <-.visible;
	    };
	  };
	  UIframe format_frame {
	    y => ((<-.overwrite_toggle.y + <-.overwrite_toggle.height) + 4);
	    width => <-.<-.<-.CCP3shell.UIpanel.clientWidth;
	    height => ((<-.format_rb.y + <-.format_rb.height) + 6);
	    parent => <-.panel;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIlabel format_label {
	    x = 4;
	    y = 0;
	    width => <-.<-.<-.CCP3shell.UIpanel.clientWidth;
	    parent => <-.format_frame;
	    label => "File Format";
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIoption format_jpeg {
	    active = 1;
	    label => "JPEG";
	    message = "JFIF: JPEG File Interchange Format";
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIoption format_pbm {
	    active = 1;
	    label => "PBM";
	    message = "Portable Bitmap Utilities";
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIoption format_tiff {
	    active = 1;
	    label => "TIFF";
	    message = "Tag Image File Format";
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIoption format_png {
	    active = 1;
	    label => "PNG";
	    message = "Portable Network Graphics Format";
	  };
	  UIradioBox format_rb {
	    x = 4;
	    y => ((<-.format_label.y + <-.format_label.height) + 4);
	    width => <-.<-.<-.CCP3shell.UIpanel.clientWidth;
	    parent => <-.format_frame;
	    cmdList => {<-.format_jpeg,<-.format_pbm,
			<-.format_tiff,format_png};
	    selectedItem = 0;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIframe color_frame {
	    y => ((<-.format_frame.y + <-.format_frame.height) + 4);
	    width => <-.<-.<-.CCP3shell.UIpanel.clientWidth;
	    height => ((<-.color_rb.y + <-.color_rb.height) + 6);
	    parent => <-.panel;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIlabel color_label {
	    x = 4;
	    y = 0;
	    width => <-.<-.<-.CCP3shell.UIpanel.clientWidth;
	    parent => <-.color_frame;
	    label => "Color Type";
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIoption color_rgb {
	    active = 1;
	    label => "RGB";
	    message = "Red, Green, Blue";
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIoption color_grey {
	    active = 1;
	    label => "Greyscale";
	    message = "Grey levels";
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIoption color_bw {
	    active => switch((<-.format_rb.selectedItem + 1),0,1,0,0);
	    label => "BW";
	    message = "Black, White";
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIradioBox color_rb {
	    x = 4;
	    y => ((<-.color_label.y + <-.color_label.height) + 4);
	    width => <-.<-.<-.CCP3shell.UIpanel.clientWidth;
	    parent => <-.color_frame;
	    cmdList => {<-.color_rgb,
			<-.color_grey,<-.color_bw};
	    selectedItem => switch((<-.format_rb.selectedItem + 1),0,0,0,0);
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIframe compression_frame {
	    y => ((<-.color_frame.y + <-.color_frame.height) + 4);
	    width => <-.<-.<-.CCP3shell.UIpanel.clientWidth;
	    height => ((<-.quality.y + <-.quality.height) + 6);
	    parent => <-.panel;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIlabel compression_label {
	    x = 4;
	    y = 0;
	    width => <-.<-.<-.CCP3shell.UIpanel.clientWidth;
	    parent => <-.compression_frame;
	    label => "Compression Type";
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIoption compression_none {
	    active => switch((<-.format_rb.selectedItem + 1),0,1,1,0);
	    label => "none";
	    message = "no compression";
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIoption compression_rle {
	    active => switch((<-.format_rb.selectedItem + 1),0,0,1,0);
	    label => "RLE";
	    message = "Run Length Encoded";
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIoption compression_lzw {
	    active => switch((<-.format_rb.selectedItem + 1),0,0,1,0);
	    label => "LZW";
	    message = "LZW Compression";
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIoption compression_jpeg {
	    active => switch((<-.format_rb.selectedItem + 1),1,0,0,0);
	    label => "JPEG";
	    message = "JPEG Compression";
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIoption compression_ccitt3 {
	    active => switch((<-.format_rb.selectedItem + 1),0,0,1,0);
	    label => "CCITT Group 3";
	    message = "CCITT T.4 Compression";
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIoption compression_ccitt4 {
	    active => switch((<-.format_rb.selectedItem + 1),0,0,1,0);
	    label => "CCITT Group 4";
	    message = "CCITT T.6 Compression";
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIoption compression_zlib {
	    active => switch((<-.format_rb.selectedItem + 1),0,0,0,1);
	    label => "Zlib";
	    message = "Zlib Compression";
	  };
	  UIradioBox compression_rb {
	    x = 4;
	    y => ((<-.compression_label.y + <-.compression_label.height) + 4);
	    width => <-.<-.<-.CCP3shell.UIpanel.clientWidth;
	    parent => <-.compression_frame;
	    cmdList => {
	      <-.compression_none,<-.compression_rle,<-.compression_lzw,
	      <-.compression_jpeg,<-.compression_ccitt3,<-.compression_ccitt4,
	      <-.compression_zlib
	    };
	    selectedItem => switch((<-.format_rb.selectedItem + 1),3,0,1,6);
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIslider quality {
	    x = 4;
	    y => ((<-.compression_rb.y + <-.compression_rb.height) + 4);
	    width => <-.<-.<-.CCP3shell.UIpanel.clientWidth;
	    active => switch((<-.format_rb.selectedItem + 1),1,0,0,0);
	    parent => <-.compression_frame;
	    min = 0.;
	    max = 100.;
	    value = 75.;
	    title => "Quality";
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  olink filename<export=3> => .file_browser.filename;
	  olink output<export=3> => .write_oneshot.do;
	  olink overwrite<export=3> => .overwrite_toggle.set;
	  olink format<export=3> => .format_rb.selectedItem;
	  olink colortype<export=3> => .color_rb.selectedItem;
	  olink compresstype<export=3> => .compression_rb.selectedItem;
	  olink compressqual<export=3> => .quality.value;
	  int flip = 0;
	  int filetype = 0;
	  int depth = 0;
	  int reducetype = 0;
	};
	CCP3.Version2.Modules.Misc.CCP3write_image CCP3write_image {
	  in => <-.in;
	  filename => write_image_ui.filename;
	  output => write_image_ui.output;
	  flip => write_image_ui.flip;
	  format => switch((<-.write_image_ui.format + 1), 3, 4, 7, 8);
	  filetype => write_image_ui.filetype;
	  depth => write_image_ui.depth;
	  colortype => write_image_ui.colortype;
	  compresstype => write_image_ui.compresstype;
	  compressqual => write_image_ui.compressqual;
	  reducetype => write_image_ui.reducetype;
	  overwrite => write_image_ui.overwrite;
	};
      };
      CCP3.Core_Macros.UI.UIobjs.DLVshell CCP3shell<NEx=231.,NEy=66.> {
	color => <-.prefs.colour;
	fontAttributes => <-.prefs.fontAttributes;
	title = "Save Image";
	UIshell {
	  width = 260;
	  height = 768;
	};
      };
    };
  };
  macro Animator<NEx=253.,NEy=99.> {
    CCP3.Core_Macros.Core.preferences &prefs;
    GMOD.instancer instancer {
      Group => <-.Anim;
      active = 2;
    };
    macro Anim<instanced=0> {
      CCP3.Core_Macros.Core.preferences &prefs => <-.prefs;
      ANIM_MACROS.animParam animParam<export_all=2,NEx=288.,NEy=88.>;
      ANIM_MACROS.animUI animUI<NEx=396.,NEy=187.> {
	link prefs => <-.prefs;
	UIparent => <-.CCP3shell.UIpanel;
	KeyframeControls {
	  CCP3.Core_Macros.Core.preferences &prefs => <-.prefs;
	  KeyframeFrame {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  KeyframeControls {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  Add {
	    do => <-.<-.<-.animParam.add_frame;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	    toolTip {
	      enable = "never";
	    };
#endif // DLV_TOOLTIP_OK
	  };
	  Delete {
	    do => <-.<-.<-.animParam.delete_frame;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	    toolTip {
	      enable = "never";
	    };
#endif // DLV_TOOLTIP_OK
	  };
	  Complete {
	    do => <-.<-.<-.animParam.complete_frames;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	    toolTip {
	      enable = "never";
	    };
#endif // DLV_TOOLTIP_OK
	  };
	  Clear {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	    toolTip {
	      enable = "never";
	    };
#endif // DLV_TOOLTIP_OK
	  };
	  ClearQuestion {
	    ok => <-.<-.<-.animParam.clear_frames;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  ReScan {
	    do => <-.<-.<-.animParam.re_scan;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	    toolTip {
	      enable = "never";
	    };
#endif // DLV_TOOLTIP_OK
	  };
	  ReInitialize {
	    do => <-.<-.<-.animParam.re_init;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	    toolTip {
	      enable = "never";
	    };
#endif // DLV_TOOLTIP_OK
	  };
	  number_of_frames =>
	    array_size(<-.<-.animCompute.AnimControl.frameTimes);
	  NumberOfFrames {
	    label => ("Total Frames: " + <-.number_of_frames);
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  FrameDelta {
	    value => <-.<-.<-.animParam.frame_delta;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  TimesList {
	    strings => <-.TimeStrings;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  loopCount => <-.<-.animCompute.loop.count;
	  CurrentTime {
	    value => <-.<-.<-.animParam.current_time;
	    link color => <-.prefs.colour;
	    link fontAttributes => <-.prefs.fontAttributes;
	    Dial {
	      &color => <-.color;
	      &fontAttributes => <-.fontAttributes;
	    };
	    Field {
	      &color => <-.color;
	      &fontAttributes => <-.fontAttributes;
	    };
	    Down {
	      &color => <-.color;
	      &fontAttributes => <-.fontAttributes;
	    };
	    Up {
	      &color => <-.color;
	      &fontAttributes => <-.fontAttributes;
	    };
	  };
	  NewTime {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	    value => <-.<-.<-.animParam.new_time;
	  };
	  Move {
	    do => <-.<-.<-.animParam.move_frame;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	    toolTip {
	      enable = "never";
	    };
#endif // DLV_TOOLTIP_OK
	  };
	  InterpolatorList {
	    selectedItem => <-.<-.<-.animParam.interpolator_selected;
	    strings => <-.<-.<-.animParam.interpolator_list;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  string TimeStrings[] =>
	    str_format("%5.3f",<-.<-.animCompute.AnimControl.frameTimes);
	  ReInitError {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  FrameDeltaLabel {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  FrameTimes {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  NewTimeLabel {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIlabel#3 {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	};
	PlaybackControls {
	  CCP3.Core_Macros.Core.preferences &prefs => <-.prefs;
	  PlaybackFrame {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  RunBack {
	    do => <-.<-.<-.animParam.run_back;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	    toolTip {
	      enable = "never";
	    };
#endif // DLV_TOOLTIP_OK
	  };
	  StepBack {
	    do => <-.<-.<-.animParam.step_back;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	    toolTip {
	      enable = "never";
	    };
#endif // DLV_TOOLTIP_OK
	  };
	  StepForward {
	    do => <-.<-.<-.animParam.step;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	    toolTip {
	      enable = "never";
	    };
#endif // DLV_TOOLTIP_OK
	  };
	  RunForward {
	    do => <-.<-.<-.animParam.run;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	    toolTip {
	      enable = "never";
	    };
#endif // DLV_TOOLTIP_OK
	  };
	  RunOptions {
	    selectedItem => <-.<-.<-.animParam.run_options;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  Current {
	    label => str_format("%5.3f",<-.<-.<-.animCompute.loop.count);
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  ParseStop {
	    v_commands = "animParam.run = 0;";
	  };
	  GoToStart {
	    do => <-.<-.<-.animParam.reset;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	    toolTip {
	      enable = "never";
	    };
#endif // DLV_TOOLTIP_OK
	  };
	  GoToEnd {
	    do => <-.<-.<-.animParam.reset_back;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	    toolTip {
	      enable = "never";
	    };
#endif // DLV_TOOLTIP_OK
	  };
	  UIfield {
	    value => <-.<-.<-.animParam.frames_per_second;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  UIlabel {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  Stop {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	    toolTip {
	      enable = "never";
	    };
#endif // DLV_TOOLTIP_OK
	  };
	  FPS {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	};
	InterpolatorEditorControls {
	  CCP3.Core_Macros.Core.preferences &prefs => <-.prefs;
	  InterpolatorEditorDialog {
	    visible => <-.<-.<-.animParam.interp_editor_visible;
	    ok => <-.<-.<-.animParam.interp_editor_ok;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  InterpolatorName {
	    visible => <-.<-.<-.animParam.interp_editor_visible;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  InterpType {
	    visible => <-.<-.<-.animParam.interp_editor_visible;
	    selectedItem => <-.<-.<-.animParam.interp_type;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  RotMethod {
	    visible => <-.<-.<-.animParam.fFrame3_visible;
	    selectedItem => <-.<-.<-.animParam.quat_or_euler;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  TimesLabel {
	    visible => <-.<-.<-.animParam.interp_editor_visible;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  InterpolatorTimesList {
	    visible => <-.<-.<-.animParam.interp_editor_visible;
	    selectedItem => <-.<-.<-.animParam.time_selected;
	    strings => <-.InterpolatorTimesStrings;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  Apply {
	    do => <-.<-.<-.animParam.interp_editor_apply;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  FScalarPanel {
	    visible => <-.<-.<-.animParam.fFrame1_visible;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  fScalar {
	    value => <-.<-.<-.animParam.fscalar;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  IScalarPanel {
	    visible => <-.<-.<-.animParam.iFrame1_visible;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  iScalar {
	    value => <-.<-.<-.animParam.iscalar;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  FVectorPanel {
	    visible => <-.<-.<-.animParam.fFrame2_visible;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  fValue1 {
	    value => <-.<-.<-.animParam.fvalue1;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  fValue2 {
	    value => <-.<-.<-.animParam.fvalue2;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  fValue3 {
	    value => <-.<-.<-.animParam.fvalue3;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  IVectorPanel {
	    visible => <-.<-.<-.animParam.iFrame2_visible;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  iValue1 {
	    value => <-.<-.<-.animParam.ivalue1;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  iValue2 {
	    value => <-.<-.<-.animParam.ivalue2;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  iValue3 {
	    value => <-.<-.<-.animParam.ivalue3;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  FMatrixPanel {
	    visible => <-.<-.<-.animParam.fFrame3_visible;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  MatValue1 {
	    value => <-.<-.<-.animParam.mat_value1;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  MatValue2 {
	    value => <-.<-.<-.animParam.mat_value2;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  MatValue3 {
	    value => <-.<-.<-.animParam.mat_value3;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  MatValue4 {
	    value => <-.<-.<-.animParam.mat_value4;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  MatValue5 {
	    value => <-.<-.<-.animParam.mat_value5;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  MatValue6 {
	    value => <-.<-.<-.animParam.mat_value6;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  string InterpolatorTimesStrings[] =>
	    str_format("%5.3f",<-.<-.animCompute.AnimControl.interpTimes);
	  FScalarLabel {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  FScalarFrame {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  IScalarLabel {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  IScalarFrame {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  InterpolatorEditorFrame {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  InterpolatorEditorPanel {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  FInterpLabel {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  FVectorFrame {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  IInterpLabel {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  IVectorFrame {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  InterpLabel2 {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  FMatrixFrame1 {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  InterpLabel3 {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  FMatrixFrame2 {
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	};
      };
      ANIM_MACROS.animCompute animCompute<NEx=176.,NEy=187.> {
	loop {
	  reset => <-.<-.animParam.reset;
	  reset_back => <-.<-.animParam.reset_back;
	  run => <-.<-.animParam.run;
	  run_back => <-.<-.animParam.run_back;
	  step => <-.<-.animParam.step;
	  step_back => <-.<-.animParam.step_back;
	  cycle => <-.<-.animParam.run_options;
	  incr => (1. / <-.<-.animParam.frames_per_second);
	};
	AnimControl {
	  frameDelta => <-.<-.animParam.frame_delta;
	  currentTime => <-.<-.animParam.current_time;
	  newTime => <-.<-.animParam.new_time;
	  framesPerSec => <-.<-.animParam.frames_per_second;
	  addFrameBtn => <-.<-.animParam.add_frame;
	  clearFramesBtn => <-.<-.animParam.clear_frames;
	  deleteFrameBtn => <-.<-.animParam.delete_frame;
	  moveFrameBtn => <-.<-.animParam.move_frame;
	  completeFramesBtn => <-.<-.animParam.complete_frames;
	  reInitBtn => <-.<-.animParam.re_init;
	  reScanBtn => <-.<-.animParam.re_scan;
	  interpEditorOkBtn => <-.<-.animParam.interp_editor_ok;
	  interpEditorApplyBtn => <-.<-.animParam.interp_editor_apply;
	  interpolatorList => <-.<-.animParam.interpolator_list;
	  interpolatorListIndex => <-.<-.animParam.interpolator_selected;
	  timeListIndex => <-.<-.animParam.time_selected;
	  quatOrEuler => <-.<-.animParam.quat_or_euler;
	  interpType => <-.<-.animParam.interp_type;
	  fScalar => <-.<-.animParam.fscalar;
	  iScalar => <-.<-.animParam.iscalar;
	  fValue1 => <-.<-.animParam.fvalue1;
	  fValue2 => <-.<-.animParam.fvalue2;
	  fValue3 => <-.<-.animParam.fvalue3;
	  iValue1 => <-.<-.animParam.ivalue1;
	  iValue2 => <-.<-.animParam.ivalue2;
	  iValue3 => <-.<-.animParam.ivalue3;
	  matValue1 => <-.<-.animParam.mat_value1;
	  matValue2 => <-.<-.animParam.mat_value2;
	  matValue3 => <-.<-.animParam.mat_value3;
	  matValue4 => <-.<-.animParam.mat_value4;
	  matValue5 => <-.<-.animParam.mat_value5;
	  matValue6 => <-.<-.animParam.mat_value6;
	  fFrame1Visible => <-.<-.animParam.fFrame1_visible;
	  fFrame2Visible => <-.<-.animParam.fFrame2_visible;
	  fFrame3Visible => <-.<-.animParam.fFrame3_visible;
	  iFrame1Visible => <-.<-.animParam.iFrame1_visible;
	  iFrame2Visible => <-.<-.animParam.iFrame2_visible;
	  interpEditorVisible => <-.<-.animParam.interp_editor_visible;
	};
      };
      CCP3.Core_Macros.UI.UIobjs.DLVshell CCP3shell<NEx=539.,NEy=99.> {
	color => <-.prefs.colour;
	fontAttributes => <-.prefs.fontAttributes;
	title = "Keyframe Animation";
	UIshell {
	  width = 270;
	  height = 645;
	};
      };
    };
  };
  macro Save_Movie<NEx=363.,NEy=66.> {
    CCP3.Core_Macros.Core.preferences &prefs;
    GMOD.instancer instancer {
      Group => <-.macro;
      active = 2;
    };
    macro macro<instanced=0> {
      CCP3.Core_Macros.Core.preferences &prefs => <-.prefs;
      CCP3.Core_Macros.UI.UIobjs.DLVshell CCP3shell<NEx=484.,NEy=55.> {
	color => <-.prefs.colour;
	fontAttributes => <-.prefs.fontAttributes;
	title = "Save Movie";
	UIshell {
	  width = 260;
	  height = 750;
	};
      };
      macro image_capture<NEx=297.,NEy=242.,
	need_objs="ANIM.ArrayInterpolator ANIM.ScalarInterpolator ANIM.StringInterpolator ANIM.TransformMatrixInterpolator"> {
	link prefs => <-.prefs;
	ANIM_MACROS.imcapParam imcapParam<export_all=2,NEx=288.,NEy=88.>;
	ANIM_MACROS.imcapUI imcapUI<NEx=396.,NEy=187.> {
	  CCP3.Core_Macros.Core.preferences &prefs => <-.prefs;
	  UIparent<NEportLevels={3,1}> => <-.<-.CCP3shell.UIpanel;
	  UItext UItext {
	    parent => <-.UIparent;
	    y = 0;
	    width => parent.clientWidth;
	    height = 48;
	    text = "Recommended viewer size for\nMPEG-1 is < 352x288";
	    rows = 2;
	    outputOnly = 1;
	    &color => <-.prefs.colour;
	    &fontAttributes => <-.prefs.fontAttributes;
	  };
	  macro ImageCapControls<NEx=143.,NEy=99.> {
	    CCP3.Core_Macros.Core.preferences &prefs => <-.prefs;
	    UIframe ImageCapFrame<NEx=200.,NEy=88.> {
	      x = 4;
	      //y = 135;
	      y => ((<-.<-.UItext.height + <-.<-.UItext.y) + 5);
	      width = 250;
	      height = 160;
	      parent<NEportLevels={3,0}> => <-.<-.UIparent;
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	    UIlabel UIlabel<NEx=210.,NEy=154.> {
	      y = 0;
	      width => parent.width;
	      parent => <-.ImageCapFrame;
	      label => "Capture Controls";
	      alignment = "center";
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	    UIoption Inactive<NEx=385.,NEy=10.> {
	      label => "Inactive";
	      set = 1;
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	    //UIoption CaptureFB<NEx=385.,NEy=100.> {
	    //  label => "Capture";
	    //};
	    //UIoption PlaybackFB<NEx=385.,NEy=190.> {
	    //  label => "Playback";
	    //};
	    UIoption CaptureView<NEx=385.,NEy=100.> {
	      label => "Capture";
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	    UIoption PlaybackView<NEx=385.,NEy=190.> {
	      label => "Playback";
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	    UIoptionMenu Mode<NEx=210.,NEy=231.> {
	      x = 4;
	      y = 24;
	      width = 175;
	      parent => <-.ImageCapFrame;
	      //cmdList => {<-.Inactive,
	      //		  <-.CaptureFB,<-.PlaybackFB};
	      cmdList => {<-.Inactive,
			  <-.CaptureView,<-.PlaybackView};
	      selectedItem => <-.<-.<-.imcapParam.mode;
	      alignment = "left";
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	    UIoption Memory<NEx=385.,NEy=286.> {
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	    UIoption Disk<NEx=385.,NEy=242.> {
	      set = 1;
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	    UIoptionMenu CaptureMode<NEx=209.,NEy=330.> {
	      x = 4;
	      y = 61;
	      width = 175;
	      parent => <-.ImageCapFrame;
	      label => "Capture Mode";
	      cmdList => {<-.Disk,<-.Memory};
	      selectedItem => <-.<-.<-.imcapParam.capture_mode;
	      alignment = "left";
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	    UIbutton Clear<NEx=210.,NEy=286.> {
	      x = 4;
	      y = 100;
	      width = 75;
	      parent => <-.ImageCapFrame;
	      label => "Clear";
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	    GMOD.parse_v ParseClear<NEx=341.,NEy=231.> {
	      v_commands = "imcapParam.clear = 1; imcapParam.reset = 1;";
	      trigger => <-.Clear.do;
	      on_inst = 0;
	      relative => <-.<-;
	    };
	    UIbutton Record<NEx=374.,NEy=385.> {
	      x => ((<-.Clear.x + <-.Clear.width) + 4);
	      y = 100;
	      width = 75;
	      parent => <-.ImageCapFrame;
	      do => <-.<-.<-.imcapParam.record;
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	    UIbutton Delete<NEx=539.,NEy=385.> {
	      x => ((<-.Record.x + <-.Record.width) + 4);
	      y = 100;
	      width = 75;
	      parent => <-.ImageCapFrame;
	      do => <-.<-.<-.imcapParam.delete;
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	    UIlabel NumberOfFrames<NEx=210.,NEy=385.> {
	      x = 4;
	      y = 130;
	      width => parent.width;
	      parent => <-.ImageCapFrame;
	      label => ("Total Frames: " + <-.<-.<-.imcapParam.total_images);
	      alignment = "left";
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	  };
	  PlaybackControls {
	    CCP3.Core_Macros.Core.preferences &prefs => <-.prefs;
	    PlaybackFrame {
	      y = 300;
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	    RunBack {
	      do => <-.<-.<-.imcapParam.run_back;
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	      toolTip {
		enable = "never";
	      };
#endif // DLV_TOOLTIP_OK
	    };
	    StepBack {
	      do => <-.<-.<-.imcapParam.step_back;
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	      toolTip {
		enable = "never";
	      };
#endif // DLV_TOOLTIP_OK
	    };
	    Stop {
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	      toolTip {
		enable = "never";
	      };
#endif // DLV_TOOLTIP_OK
	    };
	    StepForward {
	      do => <-.<-.<-.imcapParam.step;
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	      toolTip {
		enable = "never";
	      };
#endif // DLV_TOOLTIP_OK
	    };
	    RunForward {
	      do => <-.<-.<-.imcapParam.run;
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	      toolTip {
		enable = "never";
	      };
#endif // DLV_TOOLTIP_OK
	    };
	    RunOptions {
	      selectedItem => <-.<-.<-.imcapParam.run_options;
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	    Current {
	      label =>
		str_format("%2d",<-.<-.<-.imcapCompute.ImageCap.LCurrentImage);
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	    ParseStop {
	      v_commands = "imcapParam.run = 0;";
	    };
	    GoToStart {
	      do => <-.<-.<-.imcapParam.reset;
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	      toolTip {
		enable = "never";
	      };
#endif // DLV_TOOLTIP_OK
	    };
	    GoToEnd {
	      do => <-.<-.<-.imcapParam.reset_back;
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
#ifndef DLV_TOOLTIP_OK
	      toolTip {
		enable = "never";
	      };
#endif // DLV_TOOLTIP_OK
	    };
	    PlaybackDelay {
	      Delay {
		value => <-.<-.<-.<-.imcapParam.delay;
		&color => <-.<-.prefs.colour;
		&fontAttributes => <-.<-.prefs.fontAttributes;
	      };
	    };
	    UIlabel {
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	    Once {
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	    Cycle {
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	    Bounce {
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	  };
	  macro MovieControls<NEx=143.,NEy=231.> {
	    CCP3.Core_Macros.Core.preferences &prefs => <-.prefs;
	    UIframe MovieFrame<NEx=200.,NEy=88.> {
	      x = 4;
	      y = 516;
	      width = 250;
	      height => ((<-.Generate.y + <-.Generate.height) + 8);
	      parent<NEportLevels={3,0}> => <-.<-.UIparent;
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	    UIlabel UIlabel<NEx=210.,NEy=154.> {
	      y = 0;
	      width => parent.width;
	      parent => <-.MovieFrame;
	      label => "Movie Controls";
	      alignment = "center";
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	    VUIFileSelector FileSelector<NEx=210.,NEy=286.> {
	      parent => <-.MovieFrame;
	      x = 4;
	      y = 36;
	      width => (parent.clientWidth - 8);
	      title = "Movie Name";
	      file => <-.<-.<-.imcapParam.filename;
	      fileFrame {
		&color => <-.color;
		&fontAttributes => <-.fontAttributes;
	      };
	      fileText {
		width => (parent.clientWidth - 8);
		updateMode = 2;
		&color => <-.color;
		&fontAttributes => <-.fontAttributes;
	      };
	      fileBrowse {
		width => (parent.clientWidth - 8);
		&color => <-.color;
		&fontAttributes => <-.fontAttributes;
	      };
	      fileTitle {
		&color => <-.color;
		&fontAttributes => <-.fontAttributes;
	      };
	      fileFB {
		&color => <-.color;
		&fontAttributes => <-.fontAttributes;
	      };
	      link color => <-.prefs.colour;
	      link fontAttributes => <-.prefs.fontAttributes;
	    };
	    UIbutton Generate<NEx=210.,NEy=341.> {
	      x = 4;
	      y => ((<-.FileSelector.y + <-.FileSelector.height) + 4);
	      width => (parent.clientWidth - 8);
	      parent => <-.MovieFrame;
	      label => "Generate Movie";
	      do => <-.<-.<-.imcapParam.gen_movie;
	      &color => <-.prefs.colour;
	      &fontAttributes => <-.prefs.fontAttributes;
	    };
	  };
	};
	ANIM_MACROS.imcapCompute imcapCompute<NEx=176.,NEy=187.> {
	  loop {
	    reset => <-.<-.imcapParam.reset;
	    reset_back => <-.<-.imcapParam.reset_back;
	    run => <-.<-.imcapParam.run;
	    run_back => <-.<-.imcapParam.run_back;
	    step => <-.<-.imcapParam.step;
	    step_back => <-.<-.imcapParam.step_back;
	    cycle => <-.<-.imcapParam.run_options;
	  };
	  timer {
	    TimeStep => <-.<-.imcapParam.timer_step;
	    Active => <-.<-.imcapParam.timer_active;
	  };
	  ImageCap {
	    //LMode => (2 * <-.<-.imcapParam.mode);
	    LMode => switch ((<-.<-.imcapParam.mode+1),0,1,3);
	    LCaptureMode => <-.<-.imcapParam.capture_mode;
	    LClear => <-.<-.imcapParam.clear;
	    LRecord => <-.<-.imcapParam.record;
	    LDelete => <-.<-.imcapParam.delete;
	    LMovieMode = 1;
	    LGenMovie => <-.<-.imcapParam.gen_movie;
	    LTotalImages => <-.<-.imcapParam.total_images;
	    LAnimOutFile => <-.<-.imcapParam.filename;
	    LGDView<NEportLevels={3,0}>;
	    //LInputFrameBuffer<NEportLevels={4,0}> => <-.<-.<-.OutputField.output_field.Output.output;
	    LPBDelay => <-.<-.imcapParam.delay;
	    LRunForward => <-.<-.imcapParam.run;
	    LRunBack => <-.<-.imcapParam.run_back;
	    LLoopCount => <-.loop.count;
	    LTimerStep => <-.<-.imcapParam.timer_step;
	    LTimerActive => <-.<-.imcapParam.timer_active;
	    LTimerFrameStep => <-.<-.imcapParam.timer_frame_step;
	    LTimerFrameVal => <-.<-.imcapParam.timer_frame_value;
	  };
	};
      };
    };
  };
  macro Output_Model<NEx=275.,NEy=209.> {
    link prefs;
    link view<NEportLevels={2,1},NEx=176.,NEy=66.>;
    UIcmdList Save_As<NEx=77.,NEy=132.> {
      cmdList => {
	OutputImage.Image,
	Save_Movie.Movie,
	OutputVRML.VRML
      };
    };
    OutputView.OutputVRML OutputVRML<NEx=77.,NEy=209.> {
      prefs => <-.prefs;
      UIoption VRML<NEx=407.,NEy=22.> {
	label = "VRML";
	&color => <-.prefs.colour;
	&fontAttributes => <-.prefs.fontAttributes;
      };
      instancer {
	Value => <-.VRML.set;
      };
      OutputVRML<instanced=0> {
	view_in => <-.<-.view;
	vrmlUI {
	  CCP3shell {
	    UIshell {
	      visible => <-.<-.<-.<-.VRML.set;
	    };
	  };
	};
      };
    };
    OutputView.OutputImage OutputImage<NEx=297.,NEy=209.> {
      prefs => <-.prefs;
      UIoption Image<NEx=264.,NEy=33.> {
	label = "Image";
	&color => <-.prefs.colour;
	&fontAttributes => <-.prefs.fontAttributes;
      };
      instancer {
	Value => <-.Image.set;
      };
      macro<instanced=0> {
	OutputField {
	  view_in => <-.<-.<-.view;
	};
	CCP3shell {
	  UIshell {
	    visible => <-.<-.<-.Image.set;
	  };
	};
      };
      CCP3.Core_Modules.Utils.toggle_renderer toggle<NEx=423.,NEy=279.> {
	trigger => <-.Image.set;
      };
      GMOD.parse_v parse_v<NEx=423.,NEy=342.> {
	trigger => <-.toggle.run;
	on_inst = 0;
      };
    };
    OutputView.Animator Animator<NEx=473.,NEy=77.> {
      prefs => <-.prefs;
      UIoption Keyframes<NEx=484.,NEy=44.> {
	active => <-.<-.active_menus;
	label = "Keyframes";
	&color => <-.prefs.colour;
	&fontAttributes => <-.prefs.fontAttributes;
      };
      instancer {
	Value => <-.Keyframes.set;
      };
      Anim {
	CCP3shell {
	  UIshell {
	    visible => <-.<-.<-.Keyframes.set;
	  };
	};
      };
    };
    OutputView.Save_Movie Save_Movie<NEx=517.,NEy=209.> {
      prefs => <-.prefs;
      UIoption Movie<NEx=308.,NEy=33.> {
	active => <-.<-.active_menus;
	label = "Movie";
	&color => <-.prefs.colour;
	&fontAttributes => <-.prefs.fontAttributes;
      };
      instancer {
	Value => <-.Movie.set;
      };
      macro<instanced=0> {
	//OutputField {
	//  view_in => <-.<-.<-.view;
	//};
	image_capture {
	  imcapCompute {
	    ImageCap {
	      LGDView => <-.<-.<-.<-.<-.view;
	    };
	  };
	};
	CCP3shell {
	  UIshell {
	    visible => <-.<-.<-.Movie.set;
	  };
	};
      };
    };
    int active_menus<NEportLevels={2,1},NEx=473.,NEy=22.>;
  };
};
