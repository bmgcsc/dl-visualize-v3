
flibrary Base<NEeditable=1,
	      export_cxx=1,
	      build_dir="avs/src/express",
	      cxx_hdr_files="fld/Xfld.h avs/src/express/misc.hxx avs/src/core/render/cell_gen.hxx avs/src/express/display.hxx avs/src/express/data.hxx",
	      out_hdr_file="rbase.hxx",
	      out_src_file="rbase.cxx"> {
  macro props {
    GroupObject GroupObject<NEx=308.,NEy=550.> {
      Top {
	xform_mode = "Parent";
	name => name_of(<-.<-.<-);
      };
      obj<NEportLevels={1,3}>;
    };
  };
  macro space {
    macro Edits<NEx=825.,NEy=66.>;
    mlink obj<NEportLevels={1,2},NEx=363.,NEy=616.>;
  };
  group data_list {
    string label;
    string data_list[];
    int selection;
    int type_data;
    boolean vector[];
  };
  group display_list {
    string label;
    int display_type;
    int index;
  };
  group background {
    float red<NEportLevels=1> = 0.;
    float green<NEportLevels=1> = 0.;
    float blue<NEportLevels=1> = 0.;
  };
  macro parent {
    string model_name<NEportLevels=1,NEx=759.,NEy=55.>;
    DefaultXform Xform<NEx=363.,NEy=121.>;
    DefaultProps Props<NEx=561.,NEy=121.>;
    background background<NEx=363.,NEy=44.>;
    float back_col<NEportLevels=1,NEx=561.,NEy=44.>[3] => {
      background.red,
      background.green,
      background.blue
    };
    data_list data_objs<NEportLevels=1,NEx=132.,NEy=176.>[];
    int data_selection<NEportLevels=1,NEx=319.,NEy=176.>;
    data_list editable_objs<NEx=506.,NEy=176.>[];
    int editable_selection<NEportLevels=1,NEx=693.,NEy=176.>;
    display_list display_objs<NEportLevels=1,NEx=132.,NEy=242.>[];
    int display_selection<NEportLevels=1,NEx=319.,NEy=242.>;
    int display_size<NEportLevels=1,NEx=506.,NEy=242.> = 0;
    display_list edits_objs<NEportLevels=1,NEx=132.,NEy=282.>[];
    int edits_selection<NEportLevels=1,NEx=319.,NEy=282.>;
    int edits_size<NEportLevels=1,NEx=506.,NEy=282.> = 0;
    string atom_groups<NEportLevels=1,NEx=135.,NEy=342.>[];
    link r_obj<NEportLevels={1,2},NEx=242.,NEy=605.>;
    link r_edit_obj<NEportLevels=1,NEx=675.,NEy=99.>;
  };
  parent atoms {
    CCP3.Core_Modules.Data.CRYSTAL_Wavefn_info CRYSTAL_Wvfn<NEx=132.,NEy=110.,
      NEportLevels={0,1}> {
      wavefn_set = 0;
      scf_done = 0;
      tddft_done = 0;
    };
    CCP3.Core_Modules.Data.ONETEP_scf_info ONETEP_Wvfn<NEportLevels=1,
						       NEx=135.,NEy=54.> {
      scf_done = 0;
    };
    CCP3.Renderers.Base.space r_space<NEx=242.,NEy=506.> {
      CCP3.Core_Modules.Render.common_options common_data<NEx=297.,NEy=319.> {
	na = 1;
	nb = 1;
	nc = 1;
	centre_cell = 0;
	conventional_cell = 0;
	tolerance = 0.000001;
      };
      int nselected_atoms<NEportLevels=1,NEx=363.,NEy=220.> = 0;
      CCP3.Core_Modules.Render.props props<NEx=55.,NEy=319.> {
	spheres {
	  subdiv = 8;
	  opacity = 1.0;
	};
	lines {
	  width = 0;
	  smooth = 0;
	};
      };
      Xform transforms<NEx=572.,NEy=319.>[];
      int ntransforms<NEx=572.,NEy=374.> => array_size(transforms);
      int event<NEportLevels=1,NEx=803.,NEy=319.> = 0;
      int nframes<NEportLevels=1,NEx=143.,NEy=110.> = 1;
      int end_frame<NEportLevels=1,NEx=319.,NEy=110.> = 0;
      int cur_frame<NEportLevels=1,NEx=506.,NEy=110.> = 0;
    };
    CCP3.Renderers.Base.space k_space<NEx=517.,NEy=517.> {
      CCP3.Core_Modules.Render.line_props &props<NEx=77.,NEy=88.> =>
	<-.r_space.props.lines;
    };
    r_obj => r_space.obj;
    link k_obj<NEportLevels={1,2},NEx=484.,NEy=605.> => k_space.obj;
    link k_edit_obj<NEportLevels=1,NEx=783.,NEy=639.>;
  };
  parent outline {
    CCP3.Renderers.Base.space r_space<NEx=242.,NEy=506.> {
   CCP3.Core_Modules.Render.common_options common_data<NEx=297.,NEy=319.> {
	na = 1;
	nb = 1;
	nc = 1;
	centre_cell = 0;
	conventional_cell = 0;
	tolerance = 0.000001;
      };
      Xform transforms<NEx=572.,NEy=319.>[];
      int ntransforms<NEx=572.,NEy=374.> => array_size(transforms);
   };
    r_obj => r_space.obj;
  };
  parent shells {
    CCP3.Renderers.Base.space r_space<NEx=242.,NEy=506.>;
    r_obj => r_space.obj;
  };
};
