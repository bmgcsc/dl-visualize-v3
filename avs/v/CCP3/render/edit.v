
flibrary Edits<NEeditable=1,
	       export_cxx=1,
	       build_dir="avs/src/express",
	       cxx_hdr_files="fld/Xfld.h avs/src/express/planes.hxx avs/src/express/volumes.hxx avs/src/core/edits/spec_gen.hxx avs/src/core/edits/phon_gen.hxx",
	       out_hdr_file="edits.hxx",
	       out_src_file="edits.cxx"> {
  macro orthoslice {
    ilink in_field<export_all=1>;
    DV_Param_ortho_slice OrthoSliceParam<export_all=2> {
      axis = 0;
      plane+nres => (in_field.dims[axis] / 2);
    };
    DVorthoslice DVorthoslice {
      in => in_field;
      &axis => param.axis;
      &plane => param.plane;
      DV_Param_ortho_slice &param<NEportLevels={2,0}> => OrthoSliceParam;
    };
    olink out_fld<export_all=2,NEx=374.,NEy=407.> => DVorthoslice.out;
  };
  group ExtendParams {
    int max_a;
    int max_b;
    int max_c;
    int min_a;
    int min_b;
    int min_c;
    int a;
    int b;
    int c;
    int dim;
  };
  macro extend_obj {
    ilink in_field<export_all=1>;
    ExtendParams params {
      max_a+nres => (<-.in_field.dims[0] / 2);
      max_b+nres => (<-.in_field.dims[1] / 2);
      max_c+nres => switch ((<-.in_field.ndim > 2) + 1, 1,
			    (<-.in_field.dims[2] / 2));
      min_a+nres => - (<-.in_field.dims[0] / 2) + 1;
      min_b+nres => - (<-.in_field.dims[1] / 2) + 1;
      min_c+nres => switch ((<-.in_field.ndim > 2) + 1, 0,
			    - (<-.in_field.dims[2] / 2) + 1);
      a = 0;
      b = 0;
      c = 0;
      dim+nres => <-.in_field.ndim;
    };
    olink out_fld<export_all=2,NEx=225.,NEy=414.,NEportLevels={1,2}>;
  };
  DV_Param_slice SliceParam {
    int plane;
  };
  macro slice {
    ilink in_field<export_all=1>;
    CCP3.Renderers.Planes.plane_data &in_plane;
    CCP3.Renderers.Planes.slice_plane myplane {
      plane_data => <-.in_plane;
    };
    SliceParam SliceParam<NEportLevels=1,export_all=2> {
      dist = 0.;
      component =>
	init_array(<-.in_field.nnode_data, 0, (<-.in_field.nnode_data - 1));
    };
    int has_cell_data => (DVcell_data_labels.ncomp > 0);
    DVslice DVslice {
      in => in_field;
      plane => <-.myplane.obj;
      dist => param.dist;
      map_comp => param.component;
      cell_data => switch(<-.has_cell_data,param.cell_data);
      DV_Param_slice &param<NEportLevels={2,0}> => SliceParam;
    };
    DVcell_data_labels DVcell_data_labels {
      in => in_field;
    };
    olink out_fld<export_all=2> => DVslice.out;
  };
  DV_Param_cut CutParam {
    int plane;
  };
  macro cut<NEx=462.,NEy=165.> {
    ilink in_field<export_all=1>;
    //ilink in_plane<export_all=1>;
    CutParam CutParam<NEportLevels=1,export_all=2> {
      component =>
	init_array(<-.in_field.nnode_data, 0, (<-.in_field.nnode_data - 1));
      dist = 0.;
      above = 1;
    };
    CCP3.Renderers.Planes.plane_data &in_plane;
    CCP3.Renderers.Planes.slice_plane myplane {
      plane_data => <-.in_plane;
    };
    int has_cell_data => (DVcell_data_labels.ncomp > 0);
    Cut Cut {
      in_fld => in_field;
      in_pln => myplane.obj;
      param => CutParam;
      DVcut {
	cell_data => switch(has_cell_data,param.cell_data);
      };
    };
    DVcell_data_labels DVcell_data_labels {
      in => in_field;
    };
    olink out_fld<export_all=2> => Cut.out_fld;
  };
  macro clamp {
    ilink in_field<export_all=1>;
    DV_Param_clamp ClampParam<NEportLevels=1,export_all=2> {
      vector = 0;
      component = 0;
      below = 1;
      above = 1;
      reset_minmax = 1;
      min_value+nres => cache(in_field.node_data[vector].min_vec[component]);
      max_value+nres => cache(in_field.node_data[vector].max_vec[component]);
    };
    Clamp Clamp<NEy=165,NEx=264> {
      in_fld => in_field;
      param => ClampParam;
    };
    olink out_fld<export_all=2,NEx=253.,NEy=341.> => Clamp.out_fld;
  };
  macro downsize<NEx=143.,NEy=110.> {
    ilink in_field<export_all=1>;
    DV_Param_downsize DownsizeParam<export_all=2> {
      factor0 = 8.;
      factor1 = 8.;
      factor2 = 8.;
    };
    DVdownsize DVdownsize<NEy=154,NEx=286> {
      in => in_field;
      factor => param.factor[0:(in.ndim - 1)];
      DV_Param_downsize &param<NEportLevels={2,0}> => DownsizeParam;
    };
    olink out_fld<export_all=2> => DVdownsize.out;
  };
  macro crop {
    ilink in_field<export_all=1>;
    DV_Param_crop CropParam<export_all=2> {
      min+nres => init_array(in_field.ndim,0,0);
      max+nres => (in_field.dims - 1);
    };
    DVcrop DVcrop<NEy=165,NEx=308> {
      in => in_field;
      min => param.min;
      max => param.max;
      DV_Param_crop &param<NEportLevels={2,0}> => CropParam;
    };
    olink out_fld<export_all=2> => DVcrop.out;
  };
  group data_math_params {
    string operation;
    int selection2;
    int selection3;
    int selection4;
    int cmp1;
    int cmp2;
    int cmp3;
    int cmp4;
    boolean trust_user;
    string list[];
  };
  macro data_math {
    ilink in_field1<export_all=1,NEportLevels={2,1}>;
    ilink in_field2<export_all=1,NEportLevels={2,1}>;
    ilink in_field3<export_all=1,NEportLevels={2,1}>;
    ilink in_field4<export_all=1,NEportLevels={2,1}>;
    data_math_params params {
      operation = "#1";
      cmp1 = 0;
      cmp2 = 0;
      cmp3 = 0;
      cmp4 = 0;
      trust_user = 0;
    };
    //string expres<NEportLevels=1,export=2> = "#1";
    int value_type<NEportLevels=1,export=2,NEx=682.,NEy=110.> = 4;
    DVdata_math DVdata_math<NEx=231.,NEy=275.> {
      &operation => <-.params.operation;
      in1 => <-.DVextract_comp1.out;
      in2 => <-.DVextract_comp2.out;
      in3 => <-.DVextract_comp3.out;
      in4 => <-.DVextract_comp4.out;
      &data_type => <-.value_type;
    };
    olink out_fld<export_all=2,NEportLevels={1,2}> => .DVdata_math.out;
    DVextract_comp DVextract_comp1<NEx=55.,NEy=187.> {
      component => <-.params.cmp1;
      in => <-.in_field1;
    };
    DVextract_comp DVextract_comp2<NEx=231.,NEy=187.> {
      component => <-.params.cmp2;
      in => <-.in_field2;
    };
    DVextract_comp DVextract_comp3<NEx=418.,NEy=187.> {
      component => <-.params.cmp3;
      in => <-.in_field3;
    };
    DVextract_comp DVextract_comp4<NEx=616.,NEy=187.> {
      component => <-.params.cmp4;
      in => <-.in_field4;
    };
    //int cmp1<NEportLevels=1,NEx=132.,NEy=11.> = 0;
    //int cmp2<NEportLevels=1,NEx=297.,NEy=11.> = 0;
    //int cmp3<NEportLevels=1,NEx=462.,NEy=11.> = 0;
    //int cmp4<NEportLevels=1,NEx=638.,NEy=11.> = 0;
  };
  macro volr_obj {
    Field_Unif field<NEx=132.,NEy=176.,NEportLevels={0,1}> {
      ndim = 3;
      nspace = 3;
    };
    olink out_fld<export_all=2,NEx=220.,NEy=330.,NEportLevels={1,2}> => .field;
  };
  macro spectrum_data {
    CCP3.Core_Modules.EditData.spectral_data SpectrumData {
      index = 0;
      ir_or_raman = 0;
      emin = 0.0;
      emax = 4000.0;
      npoints = 200;
      broaden = 1.0;
      harmonic_scale = 0.0;
    };
    CCP3.Renderers.Plots.dos_data field;
  };
  macro phonon_trajectory {
    CCP3.Core_Modules.EditData.phonon_data PhononData {
      nframes = 20;
      magnitude = 0.2;
      use_temp = 0;
      temperature = 300.0;
    };
  };
  group plot_params {
    float shift;
    boolean use_property;
    int object;
  };
  macro edit_dos {
    CCP3.Renderers.Edits.plot_params params<NEx=585.,NEy=117.> {
      shift = 0.0;
      use_property = 0;
      object = 0;
    };
    CCP3.Renderers.Plots.dos_data &in_data;
    CCP3.Renderers.Plots.dos_data dos_data<NEx=333.,NEy=117.,
      NEportLevels={0,1}>;
    link out_dos<NEportLevels=1,NEx=369.,NEy=261.> => .dos_data;
  };
  macro edit_bands {
    CCP3.Renderers.Edits.plot_params params<NEx=585.,NEy=117.> {
      shift = 0.0;
      use_property = 0;
      object = 0;
    };
    CCP3.Renderers.Plots.band_data &in_data;
    CCP3.Renderers.Plots.band_data band_data<NEx=333.,NEy=117.,
      NEportLevels={0,1}>;
    link out_data<NEportLevels=1,NEx=369.,NEy=261.> => .band_data;
  };
};
