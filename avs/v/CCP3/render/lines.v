
flibrary Lines<NEeditable=1,
	       export_cxx=1,
	       build_dir="avs/src/express",
	       cxx_hdr_files="avs/src/express/points.hxx",
	       out_hdr_file="lines.hxx",
	       out_src_file="lines.cxx"> {
  group line_data {
    float ax;
    float ay;
    float az;
    float bx;
    float by;
    float bz;
  };
  group line_params {
    float red;
    float green;
    float blue;
  };
  macro line {
    int visible<NEportLevels=1,NEx=528.,NEy=88.> = 1;
    line_params params<NEx=385.,NEy=132.> {
      red = 0.0;
      green = 0.0;
      blue = 0.0;
    };
    string name;
    link data<NEportLevels=1,NEx=143.,NEy=132.>;
    float+nres start_points<NEportLevels=1,NEx=253.,NEy=209.>[1][3] => {
      data.ax, data.ay, data.az
    };
    float+nres end_points<NEportLevels=1,NEx=506.,NEy=209.>[1][3] => {
      data.bx, data.by, data.bz
    };
    FLD_MAP.line_disjoint_mesh line_disjoint_mesh<NEx=308.,NEy=242.> {
      coord1 => <-.start_points;
      coord2 => <-.end_points;
      DataObject {
	Props {
	  col => {
	    <-.<-.<-.params.red, <-.<-.<-.params.green, <-.<-.<-.params.blue
	  };
	  inherit = 0;
	};
	Obj {
	  xform_mode = "Parent";
	  visible => <-.<-.<-.visible;
	  name => <-.<-.<-.name;
	};
      };
    };
    olink obj<NEportLevels={1,2},NEx=286.,NEy=473.> => line_disjoint_mesh.obj;
  };
  /*macro vector {
    int visible<NEportLevels=1,NEx=528.,NEy=88.> = 1;
    float coord1<NEportLevels=1,NEx=198.,NEy=154.>[3][3];
    float coord2<NEportLevels=1,NEx=374.,NEy=154.>[3][3];
    // Todo - who uses these?
    float scale<NEportLevels=1,NEx=341.,NEy=88.>;
    float xlate<NEportLevels=1,NEx=99.,NEy=88.>[3];
    FLD_MAP.line_disjoint_mesh line_disjoint_mesh<NEx=308.,NEy=242.> {
      coord1 => <-.coord1;
      coord2 => <-.coord2;
    };
    DataObjectNoTexture DataObjectNoTexture<NEx=319.,NEy=374.> {
      in => <-.line_disjoint_mesh.out;
      Obj {
	xform_mode = "Parent";
      };
    };
    olink obj<NEportLevels={1,2},NEx=286.,NEy=473.> =>
				   .DataObjectNoTexture.obj;
				   };*/
};
