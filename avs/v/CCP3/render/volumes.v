
flibrary Volumes<NEeditable=1,
		 export_cxx=1,
		 build_dir="avs/src/express",
		 libdeps="GD GMOD",
		 need_objs="GroupObject",
		 cxx_hdr_files="fld/Xfld.h avs/src/express/misc.hxx avs/src/express/display.hxx avs/src/express/planes.hxx avs/src/core/render/krep_gen.hxx",
		 out_hdr_file="volumes.hxx",
		 out_src_file="volumes.cxx"> {
  group box_data {
    float ox;
    float oy;
    float oz;
    float ax;
    float ay;
    float az;
    float bx;
    float by;
    float bz;
    float cx;
    float cy;
    float cz;
  };
  group box_params {
    boolean lines;
    boolean planes;
    float opacity;
    float red;
    float green;
    float blue;
    boolean labels;
    boolean has_labels;
  };
  macro region3D {
    CCP3.Renderers.Volumes.box_params params<NEx=385.,NEy=132.> {
      lines = 1;
      planes = 0;
      opacity = 1.0;
      red = 0.0;
      green = 0.0;
      blue = 0.0;
      labels = 0;
      has_labels = 0;
    };
    string name;
    int visible<NEportLevels=1,NEx=605.,NEy=132.> = 1;
    link data<NEportLevels=1,NEx=143.,NEy=132.>;
    float+nres astep[3] => {
      (data.ax - data.ox), (data.ay - data.oy), (data.az - data.oz)
    };
    float+nres bstep[3] => {
      (data.bx - data.ox), (data.by - data.oy), (data.bz - data.oz)
    };
    float+nres cstep[3] => {
      (data.cx - data.ox), (data.cy - data.oy), (data.cz - data.oz)
    };
    /*float+nres start_points<NEportLevels=1,NEx=253.,NEy=209.>[12][3] => {
      data.ox, data.oy, data.oz,
      data.bx, data.by, data.bz,
      data.cx, data.cy, data.cz,
      (data.bx + cstep[0]), (data.by + cstep[1]), (data.bz + cstep[2]),
      data.ox, data.oy, data.oz,
      data.ax, data.ay, data.az,
      data.cx, data.cy, data.cz,
      (data.ax + cstep[0]), (data.ay + cstep[1]), (data.az + cstep[2]),
      data.ox, data.oy, data.oz,
      data.ax, data.ay, data.az,
      data.bx, data.by, data.bz,
      (data.ax + bstep[0]), (data.ay + bstep[1]), (data.az + bstep[2])
    };
    float+nres end_points<NEportLevels=1,NEx=506.,NEy=209.>[12][3] => {
      data.ax, data.ay, data.az,
      (data.bx + astep[0]), (data.by + astep[1]), (data.bz + astep[2]),
      (data.cx + astep[0]), (data.cy + astep[1]), (data.cz + astep[2]),
      (data.bx + cstep[0] + astep[0]), (data.by + cstep[1] + astep[1]),
      (data.bz + cstep[2] + astep[2]),
      data.bx, data.by, data.bz,
      (data.ax + bstep[0]), (data.ay + bstep[1]), (data.az + bstep[2]),
      (data.cx + bstep[0]), (data.cy + bstep[1]), (data.cz + bstep[2]),
      (data.ax + cstep[0] + bstep[0]), (data.ay + cstep[1] + bstep[1]),
      (data.az + cstep[2] + bstep[2]),
      data.cx, data.cy, data.cz,
      (data.ax + cstep[0]), (data.ay + cstep[1]), (data.az + cstep[2]),
      (data.bx + cstep[0]), (data.by + cstep[1]), (data.bz + cstep[2]),
      (data.ax + bstep[0] + cstep[0]), (data.ay + bstep[1] + cstep[1]),
      (data.az + bstep[2] + cstep[2])
    };
    FLD_MAP.line_disjoint_mesh line_disjoint_mesh<NEx=396.,NEy=308.> {
      coord1 => <-.start_points;
      coord2 => <-.end_points;
      DataObject {
	Obj {
	  xform_mode = "Parent";
	  visible => <-.<-.<-.params.lines;
	  name => "lines";
	};
      };
    };
    DVbounds edges<NEx=748.,NEy=374.> {
      in => <-.line_disjoint_mesh.out;
      faces = 1;
      data = 0;
      };*/
    float+nres points<NEportLevels=1,NEx=253.,NEy=209.>[8][3] => {
      data.ox, data.oy, data.oz,
      data.ax, data.ay, data.az,
      data.bx, data.by, data.bz,
      data.cx, data.cy, data.cz,
      (data.ax + bstep[0]), (data.ay + bstep[1]), (data.az + bstep[2]),
      (data.ax + cstep[0]), (data.ay + cstep[1]), (data.az + cstep[2]),
      (data.bx + cstep[0]), (data.by + cstep[1]), (data.bz + cstep[2]),
      (data.ax + bstep[0] + cstep[0]), (data.ay + bstep[1] + cstep[1]),
      (data.az + bstep[2] + cstep[2])
    };
    long+nres connections<NEportLevels=1,NEx=506.,NEy=209.>[24] => {
      0, 1, 5, 3,
      0, 2, 4, 1,
      0, 2, 6, 3,
      2, 4, 7, 6,
      3, 6, 7, 5,
      1, 5, 7, 4
    };
    FLD_MAP.quad_mesh quad_mesh<NEx=396.,NEy=308.> {
      coord => <-.points;
      connect => <-.connections;
      DataObject {
	Props {
	  col => {
	    <-.<-.<-.params.red, <-.<-.<-.params.green, <-.<-.<-.params.blue
	  };
	  trans => <-.<-.<-.params.opacity;
	};
	Obj {
	  xform_mode = "Parent";
	  visible => <-.<-.<-.params.planes;
	  name => "lines";
	};
      };
    };
    DVedges edges<NEx=748.,NEy=374.> {
      in => <-.quad_mesh.out;
    };
    DataObjectNoTexture edge_obj<NEx=759.,NEy=440.> {
      in => <-.edges.out;
      Props {
	col => {
	  <-.<-.params.red, <-.<-.params.green, <-.<-.params.blue
	};
      };
      Obj {
	xform_mode = "Parent";
	visible => <-.<-.params.lines;
	name => "surfaces";
      };
    };
    GroupObject region3D<NEx=660.,NEy=517.> {
      child_objs => {
	<-.quad_mesh.obj,<-.edge_obj.obj
      };
      Props {
	col => {
	  <-.<-.params.red, <-.<-.params.green, <-.<-.params.blue
	};
	trans => <-.<-.params.opacity;
      };
      Top {
	visible => <-.<-.visible;
	xform_mode = "Parent";
	name => <-.<-.name;
      };
    };
    link obj<NEportLevels={1,2},NEx=396.,NEy=550.> => .region3D.obj;
  };
  group sphere_data {
    float ox;
    float oy;
    float oz;
    float radius;
  };
  group sphere_params {
    //boolean lines;
    //boolean planes;
    float opacity;
    float red;
    float green;
    float blue;
    int subdivisions;
    //boolean labels;
    //boolean has_labels;
  };
  macro sphere {
    CCP3.Renderers.Volumes.sphere_params params<NEx=385.,NEy=132.> {
      //lines = 1;
      //planes = 0;
      opacity = 1.0;
      red = 0.0;
      green = 0.0;
      blue = 0.0;
      subdivisions = 16;
      //labels = 0;
      //has_labels = 0;
    };
    string name;
    int visible<NEportLevels=1,NEx=605.,NEy=132.> = 1;
    link data<NEportLevels=1,NEx=143.,NEy=132.>;
    float+nres coords<NEportLevels=1,NEx=297.,NEy=198.>[1][3] => {
      {data.ox,data.oy,
       data.oz}};
    float colours<NEportLevels=1,NEx=495.,NEy=198.>[1][3] => {
      {params.red,params.green,params.blue}};
    float+nres radii<NEportLevels=1,NEx=693.,NEy=198.>[1] => {data.radius};
    group point_mesh<NEx=297.,NEy=261.> {
      float &coord<NEportLevels={2,0}>[][] => <-.coords;
      int coord_dims[] => array_dims(coord);
      int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
      GMOD.print_error print_error {
	error => <-.error;
	error_source = "point_mesh";
	error_message = "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	on_inst = 1;
      };
      Mesh out<NEportLevels={0,2}> {
	nnodes => switch((!error),<-.coord_dims[1]);
	nspace => switch((!error),<-.coord_dims[0]);
	coordinates {
	  values => switch((!error),<-.<-.coord);
	};
	ncell_sets = 1;
	Point cell_set {
	  ncells => <-.nnodes;
	  node_connect_list =>
	    switch((!error),init_array(ncells,0,(ncells - 1)));
	};
      };
    };
    FLD_MAP.radius_data radius_data<NEx=738.,NEy=270.> {
      in_data => <-.radii;
    };
    FLD_MAP.node_colors node_colors<NEx=576.,NEy=270.> {
      in_data => <-.colours;
    };
    FLD_MAP.combine_node_datas combine_node_datas<NEx=684.,NEy=324.> {
      in => {<-.node_colors.out,
	     <-.radius_data.out};
    };
    group combine_mesh_data<NEx=432.,NEy=360.> {
      Mesh &in_mesh<NEportLevels={2,0}> => <-.point_mesh.out;
      Node_Data+nres &in_nd<NEportLevels={2,0}> => <-.combine_node_datas.out;
      int error => ((is_valid(in_mesh.nnodes) && is_valid(in_nd.nnodes)) && (in_mesh.nnodes != in_nd.nnodes));
      GMOD.print_error print_error {
	error => <-.error;
	error_source = "combine_mesh_data";
	error_message = "Invalid field: mesh dimensions and size of data do not match";
	on_inst = 1;
      };
      Mesh+Node_Data &out<NEportLevels={0,2}> => switch((!error),merge(in_nd,in_mesh,));
    };
    DataObjectLite draw<NEx=432.,NEy=432.> {
      in => <-.combine_mesh_data.out;
      Obj {
	name => <-.<-.name;
	xform_mode = "Parent";
	visible => <-.<-.visible;
      };
      Props {
	trans => <-.<-.params.opacity;
	subdiv => <-.<-.params.subdivisions;
	inherit = 0;
      };
    };
    link obj<NEportLevels={1,2},NEx=405.,NEy=495.> => .draw.obj;
    /*
    Field_Spher_Unif sphere<NEx=495.,NEy=252.> {
      dims => {2,<-.params.subdivisions,<-.params.subdivisions};
      points+nres => {0.0, 0.0, 0.0,
		      <-.data.radius,3.14159,6.28318};
      nspace = 3;
      ndim = 3;
    };
    DVbounds bounds<NEx=729.,NEy=243.> {
      in => <-.sphere;
      hull = 0;
      edges = 0;
      faces = 1;
      imin = 0;
      imax = 1;
      jmin = 0;
      jmax = 0;
      kmin = 0;
      kmax = 0;
      data = 0;
      component = 0;
      method+notify_val+notify_inst upd_bounds = "DVbounds_update";
    };
    DataObjectLite draw<NEx=621.,NEy=342.> {
      in => <-.bounds.out;
      Obj {
	name => <-.<-.name;
	xform_mode = "Parent";
	visible => <-.<-.visible;
      };
      Props {
	col => {
	  <-.<-.params.red, <-.<-.params.green, <-.<-.params.blue
	};
	trans => <-.<-.params.opacity;
	inherit = 0;
      };
    };
    link obj<NEportLevels={1,2},NEx=405.,NEy=495.> => .draw.obj;
    */
  };
  group wulff_plot_data {
    int nvertices;
    float vertices[nvertices][3];
    float colours[nvertices][3];
    float normals[nvertices][3];
    long connects[];
    int nlabels;
    string labels[];
    float label_pos[nlabels][3];
    int nodes[];
    int nline_verts;
    float line_vertices[nline_verts][3];
    long lines[];
    string info;
  };
  box_params wulff_params {
    boolean vertex_display; 
    //string text_list[];
  };
  macro wulff_plot {
    CCP3.Renderers.Volumes.wulff_params params<NEx=385.,NEy=132.> {
      lines = 1;
      planes = 0;
      opacity = 1.0;
      red = 1.0;
      green = 1.0;
      blue = 1.0;
      labels = 1;
      has_labels = 1;
      vertex_display = 0;
    };
    string name;
    int visible<NEportLevels=1,NEx=605.,NEy=132.> = 1;
    link data<NEportLevels=1,NEx=143.,NEy=132.>;
    // derived from model.brillouin_zone
    FLD_MAP.line_mesh line_mesh<NEx=306.,NEy=234.> {
      coord+nres => <-.data.line_vertices;
      connect+nres => <-.data.lines;
      DataObject {
	Obj {
	  xform_mode = "Parent";
	  visible => <-.<-.<-.params.lines;
	  name = "lines";
	};
      };
    };
    group plane_mesh<NEx=513.,NEy=207.> {
      float+nres &coord<NEportLevels={2,0}>[][] => <-.data.vertices;
      int+nres &poly_nodes<NEportLevels={2,0}>[] => <-.data.nodes;
      long+nres &connect<NEportLevels={2,0}>[] => <-.data.connects;
      int coord_dims[] => array_dims(coord);
      int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
      GMOD.print_error print_error {
	error => <-.error;
	error_source = "polyhedron_mesh";
	error_message =
	  "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	on_inst = 1;
      };
      Mesh out<NEportLevels={0,2}> {
	nnodes => switch((!error),<-.coord_dims[1]);
	nspace => switch((!error),<-.coord_dims[0]);
	coordinates {
	  values => switch((!error),<-.<-.coord);
	};
	ncell_sets = 1;
	Polyhedron cell_set {
	  npolys => switch((!error),array_size(<-.<-.poly_nodes));
	  poly_nnodes => switch((!error),<-.<-.poly_nodes);
	  poly_connect_list => switch((!error),<-.<-.connect);
	};
      };
    };
    FLD_MAP.node_colors node_colors<NEx=729.,NEy=198.> {
      in_data+nres => <-.data.colours;
    };
    FLD_MAP.node_normals node_normals<NEx=909.,NEy=198.> {
      in_data+nres => <-.data.normals;
    };
    FLD_MAP.combine_node_datas combine_node_datas<NEx=810.,NEy=261.> {
      in => {<-.node_colors.out,
	     <-.node_normals.out};
    };
    FLD_MAP.combine_mesh_data combine_mesh_data<NEx=693.,NEy=324.> {
      in_mesh => <-.plane_mesh.out;
      in_nd => <-.combine_node_datas.out;
      DataObject {
	Obj {
	  xform_mode = "Parent";
	  visible => <-.<-.<-.params.planes;
	  name = "planes";
	};
      };
    };
    /*FLD_MAP.polyhedron_mesh plane_mesh<NEx=451.,NEy=242.> {
      coord+nres => <-.data.vertices;
      poly_nodes+nres => <-.data.nodes;
      connect+nres => <-.data.connects;
      DataObject {
	Obj {
	  xform_mode = "Parent";
	  visible => <-.<-.<-.params.planes;
	  name = "planes";
	};
      };
    };
    */
    group point_mesh<NEx=209.,NEy=330.> {
      float+nres &coord<NEportLevels={2,0}>[][] => <-.data.label_pos;
      int coord_dims[] => array_dims(.coord);
      int error =>
	((array_size(.coord) > 0) && (array_size(.coord_dims) != 2));
      GMOD.print_error print_error {
	error => <-.error;
	error_source = "point_mesh";
	error_message =
	  "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	on_inst = 1;
      };
      Mesh out<NEportLevels={0,2}> {
	nnodes => switch((!<-.error),<-.coord_dims[1]);
	nspace => switch((!<-.error),<-.coord_dims[0]);
	coordinates {
	  values => switch((!error),<-.<-.coord);
	};
	ncell_sets = 1;
	Point cell_set {
	  ncells => <-.nnodes;
	  node_connect_list =>
	    switch((!error),init_array(ncells,0,(ncells - 1)));
	};
      };
    };
    /*FLD_MAP.point_mesh point_mesh {
      coord => <-.coord;
      };*/
    group TextValues<export_all=2> {
      int align_horiz = 1;
      int align_vert = 2;
      int drop_shadow = 0;
      int background = 0;
      int bounds = 0;
      int underline = 0;
      int lead_line = 0;
      int radial = 0;
      int do_offset = 1;
      float offset[3] => {0.0, 0.0, 0.1};
      int xform_mode;
      int color;
      string+nres text_values[] => <-.data.labels;
      int stroke = 0;
      group StrokeTextAttribs {
	int font_type;
	int style;
	int plane;
	int orient;
	int path;
	int space_mode;
	float spacing;
	float angle;
	float height;
	float expansion;
	float width;
      };
    };
    Grid+Xform &TextField => merge(.TextValues,point_mesh.out,,,);
    //Mesh+group &TextField => merge(TextValues, point_mesh.out);
    DataObjectNoTexture label {
      in => <-.TextField;
      Props {
	col => {
	  <-.<-.params.red, <-.<-.params.green, <-.<-.params.blue
	};
      };
      Obj {
	xform_mode = "Parent";
	name = "labels";
	visible => <-.<-.params.labels;
      };
    };
    GroupObject wulff<NEx=522.,NEy=441.> {
      child_objs => {
	<-.line_mesh.obj,<-.combine_mesh_data.obj,<-.label.obj
      };
      Props {
	col => {
	  <-.<-.params.red, <-.<-.params.green, <-.<-.params.blue
	};
	trans => <-.<-.params.opacity;
      };
      Top {
	visible => <-.<-.visible;
	xform_mode = "Parent";
	name => <-.<-.name;
      };
    };
    link obj<NEportLevels={1,2},NEx=396.,NEy=550.> => .wulff.obj;
    // pop-up display for vertex list
    macro viewer<NEx=506.,NEy=275.,NEportLevels={0,1},instanced=0> {
      // Todo - can't get font info!
      CCP3.Core_Macros.UI.UIobjs.DLVscrolledWindow scroll<NEx=405.,NEy=252.> {
	parent => <-.DLVshell.UIpanel;
        width => parent.clientWidth;
        height => parent.clientHeight;
#ifdef MSDOS
	virtualWidth => clientWidth;
	virtualHeight => clientHeight;
#endif // MSDOS
      };
      CCP3.Core_Macros.UI.UIobjs.DLVtext DLVlist<NEx=627.,NEy=264.> {
	parent => <-.scroll;
        width => parent.clientWidth;
        height => parent.clientHeight;
        outputOnly = 1;
#ifdef MSDOS
	multiLine = 1;
#endif // MSDOS
	//resizeToText = 1;
	rows => array_size(str_array(text, "\n")) + 1;
        text+nres => <-.<-.data.info;
      };
      CCP3.Core_Macros.UI.UIobjs.DLVshell DLVshell<NEx=286.,NEy=176.> {
	title => (<-.<-.name + " vertices");
	UIshell {
	  visible => <-.<-.<-.params.vertex_display;
	  width = 400;
	  height = 600;
	};
      };
    };
    GMOD.instancer instancer<NEx=264.,NEy=275.> {
      Value => <-.params.vertex_display;
      Group => <-.viewer;
    };
  };
  DV_Param_iso CCP3IsoParam {
    int colour_data;
    //int datamap;
    float min_colour_val;
    float max_colour_val;
    boolean complex;
  };
  macro iso3D_base {
    ilink in_field<export_all=1>;
    CCP3IsoParam IsoParam<NEportLevels=1,export_all=2> {
      iso_component = 0;
      iso_level =>
	cache(((in_field.node_data[.iso_component].min
		+ in_field.node_data[.iso_component].max) / 2));
      color = 1;
      colour_data = 0;
      //datamap = 0;
      min_colour_val+nres => cache(in_field.node_data[.iso_component].min);
      max_colour_val+nres => cache(in_field.node_data[.iso_component].max);
      complex = 0;
    };
    int &nthreads;
    //float+nres linear_min => in_field.node_data[IsoParam.colour_data].min;
    //float+nres linear_max => in_field.node_data[IsoParam.colour_data].max;
    //DefaultLinear DefaultLinear<NEx=-11.,NEy=132.> {
    //  dataMin+nres => linear_min; 
    //  dataMax+nres => linear_max;
    //};
    //DatamapTempl &Datamap<NEx=165.,NEy=132.>;
    //DatamapTempl &Datamaps<NEx=0.,NEy=187.>[] => {DefaultLinear, Datamap};
    int has_cell_data = 0;
    Iso Iso {
      ilink in_fld => <-.in_field;
      DV_Param_iso+IPort2 &param => <-.IsoParam;
      MT_MODS.DVmt_iso DViso {
	in => in_fld;
	&level => param.iso_level;
	cell_data => switch(<-.<-.has_cell_data,param.cell_data);
	&component => param.iso_component;
	nthreads => <-.<-.nthreads;
      };
      DVnmap DVnmap {
	in => in_fld;
	nparam => DViso.nparam;
	map_comp => { <-.param.colour_data };
	iso_comp => param.iso_component;
	iso_val => param.iso_level;
      };
      DVcomb_mesh_and_data DVcomb_mesh_and_data {
	in_mesh => DViso.out;
	in_nd => DVnmap.out;
      };
      olink out_fld => switch((param.color+1),DViso.out,
			      DVcomb_mesh_and_data.out);
    };
    int model<NEportLevels=1,NEx=517.,NEy=22.>;
    olink out_obj<NEportLevels={1,2},NEx=242.,NEy=495.>;
    int visible<NEportLevels=1,NEx=352.,NEy=99.> = 1;
    string name<NEportLevels=1,NEx=770.,NEy=33.>;
  };
  iso3D_base isosurface_base {
    DefaultLinear DefaultLinear<NEx=0.,NEy=132.> {
      dataMin+nres => <-.in_field.node_data[<-.IsoParam.colour_data].min;
      dataMax+nres => <-.in_field.node_data[<-.IsoParam.colour_data].max;
      DataRange[3] = {
	{
	  DataMinValue => <-.dataMin,
	  DataMaxValue => <-.<-.IsoParam.min_colour_val,
	  selectColorRange = 1,
	},
	{
	  DataMinValue => <-.<-.IsoParam.min_colour_val,
	  DataMaxValue => <-.<-.IsoParam.max_colour_val,
	  controlPoints => {
	    DatamapValue[2],
	    DatamapValue[3]
	  },
	},
	{
	  DataMinValue => <-.<-.IsoParam.max_colour_val,
	  DataMaxValue => <-.dataMax,
	  selectColorRange = 1,
	  controlPoints => {
	    DatamapValue[4],
	    DatamapValue[5]
	  },
	}
      };
      DatamapValue[6] = {
	{v1=0.0,v2=0.66,v3=1.0,v4=1.0},
	{v1=1.0,v2=0.66,v3=1.0,v4=1.0},
	{v1=0.0,v2=0.66,v3=1.0,v4=1.0},
	{v1=1.0,v2=0.0,v3=1.0,v4=1.0},
	{v1=1.0,v2=0.0,v3=1.0,v4=1.0},
	{v1=1.0,v2=0.0,v3=1.0,v4=1.0}
      };
    };
  };
  isosurface_base isosurface {
    DataObjectNoTexture obj<NEx=165.,NEy=385.> {
      in => <-.Iso.out_fld;
      //&Datamap => <-.Datamaps[<-.IsoParam.datamap];
      &Datamap => <-.DefaultLinear;
      Obj {
	xform_mode = "Parent";
#ifdef DLV_DL
	cache_size = 1024;
#else
	cache_size = 256;
#endif // DLV_DL
	name => <-.<-.name;
	visible => <-.<-.visible;
      };
    };
    out_obj => .obj.obj;
  };
  isosurface_base isosurface_rep {
    link n<NEportLevels=1,NEx=451.,NEy=176.>;
    Xform &Xform<NEx=627.,NEy=176.>[];
    Mesh+Node_Data Mesh<NEx=66.,NEy=319.,NEportLevels={0,1}>[.n] {
      nnodes => <-.Iso.out_fld.nnodes;
      nspace => <-.Iso.out_fld.nspace;
      &coordinates => <-.Iso.out_fld.coordinates;
      ncell_sets => <-.Iso.out_fld.ncell_sets;
      &cell_set => <-.Iso.out_fld.cell_set;
      &xform => <-.Xform[index_of(<-.Mesh)].xform;
      nnode_data => <-.Iso.out_fld.nnode_data;
      &node_data[] => <-.Iso.out_fld.node_data;
    };
#ifdef DLV_DL
    int cache_size = 1024;
#else
    int cache_size = 256;
#endif // DLV_DL
    int use_altobj = 0;
    DataObjectNoTexture DataObjectNoTexture<NEx=165.,NEy=385.>[.n] {
      in => <-.Mesh[index_of(<-.DataObjectNoTexture)];
      &Datamap => <-.DefaultLinear;
      //&Datamap => <-.Datamaps[<-.IsoParam.datamap];
      &Props => <-.obj.Props;
      Obj {
	use_altobj => <-.<-.use_altobj;
	cache_size => <-.<-.cache_size;
    	xform_mode = "Parent";
	name => name_of(<-.<-.<-) + index_of(<-.<-.DataObjectNoTexture);
      };
    };
    GroupObject obj<NEx=242.,NEy=440.> {
      child_objs => <-.DataObjectNoTexture.obj;
      Top {
	xform_mode = "Parent";
	name => <-.<-.name;
	visible => <-.<-.visible;
      };
    };
    out_obj => .obj.obj;
  };
  iso3D_base iso_wavefn_base {
    IsoParam {
      complex = 1;
    };
    link na;
    link nb;
    link nc;
    int sa = 1;
    int sb = 1;
    int sc = 1;
    DefaultLinear ColourCircle {
      dataMin = 0.0;
      dataMax = 6.28320;
      DatamapValue = {{v2=0.},
		      {v2=1.}};
    };
  };
  iso_wavefn_base iso_wavefn {
    DataObjectNoTexture obj<NEx=165.,NEy=385.> {
      in => <-.Iso.out_fld;
      //&Datamap => <-.map[0];
      Obj {
	xform_mode = "Parent";
	dmap => <-.<-.ColourCircle;
#ifdef DLV_DL
	cache_size = 1024;
#else
	cache_size = 256;
#endif // DLV_DL
	name => <-.<-.name;
	visible => <-.<-.visible;
      };
    };
    out_obj => .obj.obj;
  };
  iso_wavefn_base iso_wavefn_rep {
    CCP3.Core_Modules.Render.k_replicate k_replicate<NEx=583.,NEy=99.> {
      na => <-.na;
      nb => <-.nb;
      nc => <-.nc;
      sa => <-.sa;
      sb => <-.sb;
      sc => <-.sc;
    };
    link n<NEportLevels=1,NEx=451.,NEy=176.> => k_replicate.n;
    Xform &Xform<NEx=627.,NEy=176.>[] => k_replicate.Xform;
    Mesh+Node_Data Mesh<NEx=66.,NEy=319.,NEportLevels={0,1}>[.n] {
      nnodes => <-.Iso.out_fld.nnodes;
      nspace => <-.Iso.out_fld.nspace;
      &coordinates => <-.Iso.out_fld.coordinates;
      ncell_sets => <-.Iso.out_fld.ncell_sets;
      &cell_set => <-.Iso.out_fld.cell_set;
      &xform => <-.Xform[index_of(<-.Mesh)].xform;
      nnode_data => <-.Iso.out_fld.nnode_data;
      &node_data[] => <-.Iso.out_fld.node_data;
    };
#ifdef DLV_DL
    int cache_size = 1024;
#else
    int cache_size = 256;
#endif // DLV_DL
    int use_altobj = 0;
    DataObjectNoTexture DataObjectNoTexture<NEx=165.,NEy=385.>[.n] {
      in => <-.Mesh[index_of(<-.DataObjectNoTexture)];
      //&Datamap => <-.ColourCircle;
      &Props => <-.obj.Props;
      Obj {
	use_altobj => <-.<-.use_altobj;
	dmap => <-.<-.ColourCircle;
	cache_size => <-.<-.cache_size;
    	xform_mode = "Parent";
	name => name_of(<-.<-.<-) + index_of(<-.<-.DataObjectNoTexture);
      };
    };
    GroupObject obj<NEx=242.,NEy=440.> {
      child_objs => <-.DataObjectNoTexture.obj;
      Top {
	xform_mode = "Parent";
	dmap => <-.<-.ColourCircle;
	name => <-.<-.name;
	visible => <-.<-.visible;
      };
    };
    out_obj => .obj.obj;
  };
  DV_Param_contour SCellsParam {
    int map;
  };
  macro shrink_cells_base {
    link in_field<NEportLevels={2,1},NEx=121.,NEy=55.>;
    int visible<NEportLevels=1,NEx=352.,NEy=99.> = 1;
    string name;
    SCellsParam ContourParam {
      contour_comp = 0;
      level_min+nres => cache(((0.6 * in_field.node_data[.contour_comp].min) +
			       (0.4 * in_field.node_data[.contour_comp].max)));
      level_max+nres => cache(((0.4 * in_field.node_data[.contour_comp].min) +
			       (0.6 * in_field.node_data[.contour_comp].max)));
      map = 0;
    };
    macro contour<NEx=275.,NEy=253.> {
      ilink in_field<export_all=1> => <-.in_field;
      SCellsParam &ContourParam => <-.ContourParam;
      DVcontour DVcontour {
        in => <-.in_field;
        &contour_comp => <-.ContourParam.contour_comp;
        map_comp => { <-.ContourParam.map };
        &level_min => <-.ContourParam.level_min;
        &level_max => <-.ContourParam.level_max;
      };
      olink out_fld<export_all=2> => .DVcontour.out;
    };
    macro shrink_cells<NEx=264.,NEy=143.> {
      Mesh &in_field<NEportLevels={2,0},export_all=1> => <-.contour.out_fld;
      DV_Param_separate_cells ShrinkParam<export_all=2> {
        shrink = 1;
        scale = 0.7;
      };
      DVseparate_cells DVseparate_cells {
        in => <-.in_field;
        shrink => param.shrink;
        scale => param.scale;
        DV_Param_separate_cells &param<NEportLevels={2,0}> => <-.ShrinkParam;
      };
    };
    olink out_obj<NEportLevels={1,2},NEx=253.,NEy=418.>;
  };
  shrink_cells_base shrink_cells {
    macro obj {
      group &in<NEportLevels={2,1},NEnumColors=4,NEcolor0=255,NEy=132,NEx=55> => <-.shrink_cells.DVseparate_cells.out;
      imlink child_objs<NEy=77,NEx=132>;
      DefaultLinear Datamap<NEy=231,NEx=11,export_all=1> {
	dataMin => <-.<-.contour.ContourParam.level_min;
	dataMax => <-.<-.contour.ContourParam.level_max;
      };
      DefaultProps Props<NEy=198,NEx=297,export_all=1>;
      DefaultModes Modes<NEy=165,NEx=341,export_all=1>;
      DefaultPickInfo PickInfo<NEy=99,NEx=473,export_all=1>;
      AltObject AltObject<export_all=1,instanced=0> {
	alt_in => <-.in;
      };
      DefaultObject Obj<NEportLevels={0,1},NEy=385,NEx=110,export_all=2> {
	input => <-.in;
	dmap => <-.Datamap;
	xform => in.xform;
	props => <-.Props;
	modes => <-.Modes;
	objects => <-.child_objs;
#ifdef DLV_DL
	cache_size = 1024;
#else
	cache_size = 256;
#endif // DLV_DL
	altobj => <-.AltObject.obj;
	xform_mode = "Parent";
	visible => <-.<-.visible;
	name => <-.<-.name;
	pick_info => <-.PickInfo;
      };
      GMOD.instancer instancer {
	Value => <-.Obj.use_altobj;
	Group => <-.AltObject;
      };
      olink obj<NEy=385,NEx=341> => .Obj;
    };
    out_obj => .obj.obj;
  };
  shrink_cells_base shrink_cells_rep {
    link n;
    Xform &Xform[];
    Mesh+Node_Data Mesh<NEportLevels={0,1}>[.n] {
      nnodes => <-.shrink_cells.DVseparate_cells.out.nnodes;
      nspace => <-.shrink_cells.DVseparate_cells.out.nspace;
      &coordinates => <-.shrink_cells.DVseparate_cells.out.coordinates;
      ncell_sets => <-.shrink_cells.DVseparate_cells.out.ncell_sets;
      &cell_set => <-.shrink_cells.DVseparate_cells.out.cell_set;
      &xform => <-.Xform[index_of(<-.Mesh)].xform;
      nnode_data => <-.shrink_cells.DVseparate_cells.out.nnode_data;
      &node_data[] => <-.shrink_cells.DVseparate_cells.out.node_data;
    };
#ifdef DLV_DL
    int cache_size = 1024;
#else
    int cache_size = 256;
#endif // DLV_DL
    int use_altobj = 0;
    DataObjectNoTexture DataObjectNoTexture[.n] {
      in => <-.Mesh[index_of(<-.DataObjectNoTexture)];
      Datamap {
	dataMin+nres => <-.in.node_data[0].min;
	dataMax+nres => <-.in.node_data[0].max;
      };
      &Props => <-.obj.Props;
      Obj {
	use_altobj => <-.<-.use_altobj;
    	xform_mode = "Parent";
	cache_size => <-.<-.cache_size;
	name => name_of(<-.<-.<-) + index_of(<-.<-.DataObjectNoTexture);
      };
    };
    GroupObject obj {
      child_objs => <-.DataObjectNoTexture.obj;
      Top {
	xform_mode = "Parent";
	visible => <-.<-.visible;
	name => <-.<-.name;
      };
    };
    out_obj => .obj.obj;
  };
  group SphereParam {
    int component;
    int mag_comp;
    int map;
    int mag_map;
    float scale;
  };
  // Todo - take node labels from parent?
  macro spheres_base {
    link in_field<NEportLevels={2,1},NEx=121.,NEy=55.>;
    olink out_obj<NEportLevels={1,2},NEx=264.,NEy=462.>;
    int model<NEportLevels=1>;
    int visible<NEportLevels=1,NEx=352.,NEy=66.> = 1;
    string name;
    SphereParam param {
      component = 0;
      mag_comp = 0;
      map = 0;
      mag_map = 0;
      scale = 1.0;
    };
    group extract_coordinate_array<NEx=121.,NEy=176.> {
      Mesh &in<NEportLevels={2,0}> => <-.in_field;
      int comp[] = {0, 1, 2};
      DVxform_coord DVxform_coord {
	in => <-.in;
	comp => <-.comp;
      };
      olink coord => DVxform_coord.coord;
    };
    group extract_radii<NEx=374.,NEy=176.> {
      Node_Data &in<NEportLevels={2,0}> => <-.in_field;
      int comp<NEportLevels={2,0}> => <-.param.component;
      prim data<NEportLevels={0,2}>[] => in.node_data[.comp].values;
      string &label<NEportLevels={0,2}> => in.node_data[.comp].labels;
      /*DVnode_data_labels DVnode_data_labels {
	in => <-.in;
	int+nres ncomp => <-.in.nnode_data;
	};Todo - remove?*/
    };
    group extract_data<NEx=605.,NEy=176.> {
      Node_Data &in<NEportLevels={2,0}> => <-.in_field;
      int comp<NEportLevels={2,0}> => <-.param.map;
      prim data<NEportLevels={0,2}>[] => in.node_data[.comp].values;
      string &label<NEportLevels={0,2}> => in.node_data[.comp].labels;
      /*DVnode_data_labels DVnode_data_labels {
	in => <-.in;
	int+nres ncomp => <-.in.nnode_data;
	};Todo - remove?*/
    };
    FLD_MAP.radius_data radius_data<NEx=374.,NEy=297.> {
      in_data => switch ((<-.param.mag_comp + 1),
			 ((<-.param.scale * log10((extract_radii.data - in_field.node_data[<-.param.component].min) + 1.0)) + 0.01),
			 ((<-.param.scale * log10(abs(extract_radii.data) + 1.0)) + 0.01));
    };
    FLD_MAP.node_scalar node_scalar<NEx=605.,NEy=297.> {
      in_data => switch ((<-.param.mag_map + 1),
			 log10((extract_data.data - in_field.node_data[<-.param.map].min) + 1.0),
			 log10(abs(extract_data.data) + 1.0));
    };
    FLD_MAP.combine_node_datas combine_node_datas<NEx=440.,NEy=352.> {
      in => {<-.radius_data.out, <-.node_scalar.out };
    };
    group point_mesh<NEx=110.,NEy=286.> {
      float &coord<NEportLevels={2,0}>[][] => <-.extract_coordinate_array.coord;
      int coord_dims[] => array_dims(coord);
      int error => ((array_size(coord) > 0) && (array_size(coord_dims) != 2));
      GMOD.print_error print_error {
	error => <-.error;
	error_source = "point_mesh";
	error_message = "Invalid mesh: coordinates should be 2d array [nnodes][nspace]";
	on_inst = 1;
      };
      Mesh out<NEportLevels={0,2}> {
	nnodes => switch((!error),<-.coord_dims[1]);
	nspace => switch((!error),<-.coord_dims[0]);
	coordinates {
	  values => switch((!error),<-.<-.coord);
	};
	ncell_sets = 1;
	Point cell_set {
	  ncells => <-.nnodes;
	  node_connect_list => switch((!error),init_array(ncells,0,(ncells - 1)));
	};
      };
    };
    group combine_mesh_data<NEx=209.,NEy=396.> {
      Mesh &in_mesh<NEportLevels={2,0}> => <-.point_mesh.out;
      Node_Data+nres &in_nd<NEportLevels={2,0}> => <-.combine_node_datas.out;
      int error => ((is_valid(in_mesh.nnodes) && is_valid(in_nd.nnodes)) && (in_mesh.nnodes != in_nd.nnodes));
      GMOD.print_error print_error {
	error => <-.error;
	error_source = "combine_mesh_data";
	error_message = "Invalid field: mesh dimensions and size of data do not match";
	on_inst = 1;
      };
      Mesh+Node_Data &out<NEportLevels={0,2}> => switch((!error),merge(in_nd,in_mesh));
    };
  };
  spheres_base spheres {
    DataObjectNoTexture obj<NEx=165.,NEy=385.> {
      in => <-.combine_mesh_data.out;
      Obj {
        xform_mode = "Parent";
#ifdef DLV_DL
	cache_size = 1024;
#else
	cache_size = 256;
#endif // DLV_DL
	name => <-.<-.name;
	visible => <-.<-.visible;
      };
    };
    out_obj => .obj.obj;
  };
  spheres_base spheres_rep {
    link n;
    Xform &Xform[];
    Mesh+Node_Data Mesh<NEportLevels={0,1}>[.n] {
      nnodes => <-.combine_mesh_data.out.nnodes;
      nspace => <-.combine_mesh_data.out.nspace;
      &coordinates => <-.combine_mesh_data.out.coordinates;
      ncell_sets => <-.combine_mesh_data.out.ncell_sets;
      &cell_set => <-.combine_mesh_data.out.cell_set;
      &xform => <-.Xform[index_of(<-.Mesh)].xform;
      nnode_data => <-.combine_mesh_data.out.nnode_data;
      &node_data[] => <-.combine_mesh_data.out.node_data;
    };
#ifdef DLV_DL
    int cache_size = 1024;
#else
    int cache_size = 256;
#endif // DLV_DL
    int use_altobj = 0;
    DataObjectNoTexture DataObjectNoTexture[.n] {
      in => <-.Mesh[index_of(<-.DataObjectNoTexture)];
      Datamap {
	dataMin+nres => <-.in.node_data[0].min;
	dataMax+nres => <-.in.node_data[0].max;
      };
      &Props => <-.obj.Props;
      Obj {
	use_altobj => <-.<-.use_altobj;
    	xform_mode = "Parent";
	cache_size => <-.<-.cache_size;
	name => name_of(<-.<-.<-) + index_of(<-.<-.DataObjectNoTexture);
      };
    };
    GroupObject obj {
      child_objs => <-.DataObjectNoTexture.obj;
      Top {
	xform_mode = "Parent";
	visible => <-.<-.visible;
	name => <-.<-.name;
      };
    };
    out_obj => .obj.obj;
  };
  group vr_params {
    int surface;
    int volume;
    int voxel;
    int ray_algo;
    int ray_norm;
    int fat_ray;
    float absorb;
    float emit;
    int alt;
  };
  macro volume_render {
    link in_field<NEportLevels={2,1},NEx=121.,NEy=55.>;
    int visible<NEportLevels=1,NEx=352.,NEy=66.> = 1;
    string name;
    vr_params param {
      surface = 0;
      volume = 3;
      voxel = 1;
      ray_algo = 0;
      ray_norm = 0;
      fat_ray = 1;
      absorb = 0.2;
      emit = 0.2;
      alt = 1;
    };
    //Mesh_Unif+Node_Data &in_mesh<NEcolor1=16711680,NEportLevels={2,1}> =>
    //  in_field;
    DataObject DataObject {
      in => <-.in_field;
      Obj {
	use_altobj => <-.<-.param.alt;
	name => <-.<-.name;
	visible => <-.<-.visible;
      };
    };
    GDmodes_edit GDmodes_edit {
      points = "Inherit";
      lines = "Inherit";
      bounds = "Inherit";
      shell_vis = 1;
      surf => <-.param.surface;
      volume => <-.param.volume;
      modes => DataObject.Modes;
    };
    GDprops_edit GDprops_edit {
      voxel_interp => <-.param.voxel;
      ray_algo => <-.param.ray_algo;
      ray_norm => <-.param.ray_norm;
      fat_ray => <-.param.fat_ray;
      sfp_absorb => <-.param.absorb;
      sfp_emit => <-.param.emit;
      shell_vis = 1;
      props => DataObject.Props;
    };
    olink out_obj => DataObject.obj;
  };
  Points.vector_base hedgehog_base {
    int model<NEportLevels=1>;
    int component = 0;
    string name<NEportLevels=1>;
    scale = 1.0;
    macro Arrow1<NEx=451.,NEy=154.> {
      Line set1 {
    	ncells = 3;
    	node_connect_list = {0,1,1,2,1,3};
      };
      Mesh glyph1 {
    	int nnodes = 4;
	int nspace = 3;
	coordinates {
	  float values[nvals][veclen] = {
	    0.,0.,0.,1.,0.,0.,0.7,0.15,0.,0.7,-0.15,0.
	  };
	};
	int ncell_sets = 1;
	cell_set => {set1};
      };
      olink out_fld => glyph1;
    };
    glyph {
      in_glyph => <-.Arrow1.out_fld;
      GlyphParam {
	glyph_comp => <-.<-.component;
	map_comp => <-.<-.component;
	scale_comp => <-.<-.component;
	normalize = 1;
      };
    };
  };
  hedgehog_base hedgehog;
  // Todo
  hedgehog_base hedgehog_rep;
  DV_Param_stream StreamlineParam {
    int plane;
  };
  macro streamline_base {
    ilink in_field<export_all=1>;
    CCP3.Renderers.Planes.plane_data &in_probe;
    CCP3.Renderers.Planes.slice_plane myplane {
      plane_data => <-.in_probe;
    };
    //ilink in_probe<export_all=1>;
    // Not used yet - Todo
    int model<NEportLevels=1>;
    int visible<NEportLevels=1,NEx=352.,NEy=66.> = 1;
    string name<NEportLevels=1>;
    int &nthreads;
    StreamlineParam StreamParam<export_all=2> {
      component = 0;
      order = 2;
      forw_back = 1;
      nseg = 2;
      max_seg = 256;
      min_vel = 1e-05;
      color = 1;
      ribbons = 0;
      rib_width = 1.;
      rib_angle = 0.;
    };
    MT_MODS.DVmt_stream DVstream {
      in => in_field;
      probe => <-.myplane.obj;
      &order => param.order;
      &forw_back => param.forw_back;
      &nseg => param.nseg;
      &min_vel => param.min_vel;
      &max_seg => param.max_seg;
      &stream_comp => param.component;
      &ribbons => param.ribbons;
      &rib_width => param.rib_width;
      &rib_angle => param.rib_angle;
      &nthreads => <-.nthreads;
      DV_Param_stream &param<NEportLevels={2,0}> => StreamParam;
    };
    DVmagnitude DVmagnitude {
      in => DVstream.out;
    };
    DVswitch DVswitch {
      in => {DVstream.out,
	     DVmagnitude.out};
      index => StreamParam.color;
    };
    DataObjectNoTexture out_obj {
      in => DVswitch.out;
      Obj {
	alt_xform+nres => <-.<-.in_probe.xform;
	xform_mode = "Alternate";
	name => name_of(<-.<-.<-);
      };
    };
    olink obj => out_obj.obj;
  };
  streamline_base streamlines;
  // Todo
  streamline_base streamlines_rep;
};
