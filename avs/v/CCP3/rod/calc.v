
flibrary Calc<NEeditable=1,
#ifndef DLV_NO_DLLS
	     dyn_libs="libCCP3rod",
#endif // DLV_NO_DLLS
	     export_cxx=1,
	     build_dir="avs/src/express",
	     cxx_hdr_files="avs/src/express/calcs.hxx avs/src/core/fb_gen.hxx avs/src/rod/rod_fit_gen.hxx avs/src/rod/rod_plot_gen.hxx avs/src/rod/rod_lists_gen.hxx  avs/src/rod/ffactors_plot_gen.hxx avs/src/express/rod_params.hxx avs/src/express/rod_model.hxx avs/src/express/rod_fit.hxx avs/src/express/rod_plot.hxx avs/src/core/model/move_gen.hxx avs/src/core/model/can_gen.hxx",
	     out_hdr_file="rod_calc.hxx",
	     out_src_file="rod_calc.cxx"> {
  /*List of macros
    runrodUI  (tabs)
    RodFitTabUI
    RodModelTabUI
    RodParamsTabUI
    Rod1dplotTabUI
    Rod2dplotTabUI
    createmodelUI
    loadrodUI (read.v)
    viewrodUI 
    saverodUI (write.v)
   */
  macro runrodUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
    CCP3.ROD.Modules.ParamsData &params<NEx=792.,NEy=77.,
      NEportLevels={2,0}>;
    CCP3.ROD.Modules.LoadData &load_data<NEx=792.,NEy=77.,
      NEportLevels={2,0}>;
    CCP3.ROD.Modules.CalculateRodData &rod_data<NEx=792.,NEy=77.,
      NEportLevels={2,0}>;
    CCP3.ROD.Modules.CalculateFfactorsData &ffac_data<NEx=792.,NEy=77.,
      NEportLevels={2,0}>;
    CCP3.ROD.Modules.EditModelData &edit_model_data<NEx=792.,NEy=77.,
      NEportLevels={2,0}>;		  
//    CCP3.Core_Modules.Model.move_params &move_params;	  
    int visible<NEportLevels=1> = 0;
    int select_tab<NEportLevels=1> = 1;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
      width = 475;
      height = 550;
#ifdef MSDOS
      y = 0;
#endif // MSDOS
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      title = "Run Rod";
      ok = 0;
      okButton = 1;
      cancel = 0;
      cancelButton = 0;
      okLabelString = "Close";						    
      visible=> <-.visible;
    };
    UIpanel top_panel<NEx=374.,NEy=209.> {
      parent => <-.dialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    UIbutton tab_1dplot<NEx=594.,NEy=252.> {
      parent => <-.top_panel;
      color {
	foregroundColor=> switch((select_tab==1)+1, 
				 <-.<-.prefs.colour.foregroundColor,
				 "light grey");
	backgroundColor=> switch((select_tab==1)+1, 
				 <-.<-.prefs.colour.backgroundColor,
				 "#707070");
      };
      &fontAttributes => <-.prefs.fontAttributes;
      label => "1D Plot";
      width => parent.clientWidth/5;
      x => 0;
      y => 0;
    };
    UIbutton tab_2dplot<NEx=594.,NEy=252.> {
      parent => <-.top_panel;
      color {
	foregroundColor=> switch((select_tab==2)+1, 
				 <-.<-.prefs.colour.foregroundColor,
				 "light grey");
	backgroundColor=> switch((select_tab==2)+1, 
				 <-.<-.prefs.colour.backgroundColor,
				 "#707070");
      };
      &fontAttributes => <-.prefs.fontAttributes;
      label => "2D Plot";
      width => parent.clientWidth/5;
      x => parent.clientWidth/5;
      y => 0;
    };
    UIbutton tab_fit<NEx=594.,NEy=252.> {
      parent => <-.top_panel;
      color {
	foregroundColor=> switch((select_tab==3)+1, 
				 <-.<-.prefs.colour.foregroundColor,
				 "light grey");
	backgroundColor=> switch((select_tab==3)+1, 
				 <-.<-.prefs.colour.backgroundColor,
				 "#707070");
      };
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Fit";
      width => parent.clientWidth/5;
      x => parent.clientWidth*2/5;
      y => 0;
    };
    UIbutton tab_params<NEx=594.,NEy=252.> {
      parent => <-.top_panel;
      color {
	foregroundColor=> switch((select_tab==4)+1, 
				 <-.<-.prefs.colour.foregroundColor,
				 "light grey");
	backgroundColor=> switch((select_tab==4)+1, 
				 <-.<-.prefs.colour.backgroundColor,
				 "#707070");
      };
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Set Params";
      width => parent.clientWidth/5;
      x => parent.clientWidth*3/5;
      y => 0;
     };
    UIbutton tab_model<NEx=594.,NEy=252.> {
      parent => <-.top_panel;
      color {
	foregroundColor=> switch((select_tab==5)+1, 
				 <-.<-.prefs.colour.foregroundColor,
				 "light grey");
	backgroundColor=> switch((select_tab==5)+1, 
				 <-.<-.prefs.colour.backgroundColor,
				 "#707070");
      };
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Edit Model";
      width => parent.clientWidth/5;
      x => parent.clientWidth*4/5;
      y => 0;
     };		  
    CCP3.Core_Macros.UI.UIobjs.DLVscrolledWindow main_panel {
      parent => <-.top_panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = tab_1dplot.y + tab_1dplot.height;
      width => parent.clientWidth;
      height => parent.clientHeight - y - 20;
#ifdef MSDOS
      virtualWidth => width;
      virtualHeight => height;
#endif // MSDOS
    };
    CCP3.ROD.Plot.Rod1dplotTabUI Rod1dplotTabUI<NEx=176.,NEy=385.> {
      panel.parent => <-.<-.main_panel;
      prefs => <-.prefs;
      params => <-.params;
      load_data => <-.load_data;
      rod_data => <-.rod_data;
      select_tab => <-.select_tab;
    };
    GMOD.instancer instance_rod1dplot<NEx=200.,NEy=350.,instanced=0> {
      Value => <-.tab_1dplot.do;
      Group => <-.Rod1dplotTabUI;
    };
    GMOD.parse_v parse_v1<NEx=319.,NEy=77.> {
      v_commands = "select_tab = 1;";
      trigger => <-.tab_1dplot.do;
      on_inst = 0;
      relative => <-;				    
    };
    CCP3.ROD.Plot.Rod2dplotTabUI Rod2dplotTabUI<NEx=176.,NEy=385.> {
      panel.parent => <-.<-.main_panel;
      prefs => <-.prefs;
      params => <-.params;
      load_data => <-.load_data;
      ffac_data => <-.ffac_data;
      select_tab => <-.select_tab;
    };
    GMOD.instancer instance_rod2dplot<NEx=200.,NEy=350.,instanced=0> {
      Value => <-.tab_2dplot.do;
      Group => <-.Rod2dplotTabUI;
    };
    GMOD.parse_v parse_v2<NEx=319.,NEy=77.> {
      v_commands = "select_tab = 2;";
      trigger => <-.tab_2dplot.do;
      on_inst = 0;
      relative => <-;				    
    };
    CCP3.ROD.Fit.RodFitTabUI RodFitTabUI<NEx=176.,NEy=385.> {
      panel.parent => <-.<-.main_panel;
      prefs => <-.prefs;
      params => <-.params;
      load_data => <-.load_data;
      select_tab => <-.select_tab;
    };
    GMOD.instancer instance_rodfit<NEx=200.,NEy=350.,instanced=0> {
      Value => <-.tab_fit.do;
      Group => <-.RodFitTabUI;
    };
    GMOD.parse_v parse_v3<NEx=319.,NEy=77.> {
      v_commands = "select_tab = 3;";
      trigger => <-.tab_fit.do;
      on_inst = 0;
      relative => <-;				    
    };
    CCP3.ROD.Params.RodParamsTabUI RodParamsTabUI<NEx=176.,NEy=385.> {
      panel.parent => <-.<-.main_panel;
      prefs => <-.prefs;
      params => <-.params;
      select_tab => <-.select_tab;
    };
    GMOD.instancer instance_rodparams<NEx=200.,NEy=350.,instanced=0> {
      Value => <-.tab_params.do;
      Group => <-.RodParamsTabUI;
    };
    GMOD.parse_v parse_v4<NEx=319.,NEy=77.> {
      v_commands = "select_tab = 4;";
      trigger => <-.tab_params.do;
      on_inst = 0;
      relative => <-;				    
    };
    CCP3.ROD.Model.RodModelTabUI RodModelTabUI<NEx=176.,NEy=385.> {
      panel.parent => <-.<-.main_panel;
      prefs => <-.prefs;
      params => <-.params;
      load_data => <-.load_data;
      edit_model_data => <-.edit_model_data;
      select_tab => <-.select_tab;
    };
    GMOD.instancer instance_rodmodel<NEx=200.,NEy=350.,instanced=0> {
      Value => <-.tab_model.do;
      Group => <-.RodModelTabUI;
    };
    GMOD.parse_v parse_v5<NEx=319.,NEy=77.> {
      v_commands = "select_tab = 5;";
      trigger => <-.tab_model.do;
      on_inst = 0;
      relative => <-;				    
    };
    UIlabel results_chisqr {
      parent => <-.top_panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x => 0;
      y => top_panel.clientHeight- 25;
      alignment = "center";
      width => parent.clientWidth;
      label => str_format("chisqr = %9.3f normalized = %8.3f Quality = %5.3f", 
			  <-.params.chisqr, <-.params.norm, <-.params.quality);
      visible =>  <-.params.show_fit_results;
    }; 
  };
};
