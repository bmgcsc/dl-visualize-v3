
flibrary Model<NEeditable=1,
#ifndef DLV_NO_DLLS
	     dyn_libs="libCCP3rod",
#endif // DLV_NO_DLLS
	     export_cxx=1,
	     build_dir="avs/src/express",
	       cxx_hdr_files="avs/src/express/calcs.hxx avs/src/rod/rod_model_gen.hxx avs/src/rod/rod_labels_gen.hxx avs/src/core/model/move_gen.hxx avs/src/core/model/insa_gen.hxx avs/src/core/model/can_gen.hxx avs/src/express/crystal_base.hxx avs/src/express/ui/edit_model.hxx",
	     out_hdr_file="rod_model.hxx",
	     out_src_file="rod_model.cxx"> {
  macro RodModelTabUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1}>;
    CCP3.ROD.Modules.ParamsData &params<NEportLevels={2,0}>;
    CCP3.ROD.Modules.LoadData &load_data<NEx=792.,NEy=77.,
      NEportLevels={2,0}>;
    CCP3.ROD.Modules.EditModelData &edit_model_data<NEx=792.,NEy=77.,
      NEportLevels={2,0}>;		       
    link select_tab<NEportLevels=1>;
    CCP3.Core_Macros.UI.UIobjs.DLVscrolledWindow panel<NEx=374.,NEy=209.> {
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
#ifdef MSDOS
      virtualWidth => width;
      virtualHeight => height;
#endif // MSDOS
      visible => <-.select_tab==5;
    };
    int visible_dw1 = 0;
    GMOD.parse_v parse_reset<NEx=319.,NEy=77.> {
      v_commands = "visible_dw1 = 0; visible_dw2 = 0; visible_occ = 0;";
      trigger => <-.select_tab;
      on_inst = 0;
      relative => <-;				    
    };
    link nselections;
    UIlabel instructions {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y = 0;
      width => parent.clientWidth;
      alignment = "center";
      label = "Select an atom to modify its properties";
    };
    UIbutton move_atom_button<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Move atom";
      width => parent.clientWidth/3;
      active => <-.nselections;
      x => 0; 
      y => instructions.y + instructions.height + 5;
    };
    UIbutton insert_atom_button<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Insert atom";
      width => parent.clientWidth/3;
      x => 0; 
      y => move_atom_button.y + move_atom_button.height + 5;
    };
    UIbutton delete_atom_button<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Delete atom";
      width => parent.clientWidth/3;
      active => <-.nselections;
      x => 0; 
      y => insert_atom_button.y + insert_atom_button.height + 5;
    };
    UIbutton change_atom_button<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Change atom";
      width => parent.clientWidth/3;
      active => <-.nselections;
      x => 0; 
      y => delete_atom_button.y + delete_atom_button.height + 5;
    };
    macro MoveAtom<NEx=306.,NEy=503.> {
      int visible<NEportLevels=1,NEx=671.,NEy=55.> => <-.move_atom_button.do;
      link UIparent => <-.panel;
      int ok<NEportLevels=1,NEx=231.,NEy=209.> = 0;
      int cancel<NEportLevels=1,NEx=528.,NEy=220.> = 0;
      int model_type = 4; //HACK
      GMOD.instancer instancer<NEx=517.,NEy=165.> {
	Group => move_atomUI;
	Value => <-.visible;
      };
      CCP3.Core_Modules.Model.move_params move_params<NEx=774.,NEy=171.,
	NEportLevels={0,1}> {
	fractional = 0;
	x = 0.0;
	y = 0.0;
	z = 0.0;
	use_transform = 0;
      };
      CCP3.Core_Macros.UI.EditModel.move_atomUI move_atomUI<NEx=363.,NEy=286.,instanced=0> {
	preferences => <-.<-.prefs;
	UIparent => <-.<-.panel;
	visible => <-.visible;
	nselections => <-.<-.nselections;
	DLVdialog.ok => <-.<-.ok;
	DLVdialog.cancel => <-.<-.cancel;
	params => <-.move_params;
	model_type => <-.model_type;
	initialise {
	  v_commands = "index = 0; new_view = 0; name = \"rod atom move\";";
	};										    
      };
    };
    macro insert_atomUI {
      int visible<NEportLevels=1,NEx=671.,NEy=55.> => <-.insert_atom_button.do;
      link UIparent => <-.panel;
      int ok<NEportLevels=1,NEx=231.,NEy=209.> = 0;
      int cancel<NEportLevels=1,NEx=528.,NEy=220.> = 0;
      int model_type = 4; //HACK
      GMOD.instancer instancer<NEx=517.,NEy=165.> {
	Group => insert_atomUI;
	Value => <-.visible;
      };
      CCP3.Core_Modules.Model.move_params &params<NEx=639.,NEy=162.,
	NEportLevels={2,1}> {
	fractional = 0;
	x = 0.0;
	y = 0.0;
	z = 0.0;
	use_transform = 0;
      };
      CCP3.Core_Macros.UI.EditModel.insert_atomUI insert_atomUI<NEx=363.,NEy=286.,instanced=0> {
	preferences => <-.<-.prefs;
	UIparent => <-.<-.panel;
	visible => <-.visible;
	DLVdialog.ok => <-.<-.ok;
	DLVdialog.cancel => <-.<-.cancel;
	params => <-.params;
	model_type => <-.model_type;
	initialise {
	  v_commands = "index = 0; new_view = 0; name = \"rod insert atom\";";
	};										    
      };
    };
    macro delete_atomUI {
      int visible<NEportLevels=1,NEx=671.,NEy=55.> => <-.delete_atom_button.do;
      link UIparent => <-.panel;
      int ok<NEportLevels=1,NEx=231.,NEy=209.> = 0;
      int cancel<NEportLevels=1,NEx=528.,NEy=220.> = 0;
      GMOD.instancer instancer<NEx=517.,NEy=165.> {
	Group => delete_atomUI;
	Value => <-.visible;
      };
      CCP3.Core_Macros.UI.EditModel.delete_atomUI delete_atomUI<NEx=363.,NEy=286.,instanced=0> {
	preferences => <-.<-.prefs;
	UIparent => <-.<-.panel;
	visible => <-.visible;
	DLVdialog.ok => <-.<-.ok;
	DLVdialog.cancel => <-.<-.cancel;
	nselections => <-.<-.nselections;
	initialise {
	  v_commands = "index = 0; new_view = 0; name = \"rod delete atom\";";
	};										    
      };
    }; 
    macro change_atomUI {
      int visible<NEportLevels=1,NEx=671.,NEy=55.> => <-.change_atom_button.do;
      link UIparent => <-.panel;
      int ok<NEportLevels=1,NEx=231.,NEy=209.> = 0;
      int cancel<NEportLevels=1,NEx=528.,NEy=220.> = 0;
      GMOD.instancer instancer<NEx=517.,NEy=165.> {
	Group => change_atomUI;
	Value => <-.visible;
      };
      CCP3.Core_Macros.UI.EditModel.change_atomUI change_atomUI<NEx=363.,NEy=286.,instanced=0> {
	preferences => <-.<-.prefs;
	UIparent => <-.<-.panel;
	visible => <-.visible;
	DLVdialog.ok => <-.<-.ok;
	DLVdialog.cancel => <-.<-.cancel;
	nselections => <-.<-.nselections;
	initialise {
	  v_commands = "index = 0; new_view = 0; name = \"rod change atom\";";
	};										    
      };
    };
		       
    CCP3.ROD.Modules.edit_model edit_model<NEx=407.,NEy=484.> {
      trigger_edit => (<-.MoveAtom.move_params.x || <-.MoveAtom.move_params.y 
		       || <-.MoveAtom.move_params.z
		       || <-.insert_atomUI.params.x || <-.insert_atomUI.params.y 
		       || <-.insert_atomUI.params.z ||  <-.insert_atomUI.insert_atomUI.atom
		       || <-.delete_atom_button.do||  <-.change_atomUI.change_atomUI.atom);
      trigger_cancel => (<-.MoveAtom.cancel || <-.insert_atomUI.cancel
			      || <-.delete_atomUI.cancel|| <-.change_atomUI.cancel);
      trigger_accept => (<-.MoveAtom.ok || <-.insert_atomUI.ok
			      || <-.delete_atomUI.ok || <-.change_atomUI.ok);
      load_data=> <-.load_data;
      params => <-.params;
      edit_model_data => <-.edit_model_data;
    };
    int display_labels = 0;
    int edit_dw1 = 0;
    UIbutton change_dw1_button<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
#ifdef MSDOS
      label => "Show/Edit DW1 Labels";
#else
      label => "Show/Edit In-plane\n DW Labels";
#endif // MSDOS      
      width => parent.clientWidth*3/10;
      height => 48;
      visible=> !<-.visible_dw1;
      x => 0;
      y => change_atom_button.y + change_atom_button.height + 5;
    };
    UIbutton hide_dw1_button<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
#ifdef MSDOS
      label => "Hide DW1 Labels";
#else
      label => "Hide In-plane\n DW Labels";
#endif // MSDOS 
      width => parent.clientWidth*3/10;
      height => 48;
      visible=> <-.visible_dw1;
      x => 0;
      y => change_atom_button.y + change_atom_button.height + 5;
    };
    GMOD.parse_v parse_dw1_on<NEx=319.,NEy=77.> {
      v_commands = "visible_dw1 = 1; visible_dw2 = 0; visible_occ = 0; edit_dw1 = 1; display_labels = 2;";
      trigger => <-.change_dw1_button.do;
      on_inst = 0;
      relative => <-;				    
    };
    GMOD.parse_v parse_dw1_off<NEx=319.,NEy=77.> {
      v_commands = "visible_dw1 = 0; change_dw1_button.visible = 1; hide_dw1_button.visible = 0; display_labels = 0;";
      trigger => <-.hide_dw1_button.do;
      on_inst = 0;
      relative => <-;				    
    };
    GMOD.parse_v parse_dw1_edit_off<NEx=319.,NEy=77.> {
      v_commands = "edit_dw1 = 0;";
      trigger => <-.accept_dw1_button.do;
      on_inst = 0;
      relative => <-;				    
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider select_dw1<NEx=594.,NEy=216.> {
      parent => <-.panel; 
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      title => "Select label number";
      min => 0;
      max => <-.params.ndwtot+1;
      value=> showValue;
      width => parent.clientWidth*3/10 - 5;
      x => parent.clientWidth*3/10 + 5;
      y => change_dw1_button.y + 5;
      visible => visible_dw1 && nselections && edit_dw1;
    };
    UIbutton accept_dw1_button<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Accept";
      width => parent.clientWidth/5;
      x => parent.clientWidth*3/5;
      y => change_dw1_button.y + 14;
      visible => visible_dw1 && nselections && edit_dw1;
    };
    int visible_dw2 = 0;
    int edit_dw2 = 0;
    UIbutton change_dw2_button<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
#ifdef MSDOS
      label => "Show/Edit DW2 Label";
#else
      label => "Show/Edit Out-of-\n plane DW Label";
#endif // MSDOS
      label => "Show/Edit Out-of-\n plane DW Label";
      width => parent.clientWidth*3/10;
      visible=> !<-.visible_dw2;
      height => 48;
      x => 0;
      y => change_dw1_button.y + change_dw1_button.height + 5;
    };
    UIbutton hide_dw2_button<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
#ifdef MSDOS
      label => "Hide DW2 Label";
#else
      label => "Hide Out-of-\n plane DW Label";
#endif // MSDOS
      width => parent.clientWidth*3/10;
      height => 48;
      visible=> <-.visible_dw2;
      x => 0;
      y => change_dw1_button.y + change_dw1_button.height + 5;
    };
    GMOD.parse_v parse_dw2_on<NEx=319.,NEy=77.> {
      v_commands = "visible_dw1 = 0; visible_dw2 = 1; visible_occ = 0; edit_dw2 = 1; display_labels = 3;";
      trigger => <-.change_dw2_button.do;
      on_inst = 0;
      relative => <-;				    
    };
    GMOD.parse_v parse_dw2_off<NEx=319.,NEy=77.> {
      v_commands = "visible_dw2 = 0; change_dw2_button.visible = 1; hide_dw2_button.visible = 0; display_labels = 0;";
      trigger => <-.hide_dw2_button.do;
      on_inst = 0;
      relative => <-;				    
    };
    GMOD.parse_v parse_dw2_edit_off<NEx=319.,NEy=77.> {
      v_commands = "edit_dw2 = 0;";
      trigger => <-.accept_dw2_button.do;
      on_inst = 0;
      relative => <-;				    
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider select_dw2<NEx=594.,NEy=216.> {
      parent => <-.panel; 
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      title => "Select label number";
      min => 0;
      max => <-.params.ndwtot2+1;
      value=> showValue;
      width => parent.clientWidth*3/10 - 5;
      x => parent.clientWidth*3/10 + 5;
      y => change_dw2_button.y + 5;
      visible => visible_dw2 && nselections && edit_dw2;
    };
    UIbutton accept_dw2_button<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Accept";
      width => parent.clientWidth/5;
      x => parent.clientWidth*3/5;
      y => change_dw2_button.y + 14;
      visible => visible_dw2 && nselections && edit_dw2;
    };
    int visible_occ = 0;
    int edit_occ = 0;
    UIbutton change_occ_button<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
#ifdef MSDOS
      label => "Show/Edit Occ Label";
#else
      label => "Show/Edit\n Occupation Label";
#endif // MSDOS
      width => parent.clientWidth*3/10;
      height => 48;
      visible => !<-.visible_occ;
      x => 0;
      y =>  change_dw2_button.y + change_dw2_button.height + 5;
    };
    UIbutton hide_occ_button<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
#ifdef MSDOS
      label => "Hide Occ Label";
#else
      label => "Hide\n Occupation Label";
#endif // MSDOS
      width => parent.clientWidth*3/10;
      height => 48;
      visible => <-.visible_occ;
      x => 0;
      y =>  change_dw2_button.y + change_dw2_button.height + 5;
    };
    GMOD.parse_v parse_occ_on<NEx=319.,NEy=77.> {
      v_commands = "visible_dw1 = 0; visible_dw2 = 0; visible_occ = 1; edit_occ = 1; display_labels = 4;";
      trigger => <-.change_occ_button.do;
      on_inst = 0;
      relative => <-;				    
    };
    GMOD.parse_v parse_occ_off<NEx=319.,NEy=77.> {
      v_commands = "visible_occ = 0; display_labels = 0;";
      trigger => <-.hide_occ_button.do;
      on_inst = 0;
      relative => <-;				    
    };
    GMOD.parse_v parse_occ_edit_off<NEx=319.,NEy=77.> {
      v_commands = " edit_occ = 0;";
      trigger => <-.accept_occ_button.do;
      on_inst = 0;
      relative => <-;				    
    };
    CCP3.Core_Macros.UI.UIobjs.DLViSlider select_occ<NEx=594.,NEy=216.> {
      parent => <-.panel; 
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      title => "Select label number";
      min => 0;
      max => <-.params.nocctot+1;
      value=> showValue;
      width => parent.clientWidth*3/10 - 5;
      x => parent.clientWidth*3/10 + 5;
      y => change_occ_button.y + 5;
      visible => visible_occ && nselections && edit_occ;
    };
    UIbutton accept_occ_button<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "Accept";
      width => parent.clientWidth/5;
      x => parent.clientWidth*3/5;
      y => change_occ_button.y + 14;
      visible => visible_occ && nselections && edit_occ;
    };
    CCP3.ROD.Modules.atom_labels atom_labels<NEx=407.,NEy=484.> {
      trigger_show => <-.display_labels; 
      trigger_dw1 => <-.accept_dw1_button.do;
      new_dw1 => <-.select_dw1.value;
      trigger_dw2 => <-.accept_dw2_button.do;
      new_dw2 => <-.select_dw2.value;
      trigger_occ => <-.accept_occ_button.do;
      new_occ => <-.select_occ.value;
      params => <-.params;
    };   
  };
};
	       
