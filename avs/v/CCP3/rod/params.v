
flibrary Params<NEeditable=1,
#ifndef DLV_NO_DLLS
	     dyn_libs="libCCP3rod",
#endif // DLV_NO_DLLS
	     export_cxx=1,
	     build_dir="avs/src/express",
             cxx_hdr_files="avs/src/express/calcs.hxx avs/src/core/fb_gen.hxx", 
	     out_hdr_file="rod_params.hxx",
	     out_src_file="rod_params.cxx"> {


  macro RodParamsTabUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1}>;
    CCP3.ROD.Modules.ParamsData &params<NEportLevels={2,0}>;
    link select_tab<NEportLevels=1>;
    CCP3.Core_Macros.UI.UIobjs.DLVscrolledWindow panel<NEx=374.,NEy=209.> {
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
#ifdef MSDOS
      virtualWidth => width;
      virtualHeight => height;
#endif // MSDOS
      visible => <-.select_tab==4;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein atten<NEx=18.,NEy=288.>
    {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Attenuation factor of beam";
      fval => <-.params.atten;
      y =0  ;
      width=> <-.panel.clientWidth;
      label.width => (<-.panel.width*3/4)-5;
      field.x => <-.label.width;
      field.width => (<-.panel.width/4);
      field.decimalPoints = 5;
    };
  };
};

