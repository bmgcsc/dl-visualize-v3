
flibrary Plot<NEeditable=1,
#ifndef DLV_NO_DLLS
	     dyn_libs="libCCP3rod",
#endif // DLV_NO_DLLS
	     export_cxx=1,
	     build_dir="avs/src/express",
             cxx_hdr_files="avs/src/express/calcs.hxx avs/src/core/fb_gen.hxx", 
	     out_hdr_file="rod_plot.hxx",
	     out_src_file="rod_plot.cxx"> {
  macro Rod1dplotTabUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1}>;
    CCP3.ROD.Modules.ParamsData &params<NEportLevels={2,0}>;
    CCP3.ROD.Modules.CalculateRodData &rod_data<NEx=792.,NEy=77.,
      NEportLevels={2,0}>;
    CCP3.ROD.Modules.LoadData &load_data<NEx=792.,NEy=77.,
      NEportLevels={2,0}>;
    link select_tab<NEportLevels=1>;
    CCP3.Core_Macros.UI.UIobjs.DLVscrolledWindow panel<NEx=374.,NEy=209.> {
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
#ifdef MSDOS
      virtualWidth => width;
      virtualHeight => height;
#endif // MSDOS
      visible => <-.select_tab==1;
    };
   CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein indexh<NEx=18.,NEy=288.>
    {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Diffraction index h";
      fval => <-.rod_data.h;
      y = 0;
      width=> <-.panel.clientWidth;
      label.width => (<-.panel.width*3/4)-5;
      field.x => <-.label.width;
      field.width => (<-.panel.width/4);
      field.decimalPoints = 1;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein indexk<NEx=18.,NEy=288.>
    {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Diffraction index k";
      fval => <-.rod_data.k;
      y => (<-.indexh.y + <-.indexh.height + 5);
      width=> <-.panel.clientWidth;
      label.width => (<-.panel.width*3/4)-5;
      field.x => <-.label.width;
      field.width => (<-.panel.width/4);
      field.decimalPoints = 1;
    };
    UIlabel indexl {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.indexk.y + <-.indexk.height + 5);
      alignment = "left";
      width => parent.clientWidth;
      label = "Diffraction index l";
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein startl<NEx=18.,NEy=288.>
    {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "Start";
      fval => <-.rod_data.lstart;
      y => (<-.indexl.y + <-.indexl.height + 5);
      x = 0;
      width=> <-.panel.clientWidth/3.0;
      label.width => (<-.panel.width/2 - 4);
      field.x => <-.label.width;
      field.width => (<-.panel.width/2 + 4);
      field.decimalPoints = 1;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein endl<NEx=18.,NEy=288.>
    {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => "End";
      fval => <-.rod_data.lend;
      y => (<-.indexl.y + <-.indexl.height + 5);
      x => <-.panel.clientWidth/3.0;
      width=> <-.panel.clientWidth/3.0;
      label.width => (<-.panel.width/2 - 7);
      field.x => <-.label.width;
      field.width => (<-.panel.width/2 + 7);
      field.decimalPoints = 1;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVifieldTypein npoints<NEx=18.,NEy=288.>
    {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      flabel => " No. points";
      fval => <-.rod_data.nl;
      y => (<-.indexl.y + <-.indexl.height + 5);
      x => <-.panel.clientWidth*2.0/3.0;
      width=> <-.panel.clientWidth/3.0;
      label.width => (<-.panel.width/2 + 11);
      field.x => <-.label.width;
      field.width => (<-.panel.width/2 - 11);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle plot_bulk<NEx=216.,NEy=441.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((startl.y + startl.height) + 5);
      width => parent.clientWidth;
      label = "Bulk contribution (against l)";
      set => (<-.rod_data.plot_bulk); 
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle plot_surf<NEx=216.,NEy=441.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((plot_bulk.y + plot_bulk.height) + 5);
      width => parent.clientWidth;
      label = "Surface contribution (against l)";
      set => (<-.rod_data.plot_surf); 
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle plot_both<NEx=216.,NEy=441.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((plot_surf.y + plot_surf.height) + 5);
      width => parent.clientWidth;
      label = "Interference sum of bulk and surface";
      set => (<-.rod_data.plot_both); 
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle plot_exp<NEx=216.,NEy=441.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((plot_both.y + plot_both.height) + 5);
      width => parent.clientWidth;
      label = "Experimental data Points";
      set => (<-.rod_data.plot_exp);
      active => <-.load_data.got_exp_data;
    };
    UIbutton plot_rod_button<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "PLOT";
      width => parent.clientWidth/3;
      x => parent.clientWidth/3;
      y => (plot_exp.y + plot_exp.height + 5);
      active => (<-.rod_data.plot_bulk || <-.rod_data.plot_surf || 
		 <-.rod_data.plot_both || <-.rod_data.plot_exp);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle new_view<NEx=216.,NEy=441.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((plot_rod_button.y + plot_rod_button.height) + 5);
      width => parent.clientWidth;
      label = "Open in new viewer";
      set => (<-.rod_data.new_view); 
    };
    CCP3.ROD.Modules.plot_rod plot_rod<NEx=407.,NEy=484.> {
      trigger => <-.plot_rod_button.do;
      rod_data => <-.rod_data;
      params => <-.params;
      load_data => <-.load_data;						   
    };
  };
  macro Rod2dplotTabUI {
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1}>;
    CCP3.ROD.Modules.ParamsData &params<NEportLevels={2,0}>;
    CCP3.ROD.Modules.CalculateFfactorsData &ffac_data<NEx=792.,NEy=77.,
      NEportLevels={2,0}>;
    CCP3.ROD.Modules.LoadData &load_data<NEx=792.,NEy=77.,
      NEportLevels={2,0}>;
    link select_tab<NEportLevels=1>;
    CCP3.Core_Macros.UI.UIobjs.DLVscrolledWindow panel<NEx=374.,NEy=209.> {
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
#ifdef MSDOS
      virtualWidth => width;
      virtualHeight => height;
#endif // MSDOS
      visible => <-.select_tab==2;
    };

    CCP3.Core_Macros.UI.UIobjs.DLVradioBox select_fs<NEx=18.,NEy=432.> {
      parent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => <-.select_fs_list;
      selectedItem => ffac_data.select_fs;
      y =  0;
      title => "F factors selection method";
      width => parent.clientWidth;
      active = 1;
      label_cmd {
	cmd<NEdisplayMode="open">[] = {,,
				       {
					 active=> <-.<-.<-.load_data.got_exp_data,,,,,,,,,,,,,,
				       }};
      };
    };
    string select_fs_list<NEportLevels=1,NEx=33.,NEy=143.>[] = {
    	"specify range of h and k",
	"specify q-max",
	"plot for all data points"
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein indexhstart {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      panel.visible => (<-.<-.ffac_data.select_fs==0);
      flabel => " h:  Start ";
      fval => <-.ffac_data.h_start;
      y => (<-.select_fs.y + <-.select_fs.height + 5);
      width=> <-.panel.clientWidth/3+6;
      label.alignment = "right";
      label.width => (<-.panel.width-<-.field.width);
      field.x => <-.label.width;
      field.width = 50;
      field.decimalPoints = 1;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein indexhend {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      panel.visible => (<-.<-.ffac_data.select_fs==0);
      flabel => "End ";
      fval => <-.ffac_data.h_end;
      x => <-.panel.clientWidth/3+6;
      y => (<-.select_fs.y + <-.select_fs.height + 5);
      width=> <-.panel.clientWidth/3-18;
      label.alignment = "right";
      label.width => (<-.panel.width-<-.field.width);
      field.x => <-.label.width;
      field.width = 50;
      field.decimalPoints = 1;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein indexhstep {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      panel.visible => (<-.<-.ffac_data.select_fs==0);
      flabel => "Step size ";
      fval => <-.ffac_data.h_step;
      x => <-.panel.clientWidth*2/3-12;
      y => (<-.select_fs.y + <-.select_fs.height + 5);
      width=> <-.panel.clientWidth/3+12;
      label.alignment = "right";
      label.width => (<-.panel.width-<-.field.width);
      field.x => <-.label.width;
      field.width = 50;
      field.decimalPoints = 1;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein indexkstart {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      panel.visible => (<-.<-.ffac_data.select_fs==0);
      flabel => " k:  Start ";
      fval => <-.ffac_data.k_start;
      y => (<-.indexhstart.y + <-.indexhstart.height + 5);
      width=> <-.panel.clientWidth/3+6;
      label.alignment = "right";
      label.width => (<-.panel.width-<-.field.width);
      field.x => <-.label.width;
      field.width = 50;
      field.decimalPoints = 1;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein indexkend {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      panel.visible => (<-.<-.ffac_data.select_fs==0);
      flabel => "End ";
      fval => <-.ffac_data.k_end;
      x => <-.panel.clientWidth/3+6;
      y => (<-.indexhstart.y + <-.indexhstart.height + 5);
      width=> <-.panel.clientWidth/3-18;
      label.alignment = "right";
      label.width => (<-.panel.width-<-.field.width);
      field.x => <-.label.width;
      field.width = 50;
      field.decimalPoints = 1;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein indexkstep {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      panel.visible => (<-.<-.ffac_data.select_fs==0);
      flabel => "Step size ";
      fval => <-.ffac_data.k_step;
      x => <-.panel.clientWidth*2/3-12;
      y => (<-.indexhstart.y + <-.indexhstart.height + 5);
      width=> <-.panel.clientWidth/3+12;
      label.alignment = "right";
      label.width => (<-.panel.width-<-.field.width);
      field.x => <-.label.width;
      field.width = 50;
      field.decimalPoints = 1;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein maxq {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      panel.visible => (<-.<-.ffac_data.select_fs==1);
      flabel => "Max value of q (in terms of h)";
      fval => <-.ffac_data.maxq;
      y => (<-.select_fs.y + <-.select_fs.height + 5);
      width=> <-.panel.clientWidth-10;
      label.width => (<-.panel.width*2/3);
      field.x => <-.label.width;
      field.width => (<-.panel.width*2/3);
      field.decimalPoints = 1;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein qsteph {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
       panel.visible => (<-.<-.ffac_data.select_fs==1);
      flabel => "Step size of h";
      fval => <-.ffac_data.h_step;
      x =0;					       
      y => (<-.indexhstart.y + <-.indexhstart.height + 5);
      width=> <-.panel.clientWidth/2;
      label.width => (<-.panel.width*2/3);
      field.x => <-.label.width;
      field.width => (<-.panel.width/3);
      field.decimalPoints = 1;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein qstepk {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      panel.visible => (<-.<-.ffac_data.select_fs==1);
      flabel => "Step size of k";
      fval => <-.ffac_data.k_step;
      x => <-.panel.clientWidth/2;
      y => (<-.indexhstart.y + <-.indexhstart.height + 5);
      width=> <-.panel.clientWidth/2;
      label.width => (<-.panel.width*2/3);
      field.x => <-.label.width;
      field.width => (<-.panel.width/3);
      field.decimalPoints = 1;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVffieldTypein indexl {
      UIparent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      //      panel.visible => (<-.<-.ffac_data.select_fs < 2);
      flabel => "Diffraction index l";
      fval => <-.ffac_data.l;
      y => (<-.indexkstart.y + <-.indexkstart.height + 5);
      width=> <-.panel.clientWidth-10;
      label.width => (<-.panel.width*2/3);
      field.x => <-.label.width;
      field.width => (<-.panel.width*2/3);
      field.decimalPoints = 1;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle plot_calc<NEx=216.,NEy=441.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((indexl.y + indexl.height) + 5);
      width => parent.clientWidth;
      label = "Plot calculated values";
      set => (ffac_data.plot_calc); 
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle plot_exp<NEx=216.,NEy=441.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((plot_calc.y + plot_calc.height) + 5);
      width => parent.clientWidth;
      label = "Plot experimental values";
      set => (ffac_data.plot_exp);
      active => <-.load_data.got_exp_data && <-.ffac_data.select_fs==2;
    };
    GMOD.parse_v parse_v<NEx=319.,NEy=77.> {
      v_commands = "ffac_data.plot_exp= 0;";
      trigger => select_fs.selectedItem;
      on_inst = 0;
      relative => <-;				    
    };
    UIbutton plot_ffactors_button<NEx=594.,NEy=252.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      label => "PLOT";
      width => parent.clientWidth/3;
      x => parent.clientWidth/3;
      y => (plot_exp.y + plot_exp.height + 5);
      active=> (<-.ffac_data.plot_calc || <-.ffac_data.plot_exp);
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle new_view<NEx=216.,NEy=441.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => ((plot_ffactors_button.y + plot_ffactors_button.height) + 5);
      width => parent.clientWidth;
      label = "Open in new viewer";
      set => (ffac_data.new_view); 
    };
    CCP3.ROD.Modules.plot_ffactors plot_ffactors<NEx=407.,NEy=484.> {
      trigger => <-.plot_ffactors_button.do;
      ffac_data => <-.ffac_data;
      params => <-.params;
      load_data => <-.load_data;
    };
  };					   
};
