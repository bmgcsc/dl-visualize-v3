
flibrary Read<NEeditable=1,
#ifndef DLV_NO_DLLS
	     dyn_libs="libCCP3rod",
#endif // DLV_NO_DLLS
	     export_cxx=1,
	     build_dir="avs/src/express",
             cxx_hdr_files="avs/src/express/calcs.hxx avs/src/core/fb_gen.hxx avs/src/rod/rod_load_gen.hxx",
	     out_hdr_file="rod_read.hxx",
	     out_src_file="rod_read.cxx"> {
  macro loadrodUI {  // Bascially a copy of core/UI/file_ops LoadUI
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},
      NEx=286.,NEy=22.>;
    CCP3.ROD.Modules.LoadData &load_data<NEx=792.,NEy=77.,NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
      width = 300;
      height = 400;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
#ifdef MSDOS
      y = 0;
#endif // MSDOS
      title = "Load rod files";
      ok = 0;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
    };
    UIpanel panel<NEx=374.,NEy=209.> {
      parent => <-.dialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    UIlabel UIlabel1 {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y = 0;
      alignment = "left";
      width => parent.clientWidth;
      label = "Bulk Structure";
    };
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileA<NEx=704.,NEy=462.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}> => <-.load_data.file_bulk;
      pattern = "*.bul";
      //manageFB.pattern => <-.pattern;
      parent => <-.panel;
      title = "Load rod bulk structure";
      x = 0;
      y => (<-.UIlabel1.y + <-.UIlabel1.height + 5);
    };
/*    UIlabel UIlabel2 {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.FileA.y + <-.FileA.height + 10);
      alignment = "left";
      width => parent.clientWidth;
      label = "Surface Structure";
    };		   
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileB<NEx=704.,NEy=462.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}> => <-.load_data.file_surf;
      pattern = "*.sur";
      parent => <-.panel;
      title = "Load rod surface structure";
      x = 0;
      y => (<-.UIlabel2.y + <-.UIlabel2.height + 5);
      //      manageFB.pattern => <-.pattern;
    };*/
    UIlabel UIlabel3 {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.FileA.y + <-.FileA.height + 10);
      alignment = "left";
      width => parent.clientWidth;
      label = "Fit model / Surface Structure";
    };		   
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileC<NEx=704.,NEy=462.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}> => <-.load_data.file_fit;
      pattern = "*.fit";
      parent => <-.panel;
      title = "Load .fit or .sur file";
      x = 0;
      y => (<-.UIlabel3.y + <-.UIlabel3.height + 5);
      //      manageFB.pattern => <-.pattern;
    };
    UIlabel UIlabel4 {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.FileC.y + <-.FileC.height + 10);
      alignment = "left";
      width => parent.clientWidth;
      label = "Experimental Data";
    };		   
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileD<NEx=704.,NEy=462.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}> => <-.load_data.file_exp_data;
      pattern = "*.dat";
      parent => <-.panel;
      title = "Load data file";
      x = 0;
      y => (<-.UIlabel4.y + <-.UIlabel4.height + 5);
      //      manageFB.pattern => <-.pattern;
    };
    UIlabel UIlabel5 {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      y => (<-.FileD.y + <-.FileD.height + 10);
      alignment = "left";
      width => parent.clientWidth;
      label = "Parameter Data";
    };		   
    CCP3.Core_Macros.UI.UIobjs.FileBrowser FileE<NEx=704.,NEy=462.> {
      preferences => <-.prefs;
      filename<NEportLevels={2,0}> => <-.load_data.file_par;
      pattern = "*.par";
      parent => <-.panel;
      title = "Load parameter file";
      x = 0;
      y => (<-.UIlabel5.y + <-.UIlabel5.height + 5);
      //      manageFB.pattern => <-.pattern;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVtoggle new_view<NEx=462.,NEy=451.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y => (<-.FileE.y + <-.FileE.height + 10);
      width => parent.clientWidth;
      label = "Open in new viewer";
      set => <-.load_data.new_view;
    };
  };
};
