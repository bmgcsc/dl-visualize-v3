
flibrary View<NEeditable=1,
#ifndef DLV_NO_DLLS
	     dyn_libs="libCCP3rod",
#endif // DLV_NO_DLLS
	     export_cxx=1,
	     build_dir="avs/src/express",
             cxx_hdr_files="avs/src/express/calcs.hxx avs/src/core/fb_gen.hxx avs/src/rod/rod_view_gen.hxx",
	     out_hdr_file="rod_view.hxx",
	     out_src_file="rod_view.cxx"> {
  macro viewrodUI {  // Bascially a copy of core/UI/file_ops LoadUI
    CCP3.Core_Macros.Core.preferences &prefs<NEportLevels={2,1},NEx=286.,NEy=22.>;
    //    link model_type<NEportLevels={2,1},NEx=855.,NEy=27.> => <-.model_type;
    int model_type<NEportLevels={3,0},NEx=135.,NEy=54.>;// => <-.model_type;
    CCP3.ROD.Modules.LoadData &load_data<NEx=792.,NEy=77.,NEportLevels={2,0}>;
    CCP3.Core_Macros.UI.UIobjs.DLVdialog dialog<NEx=220.,NEy=121.> {
      width = 400;
      height = 500;
#ifdef MSDOS
      y = 0;
#endif // MSDOS
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      title = "View rod files";
      ok = 0;
      okButton = 1;
      cancel = 0;
      cancelButton = 1;
    };
    UIpanel panel<NEx=374.,NEy=209.> {
      parent => <-.dialog;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      x = 0;
      y = 0;
      width => parent.clientWidth;
      height => parent.clientHeight;
    };
    CCP3.Core_Macros.UI.UIobjs.DLVlist textbox<NEx=253.,NEy=297.> {
      parent => <-.panel;
      &color => <-.prefs.colour;
      &fontAttributes => <-.prefs.fontAttributes;
      listIsEditable = 0;
      strings => <-.view_rod_files.text;
      x = 5;
      y = 5;
      width => (parent.clientWidth - 10);
      height => (parent.clientHeight - 20 - <-.select.height);
    };	
    CCP3.Core_Macros.UI.UIobjs.DLVradioBox select<NEx=18.,NEy=432.> {
      parent => <-.panel;
      color => <-.prefs.colour;
      fontAttributes => <-.prefs.fontAttributes;
      labels => <-.select_list;
      selectedItem = -1;
      y => (<-.textbox.y + <-.textbox.height + 5); 
      title => "Select a file type to view";
      width => parent.clientWidth;
      UIradioBox.orientation = "horizontal";
      UIradioBox.itemWidth => <-.width/5.0;
      label_cmd {
	cmd<NEdisplayMode="open">[] = {{active=> <-.<-.<-.model_type==4,,,,,,,,,,,,,,},
				       {active=> <-.<-.<-.model_type==4,,,,,,,,,,,,,,},
				       {active=> <-.<-.<-.model_type==4,,,,,,,,,,,,,,},
				       {active=> <-.<-.<-.model_type==4,,,,,,,,,,,,,,},
				       {active=> <-.<-.<-.load_data.got_exp_data,,,,,,,,,,,,,,}};
      };
      label_cmd.cmd.active => <-.<-.active;
      //     label_cmd.cmd[].active = {1,1,1,1,0};
      //label_cmd.cmd[5].active = 0;
    //    UIradioBox.cmdlist[]
      active = 1;
    };
    CCP3.ROD.Modules.view_rod_files view_rod_files<NEx=583.,NEy=165.> {
      trigger => <-.select.selectedItem;
      data_file => <-.load_data.file_exp_data;
    };
    string select_list<NEportLevels=1,NEx=33.,NEy=143.>[] = {
    	"bul",
	"sur",
	"fit",
	"par",
	"dat"
    };
  };
};



