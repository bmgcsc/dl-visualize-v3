
flibrary Editors<compile_subs=0> {
  GDview_selector DLVview_selector;
  GDM.Uscene_editor DLVscene_editor {
    // Todo - when restructure editor panels, use font_info.
    CCP3.Core_Macros.Core.preferences &preferences<NEportLevels=1,
      NEx=176.,NEy=33.>;
    View_Editor {
      // Object selector
      InfoPanel {
	CurCameraFrame {
	  &color => <-.<-.<-.preferences.colour;
	  &fontAttributes => <-.<-.<-.preferences.fontAttributes;
	  width => 3 * height;
	};
	CurCameraLabel {
	  &color => <-.<-.<-.preferences.colour;
	  &fontAttributes => <-.<-.<-.preferences.fontAttributes;
	  label+nres => in_view.name;
	};
	CurObjFrame {
	  &color => <-.<-.<-.preferences.colour;
	  &fontAttributes => <-.<-.<-.preferences.fontAttributes;
	};
	CurObjLabel {
	  &color => <-.<-.<-.preferences.colour;
	  &fontAttributes => <-.<-.<-.preferences.fontAttributes;
	};
	RaiseCurObj {
	  &color => <-.<-.<-.preferences.colour;
	  &fontAttributes => <-.<-.<-.preferences.fontAttributes;
	};
	curobj_sel {
	  &color => <-.<-.<-.preferences.colour;
	  &fontAttributes => <-.<-.<-.preferences.fontAttributes;
	};
	UI {
	  panel {
	    x => ((parent.clientWidth / 2) + 5);
	    width => ((parent.clientWidth / 2) - 10);
	  };
	};
      };
    };
    // Set mouse buttons for 3D window.
    Track_Editor {
#ifdef xp_darwin
      int which_buttons = 1;
#else
      string buttons => getenv("DLV_V31_BUTTONS");
      int which_buttons => is_valid(buttons)+1;
#endif // xp_darwin
      int mode_table31[4][3] => {
	{ -1,  0, track_mode},
	{ -1,  1,         -1},
	{ -1,  2,         -1},
	{ -1, -1,         -1}
      };
      int mode_table32[4][3] => {
	{ track_mode,  0, -1},
	{ -1,          1, -1},
	{ -1,          2, -1},
	{ -1,          3, -1}
      };
      mode_table => switch(which_buttons, mode_table32, mode_table31);
      TrackSelect {
	// These don't have font_attributes.
	Rotate {
	  message => switch(which_buttons,
			    "Rotate mode: left mouse button rotates object",
			    "Rotate mode: right mouse button rotates object");
	};
	Scale {
	  message => switch(which_buttons,
			    "Scale mode: left mouse button scales object",
			    "Scale mode: right mouse button scales object");
	};
	Translate {
	  message => switch(which_buttons,
			    "X/Y Translate mode: left mouse button translates object in X/Y",
			    "X/Y Translate mode: right mouse button translates object in X/Y");
	};
	Z_Translate {
	  message => switch(which_buttons,
			    "Z Translate mode: left mouse button translates object in Z",
			    "Z Translate mode: right mouse button translates object in Z");
	};
      };
    };
    Camera_Editor {
      IUI {
	Clipping_Planes {
	  IUI {
	    Planes {
	      SliderFrame.height => (<-.Slider2.y + <-.Slider2.height + 6);
	    };
	  };
	};
      };
    };
  };
};
