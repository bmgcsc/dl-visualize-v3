
flibrary Scenes<
  NEeditable=1,
  export_cxx=1,
  libdeps="GMOD UI",
  build_dir="avs/src/express",
#ifdef MSDOS
  cxx_hdr_files="fld/Xfld.h avs/src/express/misc.hxx avs/src/express/display.hxx  avs/src/express/ui_objs.hxx",
#else
  cxx_hdr_files="fld/Xfld.h motif_ui/ui/ui_core.h avs/src/express/misc.hxx avs/src/express/display.hxx avs/src/express/ui_objs.hxx",
#endif // MSDOS
  out_hdr_file="scenes.hxx",
  out_src_file="scenes.cxx"> {
  // Todo - I don't think any of this needs fontinfo but worth checking.
  // Todo - might want to modify/simplify this
  GDM.Uscene2D DLV2Dscene {
    int View_Index<NEportLevels=1,NEx=462.,NEy=33.> = 1;
    string Model_Name<NEportLevels=1,NEx=682.,NEy=33.> = "Unknown Model";
    link edit_obj<NEportLevels=1,NEx=675.,NEy=99.>;
    // Todo - include macros for objects to attach to Top?
    Top {
      Top {
	name => <-.<-.Model_Name;
      };
    };
    View {
      ViewUI {
	PickInteractor {
	  startEvent = "<Btn1Down>";
	  runEvent = "<Btn1Motion>";
	  stopEvent = "<Btn1Up>";
	};
	ViewPanel {
	  UI {
	    UIshell UIshell {
	      width = 640;
	      height = 512;
	      showStatusBar = 0;
	      cancel = 0;
	      // Todo - Need a label for data object, or is this plot title?
	      title => "DLV2D view #" +
		<-.<-.<-.<-.<-.View_Index + " - " + <-.<-.<-.<-.<-.Model_Name;
	    };
	    panel {
	      parent => <-.UIshell;
	      x = 0;
	      y = 0;
	      width => parent.clientWidth;
	      height => parent.clientHeight;
	    };
	  };
	};
      };
      // Add name for display in camera selector
      View {
	int &close => <-.ViewUI.ViewPanel.UI.UIshell.cancel;
	int &index => <-.<-.View_Index;
	string name => "2D#" + index;
	link trans_obj => <-.<-.Top.Top;
      };
    };
  };
  GDM.Uscene3D DLV3Dscene {
    string Space<NEportLevels=1,NEx=242.,NEy=33.> = "Real";
    int View_Index<NEportLevels=1,NEx=462.,NEy=33.> = 1;
    string Model_Name<NEportLevels=1,NEx=682.,NEy=33.> = "Unknown Model";
    int space_type => (strcmp(Space, "Real") != 0);
    link edit_obj<NEportLevels=1,NEx=675.,NEy=99.>;
    Top {
      Top {
	name => <-.<-.Model_Name;
      };
      Xform {
	&mat;
	//&xlate; Todo?
      };
    };
    View {
      ViewUI {
	ViewPanel {
	  macro UI<NEvisible=1,NEx=0,NEy=0,ui_global_class=1> {
	    UIshell UIshell<NEx=121.,NEy=165.> {
	      width = 512;
	      height = 512;
	      showStatusBar = 0;
	      cancel = 0;
	      title => "DLV3D " + <-.<-.<-.<-.<-.Space + " space view #" +
		<-.<-.<-.<-.<-.View_Index + " - " + <-.<-.<-.<-.<-.Model_Name;
	    };
	    UIpanel panel<NEx=231.,NEy=253.> {
	      parent => <-.UIshell;
	      x = 0;
	      y = 0;
	      width => parent.clientWidth;
	      height => parent.clientHeight;
	    };
	  };
	};
#ifdef xp_darwin
	PickInteractor {
	  startEvent = "Control<Btn1Down>";
	  runEvent = "Control<Btn1Motion>";
	  stopEvent = "Control<Btn1Up>";
	};
#else
	PickInteractor {
	  startEvent => switch(<-.which_buttons,"<Btn3Down>","<Btn1Down>");
	  runEvent => switch(<-.which_buttons,"<Btn3Motion>","<Btn1Motion>");
	  stopEvent => switch(<-.which_buttons,"<Btn3Up>","<Btn1Up>");
	};
	string buttons => getenv("DLV_V31_BUTTONS");
	int which_buttons => is_valid(buttons)+1;
#endif // darwin
      };
      // Add name for display in camera selector
      View {
	&back_col;
	int &close => <-.ViewUI.ViewPanel.UI.UIshell.cancel;
	int &index => <-.<-.View_Index;
	string name => <-.<-.Space + "3D#" + index;
	link trans_obj => switch (is_valid(<-.<-.edit_obj)+1,
				  <-.<-.Top.Top,<-.<-.edit_obj);
      };
      PickCtrl {
	on_start => <-.<-.space_type;
      };
    };
  };
};
