
flibrary Viewers {
  "../avs/v/CCP3/viewers/scenes.v" Scenes;
  "../avs/v/CCP3/modules/viewer/modules.v" Modules;
  "../avs/v/CCP3/viewers/editors.v" Editors;
  macro DLVviewers {
    CCP3.Viewers.Editors.DLVview_selector DLVview_selector<NEx=363.,NEy=352.>;
    CCP3.Viewers.Modules.select select<NEx=517.,NEy=418.> {
      view_type+nres => <-.DLVview_selector.curr_view.picked_camera.type;
      index+nres => <-.DLVview_selector.curr_view.index;
    };
    //mlink top_objs<NEportLevels=1,NEx=585.,NEy=486.>;
    //link cur_top<NEportLevels=1,NEx=585.,NEy=567.> =>
    //  top_objs[DLVview_selector.curr_view.index - 1];
    // Todo - check that we get the correct camera type.
    CCP3.Viewers.Modules.remove remove<NEx=121.,NEy=429.> {
      trigger+nres => <-.GDview_sel.selected.view.close;
      view_type+nres => <-.GDview_sel.selected.view.picked_camera.type;
      index+nres => <-.GDview_sel.selected.view.index;
    };
    link save<NEportLevels=1,NEx=737.,NEy=264.>;
    CCP3.Viewers.Modules.save_transform save_transform<NEx=737.,NEy=418.,
      NEportLevels={0,1},instanced=0> {
      flag => <-.save;
      button+nres => <-.DLVscene_editor.curr_view.mouseEvents.button;
      state+nres => <-.DLVscene_editor.curr_view.mouseEvents.state;
    };
    GMOD.instancer instance_transform<NEx=737.,NEy=341.> {
      Value => <-.save;
      Group => <-.save_transform;
    };
    // Build a selector that gets the object closing, rather than selected.
    GDview_sel GDview_sel<NEx=110.,NEy=352.> {
      views => <-.DLVview_selector.input_views;
      entries {
	active+nres => view.close;
      };
    };
    CCP3.Viewers.Editors.DLVscene_editor DLVscene_editor<NEx=363.,NEy=517.> {
      in_view => <-.DLVview_selector.curr_view;
      // Todo - remove. This is temporary stuff before view panels are rebuilt
      View_Editor {
	IUI {
	  panel {
	    parent => <-.CCP3shell.UIpanel;
	  };
	  CCP3.Version2.Macros.Misc.CCP3shell CCP3shell<NEx=99.,NEy=22.> {
	    UIshell {
	      visible => <-.<-.<-.UI.option.set;
	      title = "View";
	      width = 512;
	      height = 550;
	    };
	  };
	};
	// 7.1.1 fix
	GDview_edit {
	  renderer = 1;
	};
	UI {
	  option {
	    label = "View";
	  };
	};
      };
      Track_Editor {
	IUI {
	  panel {
	    parent => <-.CCP3shell.UIpanel;
	  };
	  CCP3.Version2.Macros.Misc.CCP3shell CCP3shell<NEx=132.,NEy=44.> {
	    UIshell {
	      visible => <-.<-.<-.UI.option.set;
	      title = "Transformations";
	      width = 512;
	      height = 665;
	    };
	  };
	};
	UI {
	  option {
	    label = "Transform";
	  };
	};
	//cur_obj => <-.<-.cur_top;
	cur_obj+nres => <-.in_view.trans_obj;
      };
      Light_Editor {
	IUI {
	  panel {
	    parent => <-.CCP3shell.UIpanel;
	  };
	  CCP3.Version2.Macros.Misc.CCP3shell CCP3shell<NEx=110.,NEy=33.> {
	    UIshell {
	      visible => <-.<-.<-.UI.option.set;
	      title = "Light";
	      width = 512;
	      height = 600;
	    };
	  };
	};
	UI {
	  option {
	    label = "Light";
	  };
	};
      };
      Camera_Editor {
	IUI {
	  panel {
	    parent => <-.CCP3shell.UIpanel;
	  };
	  CCP3.Version2.Macros.Misc.CCP3shell CCP3shell<NEx=132.,NEy=22.> {
	    UIshell {
	      visible => <-.<-.<-.UI.option.set;
	      title = "Camera";
	      width = 512;
	      height = 512;
	    };
	  };
	};
	UI {
	  option {
	    label = "Camera";
	  };
	};
      };
      Object_Editor {
	IUI {
	  panel {
	    parent => <-.CCP3shell.UIpanel;
	  };
	  CCP3.Version2.Macros.Misc.CCP3shell CCP3shell<NEx=110.,NEy=22.> {
	    UIshell {
	      visible => <-.<-.<-.UI.option.set;
	      title = "Object";
	      width = 512;
	      height = 600;
	    };
	  };
	};
	UI {
	  option {
	    label = "Object";
	  };
	};
      };
      Datamap_Editor {
	IUI {
	  panel {
	    parent => <-.CCP3shell.UIpanel;
	  };
	  CCP3.Version2.Macros.Misc.CCP3shell CCP3shell<NEx=154.,NEy=22.> {
	    UIshell {
	      visible => <-.<-.<-.UI.option.set;
	      title = "Datamap";
	      width = 512;
	      height = 550;
	    };
	  };
	};
	UI {
	  option {
	    label = "Datamap";
	  };
	};
      };
      Graph_Editor {
	IUI {
	  panel {
	    parent => <-.CCP3shell.UIpanel;
	  };
	  CCP3.Version2.Macros.Misc.CCP3shell CCP3shell<NEy=11.> {
	    UIshell {
	      visible => <-.<-.<-.UI.option.set;
	      title = "Graph Editor";
	      width = 512;
	      height = 650;
	    };
	  };
#ifdef DLV_RELEASE
#ifndef MSDOS
	  Viewport {
	    IUI {
	      BackgroundColor {
		height = 42;
	      };
	      GeomWidth {
		height = 148;
	      };
	      GeomHeight {
		height = 148;
	      };	      
	    };
	  };
#endif // MSDOS
#endif // DLV_RELEASE
	};
	UI {
	  option {
	    label = "Graph";
	  };
	};
      };
      Print_Editor {
	IUI {
	  panel {
	    parent => <-.CCP3shell.UIpanel;
	  };
	  CCP3.Version2.Macros.Misc.CCP3shell CCP3shell<NEx=198.,NEy=-11.> {
	    UIshell {
	      visible => <-.<-.<-.UI.option.set;
	      title = "Print";
	      width = 512;
	      height = 512;
	    };
	  };
#ifndef MSDOS
	  FileText {
	    height = 34;
	  };
#endif // MSDOS
	};
	UI {
	  option {
	    label = "Print";
	  };
	};
      };
    };
  };
};
