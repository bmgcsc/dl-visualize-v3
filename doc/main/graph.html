<html>
<head>
<meta http-equiv="Content-Type" content="text/html">
<title>DL Visualize - Using the Graph Editor</title>
</head>

<body>
<h3 align="center">Using the Graph Editor</h3>

<p>
Once you have drawn a graph of a 1D property, you can change various
options about the graph to improve its display. First you must select
the graph in the 2D viewer window. You should then be able to
select <b>Graph</b> on the <b>Display</b> menu in DL Visualize to
display the Graph Editor panel.
</p>

<p>
The option box at the top (labelled Graph) contains seven choices:
Title, Properties, Style, X axis, Y axis, Legend and Viewport.
</p>

<dl>
  <dt>Graph <b>Title</b> contains options to change properties
      relating to the graph title.
  </dt>
  <dd>Using the <b>Show Title</b> check box, you can choose to show or
      to completely.hide the title.
  </dd>
  <dd>The <b>Title</b> text box allows you to change the text
      displayed for the title. By default this is generic text so you
      will almost certainly want to change it.
  </dd>
  <dd>The <b>Fill Color</b> is the colour of the rectangle containing
      the title. By default, this is transparent but you can change it
      to another colour by entering either a colour name (e.g. red) or
      hex code in the form #rrggbb (e.g. #FF0000) in this box.
  </dd>
  <dd>The <b>Title Height</b> controls the size of the title text and
      can be specified either as a percentage of the total height of
      the graph or in millimetres. The default setting is 8%. You will
      want to adjust this to make sure that your title text is
      appropriately sized.
  </dd>
  <dd>The <b>Frame Width</b> option draws a border around the title
      text in the same colour as the text. Increasing the value of the
      slider will increase the border size. By default there is no
      border.
  </dd>
  <dd>The <b>Title Font</b> allows the format of the title to be
      changed amongst a pre-determined list of styles, such as Courier
      and Helvetica Bold.
  </dd>
</dl>

<dl>
  <dt>Graph <b>Properties</b> contains options to change the general
      graph.
  </dt>
  <dd>The <b>Current Graph</b> slider allows you to select which plot
      of data to modify in the rest of the window. Graphs that appear
      higher in the legend have smaller numbers (starting at 0).  If
      you have only plotted one set of data then this slider will be
      disabled.
  </dd>
  <dd><b>Drawing Priority</b> determines the order in which graphs
      will be drawn. A higher number means that the graph will be
      drawn later and therefore will appear on top of other graphs.
      By default, this value is 0 for all graphs and therefore
      priority is determined by the order of graphs in the legend. You
      can use the slider to raise this priority and therefore make
      some graphs more visible.
  </dd>
  <dd><b>Graph Type</b> offers a selection of options which determine
      the style of graphs being drawn. The selection is curve (the
      default), bars (which draws 3D-style bars at various intervals),
      barlines (similar to bars but without the 3D effect), scatter
      (which draws small squares at different points along the line),
      area (shades the area under the curve), staircase (which redraws
      the curve in small horizontal and vertical segments) and
      stairarea (which shades the area under staircase). Click on one
      of these options in the list box to select it.<br>
      The most commonly used options are curve and area.
  </dd>
  <dd><b>Legend</b> is the text displayed in the legend for the
      selected graph, which you are likely to want to change for each
      graph.
  </dd>
  <dd>Checking the <b>Uniform Scale</b> check box equalizes the scales
      on the X and Y axes to make them consistent.
  </dd>
  <dd><b>Nice Limits</b> adjusts the scales on the axes slightly so
      that none of the graphs being displayed actually overlaps the
      border of the graph window.
  </dd>
  <dd><b>Color +, Color -</b> and <b>Baseline</b> all determine the
      appearance of the data points on the graph.  Baseline marks a
      threshold in the data (on the Y axis).  Above the baseline, data
      points or lines are marked with Color +, below with Color -.<br>
      To use this feature, enter a value into Baseline and press Tab
      or Enter to accept it.  Also enter colours, either in name form
      or #rrggbb form, into the Color + and Color � fields.<br>
      N.B. The threshold for Color - does not work with the area or
      stairarea graph types: these shade using Color + under all
      circumstances but they do shade the area between the graph and
      the baseline value. If you wish to remove a certain line from
      the graph display then this can be done by unchecking the
      <b>line visible</b> checkbox in the <b>1D Data Display</b>
      window.
  </dd>
</dl>

<dl>
  <dt>Graph <b>Style</b> modifies the style of the graph selected in
      the Properties section using the <b>Current Graph</b> slider.
  </dt>
  <dd><b>Line Style</b> allows a choice of appearance for lines on a
      <I>curve</I>, <I>barline </I> or <I>staircase </I> style
      graph. You can select here between solid (the default), dashed,
      dotted or dashed and dotted styles of graph.
  </dd>
  <dd><b>Line Width</b> modifies the width of the line for a
      <I>curve</I>, <I>barline </I>or <I>staircase </I>style
      graph. The width can be adjusted by moving the slider.
  </dd>
  <dd><b>Bar Width</b> modifies the widths of bars for a <I>bar</I>
      type graph by moving the slider, but there is not a large amount
      of adjustment available.
  </dd>
  <dd><b>Bar Offset</b> modifies the positioning of bars or barlines
      in these two types of graph. A value of 0.0 centres the bar
      between consecutive X axis values whereas 1.0 aligns it with the
      second of these values.
  </dd>
  <dd><b>Shift Position</b> does exactly the same as Bar Offset except
      for staircase and stairarea type graphs.
  </dd>
</dl>

<dl>
  <dt>The <b>X axis</b> and <b>Y axis</b> sections are basically
      identical and adjust settings to do with the relevant axes.
  </dt>
  <dd>To hide all markings and text to do with the axis, clear the
      <b>Show X axis</b> or <b>Show Y axis</b> check box.
  </dd>
  <dd>The axis labels can also be changed by changing the values in
      the <b>Text</b> text box.  However, the defaults for these are
      usually sensibly obtained from the data source so they might not
      need changing.
  </dd>
  <dd>The <b>Min</b> and <b>Max</b> text boxes adjust the limits of
      the scales on each axis.  If you wish to narrow down a small
      amount of data then you can enter the appropriate limits into
      these boxes to zoom in more closely.  Note that both will
      default to zero if the other one is changed so you must set a
      <b>Max</b> value if you set a <b>Min</b> one.  By default these
      are set to the maximum and minimum values in the dataset to
      correctly show all the data.
  </dd>
  <dd>The <b>Step</b> option adjusts the spacing of labels along the
      axis. Markings are displayed at this interval, which is normally
      set to a sensible but invisible default.
  </dd>
  <dd>The <b>Label Height</b> setting adjusts the size of the label
      markings along the axis. The slider sets the values in terms of
      either a percentage of the overall graph size or in
      millimetres. When adjusting this setting, you must choose one of
      these options or moving the slider will have no effect. (Note
      that if the label size is set to 0.00 then the axis label will
      disappear also.)
  </dd>
  <dd>The <b>Text Height</b> controls exactly the same property as the
      Label Height except that this is for the axis label (set in the
      Text control).
  </dd>
  <dd><b>Axis Scale</b> is useful when datasets with large variations
      in values are being plotted.  The scale along the axis can be
      varied between linear (the default), logarithmic (base 10) and
      power scales as necessary.
  </dd>
</dl>

<dl>
  <dt>The <b>Legend</b> settings control the appearance of the legend
      box.
  </dt>
  <dd>You can choose to show or hide the legend altogether using the
      <b>Show Legend</b> check box.  This will probably be useful if
      you are only plotting one set of data on the graph and therefore
      do not need a legend.
  </dd>
  <dd>The title of the legend can be changed using the <b>Title</b>
      text box. However, the default of Legend is usually appropriate.
  </dd>
  <dd>The <b>Title Height</b> and <b>Label Height</b> options control
      the size of the legend title and data series labels
      respectively, either in percentages of the overall legend area
      or in millimetres. The two options will most likely want to be
      set to a similar position on the slider to each other to keep
      the text sizes similar. Values of about 2.00 to 3.00 are
      appropriate here.
  </dd>
  <dd><b>Columns</b> decides the number of columns in which the items
      in the legend will be arranged.  A value of 1 is almost always
      suitable here.
  </dd>
  <dd><b>X origin</b> and <b>Y origin</b> determine the position of
      the bottom-left hand corner of the legend box. The default is
      (0.00, 0.00) on a scale where each axis goes from -5.00 to
      5.00. You will want to adjust these settings to move the legend
      off the main section of the graph.  An X origin of 5.00 and a Y
      origin of �1.00 will move the legend box off the right of the
      graph. Otherwise, the values will depend on how many legend
      items there are and how long their text is.
  </dd>
</dl>

<dl>
  <dt>The <b>Viewport</b> settings determine the appearance of the
      graph plot itself.
  </dt>
  <dd><b>Color</b> is the background colour of the graph plot.  You
      can either enter a colour name in here (for example beige or
      green or navyblue), a colour in #rrggbb mode or a special
      colour: background to be the same colour as the background or
      antibackground to contrast with the background. If you enter an
      invalid colour then the background will turn white.  Note that
      the default black background will print out on paper as white.
  </dd>
  <dd>The four <b>Width</b> and <b>Height</b> sliders control the
      proportionate width and height of the graph plot. Increasing the
      Upper sliders or decreasing the Lower sliders makes that
      dimension be greater in proportion to the other one.  However,
      the default settings are usually appropriate. An alternative
      method of doing this is by dragging on the graph with the middle
      (or wheel) button on the mouse held down although this will not
      change the values on the sliders here.
  </dd>
</dl>

<p>
The most useful settings to adjust are the graph title, the line
colours, &quot;Nice Limits&quot;, the axis labels and scales and the
legend position.
</p>

</body>
</html>
